==================
Command Line Tools
==================

The ``pipeliner`` command allows pipeliner functions to be run from the command line.

.. argparse::
   :module: pipeliner.api.command_line
   :func: get_arguments
   :prog: pipeliner