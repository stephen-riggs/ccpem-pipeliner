======================
Tools for Running Jobs
======================

The job factory, job manager and job runner are used to create, schedule and run
jobs. These objects and the functions inside them generally do not need to be accessed
directly, usually the desired action can be done more easily through the
functions in the :doc:`pipeliner api<pipeliner_api>`

The job factory
---------------

.. automodule:: pipeliner.job_factory
    :members:
    :undoc-members:
    :show-inheritance:

The job manager
---------------

.. automodule:: pipeliner.job_manager
    :members:
    :undoc-members:
    :show-inheritance:

The job runner
--------------

.. automodule:: pipeliner.scripts.job_runner
    :undoc-members:
    :show-inheritance: