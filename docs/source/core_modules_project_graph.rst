==============
Pipeline Tools
==============

Nodes and Processes
-------------------

.. automodule:: pipeliner.data_structure
    :members:
    :undoc-members:
    :show-inheritance:

ProjectGraph
------------

.. automodule:: pipeliner.project_graph
    :members:
    :undoc-members:
    :show-inheritance:

Metadata Tools
--------------

.. automodule:: pipeliner.metadata_tools
    :members:
    :undoc-members:
    :show-inheritance:

