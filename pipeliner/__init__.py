from importlib_metadata import version

from . import env_setup

__version__ = version("pipeliner")

# Set up python environment for project
try:
    env_setup.setup_python_environment()
except Exception:
    print("WARNING: unable to set up pipeliner python environment")
