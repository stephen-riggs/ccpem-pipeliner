#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from typing import TypedDict, List

# pipeliner style node labels AKA "top-level node names"
NODE_ATOMCOORDS = "AtomCoords"
NODE_ATOMCOORDSGROUPMETADATA = "AtomCoordsGroupMetadata"
NODE_DENSITYMAP = "DensityMap"
NODE_DENSITYMAPMETADATA = "DensityMapMetadata"
NODE_DENSITYMAPGROUPMETADATA = "DensityMapGroupMetadata"
NODE_EULERANGLES = "EulerAngles"
NODE_EVALUATIONMETRIC = "EvaluationMetric"
NODE_IMAGE2D = "Image2D"
NODE_IMAGE2DSTACK = "Image2DStack"
NODE_IMAGE2DMETADATA = "Image2DMetadata"
NODE_IMAGE2DGROUPMETADATA = "Image2DGroupMetadata"
NODE_IMAGE3D = "Image3D"
NODE_IMAGE3DMETADATA = "Image3DMetadata"
NODE_IMAGE3DGROUPMETADATA = "Image3DGroupMetadata"
NODE_LIGANDDESCRIPTION = "LigandDescription"
NODE_LOGFILE = "LogFile"
NODE_MASK2D = "Mask2D"
NODE_MASK3D = "Mask3D"
NODE_MICROGRAPHCOORDS = "MicrographCoords"
NODE_MICROGRAPHCOORDSGROUP = "MicrographCoordsGroup"
NODE_MICROGRAPH = "Micrograph"
NODE_MICROGRAPHMETADATA = "MicrographMetadata"
NODE_MICROGRAPHGROUPMETADATA = "MicrographGroupMetadata"
NODE_MICROGRAPHMOVIE = "MicrographMovie"
NODE_MICROGRAPHMOVIEMETADATA = "MicrographMovieMetadata"
NODE_MICROGRAPHMOVIEGROUPMETADATA = "MicrographMovieGroupMetadata"
NODE_MICROSCOPEDATA = "MicroscopeData"
NODE_MLMODEL = "MlModel"
NODE_OPTIMISERDATA = "OptimiserData"
NODE_PARAMSDATA = "ParamsData"
NODE_PARTICLEGROUPMETADATA = "ParticleGroupMetadata"
NODE_PROCESSDATA = "ProcessData"
NODE_RESTRAINTS = "Restraints"
NODE_RIGIDBODIES = "RigidBodies"
NODE_SEQUENCE = "Sequence"
NODE_SEQUENCEGROUP = "SequenceGroup"
NODE_SEQUENCEALIGNMENT = "SequenceAlignment"
NODE_STRUCTUREFACTORS = "StructureFactors"
NODE_TILTSERIES = "TiltSeries"
NODE_TILTSERIESMETADATA = "TiltSeriesMetadata"
NODE_TILTSERIESGROUPMETADATA = "TiltSeriesGroupMetadata"
NODE_TILTSERIESMOVIE = "TiltSeriesMovie"
NODE_TILTSERIESMOVIEMETADATA = "TiltSeriesMovieMetadata"
NODE_TILTSERIESMOVIEGROUPMETADATA = "TiltSeriesMovieGroupMetadata"
NODE_TOMOGRAM = "Tomogram"
NODE_TOMOGRAMMETADATA = "TomogramMetadata"
NODE_TOMOGRAMGROUPMETADATA = "TomogramGroupMetadata"
NODE_TOMOOPTIMISATIONSET = "TomoOptimisationSet"
NODE_TOMOTRAJECTORYDATA = "TomoTrajectoryData"
NODE_TOMOMANIFOLDDATA = "TomoManifoldData"
NODE_NEWNODETYPE = "NEW NODE TYPE (not recommended)"


# General Nodes types used by Relion 4
NODE_MOVIES_LABEL = "relion.MovieStar"
NODE_MICS_LABEL = "relion.MicrographStar"
NODE_MIC_COORDS_LABEL = "relion.CoordinateStar"
NODE_PART_DATA_LABEL = "relion.ParticleStar"
NODE_REFS_LABEL = "relion.ReferenceStar"
NODE_3DREF_LABEL = "relion.DensityMap"
NODE_MASK_LABEL = "relion.Mask"
NODE_OPTIMISER_LABEL = "relion.OptimiserStar"
NODE_HALFMAP_LABEL = "relion.HalfMap"
NODE_RESMAP_LABEL = "relion.LocalResolutionMap"
NODE_PDF_LOGFILE_LABEL = "relion.PdfLogfile"
NODE_POST_LABEL = "relion.PostprocessStar"
NODE_POLISH_PARAMS_LABEL = "relion.PolishParams"
NODE_OTHER_LABEL = "Other"

# Paths for node display files
NODE_DISPLAY_FILE = "node_display.star"
NODE_DISPLAY_DIR = "NodeDisplay"
NODE_SUMMARIES_FILE = "node_summaries.star"
NODE_SUMMARIES_BLOCK_NAME = "pipeliner_node_summaries"

# nodes variables
NODES_DIR = ".Nodes/"

# Pipeliner style job status labels
JOBSTATUS_RUN = "Running"
JOBSTATUS_SCHED = "Scheduled"
JOBSTATUS_SUCCESS = "Succeeded"
JOBSTATUS_FAIL = "Failed"
JOBSTATUS_ABORT = "Aborted"

# control files
RELION_SUCCESS_FILE = "RELION_JOB_EXIT_SUCCESS"
RELION_FAIL_FILE = "RELION_JOB_EXIT_FAILURE"
RELION_ABORT_FILE = "RELION_JOB_EXIT_ABORTED"
RELION_ABORT_TRIGGER = "RELION_JOB_ABORT_NOW"

SUCCESS_FILE = "PIPELINER_JOB_EXIT_SUCCESS"
FAIL_FILE = "PIPELINER_JOB_EXIT_FAILED"
ABORT_FILE = "PIPELINER_JOB_EXIT_ABORTED"
ABORT_TRIGGER = "PIPELINER_JOB_ABORT_NOW"

# the file the pipeliner uses to track project names
PROJECT_FILE = ".CCPEM_pipeliner_project"
CLEANUP_LOG = ".CCPEM_pipeliner_cleanup"

# the file that stores info about a specific job
JOBINFO_FILE = ".CCPEM_pipeliner_jobinfo"


class JobInfoData(TypedDict):
    job_directory: str
    comments: List[str]
    created: str
    history: List[str]
    command_history: List[str]


# values in job options that should be treated as True
TRUES = ["yes", "true", "y", "1"]
FALSES = ["no", "false", "n", "0"]

# Trash dirs
TRASH_DIR = "Trash"
CLEANUP_DIR = "Trash/Cleanup"

# Process names and old RELION ID numbers
AUTOPICK_REF2D_NAME = "relion.autopick.ref2d"
AUTOPICK_REF3D_NAME = "relion.autopick.ref3d"
AUTOPICK_REF2D_HELICAL_NAME = "relion.autopick.ref2d.helical"
AUTOPICK_REF3D_HELICAL_NAME = "relion.autopick.ref3d.helical"
AUTOPICK_LOG_NAME = "relion.autopick.log"
AUTOPICK_LOG_HELICAL_NAME = "relion.autopick.log.helical"
AUTOPICK_TOPAZ_NAME = "relion.autopick.topaz.pick"
AUTOPICK_TOPAZ_TRAIN_NAME = "relion.autopick.topaz.train"
AUTOPICK_TOPAZ_HELICAL_NAME = "relion.autopick.topaz.pick.helical"
AUTOPICK_TOPAZ_TRAIN_HELICAL_NAME = "relion.autopick.topaz.train.helical"
AUTOPICK_DIR = "AutoPick"
AUTOPICK_RELIONSTYLE_NAME = "relion.autopick"  # will be deprecated
AUTOPICK_TYPE_NUM = 4  # deprecated

BAYESPOLISH_TRAIN_NAME = "relion.polish.train"
BAYESPOLISH_POLISH_NAME = "relion.polish"
BAYESPOLISH_DIR = "Polish"
# no relion style name
BAYESPOLISH_TYPE_NUM = 20  # deprecated

CLASS2D_PARTICLE_NAME_EM = "relion.class2d.em"
CLASS2D_HELICAL_NAME_EM = "relion.class2d.em.helical"
CLASS2D_PARTICLE_NAME_VDAM = "relion.class2d.vdam"
CLASS2D_HELICAL_NAME_VDAM = "relion.class2d.vdam.helical"
CLASS2D_DIR = "Class2D"
CLASS2D_PARTICLE_NAME = "relion.class2d"  # deprecated, VDAM and EM types now split
# no relionstyle name
CLASS2D_TYPE_NUM = 8  # deprecated

CLASS3D_PARTICLE_NAME = "relion.class3d"
CLASS3D_MULTIREF_PARTICLE_NAME = "relion.class3d.multiref"
CLASS3D_HELICAL_NAME = "relion.class3d.helical"
CLASS3D_MULTIREF_HELICAL_NAME = "relion.class3d.helical.multiref"
CLASS3D_DIR = "Class3D"
# no relionstyle name
CLASS3D_TYPE_NUM = 9  # deprecated

CTFFIND_CTFFIND4_NAME = "relion.ctffind.ctffind4"
CTFFIND_GCTF_NAME = "relion.ctffind.gctf"
CTFFIND_DIR = "CtfFind"
CTFFIND_RELIONSTYLE_NAME = "relion.ctffind"
CTFFIND_TYPE_NUM = 2  # deprecated

CTFREFINE_REFINE_NAME = "relion.ctfrefine"
CTFREFINE_DIR = "CtfRefine"
# no relionstyle name
CTFREFINE_TYPE_NUM = 21  # deprecated

DYNAMIGHT_NAME = "dynamight"
DYNAMIGHT_DIR = "DynaMight"

EXCLUDE_TILT_IMAGES_DIR = "ExcludeTiltImages"
EXCLUDE_TILT_IMAGES_NAME = "relion.excludetilts"

# external type number changed in this version
RELION_EXTERNAL_NAME = "relion.external"
EXTERNAL_DIR = "External"
# no relionstyle name
EXTERNAL_TYPE_NUM = 22  # deprecated

EXTRACT_PARTICLE_NAME = "relion.extract"
EXTRACT_HELICAL_NAME = "relion.extract.helical"
EXTRACT_DIR = "Extract"
# no relionstyle name
EXTRACT_TYPE_NUM = 5  # deprecated

IMPORT_MOVIES_NAME = "relion.import.movies"
IMPORT_OTHER_NAME = "relion.import"
IMPORT_DIR = "Import"
IMPORT_RELIONSTYLE_NAME = "relion.import"
IMPORT_TYPE_NUM = 0  # deprecated
IMPORT_COORDS_NAME = "relion.import.coordinates"

INIMODEL_JOB_NAME = "relion.initialmodel"
INIMODEL_DIR = "InitialModel"
# no relionstyle name
INIMODEL_TYPE_NUM = 18  # deprecated

JOINSTAR_MOVIES_NAME = "relion.joinstar.movies"
JOINSTAR_MICS_NAME = "relion.joinstar.micrographs"
JOINSTAR_PARTS_NAME = "relion.joinstar.particles"
JOINSTAR_DIR = "JoinStar"
JOINSTAR_RELIONSTYLE_NAME = "relion.joinstar"  # will be deprecated
JOINSTAR_TYPE_NUM = 13  # deprecated

LOCALRES_RESMAP_NAME = "relion.localres.resmap"
LOCALRES_OWN_NAME = "relion.localres.own"
LOCALRES_DIR = "LocalRes"
LOCALRES_RELIONSTYLE_NAME = "relion.localres"  # to be deprecated
LOCALRES_TYPE_NUM = 16  # deprecated

MANUALPICK_JOB_NAME = "relion.manualpick"
MANUALPICK_HELICAL_NAME = "relion.manualpick.helical"
MANUALPICK_DIR = "ManualPick"
MANUALPICK_RELIONSTYLE_NAME = "relion.manualpick"  # will be deprecated
MANUALPICK_TYPE_NUM = 3  # deprecated

MASKCREATE_JOB_NAME = "relion.maskcreate"
MASKCREATE_DIR = "MaskCreate"
# no relionstyle name
MASKCREATE_TYPE_NUM = 12  # deprecated

MODELANGELO_NAME = "modelangelo"
MODELANGELO_DIR = "ModelAngelo"

MOTIONCORR_OWN_NAME = "relion.motioncorr.own"
MOTIONCORR_MOTIONCOR2_NAME = "relion.motioncorr.motioncor2"
MOTIONCORR_DIR = "MotionCorr"
MOTIONCORR_RELIONSTYLE_NAME = "relion.motioncorr"  # will be deprecated
MOTIONCORR_TYPE_NUM = 1  # deprecated

MULTIBODY_REFINE_NAME = "relion.multibody.refine"
MULTIBODY_FLEXANALYSIS_NAME = "relion.multibody.flexanalysis"
MULTIBODY_DIR = "MultiBody"
MULTIBODY_RELIONSTYLE_NAME = "relion.multibody"  # to  be deprecated
MULTIBODY_TYPE_NUM = 19  # deprecated

POSTPROCESS_JOB_NAME = "relion.postprocess"
POSTPROCESS_DIR = "PostProcess"
# no relionstyle name
POSTPROCESS_TYPE_NUM = 15  # deprecated

REFINE3D_PARTICLE_NAME = "relion.refine3d"
REFINE3D_HELICAL_NAME = "relion.refine3d.helical"
REFINE3D_DIR = "Refine3D"
# no relionstyle name
REFINE3D_TYPE_NUM = 10  # deprecated

SELECT_ONVALUE_NAME = "relion.select.onvalue"
SELECT_AUTO2D_NAME = "relion.select.class2dauto"
SELECT_REMOVEDUP_NAME = "relion.select.removeduplicates"
SELECT_DISCARD_NAME = "relion.select.discard"
SELECT_INTERACTIVE_NAME = "relion.select.interactive"
SELECT_SPLIT_NAME = "relion.select.split"
SELECT_DIR = "Select"
SELECT_RELIONSTYLE_NAME = "relion.select"  # will be deprecated
SELECT_TYPE_NUM = 7  # deprecated

# sort is deprecated but left in for back compatibility
SORT_DIR = "Sort"
SORT_TYPE_NUMBER = 6  # deprecated

SUBTRACT_JOB_NAME = "relion.subtract"
SUBTRACT_REVERT_NAME = "relion.subtract.revert"
SUBTRACT_DIR = "Subtract"
SUBTRACT_RELIONSTYLE_NAME = "relion.subtract"  # will be deprecated
SUBTRACT_TYPE_NUM = 14  # deprecated

TOMO_ALIGN_NAME = "relion.framealigntomo"
TOMO_ALIGN_DIR = BAYESPOLISH_DIR

TOMO_ALIGN_TILTSERIES_NAME = "relion.aligntiltseries"
TOMO_ALIGN_TILTSERIES_DIR = "AlignTiltSeries"

TOMO_CTFREFINE_NAME = "relion.ctfrefinetomo"
TOMO_CTFREFINE_DIR = CTFREFINE_DIR

TOMO_DENOISE_NAME = "relion.denoisetomo"
TOMO_DENOISE_DIR = "Denoise"

TOMO_IMPORT_NAME = "relion.importtomo"
TOMO_IMPORT_DIR = IMPORT_DIR

TOMO_PICK_NAME = "relion.picktomo"
TOMO_PICK_DIR = "Picks"

TOMO_RECONSTRUCT_PARTICLES_NAME = "relion.reconstructparticletomo"
TOMO_RECONSTRUCT_DIR = "Reconstruct"

TOMO_RECONSTRUCT_TOMOGRAM_NAME = "relion.reconstructtomograms"
TOMO_RECONSTRUCT_TOMOGRAM_DIR = "Tomograms"

TOMO_SUBTOMO_NAME = "relion.pseudosubtomo"
TOMO_SUBTOMO_DIR = EXTRACT_DIR

# job names that need to be converted between RELION and the pipeliner
# either because the relion name is too general or they are ambiguous
# between RELION 4.0 and the pipeliner they need to be checked before being
# used to create a job.
JOBNAMES_NEED_CONVERSION = (
    MANUALPICK_JOB_NAME,
    EXTRACT_PARTICLE_NAME,
    CLASS2D_PARTICLE_NAME,
    CLASS3D_PARTICLE_NAME,
    REFINE3D_PARTICLE_NAME,
    CTFREFINE_REFINE_NAME,
    BAYESPOLISH_POLISH_NAME,
    IMPORT_RELIONSTYLE_NAME,
    MOTIONCORR_RELIONSTYLE_NAME,
    CTFFIND_RELIONSTYLE_NAME,
    AUTOPICK_RELIONSTYLE_NAME,
    SELECT_RELIONSTYLE_NAME,
    JOINSTAR_RELIONSTYLE_NAME,
    SUBTRACT_RELIONSTYLE_NAME,
    LOCALRES_RELIONSTYLE_NAME,
    MULTIBODY_RELIONSTYLE_NAME,
)


RELION_GENERAL_PROCS = (
    IMPORT_DIR,
    MOTIONCORR_DIR,
    CTFFIND_DIR,
    MANUALPICK_DIR,
    AUTOPICK_DIR,
    EXTRACT_DIR,
    SORT_DIR,  # deprecated but left in
    SELECT_DIR,
    CLASS2D_DIR,
    CLASS3D_DIR,
    REFINE3D_DIR,
    MASKCREATE_DIR,
    JOINSTAR_DIR,
    SUBTRACT_DIR,
    POSTPROCESS_DIR,
    LOCALRES_DIR,
    INIMODEL_DIR,
    MULTIBODY_DIR,
    BAYESPOLISH_DIR,
    CTFREFINE_DIR,
    EXTERNAL_DIR,
)

# conversions for back compatibility with 3.1
# takes 3.1 process number and returns general proctype
PROC_NUM_2_GENERAL_NAME = {
    IMPORT_TYPE_NUM: IMPORT_DIR,
    MOTIONCORR_TYPE_NUM: MOTIONCORR_DIR,
    CTFFIND_TYPE_NUM: CTFFIND_DIR,
    MANUALPICK_TYPE_NUM: MANUALPICK_DIR,
    AUTOPICK_TYPE_NUM: AUTOPICK_DIR,
    EXTRACT_TYPE_NUM: EXTRACT_DIR,
    # 6: "Sort",  # depreciated
    SORT_TYPE_NUMBER: SORT_DIR,
    SELECT_TYPE_NUM: SELECT_DIR,
    CLASS2D_TYPE_NUM: CLASS2D_DIR,
    CLASS3D_TYPE_NUM: CLASS3D_DIR,
    REFINE3D_TYPE_NUM: REFINE3D_DIR,
    # 11: "Polish",  # depreciated
    MASKCREATE_TYPE_NUM: MASKCREATE_DIR,
    JOINSTAR_TYPE_NUM: JOINSTAR_DIR,
    SUBTRACT_TYPE_NUM: SUBTRACT_DIR,
    POSTPROCESS_TYPE_NUM: POSTPROCESS_DIR,
    LOCALRES_TYPE_NUM: LOCALRES_DIR,
    # 17: "MovieRefine",  # depreciated
    INIMODEL_TYPE_NUM: INIMODEL_DIR,
    MULTIBODY_TYPE_NUM: MULTIBODY_DIR,
    BAYESPOLISH_TYPE_NUM: BAYESPOLISH_DIR,
    CTFREFINE_TYPE_NUM: CTFREFINE_DIR,
    EXTERNAL_TYPE_NUM: EXTERNAL_DIR,
}

# convert old style relion 2.x/3.x status to relion 4/pipeliner format
STATUS2LABEL = {
    0: JOBSTATUS_RUN,
    1: JOBSTATUS_SCHED,
    2: JOBSTATUS_SUCCESS,
    3: JOBSTATUS_FAIL,
    4: JOBSTATUS_ABORT,
}

# convert old relion3.x style node types to new RELION style labels
NODE_TYPE2LABEL = {
    0: NODE_MOVIES_LABEL,
    1: NODE_MICS_LABEL,
    2: NODE_MIC_COORDS_LABEL,
    3: NODE_PART_DATA_LABEL,
    # 4: "Movie data", deprecated not used anymore
    5: NODE_REFS_LABEL,
    6: NODE_3DREF_LABEL,
    7: NODE_MASK_LABEL,
    8: NODE_OPTIMISER_LABEL,  # deprecated, but left in
    9: NODE_OPTIMISER_LABEL,
    10: NODE_HALFMAP_LABEL,
    11: NODE_3DREF_LABEL,  # deprecated, but left in
    12: NODE_RESMAP_LABEL,
    13: NODE_PDF_LOGFILE_LABEL,
    14: NODE_POST_LABEL,
    15: NODE_POLISH_PARAMS_LABEL,
    99: NODE_OTHER_LABEL,
}

# convert old relion3.x style node types to new style labels
NODE_INT2NODELABEL = {
    0: NODE_MICROGRAPHMOVIEGROUPMETADATA,
    1: NODE_MICROGRAPHGROUPMETADATA,
    2: NODE_MICROGRAPHCOORDSGROUP,
    3: NODE_PARTICLEGROUPMETADATA,
    # 4: "Movie data", deprecated not used anymore
    5: NODE_IMAGE2DGROUPMETADATA,
    6: NODE_DENSITYMAP,
    7: NODE_MASK3D,
    8: NODE_OPTIMISERDATA,  # deprecated, but left in
    9: NODE_OPTIMISERDATA,
    10: NODE_DENSITYMAP,
    11: NODE_DENSITYMAP,  # deprecated, but left in
    12: NODE_IMAGE3D,
    13: NODE_LOGFILE,
    14: NODE_PROCESSDATA,
    15: NODE_PROCESSDATA,
}

ISO3166_COUNTRIES = {
    "Afghanistan": "AF",
    "Albania": "AL",
    "Algeria": "DZ",
    "American Samoa": "AS",
    "Andorra": "AD",
    "Angola": "AO",
    "Anguilla": "AI",
    "Antarctica": "AQ",
    "Antigua and Barbuda": "AG",
    "Argentina": "AR",
    "Armenia": "AM",
    "Aruba": "AW",
    "Australia": "AU",
    "Austria": "AT",
    "Azerbaijan": "AZ",
    "Bahamas": "BS",
    "Bahrain": "BH",
    "Bangladesh": "BD",
    "Barbados": "BB",
    "Belarus": "BY",
    "Belgium": "BE",
    "Belize": "BZ",
    "Benin": "BJ",
    "Bermuda": "BM",
    "Bhutan": "BT",
    "Bolivia": "BO",
    "Bonaire, Sint Eustatius and Saba": "BQ",
    "Bosnia and Herzegovina": "BA",
    "Botswana": "BW",
    "Bouvet Island": "BV",
    "Brazil": "BR",
    "British Indian Ocean Territory": "IO",
    "Brunei Darussalam": "BN",
    "Bulgaria": "BG",
    "Burkina Faso": "BF",
    "Burundi": "BI",
    "Cabo Verde": "CV",
    "Cambodia": "KH",
    "Cameroon": "CM",
    "Canada": "CA",
    "Cayman Islands": "KY",
    "Central African Republic": "CF",
    "Chad": "TD",
    "Chile": "CL",
    "China": "CN",
    "Christmas Island": "CX",
    "Cocos (Keeling) Islands": "CC",
    "Colombia": "CO",
    "Comoros": "KM",
    "Democratic Republic of Congo": "CD",
    "Congo": "CG",
    "Cook Islands": "CK",
    "Costa Rica": "CR",
    "Croatia": "HR",
    "Cuba": "CU",
    "Curacao": "CW",
    "Cyprus": "CY",
    "Czechia": "CZ",
    "Cote d'Ivoire": "CI",
    "Denmark": "DK",
    "Djibouti": "DJ",
    "Dominica": "DM",
    "Dominican Republic": "DO",
    "Ecuador": "EC",
    "Egypt": "EG",
    "El Salvador": "SV",
    "Equatorial Guinea": "GQ",
    "Eritrea": "ER",
    "Estonia": "EE",
    "Eswatini": "SZ",
    "Ethiopia": "ET",
    "Falkland Islands": "FK",
    "Faroe Islands": "FO",
    "Fiji": "FJ",
    "Finland": "FI",
    "France": "FR",
    "French Guiana": "GF",
    "French Polynesia": "PF",
    "French Southern Territories": "TF",
    "Gabon": "GA",
    "Gambia": "GM",
    "Georgia": "GE",
    "Germany": "DE",
    "Ghana": "GH",
    "Gibraltar": "GI",
    "Greece": "GR",
    "Greenland": "GL",
    "Grenada": "GD",
    "Guadeloupe": "GP",
    "Guam": "GU",
    "Guatemala": "GT",
    "Guernsey": "GG",
    "Guinea": "GN",
    "Guinea-Bissau": "GW",
    "Guyana": "GY",
    "Haiti": "HT",
    "Heard Island and McDonald Islands": "HM",
    "Holy See": "VA",
    "Honduras": "HN",
    "Hong Kong": "HK",
    "Hungary": "HU",
    "Iceland": "IS",
    "India": "IN",
    "Indonesia": "ID",
    "Iran": "IR",
    "Iraq": "IQ",
    "Ireland": "IE",
    "Isle of Man": "IM",
    "Israel": "IL",
    "Italy": "IT",
    "Jamaica": "JM",
    "Japan": "JP",
    "Jersey": "JE",
    "Jordan": "JO",
    "Kazakhstan": "KZ",
    "Kenya": "KE",
    "Kiribati": "KI",
    "Kuwait": "KW",
    "Kyrgyzstan": "KG",
    "Laos": "LA",
    "Latvia": "LV",
    "Lebanon": "LB",
    "Lesotho": "LS",
    "Liberia": "LR",
    "Libya": "LY",
    "Liechtenstein": "LI",
    "Lithuania": "LT",
    "Luxembourg": "LU",
    "Macao": "MO",
    "Madagascar": "MG",
    "Malawi": "MW",
    "Malaysia": "MY",
    "Maldives": "MV",
    "Mali": "ML",
    "Malta": "MT",
    "Marshall Islands": "MH",
    "Martinique": "MQ",
    "Mauritania": "MR",
    "Mauritius": "MU",
    "Mayotte": "YT",
    "Mexico": "MX",
    "Micronesia": "FM",
    "Moldova": "MD",
    "Monaco": "MC",
    "Mongolia": "MN",
    "Montenegro": "ME",
    "Montserrat": "MS",
    "Morocco": "MA",
    "Mozambique": "MZ",
    "Myanmar": "MM",
    "Namibia": "NA",
    "Nauru": "NR",
    "Nepal": "NP",
    "Netherlands": "NL",
    "New Caledonia": "NC",
    "New Zealand": "NZ",
    "Nicaragua": "NI",
    "Niger": "NE",
    "Nigeria": "NG",
    "Niue": "NU",
    "Norfolk Island": "NF",
    "North Korea": "KP",
    "Northern Mariana Islands": "MP",
    "Norway": "NO",
    "Oman": "OM",
    "Pakistan": "PK",
    "Palau": "PW",
    "Palestine": "PS",
    "Panama": "PA",
    "Papua New Guinea": "PG",
    "Paraguay": "PY",
    "Peru": "PE",
    "Philippines": "PH",
    "Pitcairn": "PN",
    "Poland": "PL",
    "Portugal": "PT",
    "Puerto Rico": "PR",
    "Qatar": "QA",
    "Republic of North Macedonia": "MK",
    "Romania": "RO",
    "Russian Federation": "RU",
    "Rwanda": "RW",
    "Reunion": "RE",
    "Saint Bartholemy": "BL",
    "Saint Helena, Ascension and Tristan da Cunha": "SH",
    "Saint Kitts and Nevis": "KN",
    "Saint Lucia": "LC",
    "Saint Martin": "MF",
    "Saint Pierre and Miquelon": "PM",
    "Saint Vincent and the Grenadines": "VC",
    "Samoa": "WS",
    "San Marino": "SM",
    "Sao Tome and Principe": "ST",
    "Saudi Arabia": "SA",
    "Senegal": "SN",
    "Serbia": "RS",
    "Seychelles": "SC",
    "Sierra Leone": "SL",
    "Singapore": "SG",
    "Sint Maarten": "SX",
    "Slovakia": "SK",
    "Slovenia": "SI",
    "Solomon Islands": "SB",
    "Somalia": "SO",
    "South Africa": "ZA",
    "South Georgia and the South Sandwich Islands": "GS",
    "South Korea": "KR",
    "South Sudan": "SS",
    "Spain": "ES",
    "Sri Lanka": "LK",
    "Sudan": "SD",
    "Suriname": "SR",
    "Svalbard and Jan Mayen": "SJ",
    "Sweden": "SE",
    "Switzerland": "CH",
    "Syria": "SY",
    "Taiwan": "TW",
    "Tajikistan": "TJ",
    "Tanzania": "TZ",
    "Thailand": "TH",
    "Timor-Leste": "TL",
    "Togo": "TG",
    "Tokelau": "TK",
    "Tonga": "TO",
    "Trinidad and Tobago": "TT",
    "Tunisia": "TN",
    "Turkey": "TR",
    "Turkmenistan": "TM",
    "Turks and Caicos Islands": "TC",
    "Tuvalu": "TV",
    "Uganda": "UG",
    "Ukraine": "UA",
    "United Arab Emirates": "AE",
    "United Kingdom of Great Britain and Northern Ireland": "GB",
    "United States Minor Outlying Islands": "UM",
    "United States of America": "US",
    "Uruguay": "UY",
    "Uzbekistan": "UZ",
    "Vanuatu": "VU",
    "Venezuela": "VE",
    "Viet Nam": "VN",
    "Virgin Islands (British)": "VG",
    "Virgin Islands (U.S.)": "VI",
    "Wallis and Futuna": "WF",
    "Western Sahara": "EH",
    "Yemen": "YE",
    "Zambia": "ZM",
    "Zimbabwe": "ZW",
}
