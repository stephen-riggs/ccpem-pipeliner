#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
from glob import glob

from typing import List, Dict, Any, Optional
from pipeliner.deposition_tools.onedep_deposition import (
    DepositionData,
    merge_depdata_items,
)
from pipeliner.data_structure import TRUES
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import date_time_tag
from pipeliner.deposition_tools.empiar_deposition_objects import (
    EmpiarParticles,
    EmpiarRefinedParticles,
    EmpiarMovieSet,
    EmpiarCorrectedMics,
)
from pipeliner.starfile_handler import JobStar

EMPIAR_DEPOSITION_DIR = "EMPIAR"


def make_empiar_deposition_data_object(data_obj) -> DepositionData:
    """Makes a new DepositionData object of the desired type

    Args:
        data_obj (object): The data class for the object with the data in it

    Returns:
        DepositionData: The created DepositionData object
    """
    # movies can be multiple, everything else should just be last one
    if isinstance(data_obj, EmpiarMovieSet):
        return DepositionData(
            field_name="empiar",
            data_items=data_obj,
            merge_strat="multi",
            reverse=False,
        )

    return DepositionData(
        field_name="empiar",
        data_items=data_obj,
        merge_strat="overwrite",
        reverse=False,
    )


def get_deposition_objects_empiar(
    terminal_job: str,
    do_parts: bool = True,
    do_rparts: bool = True,
    do_movs: bool = True,
    do_mics: bool = True,
) -> List[List[DepositionData]]:
    depobjs: Dict[Any, List[DepositionData]] = {}

    if not any([do_mics, do_parts, do_rparts, do_movs]):
        raise ValueError("No data types selected for EMPIAR deposition")
    with ProjectGraph() as pipeline:
        upstream = pipeline.get_project_procs_list(terminal_job, reverse=False)
    for proc in upstream:
        job = active_job_from_proc(proc)
        dep_data = job.prepare_deposition_data(depo_type="EMPIAR")
        for dd in dep_data:
            try:
                depobj = make_empiar_deposition_data_object(dd)
                dt = type(dd)
                if dt in depobjs:
                    depobjs[dt].append(depobj)
                else:
                    depobjs[dt] = [depobj]
            except Exception as e:
                print(f"Error creating DepositionData object: {e}, {dd.__dict__}")
    final_depobjs = []
    if do_movs and depobjs.get(EmpiarMovieSet):
        final_depobjs.append(depobjs[EmpiarMovieSet])
    if do_mics and depobjs.get(EmpiarCorrectedMics):
        final_depobjs.append(depobjs[EmpiarCorrectedMics])
    if do_parts and depobjs.get(EmpiarParticles):
        final_depobjs.append(depobjs[EmpiarParticles])
    if do_rparts and depobjs.get(EmpiarRefinedParticles):
        final_depobjs.append(depobjs[EmpiarRefinedParticles])
    return [depobjs[x] for x in depobjs]


def assemble_deposition_objects_empiar(
    depobjs_raw: List[List[DepositionData]],
) -> List[DepositionData]:
    """Merge the deposition objects from empiar jobs - doesn't require any substitutions

     Args:
        depobjs_raw (List[List[DepositionData]]]): A list of lists containing the
            DepositionData objects of each type for each job, in order

    Returns:
        List[DepositionData]: The final deposition objects, merged where appropriate.
    """
    combined_depobjs = []
    for depobjs in depobjs_raw:
        # depobjs will come sorted from the previous function, reverse if necessary
        if depobjs[0].reverse:
            depobjs.reverse()

        # update job references and do the merging with the appropriate strategy
        n = 1
        merged = [depobjs[0]]
        while n < len(depobjs):
            merged = merge_depdata_items(depobjs[n], merged)
            n += 1
        combined_depobjs.extend(merged)

    return combined_depobjs


def get_citation_data(joboptions):
    cite_file = joboptions["citation_file"]
    if cite_file:
        with open(cite_file) as cf:
            return [json.load(cf)]
    else:
        auth_n = 0
        authors = []
        auth_list = joboptions["citation_authors"].split(":::")
        if auth_list == [""]:
            return []
        auth_list.reverse()
        for auth in auth_list:
            asplit = auth.split(",")
            surname = asplit[0].strip()
            initial = asplit[1].strip()
            orcid = None
            if len(asplit) == 3:
                orcid = asplit[2].strip()

            authors.append(
                {
                    "name": f"('{surname}', '{initial}')",
                    "order_id": auth_n,
                    "author_orcid": orcid,
                }
            )
            auth_n += 1
    ed_n = 0
    editors = []
    if joboptions["editors"] != "":
        eds_list = joboptions["editors"].split(":::")
        eds_list.reverse()
        for ed in eds_list:
            asplit = ed.split(",")
            surname = asplit[0].strip()
            initial = asplit[1].strip()
            editors.append(
                {
                    "name": f"('{surname}', '{initial}')",
                    "order_id": ed_n,
                }
            )
            ed_n += 1
    if not joboptions["published"] in TRUES:
        joboptions["j_or_nj_citation"] = "No"
    citation_raw = {
        "authors": authors,
        "editors": editors,
        "published": joboptions["published"].lower() in TRUES,
        "j_or_nj_citation": joboptions["j_or_nj_citation"].lower() in TRUES,
        "title": joboptions["citation_title"],
        "volume": joboptions["volume"],
        "country": joboptions["cite_country"],
        "first_page": joboptions["first_page"],
        "last_page": joboptions["last_page"],
        "year": joboptions["year"],
        "language": joboptions["language"],
        "doi": joboptions["doi"],
        "pubmedid": joboptions["pubmedid"],
        "details": joboptions["details"],
        "book_chapter_title": joboptions["book_chapter_title"],
        "publisher": joboptions["publisher"],
        "publication_location": joboptions["pub_location"],
        "journal": joboptions["journal"],
        "journal_abbreviation": joboptions["journal_abbrev"],
        "issue": joboptions["issue"],
    }
    citation = {}
    for i in citation_raw:
        citation[i] = None if citation_raw[i] == "" else citation_raw[i]

    return [citation]


def parse_empiar_jobstar(jobstar_file: str):
    joboptions = JobStar(jobstar_file).joboptions
    emd_ref = joboptions["emdb_ref"].split(":::")
    emd_ref.reverse()
    emp_ref = joboptions["empiar_ref"].split(":::")
    emp_ref.reverse()
    emd_crossrefs = [{"name": f"EMD-{x}"} for x in emd_ref] if emd_ref != [""] else []
    emp_crossrefs = (
        [{"name": f"EMPIAR-{x}" for x in emp_ref}] if emp_ref != [""] else []
    )
    crossrefs = emd_crossrefs + emp_crossrefs

    entry_authors = []
    auth_n = 0
    eaf = joboptions["upload_entry_author_file"]
    if eaf:
        with open(eaf) as ef:
            aut_list = ef.readlines()
    else:
        aut_list = joboptions["entry_authors"].split(":::")
    aut_list.reverse()
    for auth in aut_list:
        asplit = auth.split(",")
        surname = asplit[0].strip()
        initial = asplit[1].strip()
        orcid = None
        if len(asplit) == 3:
            if asplit[2] != "":
                orcid = asplit[2].strip()

        entry_authors.append(
            {
                "name": f"('{surname}', '{initial}')",
                "order_id": auth_n,
                "author_orcid": orcid,
            }
        )
        auth_n += 1
    corauth = {
        "author_orcid": joboptions["ca_orcid"],
        "middle_name": joboptions["ca_middle_name"],
        "organization": joboptions["ca_org"],
        "street": joboptions["ca_address"],
        "town_or_city": joboptions["ca_city"],
        "state_or_province": joboptions["ca_state"],
        "post_or_zip": joboptions["ca_postcode"],
        "telephone": joboptions["ca_tel"],
        "fax": None,
        "first_name": joboptions["ca_first_name"],
        "last_name": joboptions["ca_surname"],
        "email": joboptions["ca_email"],
        "country": joboptions["ca_country"],
    }
    corr_author = {}
    for i in corauth:
        corr_author[i] = None if corauth[i] == "" else corauth[i]

    pi_raw = {
        "author_orcid": joboptions["pi_orcid"],
        "middle_name": joboptions["pi_middle_name"],
        "organization": joboptions["pi_org"],
        "street": joboptions["pi_address"],
        "town_or_city": joboptions["pi_city"],
        "state_or_province": joboptions["pi_state"],
        "post_or_zip": joboptions["pi_postcode"],
        "telephone": joboptions["pi_tel"],
        "fax": None,
        "first_name": joboptions["pi_first_name"],
        "last_name": joboptions["pi_surname"],
        "email": joboptions["pi_email"],
        "country": joboptions["pi_country"],
    }
    pi = {}
    for i in pi_raw:
        pi[i] = None if pi_raw[i] == "" else pi_raw[i]
    pi_entry = [pi]

    release_options = {
        "Directly after the submission has been processed": "RE",
        "After the related EMDB entry has been released": "EP",
        "After the related primary citation has been published": "HP",
        "Delay release of entry by one year from the date of deposition": "HO",
    }

    dep_file_dict = {
        "title": joboptions["entry_title"],
        "release_date": release_options[joboptions["release_status"]],
        "experiment_type": 3,
        "cross_references": crossrefs,
        "workflows": [],
        "authors": entry_authors,
        "corresponding_author": corr_author,
        "principal_investigator": pi_entry,
        "workflow_file": None,  # may have EBI add this?
        "imagesets": [],
        "citation": get_citation_data(joboptions),
    }
    return dep_file_dict


def prepare_empiar_deposition(
    terminal_job: str,
    jobstar_file: Optional[str] = None,
    do_parts: bool = True,
    do_rparts: bool = True,
    do_movs: bool = True,
    do_mics: bool = True,
) -> str:
    """Prepare a deposition for empiar

    Returns:
        str: The name of the deposition file
    """
    dobs_raw = get_deposition_objects_empiar(
        terminal_job,
        do_parts=do_parts,
        do_rparts=do_rparts,
        do_mics=do_mics,
        do_movs=do_movs,
    )
    dobs_merged = assemble_deposition_objects_empiar(dobs_raw)
    merged_dicts = [x.data_items.__dict__ for x in dobs_merged]

    # get the files to archive
    files_patterns = set()
    for dep_dict in merged_dicts:
        # get file search strings for archiving
        mfp = dep_dict.get("micrographs_file_pattern")
        ppfp = dep_dict.get("picked_particles_file_pattern")
        for i in (mfp, ppfp):
            if i is not None:
                files_patterns.add(i)

    # make the archive dir
    dtt = "deposition_data"
    if not jobstar_file:
        dtt = date_time_tag(compact=True)

    if jobstar_file is None:
        depdir = EMPIAR_DEPOSITION_DIR

    else:
        depdir = os.path.dirname(jobstar_file)

    dep_data_dir = os.path.join(depdir, dtt)
    for new_dir in [depdir, dep_data_dir]:
        os.makedirs(new_dir, exist_ok=True)

    for fs in files_patterns:
        files = glob(fs)
        for f in files:
            linkname = os.path.join(dep_data_dir, str(f))
            fdir = os.path.dirname(linkname)
            os.makedirs(fdir, exist_ok=True)
            os.symlink(str(f), linkname)

    # update the file names and write the file
    filename = f"{dtt}_EMPIAR.json"

    for entry in merged_dicts:
        entry["directory"] = os.path.join(dtt, entry["directory"])
        if entry.get("micrographs_file_pattern") is not None:
            entry["micrographs_file_pattern"] = os.path.join(
                dtt, entry["micrographs_file_pattern"]
            )
        if entry.get("picked_particles_file_pattern") is not None:
            entry["picked_particles_file_pattern"] = os.path.join(
                dtt, entry["picked_particles_file_pattern"]
            )
        entry["details"] = entry["details"].replace(
            "data in file: ", f"data in file: {dtt}/"
        )

    # prepare the file
    final_dict = {
        "experiment_type": 3,
        "imagesets": merged_dicts,
    }

    if jobstar_file is not None:
        temp_data = parse_empiar_jobstar(jobstar_file)
        temp_data["imagesets"] = final_dict["imagesets"]
        final_dict = temp_data

    with open(os.path.join(depdir, filename), "w") as f:
        json.dump(final_dict, f, indent=4)

    return os.path.join(depdir, filename)
