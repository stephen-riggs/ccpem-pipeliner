#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from dataclasses import dataclass
from typing import Optional, Union, List, Literal
from pipeliner import __version__ as pipevers

ONEDEP_DEPOSITION_COMMENT = f"CCP-EM pipeliner {pipevers}"
ID_TYPE = Optional[Union[int, List[Optional[str]], str]]

# TODO: Many of the string fields can be updated to Literals for multiple choice


@dataclass
class OneDepData:
    id: ID_TYPE = None


@dataclass
class EmCtfCorrection(OneDepData):
    image_processing_id: ID_TYPE = None
    type: Literal["PHASE FLIPPING AND AMPLITUDE CORRECTION"] = (
        "PHASE FLIPPING AND AMPLITUDE CORRECTION"  # Add others, although not needed
    )
    details: Optional[str] = None


@dataclass
class EmImageProcessing(OneDepData):
    image_recording_id: ID_TYPE = None
    details: Optional[str] = None


@dataclass
class EmImaging(OneDepData):
    entry_id: str = "ENTRY_ID"
    accelerating_voltage: Optional[int] = None
    illumination_mode: Optional[str] = None
    electron_source: Optional[str] = None
    microscope_model: Optional[str] = None
    imaging_mode: Optional[str] = None
    sample_support_id: ID_TYPE = None
    specimen_holder_type: Optional[str] = None
    specimen_holder_model: Optional[str] = None
    details: Optional[str] = None
    date: Optional[str] = None
    mode: Optional[str] = None
    nominal_cs: Optional[float] = None
    nominal_defocus_min: Optional[float] = None
    nominal_defocus_max: Optional[float] = None
    tilt_angle_min: Optional[float] = None
    tilt_angle_max: Optional[float] = None
    nominal_magnification: Optional[float] = None
    calibrated_magnification: Optional[float] = None
    energy_filter: Optional[str] = None
    energy_window: Optional[str] = None
    temperature: Optional[float] = None
    detector_distance: Optional[float] = None
    recording_temperature_minimum: Optional[float] = None
    recording_temperature_maximum: Optional[float] = None


@dataclass
class EmSoftware(OneDepData):
    category: Optional[
        Literal[
            "CLASSIFICATION",
            "CTF CORRECTION",
            "DIFFRACTION INDEXING",
            "FINAL EULER ASSIGNMENT",
            "IMAGE ACQUISITION",
            "INITIAL EULER ASSIGNMENT",
            "LAYERLINE INDEXING",
            "MASKING",
            "MODEL FITTING",
            "MODEL REFINEMENT",
            "OTHER",
            "PARTICLE SELECTION",
            "RECONSTRUCTION",
        ]
    ] = None
    details: Optional[str] = None
    name: Optional[str] = None
    version: Optional[str] = None
    image_processing_id: ID_TYPE = None
    fitting_id: ID_TYPE = None
    imaging_id: ID_TYPE = None


@dataclass
class EmSampleSupport(OneDepData):
    entry_id: str = "ENTRY_ID"
    film_material: Optional[str] = None
    grid_type: Optional[str] = None
    grid_material: Optional[str] = None
    grid_mesh_size: Optional[str] = None
    details: Optional[str] = None


@dataclass
class EmSpecimen(OneDepData):
    experiment_id: int = 1
    concentration: Optional[str] = None
    details: Optional[str] = None
    embedding_applied: bool = False
    shadowing_applied: bool = False
    staining_applied: bool = False
    vitrification_applied: bool = True


@dataclass
class EmImageRecording(OneDepData):
    imaging_id: ID_TYPE = None
    avg_electron_dose_per_image: Optional[float] = None
    average_exposure_time: Optional[float] = None
    details: Optional[str] = None
    detector_mode: Optional[str] = None
    film_or_detector_model: Optional[str] = None
    num_diffraction_images: Optional[int] = None
    num_grids_imaged: Optional[int] = None
    num_real_images: Optional[int] = None


@dataclass
class Em2dProjectionSelection(OneDepData):
    entry_id: str = "ENTRY_ID"
    details: Optional[str] = None
    method: Optional[str] = None
    num_particles: Optional[int] = None
    software_name: Optional[str] = None
    citation_id: ID_TYPE = None


@dataclass
class Em3dReconstruction(OneDepData):
    entry_id: str = "ENTRY_ID"
    num_particles: Optional[int] = None
    symmetry_type: Optional[str] = None
    image_processing_id: ID_TYPE = None
    actual_pixel_size: Optional[float] = None
    algorithm: Optional[str] = None
    citation_id: ID_TYPE = None
    ctf_correction_method: Optional[str] = None
    details: Optional[str] = None
    euler_angles_details: Optional[str] = None
    fsc_type: Optional[str] = None
    magnification_calibration: Optional[str] = None
    method: Optional[str] = None
    nominal_pixel_size: Optional[float] = None
    num_class_averages: Optional[int] = None
    refinement_type: Optional[str] = None
    resolution: Optional[float] = None
    resolution_method: Optional[str] = None
    software: Optional[str] = None


# dict of the ondep dataclasses:
# {object: (field_name, merge strategy, reverse?)}
# How to decide the merging strategy:
#  - Most things should be multi, unless only a single entry is desired
#  - If the data for a single entry comes from only one job use overwrite
#    and the appropriate reverse to use the first or last job of that type
#  - If the data will come from multiple jobs use combine
#  - Combine and overwrite are mostly used by EMPIAR not OneDep

ONEDEP_OBJECTS = {
    EmCtfCorrection: ("em_ctf_correction", "multi", False),
    EmImaging: ("em_imaging", "multi", False),
    EmImageProcessing: ("em_image_processing", "multi", False),
    EmImageRecording: ("em_image_recording", "multi", False),
    EmSoftware: ("em_software", "multi", False),
    EmSampleSupport: ("em_sample_support", "multi", False),
    EmSpecimen: ("em_specimen", "multi", False),
    Em2dProjectionSelection: ("em_2d_projection_selection", "multi", False),
    Em3dReconstruction: ("em_3d_reconstruction", "multi", False),
}

# dict for reverse lookup of deposition data classes
ONEDEP_CLASSES = {}
for ddc in ONEDEP_OBJECTS:
    ONEDEP_CLASSES[ONEDEP_OBJECTS[ddc][0]] = ddc
