#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from pathlib import Path
import platform
import json
from typing import Union, Tuple
import math
import mrcfile
from pipeliner.utils import get_file_size_mb


def get_settings_path() -> Path:
    """Get doppio config file from platform-specific standard location"""
    user_platform = platform.system()
    # Linux
    if user_platform == "Linux":
        config_path = Path(os.getenv("XDG_CONFIG_HOME", Path("~/.config").expanduser()))
    # Mac
    elif user_platform == "Darwin":
        config_path = Path("~/Library/Application Support/").expanduser()
    else:
        raise RuntimeError(f'Platform "{user_platform}" is not supported.')

    doppio_settings_path = config_path / "ccpem/doppio/doppio-config/app-settings.json"
    return doppio_settings_path


def get_doppio_filesize_limit() -> float:
    try:
        with open(get_settings_path(), "r") as sf:
            settings = json.load(sf)
        return float(settings["maxResultSizeMB"])
    except FileNotFoundError:
        return 0.0


def set_doppio_map_bin(
    mapfile: Union[str, Path],
    maxsize: float,
) -> Tuple[float, float]:
    """Set the size of a map based on doppio's maximum filesize

    If the map is too big it is binned by 20% until it is an acceptable size
    If the map is still to big at 80% bin then it is just returned as normal.

    Args:
        mapfile (Union[str, Path]): the file to operate on
        maxsize (float): Maximum size of map in MB

    Returns:
        Tuple: (float: new boxsize assumed cubic, float: new pixel size in angstroms)

    """
    map_file_size = get_file_size_mb(mapfile)
    omfs = map_file_size
    with mrcfile.open(mapfile, header_only=True) as mf:
        opx = mf.voxel_size.tolist()[0]

    sizes = (1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1)
    size_n = 0
    if maxsize <= 0.0:
        return 1.0, 0
    newpx = 0
    while map_file_size > maxsize:
        size_n += 1
        map_file_size = math.ceil(omfs * sizes[size_n] ** 3)
        newpx = round(opx * sizes[size_n] ** -1, 3)
        if size_n == 9:
            return 1.0, 0

    return sizes[size_n], newpx
