#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

"""The job factory functions identify the available job types and return
the correct type of job from the job type specified in a parameter file"""

import os
from importlib_metadata import entry_points
from typing import Optional, List, Union, Mapping

from pipeliner.process import Process
from pipeliner.data_structure import (
    TRUES,
    PROC_NUM_2_GENERAL_NAME,
    JOBNAMES_NEED_CONVERSION,
)

from pipeliner.job_options import FloatJobOption, IntJobOption
from pipeliner.starfile_handler import JobStar
from pipeliner.relion_compatibility import convert_proctype, get_job_type
from pipeliner.pipeliner_job import PipelinerJob


def get_job_types(search_term: str = "") -> List[PipelinerJob]:
    """Returns all the job types and info about them

    Note that the returned list actually contains job instances, not classes.

    Args:
        search_term (str): Only return jobs with this string in
            their job type name

    Returns:
        list: A list of the :class:`~pipeliner.pipeliner_job.PipelinerJob`-based job
            objects for each job found
    """
    jobs = [
        ep.load()()
        for ep in entry_points(group="ccpem_pipeliner.jobs")
        if search_term.lower() in ep.name
    ]
    return jobs


def read_job(filename: str) -> PipelinerJob:
    """
    Reads run.job and job.star files and returns the correct Pipeliner job class

    Args:
        filename (str): The run.job or job.star file

    Returns:
        :class:`~pipeliner.pipeliner_job.PipelinerJob`: The job subclass

    Raises:
        ValueError: If the file name entered does not end with .job or .star
        RuntimeError: If the input file is in the RELION 3.1 format and conversion fails
        RuntimeError: If the job type specified in the file cannot be found
    """

    if filename.endswith(".job"):
        with open(filename) as job_file:
            job_type_raw = job_file.readline().split("==")[1].strip(" \n")
            # for back compatibility with 3.1 - convert integer job types
            job_type: str = ""
            try:
                job_type = PROC_NUM_2_GENERAL_NAME[int(job_type)].lower()
            except ValueError:
                job_type = job_type_raw.lower()
            # get the job options in case they are needed for conversion
            job_options = {}
            for line in job_file.readlines():
                if "==" in line:
                    ls = line.split("==")
                    job_options[ls[0].strip()] = ls[1].strip()

    elif filename.endswith(".star"):
        job_options = JobStar(filename).all_options_as_dict()
        job_type = get_job_type(job_options)[0].lower()

    else:
        raise ValueError(f"ERROR: The file {filename} is not a valid Relion job file")

    # Check if the job name is in the lis of jobnames that are ambiguous
    # between RELION4 and the pipeliner. Convert the name to the pipeliner
    # standard if necessary

    if job_type in JOBNAMES_NEED_CONVERSION:
        jt_old = job_type
        job_type, success = convert_proctype(job_type, job_options)
        if not success:
            raise RuntimeError(
                f"ERROR: Problem converting job type {jt_old} to the pipeliner"
                f" format:\n{job_type}"
            )
    # get the job object for the specific job type
    try:
        job = new_job_of_type(job_type, job_options)

    except RuntimeError:
        raise RuntimeError(
            f"Cannot find job type {job_type}, used in job file {filename}"
        )
    job.read(filename)

    return job


def get_job_from_entrypoint(type_name: str) -> Optional[PipelinerJob]:
    eps = list(entry_points(group="ccpem_pipeliner.jobs", name=type_name))
    if len(eps) == 0:
        return None
    elif len(eps) == 1:
        ep_class = eps[0].load()
        return ep_class()
    else:
        raise RuntimeError(f"Found {len(eps)} job types with name '{type_name}'")


def new_job_of_type(
    type_name: str,
    joboptions: Optional[Mapping[str, Union[str, int, float, bool]]] = None,
) -> PipelinerJob:
    """Creates a new object of the correct PipelinerJob sub-type

    Args:
        type_name (str): The job process name
        joboptions (dict): Dict of the job's joboptions, only necessary if
            converting from a RELION4.0 style jobname

    Returns:
        :class:`~pipeliner.pipeliner_job.PipelinerJob`: The job subclass

    Raises:
        ValueError: If the job type is not found
    """
    job = get_job_from_entrypoint(type_name)
    if job is not None:
        return job
    # if not found check if it's a relion 4 job
    if joboptions is not None:
        conv_type, converted = convert_proctype(type_name, joboptions)
        if not converted:
            raise ValueError(f"Job type '{type_name}' not found; conversion failed")
        job = get_job_from_entrypoint(conv_type)
        if job is None:
            raise ValueError(f"Job type '{type_name}' not found")
        return job

    else:
        raise ValueError(f"Job type '{type_name}' not found")


def job_from_dict(
    job_input: Mapping[str, Union[str, int, float, bool]]
) -> PipelinerJob:
    """Create a job from a dictionary

    The dict must define the job type with a '_rlnJobTypeLabel'
    key.  Any other keys will override the default options for
    that parameter

    Args:
        job_input (dict): The dict containing the params. At minimum, it must
            contain `{'_rlnJobTypeLabel': <jobtype>}`

    Returns:
        :class:`~pipeliner.pipeliner_job.PipelinerJob`: The job subclass

    Raises:
        ValueError: If the '_rlnJobTypeLabel' key is missing from the dict or is not
            a string
        ValueError: If '_rlnJobIsContinue' is in the dict - this function is not for
            creating continuations of jobs
        ValueError: If the specified jobtype is not found
        ValueError: If any of the parameters in the dict are not in the jobtype
            returned
    """
    j_type = job_input.get("_rlnJobTypeLabel")
    if j_type is None:
        raise ValueError(
            "Cannot create a job from this dict; key '_rlnJobTypeLabel' is missing"
        )
    if not isinstance(j_type, str):
        raise ValueError(
            "Cannot create a job from this dict; key '_rlnJobTypeLabel' is not a string"
        )

    try:
        job = new_job_of_type(j_type)
    except RuntimeError:
        raise ValueError(f"Cannot find job type {j_type} to create a new job")

    # set the parameters
    option_check = new_job_of_type(j_type)
    option_check.add_compatibility_joboptions()
    for param in job_input:
        if param not in ["_rlnJobTypeLabel", "_rlnJobIsContinue", "_rlnJobIsTomo"]:
            try:
                job.joboptions[param].value = job_input[param]
            except KeyError:
                if param not in option_check.joboptions:
                    raise ValueError(
                        f"Parameter {param} is not found in jobtype {j_type}"
                    )

    # set continuation
    # stringify to deal with possible mix of str and bool values
    # todo: fix that
    is_continue = job_input.get("_rlnJobIsContinue")
    if is_continue is not None:
        if str(is_continue).lower() in TRUES:
            job.is_continue = True

    is_tomo = job_input.get("_rlnJobIsTomo")
    if is_tomo is not None:
        if str(is_tomo).lower() in TRUES:
            job.is_tomo = True

    return job


def active_job_from_proc(the_proc: Process) -> PipelinerJob:
    """Create an active job from an existing process

    Used when the functions inside a job subclass need to be called on an existing
    job

    Args:
        the_proc (:class:`~pipeliner.process.Process`): The
            process to create a job from

    Returns:
        :class:`~pipeliner.pipeliner_job.PipelinerJob`: The job subclass
            object for the process

    """
    jobstar = os.path.join(the_proc.name, "job.star")
    if os.path.isfile(jobstar):
        jobops = JobStar(jobstar).all_options_as_dict()
    else:
        raise ValueError(f"job.star file not found for job {the_proc.name}")

    # check if the jobs is a relion 4 type necessary if the relion4 name is the same as
    # a pipeliner name EX: relion.polish --> relion.polish and relion.polish.train
    conv_type, converted = convert_proctype(the_proc.type, jobops)
    if converted:
        the_proc.type = conv_type

    # create the new job
    the_job = new_job_of_type(the_proc.type, jobops)
    if jobops is not None:
        for jo in jobops:
            # try/except to ignore nonexistent joboptions
            try:
                filljo = the_job.joboptions[jo]
                if isinstance(filljo, FloatJobOption):
                    the_job.joboptions[jo].value = float(jobops[jo])
                elif isinstance(filljo, IntJobOption):
                    # float the number first to deal with strings with decimal points
                    the_job.joboptions[jo].value = int(float(jobops[jo]))
                else:
                    the_job.joboptions[jo].value = jobops[jo]
            except KeyError:
                pass
    the_job.output_dir = the_proc.name
    the_job.output_nodes = the_proc.output_nodes
    the_job.input_nodes = the_proc.input_nodes
    return the_job


def job_can_run(job_type: str) -> bool:
    """Check that the programs are available to run a specific job type

    Args:
        job_type (str): The job type IE relion.class3d.helical

    Returns:
        bool: Are the programs needed to run that job available on this system
    """

    job = new_job_of_type(job_type)
    return job.jobinfo.is_available
