#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#


"""
==========
JobOptions
==========

JobOptions are used to store parameters for jobs.  They contain all the info
necessary for on-the-fly GUI generation.
"""
from __future__ import annotations

import re
import shutil
import operator
from glob import glob
from typing import Any, List, Optional, Union, Literal, Tuple, Dict
from pathlib import Path

from pipeliner.data_structure import (
    NODE_MOVIES_LABEL,
    NODE_MICS_LABEL,
    NODE_MIC_COORDS_LABEL,
    NODE_PART_DATA_LABEL,
    NODE_REFS_LABEL,
    NODE_3DREF_LABEL,
    NODE_MASK_LABEL,
    NODE_HALFMAP_LABEL,
    NODE_OTHER_LABEL,
    TRUES,
    FALSES,
)
from pipeliner.node_factory import create_node
from pipeliner.nodes import Node
from pipeliner.utils import file_in_project, increment_file_basenames

# RELION 'radio' (actually pull down menu) standard options
SAMPLING = [
    "30 degrees",
    "15 degrees",
    "7.5 degrees",
    "3.7 degrees",
    "1.8 degrees",
    "0.9 degrees",
    "0.5 degrees",
    "0.2 degrees",
    "0.1 degrees",
]

# extensions for different filetypes
# for use with files_extensions() function
EXT_MICROGRAPH_MOVIES = [".mrcs", ".tiff"]
EXT_TOMOGRAMS = [".mrc"]
EXT_STARFILE = [".star"]
EXT_COORDS = [".box", ".star"]
EXT_2DREFS = [".star", ".mrcs"]
EXT_RELION_HALFMAP = ["_unfil.mrc"]
EXT_MRC_MAP = [".mrc"]
EXT_RELION_OPT = ["_optimiser.star"]
EXT_RELION_MODEL = ["_model.star"]


def files_exts(
    name: str = "File", exts: Optional[List[str]] = None, exact: bool = False
) -> str:
    """Produce a description of files and their extensions

    In a format compatible with PyQt5 QFileDialog

    Args:
        name (str): The type of file IE: 'Micrograph movies'
        exts (list): The acceptable extensions or file search strings, e.g. '.txt' to
            find ``*.txt`` or '_half1.star' for ``*_half1.star``
        exact (bool): Match the file name(s) exactly, do not prepend a ``*``

    Returns:
        str: Formatted for PyQt5 QFileDialog: 'name (``*ext1 *ext2 *ext3``)'
    """
    if exts is None:
        flag = "(*.*)"
    elif not exact:
        flag = f"({' '.join(['*'+x for x in exts])})"
    else:
        flag = f"({' '.join([x for x in exts])})"

    return f"{name} {flag}"


NODETYPE = [
    "2D micrograph movies (*.mrcs, *.tiff)",
    "2D micrographs/tomograms (*.mrc)",
    "2D/3D particle coordinates (*.box, *_pick.star)",
    "Particles STAR file (.star)",
    # "Movie-particles STAR file (.star)",   NOT USED ANY MORE
    "2D references (.star or .mrcs)",
    "Micrographs STAR file (.star)",
    "3D reference (.mrc)",
    "3D mask (.mrc)",
    "Unfiltered half-map (unfil.mrc)",
    "Other",
]

NODETYPE_OPTIONS = {
    "2D micrograph movies (*.mrcs, *.tiff)": NODE_MOVIES_LABEL,
    "2D micrographs/tomograms (*.mrc)": NODE_MICS_LABEL,
    "2D/3D particle coordinates (*.box, *_pick.star)": NODE_MIC_COORDS_LABEL,
    "Particles STAR file (.star)": NODE_PART_DATA_LABEL,
    # "Movie-particles STAR file (.star)": NOT USED ANY MORE,
    "2D references (.star or .mrcs)": NODE_REFS_LABEL,
    "Micrographs STAR file (.star)": NODE_MICS_LABEL,
    "3D reference (.mrc)": NODE_3DREF_LABEL,
    "3D mask (.mrc)": NODE_MASK_LABEL,
    "Unfiltered half-map (unfil.mrc)": NODE_HALFMAP_LABEL,
    "Other": NODE_OTHER_LABEL,
}

GAIN_ROTATION = [
    "No rotation (0)",
    "90 degrees (1)",
    "180 degrees (2)",
    "270 degrees (3)",
]

CTF_FIT = ["No", "Per-micrograph", "Per-particle"]

GAIN_FLIP = ["No flipping (0)", "Flip upside down (1)", "Flip left to right (2)"]


class JobOptionCondition(object):
    """Definines conditions for JobOption based on other JobOptions

    Used to determine if a JobOption should be required or deactivated
    """

    def __init__(
        self,
        conditions: List[Tuple[str, str, Union[str, bool, int, float]]],
        operation: Literal["ANY", "ALL"] = "ANY",
    ) -> None:
        self.conditions = conditions
        self.operation = operation

    def check_condition_is_met(self, jobops: Dict[str, JobOption]) -> Tuple[
        bool,
        List[Tuple[str, str, Union[str, bool, int, float]]],
        List[Tuple[str, str, Union[str, bool, int, float]]],
    ]:
        """Has the condition been met?

        Args:
            jobops (dict): The JobOptions dict for the job

        Returns:
            Tuple: (bool: Has the condition been met?, List[conditions that passed],
                List[conditions that failed]
        """
        ops = {
            "=": operator.eq,
            "!=": operator.ne,
            ">": operator.gt,
            ">=": operator.ge,
            "<": operator.lt,
            "<=": operator.le,
        }
        condition_fails, condition_passes = [], []
        for con in self.conditions:
            parent, op, exp_value = con
            parent_jo = jobops[parent]
            compare_val: Union[bool, int, float, str]
            if isinstance(parent_jo, BooleanJobOption):
                compare_val = parent_jo.get_boolean()
            elif isinstance(parent_jo, (FloatJobOption, IntJobOption)):
                compare_val = parent_jo.get_number()
            else:
                compare_val = parent_jo.get_string()

            if not ops[op](compare_val, exp_value):
                condition_fails.append(con)
            else:
                condition_passes.append(con)

        if self.operation == "ANY" and condition_passes:
            return True, condition_passes, condition_fails
        elif self.operation == "ALL" and len(condition_passes) == len(self.conditions):
            return True, condition_passes, condition_fails
        else:
            return False, condition_passes, condition_fails


class JobOption(object):
    """A JobOption stores a parameter, its value, and info about the GUI for that param

    This is a general class with several more specialised class subclasses.

    """

    joboption_type = "ERROR: no job option type set"

    def __init__(
        self,
        *,
        label: str,
        default_value: Any = "",
        help_text: str = "",
        in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        only_in_continue: bool = False,
        jobop_group: str = "Main",
    ) -> None:
        """Create a JobOption object

        Args:
            label (str): A verbose label for the parameter.  This is what appears in a
                run.job file
            default_value (None) = The default value for the parameter.  The type
                will be set by the specific class method
            help_text (str): Text that will be displayed in the GUI if help is clicked
            in_continue (bool): If this parameter can be modified in a job that is
                continued
            is_required (bool): Is this joboption always required?
            required_if (JobOptionCondition): Validation option: a list of logical
                statements, if all are true the joboption will be required by the GUI.
                Requires a JobOptionCondition object  Valid operators are: '=', '>',
                '!=', '>=', '<', and '<='
            deactivate_if (list): a list of logical statements, if all are true the
                joboption will be deactivated by the GUI. Requires a JobOptionCOndition:
                object. Valid operators are: '=', '>', '>=', '<', '!='
                and '<='
            only_in_continue (bool): The joboption should only be available if the job
                is being continued

        Raises:
            NotImplementedError: If this class is instantiated directly
        """
        if type(self) is JobOption:
            raise NotImplementedError(
                "Do not create JobOption objects directly - use a sub-class"
            )
        self.label = label
        self.value = default_value
        self.default_value = default_value
        self.help_text = help_text
        self.in_continue = in_continue
        self.is_required = is_required
        self.required_if = required_if
        self.deactivate_if = deactivate_if
        self.only_in_continue = only_in_continue
        self.jobop_group = jobop_group

    def check_required_not_empty(self) -> None:
        """Check if the job option is required but has no value"""

        if self.is_required and self.value in ["", None]:
            raise ValueError(f"JobOption {self.label} is required but has no value")

    def get_string(self) -> str:
        """Get the value of a JobOption as a string

        Returns:
            str: The value

        Raises:
            ValueError: If the value is required and missing
        """
        self.check_required_not_empty()
        return str(self.value)

    def set_string(self, set_to: str) -> None:
        """Set the value of a JobOption to a string

        Args:
            set_to (str): The string to set the value to
        """
        self.value = str(set_to)
        return None

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of input parameters

        will be set by individual JobOption subtypes

        """
        results = []
        try:
            self.check_required_not_empty()
        except ValueError:
            results.append(
                JobOptionValidationResult("error", [self], "This option is required")
            )
        return results

    def check_file(self) -> List[JobOptionValidationResult]:
        """Check that specified input files exist

        Returns:
            list: A JobOptionValidationResult for any errors
        """
        in_path = shutil.which(self.value)
        is_file = Path(self.value).is_file()

        if not any([in_path, is_file]):
            return [
                JobOptionValidationResult(
                    "error", [self], f"File {self.value} not found"
                )
            ]
        return []

    def check_dir(self) -> List[JobOptionValidationResult]:
        """Check that the value is an existing directory

        Returns:
            list: A JobOptionValidationResult for any errors
        """
        raise NotImplementedError(
            "Only DirPathJobOptions can be used for specifying directory paths"
        )

    def get_basename_mapping(self) -> Dict[str, str]:
        """Get a mapping of names from input files to unique basenames.

        The purpose of this method is to get unique basenames (i.e. the file name alone
        without any directory part) for all input files, by adding a prefix if necessary
        to make sure files that would otherwise have identical basenames are all
        assigned a unique basename to use for output files.

        Returns:
            A dict with the full original input file names as keys and the unique
            basenames as values, for example:

            .. code-block:: python

              {
                "Import/job001/myfile.txt": "job001_myfile.txt",
                "Import/job002/myfile.txt": "job002_myfile.txt",
              }
        """
        raise NotImplementedError("Not a file-related JobOption")

    def get_input_nodes(self) -> List[Node]:
        """Get the input node(s) related to this job option.

        Most job option types do not create input nodes and so they should rely on this
        default implementation that returns an empty list.

        Options that do create nodes should return a list of the corresponding Node
        objects.
        """

        return []

    def get_boolean(self) -> bool:
        raise NotImplementedError("Called get_boolean() on a non-boolean JobOption")

    def get_number(self) -> Union[int, float]:
        raise NotImplementedError("Called get_number() on a non-numeric JobOption")

    def get_list(self) -> List[str]:
        raise NotImplementedError("Called get_list() on a non-list JobOption")


class StringJobOption(JobOption):
    """Create a job option object for a string parameter.

    Args:
        label (str): A verbose label for the parameter.
        default_value (None) = The default value for the parameter.
        help_text (str): Text that will be displayed in the GUI if help is clicked.
        in_continue (bool): If this parameter can be modified in a job that is
            continued
    """

    joboption_type = "STRING"

    def __init__(
        self,
        *,
        label: str,
        default_value: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        validation_regex: Optional[str] = None,
        regex_error_message: Optional[str] = None,
        jobop_group: str = "Main",
    ) -> None:
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.validation_regex = validation_regex
        self.regex_error_message = regex_error_message

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters"""
        results = super().validate()
        if results:
            return results
        if self.validation_regex is not None and self.value not in ["", None]:
            reg = re.compile(self.validation_regex)
            regerror = (
                self.regex_error_message
                if self.regex_error_message is not None
                else f"Value format must match pattern '{self.validation_regex}'"
            )
            if not reg.match(self.value):
                results.append(JobOptionValidationResult("error", [self], regerror))
        return results


class DirPathJobOption(StringJobOption):
    """A JobOption for directory paths

    Args:
        label (str): A verbose label for the parameter.
        default_value (None) = The default value for the parameter.
        help_text (str): Text that will be displayed in the GUI if help is clicked.
        in_continue (bool): If this parameter can be modified in a job that is
           continued
        must_be_in_project (bool): Does the directory have to be in the project
        must_exist (bool): Does this directory have to exist when the job option is
            validated? Usually true, unless it will be created by a previous command
            in the job
    """

    joboption_type = "DIRPATH"

    def __init__(
        self,
        *,
        label: str,
        default_value: str = "",
        help_text: str = "",
        must_be_in_project: bool = False,
        must_exist: bool = True,
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        validation_regex: Optional[str] = None,
        regex_error_message: Optional[str] = None,
        jobop_group: str = "Main",
    ) -> None:
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.validation_regex = validation_regex
        self.regex_error_message = regex_error_message
        self.must_be_in_project = must_be_in_project
        self.must_exist = must_exist

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters"""
        results = super().validate()
        if results:
            return results
        if self.validation_regex is not None:
            reg = re.compile(self.validation_regex)
            regerror = (
                self.regex_error_message
                if self.regex_error_message is not None
                else f"Directory format must match pattern '{self.validation_regex}'"
            )
            if not reg.match(self.value):
                results.append(JobOptionValidationResult("error", [self], regerror))

        if self.must_be_in_project:
            if not file_in_project(self.value):
                results.append(
                    JobOptionValidationResult(
                        "error",
                        [self],
                        "This field requires a path relative to the project and all "
                        "files must be in the project.  IE: Path cannot start with "
                        "'/' or '../'",
                    )
                )

        dpath = Path(self.value)
        if dpath.is_file():
            results.append(
                JobOptionValidationResult(
                    "error", [self], "Path is a file; must be a directory"
                )
            )
        else:
            results.extend(self.check_dir())
        return results

    def check_dir(self) -> List[JobOptionValidationResult]:
        """Check that the specified dir exists.

        Returns:
            List[JobOptionValidationResult]: An error if the dir does not exist, or
            a warning if the dir does not exist and ``must_exist`` is ``False``
        """
        result: List[JobOptionValidationResult] = []
        if not Path(self.value).is_dir():
            jrt: Literal["error", "warning"] = "error" if self.must_exist else "warning"
            result.append(
                JobOptionValidationResult(jrt, [self], "Directory does not exist")
            )
        return result


class SearchStringJobOption(StringJobOption):
    """A JobOption for search strings

    Args:
       label (str): A verbose label for the parameter.
       default_value (None) = The default value for the parameter.
       help_text (str): Text that will be displayed in the GUI if help is clicked.
       in_continue (bool): If this parameter can be modified in a job that is
           continued
    """

    joboption_type = "SEARCHSTRING"

    def __init__(
        self,
        *,
        label: str,
        default_value: str = "",
        help_text: str = "",
        must_be_in_project: bool = False,
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        validation_regex: Optional[str] = None,
        regex_error_message: Optional[str] = None,
        jobop_group: str = "Main",
    ) -> None:
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.validation_regex = validation_regex
        self.regex_error_message = regex_error_message
        self.must_be_in_project = must_be_in_project

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters"""
        results = super().validate()
        if results:
            return results
        if self.validation_regex is not None:
            reg = re.compile(self.validation_regex)
            regerror = (
                self.regex_error_message
                if self.regex_error_message is not None
                else f"Value format must match pattern '{self.validation_regex}'"
            )
            if not reg.match(self.value):
                results.append(JobOptionValidationResult("error", [self], regerror))

        if self.must_be_in_project:
            if not file_in_project(self.value):
                results.append(
                    JobOptionValidationResult(
                        "error",
                        [self],
                        "This field requires a path relative to the project and all "
                        "files must be in the project, i.e. the path cannot start with "
                        "'/', '~' or '..'",
                    )
                )
        if self.value:
            found = glob(self.value)
            if found:
                results.append(
                    JobOptionValidationResult(
                        "info", [self], f"{len(found)} files found"
                    )
                )
            else:
                results.append(
                    JobOptionValidationResult("warning", [self], "No files found")
                )
        # Do not add a validation result if self.value is not set. If this option is
        # required, the superclass check_required_not_empty() will have already returned
        # a validation error, and if not required, there's no need to show anything.

        return results

    def check_file(self) -> List[JobOptionValidationResult]:
        """Check that at least one input file exists

        Returns:
            List[JobOptionValidationResult]: Error if no files found
        """
        result: List[JobOptionValidationResult] = []
        if not glob(self.value):
            result.append(
                JobOptionValidationResult(
                    "error",
                    [self],
                    f"No files found with search string {self.get_string()}",
                )
            )
        return result

    def get_list(self):
        return glob(self.value)


class MultiStringJobOption(JobOption):
    """Create a job option object for a multiple strings. For things like keyword lists

    The value for this should be a string containing values separated by ::: which
    will be converted into a list by: `[x.strip() for x in the_str.split(":::")]`

    The only reason there can't just be a self.get_list() function in a regular
    StringJobOption is the GUI must handle them differently.

    For regex validation all values in the list of inputs must match the regex

    Args:
        label (str): A verbose label for the parameter.
        default_value (None) = The default value for the parameter.
        help_text (str): Text that will be displayed in the GUI if help is clicked.
        in_continue (bool): If this parameter can be modified in a job that is
            continued
    """

    joboption_type = "MULTISTRING"

    def __init__(
        self,
        *,
        label: str,
        default_value: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        validation_regex: Optional[str] = None,
        regex_error_message: Optional[str] = None,
        jobop_group: str = "Main",
    ) -> None:
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.validation_regex = validation_regex
        self.regex_error_message = regex_error_message

    def get_list(self) -> List[str]:
        """Return the items in self.value as a list of strings

        Returns:
            List[str]: The items in the JobOptions separated string
                as a list, with any white space at the beginning and/or end
                removed
        """
        return [x.strip() for x in self.value.split(":::") if len(x.strip()) > 0]

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters"""
        results = super().validate()
        if results:
            return results

        # validate the individual items in the string list
        if self.validation_regex is not None and self.value not in ["", None]:
            reg = re.compile(self.validation_regex)
            for sval in self.get_list():
                regerror = (
                    self.regex_error_message
                    if self.regex_error_message is not None
                    else f"Value format must match pattern '{self.validation_regex}'"
                )
                if not reg.match(sval):
                    results.append(JobOptionValidationResult("error", [self], regerror))
        return results


class ExternalFileJobOption(JobOption):
    """Define a job option as a file name

    GUI will open a file browser for the user to find an input

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        default_value (str) = This should almost always by "", as the default value
            will usually be set from the string and pattern.
        pattern (str): Info about the search string for file types. It should be
            in the format <description> (<example>) e.g.: `"MRC files
            (*.mrc)"`
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        suggestion (str): A hint that will appear in the data entry field if no
            no default value is set, it will not be used. IE: 'Enter file path here'
            If you want the suggestion used as the default then just put it in
            default_value
    """

    joboption_type = "FILENAME"

    def __init__(
        self,
        *,
        label: str,
        pattern: str = "",
        default_value: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        validation_regex: Optional[str] = None,
        regex_error_message: Optional[str] = None,
        suggestion: str = "",
        jobop_group: str = "Main",
    ) -> None:
        if suggestion != "":
            suggest = suggestion
        else:
            suggest = pattern

        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.pattern = pattern
        self.validation_regex = validation_regex
        self.regex_error_message = regex_error_message
        self.suggestion = suggest

    def get_basename_mapping(self) -> Dict[str, str]:
        return increment_file_basenames([self.value])

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            List[:class:`~pipeliner.job_options.JobOptionValidationResult`]: The
                validation error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.value not in ["", None]:
            if self.validation_regex is not None:
                reg = re.compile(self.validation_regex)
                regerror = (
                    self.regex_error_message
                    if self.regex_error_message is not None
                    else f"Value format must match pattern '{self.validation_regex}'"
                )
                if not reg.match(self.value):
                    results.append(JobOptionValidationResult("error", [self], regerror))
                return results
            if Path(self.value).exists():
                if not Path(self.value).is_file():
                    results = [
                        JobOptionValidationResult(
                            "error", [self], "Target is not a file"
                        )
                    ]
            else:
                results = [
                    JobOptionValidationResult("warning", [self], "File not found")
                ]
        return results

    def check_file(self) -> List[JobOptionValidationResult]:
        """Check that specified input files exist, also checks in path

        Returns:
            str: An error message if the file does not exist
        """
        in_path = shutil.which(self.value)
        is_file = Path(self.value).is_file()

        if not any([in_path, is_file]):
            return [
                JobOptionValidationResult(
                    "error", [self], f"File {self.value} not found"
                )
            ]
        return []


class MultiExternalFileJobOption(JobOption):
    """Define a job option as multiple file names

    GUI will open a file browser for the user to find an input for each one.
    The value for this should be a string of values separated by ::: which will be
    converted into a list by: `[x.strip() for x in the_str.split(":::")]`

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        default_value (str) = This should almost always by "", as the default value
            will usually be set from the string and pattern
        pattern (str): Info about the search string for file types. It should be
            in the format <description> (<example>) e.g.: `"MRC files
            (*.mrc)"`
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        suggestion (str): A hint that will appear in the data entry field if no
            no default value is set, it will not be used. IE: 'Enter file path here'
            If you want the suggestion used as the default then just put it in
            default_value
        allow_upload (bool): Should a file upload be allowed for this input?  If yes
            a copy of the file will end up in the project.  This should be turned off
            for external programs and files that are expected to be very large like
            ML models.
    """

    joboption_type = "MULTIFILENAME"

    def __init__(
        self,
        *,
        label: str,
        pattern: str = "",
        default_value: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        validation_regex: Optional[str] = None,
        regex_error_message: Optional[str] = None,
        suggestion: str = "",
        jobop_group: str = "Main",
        allow_upload: bool = False,
    ) -> None:
        if suggestion != "":
            suggest = suggestion
        else:
            suggest = pattern

        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.pattern = pattern
        self.validation_regex = validation_regex
        self.regex_error_message = regex_error_message
        self.suggestion = suggest
        self.allow_upload = allow_upload

    def get_list(self) -> List[str]:
        """Return the items in self.value as a list of strings

        Returns:
            List[str]: The items in the JobOptions comma separated string
                as a list, with any white space at the beginning and/or end
                removed
        """
        self.check_required_not_empty()
        return [x.strip() for x in self.value.split(":::") if len(x.strip()) > 0]

    def get_basename_mapping(self) -> Dict[str, str]:
        return increment_file_basenames(self.get_list())

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            List[:class:`~pipeliner.job_options.JobOptionValidationResult`]: The
            validation errors of any exist
        """
        results = super().validate()
        if results:
            return results
        val_list = self.get_list()
        for entered in val_list:
            if not Path(entered).is_file():
                results.append(
                    JobOptionValidationResult(
                        "warning", [self], f"{entered}; file not found"
                    )
                )
            if self.validation_regex is not None:
                reg = re.compile(self.validation_regex)
                regerror = (
                    self.regex_error_message
                    if self.regex_error_message is not None
                    else f"{entered} format must match pattern "
                    f"'{self.validation_regex}'"
                )
                if not reg.match(entered):
                    results.append(JobOptionValidationResult("error", [self], regerror))

        if len(set(self.get_list())) != len(self.get_list()):
            results.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self],
                    message="Files cannot have duplicate names",
                )
            )

        return results

    def check_file(self) -> List[JobOptionValidationResult]:
        """Check that specified input files exist

        For each input file that does not exist, a JobOptionValidationResult of type
        "error" is returned.

        Returns:
            List[:class:~pipeliner.job_options.JobOptionValidationResult]: if any of
            the files do not exist
        """
        errs = []
        for entered in self.get_list():
            in_path = shutil.which(entered)
            is_file = Path(entered).is_file()
            if not (in_path or is_file):
                errs.append(
                    JobOptionValidationResult(
                        "error", [self], f"File not found: {entered}"
                    )
                )
        return errs


class InputNodeJobOption(JobOption):
    """Define a job option as a file name, create an input node for it

    GUI will open a file browser for the user to find an input

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        node_type (str): Top level node type for this node
        default_value (str) = The default value for the parameter.  The type
            will be set by the specific class method
        pattern (str): Info about the search string for file types. It should be
            in the format <description> (<example>) IE: `"MRC files
            (*.mrc)"`
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        suggestion (str): A hint that will appear in the data entry field if no
            no default value is set, it will not be used. IE: 'Enter file path here'
            If you want the suggestion used as the default then just put it in
            default_value
    """

    joboption_type = "INPUTNODE"

    def __init__(
        self,
        *,
        label: str,
        node_type: str,
        default_value: str = "",
        pattern: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        validation_regex: Optional[str] = None,
        regex_error_message: Optional[str] = None,
        suggestion: str = "",
        jobop_group: str = "Main",
        node_kwds: Optional[List[str]] = None,
    ) -> None:
        if suggestion != "":
            suggest = suggestion
        else:
            suggest = pattern

        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.pattern = pattern
        self.node_type = node_type
        self.validation_regex = validation_regex
        self.regex_error_message = regex_error_message
        self.suggestion = suggest
        self.node_kwds = node_kwds if node_kwds else None

    def get_input_nodes(self) -> List[Node]:
        if not self.value:
            return []
        return [create_node(self.value, self.node_type, self.node_kwds)]

    def get_basename_mapping(self) -> Dict[str, str]:
        return increment_file_basenames([self.value])

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            List[:class:`~pipeliner.job_options.JobOptionValidationResult`]: The
            validation error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.validation_regex is not None and self.value not in ["", None]:
            reg = re.compile(self.validation_regex)
            regerror = (
                self.regex_error_message
                if self.regex_error_message is not None
                else f"Value format must match pattern '{self.validation_regex}'"
            )
            if not reg.match(self.value):
                results.append(JobOptionValidationResult("error", [self], regerror))
        return results

    def check_file(self) -> List[JobOptionValidationResult]:
        """Check that specified input files exist

        Returns:
            str: An error message if the file does not exist
        """

        if not Path(self.value).is_file():
            return [
                JobOptionValidationResult(
                    "error",
                    [self],
                    f"File {self.value} not found, has the job expected to produce it "
                    "finished successfully?",
                )
            ]
        return []


class MultiInputNodeJobOption(JobOption):
    """Define a job option as a file name, create an input node for it

    GUI will open a file browser for the user to find an input

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        node_type (str): Toplevel node type for this node.
        node_kwds: Optional[List[Optional[List[str]]]]: keywords for each toplevel
            type in node_types.  The index of the toplevel type is used to match the
            kwds to toplevel type.
        default_value (str) = The default value for the parameter.  The type
            will be set by the specific class method
        pattern (str): Info about the search string for file types. It should be
            in the format <description> (<example>), e.g. `"MRC files (*.mrc)"`
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        suggestion (str): A hint that will appear in the data entry field if no
            no default value is set, it will not be used. IE: 'Enter file path here'
            If you want the suggestion used as the default then just put it in
            default_value
    """

    joboption_type = "MULTIINPUTNODE"

    def __init__(
        self,
        *,
        label: str,
        node_type: str,
        default_value: str = "",
        pattern: str = "",
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        validation_regex: Optional[str] = None,
        regex_error_message: Optional[str] = None,
        suggestion: str = "",
        jobop_group: str = "Main",
        node_kwds: Optional[List[str]] = None,
    ) -> None:
        if suggestion != "":
            suggest = suggestion
        else:
            suggest = pattern

        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.pattern = pattern
        self.node_type = node_type
        self.validation_regex = validation_regex
        self.regex_error_message = regex_error_message
        self.suggestion = suggest
        self.node_kwds = node_kwds

    def get_list(self):
        return [x.strip() for x in self.value.split(":::") if len(x.strip()) > 0]

    def get_basename_mapping(self) -> Dict[str, str]:
        return increment_file_basenames(self.get_list())

    def get_input_nodes(self) -> List[Node]:
        if not self.value:
            return []
        out_nodes: List[Node] = []
        for node in self.get_list():
            out_nodes.append(create_node(node, self.node_type, self.node_kwds))
        return out_nodes

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            List[:class:`~pipeliner.job_options.JobOptionValidationResult`]: The
            validation results (if any)
        """
        results = super().validate()
        if results:
            return results
        val_list = self.get_list()
        for ent in val_list:
            if self.validation_regex is not None:
                reg = re.compile(self.validation_regex)
                regerror = (
                    self.regex_error_message
                    if self.regex_error_message is not None
                    else f"{ent} format must match pattern '{self.validation_regex}'"
                )
                if not reg.match(ent):
                    results.append(JobOptionValidationResult("error", [self], regerror))
            if not Path(ent).is_file():
                results.append(
                    JobOptionValidationResult(
                        "warning", [self], f"File {ent} does not exist yet"
                    )
                )

        if len(set(self.get_list())) != len(self.get_list()):
            results.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self],
                    message="Files cannot have duplicate names",
                )
            )

        return results

    def check_file(self) -> List[JobOptionValidationResult]:
        """Check that specified input files exist

        For each input file that does not exist, a JobOptionValidationResult of type
        "error" is returned.

        Returns:
            List[:class:~pipeliner.job_options.JobOptionValidationResult]: if any of
            the files do not exist
        """
        errs = []
        for entered in self.get_list():

            if not Path(entered).is_file():
                errs.append(
                    JobOptionValidationResult(
                        "error",
                        [self],
                        f"File {entered} not found, has the job expected to produce it "
                        "finished successfully?",
                    )
                )
        return errs


class BooleanJobOption(JobOption):
    """Define a job option as a boolean

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        default_value (bool): The default value
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
    """

    joboption_type = "BOOLEAN"

    def __init__(
        self,
        *,
        label: str,
        default_value: bool,
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        deactivate_if: Optional[JobOptionCondition] = None,
        jobop_group: str = "Main",
    ) -> None:
        # Convert bool value to string for storage
        # TODO: replace with simple bool value instead
        #  This is a bit tricky because various parts of the code use `value` or
        #  `default_value` directly, expecting it to be a string
        string_value = "Yes" if default_value else "No"
        super().__init__(
            label=label,
            default_value=string_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )

    def get_boolean(self) -> bool:
        """Get a boolean value

        Returns:
            bool: The value
        """
        return self.value.lower() in TRUES

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            :class:`~pipeliner.job_options.JobOptionValidationResult`: The validation
                error of one exists
        """
        results = super().validate()
        if results:
            return results
        if self.value not in (True, False) and self.value.lower() not in TRUES + FALSES:
            results.append(
                JobOptionValidationResult(
                    "error",
                    [self],
                    "Boolean job option must have a value that equates"
                    f" to true or false {TRUES} or {FALSES}",
                )
            )
        return results


class MultipleChoiceJobOption(JobOption):
    """Define a job option as a pull down menu

    Radio is a misnomer, the GUI will display a pull down menu with the options

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
            run.job file
        choices (list): A list :class:`str` options for the menu
        default_value_index (int): Index of the initial option for the radio; also used
            as the default value
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued

    Raises:
        ValueError: If the index of the initial option is not in the radio menu
    """

    joboption_type = "MULTIPLECHOICE"

    def __init__(
        self,
        *,
        label: str,
        choices: List[str],
        default_value_index: int,
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = True,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        jobop_group: str = "Main",
    ) -> None:
        try:
            default_value = choices[default_value_index]
        except KeyError:
            raise ValueError(
                "ERROR: Index of default value not in multiple choice options"
                f" index={default_value_index}, radio_menu has {len(choices)} items"
            )
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.choices = choices

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation of the input parameters

        Returns:
            List[:class:`~pipeliner.job_options.JobOptionValidationResult`]: The
            validation error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.value not in self.choices:
            results.append(
                JobOptionValidationResult(
                    "error",
                    [self],
                    f"'{self.value}' not in list of accepted values: {self.choices}",
                )
            )
        return results


class FloatJobOption(JobOption):
    """Define a job option as a slider for inputting numbers as floats

    The min value and max value are for display only, they do not limit the actual
    values that can be input

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
                run.job file
        default_value(float): The default value for the parameter
        suggested_min (float): The suggested minimum value for the slider
        suggested_max (float): The suggested maximum value for the slider
        step_value (float): The step value for the slider
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        hard_min (float): An error will be raised if the value is < this value
        hard_max (float): An error will be raised if the value is > this value
    """

    joboption_type = "FLOAT"

    def __init__(
        self,
        *,
        label: str,
        default_value: Optional[Union[str, float]],
        suggested_min: Optional[float] = None,
        suggested_max: Optional[float] = None,
        step_value: float = 1.0,
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        hard_min: Optional[float] = None,
        hard_max: Optional[float] = None,
        jobop_group: str = "Main",
    ) -> None:
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        self.suggested_min = suggested_min
        self.suggested_max = suggested_max
        self.step_value = step_value
        self.hard_min = hard_min
        self.hard_max = hard_max

        for val in [
            default_value,
            suggested_min,
            suggested_max,
            step_value,
            hard_min,
            hard_max,
        ]:
            if val is not None and type(val) not in (float, int):
                raise ValueError(
                    f"Improper JobOption value in '{label}', all values must be"
                    " numbers for this JobOption type."
                )

    def get_number(self) -> float:
        """Get the value of a JobOption as a number

        Returns:
            float: The value

        Raises:
            ValueError: If the value cannot be converted to a :class:`float`
            ValueError: If the value is required and missing
        """
        self.check_required_not_empty()
        try:
            val = float(self.value)
        except ValueError:
            raise ValueError(
                "ERROR: Could not return a number from the value: " + self.value
            )
        return val

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation check on the job option

        Returns:
            :class:`~pipeliner.job_options.JobOptionValidationResult`: The validation
                error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.hard_min is not None:
            if self.get_number() < self.hard_min:
                results.append(
                    JobOptionValidationResult(
                        "error", [self], f"Value cannot be less than {self.hard_min}"
                    )
                )
        if self.hard_max is not None:
            if self.get_number() > self.hard_max:
                results.append(
                    JobOptionValidationResult(
                        "error", [self], f"Value cannot be greater than {self.hard_max}"
                    )
                )

        # -1 is a special case as it is often used to mean use the default or ignore
        if self.suggested_min is not None and self.suggested_min != -1:
            if self.get_number() < self.suggested_min and self.get_number() != -1.0:
                results.append(
                    JobOptionValidationResult(
                        "warning",
                        [self],
                        f"Value outside suggested range: Min {self.suggested_min}",
                    )
                )

        if self.suggested_max is not None and self.suggested_max != -1:
            if self.get_number() > self.suggested_max and self.get_number() != 999.99:
                results.append(
                    JobOptionValidationResult(
                        "warning",
                        [self],
                        f"Value outside suggested range: Max {self.suggested_max}",
                    )
                )
        return results


class IntJobOption(JobOption):
    """Define a job option as a slider for inputting numbers as integers

    The slider min value and max value are for display only, they do not
    limit the actual values that can be input

    Args:
        label (str): A verbose label for the parameter.  This is what appears in a
                run.job file
        default_value (int): The default value for the parameter
        suggested_min (int): The suggested minimum value for the slider
        suggested_max (int): The suggested maximum value for the slider
        step_value (int): The step value for the slider
        help_text (str): Text that will be displayed in the GUI if help is clicked
        in_continue (bool): If this parameter can be modified in a job that is
            continued
        hard_min (int): An error will be raised if the value is < this value
        hard_max (int): An error will be raised if the value is > this value

    Raises:
        ValueError: If the suggested_min, suggested_max, default, hard_min , hard_max,
            or step values are not integers
    """

    joboption_type = "INT"

    def __init__(
        self,
        *,
        label: str,
        default_value: Optional[int],
        suggested_min: Optional[int] = None,
        suggested_max: Optional[int] = None,
        step_value: int = 1,
        help_text: str = "",
        in_continue: bool = False,
        only_in_continue: bool = False,
        is_required: bool = False,
        required_if: Optional[JobOptionCondition] = None,
        deactivate_if: Optional[JobOptionCondition] = None,
        hard_min: Optional[int] = None,
        hard_max: Optional[int] = None,
        jobop_group: str = "Main",
    ) -> None:
        super().__init__(
            label=label,
            default_value=default_value,
            help_text=help_text,
            in_continue=in_continue,
            only_in_continue=only_in_continue,
            is_required=is_required,
            required_if=required_if,
            deactivate_if=deactivate_if,
            jobop_group=jobop_group,
        )
        for val in [
            default_value,
            suggested_min,
            suggested_max,
            step_value,
            hard_min,
            hard_max,
        ]:
            if val is not None and not isinstance(val, int):
                raise ValueError(
                    f"Improper JobOption value in '{label}', all values must be"
                    " integers for this JobOption type.  Perhaps a FloatJobOption is"
                    " more appropriate?"
                )

        self.suggested_min = suggested_min
        self.suggested_max = suggested_max
        self.step_value = step_value
        self.hard_min = hard_min
        self.hard_max = hard_max

    def get_number(self) -> int:
        """Get the value of a JobOption as a number

        Returns:
            int: The value

        Raises:
            ValueError: If the value cannot be converted to a :class:`float`
            ValueError: If the value is required and missing
        """
        self.check_required_not_empty()
        try:
            val = int(self.value)
        except ValueError:
            raise ValueError(
                "ERROR: Could not return a number from the value: " + self.value
            )
        return val

    def validate(self) -> List[JobOptionValidationResult]:
        """Basic validation check on the job option

        Returns:
            :class:`~pipeliner.job_options.JobOptionValidationResult`: The validation
                error of one exists
        """
        results = super().validate()
        if results:
            return results

        if self.hard_min is not None:
            if self.get_number() < self.hard_min:
                results.append(
                    JobOptionValidationResult(
                        "error", [self], f"Value cannot be less than {self.hard_min}"
                    )
                )
        if self.hard_max is not None:
            if self.get_number() > self.hard_max:
                results.append(
                    JobOptionValidationResult(
                        "error", [self], f"Value cannot be greater than {self.hard_max}"
                    )
                )
        # -1 is a special case as it is often used to mean use the default or ignore
        if self.suggested_min is not None:
            if self.get_number() < self.suggested_min and self.get_number() != -1:
                results.append(
                    JobOptionValidationResult(
                        "warning",
                        [self],
                        f"Value outside suggested range: Min {self.suggested_min}",
                    )
                )
        if self.suggested_max is not None:
            if self.get_number() > self.suggested_max and self.get_number() != -1:
                results.append(
                    JobOptionValidationResult(
                        "warning",
                        [self],
                        f"Value outside suggested range: Max {self.suggested_max}",
                    )
                )
        return results


class JobOptionValidationResult(object):
    """A class for handling validation of joboptions

    Attributes:
         type (str): One of 'warning', 'error' or 'info'
         raised_by (list): List of JobOption objects that raised the warning/error.
            Generally this will only be one JobOption, unless the warning/error
            was raised from comparing two or more JobOptions
         message (str): Description of the error/warning
    """

    def __init__(
        self,
        result_type: Literal["warning", "error", "info"],
        raised_by: List[JobOption],
        message: str,
    ) -> None:
        """Create a JobOptionValidationResult object

        Args:
            result_type (str): `warning`, `error`, or `info`
            raised_by (list): The JobOption(s) that raised the error
            message (str): The warning or error message to display

        Raises:
            ValueError: If result_type is not in the list of appropriate values
        """

        if result_type not in ["warning", "error", "info"]:
            raise ValueError("result_type must be either 'warning', 'error' or 'info'")
        self.type = result_type
        self.message = message
        if not isinstance(raised_by, list):
            raise ValueError("'raised_by' must be a list")
        self.raised_by = raised_by

    def __str__(self) -> str:
        raised_by_part = ""
        if len(self.raised_by) == 1:
            raised_by_part = f'job option "{self.raised_by[0].label}": '
        elif len(self.raised_by) > 1:
            joined_labels = '", "'.join([jobop.label for jobop in self.raised_by])
            raised_by_part = f'job options "{joined_labels}": '
        return f"{self.type}: {raised_by_part}{self.message}"
