#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from pathlib import Path
from glob import glob
from pandas import read_csv
from typing import List, Dict, Any
import shutil
import random

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
    JobOptionCondition,
)
from pipeliner.node_factory import create_node
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS, NODE_SEQUENCE
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject


class EMPlacement(PipelinerJob):
    """
    Phaser EM Placement Job
    Fits model into cryoEM map.
    """

    PROCESS_NAME = "em_placement.atomic_model_fit"
    OUT_DIR = "EMPlacement"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "em_placement"
        self.jobinfo.version = "0.2"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.short_desc = "Fit model in whole map with em_placement"
        self.jobinfo.long_desc = (
            "EM Placement:  Dock a model in a cryo EM map with a likelihood target. "
            "Requires CCP4 9.0.002 or greater"
        )
        self.jobinfo.documentation = (
            "https://gitlab.developers.cam.ac.uk/scm/haematology/readgroup/phasertng"
        )
        self.jobinfo.programs = [ExternalProgram(self.get_em_placement_command())]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Millan C",
                    "McCoy AJ",
                    "Terwilliger TC",
                    "Read RJ",
                ],
                title="Likelihood-based docking of models into cryo-EM maps",
                journal="Acta Cryst D",
                year="2023",
                volume="79",
                issue="D",
                pages="281-289",
                doi="10.1107/S2059798323001602",
            ),
            Ref(
                authors=[
                    "McCoy AJ",
                    "Grosse-Kunstleve RW",
                    "Adams PD",
                    "Winn MD",
                    "Storoni LC",
                    "Read RJ",
                ],
                title="Phaser Crystallographic Software",
                journal="J Appl Crystallography",
                year="2007",
                volume="40",
                issue="1",
                pages="658-674",
                doi="10.1107/S0021889807021206",
            ),
        ]

        self.joboptions["model"] = InputNodeJobOption(
            label="Model to dock",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text=(
                "Atomic model to dock.  This can be a full or partial model of the "
                "expected complete complex."
            ),
            is_required=True,
        )

        self.joboptions["use_reference_structure"] = BooleanJobOption(
            label="Estimate composition from model",
            default_value=False,
            help_text=(
                "Provide either a reference structure or a FASTA sequence file to "
                "estimate composition of the map reconstruction. Select 'Yes' to  "
                "provide a reference model whose composition matches with the "
                "expected full complex of the maps else provide a sequence file."
            ),
        )

        self.joboptions["sequence"] = InputNodeJobOption(
            label="FASTA Sequence",
            pattern=files_exts("Sequence", [".txt", ".fasta", ".fas"]),
            node_type=NODE_SEQUENCE,
            default_value="",
            help_text=(
                "Sequence file in fasta format with composition for full "
                "reconstruction. All copies of the chains must be included as "
                "separate chains in the FASTA file.  Some FASTA files use "
                "multiple chains in the definition line (e.g. >Protein|Chains ABC) "
                "and this will be incorrectly interpretted as a single chain."
            ),
            required_if=JobOptionCondition([("use_reference_structure", "=", False)]),
            deactivate_if=JobOptionCondition([("use_reference_structure", "=", True)]),
        )

        self.joboptions["reference_structure"] = InputNodeJobOption(
            label="Composition model",
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            node_type=NODE_ATOMCOORDS,
            default_value="",
            help_text=(
                "Structure of the expected full complex for the input maps. Sequence "
                "information of the complex will be extracted from this structure."
            ),
            required_if=JobOptionCondition([("use_reference_structure", "=", True)]),
            deactivate_if=JobOptionCondition([("use_reference_structure", "=", False)]),
        )

        self.joboptions["input_map1"] = InputNodeJobOption(
            label="Input half map 1",
            node_type=NODE_DENSITYMAP,
            node_kwds=["halfmap"],
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The first half map to dock the model against",
            is_required=True,
        )

        self.joboptions["input_map2"] = InputNodeJobOption(
            label="Input half map 2",
            node_type=NODE_DENSITYMAP,
            node_kwds=["halfmap"],
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The second half map to dock the model against",
            is_required=True,
        )

        self.joboptions["best_resolution"] = FloatJobOption(
            label="Best resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Best resolution in full map (Å)",
            is_required=True,
        )

        self.joboptions["symmetry"] = StringJobOption(
            label="Point group symmetry",
            default_value="",
            help_text="Specify point group symmetry of input maps.",
            validation_regex="(C[1-9][0-9]*)|(D[2-9][0-9]*)|I[243]|O",
            regex_error_message="Symmetry must be in Cn, Dn, I[234], or O format",
        )

        self.joboptions["estimated_rmsd"] = FloatJobOption(
            label="Estimated RMSD",
            default_value=1.2,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text=(
                "1.0 should be used for an identical structure, typically 1.2 for "
                "an AlphaFold or similarly predicted structure or the expected "
                "rms for an experimentally derived homologue"
            ),
            is_required=True,
        )

        self.get_runtab_options()

    def create_output_nodes(self) -> None:
        # All output nodes are created after the job in create_post_run_output_nodes
        pass

    def get_commands(self) -> List[PipelinerCommand]:
        commands = []
        # Run in the job output directory
        self.working_dir = self.output_dir
        model_file = self.joboptions["model"].get_string()
        molecule_name = Path(model_file).stem
        input_map1 = self.joboptions["input_map1"].get_string()
        input_map2 = self.joboptions["input_map2"].get_string()
        best_resolution = self.joboptions["best_resolution"].get_number()
        symmetry = self.joboptions["symmetry"].get_string()
        sequence_file = self.joboptions["sequence"].get_string()
        estimated_rmsd = self.joboptions["estimated_rmsd"].get_number()
        use_reference = self.joboptions["use_reference_structure"].get_boolean()

        phil_name = "em_placement.phil"
        # if a reference model is given, write sequence file
        if use_reference:
            reference_structure = self.joboptions["reference_structure"].get_string()
            modelid = os.path.splitext(os.path.basename(reference_structure))[0]
            seq_generate_command = [
                "python3",
                get_job_script("em_placement/write_seq_from_structure.py"),
                "-p",
                os.path.relpath(reference_structure, self.working_dir),
                "-mid",
                modelid,
            ]
            commands.append(seq_generate_command)
            sequence_file = modelid + "_seq.fasta"
        else:
            sequence_file = os.path.relpath(sequence_file, self.working_dir)
        # Currently em_placement only supports half maps but will support
        # single maps in the future.
        half_map_1 = os.path.relpath(input_map1, self.working_dir)
        half_map_2 = os.path.relpath(input_map2, self.working_dir)
        if not symmetry:
            symmetry = "None"
        config_command = [
            "python3",
            get_job_script("em_placement/em_placement_config.py"),
            half_map_1,
            half_map_2,
            str(best_resolution),
            symmetry,
            sequence_file,
            molecule_name,
            os.path.relpath(model_file, self.working_dir),
            str(estimated_rmsd),
            phil_name,
        ]

        em_placement_command = [self.get_em_placement_command(), phil_name]
        commands.extend([config_command, em_placement_command])
        return [PipelinerCommand(x) for x in commands]

    # Work around as phaser_voyager commands are in $CCP4/libexec not
    # $CCP4/bin and therefore not on path
    @staticmethod
    def get_em_placement_command() -> str:
        ccp4_python = shutil.which("ccp4-python")
        if ccp4_python is not None:
            return ccp4_python.replace(
                "/bin/ccp4-python",
                "/libexec/phaser_voyager.em_placement",
            )
        else:
            return "phaser_voyager.em_placement not found"

    def create_post_run_output_nodes(self) -> None:
        # Find all PDB in split output directories and add as node type
        search_pdb = os.path.join(self.output_dir, "emplaced_files*/*.pdb")
        pdbs = glob(search_pdb)
        for pdb in sorted(pdbs):
            self.output_nodes.append(
                create_node(pdb, NODE_ATOMCOORDS, ["em_placement"])
            )
        search_maps = os.path.join(self.output_dir, "emplaced_files*/*.map")
        maps = glob(search_maps)
        for the_map in sorted(maps):
            self.output_nodes.append(
                create_node(the_map, NODE_DENSITYMAP, ["em_placement"])
            )

    def get_output_table_path(self) -> str:
        # Find path, required as em_placement directory can not be specified
        for the_dir in os.listdir(self.output_dir):
            if the_dir.startswith("emplaced_files"):
                csv_path = os.path.join(
                    self.output_dir, the_dir, "output_table_docking.csv"
                )
                break
        else:
            raise FileNotFoundError("Could not find emplaced_files directory")
        return csv_path

    def gather_metadata(self) -> Dict[str, Any]:
        """
        em_placement generates 'output_table_docking.csv' which contains summary
        metadata.
        """
        csv_path = self.get_output_table_path()

        # Read csv as dataframe and convert to dict
        docking_output = read_csv(csv_path)
        metadata_dict = docking_output.to_dict()
        md_str: Dict[str, Any] = {
            str(key): value for key, value in metadata_dict.items()
        }
        return md_str

    def create_results_display(self) -> List[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        # Find all PDBs in split output directories
        search_pdbs = os.path.join(self.output_dir, "emplaced_files*/*.pdb")
        pdbs = glob(search_pdbs)
        # Show models, maps and scores if available
        if pdbs:
            # Find all maps in split output directories
            search_maps = os.path.join(self.output_dir, "emplaced_files*/*.map")
            maps = glob(search_maps)
            maps_opacity = [0.5] * len(maps)
            # Set maps to different colours so users see that there are mulitple maps
            maps_colours = []
            for n in maps:
                maps_colours.append("#{:06x}".format(random.randint(0, 0xFFFFFF)))
            display_objects.append(
                make_map_model_thumb_and_display(
                    maps=maps,
                    maps_opacity=maps_opacity,
                    maps_colours=maps_colours,
                    models=pdbs,
                    title="em_placement best scoring docked models",
                    outputdir=self.output_dir,
                )
            )
            # scores table
            scores_data = []
            csv_path = self.get_output_table_path()
            with open(csv_path, "r") as o:
                for line in o:
                    lsplit = line.split(",")
                    if lsplit[0] == "Solution_name":
                        headers = lsplit[:5] + [lsplit[6]]
                    else:
                        scores_data.append(
                            lsplit[:3]
                            + [
                                round(float(lsplit[3]), 3),  # LLG
                                round(float(lsplit[4]), 3),  # Z score
                                round(float(lsplit[6]), 3),  # CC
                            ]
                        )
            display_objects.append(
                create_results_display_object(
                    "table",
                    title="Global map-fit scores",
                    headers=headers,
                    table_data=scores_data,
                    associated_data=[csv_path],
                )
            )
        return display_objects
