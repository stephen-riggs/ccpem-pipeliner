#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from pathlib import Path
from typing import List, Sequence
from pipeliner.pipeliner_job import PipelinerJob, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    files_exts,
    BooleanJobOption,
    JobOptionCondition,
)
import json
from pipeliner.node_factory import create_node
from pipeliner.nodes import NODE_ATOMCOORDS, NODE_DENSITYMAP
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
    make_mrcs_central_slices_montage,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject


class GemmiModelToMap(PipelinerJob):
    PROCESS_NAME = "gemmi.atomic_model_utilities.model_to_map"
    OUT_DIR = "GemmiModelToMap"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Gemmi Model to Map"
        self.jobinfo.short_desc = "Convert atomic model to map"
        self.jobinfo.long_desc = (
            "Calculates map from atomic using sfcalc command from Gemmi library."
        )
        self.jobinfo.programs = []
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=["Wojdyr M"],
                title=("GEMMI: A library for structural biology"),
                journal="Journal of Open Source Software",
                year="2022",
                volume="7",
                issue="43",
                pages="4200",
                doi="10.21105/joss.04200",
            )
        ]
        self.jobinfo.documentation = "https://gemmi.readthedocs.io/"

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text="Model to generate map from",
            is_required=True,
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=10.0,
            suggested_min=0.5,
            suggested_max=100,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in angstroms",
            is_required=True,
        )
        self.joboptions["reference_map"] = InputNodeJobOption(
            label="Reference map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Optionally apply map dimensions from reference map",
            is_required=False,
        )
        self.joboptions["ref_aligned"] = BooleanJobOption(
            label="Reference map aligned?",
            default_value=True,
            help_text="Reference map aligned with the model?",
            deactivate_if=JobOptionCondition([("reference_map", "=", "")]),
        )
        self.joboptions["cell_x"] = FloatJobOption(
            label="Cell x",
            default_value=-1,
            suggested_min=1.0,
            step_value=0.1,
            help_text="Optionally set map cell dimensions (in angstroms)",
            is_required=False,
        )
        self.joboptions["cell_y"] = FloatJobOption(
            label="Cell y",
            default_value=-1,
            suggested_min=1.0,
            step_value=0.1,
            help_text="Optionally set map cell dimensions (in angstroms)",
            is_required=False,
        )
        self.joboptions["cell_z"] = FloatJobOption(
            label="Cell z",
            default_value=-1,
            suggested_min=1.0,
            step_value=0.1,
            help_text="Optionally set map cell dimensions (in angstroms)",
            is_required=False,
        )
        self.get_runtab_options()

    def create_output_nodes(self) -> None:
        input_model = self.joboptions["input_model"].get_string()
        map_out = "gemmi_" + Path(input_model).stem + ".mrc"
        self.add_output_node(map_out, NODE_DENSITYMAP, ["simulated"])

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        # model_to_map --resolution=4.0 --pdb=refined.pdb

        # Get parameters
        input_model = self.joboptions["input_model"].get_string()
        self.input_nodes.append(
            create_node(self.joboptions["input_model"].get_string(), NODE_ATOMCOORDS)
        )
        resolution = self.joboptions["resolution"].get_number()
        reference_map = self.joboptions["reference_map"].get_string()
        ref_aligned = self.joboptions["ref_aligned"].get_boolean()
        cell_dims = (
            self.joboptions["cell_x"].get_number(),
            self.joboptions["cell_y"].get_number(),
            self.joboptions["cell_z"].get_number(),
        )
        command: list = ["python3", get_job_script("model_to_map.py")]
        command += ["--resolution", str(resolution)]
        command += ["--pdb", os.path.relpath(input_model, self.working_dir)]
        if reference_map != "":
            command += ["--map_ref", os.path.relpath(reference_map, self.working_dir)]
        if not ref_aligned:
            command += ["--ref_notaligned"]
        if cell_dims.count(-1) == 0:
            command += [
                "--dim_x",
                cell_dims[0],
                "--dim_y",
                cell_dims[1],
                "--dim_z",
                cell_dims[2],
            ]

        commands = [PipelinerCommand(command)]
        return commands

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        output_map = self.output_nodes[0].name
        input_models = [
            node.name
            for node in self.input_nodes
            if node.toplevel_type == NODE_ATOMCOORDS
        ]

        display_objs: List[ResultsDisplayObject] = [
            make_mrcs_central_slices_montage(
                in_files={output_map: "Map from model"},
                output_dir=self.output_dir,
            )
        ]
        map_display = make_map_model_thumb_and_display(
            maps=[output_map],
            maps_opacity=[0.5],
            title="Map from model",
            models=input_models,
            outputdir=self.output_dir,
        )
        display_objs.append(map_display)
        # b-factor distribution
        bfact_json = os.path.join(
            self.output_dir, "bfact_" + Path(input_models[0]).stem + ".json"
        )
        if os.path.isfile(bfact_json):
            with open(bfact_json, "r") as b:
                dict_b_iso = json.load(b)
            bfact_plotlyhist_obj = create_results_display_object(
                "plotlyhistogram",
                data=dict_b_iso,
                title="Atomic B-factor distribution",
                associated_data=[bfact_json],
            )
            display_objs.append(bfact_plotlyhist_obj)
        return display_objs
