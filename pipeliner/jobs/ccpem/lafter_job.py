#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from typing import List, Sequence

from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
    JobOptionCondition,
    JobOptionValidationResult,
)
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.nodes import NODE_DENSITYMAP, NODE_MASK3D
from pipeliner.results_display_objects import ResultsDisplayObject


class LafterJob(PipelinerJob):
    """Local de-noising of cryo-EM maps"""

    PROCESS_NAME = "lafter.postprocess.local_denoising"
    OUT_DIR = "Lafter"
    CATEGORY_LABEL = "Map Postprocessing"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "LAFTER"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.short_desc = "Local de-noising of SPA reconstructions"
        self.jobinfo.long_desc = "Local de-noising of cryo-EM maps."
        self.jobinfo.documentation = (
            "https://github.com/StructuralBiology-ICLMedicine/lafter"
        )
        self.jobinfo.programs = [
            ExternalProgram("lafter"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=["Ramlaul K", "Palmer CM", "Aylett CHS"],
                title=(
                    "A Local Agreement Filtering Algorithm for Transmission "
                    "EM Reconstructions"
                ),
                journal="Journal of Structural Biology",
                year="2019",
                volume="205",
                issue="1",
                pages="30-40",
                doi="10.1016/j.jsb.2018.11.011",
            ),
        ]
        self.joboptions["half_map_1"] = InputNodeJobOption(
            label="Half map 1",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="First half map for denoising (MRC format)",
            is_required=True,
        )
        self.joboptions["half_map_2"] = InputNodeJobOption(
            label="Half map 2",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Second half map for denoising (MRC format)",
            is_required=True,
        )
        self.joboptions["mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="Mask to define particle region (MRC mode 2 format)",
            required_if=JobOptionCondition([("particle_diameter", "<=", 20)]),
        )
        self.joboptions["particle_diameter"] = FloatJobOption(
            label="Particle diameter",
            default_value=0,
            hard_min=0,
            step_value=0.1,
            help_text="Particle diameter in voxels (if no mask available)",
            required_if=JobOptionCondition([("mask", "=", "")]),
            deactivate_if=JobOptionCondition([("mask", "!=", "")]),
        )
        self.joboptions["sharp"] = BooleanJobOption(
            label="Additional sharpening",
            default_value=False,
            help_text="Additional sharpening (usually not necessary)",
            jobop_group="Advanced options",
        )
        self.joboptions["fsc"] = FloatJobOption(
            label="FSC cut-off threshold",
            default_value=0.143,
            hard_min=0.1,
            hard_max=1.0,
            step_value=0.001,
            help_text="FSC cut-off threshold (default 0.143)",
            jobop_group="Advanced options",
        )
        self.joboptions["oversample"] = BooleanJobOption(
            label="Over-sample output?",
            default_value=False,
            help_text=(
                "Over-sample output maps by a factor of 2. This makes them look much"
                " smoother but makes the map files bigger and slower to work with."
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["overfitting"] = BooleanJobOption(
            label="Compensate for overfitting",
            default_value=False,
            help_text=(
                "Attempt to mitigate against the effects of overfitting from"
                " 3D refinement. This is not foolproof and it is usually a better idea"
                " to reduce overfitting at the refinement stage instead."
            ),
            jobop_group="Advanced options",
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        mask = self.joboptions["mask"].get_string()
        particle_diameter = self.joboptions["particle_diameter"].get_number()
        if not mask:
            if particle_diameter <= 0:
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[
                            self.joboptions["mask"],
                            self.joboptions["particle_diameter"],
                        ],
                        message="Must provide either a mask or particle diameter",
                    )
                )
            elif particle_diameter < 20.0:
                errors.append(
                    JobOptionValidationResult(
                        result_type="error",
                        raised_by=[self.joboptions["particle_diameter"]],
                        message="Value cannot be less than 20",
                    )
                )
        return errors

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "LAFTER_filtered.mrc", NODE_DENSITYMAP, ["lafter", "denoise"]
        )

    def get_commands(self) -> List[PipelinerCommand]:
        # lafter --v1 3488_run_half1_class001_unfil.mrc
        #   --v2 3488_run_half2_class001_unfil.mrc --particle_diameter 75.0

        # Run in the job output directory
        self.working_dir = self.output_dir

        # Get inputs
        half_map_1 = self.joboptions["half_map_1"].get_string()
        half_map_2 = self.joboptions["half_map_2"].get_string()
        mask = self.joboptions["mask"].get_string()
        particle_diameter = self.joboptions["particle_diameter"].get_number()
        fsc = self.joboptions["fsc"].get_number()
        sharp = self.joboptions["sharp"].get_boolean()
        oversample = self.joboptions["oversample"].get_boolean()
        overfitting = self.joboptions["overfitting"].get_boolean()

        # Create command arguments
        command = [self.jobinfo.programs[0].command]
        command += [
            "--v1",
            os.path.relpath(half_map_1, self.working_dir),
            "--v2",
            os.path.relpath(half_map_2, self.working_dir),
        ]
        if mask != "":
            command += ["--mask", os.path.relpath(mask, self.working_dir)]
        elif particle_diameter > 0:
            command += ["--particle_diameter", str(particle_diameter)]
        else:
            raise ValueError("No mask or particle diameter given")

        if fsc > 0:
            command += ["--fsc", str(particle_diameter)]
        if sharp:
            command += ["--sharp"]
        if not oversample:
            command += ["--downsample"]
        if overfitting:
            command += ["--overfitting"]

        return [PipelinerCommand(command)]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:

        output_map = os.path.join(self.output_dir, "LAFTER_filtered.mrc")
        return make_maps_slice_montage_and_3d_display(
            in_maps={output_map: "LAFTER denoised map"}, output_dir=self.output_dir
        )
