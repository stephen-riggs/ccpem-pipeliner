#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
from typing import List, Dict, Any, Sequence, Union
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
    StringJobOption,
    JobOptionCondition,
    MultipleChoiceJobOption,
    IntJobOption,
    ExternalFileJobOption,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    make_mrcs_central_slices_montage,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_ATOMCOORDS,
    NODE_MASK3D,
    NODE_LOGFILE,
    NODE_LIGANDDESCRIPTION,
)
from pipeliner.results_display_objects import ResultsDisplayObject


class LocalSharpen(PipelinerJob):
    PROCESS_NAME = "locscale.postprocess.local_sharpening"
    OUT_DIR = "LocScale"
    CATEGORY_LABEL = "Map Postprocessing"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "LocScale"
        self.jobinfo.short_desc = "Local sharpening of 3D maps"
        self.jobinfo.long_desc = (
            "LocScale is a reference-based local amplitude scaling tool using"
            " prior model information to improve contrast of cryo-EM density"
            " maps. It can be used as an alternative to other commonly used map"
            " sharpening methods."
        )
        self.jobinfo.programs = [ExternalProgram("locscale")]
        self.jobinfo.version = "2.0"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=["Jakobi AJ", "Wilmanns M", "Sachse C"],
                title="Model-based local density sharpening of cryo-EM maps",
                journal="eLife",
                year="2017",
                volume="6",
                issue="1",
                pages="e27131",
                doi="https://doi.org/10.7554/eLife.27131",
            ),
            Ref(
                authors=["Bharadwaj A", "Jakobi AJ"],
                title="Electron scattering properties and "
                "their use in cryo-EM map sharpening",
                journal="Faraday Discussions",
                year="2022",
                issue="240",
                pages="168-183",
                doi="https://doi.org/10.1039/D2FD00078D",
            ),
        ]
        self.jobinfo.documentation = (
            "https://gitlab.tudelft.nl/aj-lab/locscale/-/wikis/home/"
        )

        self.joboptions["input_halfmap1"] = InputNodeJobOption(
            label="Input halfmap 1",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text="Input halfmap1 for local scaling",
            is_required=True,
            node_kwds=["halfmap"],
        )
        self.joboptions["input_halfmap2"] = InputNodeJobOption(
            label="Input halfmap 2",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text="Input halfmap2 for local scaling",
            node_kwds=["halfmap"],
            is_required=True,
        )
        self.joboptions["method"] = MultipleChoiceJobOption(
            label="Method",
            choices=["Feature enhance", "Model-free", "Hybrid", "Model-based"],
            default_value_index=0,
            help_text=("Select the local map processing/sharpening mode"),
        )
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text="Reference PDB with refined B-factors",
            deactivate_if=JobOptionCondition(
                [("method", "=", "Model-free"), ("method", "=", "Feature enhance")]
            ),
            required_if=JobOptionCondition(
                [("method", "=", "Model-based"), ("method", "=", "Hybrid")]
            ),
        )

        self.joboptions["ligand_cif"] = InputNodeJobOption(
            label="Ligand dictionary (cif)",
            node_type=NODE_LIGANDDESCRIPTION,
            pattern=files_exts("Ligand definition", [".cif"]),
            default_value="",
            help_text=(
                "Input ligand dictionary for model refinement."
                "Only required for new ligands or "
                "those not available in CCP4 monomer library."
            ),
            deactivate_if=JobOptionCondition(
                [("method", "=", "Model-free"), ("method", "=", "Feature enhance")],
            ),
        )
        self.joboptions["sym_group"] = StringJobOption(
            label="Symmetry group",
            default_value="C1",
            help_text=(
                "Impose symmetry to generate symmetrised reference map, "
                "works with model_free or model_partial"
            ),
            deactivate_if=JobOptionCondition(
                [("method", "=", "Model-based")],
            ),
        )
        self.joboptions["use_gpu"] = BooleanJobOption(
            label="Use GPU acceleration?",
            default_value=True,
            help_text="If set to Yes, the job will try to use GPU acceleration.",
            deactivate_if=JobOptionCondition(
                [("method", "=", "Model-based"), ("method", "=", "Hybrid")],
            ),
            jobop_group="Running options",
        )
        self.joboptions["gpu_ids"] = StringJobOption(
            label="Which GPUs to use:",
            default_value="",
            help_text="GPU_IDs to use for gpu acceleration, space separated",
            deactivate_if=JobOptionCondition(
                [
                    ("use_gpu", "=", False),
                    ("method", "=", "Model-based"),
                    ("method", "=", "Hybrid"),
                ]
            ),
            required_if=JobOptionCondition(
                [
                    ("method", "!=", "Model-based"),
                    ("method", "!=", "Hybrid"),
                    ("use_gpu", "=", True),
                ],
                "ALL",
            ),
            jobop_group="Running options",
        )
        self.joboptions["ncpus"] = IntJobOption(
            label="Number of cores",
            default_value=1,
            hard_min=1,
            step_value=1,
            help_text="Increase this to parallelise Feature Enhance run",
            deactivate_if=JobOptionCondition(
                [
                    ("method", "!=", "Feature enhance"),
                ]
            ),
            jobop_group="Running options",
        )
        self.joboptions["use_advanced"] = BooleanJobOption(
            label="Use advanced options?",
            default_value=False,
            help_text=("Recommended for advanced users only"),
            jobop_group="Advanced options",
        )
        self.joboptions["locscale_window"] = IntJobOption(
            label="LocScale window size",
            default_value=0,
            help_text=(
                "Window size in pixels for local scaling of maps. "
                "Suggested value depends on pixel size"
            ),
            deactivate_if=JobOptionCondition([("use_advanced", "=", False)]),
            jobop_group="Advanced options",
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text=(
                "Resolution in angstroms, used while calculating local B-factors"
            ),
            deactivate_if=JobOptionCondition([("use_advanced", "=", False)]),
            jobop_group="Advanced options",
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="Calculated internally by default",
            jobop_group="Advanced options",
            deactivate_if=JobOptionCondition([("use_advanced", "=", False)]),
        )
        self.joboptions["fdr_window"] = IntJobOption(
            label="FDR window size",
            default_value=0,
            help_text=(
                "Box size to use for calculating noise statistics to perform FDR "
                "control. Check that box does not interfere with signal"
            ),
            deactivate_if=JobOptionCondition(
                [("use_advanced", "=", False), ("input_mask", "!=", "")]
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["use_low_context"] = BooleanJobOption(
            label="Use low context model for EMmerNet?",
            default_value=False,
            help_text=(
                "Use a network which is trained on low context data (Atomic Model Maps)"
                ", without any pseudo-model based scaled maps."
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("use_advanced", "=", False),
                    ("method", "=", "Model-based"),
                    ("method", "=", "Hybrid"),
                ]
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["mc_cycles"] = IntJobOption(
            label="Monte-carlo cycles",
            default_value=15,
            help_text=(
                "Number of samples to model network uncertainty using monte carlo "
                "dropout"
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("use_advanced", "=", False),
                    ("method", "!=", "Feature enhance"),
                ]
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["batch_size"] = IntJobOption(
            label="Batch size",
            default_value=8,
            help_text=("Batch size. Typically, a batch size of 8 per GPU (size 12 GB)"),
            deactivate_if=JobOptionCondition(
                [
                    ("use_advanced", "=", False),
                    ("method", "=", "Model-based"),
                    ("method", "=", "Hybrid"),
                ]
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["stride"] = IntJobOption(
            label="Stride",
            default_value=16,
            help_text=(
                "The distance between successive cubes to divide the input map for "
                "prediction"
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("use_advanced", "=", False),
                    ("method", "=", "Model-based"),
                    ("method", "=", "Hybrid"),
                ]
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["model_path"] = ExternalFileJobOption(
            label="Custom trained model:",
            default_value="",
            help_text="Path to a custom trained model (*.h5, *.hdf5)",
            deactivate_if=JobOptionCondition(
                [
                    ("use_advanced", "=", False),
                    ("method", "=", "Model-based"),
                    ("method", "=", "Hybrid"),
                ],
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["pseudomodel_cycles"] = IntJobOption(
            label="Pseudomodal cycles",
            default_value=0,
            help_text=(
                "Number of cycles of gradient descent algorithm to build a "
                "pseudo-atomic model"
            ),
            deactivate_if=JobOptionCondition(
                [("use_advanced", "=", False), ("method", "!=", "Hybrid")]
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["skip_brefine"] = BooleanJobOption(
            label="Skip B-factor refinement?",
            default_value=False,
            help_text="Set to Yes only if input model B-factors are already refined",
            deactivate_if=JobOptionCondition(
                [
                    ("use_advanced", "=", False),
                    ("method", "!=", "Model-based"),
                ]
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["brefinement_cycles"] = IntJobOption(
            label="B-factor refinement cycles",
            default_value=10,
            help_text=("Number of cycles of B-factor refinement using REFMAC"),
            deactivate_if=JobOptionCondition(
                [
                    ("use_advanced", "=", False),
                    ("method", "=", "Model-free"),
                    ("method", "=", "Feature enhance"),
                    ("skip_brefine", "=", True),
                ]
            ),
            jobop_group="Advanced options",
        )
        self.get_runtab_options(mpi=True)
        # deactivate for Feaure enhance
        self.joboptions["nr_mpi"].deactivate_if = JobOptionCondition(
            [("method", "=", "Feature enhance")]
        )
        self.joboptions["mpi_command"].deactivate_if = JobOptionCondition(
            [("method", "=", "Feature enhance"), ("nr_mpi", "=", 1)]
        )

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "locscale_output.mrc", NODE_DENSITYMAP, ["locscale", "localsharpen"]
        )
        self.add_output_node(
            os.path.join("processing_files", "locscale.log"),
            NODE_LOGFILE,
            ["locscale", "report"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        # Run LocScale using an existing atomic model:
        # locscale run_locscale -em path/to/emmap.mrc -mc path/to/model.pdb -res 3
        #       -v -o model_based_locscale.mrc
        #
        # Run LocScale without atomic model
        # locscale run_locscale -em path/to/emmap.mrc -res 3
        #       -v -o model_based_locscale.mrc

        # Command w/ and w/out mpi
        command: List[Union[str, float, int]] = []
        method = self.joboptions["method"].get_string()

        nr_mpi = self.get_nr_mpi()
        if nr_mpi > 1 and method != "Feature enhance":
            command += self.get_mpi_command()
        if self.jobinfo.programs[0].exe_path is not None:
            command += [self.jobinfo.programs[0].exe_path]
            # This is a temporary work-around to ensure
            # mpirun used corresponds to locscale conda env
            os.environ["PATH"] = (
                os.path.dirname(self.jobinfo.programs[0].exe_path)
                + ":"
                + os.environ["PATH"]
            )
        else:
            command += [self.jobinfo.programs[0].command]

        # Get parameters
        input_hmap1 = self.joboptions["input_halfmap1"].get_string()
        input_hmap2 = self.joboptions["input_halfmap2"].get_string()
        input_model = self.joboptions["input_model"].get_string()
        gpu_ids = self.joboptions["gpu_ids"].get_string()
        use_advanced = self.joboptions["use_advanced"].get_boolean()
        # # Input Map
        # # must be copied into job dir because can't control where output is written
        copy_command: List[Union[str, float, int]] = [
            "cp",
            os.path.relpath(input_hmap1, self.working_dir),
            os.path.relpath(input_hmap2, self.working_dir),
            ".",
        ]
        # feature enhance mode
        if method == "Feature enhance":
            command += ["feature_enhance"]
            command += ["-np", self.joboptions["ncpus"].get_number()]
        # inputs
        command += [
            "--halfmap_paths",
            os.path.basename(input_hmap1),
            os.path.basename(input_hmap2),
        ]
        # mode specific
        sym_group = self.joboptions["sym_group"].get_string()
        if method != "Model-based" and sym_group:
            command += ["-sym", sym_group]
        ligand_cif = self.joboptions["ligand_cif"].get_string()
        if method not in ["Model-based", "Hybrid"]:  # emmernet, feature enhance
            if self.joboptions["use_gpu"].get_boolean():
                command += ["-gpus", gpu_ids]
        else:
            command += ["-mc", os.path.relpath(input_model, self.working_dir)]
            if ligand_cif:
                command += ["--cif_info", os.path.relpath(ligand_cif, self.working_dir)]
            if method == "Hybrid":
                command += ["--complete_model"]
        # advanced options
        if use_advanced:
            advanced_options = self.gather_advanced_options(method=method)
            command += advanced_options

        # set ouput
        command += ["-o", "locscale_output.mrc"]

        # Set MPI suffix
        if nr_mpi > 1 and method != "Feature enhance":
            command += ["-mpi"]
        commands = [copy_command, command]
        return [PipelinerCommand(x) for x in commands]

    def gather_advanced_options(self, method: str) -> List[Union[float, int, str]]:
        option_list: List[Union[float, int, str]] = []
        resolution = self.joboptions["resolution"].get_number()
        if resolution != -1:
            option_list += ["-res", str(resolution)]
        locscale_window = self.joboptions["locscale_window"].get_number()
        if locscale_window:
            option_list += ["--window_size", locscale_window]
        input_mask = self.joboptions["input_mask"].get_string()
        if input_mask != "":
            option_list += ["-ma", os.path.relpath(input_mask, self.working_dir)]
        fdr_window = self.joboptions["fdr_window"].get_number()
        if fdr_window:
            option_list += ["--fdr_window_size", fdr_window]
        skipb = False
        if method in ["Model-based", "Hybrid"]:
            if method == "Model-based":
                skipb = self.joboptions["skip_brefine"].get_boolean()
                if skipb:
                    option_list += ["--skip_refine"]
            elif method == "Hybrid":
                pseudomodel_cycles = self.joboptions["pseudomodel_cycles"].get_number()
                if pseudomodel_cycles:
                    option_list += ["--total_iterations", pseudomodel_cycles]
            brefinement_cycles = self.joboptions["brefinement_cycles"].get_number()
            if not skipb and brefinement_cycles:
                option_list += [
                    "--refmac_iterations",
                    brefinement_cycles,
                ]
        elif method in ["Model-free", "Feature enhance"]:
            model_path = self.joboptions["model_path"].get_string()
            if model_path:
                option_list += ["-model_path", model_path]
            if self.joboptions["use_low_context"].get_boolean():
                option_list += ["--use_low_context_model"]
            batch_size = self.joboptions["batch_size"].get_number()
            if batch_size:
                option_list += ["--batch_size", batch_size]
            stride = self.joboptions["stride"].get_number()
            if stride and method != "Model-free":
                option_list += ["--stride", stride]
            if method == "Feature enhance":
                mc_cycles = self.joboptions["mc_cycles"].get_number()
                if mc_cycles:
                    option_list += ["--monte_carlo_iterations", mc_cycles]
        return option_list

    def gather_metadata(self) -> Dict[str, Any]:
        metadata_dict: Dict[str, Any] = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for i, line in enumerate(outlines):
            if "Maximum Distance Dn" in line:
                metadata_dict["MaximumDistanceDn"] = {}
                metadata_dict["MaximumDistanceDn"]["Dn"] = float(
                    line.split()[8].strip(",")
                )
                metadata_dict["MaximumDistanceDn"]["InTail"] = float(
                    line.split()[11].strip(".")
                )
                metadata_dict["MaximumDistanceDn"]["SampleSize"] = int(line.split()[15])
            if "Anderson-Darling" in line:
                metadata_dict["AndersonDarlingTest"] = {}
                metadata_dict["AndersonDarlingTest"]["TestSummary"] = float(
                    line.split()[3].strip(".")
                )
                metadata_dict["AndersonDarlingTest"]["PValue"] = float(
                    line.split()[5].strip(".")
                )
                metadata_dict["AndersonDarlingTest"]["SampleSize"] = int(
                    line.split()[9]
                )
            if "Estimated noise statistics" in line:
                metadata_dict["EstimatedNoiseStatistics"] = {}
                metadata_dict["EstimatedNoiseStatistics"]["Mean"] = float(
                    line.split()[4]
                )
                metadata_dict["EstimatedNoiseStatistics"]["Variance"] = float(
                    line.split()[7]
                )
            if "Calculated map threshold" in line:
                metadata_dict["CalculatedMapThreshold"] = float(line.split()[3])
                metadata_dict["FDR"] = float(line.split()[8])
            if "MRC file format" in line:
                metadata_dict["MRCFileProperties"] = {}
                metadata_dict["MRCFileProperties"]["VoxelSize"] = [
                    float(outlines[i + 1].split()[2].replace("(", "").replace(",", "")),
                    float(outlines[i + 1].split()[3].strip(",")),
                    float(outlines[i + 1].split()[4].strip(")")),
                ]
                metadata_dict["MRCFileProperties"]["Origin"] = [
                    float(outlines[i + 2].split()[1].replace("(", "").replace(",", "")),
                    float(outlines[i + 2].split()[2].strip(",")),
                    float(outlines[i + 2].split()[3].strip(")")),
                ]

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        # Get input model if available
        input_model = self.joboptions["input_model"].get_string()
        if input_model == "":
            model = None
        else:
            model = [input_model]
        # Get input and output map
        input_maps = []
        input_hmap1 = self.joboptions["input_halfmap1"].get_string()
        input_hmap2 = self.joboptions["input_halfmap2"].get_string()
        input_maps += [input_hmap1, input_hmap2]
        output_map = os.path.join(self.output_dir, "locscale_output.mrc")
        maps = input_maps + [output_map]
        if len(maps) == 2:
            maps_opacity = [0.5, 1.0]
        else:
            maps_opacity = [0.5, 0.7, 1.0]
        return [
            make_mrcs_central_slices_montage(
                in_files={map_: "" for map_ in maps},
                output_dir=self.output_dir,
            ),
            make_map_model_thumb_and_display(
                maps=maps,
                maps_opacity=maps_opacity,
                models=model,
                title="LocScale local sharpening",
                outputdir=self.output_dir,
            ),
        ]
