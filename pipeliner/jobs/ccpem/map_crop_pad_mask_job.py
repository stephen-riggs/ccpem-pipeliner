#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from pathlib import Path
from typing import List, Sequence, Union

from pipeliner.pipeliner_job import PipelinerJob, PipelinerCommand
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    InputNodeJobOption,
    MultipleChoiceJobOption,
    files_exts,
    JobOptionCondition,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    JobOptionValidationResult,
)
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    make_mrcs_central_slices_montage,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_MASK3D
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject


class CropPadMap(PipelinerJob):
    PROCESS_NAME = "ccpem_utils.map_utilities.crop_pad_mask_map"
    OUT_DIR = "CropPadMaskMap"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "CCPEM Utils Crop, Pad or Mask Map"
        self.jobinfo.short_desc = "Map crop, pad or mask util"
        self.jobinfo.long_desc = (
            "Uses CCP-EM Utils library to apply a mask, crop or pad maps. Includes"
            " options to crop based on dimensions, input mask or a contour threshold."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.programs = []
        self.jobinfo.references = []
        self.jobinfo.documentation = ""

        self.joboptions["input_map"] = MultiInputNodeJobOption(
            label="Map to process",
            node_type=NODE_DENSITYMAP,
            pattern=files_exts("Density Map", [".mrc", ".map", ".ccp4"]),
            default_value="",
            help_text="Input map to process. Choose operations below",
            is_required=True,
        )
        self.joboptions["crop_map"] = BooleanJobOption(
            label="Crop map?",
            default_value=False,
            help_text="Crop based on dimensions, input mask or map contour threshold",
            deactivate_if=JobOptionCondition([("pad_map", "=", True)]),
        )
        self.joboptions["pad_map"] = BooleanJobOption(
            label="Pad map?",
            default_value=False,
            help_text="Pad based on dimensions",
            deactivate_if=JobOptionCondition([("crop_map", "=", True)]),
        )
        self.joboptions["apply_mask"] = BooleanJobOption(
            label="Apply input mask?",
            default_value=False,
            help_text=(
                "Mask input map using input mask. Both input map and mask should have"
                " same dimensions"
            ),
            deactivate_if=JobOptionCondition([("threshold_map", "=", True)]),
        )
        self.joboptions["threshold_map"] = BooleanJobOption(
            label="Threshold map?",
            default_value=False,
            help_text="Threshold input map by given contour level",
            deactivate_if=JobOptionCondition([("apply_mask", "=", True)]),
        )
        self.joboptions["crop_mode"] = MultipleChoiceJobOption(
            label="Crop based on",
            choices=["dimensions", "input_mask", "input_contour"],
            default_value_index=0,
            help_text=(
                "If dimensions, input number of voxels to remove from either sides "
                "along X, Y and Z. If input_mask, provide a binary mask to crop the "
                "map to. A padding can be added to the mask extent ('Add padding'). "
                "If input_contour, provide a contour to threshold the map. A padding "
                "can be added to the map edges before cropping ('Add padding')."
            ),
            deactivate_if=JobOptionCondition([("crop_map", "=", False)]),
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("3D mask", [".mrc", ".ccp4", ".map"]),
            help_text=(
                "The mask to apply to the map and/or determine the extent for"
                " cropping"
            ),
            required_if=JobOptionCondition(
                [("apply_mask", "=", True), ("crop_mode", "=", "input_mask")]
            ),
            deactivate_if=JobOptionCondition(
                [("apply_mask", "=", False), ("crop_map", "=", False)], "ALL"
            ),
        )
        self.joboptions["thr_mask"] = FloatJobOption(
            label="Threshold mask at",
            default_value=0,
            step_value=0.01,
            help_text=("The mask will be thresholded at this value before cropping"),
            deactivate_if=JobOptionCondition(
                [("crop_mode", "=", "input_contour"), ("crop_mode", "=", "dimensions")],
            ),
        )
        self.joboptions["contourlevel"] = FloatJobOption(
            label="Contour Level",
            default_value=-100,
            step_value=0.01,
            help_text=(
                "Contour level to threshold map OR determine the extent for cropping."
                " If Crop map, slices with all values less than the contour are "
                "cropped along each axis. If Threshold and Crop, map is thresholded at "
                "selected contour and slices with zeros in the contoured map are "
                "then cropped along each axis."
            ),
            required_if=JobOptionCondition(
                [("threshold_map", "=", True), ("crop_mode", "=", "input_contour")]
            ),
            deactivate_if=JobOptionCondition(
                [("threshold_map", "=", False), ("crop_mode", "!=", "input_contour")],
                "ALL",
            ),
        )
        self.joboptions["croppad_x"] = IntJobOption(
            label="Crop/Pad along X",
            default_value=0,
            hard_min=0,
            help_text=(
                "The input number of slices will be added or removed from the start and"
                " end of the X axis. This field is ignored if 'Crop based on' is not "
                "'dimensions'."
            ),
            deactivate_if=JobOptionCondition(
                [("crop_map", "=", False), ("pad_map", "=", False)], "ALL"
            ),
        )
        self.joboptions["croppad_y"] = IntJobOption(
            label="Crop/Pad along Y",
            default_value=0,
            hard_min=0,
            help_text=(
                "The input number of slices will be added or removed from the start and"
                " end of the Y axis. This field is ignored if 'Crop based on' is not "
                "'dimensions'."
            ),
            deactivate_if=JobOptionCondition(
                [("crop_map", "=", False), ("pad_map", "=", False)], "ALL"
            ),
        )
        self.joboptions["croppad_z"] = IntJobOption(
            label="Crop/Pad along Z",
            default_value=0,
            hard_min=0,
            help_text=(
                "The input number of slices will be added or removed from the start and"
                " end of the Z axis. This field is ignored if 'Crop based on' is not "
                "'dimensions'."
            ),
            deactivate_if=JobOptionCondition(
                [("crop_map", "=", False), ("pad_map", "=", False)], "ALL"
            ),
        )
        self.joboptions["add_padding"] = IntJobOption(
            label="Add padding",
            default_value=10,
            hard_min=0,
            help_text=(
                "If Crop based on 'input_mask' or 'input_contour', add padding to the"
                " map edges. By default 10 slices are added to either sides along all"
                " axes."
            ),
            deactivate_if=JobOptionCondition(
                [("crop_map", "=", False), ("crop_mode", "=", "dimensions")]
            ),
        )
        self.joboptions["out_cubic"] = BooleanJobOption(
            label="Output cubic map?",
            default_value=False,
            help_text="Output cubic map based on maximum of required dimensions",
            deactivate_if=JobOptionCondition(
                [("crop_map", "=", False), ("crop_mode", "=", "dimensions")]
            ),
        )
        self.joboptions["fill_padding"] = MultipleChoiceJobOption(
            label="Fill padding with",
            choices=["minimum", "zero", "input_value"],
            default_value_index=0,
            help_text=(
                "By default, the minimum value in the input map is used to fill."
                " Otherwise it can be filled with zeros or any input value."
            ),
            deactivate_if=JobOptionCondition([("pad_map", "=", False)]),
        )
        self.joboptions["fill_value"] = FloatJobOption(
            label="Padding fill value",
            default_value=0.0,
            step_value=0.01,
            help_text=("This value will be used to fill the padding."),
            required_if=JobOptionCondition([("fill_padding", "=", "input_value")]),
            deactivate_if=JobOptionCondition(
                [
                    ("pad_map", "=", False),
                    ("fill_padding", "=", "minimum"),
                    ("fill_padding", "=", "zero"),
                ]
            ),
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        crop_map = self.joboptions["crop_map"].get_boolean()
        pad_map = self.joboptions["pad_map"].get_boolean()
        apply_mask = self.joboptions["apply_mask"].get_boolean()
        threshold_map = self.joboptions["threshold_map"].get_boolean()
        if not crop_map and not pad_map and not apply_mask and not threshold_map:
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["crop_map"],
                        self.joboptions["pad_map"],
                        self.joboptions["apply_mask"],
                        self.joboptions["threshold_map"],
                    ],
                    message=(
                        "Select atleast one of the operations from (crop or pad),"
                        " and/or (mask or threshold)"
                    ),
                )
            )
        crop_mode = self.joboptions["crop_mode"].get_string()
        input_contour = self.joboptions["contourlevel"].get_number()
        if (threshold_map or crop_mode == "input_contour") and input_contour == -100:
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["contourlevel"],
                    ],
                    message=("Input a contour level for thresholding and/or cropping"),
                )
            )
        if (
            ((crop_map and crop_mode == "dimensions") or pad_map)
            and self.joboptions["croppad_x"].get_number() == 0
            and self.joboptions["croppad_y"].get_number() == 0
            and self.joboptions["croppad_z"].get_number() == 0
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[
                        self.joboptions["croppad_x"],
                        self.joboptions["croppad_y"],
                        self.joboptions["croppad_z"],
                    ],
                    message="Input non-zero dimension(s) for cropping or padding. ",
                )
            )
        if (
            crop_map
            and crop_mode != "dimensions"
            and (
                self.joboptions["croppad_x"].get_number() != 0
                or self.joboptions["croppad_y"].get_number() != 0
                or self.joboptions["croppad_z"].get_number() != 0
            )
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[
                        self.joboptions["croppad_x"],
                        self.joboptions["croppad_y"],
                        self.joboptions["croppad_z"],
                    ],
                    message=(
                        "The crop dimension(s) will be ignored as 'Crop based on'. "
                        "is not 'dimensions'. "
                    ),
                )
            )
        return errors

    def get_output_map_name(self, outname: str) -> str:
        crop_map = self.joboptions["crop_map"].get_boolean()
        pad_map = self.joboptions["pad_map"].get_boolean()
        apply_mask = self.joboptions["apply_mask"].get_boolean()
        threshold_map = self.joboptions["threshold_map"].get_boolean()
        suffix = ""
        if apply_mask:
            suffix += "_masked"
        elif threshold_map:
            suffix += "_contoured"
        if crop_map:
            suffix += "_cropped"
        elif pad_map:
            suffix += "_padded"
        out_stem = Path(outname).stem
        out_stem += suffix
        map_out = out_stem + ".mrc"
        return map_out

    def create_output_nodes(self) -> None:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for out_map in map_names.values():
            self.add_output_node(
                self.get_output_map_name(out_map),
                NODE_DENSITYMAP,
                ["ccpem-utils", "crop_pad_map"],
            )

    def make_command(self, input_map: str, outmap: str) -> PipelinerCommand:
        # Run in the job output directory
        self.working_dir = self.output_dir
        command: List[Union[int, float, str]] = [
            "python3",
            get_job_script("crop_pad_map.py"),
        ]

        # Get parameters
        command += ["-m", os.path.relpath(input_map, self.working_dir)]
        crop_map = self.joboptions["crop_map"].get_boolean()
        pad_map = self.joboptions["pad_map"].get_boolean()
        apply_mask = self.joboptions["apply_mask"].get_boolean()
        threshold_map = self.joboptions["threshold_map"].get_boolean()
        input_mask = self.joboptions["input_mask"].get_string()
        if input_mask:
            input_mask = os.path.relpath(input_mask, self.working_dir)
        input_contour = self.joboptions["contourlevel"].get_number()
        thr_mask = self.joboptions["thr_mask"].get_number()
        crop_mode = self.joboptions["crop_mode"].get_string()
        add_padding = self.joboptions["add_padding"].get_number()
        if apply_mask:
            command += [
                "--mask",
                "--maskfile",
                input_mask,
            ]
        elif threshold_map:
            command += [
                "--contour",
                "--threshold",
                input_contour,
            ]
        if crop_map:
            command += ["--crop"]
            if crop_mode == "dimensions":
                command += ["--cpx", self.joboptions["croppad_x"].get_number()]
                command += ["--cpy", self.joboptions["croppad_y"].get_number()]
                command += ["--cpz", self.joboptions["croppad_z"].get_number()]
            elif crop_mode == "input_mask":
                if not apply_mask:
                    command += ["--maskfile", input_mask]
                if thr_mask:
                    command += ["--mask_threshold", thr_mask]
                command += ["--extend", add_padding]
            elif crop_mode == "input_contour":
                if not threshold_map:
                    command += ["--threshold", input_contour]
                command += ["--extend", add_padding]
            out_cubic = self.joboptions["out_cubic"].get_boolean()
            if out_cubic:
                command += ["--out_cubic"]
        elif pad_map:
            command += ["--pad"]
            command += ["--cpx", self.joboptions["croppad_x"].get_number()]
            command += ["--cpy", self.joboptions["croppad_y"].get_number()]
            command += ["--cpz", self.joboptions["croppad_z"].get_number()]
            fill_padding = self.joboptions["fill_padding"].get_string()
            fill_value = self.joboptions["fill_value"].get_number()
            if fill_padding == "input_value":
                command += ["--fill_pad", fill_value]

        # Set output file
        command += ["-ofile", self.get_output_map_name(outmap)]

        return PipelinerCommand(command)

    def get_commands(self) -> List[PipelinerCommand]:
        commands = []
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for in_map, out_map in map_names.items():
            commands.append(self.make_command(in_map, out_map))
        return commands

    def make_results(self, input_map: str, outfile: str) -> ResultsDisplayObject:
        output_map = self.get_output_map_name(outfile)
        output_map = os.path.join(self.output_dir, output_map)
        return make_map_model_thumb_and_display(
            maps=[input_map, output_map],
            outputdir=self.output_dir,
        )

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        outputs = {}
        for outmap in map_names.values():
            output_map = self.get_output_map_name(outmap)
            outputs[os.path.join(self.output_dir, output_map)] = ""
        results: List[ResultsDisplayObject] = [
            make_mrcs_central_slices_montage(
                in_files=outputs,
                output_dir=self.output_dir,
            )
        ]
        for map_pair in map_names.items():
            results.append(self.make_results(*map_pair))
        return results
