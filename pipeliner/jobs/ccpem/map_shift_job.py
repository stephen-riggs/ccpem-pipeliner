import os
from typing import List
from pipeliner.pipeliner_job import PipelinerJob, PipelinerCommand
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    InputNodeJobOption,
    files_exts,
    JobOptionCondition,
    JobOptionValidationResult,
    MultipleChoiceJobOption,
)
from pipeliner.utils import get_job_script
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
)
from pipeliner.results_display_objects import (
    ResultsDisplayObject,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_MASK3D,
)
import numpy as np


class MapShift(PipelinerJob):
    PROCESS_NAME = "ccpem_utils.map_utilities.shift_origin_nstart"
    OUT_DIR = "MapShift"

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Map shift"
        self.jobinfo.short_desc = "Edit map origin or nstart header records"
        self.jobinfo.long_desc = (
            "Set map origin or nstart record based on a reference map or using a "
            "given set of origin and/or nstart coordinates. The header records of all "
            "associated input maps and masks will be set based on the given parameters."
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Burnley T",
                    "Palmer C",
                    "Winn M",
                ],
                title="Recent developments in the CCP-EM software suite",
                journal="Acta Cryst. D",
                year="2017",
                volume="73",
                issue="1",
                pages="469-477",
                doi="10.1107/S2059798317007859",
            ),
        ]

        self.joboptions["input_maps"] = MultiInputNodeJobOption(
            label="Input associated maps",
            node_type=NODE_DENSITYMAP,
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            default_value="",
            help_text=(
                "Add maps whose origin/nstart are to be set. The header records "
                "all associated maps will be set based on the input parameters."
            ),
            is_required=True,
        )
        self.joboptions["input_masks"] = MultiInputNodeJobOption(
            label="Input associated masks",
            node_type=NODE_MASK3D,
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            default_value="",
            help_text=(
                "Add masks whose origin/nstart are to be set. The header records "
                "all associated masks will be set based on the input parameters."
            ),
            is_required=False,
        )
        #
        self.joboptions["origin_or_nstart"] = MultipleChoiceJobOption(
            label="Header record to set",
            choices=["origin", "nstart", "both"],
            default_value_index=0,
            help_text=(
                "Select header record to use and set: origin only, nstart only or both."
            ),
        )
        #
        self.joboptions["reference_map"] = InputNodeJobOption(
            label="Input reference map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text=(
                "The map to be used as reference to extract origin and/or nstart "
                "records. The inputs maps/masks will be shifted to align with the "
                "reference. "
            ),
            is_required=False,
        )
        self.joboptions["origin_x"] = FloatJobOption(
            label="Origin coordinate along X",
            default_value=0,
            help_text=(
                "If reference map is not input and origin has to be set, then the "
                "map origin X is set to this value"
            ),
            deactivate_if=JobOptionCondition(
                [("reference_map", "!=", ""), ("origin_or_nstart", "=", "nstart")]
            ),
        )
        self.joboptions["origin_y"] = FloatJobOption(
            label="Origin coordinate along Y",
            default_value=0,
            help_text=(
                "If reference map is not input and origin has to be set, then the "
                "map origin Y is set to this value"
            ),
            deactivate_if=JobOptionCondition(
                [("reference_map", "!=", ""), ("origin_or_nstart", "=", "nstart")]
            ),
        )
        self.joboptions["origin_z"] = FloatJobOption(
            label="Origin coordinate along Z",
            default_value=0,
            help_text=(
                "If reference map is not input and origin has to be set, then the "
                "map origin Z is set to this value"
            ),
            deactivate_if=JobOptionCondition(
                [("reference_map", "!=", ""), ("origin_or_nstart", "=", "nstart")]
            ),
        )
        self.joboptions["nstart_x"] = IntJobOption(
            label="Nstart coordinate along X",
            default_value=0,
            help_text=(
                "If reference map is not input and nstart has to be set, then the "
                "map nstart X is set to this value"
            ),
            deactivate_if=JobOptionCondition(
                [("reference_map", "!=", ""), ("origin_or_nstart", "=", "origin")]
            ),
        )
        self.joboptions["nstart_y"] = IntJobOption(
            label="Nstart coordinate along Y",
            default_value=0,
            help_text=(
                "If reference map is not input and nstart has to be set, then the "
                "map nstart Y is set to this value"
            ),
            deactivate_if=JobOptionCondition(
                [("reference_map", "!=", ""), ("origin_or_nstart", "=", "origin")]
            ),
        )
        self.joboptions["nstart_z"] = IntJobOption(
            label="Nstart coordinate along Z",
            default_value=0,
            help_text=(
                "If reference map is not input and nstart has to be set, then the "
                "map nstart Z is set to this value"
            ),
            deactivate_if=JobOptionCondition(
                [("reference_map", "!=", ""), ("origin_or_nstart", "=", "origin")]
            ),
        )
        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        if (
            not self.joboptions["reference_map"].get_string()
            and not self.joboptions["origin_x"].get_number()
            and not self.joboptions["origin_y"].get_number()
            and not self.joboptions["origin_z"].get_number()
            and not self.joboptions["nstart_x"].get_number()
            and not self.joboptions["nstart_y"].get_number()
            and not self.joboptions["nstart_z"].get_number()
        ):
            errors.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[self.joboptions["reference_map"]],
                    message=(
                        "Ensure either a reference map or correct origin "
                        "and/or nstart coordinates are input."
                    ),
                )
            )
        return errors

    @staticmethod
    def get_output_name(mapid) -> str:
        return mapid + "_shifted.mrc"

    def create_output_nodes(self) -> None:
        # Get options needed to decide which output nodes will be made
        input_map_list = self.joboptions["input_maps"].get_list()
        unique_map_ids = self.joboptions["input_maps"].get_basename_mapping()
        input_mask_list = self.joboptions["input_masks"].get_list()
        unique_mask_ids = self.joboptions["input_masks"].get_basename_mapping()
        origin_or_nstart = self.joboptions["origin_or_nstart"].get_string()
        if origin_or_nstart == "both":
            output_keyword = "origin_nstart"
        else:
            output_keyword = origin_or_nstart
        for input_map in input_map_list:
            if unique_map_ids:
                mapid = os.path.splitext(unique_map_ids[input_map])[0]
            else:
                mapid = os.path.splitext(os.path.basename(input_map))[0]
            shifted_map = self.get_output_name(mapid)
            self.add_output_node(
                shifted_map, NODE_DENSITYMAP, ["shifted_map", output_keyword]
            )
        for input_mask in input_mask_list:
            if unique_mask_ids:
                maskid = os.path.splitext(unique_mask_ids[input_mask])[0]
            else:
                maskid = os.path.splitext(os.path.basename(input_mask))[0]
            shifted_mask = self.get_output_name(maskid)
            self.add_output_node(
                shifted_mask, NODE_MASK3D, ["shifted_mask", output_keyword]
            )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        commands = []
        # Get parameters
        input_map_list = self.joboptions["input_maps"].get_list()
        input_mask_list = self.joboptions["input_masks"].get_list()
        origin_or_nstart = self.joboptions["origin_or_nstart"].get_string()
        reference_map = self.joboptions["reference_map"].get_string()
        origin_x = self.joboptions["origin_x"].get_number()
        origin_y = self.joboptions["origin_y"].get_number()
        origin_z = self.joboptions["origin_z"].get_number()
        nstart_x = self.joboptions["nstart_x"].get_number()
        nstart_y = self.joboptions["nstart_y"].get_number()
        nstart_z = self.joboptions["nstart_z"].get_number()
        unique_map_ids = self.joboptions["input_maps"].get_basename_mapping()
        unique_mask_ids = self.joboptions["input_masks"].get_basename_mapping()
        list_all_maps = [input_map_list, input_mask_list]
        list_all_ids = [unique_map_ids, unique_mask_ids]
        for m in range(len(list_all_maps)):
            lists_map = list_all_maps[m]
            for input_map in lists_map:
                mapid = os.path.splitext(list_all_ids[m][input_map])[0]
                input_map = os.path.relpath(input_map, self.working_dir)
                shift_command = ["python3", get_job_script("edit_map_origin_nstart.py")]
                shift_command += ["--map", input_map]
                shift_command += ["--use_record", origin_or_nstart]
                if reference_map:
                    shift_command += [
                        "--refmap",
                        os.path.relpath(reference_map, self.working_dir),
                    ]
                else:
                    if origin_or_nstart in ["origin", "both"]:
                        shift_command += [
                            "--ox",
                            str(origin_x),
                            "--oy",
                            str(origin_y),
                            "--oz",
                            str(origin_z),
                        ]
                    if origin_or_nstart in ["nstart", "both"]:
                        shift_command += [
                            "--nsx",
                            str(nstart_x),
                            "--nsy",
                            str(nstart_y),
                            "--nsz",
                            str(nstart_z),
                        ]
                shift_command += ["--ofile", self.get_output_name(mapid)]
                commands.append(shift_command)
        return [PipelinerCommand(command) for command in commands]

    def create_results_display(
        self,
    ) -> List[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        input_map_list = self.joboptions["input_maps"].get_list()
        input_mask_list = self.joboptions["input_masks"].get_list()
        unique_map_ids = self.joboptions["input_maps"].get_basename_mapping()
        unique_mask_ids = self.joboptions["input_masks"].get_basename_mapping()
        reference_map = self.joboptions["reference_map"].get_string()
        list_all_maps = [input_map_list, input_mask_list]
        list_all_ids = [unique_map_ids, unique_mask_ids]
        list_shifted_maps = []
        for m in range(len(list_all_maps)):
            lists_map = list_all_maps[m]
            for input_map in lists_map:
                mapid = os.path.splitext(list_all_ids[m][input_map])[0]
                shifted_map = os.path.join(self.output_dir, self.get_output_name(mapid))
                list_shifted_maps.append(shifted_map)
                if reference_map:
                    display_objects.append(
                        make_map_model_thumb_and_display(
                            maps=[reference_map, shifted_map],
                            maps_opacity=[0.5, 1.0],
                            outputdir=self.output_dir,
                        )
                    )
        if not reference_map:
            display_objects.append(
                make_map_model_thumb_and_display(
                    maps=list_shifted_maps,
                    maps_opacity=list(np.linspace(0.3, 1.0, len(list_shifted_maps))),
                    outputdir=self.output_dir,
                )
            )
        return display_objects
