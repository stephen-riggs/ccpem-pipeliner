import os
import re
import shutil
import xml.etree.ElementTree as ET
from collections import OrderedDict
from typing import Dict, List, Union, Optional
from ccpem_utils.map.mrcfile_utils import check_origin_zero
from pipeliner.utils import get_job_script
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    BooleanJobOption,
    StringJobOption,
    # MultiStringJobOption,
    FloatJobOption,
    IntJobOption,
    InputNodeJobOption,
    files_exts,
    JobOptionCondition,
    JobOptionValidationResult,
    MultipleChoiceJobOption,
)
from pipeliner.pipeliner_job import Ref

from pipeliner.display_tools import (
    create_results_display_object,
)
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.nodes import NODE_ATOMCOORDS, NODE_DENSITYMAP, NODE_PROCESSDATA

# TODO : is finding the database path really needed?
ccp4_path = shutil.which("ccp4-python")
privateer_database_path: str = ""
if ccp4_path:
    privateer_database_path = os.path.join(
        ccp4_path.split("/bin")[0], "lib/data/privateer_database.json"
    )


class PrivateerValidate(PipelinerJob):
    PROCESS_NAME = "privateer.atomic_model_validation.glycans"
    OUT_DIR = "Privateer"
    job_references = [
        Ref(
            authors=[
                "Agirre J",
                "Iglesias-Fernandez J",
                "Rovira C",
                "Davies GJ",
                "Wilson KS",
                "Cowtan KD",
            ],
            title=(
                "Privateer: software for the conformational validation of carbohydrate "
                "structures"
            ),
            journal="NSMB",
            year="2015",
            volume="22",
            pages="833-834",
            doi="10.1038/nsmb.3115",
        ),
        Ref(
            authors=[
                "Dialpuri JS",
                "Bagdonas H",
                "Atanasova M",
                "Schofield LC",
                "Hekkelman ML",
                "Joosten RP",
                "Agirre J",
            ],
            title=(
                "Analysis and validation of overall N-glycan conformation in Privateer"
            ),
            journal="Acta Cryst. D",
            year="2023",
            volume="79",
            pages="462-472",
            doi="10.1107/S2059798323003510",
        ),
    ]
    deactivation_conditions: Dict[str, JobOptionCondition] = {
        "closestmatch": JobOptionCondition([("glytoucan", "=", False)]),
        "allpermutations": JobOptionCondition(
            [("glytoucan", "=", False), ("closestmatch", "=", False)]
        ),
        "undefinedsugar_code": JobOptionCondition([("undefinedsugar", "=", False)]),
        "input_anomer": JobOptionCondition([("undefinedsugar", "=", False)]),
        "input_handedness": JobOptionCondition([("undefinedsugar", "=", False)]),
        "input_ring_conformation": JobOptionCondition([("undefinedsugar", "=", False)]),
        "input_conformation_pyranose": JobOptionCondition(
            [
                ("undefinedsugar", "=", False),
                ("input_ring_conformation", "=", "furanose"),
            ]
        ),
        "input_conformation_furanose": JobOptionCondition(
            [
                ("undefinedsugar", "=", False),
                ("input_ring_conformation", "=", "pyranose"),
            ]
        ),
        "ring_oxygen": JobOptionCondition([("undefinedsugar", "=", False)]),
        "ring_C1": JobOptionCondition([("undefinedsugar", "=", False)]),
        "ring_C2": JobOptionCondition([("undefinedsugar", "=", False)]),
        "ring_C3": JobOptionCondition([("undefinedsugar", "=", False)]),
        "ring_C4": JobOptionCondition([("undefinedsugar", "=", False)]),
        "ring_C5": JobOptionCondition([("undefinedsugar", "=", False)]),
    }

    def __init__(self):
        super().__init__()
        self.jobinfo.display_name = "Privateer"
        self.jobinfo.short_desc = "Validate Glycan conformation"
        self.jobinfo.long_desc = (
            "Evaluate and improve the atomic structures of carbohydrates, "
            "including N-glycans"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.programs = [
            ExternalProgram(command="privateer"),
        ]
        self.jobinfo.references = PrivateerValidate.job_references
        self.jobinfo.documentation = "https://privateer.york.ac.uk/"

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text="Add model to evaluate",
            is_required=True,
        )
        # OTHER OPTIONS BASED ON METRIC SELECTION
        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text="The input map to evaluate the model against",
            is_required=True,
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            hard_min=0.1,
            suggested_min=0.5,
            suggested_max=20,
            step_value=0.1,
            help_text="Map resolution in angstroms",
            is_required=True,
        )
        self.joboptions["mask_radius"] = FloatJobOption(
            label="Mask radius",
            default_value=2.5,
            step_value=0.01,
            help_text=(
                "Provide a radius for the calculation of the mask around the target "
                "sugar. Relevant in calculation of Fo-Fc map"
            ),
            hard_min=1.0,
        )
        self.joboptions["expression_system_mode"] = MultipleChoiceJobOption(
            label="Validate against:",
            help_text=(
                "Choose the expression system to validate against. Privateer will "
                "carry out basic checks around GlcNAc-Man and GlcNAc-GlcNAc linkages. "
                "Option 'undefined' will ignore the basic checks.'"
            ),
            choices=[
                "undefined",
                "bacterial",
                "fungal",
                "yeast",
                "plant",
                "insect",
                "mammalian",
                "human",
            ],
            default_value_index=0,
        )
        # Glytoucan/Glyconnect search
        self.joboptions["glytoucan"] = BooleanJobOption(
            label="Check in Glytoucan/Glyconnect?",
            default_value=True,
            help_text="Check if glycan is deposited in GlyToucan and GlyConnect "
            "databases",
            jobop_group="Glytoucan/Glyconnect",
        )
        self.joboptions["closestmatch"] = BooleanJobOption(
            label="Find closest match?",
            default_value=False,
            help_text="Look for closest match on GlyConnect database if input glycan "
            "is not found",
            deactivate_if=self.deactivation_conditions["closestmatch"],
            jobop_group="Glytoucan/Glyconnect",
        )
        self.joboptions["allpermutations"] = BooleanJobOption(
            label="Generate all permutations for search?",
            default_value=False,
            help_text="Generate all possible Glycan permutation combinations in "
            "looking for the closest match (should only be used for O-glycans as "
            "computationally very expensive)",
            deactivate_if=self.deactivation_conditions["allpermutations"],
            jobop_group="Glytoucan/Glyconnect",
        )
        # custom sugar
        self.joboptions["undefinedsugar"] = BooleanJobOption(
            label="Undefined sugar present?",
            default_value=False,
            help_text="Custom sugar is present in input model file",
            jobop_group="Custom sugar",
        )
        self.joboptions["undefinedsugar_code"] = StringJobOption(
            label="Analyse sugar with code",
            default_value="",
            help_text="PDB three letter code to analyse sugar with",
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["undefinedsugar_code"],
            required_if=JobOptionCondition([("undefinedsugar", "=", True)]),
        )
        self.joboptions["input_anomer"] = MultipleChoiceJobOption(
            label="Sugar anomer",
            help_text="Anomer of undefined sugar",
            choices=["alpha", "beta"],
            default_value_index=0,
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["input_anomer"],
        )
        self.joboptions["input_handedness"] = MultipleChoiceJobOption(
            label="Sugar handedness",
            help_text="Handedness of undefined sugar",
            choices=["-D-", "-L-"],
            default_value_index=0,
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["input_handedness"],
        )
        self.joboptions["input_ring_conformation"] = MultipleChoiceJobOption(
            label="Ring conformation",
            help_text="Ring Type of undefined sugar",
            choices=["pyranose", "furanose"],
            default_value_index=0,
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["input_ring_conformation"],
        )
        self.joboptions["input_conformation_pyranose"] = MultipleChoiceJobOption(
            label="Pyranose conformation",
            help_text=(
                "Choose the Expected Minimal energy ring conformation for input "
                "Pyranose"
            ),
            choices=[
                "4c1",
                "1c4",
                "3Ob",
                "b25",
                "14b",
                "b3O",
                "25b",
                "b14",
                "Oev",
                "ev5",
                "4ev",
                "ev3",
                "2ev",
                "ev1",
                "3ev",
                "ev2",
                "1ev",
                "evO",
                "5ev",
                "ev4",
                "Oh5",
                "4h5",
                "4h3",
                "2h3",
                "2h1",
                "Oh1",
                "3h2",
                "1h2",
                "1hO",
                "5hO",
                "5h4",
                "3h4",
                "Os2",
                "1s5",
                "1s3",
                "2sO",
                "5s1",
                "3s1",
            ],
            default_value_index=0,
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["input_conformation_pyranose"],
        )
        self.joboptions["input_conformation_furanose"] = MultipleChoiceJobOption(
            label="Furanose conformation",
            help_text=(
                "Choose the Expected Minimal energy ring conformation for input "
                "Furanose"
            ),
            choices=[
                "3t2",
                "3ev",
                "3t4",
                "ev4",
                "Ot4",
                "Oev",
                "Ot1",
                "ev1",
                "2t1",
                "2ev",
                "2t3",
                "ev3",
                "4t3",
                "4ev",
                "4tO",
                "evO",
                "1tO",
                "1ev",
                "1t2",
                "ev2",
            ],
            default_value_index=0,
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["input_conformation_furanose"],
        )
        self.joboptions["ring_oxygen"] = StringJobOption(
            label="Atom code of anomeric Oxygen",
            help_text="Atom code of anomeric Oxygen",
            default_value="O5",
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["ring_oxygen"],
            required_if=JobOptionCondition([("undefinedsugar", "=", True)]),
        )
        self.joboptions["ring_C1"] = StringJobOption(
            label="Atom code of ring C1",
            help_text="Designate the atom code for first Carbon",
            default_value="C1",
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["ring_C1"],
            required_if=JobOptionCondition([("undefinedsugar", "=", True)]),
        )
        self.joboptions["ring_C2"] = StringJobOption(
            label="Atom code of ring C2",
            help_text="Designate the atom code for second Carbon (clockwise)",
            default_value="C2",
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["ring_C2"],
            required_if=JobOptionCondition([("undefinedsugar", "=", True)]),
        )
        self.joboptions["ring_C3"] = StringJobOption(
            label="Atom code of ring C3",
            help_text="Designate the atom code for third Carbon (clockwise)",
            default_value="C3",
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["ring_C3"],
            required_if=JobOptionCondition([("undefinedsugar", "=", True)]),
        )
        self.joboptions["ring_C4"] = StringJobOption(
            label="Atom code of ring C4",
            help_text="Designate the atom code for fourth Carbon (clockwise)",
            default_value="C4",
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["ring_C4"],
            required_if=JobOptionCondition([("undefinedsugar", "=", True)]),
        )
        self.joboptions["ring_C5"] = StringJobOption(
            label="Atom code of ring C5",
            help_text="Designate the atom code for fifth Carbon (clockwise)",
            default_value="C5",
            jobop_group="Custom sugar",
            deactivate_if=self.deactivation_conditions["ring_C5"],
            required_if=JobOptionCondition(
                [
                    ("undefinedsugar", "=", True),
                    ("input_ring_conformation", "=", "pyranose"),
                ],
                "ALL",
            ),
        )
        # parallelisation
        self.joboptions["ncpus"] = IntJobOption(
            label="Number of cores",
            default_value=1,
            step_value=1,
            help_text="Increase this to parallelise Privateer run",
            jobop_group="Parallelisation settings",
        )
        # self.joboptions["sleeptimer"] = IntJobOption(
        #     label="Sleep timer between parallel loops",
        #     default_value=1,
        #     step_value=1,
        #     suggested_max=1,
        #     help_text="Set sleep timer value between parallel loops that are used in "
        #     "map calculations and GlyTouCan/GlyConnect permutation algorithm "
        #     "computations. 'Nuclear' option in case Privateer segfaults during those "
        #     "steps, should not need changing this option to higher than 1",
        #     jobop_group="Parallelisation settings",
        # )
        # self.joboptions["keywords"] = MultiStringJobOption(
        #     label="Keywords",
        #     default_value="",
        #     help_text=(
        #         "Keywords for advanced options. List of all available keywords is "
        #         "available on online documentation link in the Info tab. All "
        # "Privateer "
        #         "keywords should be controllable from within the GUI of CCP-EM"
        #     ),
        # )

        self.get_runtab_options()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errors = []
        if not os.path.isfile(privateer_database_path):
            errors.append(
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["input_model"]],
                    message=(
                        "Could not find privateer database relative to CCP4 "
                        "installation directory ('lib/data/privateer_database.json'). "
                    ),
                )
            )
        if self.joboptions["undefinedsugar"].get_boolean():
            errors.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[
                        self.joboptions["input_anomer"],
                        self.joboptions["input_handedness"],
                    ],
                    message="Ensure correct value is input",
                )
            )
        input_map = self.joboptions["input_map"].get_string()
        if os.path.isfile(input_map) and not check_origin_zero(input_map):
            errors.append(
                JobOptionValidationResult(
                    result_type="warning",
                    raised_by=[self.joboptions["input_map"]],
                    message=(
                        "Input map has non-zero origin record. Privateer currently "
                        "ignores this. A model shifted to align with the map of zero "
                        "origin will be used for scoring (_shifted_zero.cif). "
                    ),
                )
            )
        return errors

    @staticmethod
    def get_privateer_logfile_name(modelid) -> str:
        return modelid + "_program.xml"

    @staticmethod
    def get_shifted_model_name(input_model) -> str:
        return (
            os.path.splitext(os.path.basename(input_model))[0]
            + "_shifted_zero"
            + os.path.splitext(input_model)[1]
        )

    def create_output_nodes(self) -> None:
        # Get options needed to decide which output nodes will be made
        input_model = self.joboptions["input_model"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_map = self.joboptions["input_map"].get_string()
        if input_map:
            self.add_output_node(
                self.get_privateer_logfile_name(modelid),
                NODE_PROCESSDATA,
                ["privateer", "output"],
            )
        if not check_origin_zero(input_map):
            self.add_output_node(
                self.get_shifted_model_name(input_model),
                NODE_ATOMCOORDS,
                ["shifted", "zero_origin"],
            )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        commands = []
        # Get parameters
        input_model = self.joboptions["input_model"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_model = os.path.relpath(input_model, self.working_dir)
        input_map = self.joboptions["input_map"].get_string()
        input_map_abs_path = input_map
        input_map = os.path.relpath(input_map, self.working_dir)
        if not check_origin_zero(input_map_abs_path):
            model_shift_command = self.get_model_shift_command(
                input_model=input_model,
                input_map=input_map,
            )
            commands.append(model_shift_command)
            input_model = self.get_shifted_model_name(input_model)
        resolution = self.joboptions["resolution"].get_number()
        mask_radius = self.joboptions["mask_radius"].get_number()
        expression_system_mode = self.joboptions["expression_system_mode"].get_string()
        undefinedsugar = self.joboptions["undefinedsugar"].get_boolean()
        input_ring_conformation = self.joboptions[
            "input_ring_conformation"
        ].get_string()
        # run molprobity
        privateer_command = [self.jobinfo.programs[0].command]
        privateer_command += ["-pdbin", input_model]
        privateer_command += ["-mapin", input_map]
        privateer_command += ["-resolution", str(resolution)]
        privateer_command += ["-radiusin", str(mask_radius)]
        if expression_system_mode:
            privateer_command += ["-expression", expression_system_mode]
        if undefinedsugar:
            privateer_command += [
                "-valstring",
                self.joboptions["undefinedsugar_code"].get_string(),
                self.joboptions["ring_oxygen"].get_string(),
                self.joboptions["ring_C1"].get_string(),
                self.joboptions["ring_C2"].get_string(),
                self.joboptions["ring_C3"].get_string(),
                self.joboptions["ring_C4"].get_string(),
            ]
            if input_ring_conformation == "pyranose":
                privateer_command += [self.joboptions["ring_C5"].get_string()]
                privateer_command += [self.joboptions["input_anomer"].get_string()]
            privateer_command += [self.joboptions["input_handedness"].get_string()]
            if input_ring_conformation == "pyranose":
                privateer_command += [
                    self.joboptions["input_conformation_pyranose"].get_string()
                ]
            else:
                privateer_command += [
                    self.joboptions["input_conformation_furanose"].get_string()
                ]
            privateer_command += [
                "-codein",
                self.joboptions["undefinedsugar_code"].get_string(),
            ]
        glytoucan = self.joboptions["glytoucan"].get_boolean()
        if glytoucan:
            privateer_command += ["-glytoucan"]
            if privateer_database_path:
                privateer_command += ["-databasein", privateer_database_path]
            closestmatch = self.joboptions["closestmatch"].get_boolean()
            if not closestmatch:
                privateer_command += ["-closest_match_disable"]
            if self.joboptions["allpermutations"].get_boolean() and not closestmatch:
                privateer_command += ["-all_permutations"]
        privateer_command += ["-cores", str(self.joboptions["ncpus"].get_number())]
        # privateer_command += [
        #     "-sleep_timer",
        #     str(self.joboptions["sleeptimer"].get_number()),
        # ]

        commands.append(privateer_command)
        commands.append(["mv", "program.xml", self.get_privateer_logfile_name(modelid)])
        return [PipelinerCommand(x) for x in commands]

    @staticmethod
    def get_model_shift_command(input_model: str, input_map: str) -> List:
        return [
            "python3",
            get_job_script("shift_model_origin_zero.py"),
            "-m",
            input_map,
            "-p",
            input_model,
            "--origin_only",  # check origin record only
        ]

    def create_results_display(self) -> List[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        input_model = self.joboptions["input_model"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        xmlout_file = os.path.join(
            self.output_dir, self.get_privateer_logfile_name(modelid)
        )
        ValidationData = self.get_glycan_data(xmlout_file)
        assert ValidationData
        validationdata_dict = PrivateerValidate.get_privateer_validation_data(
            ValidationData
        )
        table_obj = self.create_privateer_validation_table(
            data=validationdata_dict, xmlfilename=xmlout_file
        )
        display_objects.append(table_obj)
        if "Pyranoses" in validationdata_dict:
            pyranoses = validationdata_dict["Pyranoses"]
            plot_objs = self.get_pyranose_plots(
                pyranose_data=pyranoses, xmlfilename=xmlout_file
            )
            display_objects.extend(plot_objs)
        glycanview_htmlobj = create_results_display_object(
            "html",
            title="Detected glycans",
            html_file=self.glycanViewHTML,
            associated_data=[self.glycanViewHTML],
        )
        display_objects.append(glycanview_htmlobj)
        return display_objects

    @staticmethod
    def get_privateer_validation_data(
        ValidationData: ET.Element,
    ) -> Dict[str, List[Dict[str, Optional[ET.Element]]]]:
        data: Dict[str, List[Dict[str, Optional[ET.Element]]]] = OrderedDict()
        pyranoses = ValidationData.findall("Pyranose")
        furanoses = ValidationData.findall("Furanse")
        if len(pyranoses):
            list_of_pyranoses: List[Dict[str, Optional[ET.Element]]] = []
            for pyranose in pyranoses:
                i_pyranose_dict = OrderedDict()
                i_pyranose_dict["Chain"] = pyranose.find("SugarChain")
                i_pyranose_dict["Name"] = pyranose.find("SugarName")
                i_pyranose_dict["Q"] = pyranose.find("SugarQ")
                i_pyranose_dict["Phi"] = pyranose.find("SugarPhi")
                i_pyranose_dict["Theta"] = pyranose.find("SugarTheta")
                i_pyranose_dict["Anomer"] = pyranose.find("SugarAnomer")
                i_pyranose_dict["Hand"] = pyranose.find("SugarHand")
                i_pyranose_dict["Conformation"] = pyranose.find("SugarConformation")
                i_pyranose_dict["RSCC"] = pyranose.find("SugarRSCC")
                i_pyranose_dict["BFactor"] = pyranose.find("SugarBFactor")
                i_pyranose_dict["Diagnostic"] = pyranose.find("SugarDiagnostic")
                list_of_pyranoses.append(i_pyranose_dict)
            data["Pyranoses"] = list_of_pyranoses
        if len(furanoses):
            list_of_furanoses: List[Dict[str, Optional[ET.Element]]] = []
            for furanose in furanoses:
                i_furanose_dict = OrderedDict()
                i_furanose_dict["Chain"] = furanose.find("SugarChain")
                i_furanose_dict["Name"] = furanose.find("SugarName")
                i_furanose_dict["Q"] = furanose.find("SugarQ")
                i_furanose_dict["Phi"] = furanose.find("SugarPhi")
                i_furanose_dict["Anomer"] = furanose.find("SugarAnomer")
                i_furanose_dict["Hand"] = furanose.find("SugarHand")
                i_furanose_dict["Conformation"] = furanose.find("SugarConformation")
                i_furanose_dict["RSCC"] = furanose.find("SugarRSCC")
                i_furanose_dict["BFactor"] = furanose.find("SugarBFactor")
                i_furanose_dict["Diagnostic"] = furanose.find("SugarDiagnostic")
                list_of_furanoses.append(i_furanose_dict)
            data["Furanoses"] = list_of_furanoses
        return data

    @staticmethod
    def create_privateer_validation_table(
        data: Dict, xmlfilename: str
    ) -> ResultsDisplayObject:

        table_title = "Detailed Monosaccharide validation results"
        table_headers = [
            "#",
            "Chain",
            "Name",
            "Q",
            "Phi",
            "Theta",
            "Anomer",
            "D/L<sup>2</sup>",
            "Conformation",
            "RSCC",
            "BFactor",
            "Diagnosic",
        ]
        table_header_tooltips = [
            "nth sugar detected in the model",
            "Protein backbone chain ID monosaccharide is a part of",
            "Monosaccharide's PDB CCD code",
            "Total puckering amplitude, measured in Angstroems",
            "Phi of monosaccharide",
            "Theta of monosaccharide",
            "Anomer of monosaccharide",
            "Whenever N is displayed in the D/L column, it means that Privateer has "
            "been unable to determine the handedness based solely on the structure",
            "Conformation of the sugar",
            "Real Space Correlation Coefficient",
            "BFactor of monosaccharide",
            "Geometric quality of the monosaccharide",
        ]
        list_table_data = []
        sub = str.maketrans("0123456789", "₀₁₂₃₄₅₆₇₈₉")
        sup = str.maketrans("0123456789", "⁰¹²³⁴⁵⁶⁷⁸⁹")
        i = 0  # row
        for entries in data["Pyranoses"]:
            list_row_values = [str(i + 1)]
            for name in entries:
                entry_dict = entries[name]
                entry_text: str = ""
                if entry_dict is not None and entry_dict.text:
                    entry_text = entry_dict.text
                # TODO: is there a better way to convert xml superscript string?
                if "sup" in entry_text:
                    entry_text = re.sub(
                        r"\<sup.*?\>(.*?)\<\/sup\>",
                        lambda x: x.group()[5:-6].translate(sup),
                        entry_text,
                    )
                if "sub" in entry_text:
                    entry_text = re.sub(
                        r"\<sub.*?\>(.*?)\<\/sub\>",
                        lambda x: x.group()[5:-6].translate(sub),
                        entry_text,
                    )
                list_row_values.append(entry_text)
            i += 1
            assert len(table_headers) == len(list_row_values)
            list_table_data.append(list_row_values)

        privateer_table = create_results_display_object(
            "table",
            title=table_title,
            headers=table_headers,
            header_tooltips=table_header_tooltips,
            table_data=list_table_data,
            associated_data=[xmlfilename],
        )
        return privateer_table

    @staticmethod
    def get_pyranose_plots(
        pyranose_data: List[Dict[str, Optional[ET.Element]]], xmlfilename: str
    ) -> List[ResultsDisplayObject]:
        list_plotobjs = []
        low_energy_pyranoses = []
        high_energy_pyranoses = []
        other_issues_pyranoses = []

        for pyranose in pyranose_data:
            if (
                isinstance(pyranose["Diagnostic"], ET.Element)
                and pyranose["Diagnostic"].text
            ):
                if "conformation" in pyranose["Diagnostic"].text.lower():
                    high_energy_pyranoses.append(pyranose)
                elif "Ok" in pyranose["Diagnostic"].text:
                    low_energy_pyranoses.append(pyranose)
            else:
                other_issues_pyranoses.append(pyranose)
        alpha_plot = PrivateerValidate.add_pyranose_alpha_graph(
            low_energy_pyranoses=low_energy_pyranoses,
            high_energy_pyranoses=high_energy_pyranoses,
            other_issues_pyranoses=other_issues_pyranoses,
            xmlfilename=xmlfilename,
        )
        list_plotobjs.append(alpha_plot)
        bravo_plot = PrivateerValidate.add_pyranose_bravo_graph(
            low_energy_pyranoses=low_energy_pyranoses,
            high_energy_pyranoses=high_energy_pyranoses,
            other_issues_pyranoses=other_issues_pyranoses,
            xmlfilename=xmlfilename,
        )
        list_plotobjs.append(bravo_plot)
        return list_plotobjs

    @staticmethod
    def add_pyranose_alpha_graph(
        low_energy_pyranoses: List,
        high_energy_pyranoses: List,
        other_issues_pyranoses: List,
        xmlfilename: str,
    ) -> ResultsDisplayObject:

        dx_ok_Alpha = []
        dy_ok_Alpha = []
        dx_conformation_Alpha = []
        dy_conformation_Alpha = []
        dx_other_Alpha = []
        dy_other_Alpha = []
        ycol_Alpha = "Theta"
        xcol_Alpha = "Phi"
        xmax_ok_Alpha = len(low_energy_pyranoses)
        xmax_conformation_Alpha = len(high_energy_pyranoses)
        xmax_other_Alpha = len(other_issues_pyranoses)
        for i in range(0, xmax_ok_Alpha):
            dx_ok_Alpha.append(float(low_energy_pyranoses[i][xcol_Alpha].text))
            dy_ok_Alpha.append(float(low_energy_pyranoses[i][ycol_Alpha].text))
        for i in range(0, xmax_conformation_Alpha):
            dx_conformation_Alpha.append(
                float(high_energy_pyranoses[i][xcol_Alpha].text)
            )
            dy_conformation_Alpha.append(
                float(high_energy_pyranoses[i][ycol_Alpha].text)
            )
        for i in range(0, xmax_other_Alpha):
            dx_other_Alpha.append(float(other_issues_pyranoses[i][xcol_Alpha].text))
            dy_other_Alpha.append(float(other_issues_pyranoses[i][ycol_Alpha].text))

        list_series_args = []
        plot_data = []
        plot_title = "Conformational landscape for pyranoses"
        plot_data.append(
            {
                "x": dx_ok_Alpha,
                "y": dy_ok_Alpha,
            }
        )
        list_series_args.append(
            {
                "name": "OK",
                "mode": "markers",
                "line": {"color": "green", "width": 5},
            }
        )  # plot trace name (legend)
        plot_data.append(
            {
                "x": dx_conformation_Alpha,
                "y": dy_conformation_Alpha,
            }
        )
        list_series_args.append(
            {
                "name": "Conformation might be mistaken",
                "mode": "markers",
                "line": {"color": "red", "width": 5},
            }
        )
        plot_data.append(
            {
                "x": dx_other_Alpha,
                "y": dy_other_Alpha,
            }
        )
        list_series_args.append(
            {
                "name": "Other issues",
                "mode": "markers",
                "line": {"color": "orange", "width": 5},
            }
        )
        xaxes_args = {
            "title_text": "Phi",
            "range": [360, -30],
        }
        yaxes_args = {
            "title_text": "Theta",
            "range": [180, -45],
        }
        # multi_series plot
        disp_obj = create_results_display_object(
            "plotlyobj",
            data=plot_data,
            plot_type="Scatter",  # all series scatter type
            subplot=False,
            multi_series=True,
            series_args=list_series_args,
            xaxes_args=xaxes_args,
            yaxes_args=yaxes_args,
            title=plot_title,
            associated_data=[xmlfilename],
            flag="",
        )
        return disp_obj

    @staticmethod
    def add_pyranose_bravo_graph(
        low_energy_pyranoses: List,
        high_energy_pyranoses: List,
        other_issues_pyranoses: List,
        xmlfilename: str,
    ) -> ResultsDisplayObject:

        dx_ok_Bravo = []
        dy_ok_Bravo = []
        dx_conformation_Bravo = []
        dy_conformation_Bravo = []
        dx_other_Bravo = []
        dy_other_Bravo = []
        ycol_Bravo = "RSCC"
        xcol_Bravo = "BFactor"
        xmax_ok_Bravo = len(low_energy_pyranoses)
        xmax_conformation_Bravo = len(high_energy_pyranoses)
        xmax_other_Bravo = len(other_issues_pyranoses)
        xvalues = []
        for i in range(0, xmax_ok_Bravo):
            dx_ok_Bravo.append(float(low_energy_pyranoses[i][xcol_Bravo].text))
            dy_ok_Bravo.append(float(low_energy_pyranoses[i][ycol_Bravo].text))
            xvalues.append(float(low_energy_pyranoses[i][xcol_Bravo].text))
        for i in range(0, xmax_conformation_Bravo):
            dx_conformation_Bravo.append(
                float(high_energy_pyranoses[i][xcol_Bravo].text)
            )
            dy_conformation_Bravo.append(
                float(high_energy_pyranoses[i][ycol_Bravo].text)
            )
            xvalues.append(float(high_energy_pyranoses[i][xcol_Bravo].text))
        for i in range(0, xmax_other_Bravo):
            dx_other_Bravo.append(float(other_issues_pyranoses[i][xcol_Bravo].text))
            dy_other_Bravo.append(float(other_issues_pyranoses[i][ycol_Bravo].text))
            xvalues.append(float(other_issues_pyranoses[i][xcol_Bravo].text))

        list_series_args = []
        plot_data = []
        plot_title = "BFactor vs RSCC"
        plot_data.append(
            {
                "x": dx_ok_Bravo,
                "y": dy_ok_Bravo,
            }
        )
        list_series_args.append(
            {
                "name": "OK",
                "mode": "markers",
                "line": {"color": "green", "width": 5},
            }
        )  # plot trace name (legend)
        plot_data.append(
            {
                "x": dx_conformation_Bravo,
                "y": dy_conformation_Bravo,
            }
        )
        list_series_args.append(
            {
                "name": "Conformation might be mistaken",
                "mode": "markers",
                "line": {"color": "red", "width": 5},
            }
        )
        plot_data.append(
            {
                "x": dx_other_Bravo,
                "y": dy_other_Bravo,
            }
        )
        list_series_args.append(
            {
                "name": "Other issues",
                "mode": "markers",
                "line": {"color": "orange", "width": 5},
            }
        )
        xaxes_args = {
            "title_text": "Isotropic B-Factor",
            "range": [0, int(max(xvalues)) + 20],
        }
        yaxes_args = {
            "title_text": "Real Space CC",
            "range": [0, 1.0],
        }
        # multi_series plot
        disp_obj = create_results_display_object(
            "plotlyobj",
            data=plot_data,
            plot_type="Scatter",  # all series scatter type
            subplot=False,
            multi_series=True,
            series_args=list_series_args,
            xaxes_args=xaxes_args,
            yaxes_args=yaxes_args,
            title=plot_title,
            associated_data=[xmlfilename],
            flag="",
        )
        return disp_obj

    def get_glycan_data(self, xmlfilename: str) -> Optional[ET.Element]:
        tree = ET.parse(xmlfilename)
        PrivateerResult = tree.find("PrivateerResult")
        if PrivateerResult:
            ValidationData = PrivateerResult.find("ValidationData")
        if ValidationData:
            glycans = ValidationData.findall("Glycan")
        list_of_glycans: List[Dict] = []
        if len(glycans):
            list_of_glycans = []
            for glycan in glycans:
                i_glycan_dict: Dict[
                    str,
                    Union[
                        ET.Element, List[OrderedDict[str, Optional[ET.Element]]], None
                    ],
                ] = OrderedDict()
                i_glycan_dict["Chain"] = glycan.find("GlycanChain")
                i_glycan_dict["WURCS"] = glycan.find("GlycanWURCS")
                i_glycan_dict["GTCID"] = glycan.find("GlycanGTCID")
                i_glycan_dict["GlyConnectID"] = glycan.find("GlycanGlyConnectID")
                i_glycan_dict["SVG"] = glycan.find("GlycanSVG")
                permutationList = glycan.find("GlycanPermutations")
                if permutationList is not None:
                    permutations = permutationList.findall("GlycanPermutation")
                    list_of_permutations: List[
                        OrderedDict[str, Optional[ET.Element]]
                    ] = []
                    for permutation in permutations:
                        i_permutation_dict = OrderedDict()
                        i_permutation_dict["PermutationWURCS"] = permutation.find(
                            "PermutationWURCS"
                        )
                        i_permutation_dict["PermutationScore"] = permutation.find(
                            "PermutationScore"
                        )
                        i_permutation_dict["anomerPermutations"] = permutation.find(
                            "anomerPermutations"
                        )
                        i_permutation_dict["residuePermutations"] = permutation.find(
                            "residuePermutations"
                        )
                        i_permutation_dict["residuePermutations"] = permutation.find(
                            "residuePermutations"
                        )
                        i_permutation_dict["residueDeletions"] = permutation.find(
                            "residueDeletions"
                        )
                        i_permutation_dict["PermutationGTCID"] = permutation.find(
                            "PermutationGTCID"
                        )
                        i_permutation_dict["PermutationGlyConnectID"] = (
                            permutation.find("PermutationGlyConnectID")
                        )
                        i_permutation_dict["PermutationSVG"] = permutation.find(
                            "PermutationSVG"
                        )
                        list_of_permutations.append(i_permutation_dict)
                    i_glycan_dict["Permutations"] = list_of_permutations
                else:
                    i_glycan_dict["Permutations"] = None
                list_of_glycans.append(i_glycan_dict)
        self.generate_HTML_glycan_view(list_of_glycans)
        return ValidationData

    def generate_HTML_glycan_view(self, list_of_glycans: List[Dict]) -> None:
        html = ET.Element("html")
        head = ET.Element("head")

        style = ET.Element("style")
        style.text = '\nhtml {\n\tline-height: 1.6em;\n\tfont-family: \
        "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;\n\tmargin: \
        10px;\n\ttext-align: left;\n\tborder-collapse: collapse;\n\tclear: \
        both; \n}\n\n.accordion {\n\tdisplay:block;\n\ttext-decoration:none\
        ;\n\tmargin:3px;\n\tmax-width:1000px;\n\theight:1.6em;\n\tpadding:1px\
        ;\n\tpadding-left:10px;\n\tpadding-right:10px;\n\tborder:2px \
        solid #DDD;\n\ttext-align:left;\n\tfont-size:110%;\n\t-webkit-border-radius:\
        10px;\n\tborder-radius:1px;\n\tbackground:-webkit-gradient(linear, \
        0% 0%, 0% 110%, from(#FFFFFF), to(#EEE));\n\tcursor: \
        pointer;\n\ttransition:0.4s \n}\n\n.active, .accordion:hover \
        {\n\tbackground-color: #ccc;\n}\n\n.accordion:after \
        {\n\tcontent: \'\\002B\';\n\tcolor: #777;\n\tfont-weight: bold;\n\tfloat: \
        right;\n\tmargin-left: 5px;\n}\n\n.active:after {\n\tcontent: \
        "\\2212";\n}\n\n.panel {\n\tpadding: 0 18px;\n\tbackground-color: \
        white;\n\tmax-height: 0;\n\toverflow: hidden;\n\ttransition: max-height \
        0.2s ease-out;\n}\n'

        script = ET.Element("script")

        script.text = '\nvar acc = document.getElementsByClassName("accordion");\
        \nvar i;\n\nfor (i = 0; i < acc.length; i++) {\n\t  \
        acc[i].addEventListener("click", function() \
        {\n\t\tthis.classList.toggle("active");\n\t\tvar panel = \
        this.nextElementSibling;\n\t\tif (panel.style.maxHeight) \
        {\n\t\t\tpanel.style.maxHeight = null;\n\t\t} else \
        {\n\t\t\tpanel.style.maxHeight = panel.scrollHeight + "px";\n\t\t}\n\t});\n}'

        head.append(style)
        html.append(head)

        body = ET.Element("body")
        ET.tostring(script, method="html")
        html.append(body)

        divGlobal = ET.Element("div", attrib={"class": "global"})
        explanationParagraph = ET.Element("p")
        explanationParagraph.text = "Below are graphical plots of the detected glycan "
        "trees. Placing your mouse pointer over any of the sugars will display a "
        "tooltip containing its residue name and number from the PDB file."
        divGlobal.append(explanationParagraph)

        for glycan in list_of_glycans:
            chainParagraph = ET.Element(
                "p",
                attrib={
                    "style": "font-size:110%; padding:2px; margin-top:20px; "
                    "margin-bottom:0px; font-weight:bold; margin-left:15px; clear:both"
                },
            )
            modelledGlycanChainID = glycan["Chain"].text
            chainParagraph.text = "Chain " + modelledGlycanChainID
            divGlobal.append(chainParagraph)
            modelledGlycanSVGName = glycan["SVG"].text
            modelledGlycanSVGPath = os.path.join(self.output_dir, modelledGlycanSVGName)
            if os.path.isfile(modelledGlycanSVGPath):
                # register_namespace used to set default namespace with "" prefix
                ET.register_namespace("", "http://www.w3.org/2000/svg")
                ET.register_namespace("xlink", "http://www.w3.org/1999/xlink")
                svg_file = open(modelledGlycanSVGPath, "r")
                svg_string = svg_file.read()
                svg_file.close()

                svg_string_partitioned = svg_string.partition('width="')
                svg_width = ""
                for symbol in svg_string_partitioned[2]:
                    if symbol != '"':
                        svg_width = svg_width + symbol
                    else:
                        break

                ElementTreeSVGsource = ET.parse(modelledGlycanSVGPath)
                ElementSVGsource = ElementTreeSVGsource.getroot()

                divBetweenPandSVG = ET.Element(
                    "div",
                    attrib={
                        "style": "border-width:1px; padding-top:10px; "
                        "padding-bottom:10px; border-color:black; border-style:solid; "
                        "border-radius:15px;"
                    },
                )
                # divGlobal.append(divBetweenPandSVG)

                divSVG = ET.Element("div", attrib={"style": "padding:10px;"})
                divSVG.append(ElementSVGsource)
                WURCSParagraph = ET.Element(
                    "p", attrib={"style": "font-size:110%; font-weight:bold"}
                )
                WURCSParagraph.text = glycan["WURCS"].text
                divSVG.append(WURCSParagraph)

                if glycan["GTCID"].text != "Unable to find GlyTouCan ID":
                    GTCIDParagraph = ET.Element(
                        "p", attrib={"style": "font-size:110%; font-weight:bold"}
                    )
                    GTCIDParagraph.text = "GlyTouCan ID: " + glycan["GTCID"].text
                    divSVG.append(GTCIDParagraph)
                else:
                    GTCIDParagraph = ET.Element(
                        "p",
                        attrib={
                            "style": "font-size:110%; font-weight:bold; color:#ff3300"
                        },
                    )
                    GTCIDParagraph.text = "GlyTouCan ID: Not Found"
                    divSVG.append(GTCIDParagraph)

                if glycan["GlyConnectID"].text != "Unable to find GlyConnect ID":
                    GlyConnectIDParagraph = ET.Element(
                        "p", attrib={"style": "font-size:110%; font-weight:bold"}
                    )
                    GlyConnectIDParagraph.text = (
                        "GlyConnect ID: " + glycan["GlyConnectID"].text
                    )
                    divSVG.append(GlyConnectIDParagraph)
                else:
                    GlyConnectIDParagraph = ET.Element(
                        "p",
                        attrib={
                            "style": "font-size:110%; font-weight:bold; color:#ff3300"
                        },
                    )
                    GlyConnectIDParagraph.text = "GlyConnect ID: Not Found"
                    divSVG.append(GlyConnectIDParagraph)
                    if glycan["Permutations"] is not None:
                        button = ET.Element("button", attrib={"class": "accordion"})
                        button.text = (
                            "Closest permutations detected on GlyConnect database"
                        )
                        divSVG.append(button)
                        sectionDiv = ET.Element("div", attrib={"class": "panel"})
                        sectionDivStyle = ET.Element(
                            "div",
                            attrib={
                                "style": "border-width: 1px; padding-top: 10px; "
                                "padding-bottom:10px; border-color:black; "
                                "border-style:solid; border-radius:15px;"
                            },
                        )
                        permutationList = glycan["Permutations"]

                        for permutation in permutationList:
                            permutationDivBorder = ET.Element(
                                "div",
                                attrib={
                                    "style": "border-width: 1px; padding-top: "
                                    "10px; padding-bottom:10px; border-color:grey; "
                                    "border-style:dashed; border-radius:20px;"
                                },
                            )
                            permutationGlycanSVGName = permutation[
                                "PermutationSVG"
                            ].text
                            permutationGlycanSVGPath = os.path.join(
                                self.output_dir, permutationGlycanSVGName
                            )
                            if os.path.isfile(permutationGlycanSVGPath):
                                svg_file_permutation = open(
                                    permutationGlycanSVGPath, "r"
                                )
                                svg_string = svg_file_permutation.read()
                                svg_file_permutation.close()

                                # svg_string_partitioned_permutation = (
                                #     svg_string.partition('width="')
                                # )
                                svg_width_permutation = ""
                                for symbol in svg_string_partitioned[2]:
                                    if symbol != '"':
                                        svg_width_permutation = (
                                            svg_width_permutation + symbol
                                        )
                                    else:
                                        break

                                permutationElementTreeSVGsource = ET.parse(
                                    permutationGlycanSVGPath
                                )
                                permutationElementSVGsource = (
                                    permutationElementTreeSVGsource.getroot()
                                )
                                permutationDivBorder.append(permutationElementSVGsource)

                                WURCSParagraphPermutation = ET.Element(
                                    "p",
                                    attrib={
                                        "style": "font-size:110%; font-weight:bold"
                                    },
                                )
                                WURCSParagraphPermutation.text = permutation[
                                    "PermutationWURCS"
                                ].text
                                permutationDivBorder.append(WURCSParagraphPermutation)

                                permutation["PermutationScore"].text
                                PermutationScore = ET.Element(
                                    "p", attrib={"style": "font-size:110%;"}
                                )
                                PermutationScore.text = (
                                    "Permutation Score(out of 100): "
                                )
                                if float(permutation["PermutationScore"].text) <= 1.00:
                                    spanText = ET.Element(
                                        "span",
                                        attrib={
                                            "style": "color:#00ff00; font-weight: bold"
                                        },
                                    )
                                    spanText.text = permutation["PermutationScore"].text
                                    PermutationScore.append(spanText)
                                elif (
                                    float(permutation["PermutationScore"].text) > 1.00
                                    and float(permutation["PermutationScore"].text)
                                    <= 10.00
                                ):
                                    spanText = ET.Element(
                                        "span",
                                        attrib={
                                            "style": "color:#ffa500; font-weight: bold"
                                        },
                                    )
                                    spanText.text = permutation["PermutationScore"].text
                                    PermutationScore.append(spanText)
                                elif (
                                    float(permutation["PermutationScore"].text) > 10.00
                                ):
                                    spanText = ET.Element(
                                        "span",
                                        attrib={
                                            "style": "color:#ff3300; font-weight: bold"
                                        },
                                    )
                                    spanText.text = permutation["PermutationScore"].text
                                    PermutationScore.append(spanText)

                                permutationDivBorder.append(PermutationScore)

                                anomerPermutations = ET.Element(
                                    "p", attrib={"style": "font-size:110%;"}
                                )
                                anomerPermutations.text = "Anomer Permutations: "
                                spanTextAnomer = ET.Element(
                                    "span", attrib={"style": "font-weight: bold"}
                                )
                                spanTextAnomer.text = permutation[
                                    "anomerPermutations"
                                ].text
                                anomerPermutations.append(spanTextAnomer)
                                permutationDivBorder.append(anomerPermutations)

                                residuePermutations = ET.Element(
                                    "p", attrib={"style": "font-size:110%;"}
                                )
                                residuePermutations.text = "Residue Permutations: "
                                spanTextResidue = ET.Element(
                                    "span", attrib={"style": "font-weight: bold"}
                                )
                                spanTextResidue.text = permutation[
                                    "residuePermutations"
                                ].text
                                residuePermutations.append(spanTextResidue)
                                permutationDivBorder.append(residuePermutations)

                                residueDeletions = ET.Element(
                                    "p", attrib={"style": "font-size:110%;"}
                                )
                                residueDeletions.text = "Residue Deletions: "
                                spanTextDeletions = ET.Element(
                                    "span", attrib={"style": "font-weight: bold"}
                                )
                                spanTextDeletions.text = permutation[
                                    "residueDeletions"
                                ].text
                                residueDeletions.append(spanTextDeletions)
                                permutationDivBorder.append(residueDeletions)

                                PermutationGTCIDParagraph = ET.Element(
                                    "p", attrib={"style": "font-size:110%;"}
                                )
                                PermutationGTCIDParagraph.text = "GlyTouCan ID: "
                                spanTextPermutationGTCID = ET.Element(
                                    "span", attrib={"style": "font-weight: bold"}
                                )
                                spanTextPermutationGTCID.text = permutation[
                                    "PermutationGTCID"
                                ].text
                                PermutationGTCIDParagraph.append(
                                    spanTextPermutationGTCID
                                )
                                permutationDivBorder.append(PermutationGTCIDParagraph)

                                PermutationGlyConnectIDParagraph = ET.Element(
                                    "p", attrib={"style": "font-size:110%;"}
                                )
                                PermutationGlyConnectIDParagraph.text = (
                                    "Glyconnect ID: "
                                )
                                spanTextPermutationGlyConnectID = ET.Element(
                                    "span", attrib={"style": "font-weight: bold"}
                                )
                                spanTextPermutationGlyConnectID.text = permutation[
                                    "PermutationGlyConnectID"
                                ].text
                                PermutationGlyConnectIDParagraph.append(
                                    spanTextPermutationGlyConnectID
                                )
                                permutationDivBorder.append(
                                    PermutationGlyConnectIDParagraph
                                )

                                divSeperator = ET.Element(
                                    "div", attrib={"style": "padding:10px; "}
                                )
                                permutationDivBorder.append(divSeperator)
                                sectionDivStyle.append(permutationDivBorder)

                        sectionDiv.append(sectionDivStyle)
                        divSVG.append(sectionDiv)

                divBetweenPandSVG.append(divSVG)
                divCLEAR = ET.Element("div", attrib={"style": "clear:both"})
                divBetweenPandSVG.append(divCLEAR)
                divGlobal.append(divBetweenPandSVG)

                # modelledGlycanWURCS = glycan['WURCS'].text
                # modelledGlycanGTCID = glycan['GTCID'].text
                # modelledGlycanGlyConnectID = glycan['GlyConnectID'].text

        body.append(divGlobal)
        body.append(script)

        self.glycanViewHTML = os.path.join(self.output_dir, "glycanview.html")

        ET.ElementTree(html).write(
            self.glycanViewHTML,
            encoding="unicode",
            method="html",
            xml_declaration=True,
        )  # pretty_print=True
        # parser = etree.HTMLParser()
        # glycanViewHACK = etree.parse(self.glycanViewHTML, parser)

        # finalHTMLoutput = etree.tostring(
        #     glycanViewHACK, pretty_print=True, method="html"
        # )

        # return finalHTMLoutput
