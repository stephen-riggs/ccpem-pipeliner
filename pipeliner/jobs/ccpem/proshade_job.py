#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import json
from typing import List, Dict, Sequence, Union, Tuple, Mapping


from pipeliner import user_settings
from pipeliner.pipeliner_job import PipelinerJob, Ref, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    StringJobOption,
    InputNodeJobOption,
    FloatJobOption,
    BooleanJobOption,
    files_exts,
    JobOptionCondition,
)
from pipeliner.display_tools import (
    create_results_display_object,
    make_map_model_thumb_and_display,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS, NODE_LOGFILE
from pipeliner.results_display_objects import ResultsDisplayObject


class ProShadeJobSym(PipelinerJob):
    PROCESS_NAME = "proshade.map_analysis.symmetry"
    OUT_DIR = "ProShade"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "ProShade Symmetry Determination"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn, Matt Iadanza"
        self.jobinfo.short_desc = "Point group symmetry detection"
        self.jobinfo.long_desc = (
            "ProShade (Protein Shape Description and Symmetry Detection) tool "
            "provides several separate functionalities. These functionalities "
            "accept both coordinate data as well as map data. Symmetry detection "
            "allows finding the symmetry, the symmetry axes and symmetry group "
            "elements for any input file."
        )
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/"
            "murshudov/content/proshade/proshade.html"
        )
        self.jobinfo.programs = [
            ExternalProgram(
                command="proshade", vers_com=["proshade", "-v"], vers_lines=[0]
            )
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Nicholls RA",
                    "Tykac M",
                    "Kovalevskiy O",
                    "Murshudov GN",
                ],
                title="Current approaches for the fitting and refinement of "
                "atomic models into cryo-EM maps using CCP-EM",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2018",
                volume="74",
                pages="492-505",
                doi="10.1107/S2059798318007313",
            ),
        ]

        self.sharedir = user_settings.get_ccpem_share_dir()
        sharedir_msg = "Could not find the CCPEM share dir, check pipeliner settings"
        if not self.sharedir:
            self.jobinfo.alternative_unavailable_reason = sharedir_msg
        elif not os.path.isdir(self.sharedir):
            self.jobinfo.alternative_unavailable_reason = sharedir_msg

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc"]),
            help_text="The input map",
            is_required=True,
        )

        self.joboptions["point_sym"] = StringJobOption(
            label="Expected symmetry",
            default_value="",
            validation_regex="[CD][0-9]+|^[TO]$",
            regex_error_message="Symmerty must be C[1-9] D[1-9], T, or O",
            help_text="The symmetry to be preferred, if found. This parameter forces "
            "the symmetry search to output the results for this type of "
            "symmetry, if found. The parameter should have the first letter "
            "C for cyclic, D for dihedral, T for tetrahedral, O for octahedral "
            "or I for icosahedral. C and D must be followed by a "
            "number specifying the required fold, but T, O and I must not.",
            in_continue=True,
        )

        self.get_runtab_options(addtl_args=True)

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "proshade_symmetry.json",
            NODE_LOGFILE,
            ["proshade", "map_analysis", "symmetry"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        command = ["proshade", "--rvpath", self.sharedir, "-S"]
        input_file = self.joboptions["input_map"].get_string()
        point_sym = self.joboptions["point_sym"].get_string()
        command += ["-f", input_file]
        if point_sym:
            command += ["--sym", point_sym]

        command += self.parse_additional_args()

        report_dest = os.path.join(self.output_dir, "proshade_report")
        moving_com = ["mv", "proshade_report", report_dest]

        # needs to return a list of PipelinerCommand objects
        final_commands_list = [command, moving_com]
        return [PipelinerCommand(x) for x in final_commands_list]

    # TODO: check that this function handles the log generated when
    #  no symmetry was found - don't know what this looks like and other
    #  possible log file formats I may have not anticipated

    def parse_results(
        self,
    ) -> Tuple[Dict[str, List[Mapping[str, object]]], List[List[List[str]]]]:
        """Parse the output that went to run.out"""
        # parse the logfile from proshade symmetry
        outlog = os.path.join(self.output_dir, "run.out")
        with open(outlog, "r") as ol:
            outdata = ol.readlines()
        div_count = 0
        axes, elements, alts = [], [], []
        exclude = ["----", "Symmetry elements table:", "Alternatives:"]
        for line in outdata:
            if not any([x in line for x in exclude]) and line != "\n":
                if div_count == 8:
                    axes.append(line)
                elif div_count == 10:
                    elements.append(line)
                elif div_count == 12:
                    alts.append(line)
                elif div_count > 12:
                    break
            if "--------" in line:
                div_count += 1

        # prepare the data
        outdict: Dict[str, List[Mapping[str, object]]] = {
            "Symmetry axes": [],
            "Symmetry elements": [],
            "Alternatives": [],
        }
        outax, outelems, outalts = [], [], []

        for ax in axes:
            dat = ax.split()
            angle = "".join(dat[5:8]) if len(dat) == 9 else dat[5]
            od = {
                "Symmetry Type": dat[0],
                "Fold": dat[1],
                "x": dat[2],
                "y": dat[3],
                "z": dat[4],
                "Angle": angle,
                "Peak Height": dat[-1],
            }
            outdict["Symmetry axes"].append(od)
            outax.append(dat[:5] + [angle] + [dat[-1]])

        for elm in elements:
            dat = elm.split()
            od = {
                "Symmetry Type": dat[0],
                "x": dat[1],
                "y": dat[2],
                "z": dat[3],
                "Ang (deg)": dat[4],
            }
            outdict["Symmetry elements"].append(od)
            outelems.append(dat)
        for alt in alts:
            dat = alt.split()
            angle = "".join(dat[5:8]) if len(dat) == 9 else dat[5]
            od = {
                "Symmetry Type": dat[0],
                "Fold": dat[1],
                "x": dat[2],
                "y": dat[3],
                "z": dat[4],
                "Angle": angle,
                "Peak Height": dat[-1],
            }
            outdict["Alternatives"].append(od)
            outalts.append(dat[:5] + [angle] + [dat[-1]])
        return outdict, [outax, outelems, outalts]

    def results_list(self) -> List[List[List[str]]]:
        return self.parse_results()[1]

    def results_dict(self) -> Dict[str, List[Mapping[str, object]]]:
        return self.parse_results()[0]

    def create_post_run_output_nodes(self) -> None:
        """Read the run.out and create a proper output node"""
        outf = os.path.join(self.output_dir, "proshade_symmetry.json")
        outdict = self.results_dict()
        with open(outf, "w") as outfile:
            json.dump(outdict, outfile, indent=4)

    def gather_metadata(self) -> Dict[str, object]:
        def str_convert(obj):
            if "Fold" in obj:
                obj["Fold"] = int(obj["Fold"].replace("+", "").replace(".00", ""))
            if "x" in obj:
                obj["x"] = float(obj["x"].replace("+", ""))
            if "y" in obj:
                obj["y"] = float(obj["y"].replace("+", ""))
            if "z" in obj:
                obj["z"] = float(obj["z"].replace("+", ""))
            if "Angle" in obj:
                obj["Angle"] = eval(obj["Angle"].replace("pi", "*np.pi"))
            if "Ang (deg)" in obj:
                obj["Ang (deg)"] = float(obj["Ang (deg)"].replace("+", ""))
            if "Peak Height" in obj:
                obj["Peak Height"] = float(obj["Peak Height"].replace("+", ""))
            return obj

        with open("parse_json.json", "r") as j:
            metadata_dict = json.loads(j.read(), object_hook=str_convert)

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        """create a ResultsDisplayTable object for each of the 3 outputs
        and a rvapi display object"""
        # do the three tables
        outf = os.path.join(self.output_dir, "proshade_symmetry.json")
        axes, elems, alts = self.results_list()

        ax_t = create_results_display_object(
            "table",
            title="Symmetry Axes",
            headers=[
                "Symmetry Type",
                "Fold",
                "x",
                "y",
                "z",
                "Angle",
                "Peak Height",
            ],
            table_data=axes,
            associated_data=[outf],
        )

        elem_t = create_results_display_object(
            "table",
            title="Symmetry Elements",
            headers=[
                "Symmetry Type",
                "x",
                "y",
                "z",
                "Angle",
            ],
            table_data=elems,
            associated_data=[outf],
        )

        alt_t = create_results_display_object(
            "table",
            title="Alternatives",
            headers=[
                "Symmetry Type",
                "Fold",
                "x",
                "y",
                "z",
                "Angle",
                "Peak Height",
            ],
            table_data=alts,
            associated_data=[outf],
        )

        # TODO: the HTML display is broken , removing for now
        # then the rvapi
        # report_dir = os.path.join(self.output_dir, "proshade_report")
        # rvapi = create_results_display_object(
        #     "html",
        #     title="ProShade results",
        #     html_file=os.path.join(report_dir, "index.html"),
        #     associated_data=[os.path.join(report_dir, "index.html")],
        # )
        return [ax_t, elem_t, alt_t]  # , rvapi]


class ProShadeJobOverlay(PipelinerJob):
    """CCP-EM gui creates:
    proshade --rvpath /xtal/ccpem-20220125/share -O -f in1.mrc -f in2.mrc
     --clearMap in2_overlaid.mrc --cellBorderSpace 20.0
    TODO ProShade won't accept mmCIF format coordinate files
    """

    PROCESS_NAME = "proshade.map_utilities.overlay"
    OUT_DIR = "ProShade"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "ProShade Overlay"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Martyn Winn, Matt Iadanza, Agnel Joseph"
        self.jobinfo.short_desc = "Overlay one map/model on another "
        self.jobinfo.long_desc = (
            "ProShade (Protein Shape Description and Symmetry Detection) tool "
            "provides several separate functionalities. The overlay "
            "functionality finds the rotation and translation which optimally overlays "
            "(or fits) one structure into another. Each structure can be a "
            "coordinate file or a map."
        )
        self.jobinfo.documentation = (
            "https://www2.mrc-lmb.cam.ac.uk/groups/"
            "murshudov/content/proshade/proshade.html"
        )
        self.jobinfo.programs = [
            ExternalProgram(
                command="proshade", vers_com=["proshade", "-v"], vers_lines=[0]
            )
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Nicholls RA",
                    "Tykac M",
                    "Kovalevskiy O",
                    "Murshudov GN",
                ],
                title="Current approaches for the fitting and refinement of "
                "atomic models into cryo-EM maps using CCP-EM",
                journal="Acta Crystallogr D Biol Crystallogr.",
                year="2018",
                volume="74",
                pages="492-505",
                doi="10.1107/S2059798318007313",
            ),
        ]

        self.sharedir = user_settings.get_ccpem_share_dir()
        sharedir_msg = "Could not find the CCPEM share dir, check pipeliner settings"
        if not self.sharedir:
            self.jobinfo.alternative_unavailable_reason = sharedir_msg
        elif not os.path.isdir(self.sharedir):
            self.jobinfo.alternative_unavailable_reason = sharedir_msg
        self.joboptions["static_ismap"] = BooleanJobOption(
            label="Static structure is map?",
            default_value=True,
            help_text="If No, an input model is required as a static structure",
        )
        self.joboptions["static_map"] = InputNodeJobOption(
            label="Static map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map", ".ccp4"]),
            help_text="Input a map as the static structure",
            required_if=JobOptionCondition([("static_ismap", "=", True)]),
            deactivate_if=JobOptionCondition([("static_ismap", "=", False)]),
        )
        self.joboptions["static_model"] = InputNodeJobOption(
            label="Static atomic model",
            node_type=NODE_ATOMCOORDS,
            default_value="",
            pattern=files_exts("3D coordinates", [".ent", ".pdb"]),
            help_text="Input an atomic model as the static structure",
            required_if=JobOptionCondition([("static_ismap", "=", False)]),
            deactivate_if=JobOptionCondition([("static_ismap", "=", True)]),
        )
        self.joboptions["moving_ismap"] = BooleanJobOption(
            label="Moving structure is map?",
            default_value=True,
            help_text="If No, an input model is required as a moving structure",
        )
        self.joboptions["moving_map"] = InputNodeJobOption(
            label="Moving map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map", ".ccp4"]),
            help_text="The second structure (map) is moved to match the first",
            required_if=JobOptionCondition([("moving_ismap", "=", True)]),
            deactivate_if=JobOptionCondition([("moving_ismap", "=", False)]),
        )
        self.joboptions["moving_model"] = InputNodeJobOption(
            label="Moving atomic model",
            node_type=NODE_ATOMCOORDS,
            default_value="",
            pattern=files_exts("3D coordinates", [".ent", ".pdb"]),
            help_text="The second structure (model) which is moved to match the first",
            required_if=JobOptionCondition([("moving_ismap", "=", False)]),
            deactivate_if=JobOptionCondition([("moving_ismap", "=", True)]),
        )
        self.joboptions["add_space"] = BooleanJobOption(
            label="Add extra space around structure?",
            default_value=False,
            help_text=(
                "(Optional) Extra space may be required to accommodate "
                "rotation/translation of second structure"
            ),
            in_continue=True,
        )
        self.joboptions["extra_space"] = FloatJobOption(
            label="Extra space around structure",
            default_value=0.0,
            suggested_min=0.0,
            help_text="How much extra space to add (angstroms)",
            in_continue=True,
            deactivate_if=JobOptionCondition([("add_space", "=", False)]),
            required_if=JobOptionCondition([("add_space", "=", True)]),
        )

        self.get_runtab_options(addtl_args=True)

    @staticmethod
    def has_map_extension(filename) -> bool:
        ext = os.path.splitext(filename)[1]
        return ext in (".map", ".mrc", ".ccp4")

    @staticmethod
    def get_output_filename(input_file_2) -> str:
        if ProShadeJobOverlay.has_map_extension(input_file_2):
            output_ext = "mrc"
        else:
            output_ext = "pdb"
        output_file = (
            os.path.basename(input_file_2).split(".")[0] + "_overlaid." + output_ext
        )
        return output_file

    def create_output_nodes(self) -> None:
        moving_ismap = self.joboptions["moving_ismap"].get_boolean()
        if moving_ismap:
            input2_file = self.joboptions["moving_map"].get_string()
        else:
            input2_file = self.joboptions["moving_model"].get_string()
        if self.has_map_extension(input2_file):
            out_node_type = NODE_DENSITYMAP
        else:
            out_node_type = NODE_ATOMCOORDS
        self.add_output_node(
            self.get_output_filename(input2_file),
            out_node_type,
            ["proshade", "overlaid"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        # TODO: We will need to set -rvpath from an env var or add it as a joboption
        # because it will vary
        command: List[Union[int, float, str]] = [
            "proshade",
            "--rvpath",
            self.sharedir,
            "-O",
        ]
        static_ismap = self.joboptions["static_ismap"].get_boolean()
        moving_ismap = self.joboptions["moving_ismap"].get_boolean()
        if static_ismap:
            input1_file = self.joboptions["static_map"].get_string()
        else:
            input1_file = self.joboptions["static_model"].get_string()
        if moving_ismap:
            input2_file = self.joboptions["moving_map"].get_string()
        else:
            input2_file = self.joboptions["moving_model"].get_string()
        command += ["-f", input1_file]
        command += ["-f", input2_file]

        # output type according to second (moved) input file
        output_file = self.get_output_filename(input2_file)
        command += ["--clearMap", os.path.join(self.output_dir, output_file)]

        do_extra_space = self.joboptions["add_space"].get_boolean()
        if do_extra_space:
            alt_arg = self.joboptions["extra_space"].get_number()
            command += ["--cellBorderSpace", alt_arg]
        command += self.parse_additional_args()

        report_dest = os.path.join(self.output_dir, "proshade_report")
        moving_com: List[Union[int, float, str]] = [
            "mv",
            "proshade_report",
            report_dest,
        ]

        # needs to return a list of lists
        final_commands_list: List[List[Union[str, int, float]]] = [
            command,
            moving_com,
        ]
        return [PipelinerCommand(x) for x in final_commands_list]

    def gather_metadata(self) -> Dict[str, object]:
        def strip_tags(tag):
            if "<pre>" in tag:
                tag = tag.replace("<pre>", " ")
            if "</pre>" in tag:
                tag = tag.replace("</pre>", " ")
            if "<b>" in tag:
                tag = tag.replace("<b>", " ")
            if "</b>" in tag:
                tag = tag.replace("</b>", " ")
            return tag

        metadata_dict: Dict[str, object] = {}

        with open(self.output_dir + "proshade_report/task.tsk", "r") as rf:
            results_file = rf.readlines()

        for lines in results_file:
            if "ResultsSection" in lines:
                line = strip_tags(lines)
                if "Patterson maps optimal rotation" in line:
                    line = line.split()
                    metadata_dict["PattersonMapsOptimalRotation"] = [
                        float(line[8].strip("+")),
                        float(line[9].strip("+")),
                        float(line[10].strip("+")),
                    ]
                if "Phased maps" in line:
                    line = line.split()
                    metadata_dict["PhasedMapsOptimalTranslation"] = [
                        float(line[7].strip("+")),
                        float(line[8].strip("+")),
                        float(line[9].strip("+")),
                    ]
                if "Correlation between rotated" in line:
                    metadata_dict["PattersonMapsCorrelation"] = float(
                        line.split()[7].strip("+")
                    )
                if "Correlation between translated" in line:
                    metadata_dict["PhasedMapsCorrelation"] = float(
                        line.split()[8].strip("+")
                    )

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        maps = []
        models = []
        static_ismap = self.joboptions["static_ismap"].get_boolean()
        moving_ismap = self.joboptions["moving_ismap"].get_boolean()
        if static_ismap:
            input1_file = self.joboptions["static_map"].get_string()
            maps.append(input1_file)
        else:
            input1_file = self.joboptions["static_model"].get_string()
            models.append(input1_file)
        if moving_ismap:
            input2_file = self.joboptions["moving_map"].get_string()
        else:
            input2_file = self.joboptions["moving_model"].get_string()
        output_file = self.get_output_filename(input2_file)
        output_file = os.path.join(self.output_dir, output_file)
        if self.has_map_extension(input2_file):
            maps.append(output_file)
        else:
            models.append(output_file)
        return [
            make_map_model_thumb_and_display(
                models=models,
                maps=maps,
                outputdir=self.output_dir,
            )
        ]
