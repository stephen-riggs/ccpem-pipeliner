#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Dict, Any, Sequence


from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    files_exts,
    JobOptionCondition,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import make_map_model_thumb_and_display
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_STRUCTUREFACTORS,
    NODE_ATOMCOORDS,
    NODE_LIGANDDESCRIPTION,
    NODE_MASK3D,
)
from pipeliner.results_display_objects import ResultsDisplayObject


class DifferenceMap(PipelinerJob):
    PROCESS_NAME = "servalcat.difference_map"
    OUT_DIR = "ServalcatDifferenceMap"

    def __init__(self) -> None:
        super(self.__class__, self).__init__()
        self.vers_com = ["servalcat", "--version"]
        self.jobinfo.display_name = "Servalcat difference map"
        self.jobinfo.short_desc = (
            "Calculate a difference map between a map and an atomic model"
        )
        self.jobinfo.long_desc = (
            "Fo-Fc map calculation based on model and data errors."
            "\nServalcat uses the input half maps and model to calculate a weighted"
            " difference map, showing positive peaks where the map contains density not"
            " explained by the model and negative peaks where the model indicates"
            " greater density than the value in the map."
            "\nIt is important to refine the model before calculating a difference map."
            " At least the atomic displacement parameters (ADPs, or B values), should"
            " be refined to give a meaningful result. Note that models downloaded from"
            " public databases such as the PDB might have inappropriate ADPs and such"
            " models should be re-refined before calculating difference maps from them."
        )
        self.jobinfo.programs = [
            ExternalProgram(
                command="servalcat", vers_com=["servalcat", "--version"], vers_lines=[0]
            ),
        ]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Tom Burnley"
        self.jobinfo.references = [
            Ref(
                authors=["Yamashita K", "Palmer CM", "Burnley T", "Murshudov GN"],
                title=(
                    "Cryo-EM single particle structure refinement and map "
                    "calculation using Servalcat."
                ),
                journal="Acta Cryst. D",
                year="2022",
                volume="77",
                issue="1",
                pages="1282-1291",
                doi="10.1107/S2059798321009475",
            )
        ]
        self.jobinfo.documentation = "https://servalcat.readthedocs.io/"

        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text="The input model to be refined",
            is_required=True,
        )
        self.joboptions["input_ligand"] = InputNodeJobOption(
            label="Input ligand",
            node_type=NODE_LIGANDDESCRIPTION,
            pattern=files_exts("Ligand definition", [".cif"]),
            default_value="",
            help_text="The input model",
        )

        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0,
            step_value=0.1,
            help_text="Resolution in angstroms",
            is_required=True,
        )

        self.joboptions["input_half_map1"] = InputNodeJobOption(
            label="Input map 1",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map ", [".mrc", ".map"]),
            help_text="Half map 1 from 3D refinement",
            is_required=True,
        )
        self.joboptions["input_half_map2"] = InputNodeJobOption(
            label="Input map 2",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map ", [".mrc", ".map"]),
            help_text="Half map 2 from 3D refinement",
            is_required=True,
        )
        self.joboptions["input_mask"] = InputNodeJobOption(
            label="Input mask",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("3D map ", [".mrc", ".map"]),
            help_text="The input mask required for difference map calculation",
        )
        self.joboptions["mask_radius"] = FloatJobOption(
            label="Mask radius",
            default_value=25,
            suggested_min=5,
            suggested_max=1000,
            hard_min=0,
            step_value=0.1,
            help_text="mask radius (not used if --mask is given)",
            required_if=JobOptionCondition([("input_mask", "=", "")]),
            deactivate_if=JobOptionCondition([("input_mask", "!=", "")]),
        )
        self.get_runtab_options()

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "diffmap.mtz", NODE_STRUCTUREFACTORS, ["servalcat", "difference_map"]
        )
        self.add_output_node(
            "diffmap.mrc", NODE_DENSITYMAP, ["servalcat", "difference_map"]
        )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        # servalcat fofc
        # --model refined_omit.pdb
        # --halfmaps ../emd_9937_half_map_1.map.gz ../emd_9937_half_map_2.map.gz
        # --mask  ../emd_9937_msk_1.map
        # --resolution 2.9 --normalized_map
        # -o diffmap_omit

        command = [self.jobinfo.programs[0].command]

        # Get parameters
        input_half_map1 = self.joboptions["input_half_map1"].get_string()
        input_half_map2 = self.joboptions["input_half_map2"].get_string()
        input_model = self.joboptions["input_model"].get_string()
        resolution = self.joboptions["resolution"].get_number()
        input_mask = self.joboptions["input_mask"].get_string()
        mask_radius = self.joboptions["mask_radius"].get_number()

        # Set command parameters
        command += ["fofc"]

        command += ["--model", os.path.relpath(input_model, self.working_dir)]
        command += [
            "--halfmaps",
            os.path.relpath(input_half_map1, self.working_dir),
            os.path.relpath(input_half_map2, self.working_dir),
        ]
        if input_mask != "":
            command += ["--mask", os.path.relpath(input_mask, self.working_dir)]
        else:
            command += ["-r", str(mask_radius)]

        command += ["--resolution", str(resolution)]

        output_prefix = "diffmap"
        # Make optional at later date?
        command += ["--normalized_map", "-o", output_prefix]

        # convert the mtz to mrc
        conv_com = ["gemmi", "sf2map", "diffmap.mtz", "diffmap.mrc"]

        commands: List[List[str]] = [command, conv_com]

        return [PipelinerCommand(x) for x in commands]

    def gather_metadata(self) -> Dict[str, Any]:

        metadata_dict: Dict[str, Any] = {}

        outfile = os.path.join(self.output_dir, "run.out")
        with open(outfile, "r") as of:
            outlines = of.readlines()

        for line in outlines:
            if "sharpening method" in line:
                metadata_dict["SharpeningMethod"] = line.split(":")[1]
            if "Whole volume" in line:
                metadata_dict["WholeVolume"] = int(line.split()[2])
            if "Masked volume" in line:
                metadata_dict["MaskedVolume"] = int(line.split()[2])
            if "Global mean" in line:
                metadata_dict["GlobalMean"] = float(line.split()[2])
            if "Global std" in line:
                metadata_dict["GlobalStd"] = float(line.split()[2])
            if "Masked mean" in line:
                metadata_dict["MaskedMean"] = float(line.split()[2])
            if "Masked std" in line:
                metadata_dict["MaskedStd"] = float(line.split()[2])

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        models = self.joboptions["input_model"].get_string()
        return [
            make_map_model_thumb_and_display(
                maps=[os.path.join(self.output_dir, "diffmap.mrc")],
                maps_opacity=[0.5],
                title="FoFc difference map",
                models=[models],
                outputdir=self.output_dir,
            )
        ]
