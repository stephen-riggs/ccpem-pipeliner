#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import mrcfile
import os
from pathlib import Path
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    files_exts,
    BooleanJobOption,
)
from typing import List, Union, Sequence, Tuple, Dict
from pipeliner.display_tools import (
    create_results_display_object,
    make_particle_coords_thumb,
    mrc_thumbnail,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner.nodes import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_IMAGE2DMETADATA,
    NODE_IMAGE2D,
)
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject


class AvgPartPS(PipelinerJob):
    PROCESS_NAME = "pipeliner.image_analysis.average_particle_ps"
    OUT_DIR = "ImageAnalysis"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Aligned average power spectrum"

        self.jobinfo.short_desc = (
            "Create average power spectrum for a set of aligned particles and analyse "
            "features"
        )
        self.jobinfo.long_desc = (
            "Takes a set of aligned particles, sums their power spectra. Then allows "
            "the user to select points on the power spectrum and get their "
            "resolutions. This often makes features such as diffraction spots or "
            "helical layer lines easier to see."
            "Useful in helical reconstruction for determining helical symmetry, or at "
            "least as a starting point for helical symmetry searches. Adapted from a "
            "method by Ambroise Desfosses at University of Auckland"
        )
        self.jobinfo.programs = [
            ExternalProgram(command="relion_display"),
            ExternalProgram(command="relion_star_handler"),
            ExternalProgram(command="relion_image_handler"),
        ]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.references = []
        self.jobinfo.documentation = ""

        self.joboptions["particle_star"] = InputNodeJobOption(
            label="Aligned particle STAR file:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion", "aligned"],
            default_value="",
            pattern=files_exts("Particles file", [".star"]),
            help_text=(
                "The input file should be a Relion particles star file "
                "that contains particles that have been aligned in 2D or 3D"
            ),
            is_required=True,
        )

        self.joboptions["selected_class"] = IntJobOption(
            label="Select particles from class:",
            default_value=1,
            hard_min=1,
            help_text=(
                "The particles in this class will be used. Currently only a single "
                "class is supported."
            ),
        )

        self.joboptions["do_pad"] = BooleanJobOption(
            label="Pad the images for processing?",
            default_value=True,
            help_text=(
                "If 'Yes' the image will be padded to 2x its original size.  This "
                "usually improves results.  Should only be turned off if encountering "
                "out of memory errors"
            ),
        )

        self.joboptions["pixel_size"] = FloatJobOption(
            label="Image pixel size",
            default_value=-1,
            hard_min=-1,
            step_value=1,
            help_text=(
                "The pixel size of the particle images. Use -1 to read from the file "
                "header"
            ),
            in_continue=True,
            is_required=True,
        )

        self.joboptions["do_picking"] = BooleanJobOption(
            label="Manually identify points of interest?",
            default_value=True,
            help_text=(
                "If yes, a manual picking GUI will be launched after the power spectra "
                "are created allowing you to select points and calculate their "
                "resolutions"
            ),
        )

        self.get_runtab_options()

    def get_commands(self) -> List[PipelinerCommand]:
        in_parts = self.joboptions["particle_star"].get_string()
        sel_class = self.joboptions["selected_class"].get_string()

        # files
        class_file = Path(self.output_dir) / "selected_class.star"
        parts_file = Path(self.output_dir) / "aligned_particles.mrcs"
        padded_parts_file = Path(self.output_dir) / "padded_particles.mrcs"
        ps_file = Path(self.output_dir) / "average_ps.mrc"
        selected_coords_file = Path(self.output_dir) / "average_ps_coords.star"
        do_pad = self.joboptions["do_pad"].get_boolean()

        # values
        # get the pixel size from on of the files referenced in the starfile
        ex_data = DataStarFile(in_parts).loop_as_list(
            "particles", ["_rlnClassNumber", "_rlnImageName"]
        )

        example_file = ""
        for part in ex_data:
            if part[0] == sel_class:
                example_file = part[1].split("@")[-1]
                break
        if example_file:
            with mrcfile.open(example_file) as ef:
                apix = round(float(ef.voxel_size["x"]), 3)
                imgsize = ef.data.shape[1]

        # power spectrum size
        ps_size = imgsize * 2 if do_pad else imgsize

        # pixel size
        pixel_size = self.joboptions["pixel_size"].get_number()
        if pixel_size == -1:
            pixel_size = apix

        # commands
        select_command: List[str] = [
            "relion_star_handler",
            "--i",
            in_parts,
            "--o",
            str(class_file),
            "--select",
            "rlnClassNumber",
            "--minval",
            sel_class,
            "--maxval",
            sel_class,
        ]

        stack_command: List[str] = [
            "relion_stack_create",
            "--i",
            str(class_file),
            "--o",
            str(parts_file.with_suffix("")),
            "--apply_transformation",
        ]

        pad_command: List[str] = [
            "relion_image_handler",
            "--i",
            str(parts_file),
            "--o",
            str(padded_parts_file),
            "--new_box",
            2 * imgsize,
        ]
        if not do_pad:
            padded_parts_file = parts_file

        calc_ps_command: List[str] = [
            "python3",
            get_job_script("average_power_spectrum/aps_calc_avg_ps.py"),
            "--stack",
            str(padded_parts_file),
            "--outdir",
            self.output_dir,
        ]

        coms = [PipelinerCommand(select_command), PipelinerCommand(stack_command)]
        if do_pad:
            coms.append(PipelinerCommand(pad_command))
        coms.append(PipelinerCommand(calc_ps_command))

        if self.joboptions["do_picking"].get_boolean() or self.is_continue:
            display_command: List[str] = [
                "relion_display",
                "--i",
                str(ps_file),
                "--pick",
                "--o",
                str(selected_coords_file),
                "--scale",
                "4",
                "--particle_radius",
                "4",
            ]

            get_resolutions_command: List[Union[str, float, int]] = [
                "python3",
                get_job_script("average_power_spectrum/aps_find_res.py"),
                "--ps_size",
                ps_size,
                "--apix",
                pixel_size,
                "--coords",
                str(selected_coords_file),
                "--outdir",
                self.output_dir,
            ]
            coms.extend(
                [
                    PipelinerCommand(display_command),
                    PipelinerCommand(get_resolutions_command),
                ]
            )
            if self.is_continue:
                coms = [
                    PipelinerCommand(display_command),
                    PipelinerCommand(get_resolutions_command),
                ]

        return coms

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "average_ps.mrc", NODE_IMAGE2D, ["power_spectrum", "averaged"]
        )
        if self.joboptions["do_picking"].get_boolean():
            self.add_output_node("selected_features.star", NODE_IMAGE2DMETADATA)

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        ps_file = str(Path(self.output_dir) / "average_ps.mrc")
        sel_file = str(Path(self.output_dir) / "selected_features.star")
        if os.path.isfile(sel_file):
            sel = DataStarFile(sel_file)
            picked_do: ResultsDisplayObject = make_particle_coords_thumb(
                in_mrc=ps_file,
                in_coords=sel_file,
                out_dir=self.output_dir,
                title="Selected power spectrum features",
                markers=True,
            )
            data = sel.loop_as_list("coordinates", headers=True)
            if len(data) > 1:
                summary_do = create_results_display_object(
                    "table",
                    title="Marked features",
                    headers=[x.replace("_rln", "") for x in data[0]],
                    table_data=data[1:],
                    associated_data=[ps_file, sel_file],
                    start_collapsed=False,
                )
                return [summary_do, picked_do]
        else:
            thumbs = Path(self.output_dir) / "Thumbnails"
            thumbs.mkdir(exist_ok=True)
            aps_file = str(thumbs / "average_ps.png")
            mrc_thumbnail(ps_file, 640, aps_file)
            picked_do = create_results_display_object(
                "image",
                title="Averaged particle power spectra",
                image_path=aps_file,
                image_desc="",
                associated_data=[ps_file],
            )
        picked_do.title = "Averaged particle power spectra"
        picked_do.flag = "No features were selected for analysis"
        return [picked_do]

    def gather_metadata(self) -> Dict[str, object]:
        parts_file = Path(self.output_dir) / "aligned_particles.mrcs"
        padded_parts_file = Path(self.output_dir) / "padded_particles.mrcs"
        features_file = Path(self.output_dir) / "selected_PS_coords.star"

        with mrcfile.open(parts_file) as parts:
            total_parts = parts.data.shape[0]
            orig_boxsize = parts.data.shape[1]
            pad_boxsize = orig_boxsize
        if self.joboptions["do_pad"].get_boolean():
            pad_boxsize = mrcfile.read(padded_parts_file).shape[1]

        feat_list = []
        feature_file_error = False
        try:
            features = DataStarFile(str(features_file)).loop_as_list("coordinates")
            for i in features:
                feat_list.append({"x": i[0], "y": i[1], "resolution": i[2]})
        except FileNotFoundError:
            feature_file_error = True

        return {
            "total_particles": total_parts,
            "original_boxsize": orig_boxsize,
            "padded_boxsize": pad_boxsize,
            "selected_features": None if feature_file_error else feat_list,
        }

    def prepare_clean_up_lists(
        self, do_harsh: bool = False
    ) -> Tuple[List[str], List[str]]:
        class_file = Path(self.output_dir) / "selected_class.star"
        parts_file = Path(self.output_dir) / "aligned_particles.mrcs"
        padded_parts_file = Path(self.output_dir) / "padded_particles.mrcs"
        if do_harsh:
            return [str(class_file), str(parts_file), str(padded_parts_file)], []
        return [], []
