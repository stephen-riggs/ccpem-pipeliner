#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os.path
from pathlib import Path
from typing import List, Sequence

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    StringJobOption,
    BooleanJobOption,
    IntJobOption,
    files_exts,
    JobOptionCondition,
    DirPathJobOption,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import mini_montage_from_stack
from pipeliner.nodes import NODE_IMAGE2DSTACK, NODE_MLMODEL
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject


class CinderellaJob(PipelinerJob):
    PROCESS_NAME = "cinderella"
    OUT_DIR = "Cinderella"
    CATEGORY_LABEL = "Automated Class Selection"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.long_desc = (
            "Cinderella is based on a deep learning network to classify class averages,"
            " micrographs or subtomograms into 'good' and 'bad' categories. Cinderella "
            "supports.hdf /.mrcs files for class averages, .mrc files for micrographs, "
            "and.hdf files for subtomograms. It was written to automate cryo-em data"
            " processing. A  pretrained general model is provided for classifying class"
            " averages. But models can also be trained with your own set of classes, "
            "micrographs, and/or subtomograms. with the cinderella.train jobtype"
        )
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Moriya T",
                    "Saur M",
                    "Stabrin M",
                    "Merino F",
                    "H.Voicu H",
                    "Z.Huang Z",
                    "P.A.Penczek PA",
                    "S.Raunser S",
                    "C.Gatsogiannis C",
                ],
                title=(
                    "High-resolution Single Particle Analysis from Electron "
                    "Cryo-microscopy Images Using SPHIRE"
                ),
                journal="J. Vis. Exp.",
                year="2017",
                volume="123",
                issue="21",
                pages="55448",
                doi="10.3791/55448",
            )
        ]
        self.jobinfo.documentation = (
            "https://sphire.mpg.de/wiki/doku.php?id=auto_2d_class_selection"
        )

        self.joboptions["do_gpu"] = BooleanJobOption(
            label="Use GPU?",
            default_value=True,
            help_text=(
                "The GPU version of SPHIRE/Cinderella must be installed if this is"
                " selected"
            ),
        )

        self.joboptions["gpu"] = StringJobOption(
            label="Which GPU to use?",
            default_value="",
            deactivate_if=JobOptionCondition([("do_gpu", "=", False)]),
        )


class CinderellaPredict(CinderellaJob):
    PROCESS_NAME = "cinderella.select.class_averages"
    OUT_DIR = "Cinderella"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Cinderalla Auto 2D class selection"

        self.jobinfo.short_desc = "Automated selection of 2D classes"
        self.jobinfo.programs = [ExternalProgram(command="sp_cinderella_predict.py")]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"

        self.joboptions["input_classes"] = InputNodeJobOption(
            label="Input Class averages to be sorted:",
            node_type=NODE_IMAGE2DSTACK,
            node_kwds=["class_averages"],
            pattern=files_exts("Image stack", [".mrcs", ".hdf"]),
            help_text="Image stack containing the class averages to be sorted",
            is_required=True,
        )

        self.joboptions["trained_model"] = InputNodeJobOption(
            label="Trained model:",
            pattern=files_exts("ML model", [".h5"]),
            help_text=(
                "Trained model for automated class selection. The default model"
                "can be downloaded or a new model trained with your own data"
            ),
            is_required=True,
            node_type=NODE_MLMODEL,
            node_kwds=["cinderella"],
        )

        self.joboptions["weight"] = FloatJobOption(
            label="Confidence cutoff",
            default_value=0.7,
            hard_min=0.0,
            hard_max=1.0,
            step_value=0.05,
            help_text=(
                "Classes with a confidence larger than this cutoff will be classified "
                "as 'good'"
            ),
            is_required=True,
        )

        self.get_runtab_options(mpi=False, threads=False)

        self.set_joboption_order(
            ["input_classes", "trained_model", "weight", "do_gpu", "gpu"]
        )

    def create_output_nodes(self) -> None:
        fn_in = self.joboptions["input_classes"].get_string()
        stem = Path(fn_in).stem
        self.add_output_node(f"{stem}_good.hdf", NODE_IMAGE2DSTACK, ["class_averages"])
        self.add_output_node(
            f"{stem}_bad.hdf", NODE_IMAGE2DSTACK, ["class_averages", "bad"]
        )

    def get_commands(self) -> List[PipelinerCommand]:
        command = ["sp_cinderella_predict.py", "-i"]

        fn_in = self.joboptions["input_classes"].get_string()
        command.append(fn_in)

        model = self.joboptions["trained_model"].get_string()
        command.extend(["-w", model])

        weight = self.joboptions["weight"].get_number()
        command.extend(["-t", str(weight)])

        command.extend(["-o", self.output_dir])

        do_gpu = self.joboptions["do_gpu"].get_boolean()
        if do_gpu:
            command.append("--gpu")
            gpu = self.joboptions["gpu"].get_string()
            if gpu:
                command.append(gpu)

        return [PipelinerCommand(command)]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        infile = self.joboptions["input_classes"].get_string()
        good_name = os.path.basename(infile).split(".")[0] + "_good.hdf"
        bad_name = os.path.basename(infile).split(".")[0] + "_bad.hdf"
        good_clavs = os.path.join(self.output_dir, good_name)
        bad_clavs = os.path.join(self.output_dir, bad_name)
        good_mm = mini_montage_from_stack(
            stack_file=good_clavs,
            outputdir=self.output_dir,
            nimg=-1,
            title="Selected classes",
        )
        bad_mm = mini_montage_from_stack(
            stack_file=bad_clavs,
            outputdir=self.output_dir,
            nimg=-1,
            title="Rejected classes",
        )

        return [good_mm, bad_mm]


class CinderellaTrain(CinderellaJob):
    PROCESS_NAME = "cinderella.select.class_averages.train"
    OUT_DIR = "Cinderella"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Train Cinderella model"

        self.jobinfo.short_desc = "Train a model for Cinderella 2D class selecton"
        self.jobinfo.programs = [ExternalProgram(command="sp_cinderella_train.py")]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"

        self.joboptions["input_size"] = IntJobOption(
            label="Resize input images to:",
            default_value=64,
            hard_min=1,
            is_required=True,
            help_text="This is the image size to which each class is resized to",
        )
        self.joboptions["mask_radius"] = IntJobOption(
            label="Mask radius",
            default_value=-1,
            hard_min=-1,
            help_text="Circular mask radius which is applied after resizing to the"
            " input size. If not given, it uses 0.4*input_size as default.",
            is_required=True,
        )

        self.joboptions["batch_size"] = IntJobOption(
            label="Batch Size",
            default_value=32,
            hard_min=1,
            help_text="How many classes are in one mini-batch. If you have memory"
            " problems, you can try to reduce this value",
            is_required=True,
        )

        self.joboptions["good_path"] = DirPathJobOption(
            label="Directory containing 'good' class averages:",
            help_text="Path to folder with good classes",
            is_required=True,
        )

        self.joboptions["bad_path"] = DirPathJobOption(
            label="Directory containing 'bad' class averages:",
            help_text="Path to folder with bad classes",
            is_required=True,
        )

        self.joboptions["pretrained_weights"] = StringJobOption(
            label="Path to pretrained weights:",
            help_text="Path to weights that are used to initialize the network. "
            "It can be empty. As Cinderella is using the same network architecture as "
            "crYOLO, we are typically using the general network of crYOLO as pretrained"
            "weights.",
        )

        self.joboptions["nb_epoch"] = IntJobOption(
            label="Maxinum epochs to train",
            default_value=100,
            hard_min=1,
            help_text="Maximum number of epochs to train. However, it will stop earlier"
            " (see nb_early_stop)",
            is_required=True,
        )

        self.joboptions["nb_early_stop"] = IntJobOption(
            label="Stop if no improvement after:",
            default_value=15,
            hard_min=1,
            help_text="If the validation loss did not improve “nb_early_stop” times in"
            "a row, the training will stop automatically.",
            is_required=True,
        )

    def create_output_nodes(self) -> None:
        self.add_output_node("trained_model.h5", NODE_MLMODEL, ["cinderella"])

    def get_commands(self) -> List[PipelinerCommand]:
        # make config file
        mask_radius = self.joboptions["mask_radius"].get_number()
        mr = "no_mr" if mask_radius == -1 else str(mask_radius)

        gp = self.joboptions["good_path"].get_string()
        gp += "/" if gp[-1] != "/" else gp

        bp = self.joboptions["bad_path"].get_string()
        if not os.path.isdir(bp):
            raise ValueError(f"Bad classes dir: {bp} not found")
        bp += "/" if bp[-1] != "/" else bp

        ptw = self.joboptions["pretrained_weights"].get_string()
        if not ptw:
            ptw = "no_ptw"
        else:
            ptw += "/" if ptw[-1] != "/" else ptw

        config_command = [
            "python3",
            get_job_script("cinderella/make_cinderella_config.py"),
            self.joboptions["input_size"].get_string(),
            mr,
            self.joboptions["batch_size"].get_string(),
            gp,
            bp,
            ptw,
            self.output_dir,
            self.joboptions["nb_epoch"].get_string(),
            self.joboptions["nb_early_stop"].get_string(),
        ]

        # run training
        config = os.path.join(self.output_dir, "training_config.json")
        cin_command = ["sp_cinderella_train.py", "-c", config]
        do_gpu = self.joboptions["do_gpu"].get_boolean()
        if do_gpu:
            cin_command.append("--gpu")
            gpu = self.joboptions["gpu"].get_string()
            if gpu:
                cin_command.append(gpu)

        return [PipelinerCommand(config_command), PipelinerCommand(cin_command)]
