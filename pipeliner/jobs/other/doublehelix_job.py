#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
from typing import List, Sequence
from pipeliner.pipeliner_job import (
    PipelinerJob,
    ExternalProgram,
    PipelinerCommand,
)
from pipeliner.job_options import (
    InputNodeJobOption,
    files_exts,
    StringJobOption,
    BooleanJobOption,
    JobOptionCondition,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
    make_map_model_thumb_and_display,
)
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
    NODE_LOGFILE,
    NODE_SEQUENCE,
    NODE_RESTRAINTS,
)
from pipeliner.utils import get_job_script
from pipeliner.results_display_objects import ResultsDisplayObject


class ModelSequenceAssignNA(PipelinerJob):
    PROCESS_NAME = "doublehelix.atomic_model_utilities.assign_sequence"
    OUT_DIR = "DoubleHelix"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "DoubleHelix"
        self.jobinfo.short_desc = (
            "Assign and build side-chains in RNA/DNA models based on map features"
        )
        self.jobinfo.long_desc = (
            "Find RNA/DNA model sequence (side-chains), given:\n"
            "the model backbone and \n"
            "experimental map used to build the model.\n"
            "Given an input sequence, the program can also assign side-chains and \n"
            "builds these in the backbone model. \n"
        )

        self.jobinfo.programs = [
            ExternalProgram("doubleHelix"),
        ]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Grzegorz Chojnowski"
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Chojnowski G",
                ],
                title=(
                    "doubleHelix: nucleic acid sequence identification, assignment"
                    " and validation tool for cryo-EM and crystal structure models"
                ),
                journal="Nucleic Acids Research",
                year="2023",
                volume="51(15)",
                pages="8255–8269",
                doi="10.1093/nar/gkad553",
            ),
        ]
        self.jobinfo.documentation = "https://doi.org/10.1107/S2052252521011088"

        # JOB OPTIONS
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb", ".ent"]),
            default_value="",
            help_text="The input main-chain model (side-chains ignored)",
            is_required=True,
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".ccp4", ".map"]),
            help_text="The input map to extract side-chain features",
            is_required=False,
            required_if=JobOptionCondition([("restraints", "=", False)]),
            deactivate_if=JobOptionCondition([("restraints", "=", True)]),
        )

        self.joboptions["chain_selection"] = StringJobOption(
            label="Chain selection",
            default_value="",
            help_text="e.g. A.10:100",
            is_required=False,
            validation_regex=(
                "^([A-Za-z0-9]+\\.[A-Za-z0-9]+\\:[A-Za-z0-9]+|[A-Za-z0-9]+)$"
            ),
            regex_error_message="please input selection in the format e.g. A.10:100",
        )

        self.joboptions["assign_sequence"] = BooleanJobOption(
            label="Assign given sequence?",
            default_value=False,
            help_text=(
                "If Yes, input sequence will be assigned and "
                "side-chains built in the output model"
            ),
            deactivate_if=JobOptionCondition([("restraints", "=", True)]),
        )

        self.joboptions["heteromultimer_mode"] = BooleanJobOption(
            label="Target is a hetero-multimer",
            default_value=False,
            help_text=(
                "Identifies best-matching sequence for "
                "each chain fragment in input model"
            ),
            deactivate_if=JobOptionCondition([("restraints", "=", True)]),
        )

        self.joboptions["dna_mode"] = BooleanJobOption(
            label="Target molecule is a DNA",
            default_value=False,
            help_text="Specify if the target molecule is DNA or RNA",
        )

        self.joboptions["hmm_mode"] = BooleanJobOption(
            label="Ignore secondary structure in sequence search",
            default_value=False,
            help_text=(
                "Use HMM insetad of CM in INFERNAL. It's faster, but "
                "ignores secondary strucutre information "
                "and may produce suboptimal results."
            ),
            deactivate_if=JobOptionCondition([("restraints", "=", True)]),
        )

        self.joboptions["restraints"] = BooleanJobOption(
            label="Generate refinement restraints",
            default_value=False,
            help_text=(
                "Generates secondary strcture restraints "
                "based on DNA/RNA backbone geometry only. "
                "Base conformations and identities are ignored."
            ),
        )

        self.joboptions["input_sequence"] = InputNodeJobOption(
            label="Input sequence",
            node_type=NODE_SEQUENCE,
            default_value="",
            pattern=files_exts("Seq res", [".fasta", ".fa", ".fas"]),
            help_text="Sequence to assign. Only one chain should be given",
            is_required=False,
            required_if=JobOptionCondition(
                [("assign_sequence", "=", True), ("restraints", "=", False)],
                operation="ALL",
            ),
            deactivate_if=JobOptionCondition(
                [("assign_sequence", "=", False), ("restraints", "=", True)]
            ),
        )

        self.joboptions["input_sequence_db"] = InputNodeJobOption(
            label="Input search database",
            node_type=NODE_SEQUENCE,
            default_value="",
            pattern=files_exts("Seq res", [".fasta", ".fa", ".fas"]),
            help_text=(
                "Sequence database to search against in fasta format"
                "required if Input sequence is not provided"
            ),
            is_required=False,
            required_if=JobOptionCondition(
                [("assign_sequence", "=", False), ("restraints", "=", False)],
                operation="ALL",
            ),
            deactivate_if=JobOptionCondition(
                [("assign_sequence", "=", True), ("restraints", "=", True)]
            ),
        )

        self.get_runtab_options()

    def create_output_nodes(self) -> None:
        # Get options needed to decide which output nodes will be made
        input_model = self.joboptions["input_model"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        input_map = self.joboptions["input_map"].get_string()

        if self.joboptions["restraints"].get_boolean() is False:
            mapid = os.path.splitext(os.path.basename(input_map))[0]
            # Now make the nodes
            self.add_output_node(
                modelid + "_" + mapid + "doublehelix.json",
                NODE_LOGFILE,
                ["doublehelix", "json_out"],
            )
            # add output PDB node
            # ONLY if requested, datafield may !=''
            if self.joboptions["assign_sequence"].get_boolean():
                self.add_output_node(
                    modelid + "_doublehelix.pdb",
                    NODE_ATOMCOORDS,
                    ["doublehelix", "model_out"],
                )

        else:
            self.add_output_node(
                modelid + "_doubleHelix_restraints_refmac.txt",
                NODE_RESTRAINTS,
                ["doublehelix", "restraints_out"],
            )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir

        # Get parameters
        input_model = self.joboptions["input_model"].get_string()
        input_model = os.path.relpath(input_model, self.working_dir)
        modelid = os.path.splitext(os.path.basename(input_model))[0]

        restraints = self.joboptions["restraints"].get_boolean()
        # input map is NOT needed for generating restraints
        if restraints:
            input_map = None
            mapid = "None"
        else:
            input_map = self.joboptions["input_map"].get_string()
            input_map = os.path.relpath(input_map, self.working_dir)
            mapid = os.path.splitext(os.path.basename(input_map))[0]

        dna_mode = self.joboptions["dna_mode"].get_boolean()
        hmm_mode = self.joboptions["hmm_mode"].get_boolean()
        heteromultimer_mode = self.joboptions["heteromultimer_mode"].get_boolean()

        # check if a proteome should be downloaded from Uniprot by ID
        # ONLY is "assign_sequence" is False!
        # data fileds are not cleanet
        if self.joboptions["assign_sequence"].get_boolean():
            input_seq_db = ""
            input_seq = self.joboptions["input_sequence"].get_string()
            if input_seq:
                input_seq = os.path.relpath(input_seq, self.working_dir)
        else:
            input_seq = ""
            input_seq_db = self.joboptions["input_sequence_db"].get_string()
            if input_seq_db:
                input_seq_db = os.path.relpath(input_seq_db, self.working_dir)

        chain_selection = self.joboptions["chain_selection"].get_string()
        # checkmysequence
        commands = self.get_doublehelix_commands(
            input_map,
            input_model,
            input_seq_db,
            input_seq,
            chain_selection,
            modelid,
            mapid,
            dna_mode,
            hmm_mode,
            restraints,
            heteromultimer_mode,
        )
        return [PipelinerCommand(x) for x in commands]

    @staticmethod
    def get_doublehelix_commands(
        input_map,
        input_model,
        input_seq_db,
        input_sequence,
        chain_selection,
        modelid,
        mapid,
        dna_mode,
        hmm_mode,
        restraints,
        heteromultimer_mode,
    ) -> List[List[str]]:
        # pdb_num = 1  # this needs to be set for multiple model input
        doublehelix_command = ["doubleHelix"]
        doublehelix_command += ["--modelin", input_model]

        if restraints:
            if dna_mode:
                doublehelix_command += ["--type", "DNA"]
            else:
                doublehelix_command += ["--type", "RNA"]

            doublehelix_command += ["--restraints"]

            return [
                doublehelix_command,
            ]

        if input_map:
            doublehelix_command += ["--mapin", input_map]

        if input_sequence:
            doublehelix_command += ["--seqin", input_sequence]
            model_out = modelid + "_doublehelix.pdb"
            doublehelix_command += ["--modelout", model_out]
            doublehelix_command += ["--assign"]
        elif input_seq_db:
            doublehelix_command += ["--seqin", input_seq_db]
            doublehelix_command += ["--identify"]

        if heteromultimer_mode:
            doublehelix_command += ["--multimer"]

        if hmm_mode:
            doublehelix_command += ["--hmm"]

        if dna_mode:
            doublehelix_command += ["--type", "DNA"]
        else:
            doublehelix_command += ["--type", "RNA"]

        if chain_selection:
            chain_string = ""
            chain_sel_strip = chain_selection.strip()
            if "." in chain_sel_strip:
                chain_id = chain_sel_strip.split(".")[0]
                chain_string = "chain {}".format(chain_id)
                if ":" in chain_sel_strip:
                    res_sel = chain_sel_strip.split(".")[1]
                    resi_start = res_sel.split(":")[0].strip()
                    resi_end = res_sel.split(":")[1].strip()
                    chain_string += f" and (resi {resi_start}:{resi_end})"
            else:
                chain_id = chain_sel_strip.split()[0]
                chain_string = "chain {}".format(chain_id)
            if chain_string:
                doublehelix_command += ["--select", chain_string]
        output_json = modelid + "_" + mapid + "doublehelix.json"
        doublehelix_command += ["--jsonout", output_json]
        doublehelix_results_command = [
            "python3",
            get_job_script("doublehelix/get_doublehelix_results.py"),
            "-j",
            output_json,
            "-id",
            modelid + "_" + mapid,
        ]
        return [
            doublehelix_command,
            doublehelix_results_command,
        ]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        # TODO: long files names to be trimmed ?
        input_model = self.joboptions["input_model"].get_string()
        input_map = self.joboptions["input_map"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        mapid = os.path.splitext(os.path.basename(input_map))[0]
        input_seq = self.joboptions["input_sequence"].get_string()
        display_objects: List[ResultsDisplayObject] = []
        self.create_doublehelix_report(self.output_dir, modelid, mapid, display_objects)
        if input_seq and self.joboptions["restraints"].get_boolean() is False:
            model_out = modelid + "_doublehelix.pdb"
            model_out = os.path.join(self.output_dir, model_out)
            map_model_display = make_map_model_thumb_and_display(
                maps=[input_map],
                maps_opacity=[0.5],
                models=[model_out],
                title="Model side-chains built",
                outputdir=self.output_dir,
            )
            display_objects.append(map_model_display)
        return display_objects

    def create_doublehelix_report(
        self, output_dir, modelid, mapid, display_objects
    ) -> None:

        if self.joboptions["restraints"].get_boolean():
            results_out = os.path.join(
                output_dir, modelid + "_doubleHelix_basepairs.txt"
            )

            restraints_out = os.path.join(
                output_dir,
                modelid + "_doubleHelix_restraints_refmac.txt",
            )
            if self.joboptions["dna_mode"].get_boolean():
                na_type = "DNA"
            else:
                na_type = "RNA"

            with open(results_out, "r") as ifile:
                parsed_txt = [
                    "doubleHelix identified the following secondary structure",
                    f"interactions based on the geometry of input {na_type}",
                    "model backbone, ignoring base geometries and identities.",
                    "\n",
                    "They are encoded as refinement restrains for Refmac or COOT",
                    f"in a output file:\n{restraints_out}",
                    "\n\n",
                ]
                for line in ifile:
                    if line.startswith("#"):
                        continue
                    parsed_txt.append(line.strip())

                disp_obj = create_results_display_object(
                    "text",
                    title="doubleHelix Results",
                    display_data="\n".join(parsed_txt),
                    associated_data=[results_out],
                    start_collapsed=False,
                )
                display_objects.append(disp_obj)

            return

        results_out = os.path.join(
            output_dir, modelid + "_" + mapid + "_doublehelixparse.json"
        )
        with open(results_out, "r") as j:
            dict_out = json.load(j)
        if not dict_out["doublehelix_results"]:
            display_data = "No sequence found or assigned, check logs for details"
        else:
            display_data = dict_out["doublehelix_results"]

        disp_obj = create_results_display_object(
            "text",
            title="doubleHelix Results",
            display_data=display_data,
            associated_data=[results_out],
            start_collapsed=False,
        )
        display_objects.append(disp_obj)

    def create_post_run_output_nodes(self) -> None:

        if self.joboptions["restraints"].get_boolean():
            return

        input_model = self.joboptions["input_model"].get_string()
        input_map = self.joboptions["input_map"].get_string()
        modelid = os.path.splitext(os.path.basename(input_model))[0]
        mapid = os.path.splitext(os.path.basename(input_map))[0]

        results_out = os.path.join(
            self.output_dir, modelid + "_" + mapid + "_doublehelixparse.json"
        )
        with open(results_out, "r") as j:
            dict_out = json.load(j)
        if dict_out:
            # try to add output nodes with sequence hits (if any)
            for idx, hit_ev in enumerate(
                sorted(dict_out.get("doublehelix_hits", {}), reverse=False)
            ):
                hit_out = os.path.join(
                    self.output_dir, f"sequence_no{idx+1}" + ".fasta"
                )

                with open(hit_out, "w") as ofile:
                    ofile.write(dict_out["doublehelix_hits"][hit_ev])

                # Now, make the nodes
                self.add_output_node(
                    f"sequence_no{idx+1}" + ".fasta",
                    NODE_SEQUENCE,
                    ["doublehelix", "fasta_out"],
                )
