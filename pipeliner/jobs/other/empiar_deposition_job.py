#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from glob import glob
import os
from typing import List, Sequence

from pipeliner.pipeliner_job import PipelinerJob, Ref, PipelinerCommand
from pipeliner.job_options import (
    MultipleChoiceJobOption,
    StringJobOption,
    BooleanJobOption,
    MultiStringJobOption,
    JobOptionCondition,
    JobOptionValidationResult,
    InputNodeJobOption,
)
from pipeliner.data_structure import ISO3166_COUNTRIES
from pipeliner.utils import get_job_script, clean_job_dirname
from pipeliner.nodes import NODE_PROCESSDATA, NODE_IMAGE2D
from pipeliner.display_tools import create_results_display_object
from pipeliner.results_display_objects import ResultsDisplayObject

release_options = {
    "Directly after the submission has been processed": "RE",
    "After the related EMDB entry has been released": "EP",
    "After the related primary citation has been published": "HP",
    "Delay release of entry by one year from the date of deposition": "HO",
}


class EmpiarDepositionJob(PipelinerJob):
    PROCESS_NAME = "pipeliner.deposition.empiar"
    OUT_DIR = "Deposition"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Prepare EMPIAR deposition"

        self.jobinfo.short_desc = "Prepare a deposition for EMPIAR"
        self.jobinfo.long_desc = (
            "Prepare a deposition to the EMPIAR data base from an existing job. "
            "EMPIAR depositions must first be arranged with EMPIAR staff "
        )
        self.jobinfo.programs = []
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Iudin A",
                    "Korir PK",
                    "Somasundharam S",
                    "Weyand S",
                    "Cattavitello C",
                    "Fonseca N",
                    "Salih O",
                    "Kleywegt GJ",
                    "Patwardhan A",
                ],
                title="EMPIAR: the Electron Microscopy Public Image Archive",
                journal="Nucleic Acids Res.",
                year="2023",
                volume="51",
                pages="D1503-D1511",
                doi="https://doi.org/10.1093/nar/gkac1062",
            )
        ]
        self.jobinfo.documentation = (
            "https://www.ebi.ac.uk/empiar/deposition/manual/#manDepAutomatedDeposition"
        )

        choice_search = [os.path.dirname(x) for x in glob("*/job*/job.star")]
        choice_search.sort()

        self.joboptions["input_job"] = MultipleChoiceJobOption(
            label="Job to create deposition from:",
            choices=choice_search if len(choice_search) else ["No jobs available"],
            default_value_index=0,
        )

        self.joboptions["dep_raw_mics"] = BooleanJobOption(
            label="Deposit raw micrographs (if available)?",
            default_value=True,
            help_text="Raw un-corrected micrographs will be deposited if available",
        )
        self.joboptions["dep_corr_mics"] = BooleanJobOption(
            label="Deposit corrected micrographs (if available)?",
            default_value=True,
            help_text=(
                "Motion corrected and merged micrographs will be deposited, if "
                "available"
            ),
        )
        self.joboptions["dep_parts"] = BooleanJobOption(
            label="Deposit particles (if available)?",
            default_value=True,
            help_text="The last set of particles used will be deposited, if available",
        )
        self.joboptions["dep_corr_parts"] = BooleanJobOption(
            label="Deposit polished particles (if available)?",
            default_value=True,
            help_text=(
                "The last set of 'polished' particles used will be deposited, "
                "if available"
            ),
        )
        self.joboptions["empiar_token"] = StringJobOption(
            label="EMPIAR API token or username",
            help_text=(
                "This token is provided by EMPIAR when creating a deposition, "
                "alternatively your EMPIAR user name can be used, in which case you "
                "will be prompted for your password.  The deposition cannont be "
                "completed automatically if this field is left empty"
            ),
            jobop_group="EMPIAR parameters",
        )
        self.joboptions["empiar_transfer_password"] = StringJobOption(
            label="EMPIAR transfer password",
            help_text=(
                "The EMPIAR team will provide you with a transfer password. Please "
                "note that this is not the API token, and is a separate password from"
                " the one that you create when registering EMPIAR user. The deposition "
                "cannont be completed automatically if this field is left empty"
            ),
            jobop_group="EMPIAR parameters",
        )
        self.joboptions["entry_title"] = StringJobOption(
            label="Entry title",
            is_required=True,
        )
        self.joboptions["thumbnail_file"] = InputNodeJobOption(
            label="Thumbnail image",
            is_required=False,
            help_text=(
                "Thumbnail images can be created from the pipeliner results display. "
                "Should be in .png format"
            ),
            validation_regex="^[\\w\\/]*.png$",
            regex_error_message="File must contain `.png`",
            jobop_group="Deposition info",
            node_type=NODE_IMAGE2D,
        )
        self.joboptions["release_status"] = MultipleChoiceJobOption(
            label="Release options",
            choices=list(release_options),
            default_value_index=0,
            jobop_group="Deposition info",
        )
        self.joboptions["emdb_ref"] = MultiStringJobOption(
            label="EMDB reference: EMD-",
            validation_regex="^\\d*$",
            regex_error_message="Enter only the numbers for the entry ID",
            jobop_group="Deposition info",
        )
        self.joboptions["empiar_ref"] = MultiStringJobOption(
            label="Related EMPIAR entries: EMPIAR-",
            validation_regex="^\\d*$",
            regex_error_message="Enter only the numbers for the entry ID",
            jobop_group="Deposition info",
        )
        self.joboptions["entry_authors"] = MultiStringJobOption(
            label="Entry authors (Surname, Initials, optional:ORCID ID )",
            help_text=(
                "The names of the authors for the EMPIAR entry, Might be different "
                "Than the authors of the publication. ORCID IDs are optional"
            ),
            deactivate_if=JobOptionCondition([("upload_entry_author_file", "!=", "")]),
            required_if=JobOptionCondition([("upload_entry_author_file", "=", "")]),
            jobop_group="Entry authors",
            validation_regex=(
                "^\\w*,\\s*\\w*,\\s*(\\d{16}|\\d{4}-\\d{4}-\\d{4}-\\d{4}$)|^"
                "\\w*,\\s*\\w*$"
            ),
            regex_error_message=(
                "Invalid entry format (Surname, Initials, ORCID [optional])"
            ),
        )
        self.joboptions["upload_entry_author_file"] = InputNodeJobOption(
            label="OR: upload entry authors in a csv file:",
            help_text=(
                "Entry authors list can also be uploaded as as csv file in the format"
                "Surname, Initials, OrcidID"
            ),
            deactivate_if=JobOptionCondition([("entry_authors", "!=", "")]),
            jobop_group="Entry authors",
            validation_regex="^[\\w\\/]*.csv$",
            regex_error_message="File must end with '.csv'",
            node_type=NODE_PROCESSDATA,
            node_kwds=["authorlist"],
        )
        self.joboptions["ca_surname"] = StringJobOption(
            label="Corresponding author surname",
            jobop_group="Corresponding author",
            is_required=True,
        )
        self.joboptions["ca_first_name"] = StringJobOption(
            label="Corresponding author first name",
            jobop_group="Corresponding author",
            is_required=True,
        )
        self.joboptions["ca_middle_name"] = StringJobOption(
            label="Corresponding author middle name",
            jobop_group="Corresponding author",
        )
        self.joboptions["ca_orcid"] = StringJobOption(
            label="Corresponding author orcid ID:",
            jobop_group="Corresponding author",
            validation_regex="^\\d{16}|\\d{4}-\\d{4}-\\d{4}-\\d{4}$",
            regex_error_message="Invalid 16 digit ORCID ID",
        )
        self.joboptions["ca_org"] = StringJobOption(
            label="Corresponding author organisation:",
            jobop_group="Corresponding author",
            is_required=True,
        )
        self.joboptions["ca_email"] = StringJobOption(
            label="Corresponding author email:",
            jobop_group="Corresponding author",
            is_required=True,
            validation_regex="^\\w*@[\\w.]*$",
            regex_error_message="Email must contain '@'",
        )
        self.joboptions["ca_address"] = StringJobOption(
            label="Corresponding author street address:",
            jobop_group="Corresponding author",
        )
        self.joboptions["ca_city"] = StringJobOption(
            label="Corresponding author city:",
            jobop_group="Corresponding author",
        )
        self.joboptions["ca_state"] = StringJobOption(
            label="Corresponding author state/province:",
            jobop_group="Corresponding author",
        )
        self.joboptions["ca_postcode"] = StringJobOption(
            label="Corresponding author post code:",
            jobop_group="Corresponding author",
        )
        self.joboptions["ca_country"] = MultipleChoiceJobOption(
            label="Corresponding author country:",
            choices=[""] + list(ISO3166_COUNTRIES),
            default_value_index=0,
            jobop_group="Corresponding author",
        )
        self.joboptions["ca_tel"] = StringJobOption(
            label="Corresponding author telephone:",
            jobop_group="Corresponding author",
            validation_regex="[0-9-+\\s]",
            regex_error_message="Invalid telephone number format",
        )

        self.joboptions["pi_surname"] = StringJobOption(
            label="Principal investigator surname",
            jobop_group="Principal investigator",
            is_required=True,
        )
        self.joboptions["pi_first_name"] = StringJobOption(
            label="Principal investigator first name",
            jobop_group="Principal investigator",
            is_required=True,
        )
        self.joboptions["pi_middle_name"] = StringJobOption(
            label="Principal investigator middle name",
            jobop_group="Principal investigator",
        )
        self.joboptions["pi_orcid"] = StringJobOption(
            label="Principal investigator orcid ID:",
            jobop_group="Principal investigator",
            validation_regex="^\\d{16}|\\d{4}-\\d{4}-\\d{4}-\\d{4}$",
            regex_error_message="Invalid 16 digit ORCID ID",
        )
        self.joboptions["pi_org"] = StringJobOption(
            label="Principal investigator organisation:",
            jobop_group="Principal investigator",
            is_required=True,
        )
        self.joboptions["pi_email"] = StringJobOption(
            label="Principal investigator email:",
            jobop_group="Principal investigator",
            is_required=True,
            validation_regex="^\\w*@[\\w.]*$",
            regex_error_message="Email must contain '@'",
        )
        self.joboptions["pi_address"] = StringJobOption(
            label="Principal investigator street address:",
            jobop_group="Principal investigator",
        )
        self.joboptions["pi_city"] = StringJobOption(
            label="Principal investigator city:",
            jobop_group="Principal investigator",
        )
        self.joboptions["pi_state"] = StringJobOption(
            label="Principal investigator state/province:",
            jobop_group="Principal investigator",
        )
        self.joboptions["pi_postcode"] = StringJobOption(
            label="Principal investigator post code:",
            jobop_group="Principal investigator",
        )
        self.joboptions["pi_country"] = MultipleChoiceJobOption(
            label="Principal investigator country:",
            choices=["Select a country"] + list(ISO3166_COUNTRIES),
            default_value_index=0,
            jobop_group="Principal investigator",
        )
        self.joboptions["pi_tel"] = StringJobOption(
            label="Principal investigator telephone:",
            jobop_group="Principal investigator",
            validation_regex="[0-9-+\\s]",
            regex_error_message="Invalid telephone number format",
        )
        self.joboptions["citation_file"] = InputNodeJobOption(
            label="Upload JSON file containing citation info",
            help_text="See the format instructions in the pipeliner documentation",
            jobop_group="Citation",
            validation_regex="^[\\w\\/]*.json$",
            regex_error_message="File must end in '.json'",
            node_type=NODE_PROCESSDATA,
            node_kwds=["citation"],
        )
        self.joboptions["citation_authors"] = MultiStringJobOption(
            label="Citation authors (Surname, Initials, optional:ORCID ID )",
            help_text=(
                "The names of the authors for the EMPIAR entry, Might be different "
                "Than the authors of the publication"
            ),
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
            validation_regex=(
                "^\\w*,\\s*\\w*,\\s*(\\d{16}|\\d{4}-\\d{4}-\\d{4}-\\d{4}$)|"
                "^\\w*,\\s*\\w*$"
            ),
            regex_error_message=(
                "Invalid entry format (Surname, Initials, ORCID [optional])"
            ),
        )
        self.joboptions["editors"] = MultiStringJobOption(
            label="Editors: surname, initials",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
            validation_regex="^\\w*,\\s*\\w*$",
            regex_error_message="Invalid entry format (Surname, Initials)",
        )
        self.joboptions["published"] = BooleanJobOption(
            label="Has the citation been published?",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
            default_value=False,
        )
        self.joboptions["j_or_nj_citation"] = BooleanJobOption(
            label="Is the citation in an academic journal?",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
            default_value=False,
        )
        self.joboptions["citation_title"] = StringJobOption(
            label="Citation title",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["year"] = StringJobOption(
            label="Year of publication",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
            validation_regex="^\\d{4}$",
            regex_error_message="Invalid year format",
        )
        self.joboptions["journal"] = StringJobOption(
            label="Journal",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["journal_abbrev"] = StringJobOption(
            label="Journal Abbreviation",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["volume"] = StringJobOption(
            label="Volume",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["issue"] = StringJobOption(
            label="Issue",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["first_page"] = StringJobOption(
            label="First page",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["last_page"] = StringJobOption(
            label="Last page",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["doi"] = StringJobOption(
            label="doi",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
            validation_regex="^10.[\\w\\/.]*$",
            regex_error_message="Invalid format; DOI must start with '10.'",
        )
        self.joboptions["pubmedid"] = StringJobOption(
            label="PubMed ID",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
            validation_regex="^\\d{1,8}$",
            regex_error_message="Invalid PMID format: 1-8 digits",
        )
        self.joboptions["details"] = StringJobOption(
            label="Publication details",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["book_chapter_title"] = StringJobOption(
            label="Book chapter and title",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["publisher"] = StringJobOption(
            label="Publisher",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["pub_location"] = StringJobOption(
            label="Publication location",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["cite_country"] = MultipleChoiceJobOption(
            label="Publication country",
            choices=[""] + list(ISO3166_COUNTRIES),
            default_value_index=0,
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            is_required=False,
            required_if=JobOptionCondition([("citation_title", "!=", "")]),
            jobop_group="Citation",
        )
        self.joboptions["language"] = StringJobOption(
            label="Publication Language",
            deactivate_if=JobOptionCondition([("citation_file", "!=", "")]),
            jobop_group="Citation",
        )

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        input_job = self.joboptions["input_job"].get_string()
        if input_job == "No jobs available":
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["input_job"]],
                    message="No jobs available for creating a deposition",
                )
            ]
        return []

    def get_commands(self) -> List[PipelinerCommand]:
        commands = [
            PipelinerCommand(
                [
                    "python3",
                    get_job_script("deposition/make_empiar_deposition.py"),
                    "--terminal_job",
                    clean_job_dirname(self.joboptions["input_job"].get_string()),
                    "--jobstar_file",
                    os.path.join(self.output_dir, "job.star"),
                ]
            )
        ]

        dtype_coms = {
            self.joboptions["dep_raw_mics"]: "--movies",
            self.joboptions["dep_corr_mics"]: "--mics",
            self.joboptions["dep_parts"]: "--parts",
            self.joboptions["dep_corr_parts"]: "--corr_parts",
        }
        for jo in dtype_coms:
            if jo.get_boolean():
                commands[0].com.append(dtype_coms[jo])

        thumb_file = self.joboptions["thumbnail_file"].get_string()
        if thumb_file:
            commands.append(
                PipelinerCommand(
                    ["cp", thumb_file, os.path.join(self.output_dir, thumb_file)]
                )
            )

        # make the script command
        commands.append(
            PipelinerCommand(
                [
                    "python3",
                    get_job_script("deposition/make_empiar_submission_script.py"),
                    "--jobstar_file",
                    os.path.join(self.output_dir, "job.star"),
                ]
            )
        )
        return commands

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "deposition_data_EMPIAR.json",
            NODE_PROCESSDATA,
            ["deposition_data", "empiar"],
        )

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        result_text = os.path.join(self.output_dir, ".result_text.txt")
        message = create_results_display_object("textfile", file_path=result_text)
        output = self.output_nodes[0].default_results_display(self.output_dir)
        return [message, output]
