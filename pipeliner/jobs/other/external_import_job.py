#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import json
import os
from typing import List, Dict, Sequence, Tuple
from textwrap import dedent

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import InputNodeJobOption
from pipeliner.node_factory import create_node
from pipeliner.nodes import Node, NODE_PROCESSDATA
from pipeliner.display_tools import create_results_display_object
from pipeliner.results_display_objects import ResultsDisplayObject


class ExternalWorkflowImportJob(PipelinerJob):
    PROCESS_NAME = "pipeliner.workflow.import_external"
    OUT_DIR = "Import"

    def __init__(self):
        super().__init__()

        self.jobinfo.display_name = "Import external workflow"
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.short_desc = "Import results from outside the pipeliner"
        self.jobinfo.long_desc = (
            "This job is used to incorporate the results of external processing where"
            "data was removed from the pipeliner, processed with another program and "
            "then re-incorporated into the pipeliner.\n\n"
            "A json file with a specific format is required. This can be written "
            "manually or generated with a script."
            + dedent(
                """
            The format for this file is:
            {
                'InputNodes':{
                    '<file_name>': ['<node_type>', [kwds]],
                    '<file_name>': ['<node_type>', [kwds]]
                },
                'OutputNodes':{
                    '<file_name>': ['<node_type>', [kwds]],
                    '<file_name>': ['<node_type>', [kwds]]
                },
                'ProcessingDescription': <processing description>
            }

            The <processing description> is a dict:
            {
                'Step 1': 'I did a thing',
                'Step 2': 'I did another thing',
                'Step 3':{
                    'substep a': 'A result',
                    'substep b': 'B result'
                }
            }

            See the pipeliner documentation for a description of node types
            """
            )
        )
        self.jobinfo.programs = [ExternalProgram("cp")]
        self.joboptions["external_import_data"] = InputNodeJobOption(
            label="External import json data file:",
            default_value="",
            pattern="*.json",
            help_text=(
                "This file contains information about the processing steps that"
                " were carried out externally.  See the pipeliner documentation for"
                " the format of the file"
            ),
            is_required=True,
            node_type=NODE_PROCESSDATA,
            node_kwds=["pipeliner", "external_workflow"],
        )

    def read_input_data(
        self,
    ) -> Tuple[
        Dict[str, Tuple[str, List[str]]],
        Dict[str, Tuple[str, List[str]]],
        Dict[str, object],
    ]:
        data_file = self.joboptions["external_import_data"].get_string()
        with open(data_file, "r") as df:
            input_data = json.load(df)
        input_nodes: Dict[str, Tuple[str, List[str]]] = {}
        for node in input_data["InputNodes"]:
            input_nodes[node] = tuple(input_data["InputNodes"][node])
        output_nodes: Dict[str, Tuple[str, List[str]]] = {}
        for node in input_data["OutputNodes"]:
            output_nodes[node] = tuple(input_data["OutputNodes"][node])
        processing = input_data["ProcessingDescription"]

        return input_nodes, output_nodes, processing

    def create_input_nodes(self) -> None:
        # Override to handle input nodes listed in the input JSON file
        super().create_input_nodes()

        # Read input data and create all input nodes listed
        input_nodes = self.read_input_data()[0]
        for node in input_nodes:
            try:
                kwds = input_nodes[node][1]
            except IndexError:
                kwds = []
            self.input_nodes.append(
                create_node(
                    node,
                    input_nodes[node][0],
                    kwds,
                )
            )

    @staticmethod
    def get_commands_and_output_nodes(
        outnodes: Dict[str, Tuple[str, List[str]]], job_dir: str
    ) -> Tuple[List[PipelinerCommand], List[Node]]:
        """Get the commands and the output nodes.

        Not ideal to make these together, but they are very tightly coupled in this job.

        To minimise the chance of problems, this is deliberately written as a static
        method, so it acts as a pure function and should have no side effects.
        """
        # make sure no nodes are duplicated:
        # first see if the filenames are unique
        depth = 1
        nodes_dict: Dict[str, Tuple[str, str, List[str]]] = {}
        nodes_list = [os.path.basename(x) for x in outnodes]
        if len(set(nodes_list)) == len(nodes_list):
            depth = 0

        # if any of the nodes have same names add directories until all are unique
        if depth > 0:
            max_length = max([len(x.split("/")) for x in outnodes])
            cycle = True
            depth = -1
            while cycle:
                depth += -1
                if -depth > max_length:
                    raise ValueError(
                        "2 or more of the nodes in the external_processing "
                        "have identical names!"
                    )
                nodes_dict = {}
                for node in outnodes:
                    shortname = "/".join(node.split("/")[depth:])
                    nodeinfo = outnodes[node]
                    nodes_dict[shortname] = (node, nodeinfo[0], nodeinfo[1])

                if len(nodes_dict) != len(outnodes):
                    continue
                cycle = False

        commands = []
        output_nodes = []
        if depth == 0:
            # if all node files names are unique just copy them all in
            for node in outnodes:
                inname = os.path.join(job_dir, os.path.basename(node))
                commands.append(PipelinerCommand(["cp", os.path.abspath(node), inname]))
                nodeinfo = outnodes[node]
                try:
                    kwds = nodeinfo[1]
                except IndexError:
                    kwds = []
                output_nodes.append(create_node(inname, nodeinfo[0], kwds))
        elif depth < 0:
            # if the file names are not unique then a directory structure must be
            # created
            for node in nodes_dict:
                inname = os.path.join(job_dir, node)
                commands.append(
                    PipelinerCommand(["mkdir", "-p", os.path.dirname(inname)])
                )
                node_data = nodes_dict[node]
                commands.append(
                    PipelinerCommand(["cp", os.path.abspath(node_data[0]), inname])
                )
                output_nodes.append(create_node(inname, node_data[1], node_data[2]))

        return commands, output_nodes

    def create_output_nodes(self) -> None:
        outs = self.read_input_data()[1]
        self.output_nodes = self.get_commands_and_output_nodes(outs, self.output_dir)[1]

    def get_commands(self) -> List[PipelinerCommand]:
        outs = self.read_input_data()[1]
        commands = self.get_commands_and_output_nodes(outs, self.output_dir)[0]

        # copy in the info file
        data_file = self.joboptions["external_import_data"].get_string()
        commands.append(
            PipelinerCommand(
                [
                    "cp",
                    data_file,
                    os.path.join(self.output_dir, "external_processing.json"),
                ]
            )
        )
        return commands

    def gather_metadata(self) -> Dict[str, object]:
        data_file = os.path.join(self.output_dir, "external_processing.json")
        with open(data_file, "r") as df:
            metadata = json.load(df)["ProcessingDescription"]
        return metadata

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        input_nodes = [[x.name, x.type] for x in self.input_nodes]
        innodes = create_results_display_object(
            "table",
            title="Inputs",
            headers=["File", "NodeType"],
            table_data=input_nodes,
            associated_data=[x[0] for x in input_nodes],
        )
        output_nodes = [[x.name, x.type] for x in self.output_nodes]
        outnodes = create_results_display_object(
            "table",
            title="Outputs",
            headers=["File", "NodeType"],
            table_data=output_nodes,
            associated_data=[x[0] for x in output_nodes],
        )

        data_file = os.path.join(self.output_dir, "external_processing.json")
        with open(data_file, "r") as df:
            dat = json.load(df)["ProcessingDescription"]
        pretty_data = str(json.dumps(dat, indent=2))
        p_data = pretty_data.replace("{", "").replace("}", "").replace('"', "")
        proc_desc = create_results_display_object(
            "text",
            title="Processing Info",
            display_data=p_data,
            associated_data=[data_file],
        )

        return [innodes, outnodes, proc_desc]
