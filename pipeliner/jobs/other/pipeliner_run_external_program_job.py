#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from typing import List
import shlex
import re
import os

from pipeliner.pipeliner_job import ExternalProgram, PipelinerCommand
from pipeliner.jobs.relion.relion_job import RelionJob
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    StringJobOption,
    MultiExternalFileJobOption,
    MultiStringJobOption,
    MultipleChoiceJobOption,
    files_exts,
    EXT_MRC_MAP,
    EXT_STARFILE,
    JobOptionCondition,
    BooleanJobOption,
    JobOptionValidationResult,
)
from pipeliner.data_structure import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_NEWNODETYPE,
    EXTERNAL_DIR,
)
from pipeliner.node_factory import all_node_toplevel_types


class PipelinerExternal(RelionJob):
    OUT_DIR = EXTERNAL_DIR
    PROCESS_NAME = "pipeliner.run_external_program"
    CATEGORY_LABEL = "External"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "Run an external program"
        self.jobinfo.programs = []
        self.jobinfo.short_desc = "Run a program that is not part of doppio"
        self.jobinfo.long_desc = "Run external programs through doppio and pipeliner"

        self.joboptions["in_movies"] = MultiInputNodeJobOption(
            label="Input movies starfile:",
            node_type=NODE_MICROGRAPHMOVIEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Movies STAR files", EXT_STARFILE),
            help_text=(
                "Input raw movies starfiles. These can be used in the commands with"
                "XXXmovies_1XXX, XXXmovies_2XXX, and so on"
            ),
        )

        self.joboptions["in_micrographs"] = MultiInputNodeJobOption(
            label="Input micrographs starfile:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Micrographs STAR files", EXT_STARFILE),
            help_text=(
                "Input micrographs starfiles. These can be used in the commands with"
                " XXXmicrographs_1XXX, XXXmicrographs_2XXX, and so on"
            ),
        )

        self.joboptions["in_particles"] = MultiInputNodeJobOption(
            label="Input particles starfile:",
            node_type=NODE_PARTICLEGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Particles STAR file", EXT_STARFILE),
            help_text=(
                "Input particles starfiles. These can be used in the commands with"
                " XXXparticles_1XXX, XXXparticles_2XXX, and so on"
            ),
        )

        self.joboptions["in_map"] = MultiInputNodeJobOption(
            label="Input 3D density map:",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("MRC Map", EXT_MRC_MAP),
            help_text=(
                "Input density maps. These can be used in the commands with"
                " XXXmap_1XXX, XXXmap_2XXX, and so on"
            ),
        )

        self.joboptions["in_mask"] = MultiInputNodeJobOption(
            label="Input 3D mask:",
            node_type=NODE_MASK3D,
            default_value="",
            pattern=files_exts("MRC Mask", EXT_MRC_MAP),
            help_text=(
                "Input 3D masks. These can be used in the commands with XXXmask_1XXX,"
                " XXXmask_2XXX, and so on"
            ),
        )

        self.joboptions["in_other"] = MultiExternalFileJobOption(
            label="Input other file types:",
            default_value="",
            help_text=(
                "Input any other file types. These can be used in the commands with"
                "XXXother_1XXX, XXXother_2XXX"
            ),
        )

        # makeing the commands
        self.joboptions["commands"] = MultiStringJobOption(
            label="Commands to be executed",
            help_text=(
                "These commands will be executed in order when the job is run. Variable"
                " names will be substituted in before running so, for example,"
                " XXXmap_1XXX in the command string will be replaced with the name of"
                " the first map file input. Commands will be executed in the job's"
                " working dir IE: External/job999/"
            ),
        )

        # addition for making output nodes

        self.joboptions["do_outputs"] = BooleanJobOption(
            label="Create output node(s)?",
            default_value=False,
            help_text=(
                "If the job outputs a single file, or all of the outputs will be of the"
                " same type then the pipeliner can integrate these files into the "
                " project automatically.  Otherwise the file output from this job will"
                " need to be imported into the project with a relion.import job"
            ),
        )

        self.joboptions["fn_output"] = MultiStringJobOption(
            label="Output file(s) name(s) (optional):",
            default_value="",
            help_text=(
                "The name of the output file (if known).  The file name should be"
                " relative the the job directory. This will allow the"
                " creation of an output node for the file"
            ),
            deactivate_if=JobOptionCondition([("do_outputs", "=", False)]),
        )

        self.joboptions["output_nodetype"] = MultipleChoiceJobOption(
            label="Output node type:",
            choices=all_node_toplevel_types(add_new=True),
            default_value_index=0,
            help_text=(
                "Select the node type to assign to the output file(s), this tells the"
                " pipeliner what kind of file it is"
            ),
            deactivate_if=JobOptionCondition([("do_outputs", "=", False)]),
        )

        self.joboptions["alt_nodetype"] = StringJobOption(
            label="OR: Create a new node type:",
            default_value="",
            help_text=(
                "Add a new node type.  Only do this if none of the standard node"
                " types are applicable. They should almost always be. Actually don't"
                " do this!"
            ),
            required_if=JobOptionCondition(
                [("output_nodetype", "=", NODE_NEWNODETYPE)]
            ),
            deactivate_if=JobOptionCondition(
                [("output_nodetype", "!=", NODE_NEWNODETYPE)]
            ),
        )

        self.joboptions["output_kwds"] = MultiStringJobOption(
            label="Output node keywords:",
            default_value="",
            help_text=(
                "This should be things like the program that created the file "
                "and any relevant descriptions of the file.  For example a halfmap"
                " should have 'halfmap' in its keywords. More descriptive keywords"
                " will allow Doppio to suggest the file as an input to future jobs "
            ),
        )

        external_exe = ExternalProgram(command="")
        external_exe.command = "<External programs>"
        external_exe.exe_path = "<Varies depending on parameters>"
        self.jobinfo.programs = [external_exe]

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        errs = []
        coms_jo = self.joboptions["commands"]
        has_redir = False
        for com in coms_jo.get_list():
            has_redir = True if any([x in com for x in ["<", ">"]]) else has_redir
        if has_redir:
            errs.append(
                JobOptionValidationResult(
                    "warning",
                    raised_by=[coms_jo],
                    message=(
                        "'<' and '>' cannot be used to redirect inputs and"
                        " outputs for commands in this job"
                    ),
                )
            )

        return errs

    def create_output_nodes(self) -> None:
        if self.joboptions["do_outputs"].get_boolean():
            fn_output = self.joboptions["fn_output"].get_list()
            out_nodetype = self.joboptions["output_nodetype"].get_string()
            out_kwds = self.joboptions["output_kwds"].get_list()
            if out_nodetype == NODE_NEWNODETYPE:
                out_nodetype = self.joboptions["alt_nodetype"].get_string()
            for outfile in fn_output:
                self.add_output_node(outfile, out_nodetype, out_kwds)

    def get_commands(self) -> List[PipelinerCommand]:
        self.working_dir = self.output_dir
        inputs = {
            "movies": self.joboptions["in_movies"].get_list(),
            "micrographs": self.joboptions["in_micrographs"].get_list(),
            "particles": self.joboptions["in_particles"].get_list(),
            "map": self.joboptions["in_map"].get_list(),
            "mask": self.joboptions["in_mask"].get_list(),
            "other": self.joboptions["in_other"].get_list(),
        }
        coms_list = [shlex.split(x) for x in self.joboptions["commands"].get_list()]
        pattern = r"XXX(.*?)XXX"

        commands = []
        for com in coms_list:
            updated_com = []
            for arg in com:
                re_result = re.search(pattern, arg)
                if re_result:
                    arg_type, arg_index = re_result.group(1).split("_")
                    try:
                        new_arg = inputs[arg_type][int(arg_index) - 1]
                        rel_arg = os.path.relpath(new_arg, self.output_dir)
                    except IndexError:
                        raise ValueError(
                            f"{arg} was requested in a command but there are not enough"
                            f" items in the list of {arg_type} inputs"
                        )
                    updated_com.append(rel_arg)
                else:
                    updated_com.append(arg)
            commands.append(updated_com)
        return [PipelinerCommand(x) for x in commands]
