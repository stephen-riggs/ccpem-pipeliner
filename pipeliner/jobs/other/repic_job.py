#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Sequence
from glob import glob

from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    IntJobOption,
    files_exts,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    create_results_display_object,
    make_particle_coords_thumb,
)
from pipeliner.nodes import NODE_MICROGRAPHCOORDSGROUP

from pipeliner.starfile_handler import DataStarFile
from pipeliner.utils import get_job_script, count_file_lines
from pipeliner.results_display_objects import ResultsDisplayObject


class REPICJob(PipelinerJob):
    PROCESS_NAME = "repic.autopick.consensus"
    OUT_DIR = "REPIC"
    CATEGORY_LABEL = "Automated Particle Picking"

    def __init__(self) -> None:
        super().__init__()

        self.jobinfo.display_name = "Create consensus particle set"

        self.jobinfo.short_desc = (
            "Use REPIC to create a consensus particle set from multiple pickers"
        )
        self.jobinfo.long_desc = (
            "REPIC is an ensemble learning methodology that uses multiple pickers to "
            "find consensus particles. REPIC identifies consensus particles by framing "
            "its task as a graph problem and using integer linear programming to "
            "select particles. REPIC picks high-quality particles when the best picker "
            "is not known a priori and for known difficult-to-pick particles "
            "(e.g., TRPV1). Reconstructions using consensus particles can achieve "
            "resolutions comparable to those from particles picked by experts, without "
            "the need for downstream particle filtering."
        )
        self.jobinfo.programs = [ExternalProgram(command="repic")]
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Matt Iadanza"
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Cameron CJF",
                    "Seager JHS",
                    "Sigworth FJ",
                    "Tagare HD",
                    "Gerstein MB",
                ],
                title=(
                    "REPIC — an ensemble learning methodology for cryo-EM particle "
                    "picking"
                ),
                journal="bioRxiv",
                year="2023",
                doi="10.1101/2023.05.13.540636",
            )
        ]
        self.jobinfo.documentation = "https://github.com/ccameron/REPIC"

        # TODO: This should be replaced with a MultiSelectJobOption
        #  when that class of JobOption is added.
        self.joboptions["input_files"] = MultiInputNodeJobOption(
            label="Input AutoPick files",
            pattern=files_exts("AutoPick STAR file", ["autopick.star"], exact=True),
            is_required=True,
            node_type=NODE_MICROGRAPHCOORDSGROUP,
        )

        self.joboptions["box_size"] = IntJobOption(
            label="Box size",
            default_value=180,
            hard_min=1,
            help_text="Box size for the particles",
            is_required=True,
        )
        self.get_runtab_options(addtl_args=True)

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "consensus_picks.star", NODE_MICROGRAPHCOORDSGROUP, ["consensus"]
        )

    def get_commands(self) -> List[PipelinerCommand]:
        script_com: List[str] = [
            "python3",
            get_job_script("repic/repic_setup.py"),
            "--outdir",
            self.output_dir,
            "--autopick_files",
        ]
        script_com.extend(self.joboptions["input_files"].get_list())
        s_com = PipelinerCommand(script_com)
        clique_dir = os.path.join(self.output_dir, "cliques")

        clique_com = PipelinerCommand(
            [
                "repic",
                "get_cliques",
                self.output_dir,
                clique_dir,
                str(self.joboptions["box_size"].get_number()),
            ]
        )

        ilp_com = PipelinerCommand(["repic", "run_ilp", clique_dir, 180])

        summary_com = PipelinerCommand(
            [
                "python3",
                get_job_script("repic/repic_setup.py"),
                "--write_summary",
                "--outdir",
                self.output_dir,
                "--autopick_files",
                self.joboptions["input_files"].get_list()[0],
            ]
        )

        return [s_com, clique_com, ilp_com, summary_com]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        """count the individual particle starfiles rather than the summary
        star file for on-the-fly updating"""
        # make the histogram
        outfile = os.path.join(self.output_dir, "consensus_picks.star")
        clique_dir = os.path.join(self.output_dir, "cliques")
        partsfiles = glob(clique_dir + "/*.box")
        pcounts = []
        for pf in partsfiles:
            pcounts.append(count_file_lines(pf))
        total_parts = sum(pcounts)

        graph = create_results_display_object(
            "histogram",
            title=f"{total_parts} picked particles",
            data_to_bin=pcounts,
            xlabel="Number of particles",
            ylabel="Micrographs",
            associated_data=[outfile],
        )

        # make the sample image
        fb = DataStarFile(outfile).get_block("coordinate_files")
        files = fb.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])[0]

        image = make_particle_coords_thumb(files[0], files[1], self.output_dir)

        return [image, graph]
