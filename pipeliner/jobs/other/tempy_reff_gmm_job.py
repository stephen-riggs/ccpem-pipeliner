#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from pathlib import Path
from typing import List, Sequence, Dict, Any
import json
from pipeliner.pipeliner_job import PipelinerJob, ExternalProgram, PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    FloatJobOption,
    IntJobOption,
    BooleanJobOption,
    files_exts,
    JobOptionCondition,
    MultipleChoiceJobOption,
)
from pipeliner.pipeliner_job import Ref
from pipeliner.display_tools import (
    make_map_model_thumb_and_display,
    create_results_display_object,
)
from pipeliner.nodes import (
    NODE_ATOMCOORDS,
    NODE_DENSITYMAP,
)
from pipeliner.results_display_objects import ResultsDisplayObject
from pipeliner.jobs.other.tempy_reff_flexfit_job import FlexfitRefine


class GMMRefine(PipelinerJob):
    PROCESS_NAME = "tempy_reff.atomic_model_refine.gmm"
    OUT_DIR = "TEMPyReFF-GMM"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "TEMPy-ReFF GMM"
        self.jobinfo.short_desc = "All atom based flexible fitting with TEMPy-ReFF"
        self.jobinfo.long_desc = (
            "Gaussian mixture model based refinement of atomic structures guided by "
            "cryoEM density. N.B. ligands and waters are not currently supported and "
            "will be automatically removed from the input model. The GMM approach will "
            "produce slightly better results but refinement will be significantly "
            "slower than using the rigid body density fitting. If the starting "
            "structure is far from the target, it is recommended to run an initial "
            "round of flexible fitting with RIBFIND2 restraints and the density force "
            "(TEMPy-ReFF FlexFit job), to quickly account for larger movements, and "
            "then correct any residual errors with a round of refinement  using the GMM"
        )
        self.jobinfo.version = "0.1"
        self.jobinfo.job_author = "Agnel Joseph"
        self.jobinfo.programs = [
            ExternalProgram(command="tempy-reff"),
            ExternalProgram(command="gemmi"),
        ]
        self.jobinfo.references = [
            Ref(
                authors=[
                    "Mulvaney T",
                    "Cragnolini T",
                    "Beton J",
                    "Topf M",
                ],
                title=(
                    "Cryo-EM structure and B-factor refinement with ensemble"
                    " representation"
                ),
                journal="bioRXiv",
                year="2022",
                doi="10.1101/2022.06.08.495259",
            ),
        ]
        self.joboptions["input_model"] = InputNodeJobOption(
            label="Input model",
            node_type=NODE_ATOMCOORDS,
            pattern=files_exts("Atomic model", [".cif", ".pdb"]),
            default_value="",
            help_text="The input model to be fitted/refined",
            is_required=True,
        )
        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            default_value="",
            pattern=files_exts("3D map", [".mrc", ".map"]),
            help_text="The input map to refine the model against",
            is_required=True,
        )
        self.joboptions["resolution"] = FloatJobOption(
            label="Resolution",
            default_value=-1,
            suggested_min=0.5,
            suggested_max=20,
            hard_min=0.1,
            step_value=0.1,
            help_text="Resolution in angstroms",
            is_required=True,
        )
        self.joboptions["force_gmm"] = FloatJobOption(
            label="Weight of GMM fit",
            default_value=5e4,
            suggested_min=10,
            suggested_max=1e7,
            step_value=10,
            help_text=(
                "If the model does not move into obvious cryo-EM density, try "
                "increasing the fitting force by, say, a factor of 2. Conversely, if "
                "the model geometry starts to get worse, try reducing the fitting "
                "force."
            ),
            is_required=False,
        )
        self.joboptions["convergence_type"] = MultipleChoiceJobOption(
            label="Convergence method",
            choices=["variance", "patience"],
            default_value_index=1,
            help_text=(
                "The 'variance' method checks whether the variance in a scoring "
                "function (normally the CCC between the model and cryo-EM map) is "
                "improving. The 'patience' approach, checks if the CCC between the map "
                "and model is improving, and terminates the refinement if it does not "
                "improve for a set number of steps. Recommend using the 'patience' "
                "method for the non RIBFIND refinement using GMMs."
            ),
        )
        self.joboptions["convergence_threshold"] = FloatJobOption(
            label="Convergence threshold",
            hard_min=0,
            default_value=1e-6,
            suggested_min=1e-8,
            suggested_max=1e-3,
            step_value=1e-7,
            help_text=(
                "When the CCC variance of the convergence runs drops below this value, "
                "stop or move to the next stage of fitting."
            ),
            deactivate_if=JobOptionCondition([("convergence_type", "!=", "variance")]),
            is_required=False,
        )
        self.joboptions["convergence_patience"] = IntJobOption(
            label="Steps for convergence",
            default_value=5,
            hard_min=0,
            suggested_min=3,
            suggested_max=20,
            step_value=1,
            help_text=(
                "The number of last conformations to test for convergence. Only "
                "valid for convergence type 'patience'"
            ),
            deactivate_if=JobOptionCondition([("convergence_type", "!=", "patience")]),
            is_required=False,
        )
        self.joboptions["convergence_timeout"] = IntJobOption(
            label="Timeout steps",
            default_value=100,
            hard_min=0,
            suggested_min=0,
            step_value=1,
            help_text=(
                "This sets the maximum number of steps to run before just "
                "stopping refinement if convergence is not reached."
            ),
            is_required=False,
        )
        self.joboptions["use_gpu"] = BooleanJobOption(
            label="Use GPU?",
            default_value=True,
            help_text=(
                "Use for running the MD simulation on a CUDA capable GPU. Select 'No' "
                "to do the run on the CPU."
            ),
        )
        self.get_runtab_options()

    def create_output_nodes(self) -> None:
        self.add_output_node(
            os.path.join("gmm_out", "final.cif"),
            NODE_ATOMCOORDS,
            ["tempy_reff", "gmm_fit"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        # Run in the job output directory
        self.working_dir = self.output_dir
        commands = []
        # Get inputs
        input_model_orig = self.joboptions["input_model"].get_string()
        input_model = os.path.relpath(input_model_orig, self.working_dir)
        model_id = Path(input_model).stem
        input_map_orig = self.joboptions["input_map"].get_string()
        input_map = os.path.relpath(input_map_orig, self.working_dir)
        resolution = str(self.joboptions["resolution"].get_number())
        convergence_type = self.joboptions["convergence_type"].get_string()
        convergence_threshold = self.joboptions["convergence_threshold"].get_number()
        convergence_patience = self.joboptions["convergence_patience"].get_number()
        convergence_timeout = self.joboptions["convergence_timeout"].get_number()
        force_gmm = str(self.joboptions["force_gmm"].get_number())
        use_gpu = self.joboptions["use_gpu"].get_boolean()
        # Remove waters and ligands from input model with Gemmi convert
        # also ensure .pdb is generated
        model_no_wat_lig_rel = model_id + "_no_wat_lig.pdb"
        gemmi_command = [
            "gemmi",
            "convert",
            "--remove-lig-wat",
            input_model,
            model_no_wat_lig_rel,
        ]
        commands += [gemmi_command]

        models_to_score = [model_no_wat_lig_rel]
        tempy_reff_command = [
            "tempy-reff",
            "--model",
            model_no_wat_lig_rel,
            "--map",
            input_map,
            "--resolution",
            resolution,
        ]
        tempy_reff_command += FlexfitRefine.get_convergence_options(
            convergence_type=convergence_type,
            convergence_threshold=convergence_threshold,
            convergence_patience=convergence_patience,
            convergence_timeout=convergence_timeout,
        )
        if use_gpu:
            tempy_reff_command += ["--platform-cuda"]
        else:
            tempy_reff_command += ["--platform-cpu"]
        tempy_reff_command += [
            "--fitting-gmm",
            "--fitting-gmm-k",
            force_gmm,
            "--output-dir",
            "gmm_out",
        ]
        commands += [tempy_reff_command]
        models_to_score.append(os.path.join("gmm_out", "final.cif"))

        # Gemmi convert to make cif output file
        gemmi_cif = FlexfitRefine.get_gemmi_convert_to_cif_command(
            "gmm_out/final.pdb", "gmm_out/final.cif", remove_h=True
        )
        commands += [gemmi_cif]
        # run smoc scoring
        smoc_command = FlexfitRefine.get_smoc_command(
            input_map, models_to_score, resolution
        )
        commands += [smoc_command]
        return [PipelinerCommand(x) for x in commands]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        display_objects: List[ResultsDisplayObject] = []
        input_map = self.joboptions["input_map"].get_string()
        maps = [input_map]
        input_model = self.joboptions["input_model"].get_string()
        model_no_wat_lig_rel = Path(input_model).stem + "_no_wat_lig.pdb"
        models = [os.path.join(self.output_dir, model_no_wat_lig_rel)]
        colors = ["#000000"]
        models.append(os.path.join(self.output_dir, "gmm_out", "final.cif"))
        colors.append("#00FF0F")
        map_model_display = make_map_model_thumb_and_display(
            maps=maps,
            maps_opacity=[0.5],
            models=models,
            models_colours=colors,
            outputdir=self.output_dir,
        )
        display_objects.append(map_model_display)
        smoc_score_jsons = []
        map_id = Path(input_map).stem
        colors = ["black"]
        if len(models) == 3:
            colors.append("orange")
        colors.append("green")
        if len(colors) > 3 or len(colors) < 1:
            return display_objects
        dict_model_colors: Dict[str, Any] = {}
        dict_chain_scores: Dict[str, Any] = {}
        mod_count = 0
        # skip initial model from scoring for now
        # Atomic B-factors appear unrefined in the refined models
        for model in models:
            model_id = Path(model).stem
            smoc_json = f"SMOC_{model_id}_vs_{map_id}.json"
            smoc_score_jsons.append(smoc_json)
            if model_id not in dict_model_colors:
                dict_model_colors[model_id] = colors[mod_count]
            with open(os.path.join(self.output_dir, smoc_json), "r") as j:
                smoc_scores = json.load(j)
            for chain in smoc_scores["chains"]:
                list_resnum = []
                list_scores = []
                for resnum in smoc_scores["chains"][chain]:
                    try:
                        list_resnum.append(int(resnum))
                    except TypeError:
                        continue
                    list_scores.append(float(smoc_scores["chains"][chain][resnum]))
                try:
                    dict_chain_scores[chain].append(
                        [model_id, {"x": list_resnum, "y": list_scores}]
                    )
                except KeyError:
                    dict_chain_scores[chain] = [
                        [model_id, {"x": list_resnum, "y": list_scores}]
                    ]
            mod_count += 1
        for chain in dict_chain_scores:
            list_series_args = []
            data = []
            for name_data in dict_chain_scores[chain]:
                model_id = name_data[0]
                series_data = name_data[1]
                series_args = {
                    "name": model_id,
                    "mode": "lines",
                    "line": {"color": dict_model_colors[model_id], "width": 2},
                }
                list_series_args.append(series_args)
                data.append(series_data)

            xaxes_args = {
                "gridcolor": "lightgrey",
                "title_text": "Residue number",
            }
            yaxes_args = {
                "title_text": "SMOC score",
                "gridcolor": "lightgrey",
            }
            # multi_series plot
            disp_obj = create_results_display_object(
                "plotlyobj",
                data=data,
                plot_type="Scatter",  # all series scatter type
                subplot=False,
                multi_series=True,
                series_args=list_series_args,
                layout_args={
                    "plot_bgcolor": "white",
                },
                xaxes_args=xaxes_args,
                yaxes_args=yaxes_args,
                title="SMOC_residue_fit_chain_" + chain,
                associated_data=smoc_score_jsons,
                flag="",
                start_collapsed=True,
            )
            display_objects.append(disp_obj)

        return display_objects
