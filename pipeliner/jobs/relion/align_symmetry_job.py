#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#


from typing import List, Dict, Sequence
from pathlib import Path

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.job_options import (
    InputNodeJobOption,
    MultipleChoiceJobOption,
    IntJobOption,
    JobOptionCondition,
    BooleanJobOption,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionAlignSymmetry(RelionJob):
    OUT_DIR = "AlignSymmetry"
    PROCESS_NAME = "relion.map_utilities.align_symmetry"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_align_symmetry")]
        self.jobinfo.display_name = "Align map symmetry"
        self.jobinfo.short_desc = "Align a map on its symmetry axes"

        self.jobinfo.long_desc = (
            "Align the map's symmetry axes on the main XYZ axes of the coordinate"
            " system"
        )

        self.joboptions["input_map"] = InputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to align",
            is_required=True,
            default_value="",
        )

        self.joboptions["sym"] = MultipleChoiceJobOption(
            label="Symmetry type",
            choices=[
                "Cyclic (C)",
                "Dihedral (D)",
                "Tetrahedral (T)",
                "Octahedral (O)",
                "Icosahedral (I)",
            ],
            default_value_index=0,
            help_text="Choose the type of symmetry to apply",
            in_continue=True,
        )

        self.joboptions["fold"] = IntJobOption(
            label="Symmetry fold",
            default_value=1,
            hard_min=1,
            required_if=JobOptionCondition(
                [("sym", "=", "Cyclic (C)"), ("sym", "=", "Dihedral (D)")],
                operation="ANY",
            ),
            deactivate_if=JobOptionCondition(
                [
                    ("sym", "=", "Tetrahedral (T)"),
                    ("sym", "=", "Octahedral (O)"),
                    ("sym", "=", "Icosahedral (I)"),
                ],
                operation="ANY",
            ),
        )
        self.joboptions["box_size"] = IntJobOption(
            label="Box size",
            default_value=64,
            hard_min=1,
            help_text=(
                "Bin the map to this size for symmetry search. A very small box"
                " (such that Nyquist is around 20 Å) is usually sufficient"
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["do_recentre"] = BooleanJobOption(
            label="Recentre input map?",
            default_value=True,
            help_text=(
                "Recentre the input map before finding the symmetry axes. This should"
                " almost always be Yes"
            ),
            jobop_group="Advanced options",
        )
        self.joboptions["nr_uniform"] = IntJobOption(
            label="Number of uniform projections",
            default_value=400,
            hard_min=1,
            help_text="Randomly search this many orientations",
            jobop_group="Advanced options",
        )
        self.joboptions["only_rot"] = BooleanJobOption(
            label="Only search rotations?",
            default_value=False,
            help_text=(
                "Keep TILT and PSI fixed and search only ROT (rotation along the Z "
                "axis)"
            ),
            jobop_group="Advanced options",
        )

    def get_sym(self) -> str:
        symtype = self.joboptions["sym"].get_string()[0]
        symfold = self.joboptions["fold"].get_number()
        sym = symtype + str(symfold) if symtype in ["C", "D"] else symtype
        return sym

    def create_output_nodes(self) -> None:
        self.add_output_node("aligned.mrc", NODE_DENSITYMAP)

    def get_commands(self) -> List[PipelinerCommand]:
        sym = self.get_sym()
        inmap = self.joboptions["input_map"].get_string()
        boxsize = self.joboptions["box_size"].get_string()
        nr_uni = self.joboptions["nr_uniform"].get_string()
        command = [
            "relion_align_symmetry",
            "--i",
            inmap,
            "--sym",
            sym,
            "--o",
            str(Path(self.output_dir) / "aligned.mrc"),
            "--box_size",
            boxsize,
            "--nr_uniform",
            nr_uni,
        ]
        if not self.joboptions["do_recentre"].get_boolean():
            command.append("--no_recentre")
        if self.joboptions["only_rot"].get_boolean():
            command.extend(["--only_rot", "True"])

        return [PipelinerCommand(command)]

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        outfile_path = str(Path(self.output_dir) / "aligned.mrc")
        return make_maps_slice_montage_and_3d_display(
            in_maps={outfile_path: "Map aligned on symmetry axes"},
            output_dir=self.output_dir,
        )

    def gather_metadata(self) -> Dict[str, object]:
        refined_vals = {}
        with open(Path(self.output_dir) / "run.out") as logfile:
            for line in logfile:
                if "The refined solution is" in line:
                    line_split = line.split()
                    refined_vals["rot"] = float(line_split[6])
                    refined_vals["tilt"] = float(line_split[9])
                    refined_vals["psi"] = float(line_split[12])
                    break
        return {"applied_rotations": refined_vals}
