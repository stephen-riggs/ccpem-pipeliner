#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Sequence
from pathlib import Path

from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    FloatJobOption,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionApplyBFactor(RelionJob):
    OUT_DIR = "BFactor"
    PROCESS_NAME = "relion.map_utilities.apply_bfactor"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Apply B-factor"
        self.jobinfo.short_desc = "Apply B-factor to a map"

        self.jobinfo.long_desc = (
            "The term B-factor, sometimes called the Debye-Waller factor, temperature"
            " factor, or atomic displacement parameter, is used to account for the"
            " attenuation of scattering caused by thermal motion."
        )

        self.joboptions["input_map"] = MultiInputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to apply B-factor correction to",
            is_required=True,
            default_value="",
        )

        self.joboptions["bfact"] = FloatJobOption(
            label="B-factor to apply (Å²)",
            default_value=-100,
            suggested_max=0,
            is_required=True,
        )

    @staticmethod
    def get_outfile_name(map_name) -> str:
        return os.path.splitext(os.path.basename(map_name))[0] + "_bfact.mrc"

    def create_output_nodes(self) -> None:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for out_map in map_names.values():
            outfile_name = self.get_outfile_name(out_map)
            self.add_output_node(outfile_name, NODE_DENSITYMAP, ["b_factor"])

    def make_command(self, in_file: str) -> PipelinerCommand:
        if Path(in_file).suffix != ".mrc":
            in_file += ":mrc"
        bfact = self.joboptions["bfact"].get_number()
        outfile_name = self.get_outfile_name(in_file)
        outfile_path = os.path.join(self.output_dir, outfile_name)
        command = [
            "relion_image_handler",
            "--i",
            in_file,
            "--bfactor",
            str(bfact),
            "--o",
            outfile_path,
        ]

        return PipelinerCommand(command)

    def get_commands(self) -> List[PipelinerCommand]:
        commands: List[PipelinerCommand] = []
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for out_map in map_names.values():
            commands.append(self.make_command(out_map))
        return commands

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        bfact = self.joboptions["bfact"].get_number()
        outputs = {}
        for a_map in map_names.values():
            outfile_name = self.get_outfile_name(a_map)
            outputs[os.path.join(self.output_dir, outfile_name)] = (
                f"{outfile_name} with applied B-factor {bfact} Å²"
            )
        return make_maps_slice_montage_and_3d_display(
            in_maps=outputs, output_dir=self.output_dir, combine_montages=False
        )
