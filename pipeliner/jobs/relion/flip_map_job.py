#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Sequence
from pathlib import Path

from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.job_options import (
    MultiInputNodeJobOption,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionFlipMap(RelionJob):
    OUT_DIR = "Flip"
    PROCESS_NAME = "relion.map_utilities.flip_handedness"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Flip map handedness"
        self.jobinfo.short_desc = "Flip the handedness of a map"

        self.jobinfo.long_desc = (
            "Biological macromolecules are chiral! Reconstruction from 2D projections"
            " does not preserve chiral information and may result in a map with"
            " incorrect handedness. Use this job to flip the handedness of a map."
        )

        self.joboptions["input_map"] = MultiInputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to flip",
            is_required=True,
            default_value="",
        )

    @staticmethod
    def get_outfile_name(map_name) -> str:
        return os.path.splitext(map_name)[0] + "_flipped.mrc"

    def create_output_nodes(self) -> None:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for out_map in map_names.values():
            outfile_name = self.get_outfile_name(out_map)
            self.add_output_node(outfile_name, NODE_DENSITYMAP)

    def make_command(self, infile: str, outfile: str) -> PipelinerCommand:
        if Path(infile).suffix != ".mrc":
            infile += ":mrc"
        outfile_name = self.get_outfile_name(outfile)
        command = [
            "relion_image_handler",
            "--i",
            infile,
            "--invert_hand",
            "--o",
            os.path.join(self.output_dir, outfile_name),
        ]

        return PipelinerCommand(command)

    def get_commands(self) -> List[PipelinerCommand]:
        commands: List[PipelinerCommand] = []
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for map_pair in map_names.items():
            commands.append(self.make_command(*map_pair))
        return commands

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        outputs = {}
        for outfile in map_names.values():
            outfile_name = self.get_outfile_name(outfile)
            outputs[os.path.join(self.output_dir, outfile_name)] = (
                f"{outfile_name} flipped"
            )
        return make_maps_slice_montage_and_3d_display(
            in_maps=outputs,
            output_dir=self.output_dir,
            combine_montages=False,
        )
