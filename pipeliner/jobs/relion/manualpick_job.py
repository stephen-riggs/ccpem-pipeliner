#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from glob import glob
from typing import List, Dict, Sequence

from pipeliner.jobs.relion.relion_job import relion_program, RelionJob
from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner import user_settings
from pipeliner.starfile_handler import DataStarFile
from pipeliner.job_options import (
    InputNodeJobOption,
    StringJobOption,
    files_exts,
    EXT_STARFILE,
    BooleanJobOption,
    FloatJobOption,
    IntJobOption,
    JobOptionCondition,
    JobOptionValidationResult,
    ExternalFileJobOption,
)
from pipeliner.data_structure import (
    MANUALPICK_JOB_NAME,
    MANUALPICK_HELICAL_NAME,
    MANUALPICK_DIR,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_PARTICLEGROUPMETADATA,
)
from pipeliner.display_tools import (
    create_results_display_object,
    make_particle_coords_thumb,
)
from pipeliner.utils import decompose_pipeline_filename
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionManualPickJob(RelionJob):
    OUT_DIR = MANUALPICK_DIR
    CATEGORY_LABEL = "Manual Particle Picking"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_display")]
        self.jobinfo.display_name = "RELION manual particle picking"

        self.jobinfo.short_desc = "Manually pick particles from micrographs"
        self.jobinfo.long_desc = (
            "Manually pick particles by pointing and clicking, just like in the bad old"
            " days!"
        )

        self.joboptions["fn_in"] = InputNodeJobOption(
            label="Input micrographs:",
            node_type=NODE_MICROGRAPHGROUPMETADATA,
            node_kwds=["relion"],
            default_value="",
            pattern=files_exts("Micrographs STAR file", EXT_STARFILE),
            help_text=(
                "Input STAR file (with or without CTF information), OR a unix-type"
                " wildcard with all micrographs in MRC format(in this  case no CTFs can"
                " be used)."
            ),
        )

        self.joboptions["diameter"] = IntJobOption(
            label="Particle diameter (A):",
            default_value=100,
            hard_min=0,
            suggested_max=500,
            step_value=50,
            help_text=(
                "The diameter of the circle used around picked particles (in"
                " Angstroms). Only used for display."
            ),
            in_continue=True,
        )

        self.joboptions["micscale"] = FloatJobOption(
            label="Scale for micrographs:",
            default_value=0.2,
            suggested_min=0.1,
            suggested_max=1,
            step_value=0.05,
            help_text=(
                "The micrographs will be displayed at this relative scale, i.e. a value"
                " of 0.5 means that only every second pixel will be displayed."
            ),
            in_continue=True,
        )

        self.joboptions["sigma_contrast"] = FloatJobOption(
            label="Sigma contrast:",
            default_value=3,
            suggested_min=0,
            suggested_max=10,
            step_value=0.5,
            help_text=(
                "The micrographs will be displayed with the black value set to the"
                " average of all values MINUS this values times the standard deviation"
                " of all values in the micrograph, and the white value will be set to"
                " the average PLUS this value times the standard deviation. Use zero to"
                " set the minimum value in the micrograph to black, and the maximum"
                " value to white "
            ),
            in_continue=True,
        )

        self.joboptions["white_val"] = FloatJobOption(
            label="White value:",
            default_value=0,
            suggested_min=0,
            suggested_max=512,
            step_value=16,
            help_text=(
                "Use non-zero values to set the value of the whitest pixel in the"
                " micrograph."
            ),
            in_continue=True,
        )

        self.joboptions["black_val"] = FloatJobOption(
            label="Black value:",
            default_value=0,
            suggested_min=0,
            suggested_max=512,
            step_value=16,
            help_text=(
                "Use non-zero values to set the value of the blackest pixel in the"
                " micrograph."
            ),
            in_continue=True,
        )

        self.joboptions["lowpass"] = FloatJobOption(
            label="Lowpass filter (A)",
            default_value=20,
            suggested_min=10,
            suggested_max=100,
            step_value=5,
            help_text=(
                "Lowpass filter that will be applied to the micrographs. Give a"
                " negative value to skip the lowpass filter."
            ),
            in_continue=True,
        )

        self.joboptions["highpass"] = FloatJobOption(
            label="Highpass filter (A)",
            default_value=-1,
            suggested_min=100,
            suggested_max=1000,
            step_value=100,
            help_text=(
                "Highpass filter that will be applied to the micrographs. This may be"
                " useful to get rid of background ramps due to uneven ice"
                " distributions. Give a negative value to skip the highpass filter."
                " Useful values are often in the range of 200-400 Angstroms."
            ),
            in_continue=True,
        )

        self.joboptions["angpix"] = FloatJobOption(
            label="Pixel size (A)",
            default_value=-1,
            suggested_min=0.3,
            suggested_max=5,
            step_value=0.1,
            help_text=(
                "Pixel size in Angstroms. This will be used to calculate the filters"
                " and the particle diameter in pixels. If a CTF-containing STAR file is"
                " input, then the value given here will be ignored, and the pixel size"
                " will be calculated from the values in the STAR file. A negative value"
                " can then be given here."
            ),
            in_continue=True,
        )

        # removed in RELION 4.0
        # self.joboptions["ctfscale"] = JobOption.as(
        # "Scale for CTF image:",
        # 1,
        # 0.1,
        # 2,
        # 0.1,
        # "CTFFINDs CTF image (with the Thon rings) will be displayed"
        # " at this relative scale, i.e. a value of 0.5 means that only"
        # " every second pixel will be displayed.",
        # True,
        # )

        self.joboptions["do_color"] = BooleanJobOption(
            label="Blue<>red color particles?",
            default_value=False,
            help_text=(
                "If set to true, then the circles for each particles are coloured from"
                " red to blue (or the other way around) for a given metadata label. If"
                " this metadata label is not in the picked coordinates STAR file"
                " (basically only the rlnAutopickFigureOfMerit or rlnClassNumber) would"
                " be useful values there, then you may provide an additional STAR file"
                " (e.g. after classification/refinement below. Particles with values"
                " -999, or that are not in the additional STAR file will be coloured"
                " the default color: green"
            ),
            in_continue=True,
        )

        self.joboptions["color_label"] = StringJobOption(
            label="MetaDataLabel for color:",
            default_value="rlnParticleSelectZScore",
            help_text=(
                "The Metadata label of the value to plot from red<>blue. Useful"
                " examples might be: \nrlnParticleSelectZScore \n rlnClassNumber \n"
                " rlnAutopickFigureOfMerit \n rlnAngleTilt \n rlnLogLikeliContribution"
                " \n rlnMaxValueProbDistribution \n rlnNrOfSignificantSamples\n"
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_color", "=", False)]),
        )

        self.joboptions["fn_color"] = InputNodeJobOption(
            label="STAR file with color label:",
            default_value="",
            help_text=(
                "The program will figure o  ut which particles in this STAR file are on"
                " the current micrograph and color their circles according to the value"
                " in the corresponding column. Particles that are not in this STAR"
                " file, but present in the picked coordinates file will be colored"
                " green. If this field is left empty, then the color label (e.g."
                " rlnAutopickFigureOfMerit) should be present in the coordinates STAR"
                " file."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_color", "=", False)]),
            node_type=NODE_PARTICLEGROUPMETADATA,
        )

        self.joboptions["blue_value"] = FloatJobOption(
            label="Blue value:",
            default_value=0.0,
            suggested_min=0.0,
            suggested_max=4.0,
            step_value=0.1,
            help_text=(
                "The value of this entry will be blue. There will be a linear scale"
                " from blue to red, according to this value and the one given below."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_color", "=", False)]),
        )

        self.joboptions["red_value"] = FloatJobOption(
            label="Red value:",
            default_value=2.0,
            suggested_min=0.0,
            suggested_max=4.0,
            step_value=0.1,
            help_text=(
                "The value of this entry will be red. There will be a linear scale from"
                " blue to red, according to this value and the one given above."
            ),
            in_continue=True,
            deactivate_if=JobOptionCondition([("do_color", "=", False)]),
        )

        self.joboptions["do_topaz_denoise"] = BooleanJobOption(
            label="OR: use Topaz denoising?",
            default_value=False,
            help_text=(
                "If set to true, Topaz denoising will be performed instead of lowpass"
                " filtering."
            ),
        )

        topaz_path = user_settings.get_topaz_executable()

        self.joboptions["fn_topaz_exec"] = ExternalFileJobOption(
            label="Topaz executable:",
            default_value=topaz_path,
            help_text=(
                "The location of the Topaz executable. You can control the default of"
                " this field by setting environment variable"
                " PIPELINER_TOPAZ_EXECUTABLE. RELION 5 automatically uses its own"
                " `relion_python_topaz` wrapper and ignores this field"
            ),
            required_if=JobOptionCondition([("do_topaz_denoise", "=", True)]),
            deactivate_if=JobOptionCondition([("do_topaz_denoise", "=", False)]),
        )

        self.joboptions["do_fom_threshold"] = BooleanJobOption(
            label="Use autopick FOM threshold?",
            default_value=False,
            help_text=(
                "If set to Yes, only particles with rlnAutopickFigureOfMerit values"
                " below the threshold below will be extracted. This option is only used"
                " when manually editing the results of an AutoPickjob"
            ),
        )

        self.joboptions["minimum_pick_fom"] = FloatJobOption(
            label="Minimum autopick FOM:",
            default_value=0,
            suggested_min=-5,
            suggested_max=10,
            step_value=0.1,
            help_text=(
                "The minimum value for the rlnAutopickFigureOfMerit for particles to be"
                " extracted."
            ),
            deactivate_if=JobOptionCondition([("do_fom_threshold", "=", False)]),
        )

        self.get_runtab_options(addtl_args=True)

    def common_commands(self) -> None:

        self.command = ["relion_manualpick"]

        input_star_mics = self.joboptions["fn_in"].get_string()
        self.command += ["--i", input_star_mics]

        self.command += ["--odir", self.output_dir, "--pickname", "manualpick"]

        fn_outstar = self.output_dir + "micrographs_selected.star"
        self.command += [
            "--allow_save",
            "--fast_save",
            "--selection",
            fn_outstar,
            "--scale",
            self.joboptions["micscale"].get_string(),
            "--sigma_contrast",
            self.joboptions["sigma_contrast"].get_string(),
            "--black",
            self.joboptions["black_val"].get_string(),
            "--white",
            self.joboptions["white_val"].get_string(),
        ]

        if self.joboptions["do_topaz_denoise"].get_boolean():
            self.command.append("--topaz_denoise")
            topaz_exe = self.joboptions["fn_topaz_exec"].get_string()
            self.command += ["--topaz_exe", topaz_exe]

        else:
            lowpass = int(self.joboptions["lowpass"].get_number())
            if lowpass > 0:
                self.command += ["--lowpass", str(lowpass)]

            highpass = int(self.joboptions["highpass"].get_number())
            if highpass > 0:
                self.command += ["--highpass", str(highpass)]

            angpix = self.joboptions["angpix"].get_number()
            if angpix > 0:
                self.command += ["--angpix", str(angpix)]

        self.command += [
            "--particle_diameter",
            self.joboptions["diameter"].get_string(),
        ]

        if self.joboptions["do_fom_threshold"].get_boolean():
            self.command += [
                "--minimum_pick_fom",
                self.joboptions["minimum_pick_fom"].get_string(),
            ]

    def common_commands2(self) -> None:
        do_color = self.joboptions["do_color"].get_boolean()
        if do_color:
            self.command += [
                "--color_label",
                self.joboptions["color_label"].get_string(),
                "--blue",
                self.joboptions["blue_value"].get_string(),
                "--red",
                self.joboptions["red_value"].get_string(),
            ]
            fn_color = self.joboptions["fn_color"].get_string()
            if len(fn_color) > 0:
                self.command += ["--color_star", fn_color]

        other_args = self.joboptions["other_args"].get_string()
        if len(other_args) > 0:
            self.command += self.parse_additional_args()

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        lp = self.joboptions["lowpass"].get_number()
        hp = self.joboptions["highpass"].get_number()
        if not any([x < 0 for x in (lp, hp)]) and hp < lp:
            return [
                JobOptionValidationResult(
                    result_type="error",
                    raised_by=[self.joboptions["lowpass"], self.joboptions["highpass"]],
                    message="High pass cannot be smaller than lowpass",
                )
            ]
        return []

    def gather_metadata(self) -> Dict[str, object]:

        metadata_dict: Dict[str, object] = {}

        logsdir = os.path.join(self.output_dir, "Movies")
        partsfiles = glob(logsdir + "/*.star")
        pcounts = []
        for pf in partsfiles:
            pcounts.append(DataStarFile(pf).count_block())
        total_parts = sum(pcounts)

        metadata_dict["TotalParticlesPicked"] = total_parts
        metadata_dict["ParticlesPerMicrograph"] = [
            list(x) for x in zip(partsfiles, pcounts)
        ]

        return metadata_dict

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        """count the individual particle starfiles rather that the summary
        star file for on-the-fly updating"""
        # get the star files
        instar = DataStarFile(self.joboptions["fn_in"].get_string())
        mdat = instar.get_block("micrographs").find(["_rlnMicrographName"])[0][0]
        logsdir = os.path.join(
            self.output_dir, os.path.dirname(decompose_pipeline_filename(mdat)[2])
        )
        partsfiles = glob(logsdir + "/*.star")
        pcounts = []
        for pf in partsfiles:
            pcounts.append(DataStarFile(pf).count_block())
        total_parts = sum(pcounts)

        graph = create_results_display_object(
            "histogram",
            title=f"{total_parts} picked particles",
            data_to_bin=pcounts,
            xlabel="Number of particles",
            ylabel="Micrographs",
            associated_data=[os.path.join(self.output_dir, "manualpick.star")],
        )

        # make the sample image
        out_file = os.path.join(self.output_dir, "manualpick.star")
        fb = DataStarFile(out_file).get_block("coordinate_files")
        files = fb.find(["_rlnMicrographName", "_rlnMicrographCoordinates"])[0]

        image = make_particle_coords_thumb(files[0], files[1], self.output_dir)

        return [image, graph]


class RelionManualPick(RelionManualPickJob):
    PROCESS_NAME = MANUALPICK_JOB_NAME

    def __init__(self) -> None:
        super().__init__()

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "micrographs_selected.star", NODE_MICROGRAPHGROUPMETADATA, ["relion"]
        )
        self.add_output_node(
            "manualpick.star", NODE_MICROGRAPHCOORDSGROUP, ["relion", "manualpick"]
        )

    def get_commands(self) -> List[PipelinerCommand]:
        self.common_commands()
        self.common_commands2()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands


class RelionManualPickHelical(RelionManualPickJob):
    PROCESS_NAME = MANUALPICK_HELICAL_NAME

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.display_name = "RELION manual helical particle picking"

    def create_output_nodes(self) -> None:
        self.add_output_node(
            "micrographs_selected.star", NODE_MICROGRAPHGROUPMETADATA, ["relion"]
        )
        self.add_output_node(
            "manualpick.star",
            NODE_MICROGRAPHCOORDSGROUP,
            ["relion", "manualpick", "helixstartend"],
        )

    def get_commands(self) -> List[PipelinerCommand]:
        self.common_commands()
        self.command.append("--do_startend")
        self.common_commands2()
        commands = [PipelinerCommand(self.command, relion_control=True)]
        return commands
