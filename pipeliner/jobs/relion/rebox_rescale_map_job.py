#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
from typing import List, Sequence
from pathlib import Path

from pipeliner.pipeliner_job import PipelinerCommand
from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner.job_options import (
    MultiInputNodeJobOption,
    IntJobOption,
    BooleanJobOption,
    FloatJobOption,
    JobOptionValidationResult,
    JobOptionCondition,
)
from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner.display_tools import make_maps_slice_montage_and_3d_display
from pipeliner.results_display_objects import ResultsDisplayObject


class RelionReboxRescaleMap(RelionJob):
    OUT_DIR = "ReboxRescale"
    PROCESS_NAME = "relion.map_utilities.rebox_rescale_map"

    def __init__(self) -> None:
        super().__init__()
        self.jobinfo.programs = [relion_program("relion_image_handler")]
        self.jobinfo.display_name = "Change map box and pixel size"
        self.jobinfo.short_desc = (
            "Modify the box size of a map and change the pixel size if desired"
        )

        self.jobinfo.long_desc = (
            "Modify the box size of a map and change the pixel size if desired.  If the"
            " map is rescaled the new pixel box size must be an even number."
        )

        self.joboptions["input_map"] = MultiInputNodeJobOption(
            label="Input map",
            node_type=NODE_DENSITYMAP,
            help_text="The map to rescale or rebox",
            is_required=True,
            default_value="",
        )

        self.joboptions["do_rebox"] = BooleanJobOption(
            label="Rebox the map?",
            default_value=False,
            help_text=(
                "Select this option to trim or pad the map box to a different size."
                " If the map is also to be rescaled, the new box size must be even."
            ),
        )

        self.joboptions["box_size"] = IntJobOption(
            label="New box size (px):",
            default_value=100,
            hard_min=2,
            required_if=JobOptionCondition([("do_rebox", "=", True)]),
            deactivate_if=JobOptionCondition([("do_rebox", "=", False)]),
        )

        self.joboptions["do_rescale"] = BooleanJobOption(
            label="Rescale the map?",
            default_value=False,
            help_text=(
                "Select this option to resample the map with a different pixel size."
                " If the map is also to be reboxed, the new box size must be even."
            ),
        )

        self.joboptions["rescale_angpix"] = FloatJobOption(
            label="Rescaled pixel size:",
            default_value=1.0,
            hard_min=0.1,
            help_text="New pixel size for the map",
            required_if=JobOptionCondition([("do_rescale", "=", True)]),
            deactivate_if=JobOptionCondition([("do_rescale", "=", False)]),
        )
        self.joboptions["force_pixel_size"] = BooleanJobOption(
            label="Fix disparities in pixel size precision?",
            default_value=True,
            deactivate_if=JobOptionCondition([("do_rescale", "=", False)]),
            help_text=(
                "Somtimes the precision of the pixel size in the file header may be "
                "slightly different than the entered value IE: requested 5 and got "
                "4.99999999998. If this option is True the precision of the pixel size "
                "in the file header will be made to exactly match the value entered "
                "above. This option should be set to 'Yes' in almost all cases"
            ),
        )

    def additional_joboption_validation(self) -> List[JobOptionValidationResult]:
        if (
            not self.joboptions["do_rescale"].get_boolean()
            and not self.joboptions["do_rebox"].get_boolean()
        ):
            return [
                JobOptionValidationResult(
                    "error",
                    [self.joboptions["do_rescale"], self.joboptions["do_rebox"]],
                    (
                        "No action will be performed; Select either rescale, rebox, "
                        "or both"
                    ),
                )
            ]
        if (
            self.joboptions["do_rescale"].get_boolean()
            and self.joboptions["do_rebox"].get_boolean()
        ):
            if self.joboptions["box_size"].get_number() % 2:
                return [
                    JobOptionValidationResult(
                        "error",
                        [self.joboptions["box_size"], self.joboptions["do_rescale"]],
                        (
                            "If the map is to be rescaled the new box size must be an"
                            " even number"
                        ),
                    )
                ]
        return []

    def get_outfile_name(self, map_name) -> str:
        name = os.path.splitext(map_name)[0]
        if self.joboptions["do_rebox"].get_boolean():
            name += "_reboxed"
        if self.joboptions["do_rescale"].get_boolean():
            name += "_rescaled"
        return name + ".mrc"

    def create_output_nodes(self) -> None:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for out_map in map_names.values():
            outfile_name = self.get_outfile_name(out_map)
            self.add_output_node(outfile_name, NODE_DENSITYMAP)

    def make_command(self, inname: str, outname: str) -> PipelinerCommand:
        if Path(inname).suffix != ".mrc":
            inname += ":mrc"

        command = ["relion_image_handler", "--i", inname]

        if self.joboptions["do_rebox"].get_boolean():
            box_size = self.joboptions["box_size"].get_number()
            command.extend(["--new_box", str(box_size)])

        if self.joboptions["do_rescale"].get_boolean():
            newpx = str(self.joboptions["rescale_angpix"].get_number())
            command.extend(["--rescale_angpix", newpx])
            if self.joboptions["force_pixel_size"].get_boolean():
                command.extend(["--force_header_angpix", newpx])

        outfile_name = self.get_outfile_name(outname)
        command.extend(["--o", os.path.join(self.output_dir, outfile_name)])

        return PipelinerCommand(command)

    def get_commands(self) -> List[PipelinerCommand]:
        commands: List[PipelinerCommand] = []
        map_names = self.joboptions["input_map"].get_basename_mapping()
        for map_pair in map_names.items():
            commands.append(self.make_command(*map_pair))
        return commands

    def get_title(self, outfile: str) -> str:
        box_size = self.joboptions["box_size"].get_number()
        px_size = self.joboptions["rescale_angpix"].get_number()
        title = outfile
        if self.joboptions["do_rebox"].get_boolean():
            title += f" reboxed to {box_size}³ px"
        if self.joboptions["do_rescale"].get_boolean():
            title += f" rescaled to {px_size} Å/px"

        return title

    def create_results_display(self) -> Sequence[ResultsDisplayObject]:
        map_names = self.joboptions["input_map"].get_basename_mapping()
        outputs = {}
        for out in map_names.values():
            outmap = self.get_outfile_name(out)
            outputs[str(Path(self.output_dir) / outmap)] = self.get_title(outmap)

        return make_maps_slice_montage_and_3d_display(
            in_maps=outputs,
            output_dir=self.output_dir,
        )
