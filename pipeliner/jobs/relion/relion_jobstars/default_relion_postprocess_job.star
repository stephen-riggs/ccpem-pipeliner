
# version 30001

data_job

_rlnJobTypeLabel             relion.postprocess
_rlnJobIsContinue                       0
_rlnJobIsTomo                           0
 

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
adhoc_bfac      -1000 
    angpix      1 
autob_lowres         10 
do_adhoc_bfac         No 
do_auto_bfac        Yes 
  do_queue         No 
do_skip_fsc_weighting         No 
     fn_in 		""
   fn_mask 		""
    fn_mtf 		""
  low_pass          5 
min_dedicated         1 
mtf_angpix      1 
other_args         "" 
      qsub       qsub 
qsubscript 	"" 
 queuename    openmpi 
 
