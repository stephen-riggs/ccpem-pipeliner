#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from typing import List, Optional

from pipeliner.utils import check_for_illegal_symbols
from pipeliner.data_structure import (
    NODE_ATOMCOORDS,
    NODE_ATOMCOORDSGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_DENSITYMAPMETADATA,
    NODE_DENSITYMAPGROUPMETADATA,
    NODE_EULERANGLES,
    NODE_EVALUATIONMETRIC,
    NODE_IMAGE2D,
    NODE_IMAGE2DSTACK,
    NODE_IMAGE2DMETADATA,
    NODE_IMAGE2DGROUPMETADATA,
    NODE_IMAGE3D,
    NODE_IMAGE3DMETADATA,
    NODE_IMAGE3DGROUPMETADATA,
    NODE_LIGANDDESCRIPTION,
    NODE_LOGFILE,
    NODE_MASK2D,
    NODE_MASK3D,
    NODE_MICROGRAPHCOORDS,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_MICROGRAPH,
    NODE_MICROGRAPHMETADATA,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHMOVIE,
    NODE_MICROGRAPHMOVIEMETADATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_MICROSCOPEDATA,
    NODE_MLMODEL,
    NODE_OPTIMISERDATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_PARAMSDATA,
    NODE_PROCESSDATA,
    NODE_RESTRAINTS,
    NODE_RIGIDBODIES,
    NODE_SEQUENCE,
    NODE_SEQUENCEGROUP,
    NODE_SEQUENCEALIGNMENT,
    NODE_STRUCTUREFACTORS,
    NODE_TILTSERIES,
    NODE_TILTSERIESMETADATA,
    NODE_TILTSERIESGROUPMETADATA,
    NODE_TILTSERIESMOVIE,
    NODE_TILTSERIESMOVIEMETADATA,
    NODE_TILTSERIESMOVIEGROUPMETADATA,
    NODE_TOMOGRAM,
    NODE_TOMOGRAMMETADATA,
    NODE_TOMOGRAMGROUPMETADATA,
    NODE_TOMOOPTIMISATIONSET,
    NODE_TOMOTRAJECTORYDATA,
    NODE_TOMOMANIFOLDDATA,
    NODE_NEWNODETYPE,
)
from pipeliner.nodes import (
    Node,
    AtomCoordsNode,
    AtomCoordsGroupMetadataNode,
    DensityMapNode,
    DensityMapMetadataNode,
    DensityMapGroupMetadataNode,
    EulerAnglesNode,
    EvaluationMetricNode,
    Image2DNode,
    Image2DStackNode,
    Image2DMetadataNode,
    Image2DGroupMetadataNode,
    Image3DNode,
    Image3DMetadataNode,
    Image3DGroupMetadataNode,
    LigandDescriptionNode,
    LogFileNode,
    Mask2DNode,
    Mask3DNode,
    MicrographCoordsNode,
    MicrographCoordsGroupNode,
    MicrographNode,
    MicrographMetadataNode,
    MicrographGroupMetadataNode,
    MicrographMovieNode,
    MicrographMovieMetadataNode,
    MicrographMovieGroupMetadataNode,
    MicroscopeDataNode,
    MlModelNode,
    OptimiserDataNode,
    ParamsDataNode,
    ParticleGroupMetadataNode,
    ProcessDataNode,
    RestraintsNode,
    RigidBodiesNode,
    SequenceNode,
    SequenceGroupNode,
    SequenceAlignmentNode,
    StructureFactorsNode,
    TiltSeriesNode,
    TiltSeriesMetadataNode,
    TiltSeriesGroupMetadataNode,
    TiltSeriesMovieNode,
    TiltSeriesMovieMetadataNode,
    TiltSeriesMovieGroupMetadataNode,
    TomogramNode,
    TomogramMetadataNode,
    TomogramGroupMetadataNode,
    TomoOptimisationSetNode,
    TomoTrajectoryDataNode,
    TomoManifoldDataNode,
)

NODE_CLASSES = {
    NODE_ATOMCOORDS: AtomCoordsNode,
    NODE_ATOMCOORDSGROUPMETADATA: AtomCoordsGroupMetadataNode,
    NODE_DENSITYMAP: DensityMapNode,
    NODE_DENSITYMAPMETADATA: DensityMapMetadataNode,
    NODE_DENSITYMAPGROUPMETADATA: DensityMapGroupMetadataNode,
    NODE_EULERANGLES: EulerAnglesNode,
    NODE_EVALUATIONMETRIC: EvaluationMetricNode,
    NODE_IMAGE2D: Image2DNode,
    NODE_IMAGE2DSTACK: Image2DStackNode,
    NODE_IMAGE2DMETADATA: Image2DMetadataNode,
    NODE_IMAGE2DGROUPMETADATA: Image2DGroupMetadataNode,
    NODE_IMAGE3D: Image3DNode,
    NODE_IMAGE3DMETADATA: Image3DMetadataNode,
    NODE_IMAGE3DGROUPMETADATA: Image3DGroupMetadataNode,
    NODE_LIGANDDESCRIPTION: LigandDescriptionNode,
    NODE_LOGFILE: LogFileNode,
    NODE_MASK2D: Mask2DNode,
    NODE_MASK3D: Mask3DNode,
    NODE_MICROGRAPHCOORDS: MicrographCoordsNode,
    NODE_MICROGRAPHCOORDSGROUP: MicrographCoordsGroupNode,
    NODE_MICROGRAPH: MicrographNode,
    NODE_MICROGRAPHMETADATA: MicrographMetadataNode,
    NODE_MICROGRAPHGROUPMETADATA: MicrographGroupMetadataNode,
    NODE_MICROGRAPHMOVIE: MicrographMovieNode,
    NODE_MICROGRAPHMOVIEMETADATA: MicrographMovieMetadataNode,
    NODE_MICROGRAPHMOVIEGROUPMETADATA: MicrographMovieGroupMetadataNode,
    NODE_MICROSCOPEDATA: MicroscopeDataNode,
    NODE_MLMODEL: MlModelNode,
    NODE_OPTIMISERDATA: OptimiserDataNode,
    NODE_PARAMSDATA: ParamsDataNode,
    NODE_PARTICLEGROUPMETADATA: ParticleGroupMetadataNode,
    NODE_PROCESSDATA: ProcessDataNode,
    NODE_RESTRAINTS: RestraintsNode,
    NODE_RIGIDBODIES: RigidBodiesNode,
    NODE_SEQUENCE: SequenceNode,
    NODE_SEQUENCEGROUP: SequenceGroupNode,
    NODE_SEQUENCEALIGNMENT: SequenceAlignmentNode,
    NODE_STRUCTUREFACTORS: StructureFactorsNode,
    NODE_TILTSERIES: TiltSeriesNode,
    NODE_TILTSERIESMETADATA: TiltSeriesMetadataNode,
    NODE_TILTSERIESGROUPMETADATA: TiltSeriesGroupMetadataNode,
    NODE_TILTSERIESMOVIE: TiltSeriesMovieNode,
    NODE_TILTSERIESMOVIEMETADATA: TiltSeriesMovieMetadataNode,
    NODE_TILTSERIESMOVIEGROUPMETADATA: TiltSeriesMovieGroupMetadataNode,
    NODE_TOMOGRAM: TomogramNode,
    NODE_TOMOGRAMMETADATA: TomogramMetadataNode,
    NODE_TOMOGRAMGROUPMETADATA: TomogramGroupMetadataNode,
    NODE_TOMOOPTIMISATIONSET: TomoOptimisationSetNode,
    NODE_TOMOTRAJECTORYDATA: TomoTrajectoryDataNode,
    NODE_TOMOMANIFOLDDATA: TomoManifoldDataNode,
}


def all_node_toplevel_types(add_new=False, new_pos=-1) -> List[str]:
    """Get a list of all the node types

    Args:
        add_new (bool): If True 'NEW NODE TYPE (not recommended)' is added to the
            list of node types
        new_pos (int): position to insert the New node tyoe in the nodes list
    """
    nodes = list(NODE_CLASSES)
    nodes.sort()
    if add_new:
        if new_pos >= 0:
            nodes.insert(new_pos, NODE_NEWNODETYPE)
        else:
            nodes.append(NODE_NEWNODETYPE)
    return nodes


def create_node(
    filename: str,
    nodetype: str,
    kwds: Optional[List[str]] = None,
    do_validation: bool = True,
    override_format: str = "",
) -> Node:
    error_msg = check_for_illegal_symbols(nodetype, "node type")
    if error_msg:
        raise ValueError(error_msg)
    if nodetype not in NODE_CLASSES:
        return Node(
            name=filename,
            toplevel_type=nodetype,
            kwds=kwds,
            do_validation=do_validation,
            override_format=override_format,
        )

    the_node = NODE_CLASSES[nodetype](
        name=filename,
        kwds=kwds,
        do_validation=do_validation,
        override_format=override_format,
    )

    return the_node
