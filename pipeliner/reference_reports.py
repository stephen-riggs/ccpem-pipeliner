#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import json
from typing import List, Tuple, Optional, Dict

from pipeliner.pipeliner_job import Ref, ExternalProgram
from pipeliner.project_graph import active_job_from_proc
from pipeliner.summary_data_tools import add_to_summary_file, remove_from_summary_file
from pipeliner.project_graph import ProjectGraph
from pipeliner.utils import get_job_number

REFERENCE_REPORT_DIR = "ReferenceReports"


def write_reference_file(refs: List[Tuple[Ref, List[str]]], outfile: str = "") -> str:
    """Write out a reference report file

    Args:
      refs (list): A list of tuples [(Ref object, [jobs, using, ref])]
      outfile (str): The name of the file to write

    Returns:
        The name of the file written
    """
    ofdir = os.path.dirname(outfile)
    if ofdir:
        os.makedirs(ofdir, exist_ok=True)

    big_ref_dict = {}
    for n, ref_pair in enumerate(refs):
        comb = ref_pair[0].__dict__
        comb["string"] = str(ref_pair[0]).replace("\n", " ")
        comb["jobs"] = ref_pair[1]
        big_ref_dict[n] = comb
    rfdir = os.path.dirname(outfile)
    if os.path.dirname(rfdir):
        os.makedirs(rfdir, exist_ok=True)
    with open(outfile, "w") as f:
        json.dump(big_ref_dict, f, indent=4)

    return outfile


def get_reference_list(
    terminal_job: str,
) -> Dict[str, Tuple[List[ExternalProgram], List[Ref]]]:
    """Prepares a list of every piece of software used to generate a terminal job

    Doesn't get the extra references from fetch jobs

    Args:
        terminal_job (str): The job to start at
    Returns:
        dict: The references used to run the job
            {job_name: [[ExternalProgram], [Ref]]}
    """

    run_jobs = {}
    with ProjectGraph() as pipeline:
        terminal_job_proc = pipeline.find_process(terminal_job)
        upstream = pipeline.get_upstream_network(terminal_job_proc)
        upstream_procs = [os.path.dirname(x[0].name) + "/" for x in upstream]
        upstream_procs.append(terminal_job)

        for job in upstream_procs:
            jobproc = pipeline.find_process(job)
            if jobproc is not None:
                jobinfo = active_job_from_proc(jobproc).jobinfo
                run_jobs[jobproc.name] = jobinfo.programs, jobinfo.references

        return run_jobs


def prepare_reference_report(terminal_job: str, outname: Optional[str] = None) -> int:
    """Prepare a list of literature references for software used in a project and extras

    Also gets the references to maps and models taht have been fetched from outside
    sources

    Args:
        terminal_job (str): The job to trace backwards from
        outname (str): name of file to write

    Returns:
        int: The number of jobs in the report
    """

    with ProjectGraph() as pipeline:
        term_proc = pipeline.find_process(terminal_job)
        backtrace = pipeline.get_upstream_network(term_proc)
    back_procs = set([term_proc] + [x[1] for x in backtrace if x[1] is not None])
    refs_list, jobs_list, refs_set = [], [], []
    for bp in back_procs:
        job = active_job_from_proc(bp)
        for ref in job.jobinfo.references:
            refs_list.append(ref)
            jobs_list.append(job.output_dir)
            if ref not in refs_set:
                refs_set.append(ref)

        # check for the special get_reference_info() function in some jobs
        # IE pipeliner.fetch.pdb, pipeliner.fetch.emdb
        addref = job.get_additional_reference_info()
        if addref:
            refs_list.extend(addref)
            jobs_list.extend([job.output_dir] * len(addref))
            for aref in addref:
                if aref not in refs_set:
                    refs_set.append(aref)
    final_refs_list = []
    rj_pairs = list(zip(refs_list, jobs_list))
    for ref in refs_set:
        joblist = []
        for rjp in rj_pairs:
            if rjp[0] == ref and rjp[1] not in joblist:
                joblist.append(rjp[1])
            joblist.sort(key=lambda x: get_job_number(x))
        final_refs_list.append((ref, joblist))
    # write the file
    if outname is not None:
        write_reference_file(final_refs_list, outname)
    return len(back_procs) + 1


def add_ref_report_to_summary_file(file: str, terminal_job: str, njobs: int) -> None:
    """Add a line to the summary data file with a reference report

    Args:
        file (str): Name of the report file
        terminal_job (str): The job  the report was created from
        njobs (int): Number of jobs covered by the report
    """
    add_to_summary_file("reference_reports", [file, terminal_job, str(njobs)])


def remove_ref_report_from_summary_file(file: str) -> None:
    """Remove a reference report from the summary data file

    Args:
        file (str): The name of the report to remove
    """
    remove_from_summary_file("reference_reports", file)
