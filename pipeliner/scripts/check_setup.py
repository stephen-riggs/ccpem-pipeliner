#!/usr/bin/env python

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#


import sys

ersp = " " * 7
RED = "\033[91m"
GREEN = "\33[32m"
YELLOW = "\033[33m"
END = "\033[0m"
BLUEBG = "\33[44m"


def check_job(job):
    error = False
    sys.stdout.write(
        "\n{}.{}.".format(job.PROCESS_NAME, "." * (50 - len(job.PROCESS_NAME)))
    )
    if all(x.exe_path for x in job.jobinfo.programs):
        sys.stdout.write(GREEN + "Success" + END)
        for prog in job.jobinfo.programs:
            sys.stdout.write(f"\n{52 * ' '}{prog.exe_path}")
        sys.stdout.flush()
    else:
        sys.stdout.write(YELLOW + "Unavailable" + END)
        sys.stdout.flush()
        for prog in job.jobinfo.programs:
            if prog.exe_path:
                sys.stdout.write(f"\n{52 * ' '}{prog.exe_path}")
            else:
                sys.stdout.write(
                    f"\n{52 * ' '}{RED}'{prog.command}' missing from system "
                    f"path{END}"
                )

        error = True
    return error


def main():

    try:
        from shutil import which
    except ImportError:
        print(
            RED + "\nERROR" + END + ": Problem importing the `shutil.which` module."
            "\n{0}This is most likely due to using an older version of python"
            "\n{0}The Pipeliner is designed for python 3.8+\n{0}The current python "
            "version is: {1}\n".format(" " * 7, sys.version.split("\n")[0])
        )
        sys.exit(0)

    try:
        from pipeliner.job_factory import get_job_types
    except ImportError as e:
        print(
            RED
            + "\nERROR"
            + END
            + ": Basic Pipeliner functionality could not be opened."
            f" The pipeliner is not installed correctly.\n{str(e)}"
        )
        return

    jobs = get_job_types()

    pipeliner_programs = [
        "pipeliner",
        "pipeliner.bfactor_plot",
        "pipeliner.update_metadata_schema",
        "pipeliner.recover_project",
        "pipeliner.check_setup",
        "pipeliner.regenerate_results",
    ]

    print("\n{0}\nCCPEM pipeliner setup check\n{0}\n".format("=" * 27))
    print(BLUEBG + "-- checking python version --" + END)
    print("Python vers: " + sys.version)

    sys.stdout.write("\n" + BLUEBG + "-- checking available jobtypes --" + END)
    sys.stdout.flush()
    # check for relion in path

    job_err = []
    for job in jobs:
        job_err.append(check_job(job))
    pyvers = sys.version.split()[0].rpartition(".")[0].split(".")
    relion_errors = any(job_err)

    sys.stdout.write("\n\n" + BLUEBG + "-- checking pipeliner setup --" + END)
    sys.stdout.flush()
    # check for pipeliner programs
    pipe_errors = False
    for pipex in pipeliner_programs:
        spacer = int(1.25 * max([len(x) for x in pipeliner_programs]))
        sys.stdout.write(
            "\n Looking for pipeliner programs: {}.{}.".format(
                pipex, "." * (spacer - len(pipex))
            )
        )

        findit = which(pipex)
        if findit:
            sys.stdout.write(GREEN + "Success" + END)
            sys.stdout.flush()
        else:
            sys.stdout.write(RED + "ERROR" + END)
            sys.stdout.flush()
            pipe_errors = True

    print("\n\n" + BLUEBG + "== Final setup report ==" + END)

    if not pipe_errors and not relion_errors:
        msg = (
            GREEN + "SUCCESS: The pipeliner and all job types appear to be correctly"
            " installed and ready to use" + END
        )

    else:
        if relion_errors:
            print(
                YELLOW + "\nWARNING" + END + ": Some jobtypes are unavailable\n"
                "{0}Make sure the executables for these jobs are installed\n{0}and "
                "available in your system path \n".format(" " * 9)
            )
            msg = "The pipeliner is correctly installed, but some jobs are unavailable"

        if pipe_errors:
            print(
                RED + "\nERROR" + END + ": There appears to be a problem with the "
                "installation of the pipeliner\n{}Could not find one or more of the "
                "pipeliner programs, try reinstalling".format(ersp)
            )
            print(
                RED
                + "SETUP FAILURE"
                + END
                + ": Problems were found with the pipeliner installation!"
            )
            msg = "The pipeliner is not installed correctly"

    if int(pyvers[0]) < 3 or int(pyvers[1]) < 9:
        print(
            "\n" + YELLOW + "WARNING" + END + ": Your python version is {0}\n{1}Some "
            "prototype functions require python 3.9 or higher\n{1}The basic "
            "pipeliner functions should work but some\n{1}advanced prototype features"
            " may behave strangely".format(sys.version.split()[0], " " * 9)
        )
    if int(pyvers[0]) < 3 or int(pyvers[1]) < 8:
        print(
            RED + "\nERROR" + END + ":\tYour Python version is {}\n\tThe pipeliner "
            "requires python > 3.8".format(pyvers)
        )
        msg = "Pipeliner cannot run because python needs to be updated"

    print("\n{0}\n{1}\n{0}\n".format("=" * len(msg), msg))


if __name__ == "__main__":
    main()
