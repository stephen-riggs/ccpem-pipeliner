#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
"""
job_runner.py
=================

This script is responsible for executing pipeliner jobs.

It is intended only for internal use by the pipeliner and should not normally be called
by any other code.

Users should instead run jobs using the "--run_job" argument to the standard "pipeliner"
command.
"""
import argparse
import os
import shlex
import subprocess
import sys
import traceback
from pathlib import Path
from typing import List, Optional

from pipeliner import job_factory
from pipeliner.data_structure import (
    SUCCESS_FILE,
    FAIL_FILE,
    ABORT_FILE,
    ABORT_TRIGGER,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_ABORT,
    JOBSTATUS_FAIL,
)
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.process import Process
from pipeliner.project_graph import ProjectGraph
from pipeliner.utils import (
    date_time_tag,
    subprocess_popen,
    update_jobinfo_file,
)


def run_job(job_file: Path, job_dir: Optional[Path] = None) -> None:
    """Run a job loaded from a job.star or run.job file.

    The job's output_dir is set as the directory containing the job file, unless the
    job_dir argument is given in which case that is used instead.
    """
    if job_dir is None:
        job_dir = job_file.parent
    try:
        if not job_file.is_file():
            raise FileNotFoundError(f"Job file {job_file} does not exist")
        print(f"{date_time_tag()}: CCP-EM Pipeliner: Loading job from {job_file}")
        job = job_factory.read_job(os.fspath(job_file))
        job.output_dir = os.fspath(job_dir) + "/"
        runner = JobRunner(job)
    except BaseException as ex:
        # Catch-all - if something unexpected failed, log it, mark the job as
        # failed and make sure we re-raise the exception.
        try:
            job_dir.mkdir(parents=True, exist_ok=True)
            (job_dir / FAIL_FILE).touch()
            with open(job_dir / "run.err", "a") as err_file:
                print(f"ERROR: Failed to read job file {job_file}", file=err_file)
                traceback.print_exc(file=err_file)
        finally:
            raise ex

    runner.run_job()


class JobFailedException(Exception):
    """For internal use. Raised when the job has failed."""

    pass


class JobAbortedException(Exception):
    """For internal use. Raised when the job should abort or has been aborted."""

    pass


class JobRunner:
    """This class is responsible for running a single pipeliner job.

    It runs each command in turn, followed by the job's create_post_run_output_nodes().

    If there are any errors or a command has non-zero exit status, the job is marked
    as failed.

    If the abort trigger file is created while the job is running, the current command
    is terminated and the job is marked as aborted.

    Args:
        job: The job to run.
        abort_poll_interval: The time to wait before checking if the abort trigger file
            has been written. The default is 5 seconds. There should normally be no
            need to change this, it is configurable primarily to allow unit tests to run
            more quickly.
    """

    def __init__(self, job: PipelinerJob, abort_poll_interval=5.0):
        super().__init__()
        self.job = job
        self.abort_poll_interval = abort_poll_interval

        # Prepare some file paths we will need later
        self.job_name = job.output_dir
        job_dir = Path(self.job_name)
        self.out_file_path = job_dir / "run.out"
        self.err_file_path = job_dir / "run.err"
        self.note_file_path = job_dir / "note.txt"
        self.success_file_path = job_dir / SUCCESS_FILE
        self.failed_file_path = job_dir / FAIL_FILE
        self.aborted_file_path = job_dir / ABORT_FILE
        self.abort_trigger_path = job_dir / ABORT_TRIGGER

    @staticmethod
    def log_to_stdout_and_file(msg, file: Optional[os.PathLike] = None):
        """Log a message to sys.stdout and optionally also to a named file."""
        print(msg, flush=True)
        if file is not None:
            with open(file, "a") as log_file:
                print(msg, file=log_file)

    def log(self, msg):
        self.log_to_stdout_and_file(
            f"{date_time_tag()}: CCP-EM Pipeliner job {self.job_name}: {msg}",
            self.out_file_path,
        )

    def check_abort_trigger(self):
        if self.abort_trigger_path.is_file():
            self.log("Found abort trigger file, aborting job")
            raise JobAbortedException

    def clear_termination_status_indicators(self):
        self.success_file_path.unlink(missing_ok=True)
        self.failed_file_path.unlink(missing_ok=True)
        self.aborted_file_path.unlink(missing_ok=True)

    def write_note_txt_file(self, command_list):
        with open(self.note_file_path, "a") as note_file:
            print(get_note_txt_str(self.job_name, command_list), file=note_file)

    def run_single_command(self, number: int, command: List[str]) -> None:
        """Run a single command in a job.

        Returns: None, if the command was successful

        Raises:
            JobAbortedException: If the job was aborted
            JobFailedException: If the command failed or an error occurred
        """
        # Open the stdout and stderr files so they can be passed to the subprocess
        with open(self.err_file_path, "a") as err_file, open(
            self.out_file_path, "a"
        ) as out_file:
            # Start the command running in a subprocess
            process = subprocess_popen(
                command, stdout=out_file, stderr=err_file, cwd=self.job.working_dir
            )
            try:
                update_jobinfo_file(self.job.output_dir, command_list=command)
            except Exception as ex:
                self.log(f"Error updating job info file: {ex}")
            try:
                # Wait for the command to finish
                while True:
                    # Check to see if the abort trigger file has been written. If so,
                    # a JobAbortedException will be raised by this check.
                    self.check_abort_trigger()
                    try:
                        # Check to see if the subprocess has finished. If so, break out
                        # of the while loop and continue to check the exit status below.
                        process.wait(self.abort_poll_interval)
                        break
                    except subprocess.TimeoutExpired:
                        # The process is still running, so we repeat the loop.
                        pass
            except JobAbortedException:
                # Trigger file was found, abort job now
                # First try to terminate the process cleanly with SIGTERM.
                out_file.flush()
                self.log(f"Terminating command {number}")
                try:
                    update_jobinfo_file(self.job.output_dir, action="Aborted job")
                except Exception as ex:
                    self.log(f"Error updating job info file: {ex}")
                process.terminate()
                try:
                    # Give the process a few seconds to shut itself down cleanly.
                    process.wait(self.abort_poll_interval)

                    # The process has terminated, log it and raise JobAbortedException
                    # to break out of this function and return control to the caller.
                    self.log(f"Command {number} terminated")
                    raise JobAbortedException
                except subprocess.TimeoutExpired:
                    # The process has not responded in a reasonable time to SIGTERM,
                    # so now we kill it without chance of cleanup using SIGKILL.
                    self.log(
                        "Timed out waiting for process to stop. Killing command"
                        f" {number}"
                    )
                    process.kill()
                    try:
                        process.wait(5.0)

                        # Process has terminated, log it and raise JobAbortedException
                        # to return control to the caller.
                        self.log(f"Command {number} killed")
                        raise JobAbortedException
                    except subprocess.TimeoutExpired:
                        raise RuntimeError(
                            "Process still running after kill signal. This should never"
                            " happen. Please report this error to the CCP-EM Pipeliner"
                            " developers."
                        )
        # The subprocess has finished running. Check its exit status to see if it was
        # successful or not.
        exit_status = process.returncode
        if exit_status == 0:
            # Zero exit status indicates success. Log it and return.
            self.log(f"Command {number} finished successfully")
            return
        else:
            # Non-zero exit status indicates an error or failure of some kind. Log it
            # and raise JobFailedException to indicate to the caller that the command
            # has failed.
            self.log(f"ERROR: Command {number} failed with exit status {exit_status}")
            raise JobFailedException

    def run_post_run_node_creation(self) -> None:
        """Run the job's post run node creation function.

        Returns: None, if the post run node creation was successful

        Raises:
            JobFailedException: If an error occurred
        """
        try:
            self.job.create_post_run_output_nodes()
        except Exception:
            self.log("ERROR: post run node creation failed")
            with open(self.err_file_path, "a") as err_file:
                print("ERROR: post run node creation raised an error", file=err_file)
                traceback.print_exc(file=err_file)
            raise JobFailedException

    def write_job_pipeline(self, job_status: str):
        """Update the job's status in job_pipeline.star and add its output nodes

        Args:
            job_status (str): The job's status
        """
        job_pipeline_file = Path(self.job_name) / "job_pipeline.star"
        with ProjectGraph(
            name="job",
            pipeline_dir=self.job_name,
            read_only=False,
            create_new=not job_pipeline_file.is_file(),
        ) as job_pipeline:
            process = Process(self.job_name, self.job.PROCESS_NAME, job_status)
            found_proc = job_pipeline.add_process(process, do_overwrite=True)
            # add the nodes if the files exist
            for node in self.job.output_nodes:
                if Path(node.name).is_file():
                    job_pipeline.add_new_output_edge(found_proc, node)

    def run_job(self):
        """Run the job."""
        try:
            # Prepare to run the job
            self.clear_termination_status_indicators()
            command_list = self.job.get_final_commands()
            self.write_note_txt_file(command_list)

            # Run each of the job's commands in turn
            for number, command in enumerate(command_list, start=1):
                self.check_abort_trigger()
                self.log(
                    f"Running command {number} of {len(command_list)}:"
                    f" {shlex.join(command)}"
                )
                self.run_single_command(number, command)

            # Run the job's post run actions
            self.check_abort_trigger()
            self.run_post_run_node_creation()

            # Create or update the job's mini-pipeline file
            self.write_job_pipeline(job_status=JOBSTATUS_SUCCESS)

            # If we got here with no exceptions, everything has completed normally.
            # We can mark the job as finished and successful.
            self.log("Job finished")
            self.success_file_path.touch()

        except JobFailedException:
            # The job has failed. Log a message and create an indicator file.
            self.log("Job failed")
            self.write_job_pipeline(job_status=JOBSTATUS_FAIL)
            self.failed_file_path.touch()

        except JobAbortedException:
            # The job was aborted. Log a message and create an indicator file.
            self.log("Job aborted")
            self.write_job_pipeline(job_status=JOBSTATUS_ABORT)
            self.aborted_file_path.touch()

        except BaseException as ex:
            # Catch-all - if something unexpected failed, log it, mark the job as
            # failed and make sure we re-raise the exception.
            try:
                self.log(
                    "WARNING: Job failed with an unexpected error. Job processes might"
                    " not have been cleaned up."
                )
                self.failed_file_path.touch()
                with open(self.err_file_path, "a") as err_file:
                    traceback.print_exc(file=err_file)
                self.write_job_pipeline(job_status=JOBSTATUS_FAIL)
            finally:
                raise ex

        finally:
            # Ensure that the abort trigger is always removed
            self.abort_trigger_path.unlink(missing_ok=True)


def get_note_txt_str(job_name, command_list):
    # Trick to leave out the 's' of 'commands' if there's only one
    # see https://stackoverflow.com/a/65063284
    plural_suffix = "s"[: len(command_list) ^ 1]
    note_list = [
        f" ++++ CCP-EM Pipeliner running job {job_name} at {date_time_tag()}",
        f" ++++ with the following command{plural_suffix}:",
    ]
    for com in command_list:
        note_list.append(shlex.join(com))
    note_list.append(" ++++")
    return "\n".join(note_list)


def main(args_list: Optional[List[str]] = None):
    if args_list is None:
        args_list = sys.argv[1:]
    parser = argparse.ArgumentParser(
        description="Job runner, used internally by the Pipeliner to run jobs."
    )
    parser.add_argument("job_file", help="The run.job or job.star file to run")
    args = parser.parse_args(args_list)
    run_job(Path(args.job_file))


if __name__ == "__main__":
    main()
