#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os

import numpy as np
import mrcfile
import sys
import argparse
from pathlib import Path


def get_arguments():
    parser = argparse.ArgumentParser(description="MRC stack summed power spectra")

    parser.add_argument(
        "--stack",
        help="The padded particle image stack",
        nargs="?",
        metavar="padded particle stack ",
        required=True,
    )
    parser.add_argument(
        "--outdir",
        help="Directory to write the out put file to",
        nargs="?",
        metavar="padded particle stack ",
        required=True,
    )
    return parser


# read in mrc stack
def main(in_args=None) -> None:
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    with mrcfile.open(args.stack) as mrc:
        mrc_data = mrc.data

    # put power spectra into new array
    ps_array: np.ndarray = np.zeros(mrc_data.shape, dtype=np.float32)
    for n in range(mrc_data.shape[0]):
        img = mrc_data[n, :, :]
        fft = np.fft.fftshift(np.fft.fft2(img))
        amps: np.ndarray = np.square(np.real(fft)).astype(np.float32)
        ps_array[n, :, :] = amps

    # sum the power spectra
    summed = ps_array.sum(axis=0)
    os.makedirs(args.outdir, exist_ok=True)
    outfile = Path(args.outdir) / "average_ps.mrc"
    with mrcfile.new(str(outfile), overwrite=True) as out_mrc:
        out_mrc.set_data(summed)


if __name__ == "__main__":
    main()
