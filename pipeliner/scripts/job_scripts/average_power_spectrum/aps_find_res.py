#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import numpy as np
import argparse
import sys
import os

from pipeliner.starfile_handler import DataStarFile
from pipeliner.utils import make_pretty_header


def get_arguments():
    parser = argparse.ArgumentParser(description="MRC stack summed power spectra")

    parser.add_argument(
        "--ps_size",
        help="The power spectrum image size in pixels",
        nargs="?",
        metavar="Power spectrum size in px",
        required=True,
        type=int,
    )
    parser.add_argument(
        "--apix",
        help="Image pixes size in angstrom",
        nargs="?",
        metavar="pixel size (Å)",
        required=True,
        type=float,
    )
    parser.add_argument(
        "--coords",
        help="STAR file with coordinates to analyse",
        nargs="?",
        metavar="coords STAR file",
        required=True,
    )
    parser.add_argument(
        "--outdir",
        help="Where to write the output",
        nargs="?",
        metavar="output dir",
        required=True,
    )
    return parser


def main(in_args=None):
    output = [
        "data_coordinates",
        "loop_",
        "_rlnCoordinateX",
        "_rlnCoordinateY",
        "_rlnResolution",
    ]

    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    if os.path.isfile(args.coords):
        raw_coords = DataStarFile(args.coords)
        coords = raw_coords.loop_as_list("", ["_rlnCoordinateX", "_rlnCoordinateY"])
        center = [0.5 * args.ps_size, 0.5 * args.ps_size]
        resos = []
        for cpair in coords:
            xy = [float(x) for x in cpair]
            dist = np.sqrt((center[0] - xy[0]) ** 2 + (center[1] - xy[1]) ** 2)
            nyquist = 2 * args.apix
            if dist:
                resolution = ((args.ps_size * 0.5) * nyquist) / dist
            else:
                resolution = float("inf")
            resos.append(f"{int(xy[0]):5d} {int(xy[1]):5d}  {resolution:0.3f}")

        print(
            make_pretty_header(
                text=f"Found resolution for {len(coords)} points", char="-"
            )
        )
        output.extend(resos)

    output_file = os.path.join(args.outdir, "selected_features.star")
    os.makedirs(args.outdir, exist_ok=True)
    with open(output_file, "w") as outfile:
        for line in output:
            outfile.write(f"{line}\n")


if __name__ == "__main__":
    main()
