import argparse
import sys
from pathlib import Path


def get_arguments():
    parser = argparse.ArgumentParser(description="Check a job's output files exist")

    parser.add_argument(
        "--files",
        "-f",
        help="Expected files",
        nargs="*",
        metavar="Expected files",
        type=str,
    )

    return parser


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    print("\n-- Checking for expected output files --")
    files = [] if args.files is None else args.files

    errors = []
    for f in files:
        if not Path(f).is_file():
            errors.append(f)
    if errors:
        missing = ", ".join(files)
        raise ValueError(
            f"The following files are expected to exist but were not found {missing}"
        )


if __name__ == "__main__":
    main()
