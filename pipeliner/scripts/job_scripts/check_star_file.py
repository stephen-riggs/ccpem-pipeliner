#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import sys
from pipeliner.starfile_handler import DataStarFile
import argparse
from typing import Optional, List
from pipeliner.data_structure import RELION_SUCCESS_FILE
from pathlib import Path


def get_arguments():
    description = "Check a starfile contains data in the stated block"
    parser = argparse.ArgumentParser(
        prog="starfile_check", description="".join(description)
    )
    parser.add_argument(
        "--fn_in",
        help="Input file name",
        nargs="?",
        metavar="Input file",
        required=True,
    )

    parser.add_argument(
        "--block",
        help="Block name to check",
        nargs="?",
        metavar="block",
        default=None,
    )

    parser.add_argument(
        "--clear_relion_success_file",
        help=(
            'If the star file is empty, remove a "RELION_JOB_SUCCESS" file from the'
            " job if there is one"
        ),
        action="store_true",
    )

    return parser


def main(in_args: Optional[List[str]] = None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    err = ""
    try:
        infile = DataStarFile(args.fn_in)
        n_block = infile.count_block(args.block)
        bn = f"Block {args.block} in " if args.block is not None else "The file "
    except Exception as e:
        err = f"An error occurred trying to read {args.fn_in}: {e}"
    else:
        if n_block == 0:
            err = f"{bn} {args.fn_in} does not contain any data"
    finally:
        if err:
            if args.clear_relion_success_file:
                success_file = Path(args.fn_in).parent / RELION_SUCCESS_FILE
                success_file.unlink(missing_ok=True)
            raise RuntimeError(
                f"{err}. "
                f"This generally means the job that created this file has failed."
            )
    return True


if __name__ == "__main__":
    main()
