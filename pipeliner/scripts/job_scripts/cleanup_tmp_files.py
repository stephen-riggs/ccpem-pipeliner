import argparse
import shutil
import sys
from pathlib import Path


def get_arguments():
    parser = argparse.ArgumentParser(description="Clean up temporary files")

    parser.add_argument(
        "--files",
        "-f",
        help="Temp files to remove",
        nargs="*",
        metavar="Files to remove",
        type=str,
    )
    parser.add_argument(
        "--dirs",
        "-d",
        help="Temporary dirs to remove (along with their contents)",
        nargs="*",
        metavar="Dirs to remove",
        type=str,
    )

    return parser


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    print("\n-- Cleaning up temporary files and directories --")
    files = [] if args.files is None else args.files
    dirs = [] if args.dirs is None else args.dirs

    for f in files:
        sys.stdout.write(f"Removing: {f}...")
        sys.stdout.flush()
        try:
            Path(f).unlink()
            sys.stdout.write("SUCCESS\n")
        except Exception as e:
            sys.stdout.write(f"ERROR: {e}\n")

    for d in dirs:
        sys.stdout.write(f"Removing: {d}...")
        sys.stdout.flush()
        try:
            shutil.rmtree(d)
            sys.stdout.write("SUCCESS\n")
        except Exception as e:
            sys.stdout.write(f"ERROR: {e}\n")


if __name__ == "__main__":
    main()
