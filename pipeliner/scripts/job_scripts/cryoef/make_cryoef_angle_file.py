#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import sys
import os
from typing import Optional, List
import argparse

from gemmi import cif


def main(in_args: Optional[List[str]] = None):
    """Generates a cryoEF input angles file from a Relion particles starfile"""
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_starfile", type=str, required=True)
    parser.add_argument("--output_dir", type=str, required=True)

    if in_args is None:
        in_args = sys.argv[1:]
    args = parser.parse_args(in_args)

    # convert input an angles file
    in_star = cif.read_file(args.input_starfile)
    parts_block = in_star.find_block("particles")
    phi_angs = parts_block.find_loop("_rlnAngleTilt")
    psi_angs = parts_block.find_loop("_rlnAngleRot")
    angles_file = os.path.join(args.output_dir, "cryoef_angles.dat")
    with open(angles_file, "w") as output_file:
        for pair in zip(phi_angs, psi_angs):
            output_file.write("{}\n".format("\t".join(pair)))


if __name__ == "__main__":
    main()
