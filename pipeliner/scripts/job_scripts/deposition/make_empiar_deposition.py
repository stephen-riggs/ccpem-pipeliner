#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import argparse

from pipeliner.api.manage_project import PipelinerProject


def main():
    parser = argparse.ArgumentParser(
        prog="empiar_deposition_creation",
        description="make an EMPIAR deposition template",
    )

    parser.add_argument(
        "--terminal_job",
        default=None,
        help="The terminal job to create the deposition from",
    )

    parser.add_argument(
        "--jobstar_file",
        default=None,
        help="The jobstar for a pipeliner.deposition.empiar job",
    )
    parser.add_argument(
        "--movies",
        help=(
            "(optional) Add this argument to deposition to include unprocessed movies "
            "in the EMPIAR deposition"
        ),
        action="store_true",
    )
    parser.add_argument(
        "--mics",
        help=(
            "(optional) Add this argument to deposition to include corrected "
            "micrographs in the EMPIAR deposition"
        ),
        action="store_true",
    )
    parser.add_argument(
        "--parts",
        help=(
            "(optional) Add this argument to deposition to include particles in the "
            "EMPIAR deposition"
        ),
        action="store_true",
    )
    parser.add_argument(
        "--corr_parts",
        help=(
            "(optional) Add this argument to deposition to include corrected "
            "(polished) particles in the EMPIAR deposition"
        ),
        action="store_true",
    )

    args = parser.parse_args()
    proj = PipelinerProject()
    jobname = proj.parse_procname(args.terminal_job)
    movs = args.movies
    mics = args.mics
    parts = args.parts
    rparts = args.corr_parts
    if not any([movs, mics, parts, rparts]):
        raise ValueError(
            "Nothing selected for deposition add '--movies', '--mics', '--parts', "
            "and/or '--corr_parts arguments' to the command"
        )
    proj.prepare_deposition(
        terminal_job=jobname,
        depo_type="empiar",
        jobstar_file=args.jobstar_file,
        empiar_do_mov=movs,
        empiar_do_mics=mics,
        empiar_do_parts=parts,
        empiar_do_rparts=rparts,
    )


if __name__ == "__main__":
    main()
