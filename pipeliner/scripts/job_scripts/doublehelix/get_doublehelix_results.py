#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import argparse
import json
from typing import Optional


def parse_args():
    parser = argparse.ArgumentParser(description="get doubleHelix results")
    parser.add_argument(
        "-j",
        "--jsonout",
        required=True,
        help="Input doubleHelix result (.json)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-id",
        "--id",
        required=False,
        default="doublehelix_id",
        help="ID used to save results json file",
    )

    return parser.parse_args()


def get_seqassign_results(text_string, chain_result):
    text_string += "*** Sequence assignment: \n\n"
    try:
        text_string += (
            f"Input model: \n"
            f"    chain {chain_result['chain_id']} and "
            f"residues {chain_result['input_resseq_start']}-"
            f"{chain_result['input_resseq_end']}\n"
        )
    except KeyError:
        pass
    try:
        seqname = chain_result.get("assigned_sequence_name", "")
        text_string += (
            f"Assigned sequence: {seqname}\n"
            f"    {chain_result['output_resseq_start']}-"
            f"{chain_result['output_resseq_end']}; "
            f"p-value {chain_result['pvalue']:.2e};\n\n"
        )
    except KeyError:
        pass
    try:
        len_seq = len(chain_result["input_sequence"])
        seglen = 60
        for lseq in range(0, len_seq, 60):
            text_string += (
                "input_sequence   :  "
                f"{chain_result['input_sequence'][lseq:min(len_seq,lseq+seglen)]}"
                "\n"
            )
            text_string += (
                "assigned_sequence:  "
                f"{chain_result['assigned_sequence'][lseq:min(len_seq,lseq+seglen)]}\n"
            )
            text_string += (
                "RSCC deciles     :  "
                f"{chain_result['density_fit_rscc'][lseq:min(len_seq,lseq+seglen)]}"
                "\n\n\n"
            )
    except KeyError:
        pass
    return text_string


def get_doublehelix_results(checkseq_scores, dict_doublehelix_results):
    text_string = ""
    for seq_result in checkseq_scores:
        chain_result = checkseq_scores[seq_result]
        if text_string:
            text_string += "\n\n"  # separate chain results
        if "assigned_sequence" in chain_result:
            text_string = get_seqassign_results(text_string, chain_result)
        elif "evalue" in chain_result:
            text_string += "*** Sequence identification: \n\n"
            try:
                text_string += (
                    f">sequence_id: {chain_result['sequence_id']}; "
                    f"E-value: {chain_result['evalue']}\n"
                )
            except KeyError:
                pass

            try:
                len_seq = len(chain_result["sequence"])
                seglen = 80
                for lseq in range(0, len_seq, seglen):
                    text_string += (
                        f"{chain_result['sequence'][lseq:min(len_seq,lseq+seglen)]}"
                        "\n"
                    )

                _a = dict_doublehelix_results.setdefault("doublehelix_hits", {})
                _a[float(chain_result["evalue"])] = (
                    f">{chain_result['sequence_id']};"
                    f"E-value: {chain_result['evalue']}\n"
                    f"{chain_result['sequence']}"
                )

            except KeyError:
                pass
    dict_doublehelix_results["doublehelix_results"] = text_string


def parse_doublehelix_json(out_json: str, out_id: Optional[str] = None):
    dict_doublehelix_results: dict = {}
    with open(out_json, "r") as j:
        doublehelix_results = json.load(j)

    get_doublehelix_results(doublehelix_results, dict_doublehelix_results)

    if not out_id:
        out_id = "doublehelix"
    results_json = out_id + "_doublehelixparse.json"
    with open(results_json, "w") as j:
        json.dump(dict_doublehelix_results, j)


def main():
    args = parse_args()
    # read input
    doublehelix_outputfile = args.jsonout
    parse_doublehelix_json(out_json=doublehelix_outputfile, out_id=args.id)


if __name__ == "__main__":
    main()
