#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import sys
from typing import Optional, List

"""
This is required as passing phil values via the command results in a bug where
the molecule attributes (molecule_name, map_or_model_file, starting_model_vrms)
result in three molecule groups with one attribute as opposed to one molecule
group with three attributes.  If more phil parsing is required consider using:

https://pypi.org/project/freephil/
"""

# TODO: This should be rewritten to use argparse to make it more robust


def main(inputs: Optional[List] = None):
    if inputs is None:
        (
            full_map,
            half_map_1,
            half_map_2,
            resolution,
            molecule_name,
            map_or_model_file,
            starting_model_vrs,
            centre_x,
            centre_y,
            centre_z,
            path,
        ) = sys.argv[1:]
    else:
        (
            full_map,
            half_map_1,
            half_map_2,
            resolution,
            molecule_name,
            map_or_model_file,
            starting_model_vrs,
            centre_x,
            centre_y,
            centre_z,
            path,
        ) = inputs

    phil_text = f"""
    voyager
        {{
        remove_phasertng_folder = True
        model_file = {map_or_model_file}
        map = {full_map}
        map1 = {half_map_1}
        map2 = {half_map_2}
        d_min = {resolution}
        model_vrms = {starting_model_vrs}
        sphere_center = {centre_x},{centre_y},{centre_z}
        biological_unit {{
        molecule
        {{
            molecule_name = {molecule_name}
        }}
        }}
    }}
    """
    with open(path, "w") as phil:
        phil.write(phil_text)


if __name__ == "__main__":
    main()
