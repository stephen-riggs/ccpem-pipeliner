import argparse
import os

from ccpem_utils.model.gemmi_model_utils import (
    GemmiModelUtils,
)


def parse_args():
    parser = argparse.ArgumentParser(description="CCP-EM analyse B-factors")
    parser.add_argument(
        "-p",
        "--model",
        required=True,
        help="Input atomic model (.pdb, .cif)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-mid",
        "--mid",
        required=False,
        default=None,
        help="Model ID to prefix output file names",
    )

    return parser.parse_args()


def write_fasta_from_structure(modelfile: str, output_fasta: str) -> None:
    gu = GemmiModelUtils(modelfile)
    dict_sequences = gu.get_sequence_from_atom_records(keep_gaps=False)
    with open(output_fasta, "w") as o:
        for k in sorted(dict_sequences.keys()):
            o.write(f">Chain {k}\n")
            o.write(f"{dict_sequences[k]}\n")


def main():
    args = parse_args()
    # input model
    modelfile = args.model
    # output file
    if args.mid:
        modelid = args.mid
    else:
        modelid = os.path.splitext(os.path.basename(modelfile))[0]
    seq_output = modelid + "_seq.fasta"
    if args.odir:
        seq_output = os.path.join(args.odir, seq_output)
    write_fasta_from_structure(modelfile, output_fasta=seq_output)


if __name__ == "__main__":
    main()
