from PIL import Image
import os
import argparse
import sys


def get_arguments():
    parser = argparse.ArgumentParser(description="Make emdb validation images")

    parser.add_argument(
        "--outdir",
        "-o",
        help="Output directory",
        metavar="Output directory",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--map",
        help="Input map",
        metavar="Input map",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--model",
        help="Input model",
        metavar="Input model",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--hm1",
        help="Halfmap 1",
        metavar="Halfmap 1",
        required=False,
        type=str,
    )
    parser.add_argument(
        "--hm2",
        help="Halfmap 2",
        metavar="Halfmap 2",
        required=False,
        type=str,
    )
    return parser


def convert_img_tifflzw(img):
    with Image.open(img) as im:
        im.save(os.path.splitext(img)[0] + ".tif", "TIFF", compression="tiff_lzw")


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    input_map = args.map
    input_map = os.path.join(args.outdir, os.path.basename(input_map))
    input_map_basename = os.path.basename(input_map)
    input_model = args.model
    if input_model:
        input_model = os.path.join(args.outdir, os.path.basename(input_model))
    list_images_to_tif = []
    # the output paths are relative to project dir
    # orthogonal projections
    x_proj = input_map + "_xprojection.jpeg"
    y_proj = input_map + "_yprojection.jpeg"
    z_proj = input_map + "_zprojection.jpeg"
    list_images_to_tif.extend([x_proj, y_proj, z_proj])
    # orthogonal max-value projections
    x_projmax = input_map + "_xmax.jpeg"
    y_projmax = input_map + "_ymax.jpeg"
    z_projmax = input_map + "_zmax.jpeg"
    list_images_to_tif.extend([x_projmax, y_projmax, z_projmax])
    # orthogonal false color max value projections
    x_projglowmax = input_map + "_glow_xmax.jpeg"
    y_projglowmax = input_map + "_glow_ymax.jpeg"
    z_projglowmax = input_map + "_glow_zmax.jpeg"
    list_images_to_tif.extend([x_projglowmax, y_projglowmax, z_projglowmax])
    # orthogonal false color max value projections
    x_projstd = input_map + "_xstd.jpeg"
    y_projstd = input_map + "_ystd.jpeg"
    z_projstd = input_map + "_zstd.jpeg"
    list_images_to_tif.extend([x_projstd, y_projstd, z_projstd])
    # orthogonal false color std dev projections
    x_projglowstd = input_map + "_glow_xstd.jpeg"
    y_projglowstd = input_map + "_glow_ystd.jpeg"
    z_projglowstd = input_map + "_glow_zstd.jpeg"
    list_images_to_tif.extend([x_projglowstd, y_projglowstd, z_projglowstd])
    # central slices
    x_slicecentral = input_map + "_scaled_xcentral_slice.jpeg"
    y_slicecentral = input_map + "_scaled_ycentral_slice.jpeg"
    z_slicecentral = input_map + "_scaled_zcentral_slice.jpeg"
    list_images_to_tif.extend([x_slicecentral, y_slicecentral, z_slicecentral])
    # largest variance slices
    x_slicelargevar = input_map + "_xlargestvariance_slice.jpeg"
    y_slicelargevar = input_map + "_ylargestvariance_slice.jpeg"
    z_slicelargevar = input_map + "_zlargestvariance_slice.jpeg"
    list_images_to_tif.extend([x_slicelargevar, y_slicelargevar, z_slicelargevar])
    # surface views
    x_surface = input_map + "_scaled_xsurface.jpeg"
    y_surface = input_map + "_scaled_ysurface.jpeg"
    z_surface = input_map + "_scaled_zsurface.jpeg"
    list_images_to_tif.extend([x_surface, y_surface, z_surface])
    # map with model views
    x_modelsurf = input_model + "_" + input_map_basename + "_scaled_xsurface.jpeg"
    y_modelsurf = input_model + "_" + input_map_basename + "_scaled_ysurface.jpeg"
    z_modelsurf = input_model + "_" + input_map_basename + "_scaled_zsurface.jpeg"
    list_images_to_tif.extend([x_modelsurf, y_modelsurf, z_modelsurf])
    # model views
    x_modelfit = input_model + "_" + input_map_basename + "_scaled_xfitsurface.jpeg"
    y_modelfit = input_model + "_" + input_map_basename + "_scaled_yfitsurface.jpeg"
    z_modelfit = input_model + "_" + input_map_basename + "_scaled_zfitsurface.jpeg"
    list_images_to_tif.extend([x_modelfit, y_modelfit, z_modelfit])
    # half map input?
    input_hmap1 = args.hm1
    input_hmap2 = args.hm2
    if all([input_hmap1 is not None, input_hmap2 is not None]):
        input_hmap1_basename = os.path.basename(input_hmap1)
        input_hmap2_basename = os.path.basename(input_hmap2)
        hmap_basename = "_".join(
            [input_hmap2_basename, input_hmap1_basename, "rawmap.map"]
        )
        hmap_path = os.path.join(args.outdir, hmap_basename)
        x_proj = hmap_path + "_xprojection.jpeg"
        y_proj = hmap_path + "_yprojection.jpeg"
        z_proj = hmap_path + "_zprojection.jpeg"
        list_images_to_tif.extend([x_proj, y_proj, z_proj])
        # orthogonal max-value projections
        x_projmax = hmap_path + "_xmax.jpeg"
        y_projmax = hmap_path + "_ymax.jpeg"
        z_projmax = hmap_path + "_zmax.jpeg"
        list_images_to_tif.extend([x_projmax, y_projmax, z_projmax])
        # orthogonal false color max value projections
        x_projglowmax = hmap_path + "_glow_xmax.jpeg"
        y_projglowmax = hmap_path + "_glow_ymax.jpeg"
        z_projglowmax = hmap_path + "_glow_zmax.jpeg"
        list_images_to_tif.extend([x_projglowmax, y_projglowmax, z_projglowmax])
        # orthogonal false color max value projections
        x_projstd = hmap_path + "_xstd.jpeg"
        y_projstd = hmap_path + "_ystd.jpeg"
        z_projstd = hmap_path + "_zstd.jpeg"
        list_images_to_tif.extend([x_projstd, y_projstd, z_projstd])
        # orthogonal false color std dev projections
        x_projglowstd = hmap_path + "_glow_xstd.jpeg"
        y_projglowstd = hmap_path + "_glow_ystd.jpeg"
        z_projglowstd = hmap_path + "_glow_zstd.jpeg"
        list_images_to_tif.extend([x_projglowstd, y_projglowstd, z_projglowstd])
        # central slices
        x_slicecentral = hmap_path + "_scaled_xcentral_slice.jpeg"
        y_slicecentral = hmap_path + "_scaled_ycentral_slice.jpeg"
        z_slicecentral = hmap_path + "_scaled_zcentral_slice.jpeg"
        list_images_to_tif.extend([x_slicecentral, y_slicecentral, z_slicecentral])
        # largest variance slices
        x_slicelargevar = hmap_path + "_xlargestvariance_slice.jpeg"
        y_slicelargevar = hmap_path + "_ylargestvariance_slice.jpeg"
        z_slicelargevar = hmap_path + "_zlargestvariance_slice.jpeg"
        list_images_to_tif.extend([x_slicelargevar, y_slicelargevar, z_slicelargevar])
        # surface views
        x_surface = hmap_path + "_scaled_xsurface.jpeg"
        y_surface = hmap_path + "_scaled_ysurface.jpeg"
        z_surface = hmap_path + "_scaled_zsurface.jpeg"
        list_images_to_tif.extend([x_surface, y_surface, z_surface])

    # loop through and convert
    for img in list_images_to_tif:
        if os.path.exists(img):
            convert_img_tifflzw(img)


if __name__ == "__main__":
    main()
