from pathlib import Path
import argparse
import xml.etree.ElementTree as ET
from pipeliner.scripts.job_scripts import download_map_model
import sys
import requests
from typing import List
from pipeliner.utils import run_subprocess


def get_arguments():
    parser = argparse.ArgumentParser(description="EMDB get associated models")

    parser.add_argument(
        "-i",
        "--id",
        help="The EMDB ID",
        nargs="?",
        required=True,
    )
    parser.add_argument(
        "-o",
        "--outdir",
        help="The job output directory",
        nargs="?",
        required=True,
    )

    parser.add_argument(
        "--get_seqs",
        action="store_true",
    )
    parser.add_argument("--do_cif", action="store_true")

    return parser


def get_pdb_asoc_address(pdbid: str, do_cif: bool) -> str:
    ext = "cif" if do_cif else "pdb"
    addy = f"https://files.rcsb.org/download/{pdbid.upper()}.{ext}.gz"
    return addy


def get_associated_models(dbid: str, odir: str, do_cif: bool) -> List[str]:
    xml = Path(odir) / f"emd-{dbid}.xml"
    xmlroot = ET.parse(xml).getroot()
    ap = "./crossreferences/pdb_list/pdb_reference"
    asocmod = xmlroot.findall(ap)
    print(f"There are {len(asocmod)} associated PDB entries for EMD-{dbid}")
    pdbids = []
    found = 0

    for index, mod in enumerate(asocmod, start=1):
        pdbid = mod.findtext("pdb_id")
        if not pdbid:
            print(f"Could not find PDB ID for model {index}")
            continue
        addy = get_pdb_asoc_address(pdbid, do_cif)
        print(f"Attempting to download from: {addy}")
        response = requests.head(addy, timeout=30.1)
        if response.status_code == 200:
            download_map_model.main([addy, "-P", odir])
            fname = Path(odir) / addy.split("/")[-1]
            run_subprocess(["gzip", "-d", fname])
            found += 1
            pdbids.append(pdbid)
    print(f"{found} associated models were downloaded")
    return pdbids


def get_sequences(dbid: str, odir: str):
    seq_addy = f"https://www.ebi.ac.uk/pdbe/entry/pdb/{dbid}/fasta"
    print(f"Attempting sequence download: {seq_addy}")
    download_map_model.main([seq_addy, "-P", odir])
    fasta_name = dbid.lower() + ".fasta"
    opath = Path(odir) / "fasta"
    opath.rename(Path(odir) / fasta_name)


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    pdb_ids = get_associated_models(args.id, args.outdir, args.do_cif)
    if args.get_seqs:
        for pdbid in pdb_ids:
            get_sequences(pdbid, args.outdir)


if __name__ == "__main__":
    main()
