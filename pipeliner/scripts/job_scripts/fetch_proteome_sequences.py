#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import argparse
import requests
import os
import pathlib
from typing import Union


def parse_args():
    parser = argparse.ArgumentParser(description="Fetch uniprot proteome")
    parser.add_argument(
        "-id",
        "--proteome_id",
        required=True,
        help="Input UNIPROT proteome ID (e.g. UP000005206)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )

    return parser.parse_args()


def fetch_uniprot_proteome(
    input_uniprot_id: str, outdir: Union[str, pathlib.Path, None] = None
):
    if outdir:
        fasta_fname = os.path.join(outdir, f"{input_uniprot_id}.fasta.gz")
    else:
        fasta_fname = f"{input_uniprot_id}.fasta.gz"
    url = (
        f"https://rest.uniprot.org/uniprotkb/stream?compressed="
        f"true&format=fasta&query=%28%28proteome%3A{input_uniprot_id}%29%29"
    )
    request = requests.get(url, stream=True, timeout=30.1)
    request.raise_for_status()
    with open(fasta_fname, "wb") as f:
        for chunk in request.iter_content(chunk_size=1024):
            f.write(chunk)


def main():
    args = parse_args()
    fetch_uniprot_proteome(args.proteome_id, outdir=args.odir)


if __name__ == "__main__":
    main()
