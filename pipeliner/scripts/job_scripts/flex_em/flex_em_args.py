#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import json
import argparse
import mrcfile

description = "Python tools for running Flex-EM from CCP-EM 1.x via CCP-EM Pipeliner"
parser = argparse.ArgumentParser(prog="flex_em_args", description="".join(description))
parser.add_argument(
    "-w",
    "--working_dir",
    default=None,
    help="working directory",
)
parser.add_argument(
    "-s",
    "--input_model",
    default=None,
    help="Input model (.pdb)",
)
parser.add_argument(
    "-m",
    "--input_map",
    default=None,
    help="Input map (.mrc)",
)
parser.add_argument(
    "-rb",
    "--ribfind_rigid_body_file",
    default=None,
    help="Ribfind rigid body file (.txt)",
)
parser.add_argument(
    "-d",
    "--density_weight",
    default=None,
    type=float,
    help="Density weight",
)
parser.add_argument(
    "-r",
    "--resolution",
    default=None,
    type=float,
    help="Resolution",
)
parser.add_argument(
    "-p", "--phi_psi", action="store_true", help="Use phi/psi restraints"
)


def get_args_dict(
    working_dir,
    input_map,
    input_model,
    ribfind_rigid_body_file,
    density_weight,
    resolution,
    phi_psi,
    apix,
    x,
    y,
    z,
):
    return {
        "code": "fit",
        "init_dir": 1,
        "density_weight": density_weight,
        "phi_psi_restraints": phi_psi,
        "max_atom_disp": 0.2,
        "job_location": os.path.abspath(working_dir),
        "test_mode": False,
        "apix": apix,
        "em_map_file": "../" + input_map,
        "map_format": "MRC",
        "max_distance": 5.0,
        "path": None,
        "keyword_json": None,
        "input_pdb_file": "../" + input_model,
        "num_of_iter": 2,
        "optimization": "MD",
        "x": x,
        "y": y,
        "z": z,
        "resolution": resolution,
        "rigid_filename": "../" + ribfind_rigid_body_file,
        "distance_restraints": False,
    }


def write_args_json_file(args_dict):
    """
    Takes list args dictionary and write json for Flex-EM
    """
    with open("flex_em_args.json", "w") as outfile:
        json.dump(args_dict, outfile)


def main():
    print("Flex-EM make args")
    args = parser.parse_args()
    # Get map data
    mrc = mrcfile.open(args.input_map)
    args_dict = get_args_dict(
        working_dir=args.working_dir,
        input_map=args.input_map,
        input_model=args.input_model,
        ribfind_rigid_body_file=args.ribfind_rigid_body_file,
        density_weight=args.density_weight,
        resolution=args.resolution,
        phi_psi=args.phi_psi,
        apix=mrc.voxel_size.z.item(),
        x=mrc.header.origin.x.item(),
        y=mrc.header.origin.y.item(),
        z=mrc.header.origin.z.item(),
    )
    write_args_json_file(args_dict=args_dict)


if __name__ == "__main__":
    main()
