import argparse
from gemmi import cif
import sys
import os
from glob import glob
from pipeliner.jobs.relion.import_job import RelionImportCoords


def get_arguments():
    parser = argparse.ArgumentParser(description="Match coordinate files to microraphs")

    parser.add_argument(
        "--mics",
        "-m",
        help="Micrographs file",
        nargs="?",
        metavar="Micrographs file",
        required=True,
    )
    parser.add_argument(
        "--coords",
        "-c",
        help="Coordinates search string",
        nargs="?",
        metavar="Coordinates search string",
        required=True,
    )
    parser.add_argument(
        "--outdir",
        "-o",
        help="Output directory",
        nargs="?",
        metavar="Output directory",
        required=True,
    )
    return parser


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    mics_list, coordsdir, search_string = RelionImportCoords.get_mics_coords_search(
        mics=args.mics,
        search_string=args.coords,
        output_dir=args.outdir,
    )
    output_star = cif.Document()
    output_star.add_new_block("coordinate_files")
    cf_block = output_star.find_block("coordinate_files")
    loop = cf_block.init_loop("_rln", ["MicrographName #1", "MicrographCoordinates #2"])
    coords_glob = coordsdir + "/*.*"
    coords = [os.path.basename(x) for x in glob(coords_glob)]
    found_mics = []
    found_cfiles = []
    for mic in mics_list:
        mic_shortname = os.path.basename(mic).split(".")[0]
        for cfile in coords:
            cfile_shortname = os.path.basename(cfile)
            if cfile_shortname.startswith(mic_shortname):
                loop.add_row([mic, os.path.join(coordsdir, cfile)])
                found_mics.append(mic)
                found_cfiles.append(cfile)
    outfile = os.path.join(args.outdir, "coordinates.star")
    output_star.write_file(outfile)

    if not len(found_cfiles):
        raise ValueError(
            f"None of {len(coords)} coordinate files were matched to Micrographs"
        )

    for mic in mics_list:
        if mic not in found_mics:
            sys.stderr.write(f"No associated coordinate file found for {mic}\n")
    for cfile in coords:
        if cfile not in found_cfiles:
            sys.stderr.write(f"No associated micrograph found for {cfile}\n")
    print(
        f"{len(found_mics)}/{len(mics_list)} micrographs were associated with "
        "coordinate files"
    )
    print(
        f"{len(found_cfiles)}/{len(coords)} coordinate files were associated with "
        "micrographs"
    )


if __name__ == "__main__":
    main()
