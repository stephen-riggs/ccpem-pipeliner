#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import argparse
import json
from typing import Optional


def parse_args():
    parser = argparse.ArgumentParser(description="get CheckMySequence results")
    parser.add_argument(
        "-j",
        "--jsonout",
        required=True,
        help="Input checkmyseq result (.json)",
    )
    parser.add_argument(
        "-odir",
        "--odir",
        required=False,
        default=None,
        help="Output directory",
    )
    parser.add_argument(
        "-id",
        "--id",
        required=False,
        default="checkmyseq_id",
        help="ID used to save results json file",
    )

    return parser.parse_args()


def get_checkseq_globresults(checkseq_scores, dict_checkseq_glob):
    if checkseq_scores["clean_report"] is True:
        dict_checkseq_glob["Summary"] = ["No issues detected, congratulations!"]

    for key in [
        "indexing_issues",
        "unidentified_chains",
        "register_shifts",
        "sequence_mismatches",
    ]:
        for polymer in checkseq_scores[key]:
            if polymer == "clean_report":
                continue
            for e in checkseq_scores[key][polymer]:
                chain_data = polymer + " chain " + e["chain_id_reference"]
                try:
                    dict_checkseq_glob[key.replace("_", " ").capitalize()].append(
                        chain_data
                    )
                except KeyError:
                    dict_checkseq_glob[key.replace("_", " ").capitalize()] = [
                        chain_data
                    ]


def get_checkseq_localresults(checkseq_scores, dict_chain_issues):
    if checkseq_scores["sequence_mismatches"]:
        for polymer in checkseq_scores["sequence_mismatches"]:
            polymer_fix = "nucleic-acid" if polymer == "na" else polymer
            checkseq_scores["sequence_mismatches"][polymer]
            for m in checkseq_scores["sequence_mismatches"][
                polymer
            ]:  # list of mismatches
                chain_id = m["chain_id_reference"]
                chain_id_polymer = f"{polymer_fix} chain {chain_id}"
                # alignment_string_lines = [l.strip() for l in m["alignment"]
                # .split("\n")]
                alignment_string = m["alignment"]
                # alignment_string = alignment_string.replace(" ", "\t")
                text_string = (
                    "*** Sequence mismatch: "
                    f"\n\n{polymer_fix} "
                    f"chain {chain_id} {m['resid_start_reference']}"
                    f":{m['resid_end_reference']} "
                    f"has sequence identity to reference:"
                    f" {m['seq2ref_si']:.2f}% [E-value: {m['evalue']}]"
                    f"\n\n {alignment_string}\n\n\n"
                )
                try:
                    dict_chain_issues[chain_id_polymer] += text_string
                except KeyError:
                    dict_chain_issues[chain_id_polymer] = text_string

    if checkseq_scores["indexing_issues"]:
        for polymer in checkseq_scores["indexing_issues"]:
            polymer_fix = "nucleic-acid" if polymer == "na" else polymer
            for n in checkseq_scores["indexing_issues"][polymer]:
                chain_id = n["chain_id_reference"]
                chain_id_polymer = f"{polymer_fix} chain {chain_id}"
                alignment_string = n["align_sto"]
                # alignment_string = alignment_string.replace(" ", "\t")
                text_string = (
                    "*** Indexing issue: "
                    f"\n\n{polymer_fix} "
                    f"chain fragment {n['resid_start_reference']}"
                    f":{n['resid_end_reference']} "
                    f"has a chain break at residue {n['break_idx']}"
                    f" and no residue indexing gap, check!"
                    f"\n\n {alignment_string}\n\n\n"
                )
                try:
                    dict_chain_issues[chain_id_polymer] += text_string
                except KeyError:
                    dict_chain_issues[chain_id_polymer] = text_string


def parse_checkseq_json(out_json: str, out_id: Optional[str] = None):
    dict_checkseq_glob: dict = {}
    dict_chain_issues: dict = {}
    dict_frag_plot = {}
    with open(out_json, "r") as j:
        checkseq_scores = json.load(j)

    get_checkseq_globresults(checkseq_scores, dict_checkseq_glob)

    get_checkseq_localresults(checkseq_scores, dict_chain_issues)

    if checkseq_scores["register_shifts"]:
        for polymer in checkseq_scores["register_shifts"]:
            polymer_fix = "nucleic-acid" if polymer == "na" else polymer
            checkseq_scores["register_shifts"][polymer]
            for m in checkseq_scores["register_shifts"][polymer]:  # list of mismatches
                chain_id = m["chain_id_reference"]
                chain_id_polymer = f"{polymer_fix} chain {chain_id}"
                text_string = (
                    "*** Register shift: "
                    f"\n\n{polymer_fix} "
                    f"chain fragment {m['resid_start_reference']}"
                    f":{m['resid_end_reference']} "
                    f"may be shifted by {-m['shift']}"
                    f" residue{'s' if abs(m['shift'])>1 else ''}"
                    f" [p-value: {10**-m['mlogpv']:.2e}]\n"
                    f"model seq {m['resid_start_reference']}"
                    f":{m['resid_end_reference']}\n  {m['model_seq']}\n"
                    f"new seq   {m['resid_start_new']}"
                    f":{m['resid_end_new']}\n  {m['new_seq']}\n\n\n"
                )
                try:
                    dict_chain_issues[chain_id_polymer] += text_string
                except KeyError:
                    dict_chain_issues[chain_id_polymer] = text_string
                try:
                    dict_checkseq_glob["register_shifts"].append(
                        {
                            chain_id_polymer: [
                                m["resid_start_reference"],
                                m["resid_end_reference"],
                            ]
                        }
                    )
                except KeyError:
                    dict_checkseq_glob["register_shifts"] = [
                        {
                            chain_id_polymer: [
                                m["resid_start_reference"],
                                m["resid_end_reference"],
                            ]
                        }
                    ]
    if checkseq_scores["raw_results"]:
        for polymer in checkseq_scores["raw_results"]:
            polymer_fix = "nucleic-acid" if polymer == "na" else polymer
            for chain_id in checkseq_scores["raw_results"][polymer]:
                list_x = []
                list_y = []
                for p in checkseq_scores["raw_results"][polymer][chain_id]:
                    list_x.append(str(p["s"]) + "-" + str(p["e"]))
                    list_y.append(p["mlogpv"])
                dict_frag_plot[f"{polymer_fix} chain {chain_id}"] = [list_x, list_y]

    if not out_id:
        out_id = "checkseq"
    chain_out = out_id + "_checkseq_chain.json"
    with open(chain_out, "w") as j:
        json.dump(dict_chain_issues, j)
    glob_out = out_id + "_checkseq_glob.json"
    with open(glob_out, "w") as j:
        json.dump(dict_checkseq_glob, j)
    frag_plot = out_id + "_checkseq_plot.json"
    with open(frag_plot, "w") as j:
        json.dump(dict_frag_plot, j)


def main():
    args = parse_args()
    # read input
    checkseq_outputfile = args.jsonout
    parse_checkseq_json(out_json=checkseq_outputfile, out_id=args.id)


if __name__ == "__main__":
    main()
