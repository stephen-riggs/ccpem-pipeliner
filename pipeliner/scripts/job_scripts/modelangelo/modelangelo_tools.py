#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import argparse
from pathlib import Path

keywords_file = "keywords.txt"
description = "Python tools for running ModelAngelo via CCP-EM Pipeliner"
parser = argparse.ArgumentParser(
    prog="modelangelo_setup", description="".join(description)
)
parser.add_argument(
    "-s",
    "--symlink_mrc_map",
    default=None,
    help="Create .mrc symlink to .map input as molrep does not accept .map ext",
)


def symlink_mrc_map(orig_file):
    """
    Create soft link for input .map to .mrc

    This is done as ModelAgnelo does not currently accept maps with .map
    extension as valid inputs.  File linked to .mrc to minimize space
    """
    # Link should be in current working directory which is the job directory
    # orig_file should already be a relative path from here to the actual file
    link_name = Path(orig_file).with_suffix(".mrc").name
    print(f"Making symlink from {link_name} to {orig_file}")
    Path(link_name).symlink_to(orig_file)


def main():
    print("ModelAngelo Tools")
    args = parser.parse_args()
    if args.symlink_mrc_map is not None:
        symlink_mrc_map(orig_file=args.symlink_mrc_map)
    print("ModelAngelo Tools finished")


if __name__ == "__main__":
    main()
