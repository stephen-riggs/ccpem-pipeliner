#
#     Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/

import os
import sys
from typing import Optional

from gemmi import cif
import glob
import argparse

from pipeliner.star_writer import write
from pipeliner.api.api_utils import validate_starfile


class MicrographStarfile(object):
    def __init__(
        self,
        mtf_star: str,
        original_pixel_size: float,
        pixel_size: float,
        voltage: float,
        c_30: float,
        micrograph_name_dir: str,
        micrograph_search_string: str = "image*.mrc",
        optics_group_name: Optional[str] = "opticsGroup1",
        amplitude_contrast: Optional[float] = 0.1,
        optics_group: Optional[int] = 1,
    ):
        # data optics variables
        self.optics_group_name = optics_group_name
        self.optics_group_optics = optics_group
        self.mtf_star = mtf_star
        self.original_pixel_size = original_pixel_size
        self.pixel_size = pixel_size
        self.voltage = voltage
        self.c_30 = c_30
        self.amplitude_contrast = amplitude_contrast

        # grab the micrograph filepaths (relative to project dir)
        self.micrograph_name_list = glob.glob(
            os.path.join(micrograph_name_dir, micrograph_search_string)
        )

        # now back to the filling with values which are by default not None
        # but instead are lists to be filled with a single parameter value which is set
        # by the argument
        self.optics_group = []
        i = 0
        while i < len(self.micrograph_name_list):
            self.optics_group.append(optics_group)
            i += 1

        self.parakeet_micrographs = cif.Document()
        self.make_data_optics_loop()
        self.make_data_micrographs_loop()

    def make_data_optics_loop(self) -> None:
        optics_loop = self.parakeet_micrographs.add_new_block("optics")
        columns = [
            "OpticsGroupName",
            "OpticsGroup",
            "MtfFileName",
            "MicrographPixelSize",
            "MicrographOriginalPixelSize",
            "Voltage",
            "SphericalAberration",
            "AmplitudeContrast",
        ]
        loop = optics_loop.init_loop("_rln", columns)
        row = list(
            (
                str(self.optics_group_name),
                str(self.optics_group_optics),
                str(self.mtf_star),
                str(self.pixel_size),
                str(self.original_pixel_size),
                str(self.voltage),
                str(self.c_30),
                str(self.amplitude_contrast),
            )
        )
        loop.add_row(row)
        return

    def make_data_micrographs_loop(self) -> None:
        micrographs_loop = self.parakeet_micrographs.add_new_block("micrographs")
        columns = [
            "MicrographName",
            "OpticsGroup",
        ]
        loop = micrographs_loop.init_loop("_rln", columns)
        for idx in range(len(self.micrograph_name_list)):
            row = list(
                (
                    str(self.micrograph_name_list[idx]),
                    str(self.optics_group[idx]),
                )
            )
            loop.add_row(row)
        return

    def write_micrographs(self, micrographs_filename: str = "micrographs.star") -> None:
        write(self.parakeet_micrographs, micrographs_filename)
        return


def setup_parser_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--mtf_star",
        help="Star file with MTF data for microscope.",
        type=str,
        default="relion/mtf_300kV.star",
    )

    parser.add_argument(
        "--original_pixel_size",
        help="Original pixel size of micrographs",
        type=float,
        default=1.0,
    )

    parser.add_argument(
        "--pixel_size",
        help="Pixel size of micrographs",
        type=float,
        default=1.0,
    )

    parser.add_argument(
        "--voltage",
        help="Voltage of microscope",
        type=float,
        default=300.0,
    )

    parser.add_argument(
        "--c_30",
        help="Spherical aberration of microscope",
        type=float,
        default=2.7,
    )

    parser.add_argument(
        "--micrograph_name_dir",
        help="Directory containing micrographs",
        type=str,
        default="Micrographs",
    )

    parser.add_argument(
        "--micrograph_search_string",
        help="String to use in search for micrographs. May contain wildcards.",
        type=str,
        default="*.mrc",
    )

    parser.add_argument(
        "--optics_group_name",
        help="Name of optics group for micrographs",
        type=str,
        default="opticsGroup1",
    )

    parser.add_argument(
        "--amplitude_contrast",
        help="Amplitude contrast of microscope",
        type=float,
        default="0.1",
    )

    parser.add_argument(
        "--micrograph_starfile_name",
        help="Micrographs starfile name",
        type=str,
        default="micrographs.star",
    )

    return parser


def parse_args(parser, args):
    parsed_args = parser.parse_intermixed_args(args)
    return parsed_args


def create_starfile(args):
    micrograph_starfile = MicrographStarfile(
        mtf_star=args.mtf_star,
        original_pixel_size=args.original_pixel_size,
        pixel_size=args.pixel_size,
        voltage=args.voltage,
        c_30=args.c_30,
        micrograph_name_dir=args.micrograph_name_dir,
        micrograph_search_string=args.micrograph_search_string,
        optics_group_name=args.optics_group_name,
        amplitude_contrast=args.amplitude_contrast,
    )
    micrograph_starfile.write_micrographs(
        micrographs_filename=args.micrograph_starfile_name
    )
    # check the starfile is valid
    validate_starfile(args.micrograph_starfile_name)


def main(raw_args):
    parser = setup_parser_args()
    args = parse_args(parser, raw_args)
    create_starfile(args)


if __name__ == "__main__":
    main(sys.argv[1:])
