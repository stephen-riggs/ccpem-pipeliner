#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import argparse
import sys
from pathlib import Path
from gemmi import cif
from typing import List, Optional

from pipeliner.mrc_image_tools import substack_mrcs
from pipeliner.starfile_handler import StarFile, read_relion_optimiser_starfile


def get_arguments() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Select classes")

    parser.add_argument(
        "--optimiser_file",
        "-m",
        help="optimiser file from RELION Class2D or Class3D job",
        nargs="?",
        metavar="optimiser file",
        required=True,
    )
    parser.add_argument(
        "--outdir",
        "-o",
        help="Output directory",
        nargs="?",
        metavar="Output directory",
        required=True,
    )
    parser.add_argument(
        "--classes",
        help="Classes to select",
        nargs="+",
        type=int,
        required=True,
    )
    parser.add_argument(
        "--recenter",
        action="store_true",
    )
    return parser


def make_stack_and_star(outdir: Path, opt_file: Path, classes: List[int]) -> None:
    # make stack
    model_file = Path(str(opt_file).replace("optimiser", "model"))
    output_name = Path(outdir) / "class_averages.mrcs"
    optdict = read_relion_optimiser_starfile(str(opt_file))
    intype = optdict["_rlnOutputRootName"].split("/")[0]
    if intype == "Class2D":
        stackfile_name = model_file.name.replace("model.star", "classes.mrcs")
        stackfile = model_file.parent / stackfile_name
        substack_mrcs(
            input_stack=str(stackfile),
            imgs=[x - 1 for x in classes],
            out=str(output_name),
        )

    # make starfile
    keep_lines, tags = [], []
    if intype == "Class2D":
        blockname, outname = "class_averages", "class_averages"
        model_data = StarFile(str(model_file)).loop_as_list(
            block="model_classes", headers=True
        )
        img_index = model_data[0].index("_rlnReferenceImage")
        for clvg in model_data[1:]:
            clnumber = int(clvg[img_index].split("@")[0])
            if clnumber in classes:
                keep_lines.append(clvg)
            tags = model_data[0]
    elif intype == "Class3D":
        blockname, outname = "3d_classes", "selected_classes"
        model_classes = StarFile(str(model_file)).loop_as_list(
            "model_classes", headers=True
        )
        img_index = model_classes[0].index("_rlnReferenceImage")
        for line in model_classes[1:]:
            for cn in classes:
                if f"class{cn:03d}" in line[img_index]:
                    keep_lines.append(line)
        tags = model_classes[0]
    else:
        raise ValueError("Selection must be from Class2D or Class3D job")

    outfile = cif.Document()
    block = outfile.add_new_block(blockname)
    loop = block.init_loop(prefix="", tags=tags)
    for line in keep_lines:
        loop.add_row(line)
    output_file = str(outdir / f"{outname}.star")
    outfile.write_file(output_file)


def main(in_args: Optional[List[str]] = None) -> None:
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)
    make_stack_and_star(
        Path(args.outdir),
        Path(args.optimiser_file),
        args.classes,
    )


if __name__ == "__main__":
    main()
