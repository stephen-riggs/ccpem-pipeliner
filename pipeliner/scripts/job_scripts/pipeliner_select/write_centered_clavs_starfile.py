#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import sys
from typing import List, Optional
import argparse
from gemmi import cif
from pathlib import Path

from pipeliner.starfile_handler import StarFile


def get_arguments() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument("--optimiser_file", type=str, required=True)
    parser.add_argument("--outdir", type=str, required=True)
    parser.add_argument("--classes", type=int, nargs="+", required=True)
    return parser


def main(in_args: Optional[List[str]] = None) -> None:
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    # get the new offsets
    with open(Path(args.outdir) / "run.out") as runlog:
        loglines = [x for x in runlog.readlines() if "Center of mass: x=" in x]
    raw_offsets = [x.split("Center of mass: x= ")[-1] for x in loglines]
    new_xoff, new_yoff = [], []
    for line in raw_offsets:
        line_split = line.split(" y= ")
        new_xoff.append(float(line_split[0]))
        new_yoff.append(float(line_split[1].split(" z= ")[0]))

    model_file = args.optimiser_file.replace("optimiser", "model")
    model_data = StarFile(model_file).loop_as_list(block="model_classes", headers=True)

    # have to do this in this more complex way to insure compatibility if the headers
    # are not always in the same order
    img_index = model_data[0].index("_rlnReferenceImage")
    xoff_index = model_data[0].index("_rlnClassPriorOffsetX")
    yoff_index = model_data[0].index("_rlnClassPriorOffsetY")
    keep_lines: List[List[str]] = []
    od = Path(args.outdir) / "class_averages_centered.mrcs"

    class_n = 1
    for clavg in model_data[1:]:
        clnumber = int(clavg[img_index].split("@")[0])
        n = 0
        if clnumber in args.classes:
            new_line = []
            new_img = f"{class_n:06d}@{od}"
            for item in clavg:
                if clavg.index(item) == img_index:
                    new_line.append(new_img)
                elif clavg.index(item) == xoff_index:
                    new_line.append(str(new_xoff[n]))
                elif clavg.index(item) == yoff_index:
                    new_line.append(str(new_yoff[n]))
                else:
                    new_line.append(item)
            n += 1
            class_n += 1
            keep_lines.append(new_line)

    outfile = cif.Document()
    block = outfile.add_new_block("class_averages")
    loop = block.init_loop(prefix="", tags=model_data[0])
    for n_line in keep_lines:
        loop.add_row(n_line)
    output_file = str(Path(args.outdir) / "class_averages_centered.star")
    outfile.write_file(output_file)


if __name__ == "__main__":
    main()
