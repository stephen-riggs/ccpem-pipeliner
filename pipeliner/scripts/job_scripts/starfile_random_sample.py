#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

from gemmi import cif
import argparse
import sys
from pipeliner.starfile_handler import DataStarFile
from pathlib import Path
import random
from typing import List


def get_arguments():
    parser = argparse.ArgumentParser(
        description="Take random selections from a STAR file"
    )

    parser.add_argument(
        "--input_file",
        help="The starfile to sample",
        nargs="?",
        metavar="File to sample ",
        required=True,
    )
    parser.add_argument(
        "--n_samples",
        help="Number of samples to take",
        nargs="?",
        metavar="number of samples ",
        required=True,
    )
    parser.add_argument(
        "--block",
        help="Which block to sample",
        nargs="?",
        metavar="block to sample ",
        required=True,
    )
    parser.add_argument(
        "--output",
        help="Output name",
        nargs="?",
        metavar="output name ",
        required=True,
    )

    return parser


def main(in_args=None) -> None:
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    data_star = DataStarFile(args.input_file)
    data = data_star.loop_as_list(args.block, headers=True)

    needs_optics = ["micrographs", "movies", "particles"]
    output = cif.Document()

    if args.block in needs_optics:
        optics_data: List[List[str]] = data_star.loop_as_list("optics", headers=True)
        if optics_data:
            optics_block = output.add_new_block("optics")
            optics_loop = optics_block.init_loop("", optics_data[0])
            for line in optics_data[1:]:
                optics_loop.add_row(line)
    try:
        sample = random.sample(data[1:], int(args.n_samples))
    except ValueError:
        sample = data[1:]
    main_block = output.add_new_block(args.block)
    data_loop = main_block.init_loop("", data[0])
    for item in sample:
        data_loop.add_row(item)

    Path(args.output).parent.mkdir(parents=True, exist_ok=True)
    output.write_file(args.output)


if __name__ == "__main__":
    main()
