#!/usr/bin/env python3

#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import json
from glob import glob
from typing import Dict, Union

from pipeliner.utils import get_pipeliner_root
from pipeliner.metadata_tools import (
    make_job_parameters_schema,
    make_job_results_schema,
)
from pipeliner.job_factory import get_job_types
from pipeliner.pipeliner_job import PipelinerJob
from pipeliner.utils import make_pretty_header
from pipeliner.data_structure import TRUES


def update_schema(
    job: PipelinerJob,
    test_schema: Dict[str, Union[str, Dict[str, Union[str, Dict[str, str]]]]],
    schema_file: str,
    s_type: str,
):
    try:
        with open(schema_file, "r") as schema_template:
            exp_schema = json.loads(schema_template.read())
    except FileNotFoundError:
        with open(schema_file, "w") as new_params_schema:
            json.dump(test_schema, new_params_schema, indent=4)
            return (
                f"No {s_type} schema found for {job.PROCESS_NAME}; "
                f"Wrote pipeliner{schema_file.replace(str(get_pipeliner_root()), '')}"
            )

    if test_schema != exp_schema:
        with open(schema_file, "w") as new_params_schema:
            json.dump(test_schema, new_params_schema, indent=4)
        return f"Updated {s_type} schema for {job.PROCESS_NAME}"


def check_existing_main_schema():
    dellist = []
    main_schema_dir = os.path.join(get_pipeliner_root(), "metadata_schema")
    main_schema = glob(f"{main_schema_dir}/*.json")
    job_names = [x.PROCESS_NAME.replace(".", "_") for x in get_job_types()]
    for sch in main_schema:
        scheme_name = os.path.basename(sch).replace(".json", "")
        if scheme_name not in job_names:
            dellist.append(sch)
    return dellist


def check_existing_params_schema():
    dellist = []
    main_schema_dir = os.path.join(get_pipeliner_root(), "metadata_schema/parameters")
    main_schema = glob(f"{main_schema_dir}/*.json")
    job_names = [x.PROCESS_NAME.replace(".", "_") for x in get_job_types()]
    for sch in main_schema:
        scheme_name = os.path.basename(sch).replace(".json", "")
        if scheme_name.replace("params_", "") not in job_names:
            dellist.append(sch)
    return dellist


def check_existing_results_schema():
    dellist = []
    main_schema_dir = os.path.join(get_pipeliner_root(), "metadata_schema/results")
    main_schema = glob(f"{main_schema_dir}/*.json")
    job_names = [x.PROCESS_NAME.replace(".", "_") for x in get_job_types()]
    for sch in main_schema:
        scheme_name = os.path.basename(sch).replace(".json", "")
        if scheme_name.replace("results_", "") not in job_names:
            dellist.append(sch)
    return dellist


def check_results_schema(job_type):
    results_dir = os.path.join(get_pipeliner_root(), "metadata_schema/results")
    rfile = os.path.join(results_dir, f"results_{job_type.replace('.', '_')}.json")
    if not os.path.isfile(rfile):
        empty_default = make_job_results_schema(job_type)
        with open(rfile, "w") as f:
            json.dump(empty_default, f, indent=4)
        return f"No default results schema for {job_type}; wrote an empty one"


def main() -> None:
    md_dir = os.path.join(get_pipeliner_root(), "metadata_schema")
    params_dir = os.path.join(md_dir, "parameters")
    made_changes = []

    for job in get_job_types():
        jname = job.PROCESS_NAME.replace(".", "_")

        # check for the main schema
        default_schema: Dict[str, Union[str, Dict[str, Union[str, Dict[str, str]]]]] = {
            "$schema": "http://json-schema.org/draft-04/schema#",
            "title": job.PROCESS_NAME,
            "description": f"{job.jobinfo.short_desc}",
            "type": "object",
            "properties": {
                "run parameters": {
                    "description": "Complete parameters used for running the job",
                    "$ref": f"parameters/params_{jname}.json",
                },
                "results": {
                    "description": "Data on the results of the job",
                    "$ref": f"results/results_{jname}.json",
                },
            },
        }
        schema_file = os.path.join(md_dir, f"{jname}.json")
        check = update_schema(job, default_schema, schema_file, "main")
        if check:
            made_changes.append(check)

        # check the params schema
        schema_file = os.path.join(
            get_pipeliner_root(),
            params_dir,
            f"params_{jname}.json",
        )
        params_schema = make_job_parameters_schema(job.PROCESS_NAME)
        check = update_schema(job, params_schema, schema_file, "parameters")
        if check:
            made_changes.append(check)

        check = check_results_schema(job.PROCESS_NAME)
        if check:
            made_changes.append(check)

    print(make_pretty_header("PIPELINER METADATA SCHEMA UPDATE"))
    if made_changes:
        for update in made_changes:
            print(f"- {update}")
    else:
        print("All existing schema are up to date")

    print("Checking for stale schema...")
    stale = check_existing_main_schema()
    stale.extend(check_existing_params_schema())
    stale.extend(check_existing_results_schema())

    if not stale:
        print("No stale schema found")
        return

    print("The following schema are stale; No associated job: ")
    for ss in stale:
        print(f"- {ss}")
    do_del = input("Delete these schema? (Y/N)")
    if do_del in TRUES:
        for ss in stale:
            os.remove(ss)
        print(f"Deleted {len(stale)} files")


if __name__ == "__main__":
    main()
