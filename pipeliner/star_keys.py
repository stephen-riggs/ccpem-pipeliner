#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

"""
Keys for field names in RELION pipeline STAR files
"""

# General
GENERAL_BLOCK = "pipeline_general"
JOB_COUNTER = "_rlnPipeLineJobCounter"

# Processes
PROCESS_BLOCK = "pipeline_processes"
PROCESS_PREFIX = "_rlnPipeLineProcess"
PROCESS_SUFFIXES = ["Name", "Alias", "TypeLabel", "StatusLabel"]
PROCESS_SUFFIXES_OLD = ["Name", "Alias", "Type", "Status"]

# Nodes
NODE_BLOCK = "pipeline_nodes"
NODE_PREFIX = "_rlnPipeLineNode"
NODE_SUFFIXES = ["Name", "TypeLabel"]
NODE_SUFFIXES_OLD = ["Name", "Type"]
# Input edges
INPUT_EDGE_BLOCK = "pipeline_input_edges"
INPUT_EDGE_PREFIX = "_rlnPipeLineEdge"
INPUT_EDGE_SUFFIXES = ["FromNode", "Process"]

# Output edges
OUTPUT_EDGE_BLOCK = "pipeline_output_edges"
OUTPUT_EDGE_PREFIX = "_rlnPipeLineEdge"
OUTPUT_EDGE_SUFFIXES = ["Process", "ToNode"]
