#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

"""
--------------------------
Starfile writing utilities
--------------------------

Writes star files in the same style as RELION
"""
import os
import string
import threading
from typing import IO, Union

from gemmi import cif

from pipeliner import __version__

RELION_VERSION_LINE = "# version 50001\n"
PIPELINER_VERSION_LINE = f"# CCP-EM Pipeliner version {__version__}\n"
FULL_VERSION_STR = f"\n{RELION_VERSION_LINE}{PIPELINER_VERSION_LINE}\n"


# Construct a set of characters that can be written to a STAR file without quoting.
# This is almost the same set used internally by gemmi.cif.quote, except that we allow
# underscores (and instead handle only initial underscores and reserved words below)
SAFE_UNQUOTED_CHARS = frozenset(
    string.ascii_letters + string.digits + string.punctuation
) - set("\"#$';[{}")


# Tuple of words that need quoting if they appear at the start of a STAR file value
RESERVED_WORDS = ("_", "data_", "loop_", "global_", "save_", "stop_")


def star_quote(val: str) -> str:
    """Quote a string value as needed for writing to a STAR file.

    If the value contains only characters that can be safely written without quoting,
    and does not begin with a reserved word of the STAR format, it is returned
    unchanged.

    Otherwise, the value  is passed to ``gemmi.cif.quote()`` for quoting, which handles
    all of the more complicated quoting logic that is sometimes required.
    """
    if (
        val
        and set(val).issubset(SAFE_UNQUOTED_CHARS)
        and not val.startswith(RESERVED_WORDS)
    ):
        return val
    else:
        return cif.quote(val)


def write(doc: cif.Document, filename: Union[str, "os.PathLike[str]"]):
    """Write a Gemmi CIF document to a RELION-style STAR file.

    The file is written as an atomic operation. This ensures that any processes reading
    it will always see a valid version (old or new) and not a half-written new file.

    The document is written to a temporary file first, then after the writing is
    complete and the file has been flushed to disk, the target file is replaced with the
    temporary one. The temporary file will always be removed even if the replacement is
    unsuccessful.

    Note: it is still possible for data to be lost using this function, if two processes
    try to write to the file at the same time. In that case, one of the new versions of
    the file will be kept and the other will not.

    Args:
        doc (:class:`gemmi.cif.Document`): The data to write out
        filename (str or pathlib.Path): The name of the file to write the data to
    """
    temp_filename = f"{filename}.tmp{threading.get_native_id()}"
    try:
        with open(temp_filename, "w") as temp_file:
            # Write the data to a temporary file and flush it to disk
            write_to_stream(doc, temp_file)
            temp_file.flush()
            os.fsync(temp_file.fileno())

        # Replace the target file with the temporary file
        os.replace(temp_filename, filename)
    finally:
        try:
            # Tidy up by removing the temporary file if it still exists
            os.remove(temp_filename)
        except FileNotFoundError:
            # Ignore the error if the temporary file does not exist anymore
            pass


def write_to_stream(doc: cif.Document, out_stream: IO[str]):
    """Write a Gemmi CIF document to an output stream using RELION's output style.

    Args:
        doc (:class:`gemmi.cif.Document`): The data to write out
        out_stream (str): The name of the file to write the data to
    """
    for block in doc:
        out_stream.write(FULL_VERSION_STR)
        out_stream.write("data_{}\n\n".format(block.name))
        for item in block:
            if item.pair:
                # Need cif.quote to handle strings with reserved words, chars etc
                # Need cif.as_string first to avoid double-quoting values read from an
                # existing STAR file
                out_stream.write(
                    f"{item.pair[0]}    {star_quote(cif.as_string(item.pair[1]))}\n"
                )
            elif item.loop:
                out_stream.write("loop_ \n")
                loop = item.loop
                for ii, tag in enumerate(loop.tags, start=1):
                    out_stream.write("{} #{} \n".format(tag, ii))
                for row in range(loop.length()):
                    for col in range(loop.width()):
                        value = loop[row, col]
                        # Annoyingly, RELION's output uses 12-char wide fields except
                        # if value is "None"
                        if value == "None":
                            out_stream.write("{:>10} ".format(value))
                        # Need to quote empty strings
                        # (can normally use gemmi.cif.quote() for this but not if we
                        # want strict RELION-style output)
                        elif value == "":
                            out_stream.write("{:>12} ".format('""'))
                        else:
                            out_stream.write(f"{star_quote(cif.as_string(value)):>12} ")
                    out_stream.write("\n")
            out_stream.write(" \n")


def write_jobstar(in_dict: dict, out_fn: Union[str, "os.PathLike[str]"]):
    """Write a job.star file from a dictionary of options

    Args:
        in_dict (dict): Dict of job option keys and values
        out_fn (str or :class:`~pathlib.Path`): Name of the file to write to
    """
    jobstar = cif.Document()

    # make the job block - don't know why I have to separate these
    # two functions but Gemmi seg faults if you don't....
    job_block = jobstar.add_new_block("job")

    for param in in_dict:
        val = in_dict[param]
        # get the three that go to the job block
        if param in ["_rlnJobTypeLabel", "_rlnJobIsContinue", "_rlnJobIsTomo"]:
            job_block.set_pair(param, val)

    # make the options block
    jobop_block = jobstar.add_new_block("joboptions_values")
    jobop_loop = jobop_block.init_loop(
        "_", ["rlnJobOptionVariable", "rlnJobOptionValue"]
    )

    for param in in_dict:
        val = in_dict[param]
        # add all the remaining job options
        if param not in ["_rlnJobTypeLabel", "_rlnJobIsContinue", "_rlnJobIsTomo"]:
            jobop_loop.add_row([param, str(val)])

    write(jobstar, out_fn)
