#!/usr/bin/env python3
#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import sys
import os
import argparse
import shutil
from math import log, sqrt
from time import sleep
import matplotlib.pyplot as plt
from numpy import polyfit
from pipeliner.starfile_handler import JobStar, DataStarFile
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.data_structure import (
    SUCCESS_FILE,
    FAIL_FILE,
    ABORT_FILE,
    RELION_SUCCESS_FILE,
    TRUES,
)


def get_arguments():

    parser = argparse.ArgumentParser(description="CCPEM Pipeliner bfactor Tool")

    parser.add_argument(
        "--postprocess_job",
        help="The postprocess job to test",
        nargs="?",
        metavar="mask mrc file",
        required=True,
    )

    parser.add_argument(
        "--start_at_pp",
        action="store_true",
        help=(
            "Start at the postprocessing step, this is for when the "
            "program terminated while waiting for queued refinement "
            "jobs to finish.  Assumes that all refinement jobs have been "
            "completed"
        ),
    )

    parser.add_argument(
        "--only_analysis",
        action="store_true",
        help=(
            "Just run the analysis step.  Use if the refinement and "
            "postprocessing steps have already been run"
        ),
    )

    parser.add_argument(
        "--angpix",
        default=1,
        dest="angpix",
        help="Pixel size for PostProcessing jobs",
    )

    parser.add_argument(
        "--min_particles",
        default=100,
        dest="min_particles",
        help="Minimum number of particles to calculate resolution for",
    )
    return parser


class BFactorPlotTool(object):
    def __init__(self, postprocess_job, only_analysis, angpix):
        self.postprocess_template = os.path.abspath(
            os.path.join(postprocess_job, "job.star")
        )

        # make sure things are good to run
        if os.path.isdir("pipeliner_bfactor") and not only_analysis:
            print(
                "BFACTOR: The bfactor tool has already been run on this project\n"
                "BFACTOR: To view the results of a completed run use the "
                "--only_analysis argument when running"
            )
            doit = input("BFACTOR: Erase previous results and continue? (Y/N) ")
            if doit.lower() in TRUES:
                shutil.rmtree("pipeliner_bfactor")
            else:
                sys.exit("BFACTOR: Goodbye")
        fails = []
        # check the jobs exist and have succeeded
        post_stardata = JobStar(self.postprocess_template).all_options_as_dict()
        self.refine_job = os.path.dirname(post_stardata["fn_in"]) + "/"
        self.refine_template = os.path.abspath(
            os.path.join(self.refine_job, "job.star")
        )

        for adir in [self.refine_job, postprocess_job]:
            if not os.path.isdir(adir):
                fails.append(f"{adir} not found")
            successfiles = [
                os.path.join(adir, x) for x in (RELION_SUCCESS_FILE, SUCCESS_FILE)
            ]
            if not any([os.path.isfile(x) for x in successfiles]):
                fails.append(f"{adir} does not appear to have finished successfully")

        # get the particles
        self.parts_data = os.path.abspath(
            os.path.join(self.refine_job, "run_data.star")
        )
        parts_star = DataStarFile(self.parts_data)
        self.parts_count = parts_star.count_block("particles")
        self.angpix = angpix

        # particles checks
        if not os.path.isfile(self.parts_data):
            fails.append(f"file {self.parts_data} not found")
        if not (self.parts_count > 0):
            fails.append(f"no particles found in {self.parts_data}")

        refine_jo = JobStar(self.refine_template).all_options_as_dict()
        mask = refine_jo["fn_mask"]
        self.refinement_mask = os.path.abspath(mask) if mask else None
        if self.refinement_mask is not None and not os.path.isfile(
            self.refinement_mask
        ):
            fails.append(f"Refinement mask file {self.refinement_mask} not found")

        # get the dirs the data reside in
        self.parts_dirs = {}
        parts_block = parts_star.get_block("particles")
        for part_img in parts_block.find_values("_rlnImageName"):
            part_dir = os.path.dirname(part_img.split("@")[-1])
            self.parts_dirs[part_dir] = os.path.abspath(part_dir)

        # get the reference
        self.ref_map = os.path.abspath(refine_jo["fn_ref"])
        if not os.path.isfile(self.ref_map):
            fails.append(f"Refinement reference map file {self.ref_map} not found")

        # check that the qsub variables in the file match current ones
        # give the user the option to modify them.
        orig_jo = JobStar(self.refine_template).all_options_as_dict()
        qsub_coms = {
            "qsub": "PIPELINER_QSUB_COMMAND",
            "qsubscript": "PIPELINER_QSUB_TEMPLATE",
            "queuename": "PIPELINER_QUEUE_NAME",
        }
        self.ref_params = {}
        for i in qsub_coms:
            e_com = os.environ.get(qsub_coms[i])
            if e_com and orig_jo[i] != e_com:
                print(
                    "BFACTOR: There is a mismatch between parameters set by your"
                    " environment and the parameters in the original jobs being "
                    f"analysed.\n\t Parameter:      {i} (${qsub_coms[i]})\n"
                    f"\t Original value: {orig_jo[i]}\n\t Environ value:  {e_com}"
                )
                useit = input(f"BFACTOR: Switch to the env version of {i}? (Y/N): ")
                if useit[0].lower() == "y":
                    self.ref_params[i] = e_com

        # decide if to run in parallel
        self.do_parallel = True if refine_jo["do_queue"].lower() in TRUES else False
        self.postprocess_mask = os.path.abspath(post_stardata["fn_mask"])
        if not os.path.isfile(self.postprocess_mask):
            fails.append(f"Postprocessing mask file {self.postprocess_mask} not found")

        self.parts_bins = []
        self.homedir = os.getcwd()
        self.running_jobs = set()

        if len(fails) != 0:
            print("BFACTOR: Cannot run for the following reasons!")
            for reason in fails:
                print(f"BFACTOR:\t{reason}")
            sys.exit()

        # screen output
        print(f"BFACTOR: Using {self.refine_job} with {self.parts_count} particles")
        print(f"BFACTOR: Refinement job template: {self.refine_template}")
        print(f"BFACTOR: Refinement reference: {self.ref_map}")
        if self.refinement_mask is not None:
            print(f"BFACTOR: Refinement mask: {self.refinement_mask}")
        print(f"BFACTOR: Postprocess job template: {self.postprocess_template}")
        print(f"BFACTOR: Postprocess mask: {self.postprocess_mask}")

    def prepare_part_sets(self):
        # make the dirs and data files
        print("BFACTOR: Preparing particle sets")
        for pb in self.parts_bins:
            # setup the directory and links to the files
            dirname = f"pipeliner_bfactor/bftmp_{pb:07d}"
            os.makedirs(dirname)
            os.chdir(dirname)
            os.symlink(self.parts_data, "parts_data.star")
            os.symlink(self.postprocess_mask, "postprocess_mask.mrc")
            os.symlink(self.ref_map, "refinement_ref.mrc")
            for part_dir in self.parts_dirs:
                subdir = "/".join(part_dir.split("/")[:-1])
                os.makedirs(subdir, exist_ok=True)
                os.symlink(
                    self.parts_dirs[part_dir], part_dir, target_is_directory=True
                )
            # setup the refinement params and make file

            self.ref_params.update(
                {
                    "fn_img": "Select/job001/particles_split1.star",
                    "fn_ref": "refinement_ref.mrc",
                }
            )
            if self.refinement_mask is not None:
                os.symlink(self.refinement_mask, "refine_mask.mrc")
                self.ref_params["fn_mask"] = "refine_mask.mrc"
            js = JobStar(self.refine_template)
            js.modify(self.ref_params)
            js.write("refine_job.star")
            # setup project and run the select job to make particle set
            proj = PipelinerProject(make_new_project=True)
            proj.run_job(
                {
                    "_rlnJobTypeLabel": "relion.select.split",
                    "fn_data": "parts_data.star",
                    "nr_split": 1,
                    "split_size": pb,
                    "do_random": "True",
                },
            )
            os.chdir(self.homedir)
        print(f"BFACTOR: {len(self.parts_bins)} particle sets created")

    def run_refine_jobs(self):
        proj = PipelinerProject()
        print("BFACTOR: Starting refinement jobs")
        if self.do_parallel:
            print(
                f"BFACTOR: Starting parallel execution {len(self.parts_bins)} of jobs"
            )
        else:
            print(
                f"BFACTOR: {len(self.parts_bins)} jobs will be run sequentially\n"
                "BFACTOR: This may take a LONG time\n"
            )

        for pb in self.parts_bins:
            dirname = f"pipeliner_bfactor/bftmp_{pb:07d}"
            os.chdir(dirname)
            proj = PipelinerProject()
            job = proj.run_job("refine_job.star")
            self.running_jobs.add(os.path.join(dirname, job.output_dir))
            os.chdir(self.homedir)

    def wait_for_jobs_to_finish(self):
        print("BFACTOR: Waiting for all jobs to finish")
        while len(self.running_jobs) != 0:
            test_set = list(self.running_jobs)
            for job in test_set:
                for statfile in [SUCCESS_FILE, FAIL_FILE, ABORT_FILE]:
                    if os.path.isfile(os.path.join(job, statfile)):
                        self.running_jobs.remove(job)
                        print(f"BFACTOR: {job} has finished")
            sleep(1)

    def run_pp_jobs(self):
        print("BFACTOR: Running postprocessing jobs")
        for pb in self.parts_bins:
            dirname = f"pipeliner_bfactor/bftmp_{pb:07d}"
            os.chdir(dirname)
            proj = PipelinerProject()
            job = proj.run_job(
                {
                    "_rlnJobTypeLabel": "relion.postprocess",
                    "fn_in": "Refine3D/job002/run_half1_class001_unfil.mrc",
                    "fn_mask": "postprocess_mask.mrc",
                    "angpix": self.angpix,
                }
            )
            self.running_jobs.add(os.path.join(dirname, job.output_dir))
            os.chdir(self.homedir)


def main(in_args=None):
    if in_args is None:
        in_args = sys.argv[1:]
    parser = get_arguments()
    args = parser.parse_args(in_args)

    # start session
    sesh = BFactorPlotTool(args.postprocess_job, args.only_analysis, args.angpix)

    # calculate the split numbers
    current_part_no = int(args.min_particles)
    while current_part_no <= sesh.parts_count:
        sesh.parts_bins.append(current_part_no)
        current_part_no = current_part_no * 2

    if not args.only_analysis:
        sesh.prepare_part_sets()
        if not args.start_at_pp:
            sesh.run_refine_jobs()
            sesh.wait_for_jobs_to_finish()
        sesh.run_pp_jobs()
        sesh.wait_for_jobs_to_finish()

    def get_pp_vals(pb):
        if isinstance(pb, int):
            pp_outfile = f"pipeliner_bfactor/bftmp_{pb:07d}/PostProcess/job003/run.out"
        else:
            pp_outfile = pb
        with open(pp_outfile, "r") as ppout:
            pp_lines = ppout.readlines()
        bfact, res, recp_res = None, None, None
        for line in pp_lines:
            linesplit = line.split()
            if linesplit[0:3] == ["+", "apply", "b-factor"]:
                bfact = float(linesplit[-1])
            if linesplit[0:3] == ["+", "FINAL", "RESOLUTION:"]:
                recp_res = 1.0 / float(linesplit[-1])
                res = float(linesplit[-1])
        return [recp_res, res, bfact]

    # get the postprocess stats
    bins_values = {}
    for pb in sesh.parts_bins:
        bins_values[pb] = get_pp_vals(pb)

    # get the stats from the full dataset
    full_pp = os.path.join(args.postprocess_job, "run.out")
    bins_values[sesh.parts_count] = get_pp_vals(full_pp)

    # parts for b-factor calculations
    gparts, gresos = [], []
    # screen display
    print("BFACTOR: FINAL results")
    print("BFACTOR:  Particles   1/Resolution   Resolution   b-factor")
    bv_keys = list(bins_values)
    bv_keys.sort()
    resos, the_parts = [], []
    for pb in bins_values:
        partcount = f"{pb:>9}"
        rslt = []
        if isinstance(bins_values[pb][1], float):
            resos.append(bins_values[pb][1])
            the_parts.append(pb)

        for v in bins_values[pb]:
            rslt.append(round(v, 3) if isinstance(v, float) else "n/a")

        print(
            f"BFACTOR:  {partcount:>8}   {rslt[0]:>12}   {rslt[1]:>10}   {rslt[2]:>8}"
        )
        if rslt[2] != "n/a":
            gparts.append(pb)
            gresos.append(rslt[1])
    resos.append(bins_values[sesh.parts_count][1])

    # add the actual model
    gparts.append(sesh.parts_count)
    gresos.append(bins_values[sesh.parts_count][1])
    # bfactor calculations

    log_all_parts = [log(x) for x in gparts]
    reso_sq = [1 / (x * x) for x in gresos]
    slope, intercept = polyfit(log_all_parts, reso_sq, 1)
    bfactor = 2.0 / slope

    print(
        f"BFACTOR:  Estimated b-factor from {sesh.parts_count} points is {bfactor:.2f}"
    )
    print(
        f"BFACTOR:  Estimated resolution = 1 / Sqrt(2 / "
        f"{bfactor:.3f} * ln(#Particles) + {intercept:.3f})\n"
        "BFACTOR:  IF this trend holds, you will get:"
    )

    pred_parts, pred_res = [], []
    for x in (1, 1.5, 2, 4, 8):
        cnp = int(sesh.parts_count * x)
        pred_parts.append(cnp)
        res = 1 / sqrt(slope * log(cnp) + intercept)
        pred_res.append(res)
        print(
            f"BFACTOR:    {res:.1f} Å from {cnp} particles ({x}x the current "
            "particle number)"
        )

    # make the plot
    fig, axs = plt.subplots(3)

    # make the plot
    axs[0].scatter(log_all_parts, reso_sq)
    ys = [slope * x + intercept for x in log_all_parts]
    axs[0].plot(log_all_parts, ys, color="orange")
    axs[0].set_xlabel("ln(particles)")
    axs[0].set_ylabel("1/resolution²")
    axs[0].set_title("b-factor fit")

    comb_pn = the_parts + [sesh.parts_count]
    axs[1].plot(pred_parts, pred_res, color="red", label="predicted")
    axs[1].plot(comb_pn, resos, label="actual")
    axs[1].set_ylabel("resolution (Å)")
    axs[1].set_xlabel("particle number")
    axs[1].set_title("resolution by particle number")
    axs[1].legend()

    axs[2].plot(pred_parts, pred_res, color="red")
    axs[2].hlines(
        resos[-1],
        min(pred_parts),
        max(pred_parts),
        color="green",
        linestyles="dashed",
        label=f"{resos[-1]} A",
    )
    axs[2].vlines(
        sesh.parts_count,
        min(pred_res),
        max(pred_res),
        color="orange",
        linestyles="dashed",
        label=f"{sesh.parts_count} particles",
    )
    axs[2].legend()
    axs[2].set_ylabel("resolution (Å)")
    axs[2].set_xlabel("particle number")
    axs[2].set_title("predicted resolutions")

    fig.tight_layout()
    fig.savefig("results_bfactor.png")
    print("BFACTOR: Output graphs written to: results_bfactor.png\nBFACTOR: Finished!")


if __name__ == "__main__":
    main()
