#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shutil
import tempfile
import unittest

from pipeliner.api import api_utils
from pipeliner.starfile_handler import JobStar
from pipeliner_tests import test_data


class APIUtilsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.test_data = os.path.dirname(test_data.__file__)
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def general_default_writing_test(self, jobtype):
        api_utils.write_default_jobstar(jobtype)

        jobtype_name = jobtype.replace(".", "_").lower()
        out_file = f"{jobtype_name}_job.star"
        exp_file = os.path.join(
            self.test_data,
            f"JobFiles/Default_JobStar/default_{jobtype_name}_job.star",
        )
        wrote = JobStar(out_file).all_options_as_dict()
        try:
            expected = JobStar(exp_file).all_options_as_dict()
        except FileNotFoundError:
            return f"File not found {exp_file}"

        special_lines = [
            "fn_motioncor2_exe",
            "fn_ctffind_exe",
            "fn_gctf_exe",
            "scratch_dir",
            "fn_topaz_exec",
            "fn_resmap",
            "queuename",
            "qsub",
            "qsubscript",
            "do_queue",
        ]
        for jobop in wrote:
            if None in [wrote.get(jobop), expected.get(jobop)]:
                return jobtype, jobop, wrote.get(jobop), expected.get(jobop)
            if wrote[jobop] != expected[jobop] and jobop not in special_lines:
                return jobtype, jobop, wrote[jobop], expected[jobop]
        for jobop in expected:
            if None in [wrote.get(jobop), expected.get(jobop)]:
                return jobtype, jobop, wrote.get(jobop), expected.get(jobop)
            if wrote[jobop] != expected[jobop] and jobop not in special_lines:
                return jobtype, jobop, wrote[jobop], expected[jobop]

        return None

    def test_default_jobstar_writing(self):
        errors = []
        for job in [
            "relion.import.movies",
            "servalcat.difference_map",
            "cryoef.map_analysis",
        ]:
            err = self.general_default_writing_test(job)
            if err:
                errors.append(err)
        if errors:
            for i in errors:
                print(i)
            raise AssertionError("Errors in written files")

    def test_default_jobstar_writing_relionstyle(self):
        jobstar_name = "class3d_job.star"
        api_utils.write_default_jobstar(
            "relion.class3d", out_fn=jobstar_name, relionstyle=True
        )
        options = JobStar(jobstar_name).all_options_as_dict()
        # Check a few options
        assert options["do_helix"] == "No"
        assert options["qsub"] == "qsub"
        assert options["nr_mpi"] == "1"

    def test_default_jobstar_writing_relionstyle_raises_error_for_bad_job_type(self):
        with self.assertRaises(ValueError):
            api_utils.write_default_jobstar("fake.fake.fake", relionstyle=True)

    def test_default_runjob_writing(self):
        errors = []
        for job in [
            "relion.import.movies",
            "servalcat.difference_map",
            "cryoef.map_analysis",
        ]:
            anyerr = self.general_default_writing_runjob(job)
            if anyerr:
                errors.append(anyerr)
        if errors:
            raise AssertionError("Errors in written files")

    def general_default_writing_runjob(self, jobtype):
        api_utils.write_default_runjob(jobtype)
        jobtype_name = jobtype.replace(".", "_").lower()
        out_file = "{}_run.job".format(jobtype_name)

        template_file = "JobFiles/Default_RunJob/{}_run.job".format(jobtype_name)

        expected_file = os.path.join(self.test_data, template_file)
        with open(expected_file, "r") as expected:
            expected_lines = [
                x.replace(" ", "").split("==") for x in expected.readlines()
            ]
        with open(out_file, "r") as written_file:
            lines = written_file.readlines()
        written_lines = [x.replace(" ", "").split("==") for x in lines]
        actout = "".join(lines)

        # these are lines that will be different based of the
        # environment vars so they are skipped for now
        special_lines = [
            "Copyparticlestoscratchdirectory:",
            "MOTIONCOR2executable:",
            "Gctfexecutable:",
            "Standardsubmissionscript:",
            "Submittoqueue?",
            "Queuename:",
            "",
            "Queuesubmitcommand:",
            "CTFFIND-4.1executable:",
            "ResMapexecutable:",
            "Topazexecutable:",
        ]

        for line in written_lines:
            if line[0] not in special_lines and line != "":
                if line not in expected_lines:
                    return actout
        for line in expected_lines:
            if line[0] not in special_lines and line != "":
                if line not in expected_lines:
                    return actout
        return None


if __name__ == "__main__":
    unittest.main()
