#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import sys
import json
from glob import glob
from typing import Optional
from pathlib import Path

from pipeliner_tests import test_data

from pipeliner_tests.testing_tools import (
    live_test,
    slow_test,
    make_conversion_file_structure,
    ShortpipeFileStructure,
    compare_starfiles,
    clean_starfile,
)
from pipeliner.utils import touch, get_pipeliner_root
from pipeliner.data_structure import (
    SUCCESS_FILE,
    FAIL_FILE,
    ABORT_TRIGGER,
    MOTIONCORR_OWN_NAME,
    CTFFIND_CTFFIND4_NAME,
    POSTPROCESS_JOB_NAME,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_ABORT,
    JOBSTATUS_RUN,
    JOBSTATUS_FAIL,
    JOBSTATUS_SCHED,
    PROJECT_FILE,
    CLEANUP_DIR,
    TRASH_DIR,
)
from pipeliner.starfile_handler import JobStar
from pipeliner.api import command_line as ccpem_pipeliner
from pipeliner.project_graph import ProjectGraph
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_PROCESSDATA,
    NODE_LOGFILE,
    NODE_MASK3D,
)
from pipeliner.job_manager import wait_for_job_to_finish


class MockArgs(object):
    def __init__(
        self, run_job: Optional[str] = None, continue_job: Optional[str] = None
    ):
        self.overwrite = None
        self.run_job = run_job
        self.continue_job = continue_job


class CommandLinePipelineTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.old_stdin = sys.stdin
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        sys.stdin = self.old_stdin

    @staticmethod
    def read_pipeline(pipeline_name="default"):
        """Return the contents of the pipline as a string, for comparisons"""
        return clean_starfile(f"{pipeline_name}_pipeline.star")

    @live_test(job="relion.postprocess")
    def run_postprocess_job(self, run_in_foreground=True):
        """copy in the necessary files and run a postprocess job"""

        # get the files
        assert not os.path.isfile("default_pipeline.star")

        halfmap_import_dir = "HalfMaps"
        os.makedirs(halfmap_import_dir)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Mask"
        os.makedirs(mask_import_dir)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        # intialize a new project
        ccpem_pipeliner.main(["--new_project"])

        # run the job
        jobfile = os.path.join(
            self.test_data, "JobFiles/PostProcess/postprocess_CL_job.star"
        )
        job = ccpem_pipeliner.f_run_job(MockArgs(jobfile))

        if run_in_foreground:
            # Wait for the job to finish and update the pipeline status
            assert wait_for_job_to_finish(job) == JOBSTATUS_SUCCESS
            with ProjectGraph(read_only=False) as pipeline:
                pipeline.check_process_completion()
        return job

    def schedule_job(self):
        """schedule a job to be used in later tests"""
        # get the files
        jobfile = os.path.join(
            self.test_data, "JobFiles/PostProcess/postprocess_CL_job.star"
        )

        halfmap_import_dir = "HalfMaps"
        os.makedirs(halfmap_import_dir, exist_ok=True)
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        shutil.copy(
            os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
            halfmap_import_dir,
        )
        mask_import_dir = "Mask"
        os.makedirs(mask_import_dir, exist_ok=True)
        shutil.copy(os.path.join(self.test_data, "emd_3488_mask.mrc"), mask_import_dir)

        # intialize a new project the first time
        if not os.path.isfile("default_pipeline.star"):
            ccpem_pipeliner.main(["--new_project"])

        # schedule the job
        ccpem_pipeliner.main(["--schedule_job", jobfile])

    def test_initialize_project(self):
        # there should not be a default_pipeline files
        assert not os.path.isfile("default_pipeline.star")

        # initialize a new project
        ccpem_pipeliner.main(["--new_project"])

        # check the pipeline is written as expected
        assert os.path.isfile("default_pipeline.star")
        lines = [
            ["data_pipeline_general"],
            ["_rlnPipeLineJobCounter", "1"],
        ]
        pipe_data = self.read_pipeline()
        for line in lines:
            assert line in pipe_data, line
        with open(PROJECT_FILE) as project_file:
            project_name = json.load(project_file)["project name"]
        assert project_name == "New project", project_name

    def test_initialize_project_nondefault_name(self):
        # there should not be a default_pipeline files
        assert not os.path.isfile("fancyfancy_pipeline.star")

        # initialize a new project
        ccpem_pipeliner.main(["--new_project", "fancyfancy"])

        # check the pipeline is written as expected
        assert os.path.isfile("fancyfancy_pipeline.star")
        lines = [
            ["data_pipeline_general"],
            ["_rlnPipeLineJobCounter", "1"],
        ]
        pipe_data = self.read_pipeline(pipeline_name="fancyfancy")
        for line in lines:
            if "# version" not in line:
                assert line in pipe_data, line

        with open(PROJECT_FILE) as project_file:
            proj_info = json.load(project_file)

        assert proj_info["pipeline file"] == "fancyfancy_pipeline.star"

    def test_initialize_project_name_error_illegal_symbol(self):
        """using an illegal symbol in pipeline name should raise error"""

        # initialize a new project with a bad name
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(["--new_project", "BAD!!"])

    def test_initialize_project_name_illegal_name(self):
        """calling th pipeline 'mini' should raise error"""

        # initialize a new project with a bad name
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(["--new_project", "mini"])

    def test_initializing_with_existing_project_fails(self):
        # make the directory already contain a pipeline files
        touch("default_pipeline.star")
        assert os.path.isfile("default_pipeline.star")

        # initializing a new project should should raise error
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(["--new_project"])

    @live_test(job="relion.postprocess")
    def test_run_job(self):
        # run a postprocess job
        self.run_postprocess_job()

        # check the expected files are produced
        pp_files = [
            SUCCESS_FILE,
            "logfile.pdf",
            "logfile.pdf.lst",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "default_pipeline.star",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
        ]
        for f in pp_files:
            assert os.path.isfile(os.path.join("PostProcess/job001", f)), f

        # check the expected lines are added to the pipeline
        pipe_lines = [
            ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SUCCESS],
            [
                "HalfMaps/3488_run_half1_class001_unfil.mrc",
                f"{NODE_DENSITYMAP}.mrc.halfmap",
            ],
            [
                "PostProcess/job001/postprocess.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job001/postprocess_masked.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job001/postprocess.star",
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job001/logfile.pdf",
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["Mask/emd_3488_mask.mrc", "PostProcess/job001/"],
            ["HalfMaps/3488_run_half1_class001_unfil.mrc", "PostProcess/job001/"],
            ["PostProcess/job001/", "PostProcess/job001/postprocess.mrc"],
            ["PostProcess/job001/", "PostProcess/job001/postprocess_masked.mrc"],
            ["PostProcess/job001/", "PostProcess/job001/postprocess.star"],
            ["PostProcess/job001/", "PostProcess/job001/logfile.pdf"],
            ["_rlnPipeLineJobCounter", "2"],
        ]

        pipe_data = self.read_pipeline()
        for line in pipe_lines:
            assert line in pipe_data, line

    @live_test(job="relion.postprocess")
    def test_abort_job(self):
        # start a postprocess job running
        job = self.run_postprocess_job(run_in_foreground=False)

        # abort the job
        ccpem_pipeliner.main(["--abort_job", "1"])

        # wait for the job to finish
        assert wait_for_job_to_finish(job) == JOBSTATUS_ABORT
        with ProjectGraph(read_only=False) as pipeline:
            pipeline.check_process_completion()

        # check the expected lines are added to the pipeline
        pipe_lines = [
            ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_ABORT],
        ]
        pipe_data = self.read_pipeline()
        for line in pipe_lines:
            assert line in pipe_data, line

    @live_test(job="relion.postprocess")
    def test_set_alias(self):
        # run the postprocess job to make the files
        self.run_postprocess_job()

        # change the alias
        ccpem_pipeliner.main(["--set_alias", "PostProcess/job001/", "new_alias"])

        # check the pipeline has been updated
        pipe_data = self.read_pipeline()
        new_ln = [
            "PostProcess/job001/",
            "PostProcess/new_alias/",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_SUCCESS,
        ]
        old_ln = [
            "PostProcess/job001/",
            "None",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_SUCCESS,
        ]
        assert new_ln in pipe_data, new_ln
        assert old_ln not in pipe_data, old_ln
        return new_ln, old_ln

    @live_test(job="relion.postprocess")
    def test_set_alias_and_clear(self):
        # run the previous test to make a job with an alias
        new_ln, old_ln = self.test_set_alias()

        # clear the alias
        ccpem_pipeliner.main(["--clear_alias", "PostProcess/job001/"])

        # check the pipeline has been updated
        pipe_data = self.read_pipeline()
        assert new_ln not in pipe_data, new_ln
        assert old_ln in pipe_data, old_ln

    @live_test(job="relion.postprocess")
    def test_set_status_to_failed_short_entry(self):
        # run the postprocess job to make the files
        self.run_postprocess_job()

        # make a directory like the job was actually run
        os.makedirs("PostProcess/job001", exist_ok=True)

        # change the status to failed
        ccpem_pipeliner.main(["--set_status", "PostProcess/job001/", "fail"])
        assert os.path.isfile("PostProcess/job001/" + FAIL_FILE)

    @live_test(job="relion.postprocess")
    def test_set_status_to_failed_bad_entry(self):
        self.run_postprocess_job()
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(["--set_status", "PostProcess/job001/", "not valid"])

    @live_test(job="relion.postprocess")
    def test_set_status_to_failed_ambiguous_entry(self):
        self.run_postprocess_job()
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(["--set_status", "PostProcess/job001/", "s"])

    @live_test(job="relion.postprocess")
    def test_set_status_to_failed_and_revert(self):
        # run the postprocess job to make the files
        self.run_postprocess_job()

        # make a directory like the job was actually run
        os.makedirs("PostProcess/job001", exist_ok=True)

        # change the status to failed
        ccpem_pipeliner.main(["--set_status", "PostProcess/job001/", "failed"])
        assert os.path.isfile("PostProcess/job001/" + FAIL_FILE)

        # check the pipeline has been updated
        pipe_data = self.read_pipeline()
        fail_line = [
            "PostProcess/job001/",
            "None",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_FAIL,
        ]
        assert fail_line in pipe_data

        # change the status back to finished
        ccpem_pipeliner.main(["--set_status", "PostProcess/job001/", "success"])
        assert os.path.isfile("PostProcess/job001/" + SUCCESS_FILE)
        assert not os.path.isfile("PostProcess/job001/" + FAIL_FILE)

        # check the pipeline has been updated
        fin_line = [
            "PostProcess/job001/",
            "None",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_SUCCESS,
        ]
        pipe_data = self.read_pipeline()
        assert fin_line in pipe_data

    def test_set_status_from_running_to_aborted_raises_error(self):
        # run the postprocess job to make the files
        self.run_postprocess_job()
        # make a directory like the job was actually run
        os.makedirs("PostProcess/job001", exist_ok=True)

        # change the status to failed
        ccpem_pipeliner.main(["--set_status", "PostProcess/job001/", "Running"])

        # check the pipeline has been updated
        pipe_data = self.read_pipeline()
        run_line = [
            "PostProcess/job001/",
            "None",
            POSTPROCESS_JOB_NAME,
            JOBSTATUS_RUN,
        ]
        assert run_line in pipe_data

        # set the status to aborted
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(["--set_status", "PostProcess/job001/", "aborted"])

    def test_deleting_job(self):
        # make the necessary files
        ShortpipeFileStructure(["Import", "MotionCorr", "CtfFind"])
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_3jobs_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        # count the files
        mocorr_file_count = len(os.listdir("MotionCorr/job002"))
        mocorr_rawdata_file_count = len(os.listdir("MotionCorr/job002/Raw_data"))
        ctffind_file_count = len(os.listdir("CtfFind/job003"))

        # make sure the expected lines are in the pipeline
        pipe_data = self.read_pipeline()
        inc_lines = [
            ["MotionCorr/job002/", "None", MOTIONCORR_OWN_NAME, JOBSTATUS_SUCCESS],
            ["CtfFind/job003/", "None", CTFFIND_CTFFIND4_NAME, JOBSTATUS_SUCCESS],
            [
                "MotionCorr/job002/corrected_micrographs.star",
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.motioncorr",
            ],
            ["MotionCorr/job002/logfile.pdf", f"{NODE_LOGFILE}.pdf.relion.motioncorr"],
            [
                "CtfFind/job003/micrographs_ctf.star",
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.ctf",
            ],
            ["CtfFind/job003/logfile.pdf", f"{NODE_LOGFILE}.pdf.relion.ctffind"],
            ["MotionCorr/job002/corrected_micrographs.star", "CtfFind/job003/"],
            ["MotionCorr/job002/", "MotionCorr/job002/corrected_micrographs.star"],
            ["MotionCorr/job002/", "MotionCorr/job002/logfile.pdf"],
            ["CtfFind/job003/", "CtfFind/job003/micrographs_ctf.star"],
            ["CtfFind/job003/", "CtfFind/job003/logfile.pdf"],
        ]
        for line in inc_lines:
            assert line in pipe_data, line

        # delete the MotionCorr job, child CtfFind job should go with it
        ccpem_pipeliner.main(["--delete_job", "MotionCorr/job002/"])

        # check the lines are gone from the pipeline
        pipe_data = self.read_pipeline()
        for line in inc_lines:
            assert line not in pipe_data, line

        # make sure the files have been moved to the trash
        mocorr_trash_count = len(os.listdir(f"{TRASH_DIR}/MotionCorr/job002"))
        mocorr_rawdata_trash_count = len(
            os.listdir(f"{TRASH_DIR}/MotionCorr/job002/Raw_data")
        )
        ctffind_trash_count = len(os.listdir(f"{TRASH_DIR}/CtfFind/job003"))
        assert mocorr_trash_count == mocorr_file_count
        assert mocorr_rawdata_trash_count == mocorr_rawdata_file_count
        assert ctffind_trash_count == ctffind_file_count

        # the job directories shoudl be gone
        assert not os.path.isdir("MotionCorr/job002")
        assert not os.path.isdir("CtfFind/job003")

    def test_delete_then_undelete(self):
        # make all of the necessary files
        ShortpipeFileStructure(["Import", "MotionCorr", "CtfFind"])
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_3jobs_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/minipipe_mocorr_pipeline.star"),
            os.path.join(self.test_dir, "MotionCorr/job002/job_pipeline.star"),
        )
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/minipipe_ctffind_pipeline.star"),
            os.path.join(self.test_dir, "CtfFind/job003/job_pipeline.star"),
        )

        # count the files
        mocorr_file_count = len(os.listdir("MotionCorr/job002"))
        mocorr_rawdata_file_count = len(os.listdir("MotionCorr/job002/Raw_data"))
        ctffind_file_count = len(os.listdir("CtfFind/job003"))

        # read the default pipeline and check that the expected lines are there
        pipe_data = self.read_pipeline()
        inc_lines = [
            ["MotionCorr/job002/", "None", MOTIONCORR_OWN_NAME, JOBSTATUS_SUCCESS],
            ["CtfFind/job003/", "None", CTFFIND_CTFFIND4_NAME, JOBSTATUS_SUCCESS],
            [
                "MotionCorr/job002/corrected_micrographs.star",
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.motioncorr",
            ],
            ["MotionCorr/job002/logfile.pdf", f"{NODE_LOGFILE}.pdf.relion.motioncorr"],
            [
                "CtfFind/job003/micrographs_ctf.star",
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.ctf",
            ],
            ["CtfFind/job003/logfile.pdf", f"{NODE_LOGFILE}.pdf.relion.ctffind"],
            ["MotionCorr/job002/corrected_micrographs.star", "CtfFind/job003/"],
            ["MotionCorr/job002/", "MotionCorr/job002/corrected_micrographs.star"],
            ["MotionCorr/job002/", "MotionCorr/job002/logfile.pdf"],
            ["CtfFind/job003/", "CtfFind/job003/micrographs_ctf.star"],
            ["CtfFind/job003/", "CtfFind/job003/logfile.pdf"],
        ]

        for line in inc_lines:
            assert line in pipe_data, [line]
        # delete the mocorr job along with 1 child
        ccpem_pipeliner.main(["--delete_job", "MotionCorr/job002/"])

        # make sure the files have been moved to the trash
        assert not os.path.isdir("MotionCorr/job002")
        assert not os.path.isdir("CtfFind/job003")
        mocorr_trash_count = len(os.listdir(f"{TRASH_DIR}/MotionCorr/job002"))
        mocorr_rawdata_trash_count = len(
            os.listdir(f"{TRASH_DIR}/MotionCorr/job002/Raw_data")
        )
        ctffind_trash_count = len(os.listdir(f"{TRASH_DIR}/CtfFind/job003"))
        assert mocorr_trash_count == mocorr_file_count
        assert mocorr_rawdata_trash_count == mocorr_rawdata_file_count
        assert ctffind_trash_count == ctffind_file_count

        # reread the pipeline and make sure the lines are gone
        pipe_data = self.read_pipeline()
        for line in inc_lines:
            assert line not in pipe_data, line

        # undelete the ctffind job - which should include 1 parent
        ccpem_pipeliner.main(["--undelete_job", "CtfFind/job003/"])

        # check that the lines are back in the default pipeline
        pipe_data = self.read_pipeline()
        for line in inc_lines:
            assert line in pipe_data, line

        # count the files - make sure they have been moved back from the trash...
        assert not os.path.isdir(f"{TRASH_DIR}/MotionCorr/job002")
        assert not os.path.isdir(f"{TRASH_DIR}/CtfFind/job003")

        # ...and are back in the right place
        mocorr_file_count2 = len(os.listdir("MotionCorr/job002"))
        mocorr_rawdata_file_count2 = len(os.listdir("MotionCorr/job002/Raw_data"))
        ctffind_file_count2 = len(os.listdir("CtfFind/job003"))

        assert mocorr_file_count2 == mocorr_file_count
        assert mocorr_rawdata_file_count2 == mocorr_rawdata_file_count
        assert ctffind_file_count2 == ctffind_file_count

    @live_test(job="relion.postprocess")
    def test_schedule_job(self):
        job = "001"
        self.schedule_job()
        # check the expected lines are in the pipeline
        expected_lines = [
            [
                "PostProcess/job{}/".format(job),
                "None",
                POSTPROCESS_JOB_NAME,
                "Scheduled",
            ],
            [
                "Mask/emd_3488_mask.mrc",
                f"{NODE_MASK3D}.mrc",
            ],
            [
                "HalfMaps/3488_run_half1_class001_unfil.mrc",
                f"{NODE_DENSITYMAP}.mrc.halfmap",
            ],
            [
                "PostProcess/job{}/postprocess.mrc".format(job),
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess",
            ],
            [
                "PostProcess/job{}/postprocess_masked.mrc".format(job),
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked",
            ],
            [
                "PostProcess/job{}/postprocess.star".format(job),
                f"{NODE_PROCESSDATA}.star.relion.postprocess",
            ],
            [
                "PostProcess/job{}/logfile.pdf".format(job),
                f"{NODE_LOGFILE}.pdf.relion.postprocess",
            ],
            ["Mask/emd_3488_mask.mrc", "PostProcess/job{}/".format(job)],
            [
                "HalfMaps/3488_run_half1_class001_unfil.mrc",
                "PostProcess/job{}/".format(job),
            ],
            [
                "PostProcess/job{0}/".format(job),
                "PostProcess/job{0}/postprocess.mrc".format(job),
            ],
            [
                "PostProcess/job{0}/".format(job),
                "PostProcess/job{0}/postprocess_masked.mrc".format(job),
            ],
            [
                "PostProcess/job{0}/".format(job),
                "PostProcess/job{0}/postprocess.star".format(job),
            ],
            [
                "PostProcess/job{0}/".format(job),
                "PostProcess/job{0}/logfile.pdf".format(job),
            ],
        ]

        pipe_data = self.read_pipeline()
        for line in expected_lines:
            assert line in pipe_data, line

        # check that the .Nodes files have been made
        expected_files = [
            ".Nodes/{}/PostProcess/job{}/postprocess.mrc".format(
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess".split(".")[0], job
            ),
            ".Nodes/{}/PostProcess/job{}/postprocess_masked.mrc".format(
                f"{NODE_DENSITYMAP}.mrc.relion.postprocess.masked".split(".")[0], job
            ),
            ".Nodes/{}/PostProcess/job{}/logfile.pdf".format(
                f"{NODE_LOGFILE}.pdf.relion.postprocess".split(".")[0], job
            ),
            ".Nodes/{}/PostProcess/job{}/postprocess.star".format(
                f"{NODE_PROCESSDATA}.star.relion.postprocess".split(".")[0],
                job,
            ),
        ]

        for f in expected_files:
            assert os.path.isfile(f), f

    @live_test(job="relion.postprocess")
    def test_running_schedule(self):
        """Make a schedule with two jobs and run it 3 times"""

        # schedule two postprocess jobs
        self.schedule_job()
        self.schedule_job()

        # run the schedule 3 times
        ccpem_pipeliner.main(
            [
                "--run_schedule",
                "--name",
                "schedule1",
                "--jobs",
                "PostProcess/job001/",
                "PostProcess/job002/",
                "--nr_repeats",
                "3",
                "--min_between",
                "0",
                "--wait_min_before",
                "0",
                "--wait_sec_after",
                "1",
            ]
        )

        # check the expected files are produced
        pp_files = [
            SUCCESS_FILE,
            "logfile.pdf",
            "logfile.pdf.lst",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "default_pipeline.star",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
        ]
        for f in pp_files:
            for job in ["001", "002"]:
                assert os.path.isfile(os.path.join("PostProcess/job" + job, f)), f

        # check the jobs ran 3x and schedule log was written properly
        with open("pipeline_schedule1.log") as logfile:
            log_data = logfile.readlines()
        job001_count = 0
        job002_count = 0
        for line in log_data:
            if "---- Executing PostProcess/job001/" in line:
                job001_count += 1
            if "---- Executing PostProcess/job002/" in line:
                job002_count += 1
        assert job001_count == 3
        assert job002_count == 3

        # TODO: check the mini_pipeline is as expected

        # TODO: Add test the 2nd and 3rd runs were continues

    @live_test(job="relion.postprocess")
    def test_running_schedule_fails_when_schedule_lock_present(self):
        """Make a schedule with two jobs but it fails because the schedule lock
        file is present"""

        # schedule two postprocess jobs
        self.schedule_job()
        self.schedule_job()
        touch("RUNNING_PIPELINER_default_schedule1")
        # run the schedule 3 times
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(
                [
                    "--run_schedule",
                    "--name",
                    "schedule1",
                    "--jobs",
                    "PostProcess/job001/",
                    "PostProcess/job002/",
                    "--nr_repeats",
                    "3",
                    "--min_between",
                    "0",
                    "--wait_min_before",
                    "0",
                    "--wait_sec_after",
                    "1",
                ]
            )

    def test_cleanup_single_job(self):
        # create the files
        procname = "Class3D"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles
        files = glob("Class3D/job009/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )

        # list of files that should be deleted
        del_files = glob("Class3D/job009/run_it*")
        for f in [
            "Class3D/job009/run_it025_class001.mrc",
            "Class3D/job009/run_it025_class001_angdist.bild",
            "Class3D/job009/run_it025_class002.mrc",
            "Class3D/job009/run_it025_class002_angdist.bild",
            "Class3D/job009/run_it025_class003.mrc",
            "Class3D/job009/run_it025_class003_angdist.bild",
            "Class3D/job009/run_it025_sampling.star",
            "Class3D/job009/run_it025_data.star",
            "Class3D/job009/run_it025_model.star",
            "Class3D/job009/run_it025_optimiser.star",
        ]:
            del_files.remove(f)

        # do the cleanup
        ccpem_pipeliner.main(["--cleanup", "Class3D/job009/"])

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        # check they are all in the right place
        files = glob("Class3D/job009/*")
        trash = glob(f"{CLEANUP_DIR}/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert f"{CLEANUP_DIR}/{f}" in trash

    def test_restore_cleanup_single_job(self):
        # create the files
        procname = "Class3D"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles
        files = glob("Class3D/job009/*")
        assert len(files) == len(outfiles[procname]), (
            len(files),
            len(outfiles[procname]),
        )

        # list of files that should be deleted
        del_files = glob("Class3D/job009/run_it*")
        for f in [
            "Class3D/job009/run_it025_class001.mrc",
            "Class3D/job009/run_it025_class001_angdist.bild",
            "Class3D/job009/run_it025_class002.mrc",
            "Class3D/job009/run_it025_class002_angdist.bild",
            "Class3D/job009/run_it025_class003.mrc",
            "Class3D/job009/run_it025_class003_angdist.bild",
            "Class3D/job009/run_it025_sampling.star",
            "Class3D/job009/run_it025_data.star",
            "Class3D/job009/run_it025_model.star",
            "Class3D/job009/run_it025_optimiser.star",
        ]:
            del_files.remove(f)

        cleaned = Path(CLEANUP_DIR) / "Class3D/job009/"
        cleaned.mkdir(parents=True)
        for f in del_files:
            Path(f).rename(Path(CLEANUP_DIR) / f)

        # do the restore
        ccpem_pipeliner.main(["--restore_cleaned_files", "Class3D/job009/"])

        # check they are all in the right place
        for f in del_files:
            assert Path(f).is_file()
            assert not Path(f"{CLEANUP_DIR}/{f}").is_file()
            assert not cleaned.is_dir()

    @slow_test
    def test_cleanup_multiple_jobs(self):
        # create the files
        procnames = ["Class3D", "InitialModel"]
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        outfiles = ShortpipeFileStructure(procnames).outfiles

        # list of files that should be deleted
        df1 = "Class3D/job009/run_it*"
        df2 = "InitialModel/job008/run_it*"
        del_files = glob(df1) + glob(df2)

        for f in [
            "Class3D/job009/run_it025_class001.mrc",
            "Class3D/job009/run_it025_class001_angdist.bild",
            "Class3D/job009/run_it025_class002.mrc",
            "Class3D/job009/run_it025_class002_angdist.bild",
            "Class3D/job009/run_it025_class003.mrc",
            "Class3D/job009/run_it025_class003_angdist.bild",
            "Class3D/job009/run_it025_sampling.star",
            "Class3D/job009/run_it025_data.star",
            "Class3D/job009/run_it025_model.star",
            "Class3D/job009/run_it025_optimiser.star",
            "InitialModel/job008/run_it150_data.star",
            "InitialModel/job008/run_it150_class001_data.star",
            "InitialModel/job008/run_it150_class002_data.star",
            "InitialModel/job008/run_it150_sampling.star",
            "InitialModel/job008/run_it150_model.star",
            "InitialModel/job008/run_it150_optimiser.star",
            "InitialModel/job008/run_it150_class001.mrc",
            "InitialModel/job008/run_it150_grad001.mrc",
            "InitialModel/job008/run_it150_class002.mrc",
            "InitialModel/job008/run_it150_grad002.mrc",
        ]:
            del_files.remove(f)

        # do the cleanup
        ccpem_pipeliner.main(["--cleanup", "Class3D/job009/", "InitialModel/job008/"])

        # sort the files
        removed, kept = [], []
        for procname in procnames:
            for f in outfiles[procname]:
                if f in del_files:
                    removed.append(f)
                else:
                    kept.append(f)

        # check they are all in the right place
        files = glob("Class3D/job009/*") + glob("InitialModel/job008/*")
        trash = glob(f"{CLEANUP_DIR}/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert f"{CLEANUP_DIR}/{f}" in trash

    def test_cleanup_single_job_harsh(self):
        procname = "Polish"
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        outfiles = ShortpipeFileStructure([procname]).outfiles

        files = glob("Polish/job014/**/*", recursive=True)

        assert len(files) == len(outfiles[procname]) + 1, (
            len(files),
            len(outfiles[procname]) + 1,
        )

        del_files = []
        for ext in [
            "*_FCC_cc.mrc",
            "*_FCC_w0.mrc",
            "*_FCC_w1.mrc",
            "*shiny.star",
            "*shiny.mrcs",
        ]:
            del_files += glob("Polish/job014/Raw_data/" + ext)

        # do the cleanup
        ccpem_pipeliner.main(["--cleanup", "Polish/job014/", "--harsh"])

        # sort the files
        removed, kept = [], []
        for f in outfiles[procname]:
            if f in del_files:
                removed.append(f)
            else:
                kept.append(f)

        # check they are all in the right place
        files = glob("Polish/job014/**/*", recursive=True)
        trash = glob(f"{CLEANUP_DIR}/**/*", recursive=True)
        for f in kept:
            assert f in files, f
        for f in removed:
            assert f not in files, f
            assert f"{CLEANUP_DIR}/{f}" in trash

    @slow_test
    def test_cleanup_alljobs(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        ShortpipeFileStructure(["all"])

        del_files = {
            "Import": [],
            "MotionCorr": [
                "/job002/Raw_data/*.com",
                "/job002/Raw_data/*.err",
                "/job002/Raw_data/*.out",
                "/job002/Raw_data/*.log",
            ],
            "CtfFind": [
                "/job003/gctf*.out",
                "/job003/gctf*.err",
            ],
            "AutoPick": ["/job004/Raw_data/*.spi"],
            "Extract": ["/job005/Raw_data/*_extract.star"],
            "Class2D": ["/job006/run_it*"],
            "Select": [],
            "InitialModel": ["/job008/run_it*"],
            "Class3D": ["/job009/run_it*"],
            "Refine3D": ["/job010/run_it*"],
            "MultiBody": ["/job011/run_it*"],
            "CtfRefine": [
                "/job012/Raw_data/*_wAcc.mrc",
                "/job012/Raw_data/*_xyAcc_real.mrc",
                "/job012/Raw_data/*_xyAcc_imag.mrc",
            ],
            "MaskCreate": [],
            "Polish": [
                "/job014/Raw_data/*_FCC_cc.mrc",
                "/job014/Raw_data/*_FCC_w0.mrc",
                "/job014/Raw_data/*_FCC_w1.mrc",
            ],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "PostProcess": ["/job017/*masked.mrc"],
            "LocalRes": [],
        }

        exclude_files = {
            "Import": [],
            "MotionCorr": [],
            "CtfFind": [],
            "AutoPick": [],
            "Extract": [],
            "Class2d": [
                "Class2D/job006/run_it025_data.star",
                "Class2D/job006/run_it025_sampling.star",
                "Class2D/job006/run_it025_model.star",
                "Class2D/job006/run_it025_optimiser.star",
                "Class2D/job006/run_it025_classes.mrcs",
            ],
            "Select": [],
            "InitialModel": [
                "InitialModel/job008/run_it150_data.star",
                "InitialModel/job008/run_it150_class001_data.star",
                "InitialModel/job008/run_it150_class002_data.star",
                "InitialModel/job008/run_it150_sampling.star",
                "InitialModel/job008/run_it150_model.star",
                "InitialModel/job008/run_it150_optimiser.star",
                "InitialModel/job008/run_it150_class001.mrc",
                "InitialModel/job008/run_it150_grad001.mrc",
                "InitialModel/job008/run_it150_class002.mrc",
                "InitialModel/job008/run_it150_grad002.mrc",
            ],
            "Class3D": [
                "Class3D/job009/run_it025_class001.mrc",
                "Class3D/job009/run_it025_class001_angdist.bild",
                "Class3D/job009/run_it025_class002.mrc",
                "Class3D/job009/run_it025_class002_angdist.bild",
                "Class3D/job009/run_it025_class003.mrc",
                "Class3D/job009/run_it025_class003_angdist.bild",
                "Class3D/job009/run_it025_sampling.star",
                "Class3D/job009/run_it025_data.star",
                "Class3D/job009/run_it025_model.star",
                "Class3D/job009/run_it025_optimiser.star",
            ],
            "Refine3D": [
                "Refine3D/job010/run_it016_half1_class001.mrc",
                "Refine3D/job010/run_it016_half1_class001_angdist.bild",
                "Refine3D/job010/run_it016_half1_model.star",
                "Refine3D/job010/run_it016_half2_class001.mrc",
                "Refine3D/job010/run_it016_half2_class001_angdist.bild",
                "Refine3D/job010/run_it016_half2_model.star",
                "Refine3D/job010/run_it016_sampling.star",
                "Refine3D/job010/run_it016_data.star",
                "Refine3D/job010/run_it016_optimiser.star",
            ],
            "MultiBody": [
                "MultiBody/job011/run_it012_data.star",
                "MultiBody/job011/run_it012_half1_body001_angdist.bild",
                "MultiBody/job011/run_it012_half1_body001.mrc",
                "MultiBody/job011/run_it012_half1_body002_angdist.bild",
                "MultiBody/job011/run_it012_half1_body002.mrc",
                "MultiBody/job011/run_it012_half1_model.star",
                "MultiBody/job011/run_it012_half2_body001_angdist.bild",
                "MultiBody/job011/run_it012_half2_body001.mrc",
                "MultiBody/job011/run_it012_half2_body002_angdist.bild",
                "MultiBody/job011/run_it012_half2_body002.mrc",
                "MultiBody/job011/run_it012_half2_model.star",
            ],
            "CtfRefine": [],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "LocalRes": [],
        }

        files = list()
        for search_string in del_files:
            files += glob("{}/*/**/*".format(search_string), recursive=True)

        delete_list = list()
        for f in del_files:
            for search_string in del_files[f]:
                if len(search_string) > 1:
                    add = glob(f + search_string)
                    delete_list += add

        for f in exclude_files:
            for ff in exclude_files[f]:
                delete_list.remove(ff)

        ccpem_pipeliner.main(["--cleanup", "ALL"])

        # sort the files
        dirnames = [
            "Import/job001",
            "MotionCorr/job002",
            "MotionCorr/job002/Raw_data",
            "CtfFind/job003",
            "CtfFind/job003/Raw_data",
            "AutoPick/job004/Raw_data",
            "Extract/job005/Raw_data",
            "Class2D/job006",
            "Select/job007",
            "InitialModel/job008",
            "Class3D/job009",
            "Refine3D/job010",
            "MultiBody/job011",
            "CtfRefine/job012/Raw_data",
            "MaskCreate/job013",
            "Polish/job014/Raw_data",
            "JoinStar/job015",
            "Subtract/job016",
            "External/job017",
            "PostProcess/job018",
            "LocalRes/job019",
        ]
        removed, kept = [], []
        for f in files:
            if f in delete_list:
                removed.append(f)
            else:
                if f not in dirnames:
                    kept.append(f)

        # check they are all in the right place
        files2 = list()
        for search_string in del_files:
            files2 += glob("{}/*/**/*".format(search_string), recursive=True)
        trash = glob(f"{CLEANUP_DIR}/**/*", recursive=True)
        for f in kept:
            assert f in files2, f
        for f in removed:
            assert f not in files2, f
            assert f"{CLEANUP_DIR}/{f}" in trash

    @slow_test
    def test_cleanup_alljobs_harsh(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_full_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        ShortpipeFileStructure(["all"])

        del_files = {
            "Import": [],
            "MotionCorr": ["/job002/Raw_data/*"],
            "CtfFind": [
                "/job003/gctf*.out",
                "/job003/gctf*.err",
                "/job003/Raw_data/*",
            ],
            "AutoPick": ["/job004/Raw_data/*.spi"],
            "Extract": ["/job005/Raw_data/*"],
            "Class2D": ["/job006/run_it*"],
            "Select": [],
            "InitialModel": ["/job008/run_it*"],
            "Class3D": ["/job009/run_it*"],
            "Refine3D": ["/job010/run_it*"],
            "MultiBody": ["/job011/run_it*", "/job011/analyse_component*_bin*.mrc"],
            "CtfRefine": [
                "/job012/Raw_data/*_wAcc.mrc",
                "/job012/Raw_data/*_xyAcc_real.mrc",
                "/job012/Raw_data/*_xyAcc_imag.mrc",
            ],
            "MaskCreate": [],
            "Polish": [
                "/job014/Raw_data/*_FCC_cc.mrc",
                "/job014/Raw_data/*_FCC_w0.mrc",
                "/job014/Raw_data/*_FCC_w1.mrc",
                "/job014/Raw_data/*shiny.star",
                "/job014/Raw_data/*shiny.mrcs",
            ],
            "JoinStar": [],
            "Subtract": ["/job016/subtracted_*"],
            "External": [],
            "PostProcess": ["/job017/*masked.mrc"],
            "LocalRes": [],
        }

        exclude_files = {
            "Import": [],
            "MotionCorr": [],
            "CtfFind": [],
            "AutoPick": [],
            "Extract": [],
            "Class2d": [
                "Class2D/job006/run_it025_data.star",
                "Class2D/job006/run_it025_sampling.star",
                "Class2D/job006/run_it025_model.star",
                "Class2D/job006/run_it025_optimiser.star",
                "Class2D/job006/run_it025_classes.mrcs",
            ],
            "Select": [],
            "InitialModel": [
                "InitialModel/job008/run_it150_data.star",
                "InitialModel/job008/run_it150_class001_data.star",
                "InitialModel/job008/run_it150_class002_data.star",
                "InitialModel/job008/run_it150_sampling.star",
                "InitialModel/job008/run_it150_model.star",
                "InitialModel/job008/run_it150_optimiser.star",
                "InitialModel/job008/run_it150_class001.mrc",
                "InitialModel/job008/run_it150_grad001.mrc",
                "InitialModel/job008/run_it150_class002.mrc",
                "InitialModel/job008/run_it150_grad002.mrc",
            ],
            "Class3D": [
                "Class3D/job009/run_it025_class001.mrc",
                "Class3D/job009/run_it025_class001_angdist.bild",
                "Class3D/job009/run_it025_class002.mrc",
                "Class3D/job009/run_it025_class002_angdist.bild",
                "Class3D/job009/run_it025_class003.mrc",
                "Class3D/job009/run_it025_class003_angdist.bild",
                "Class3D/job009/run_it025_sampling.star",
                "Class3D/job009/run_it025_data.star",
                "Class3D/job009/run_it025_model.star",
                "Class3D/job009/run_it025_optimiser.star",
            ],
            "Refine3D": [
                "Refine3D/job010/run_it016_half1_class001.mrc",
                "Refine3D/job010/run_it016_half1_class001_angdist.bild",
                "Refine3D/job010/run_it016_half1_model.star",
                "Refine3D/job010/run_it016_half2_class001.mrc",
                "Refine3D/job010/run_it016_half2_class001_angdist.bild",
                "Refine3D/job010/run_it016_half2_model.star",
                "Refine3D/job010/run_it016_sampling.star",
                "Refine3D/job010/run_it016_data.star",
                "Refine3D/job010/run_it016_optimiser.star",
            ],
            "MultiBody": [
                "MultiBody/job011/run_it012_data.star",
                "MultiBody/job011/run_it012_half1_body001_angdist.bild",
                "MultiBody/job011/run_it012_half1_body001.mrc",
                "MultiBody/job011/run_it012_half1_body002_angdist.bild",
                "MultiBody/job011/run_it012_half1_body002.mrc",
                "MultiBody/job011/run_it012_half1_model.star",
                "MultiBody/job011/run_it012_half2_body001_angdist.bild",
                "MultiBody/job011/run_it012_half2_body001.mrc",
                "MultiBody/job011/run_it012_half2_body002_angdist.bild",
                "MultiBody/job011/run_it012_half2_body002.mrc",
                "MultiBody/job011/run_it012_half2_model.star",
            ],
            "CtfRefine": [],
            "MaskCreate": [],
            "Polish": [],
            "JoinStar": [],
            "Subtract": [],
            "External": [],
            "LocalRes": [],
        }

        files = list()
        for search_string in del_files:
            files += glob("{}/*/**/*".format(search_string), recursive=True)

        delete_list = list()
        for f in del_files:
            for search_string in del_files[f]:
                if len(search_string) > 1:
                    add = glob(f + search_string)
                    delete_list += add

        for f in exclude_files:
            for ff in exclude_files[f]:
                delete_list.remove(ff)

        # run the cleanup
        ccpem_pipeliner.main(["--cleanup", "ALL", "--harsh"])
        # sort the files
        dirnames = [
            "Import/job001",
            "MotionCorr/job002",
            "MotionCorr/job002/Raw_data",
            "CtfFind/job003",
            "CtfFind/job003/Raw_data",
            "AutoPick/job004/Raw_data",
            "Extract/job005/Raw_data",
            "Class2D/job006",
            "Select/job007",
            "InitialModel/job008",
            "Class3D/job009",
            "Refine3D/job010",
            "MultiBody/job011",
            "CtfRefine/job012/Raw_data",
            "MaskCreate/job013",
            "Polish/job014/Raw_data",
            "JoinStar/job015",
            "Subtract/job016",
            "External/job017",
            "PostProcess/job018",
            "LocalRes/job019",
        ]
        removed, kept = [], []
        for f in files:
            if f in delete_list:
                removed.append(f)
            else:
                if f not in dirnames:
                    kept.append(f)

        # check they are all in the right place
        files2 = list()
        for search_string in del_files:
            files2 += glob("{}/*/**/*".format(search_string), recursive=True)
        trash = glob(f"{CLEANUP_DIR}/**/*", recursive=True)
        for f in kept:
            assert f in files2, f
        for f in removed:
            assert f not in files2, f
            assert f"{CLEANUP_DIR}/{f}" in trash

    def test_validate_starfile(self):
        """run validate on a good starfile"""
        shutil.copy(
            os.path.join(self.test_data, "StarFiles/motioncorr_changed.star"),
            self.test_dir,
        )
        ccpem_pipeliner.main(["--validate_starfile", "motioncorr_changed.star"])

    def test_validate_starfile_reserved_word(self):
        """run validate on a starfile with a reserved word"""
        shutil.copy(
            os.path.join(self.test_data, "StarFiles/motioncorr_not_quotated_fail.star"),
            self.test_dir,
        )
        ccpem_pipeliner.main(
            ["--validate_starfile", "motioncorr_not_quotated_fail.star"]
        )
        badlines = [
            ["save_noDW", "No"],
            ["save_ps", "Yes"],
        ]
        goodlines = [
            ["'save_noDW'", "No"],
            ["'save_ps'", "Yes"],
        ]
        with open("motioncorr_not_quotated_fail.star") as fixed:
            fixed_data = [x.split() for x in fixed.readlines()]
        for line in badlines:
            assert line not in fixed_data, line
        for line in goodlines:
            assert line in fixed_data, line

    def test_validate_starfile_reserved_word_different_dir(self):
        """run validate on a starfile with a reserved word not located
        in the directory that the program was run from, make sure it writes in
        the correct place"""
        os.makedirs(os.path.join(self.test_dir, "1/2/3"))
        shutil.copy(
            os.path.join(self.test_data, "StarFiles/motioncorr_not_quotated_fail.star"),
            os.path.join(self.test_dir, "1/2/3"),
        )
        ccpem_pipeliner.main(
            ["--validate_starfile", "1/2/3/motioncorr_not_quotated_fail.star"]
        )
        badlines = [
            ["save_noDW", "No"],
            ["save_ps", "Yes"],
        ]
        goodlines = [
            ["'save_noDW'", "No"],
            ["'save_ps'", "Yes"],
        ]
        with open("1/2/3/motioncorr_not_quotated_fail.star") as fixed:
            fixed_data = [x.split() for x in fixed.readlines()]
        for line in badlines:
            assert line not in fixed_data, line
        for line in goodlines:
            assert line in fixed_data, line

    def test_validate_starfile_unfixable(self):
        """run validate on a good starfile"""
        shutil.copy(
            os.path.join(self.test_data, "StarFiles/invalid_starfile.star"),
            self.test_dir,
        )
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(["--validate_starfile", "invalid_starfile.star"])

    def test_deleting_file_then_empty_trash(self):
        self.test_deleting_job()
        nr_trash_files = len(glob(f"{TRASH_DIR}/*/*/*"))
        assert nr_trash_files == 39
        with open("Yes", "w") as doit:
            doit.write("Yes")
        yes = open("Yes")
        sys.stdin = yes
        ccpem_pipeliner.main(["--empty_trash"])
        nr_trash_files = len(glob(f"{TRASH_DIR}/*/*/*"))
        assert nr_trash_files == 0
        yes.close()

    def test_empty_trash_error_nofiles(self):
        nr_trash_files = len(glob(f"{TRASH_DIR}/*/*/*"))
        assert nr_trash_files == 0
        ccpem_pipeliner.main(["--new_project"])
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(["--empty_trash"])

    @slow_test
    def test_continuing_job_basic(self):
        """Make sure continuation creates the job.star.ct000 file"""
        # run a postprocess job
        job = self.run_postprocess_job()
        # continue that job
        ccpem_pipeliner.f_continue_job(MockArgs(continue_job=job.output_dir))
        # Wait for the job to finish
        assert wait_for_job_to_finish(job) == JOBSTATUS_SUCCESS
        assert os.path.isfile("PostProcess/job001/job.star.ct000")
        # make sure it actually ran 2x
        with open("PostProcess/job001/run.out") as runout:
            runout_lines = runout.readlines()
        count = 0
        for line in runout_lines:
            if "FINAL RESOLUTION:" in line:
                count += 1
        assert count == 2, count

    @slow_test
    def test_continuing_job_multiple(self):
        """Make sure multiple continuations increment the job.star.ctxxx file"""
        # run a postprocess job
        job = self.run_postprocess_job()
        # continue that job 3x
        job = ccpem_pipeliner.f_continue_job(MockArgs(continue_job=job.output_dir))
        # Wait for the job to finish
        assert wait_for_job_to_finish(job) == JOBSTATUS_SUCCESS
        job = ccpem_pipeliner.f_continue_job(MockArgs(continue_job=job.output_dir))
        # Wait for the job to finish
        assert wait_for_job_to_finish(job) == JOBSTATUS_SUCCESS
        job = ccpem_pipeliner.f_continue_job(MockArgs(continue_job=job.output_dir))
        # Wait for the job to finish
        assert wait_for_job_to_finish(job) == JOBSTATUS_SUCCESS

        # make sure the files are there
        assert os.path.isfile("PostProcess/job001/job.star.ct000")
        assert os.path.isfile("PostProcess/job001/job.star.ct001")
        assert os.path.isfile("PostProcess/job001/job.star.ct002")

        # make sure it actually ran 4x
        with open("PostProcess/job001/run.out") as runout:
            runout_lines = runout.readlines()
        count = 0
        for line in runout_lines:
            if "FINAL RESOLUTION:" in line:
                count += 1
        assert count == 4, count

    @live_test(job="relion.postprocess")
    def test_continuing_job_continue_file_is_missing(self):
        """If the continue_job.star file is missing fall back to the
        orginial job.star"""
        # run a postprocess job
        job = self.run_postprocess_job()
        # continue that job
        os.remove("PostProcess/job001/continue_job.star")
        ccpem_pipeliner.main(["--continue_job", "PostProcess/job001"])
        # Wait for the job to finish
        assert wait_for_job_to_finish(job) == JOBSTATUS_SUCCESS
        assert os.path.isfile("PostProcess/job001/job.star.ct000")
        # make sure it actually ran 2x
        with open("PostProcess/job001/run.out") as runout:
            runout_lines = runout.readlines()
        count = 0
        for line in runout_lines:
            if "FINAL RESOLUTION:" in line:
                count += 1
        assert count == 2, count

    @live_test(job="relion.postprocess")
    def test_continuing_job_both_files_missing(self):
        """If the continue_job.star and the job.star files are
        missing the continue can't run"""
        # run a postprocess job
        self.run_postprocess_job()
        # continue that job
        os.remove("PostProcess/job001/continue_job.star")
        os.remove("PostProcess/job001/job.star")
        with self.assertRaises(ValueError):
            ccpem_pipeliner.main(["--continue_job", "PostProcess/job001"])

    @live_test(job="relion.postprocess")
    def test_continuing_job_parameters_are_set_correctly(self):
        """Make sure continuation subs in the values from the continue
        as expected"""
        # run a postprocess job
        job = self.run_postprocess_job()

        # read the command that was executed
        with open("PostProcess/job001/note.txt") as notefile:
            note1 = notefile.read()

        # edit the continue_file
        contfile = "PostProcess/job001/continue_job.star"
        js = JobStar(contfile)
        js.modify({"angpix": "2.55"})
        js.write(contfile)

        # continue that job
        job = ccpem_pipeliner.f_continue_job(MockArgs(continue_job=job.output_dir))
        # Wait for the job to finish
        assert wait_for_job_to_finish(job) == JOBSTATUS_SUCCESS
        assert os.path.isfile("PostProcess/job001/job.star.ct000")

        # read the new command
        with open("PostProcess/job001/note.txt") as notefile:
            note2 = notefile.read()

        # make sure the commands are different
        assert "--angpix 1.244" in note1
        assert "--angpix 2.55" in note2

        # make sure it actually ran 2x
        with open("PostProcess/job001/run.out") as runout:
            runout_lines = runout.readlines()

        # make sure it was run with the new parameter
        resolution_lines = []
        for line in runout_lines:
            if "FINAL RESOLUTION:" in line:
                resolution_lines.append(line)
        assert len(resolution_lines) == 2
        assert resolution_lines[0] != resolution_lines[1]

    @live_test(job="relion.postprocess")
    def test_scheduling_continue_job(self):
        """Make sure scheduling continuation of a job
        works as expected"""
        # run a postprocess job
        self.run_postprocess_job()

        line = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SUCCESS]
        pipeline = self.read_pipeline()

        assert line in pipeline, line
        # schedule a continuation of that job
        ccpem_pipeliner.main(["--schedule_job", "PostProcess/job001"])

        line = ["PostProcess/job001/", "None", POSTPROCESS_JOB_NAME, JOBSTATUS_SCHED]
        pipeline = self.read_pipeline()
        assert line in pipeline

    def test_convert_pipeline(self):
        """Convert an old style pipeline to a new style one"""
        # copy in the pipeline - old version
        make_conversion_file_structure()

        # additional dirs for the micrograph files coordinate conversion reads
        mic_files = [
            "Select/job005/micrographs_selected.star",
            "CtfFind/job003//micrographs_ctf.star",
        ]
        testfile = os.path.join(self.test_data, "micrographs_ctf.star")
        for f in mic_files:
            shutil.copy(testfile, f)

        shutil.copy(
            os.path.join(self.test_data, "Pipelines/relion31_tutorial_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )
        # copy in the pipeline - new version
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/converted_relion31_pipeline.star"),
            self.test_dir,
        )
        ccpem_pipeliner.main(["--convert_pipeline_file", "default_pipeline.star"])
        compare_starfiles("default_pipeline.star", "converted_relion31_pipeline.star")

    def test_convert_pipeline_convert_error(self):
        """Convert error raised when pipeline is already new style"""
        # copy in the pipeline - new version
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/converted_relion31_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        assert not ccpem_pipeliner.main(
            ["--convert_pipeline_file", "default_pipeline.star"]
        )

    @staticmethod
    def test_stop_schedule_GUI_style():
        """if the schedule is made by the GUI the RUNNING_ file is empty
        and should be deleted"""
        # make the RUNNING_ file
        touch("RUNNING_PIPELINER_default_empty")
        assert os.path.isfile("RUNNING_PIPELINER_default_empty")

        # stop the schedule
        ccpem_pipeliner.main(["--new_project"])
        ccpem_pipeliner.main(["--stop_schedule", "empty"])

        # file should be gone
        assert not os.path.isfile("RUNNING_PIPELINER_default_empty")

    def test_stop_schedule_from_API_style(self):
        """if the schedule is made by the API the RUNNING_ file
        just has a list of jobs"""

        # make the RUNNING_ file and populate it
        touch("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api", "w") as f:
            f.write("PostProcess/job001/\n")
        assert os.path.isfile("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api") as f:
            runfile_data = f.readlines()
        assert runfile_data[0] == "PostProcess/job001/\n"

        # initialize the project and copy in the pipeline
        ccpem_pipeliner.main(["--new_project"])
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/running_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        # make the necesary project dirs
        os.makedirs("PostProcess/job001")

        # stop the schedule
        ccpem_pipeliner.main(["--stop_schedule", "api"])

        # RUNNING file should be gone and ABORTED file should appear
        assert not os.path.isfile("RUNNING_PIPELINER_default_api")
        assert os.path.isfile("PostProcess/job001/" + ABORT_TRIGGER)

    def test_stop_schedule_from_API_style_pipeliner_custom_name(self):
        """if the schedule is made by the API the RUNNING_ file
        just has a list of jobs.  Check this works if the pipeline
        name is not default"""

        # make the RUNNING_ file and populate it
        touch("RUNNING_PIPELINER_fancypants_api")
        with open("RUNNING_PIPELINER_fancypants_api", "w") as f:
            f.write("PostProcess/job001/\n")
        assert os.path.isfile("RUNNING_PIPELINER_fancypants_api")
        with open("RUNNING_PIPELINER_fancypants_api") as f:
            runfile_data = f.readlines()
        assert runfile_data[0] == "PostProcess/job001/\n"

        # initialize the project and copy in the pipeline
        ccpem_pipeliner.main(["--new_project", "fancypants"])
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/running_pipeline.star"),
            os.path.join(self.test_dir, "fancypants_pipeline.star"),
        )

        # make the necesary project dirs
        os.makedirs("PostProcess/job001")

        # stop the schedule
        ccpem_pipeliner.main(["--stop_schedule", "api"])

        # RUNNING file should be gone and ABORTED file should appear
        assert not os.path.isfile("RUNNING_PIPELINER_fancypants_api")
        assert os.path.isfile("PostProcess/job001/" + ABORT_TRIGGER)

    def test_stop_schedule_from_CL_style(self):
        """if the schedule is made by pipeliner the RUNNING_ file
        has a PID and a list of jobs, the schedule process needs to
        be killed as well"""

        # make the RUNNING_ file and populate it
        touch("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api", "w") as f:
            f.write("CL_RELION_SCHEDULE\n")
            f.write("9999999999\n")
            f.write("PostProcess/job001/\n")
        assert os.path.isfile("RUNNING_PIPELINER_default_api")
        with open("RUNNING_PIPELINER_default_api") as f:
            runfile_data = f.readlines()
        assert runfile_data == [
            "CL_RELION_SCHEDULE\n",
            "9999999999\n",
            "PostProcess/job001/\n",
        ]

        # initialize the project and copy in the pipeline
        ccpem_pipeliner.main(["--new_project"])
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/running_pipeline.star"),
            os.path.join(self.test_dir, "default_pipeline.star"),
        )

        # make the necesary project dirs
        os.makedirs("PostProcess/job001")

        # stop the schedule
        ccpem_pipeliner.main(["--stop_schedule", "api"])

        # RUNNING file should be gone and ABORTED file should appear
        assert not os.path.isfile("RUNNING_PIPELINER_default_api")
        assert os.path.isfile("PostProcess/job001/" + ABORT_TRIGGER)

    @staticmethod
    def test_write_default_starfile():
        """No need to test for all types as this is done in api_utils
        tests"""
        ccpem_pipeliner.main(["--default_jobstar", "relion.import.movies"])
        assert os.path.isfile("relion_import_movies_job.star")

    @staticmethod
    def test_write_default_runjob():
        """No need to test for all types as this is done in api_utils
        tests"""
        ccpem_pipeliner.main(["--default_runjob", "relion.import.movies"])
        assert os.path.isfile("relion_import_movies_run.job")

    @staticmethod
    def test_default_writing_relionstyle_jobstar():
        ccpem_pipeliner.main(["--default_jobstar", "relion.import", "--relionstyle"])
        wrote = JobStar("relion_import_job.star").all_options_as_dict()

        template_file = os.path.join(
            get_pipeliner_root(),
            "jobs/relion/relion_jobstars/default_relion_import_job.star",
        )
        exp_data = JobStar(template_file).all_options_as_dict()

        for jobop in wrote:
            assert exp_data[jobop] == wrote[jobop], (
                jobop,
                exp_data[jobop],
                wrote[jobop],
            )


if __name__ == "__main__":
    unittest.main()
