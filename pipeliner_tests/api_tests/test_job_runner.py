#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import contextlib
import io
import itertools
import os
import shutil
import tempfile
import textwrap
import threading
import time
import unittest
from functools import partial
from pathlib import Path
from unittest.mock import patch

from pipeliner.scripts import job_runner
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import DummyJob, clean_starfile
from pipeliner.pipeliner_job import PipelinerCommand


def mock_time_generator():
    for count in itertools.count(start=1):
        yield f"mock time {count}"


class JobRunnerTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = Path(test_data.__file__).parent
        # Change to test directory
        orig_dir = os.getcwd()
        test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        os.chdir(test_dir)
        self.addCleanup(os.chdir, orig_dir)
        # Use partial() here to avoid PyCharm type inspection errors from addCleanup()
        self.addCleanup(partial(shutil.rmtree, path=test_dir, ignore_errors=True))

        # Patch date_time_tag() for reproducible outputs
        patcher = patch("pipeliner.scripts.job_runner.date_time_tag")
        self.addCleanup(patcher.stop)
        mock_date_time_tag = patcher.start()
        mock_date_time_tag.side_effect = mock_time_generator()

    def test_run_job_from_missing_file(self):
        job_dir = Path("Test/job001")
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            with self.assertRaises(FileNotFoundError):
                job_runner.run_job(Path("job.star"), job_dir=job_dir)
        captured_stdout = redirected_stdout.getvalue()
        assert captured_stdout == ""
        assert (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()
        assert (job_dir / "run.err").is_file()
        assert (
            (job_dir / "run.err")
            .read_text()
            .startswith("ERROR: Failed to read job file job.star")
        )

    @staticmethod
    def test_get_note_txt_one_command():
        note_txt = job_runner.get_note_txt_str("Example/job002/", [["command", "one"]])
        assert note_txt == textwrap.dedent(
            """\
             ++++ CCP-EM Pipeliner running job Example/job002/ at mock time 1
             ++++ with the following command:
            command one
             ++++"""
        )

    @staticmethod
    def test_get_note_txt_two_commands():
        note_txt = job_runner.get_note_txt_str(
            "Example/job002/", [["command", "one"], ["command", "two with spaces"]]
        )
        assert note_txt == textwrap.dedent(
            """\
             ++++ CCP-EM Pipeliner running job Example/job002/ at mock time 1
             ++++ with the following commands:
            command one
            command 'two with spaces'
             ++++"""
        )

    @staticmethod
    def test_check_abort_trigger_without_signal():
        runner = job_runner.JobRunner(DummyJob())
        runner.check_abort_trigger()

    def test_check_abort_trigger_with_signal(self):
        runner = job_runner.JobRunner(DummyJob())
        Path("PIPELINER_JOB_ABORT_NOW").touch()
        with self.assertRaises(job_runner.JobAbortedException):
            runner.check_abort_trigger()

    @staticmethod
    def test_clear_termination_status_indicators_with_all_present():
        runner = job_runner.JobRunner(DummyJob())
        Path("PIPELINER_JOB_EXIT_SUCCESS").touch()
        Path("PIPELINER_JOB_EXIT_FAILED").touch()
        Path("PIPELINER_JOB_EXIT_ABORTED").touch()
        runner.clear_termination_status_indicators()
        assert not Path("PIPELINER_JOB_EXIT_SUCCESS").exists()
        assert not Path("PIPELINER_JOB_EXIT_FAILED").exists()
        assert not Path("PIPELINER_JOB_EXIT_ABORTED").exists()

    @staticmethod
    def test_clear_termination_status_indicators_with_none_present():
        runner = job_runner.JobRunner(DummyJob())
        runner.clear_termination_status_indicators()
        assert not Path("PIPELINER_JOB_EXIT_SUCCESS").exists()
        assert not Path("PIPELINER_JOB_EXIT_FAILED").exists()
        assert not Path("PIPELINER_JOB_EXIT_ABORTED").exists()

    @staticmethod
    def test_run_single_command_successful():
        runner = job_runner.JobRunner(DummyJob())
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            runner.run_single_command(3, ["echo", "example output"])
        captured_stdout = redirected_stdout.getvalue()
        assert "Command 3 finished successfully" in captured_stdout

    def test_run_single_command_unsuccessful(self):
        runner = job_runner.JobRunner(DummyJob())
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            with self.assertRaises(job_runner.JobFailedException):
                runner.run_single_command(10, ["sh", "-c", "exit 15"])
        captured_stdout = redirected_stdout.getvalue()
        assert "Command 10 failed with exit status 15" in captured_stdout

    def test_run_single_command_aborted(self):
        runner = job_runner.JobRunner(DummyJob())
        Path("PIPELINER_JOB_ABORT_NOW").touch()
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            with self.assertRaises(job_runner.JobAbortedException):
                runner.run_single_command(6, ["sleep", "1"])
        captured_stdout = redirected_stdout.getvalue()
        assert "Command 6 terminated" in captured_stdout

    @staticmethod
    @patch("pipeliner.job_factory.read_job")
    def test_full_run_of_dummy_job_with_mock_load(mock_read_job):
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)
        # Even though it won't be used because of the mock, we need to create the fake
        # job file so we don't get a FileNotFoundError
        fake_job_file = Path("fake job file")
        fake_job_file.touch()

        job = DummyJob()
        mock_read_job.return_value = job

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_runner.run_job(
                job_file=fake_job_file,
                job_dir=job_dir,
            )
        captured_stdout = redirected_stdout.getvalue()

        assert (job_dir / "run.out").is_file()
        assert (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert (job_dir / "test_file.txt").is_file()
        assert (job_dir / "job_pipeline.star").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_ABORTED").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()

        expected_commands = [
            f"{shutil.which('touch')} Test/job001/test_file.txt",
            f"{shutil.which('touch')} Test/job001/post_run.txt",
            f"{shutil.which('echo')} 'echo command wrote this line'",
        ]

        assert (job_dir / "note.txt").read_text() == textwrap.dedent(
            f"""\
             ++++ CCP-EM Pipeliner running job Test/job001/ at mock time 2
             ++++ with the following commands:
            {expected_commands[0]}
            {expected_commands[1]}
            {expected_commands[2]}
             ++++
            """
        )
        assert captured_stdout == textwrap.dedent(
            f"""\
            mock time 1: CCP-EM Pipeliner: Loading job from fake job file
            mock time 3: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
            mock time 4: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
            mock time 5: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
            mock time 6: CCP-EM Pipeliner job Test/job001/: Command 2 finished successfully
            mock time 7: CCP-EM Pipeliner job Test/job001/: Running command 3 of 3: {expected_commands[2]}
            mock time 8: CCP-EM Pipeliner job Test/job001/: Command 3 finished successfully
            mock time 9: CCP-EM Pipeliner job Test/job001/: Job finished
            """  # noqa: E501
        )
        assert (job_dir / "run.out").read_text() == textwrap.dedent(
            f"""\
            mock time 3: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
            mock time 4: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
            mock time 5: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
            mock time 6: CCP-EM Pipeliner job Test/job001/: Command 2 finished successfully
            mock time 7: CCP-EM Pipeliner job Test/job001/: Running command 3 of 3: {expected_commands[2]}
            echo command wrote this line
            mock time 8: CCP-EM Pipeliner job Test/job001/: Command 3 finished successfully
            mock time 9: CCP-EM Pipeliner job Test/job001/: Job finished
            """  # noqa: E501
        )
        assert (job_dir / "run.err").read_text() == ""
        assert (job_dir / "test_file.txt").is_file()

    @staticmethod
    def test_full_run_with_command_failure():
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)

        job = DummyJob(
            commands=[
                PipelinerCommand(["echo", "output from first command"]),
                PipelinerCommand(["bash", "-c", "exit 1"]),
                PipelinerCommand(["touch", "Test/job001/test_file.txt"]),
            ]
        )
        job.output_dir = os.fspath(job_dir) + "/"

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_runner.JobRunner(job).run_job()
        captured_stdout = redirected_stdout.getvalue()

        assert (job_dir / "run.out").is_file()
        assert (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_ABORTED").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()

        expected_commands = [
            f"{shutil.which('echo')} 'output from first command'",
            f"{shutil.which('bash')} -c 'exit 1'",
            f"{shutil.which('touch')} Test/job001/test_file.txt",
        ]

        assert (job_dir / "note.txt").read_text() == textwrap.dedent(
            f"""\
             ++++ CCP-EM Pipeliner running job Test/job001/ at mock time 1
             ++++ with the following commands:
            {expected_commands[0]}
            {expected_commands[1]}
            {expected_commands[2]}
             ++++
            """
        )
        assert captured_stdout == textwrap.dedent(
            f"""\
            mock time 2: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
            mock time 3: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
            mock time 4: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
            mock time 5: CCP-EM Pipeliner job Test/job001/: ERROR: Command 2 failed with exit status 1
            mock time 6: CCP-EM Pipeliner job Test/job001/: Job failed
            """  # noqa: E501
        )
        assert (job_dir / "run.out").read_text() == textwrap.dedent(
            f"""\
            mock time 2: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
            output from first command
            mock time 3: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
            mock time 4: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
            mock time 5: CCP-EM Pipeliner job Test/job001/: ERROR: Command 2 failed with exit status 1
            mock time 6: CCP-EM Pipeliner job Test/job001/: Job failed
            """  # noqa: E501
        )
        assert (job_dir / "run.err").read_text() == ""
        assert not (job_dir / "test_file.txt").exists()

    @staticmethod
    def test_full_run_with_post_run_node_creation_failure():
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)

        job = DummyJob()
        job.output_dir = os.fspath(job_dir) + "/"
        job.create_post_run_output_nodes = lambda: 1 / 0

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_runner.JobRunner(job).run_job()
        captured_stdout = redirected_stdout.getvalue()

        assert (job_dir / "run.out").is_file()
        assert (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_ABORTED").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()

        expected_commands = [
            f"{shutil.which('touch')} Test/job001/test_file.txt",
            f"{shutil.which('touch')} Test/job001/post_run.txt",
            f"{shutil.which('echo')} 'echo command wrote this line'",
        ]

        assert (job_dir / "note.txt").read_text() == textwrap.dedent(
            f"""\
             ++++ CCP-EM Pipeliner running job Test/job001/ at mock time 1
             ++++ with the following commands:
            {expected_commands[0]}
            {expected_commands[1]}
            {expected_commands[2]}
             ++++
            """
        )
        assert captured_stdout == textwrap.dedent(
            f"""\
             mock time 2: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
             mock time 3: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
             mock time 4: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
             mock time 5: CCP-EM Pipeliner job Test/job001/: Command 2 finished successfully
             mock time 6: CCP-EM Pipeliner job Test/job001/: Running command 3 of 3: {expected_commands[2]}
             mock time 7: CCP-EM Pipeliner job Test/job001/: Command 3 finished successfully
             mock time 8: CCP-EM Pipeliner job Test/job001/: ERROR: post run node creation failed
             mock time 9: CCP-EM Pipeliner job Test/job001/: Job failed
            """  # noqa: E501
        )
        assert (job_dir / "run.out").read_text() == textwrap.dedent(
            f"""\
            mock time 2: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
            mock time 3: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
            mock time 4: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
            mock time 5: CCP-EM Pipeliner job Test/job001/: Command 2 finished successfully
            mock time 6: CCP-EM Pipeliner job Test/job001/: Running command 3 of 3: {expected_commands[2]}
            echo command wrote this line
            mock time 7: CCP-EM Pipeliner job Test/job001/: Command 3 finished successfully
            mock time 8: CCP-EM Pipeliner job Test/job001/: ERROR: post run node creation failed
            mock time 9: CCP-EM Pipeliner job Test/job001/: Job failed
            """  # noqa: E501
        )
        stderr = (job_dir / "run.err").read_text().splitlines()
        assert stderr[0] == "ERROR: post run node creation raised an error"
        assert stderr[1] == "Traceback (most recent call last):"
        assert stderr[-1] == "ZeroDivisionError: division by zero"
        assert (job_dir / "test_file.txt").is_file()
        assert (job_dir / "test_file.txt").read_text() == ""

    @staticmethod
    def test_full_run_with_immediate_abort():
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)

        job = DummyJob()
        job.output_dir = os.fspath(job_dir) + "/"

        abort_trigger = job_dir / "PIPELINER_JOB_ABORT_NOW"
        abort_trigger.touch()

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_runner.JobRunner(job).run_job()
        captured_stdout = redirected_stdout.getvalue()

        assert (job_dir / "run.out").is_file()
        assert not (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_ABORTED").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()

        # Check that the abort trigger has been removed
        assert not abort_trigger.is_file()

        expected_commands = [
            f"{shutil.which('touch')} Test/job001/test_file.txt",
            f"{shutil.which('touch')} Test/job001/post_run.txt",
            f"{shutil.which('echo')} 'echo command wrote this line'",
        ]

        assert (job_dir / "note.txt").read_text() == textwrap.dedent(
            f"""\
             ++++ CCP-EM Pipeliner running job Test/job001/ at mock time 1
             ++++ with the following commands:
            {expected_commands[0]}
            {expected_commands[1]}
            {expected_commands[2]}
             ++++
            """
        )
        assert captured_stdout == textwrap.dedent(
            """\
            mock time 2: CCP-EM Pipeliner job Test/job001/: Found abort trigger file, aborting job
            mock time 3: CCP-EM Pipeliner job Test/job001/: Job aborted
            """  # noqa: E501
        )
        assert (job_dir / "run.out").read_text() == textwrap.dedent(
            """\
            mock time 2: CCP-EM Pipeliner job Test/job001/: Found abort trigger file, aborting job
            mock time 3: CCP-EM Pipeliner job Test/job001/: Job aborted
            """  # noqa: E501
        )
        assert not (job_dir / "test_file.txt").exists()

    @staticmethod
    def test_full_run_with_abort_during_long_running_command():
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)

        first_command_output_file = job_dir / "1.out"

        job = DummyJob(
            commands=[
                PipelinerCommand(["touch", os.fspath(first_command_output_file)]),
                PipelinerCommand(["sleep", "10"]),
                PipelinerCommand(["touch", "Test/job001/test_file.txt"]),
            ]
        )
        job.output_dir = os.fspath(job_dir) + "/"
        runner = job_runner.JobRunner(job, abort_poll_interval=0.1)

        abort_trigger = job_dir / "PIPELINER_JOB_ABORT_NOW"

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_running_thread = threading.Thread(
                target=runner.run_job,
                daemon=True,
            )
            assert not first_command_output_file.is_file()
            job_running_thread.start()

            try:
                # Wait for the file from the first command to appear
                wait_count = 0
                while not first_command_output_file.is_file():
                    time.sleep(0.05)
                    wait_count += 1
                    if wait_count > 100:
                        raise TimeoutError("Timed out waiting for file to appear")
                abort_trigger.touch()
            finally:
                job_running_thread.join(timeout=10.0)
                assert not job_running_thread.is_alive(), "join() timed out"

        captured_stdout = redirected_stdout.getvalue()

        assert first_command_output_file.is_file()
        assert (job_dir / "run.out").is_file()
        assert (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_ABORTED").is_file()

        # Check that the abort trigger has been removed
        assert not abort_trigger.is_file()

        expected_commands = [
            f"{shutil.which('touch')} Test/job001/1.out",
            f"{shutil.which('sleep')} 10",
            f"{shutil.which('touch')} Test/job001/test_file.txt",
        ]

        assert (job_dir / "note.txt").read_text() == textwrap.dedent(
            f"""\
             ++++ CCP-EM Pipeliner running job Test/job001/ at mock time 1
             ++++ with the following commands:
            {expected_commands[0]}
            {expected_commands[1]}
            {expected_commands[2]}
             ++++
            """
        )
        assert captured_stdout == textwrap.dedent(
            f"""\
            mock time 2: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
            mock time 3: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
            mock time 4: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
            mock time 5: CCP-EM Pipeliner job Test/job001/: Found abort trigger file, aborting job
            mock time 6: CCP-EM Pipeliner job Test/job001/: Terminating command 2
            mock time 7: CCP-EM Pipeliner job Test/job001/: Command 2 terminated
            mock time 8: CCP-EM Pipeliner job Test/job001/: Job aborted
            """  # noqa: E501
        )
        assert (job_dir / "run.out").read_text() == textwrap.dedent(
            f"""\
            mock time 2: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
            mock time 3: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
            mock time 4: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
            mock time 5: CCP-EM Pipeliner job Test/job001/: Found abort trigger file, aborting job
            mock time 6: CCP-EM Pipeliner job Test/job001/: Terminating command 2
            mock time 7: CCP-EM Pipeliner job Test/job001/: Command 2 terminated
            mock time 8: CCP-EM Pipeliner job Test/job001/: Job aborted
            """  # noqa: E501
        )
        assert (job_dir / "run.err").read_text() == ""
        assert not (job_dir / "test_file.txt").exists()

    @staticmethod
    def test_full_run_with_abort_when_command_ignores_sigterm():
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)

        first_command_output_file = job_dir / "1.out"

        job = DummyJob(
            commands=[
                PipelinerCommand(["touch", os.fspath(first_command_output_file)]),
                PipelinerCommand(["bash", "-c", 'trap "" TERM; sleep 30']),
                PipelinerCommand(["touch", "Test/job001/test_file.txt"]),
            ]
        )
        job.output_dir = os.fspath(job_dir) + "/"
        runner = job_runner.JobRunner(job, abort_poll_interval=0.1)

        abort_trigger = job_dir / "PIPELINER_JOB_ABORT_NOW"

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_running_thread = threading.Thread(
                target=runner.run_job,
                daemon=True,
            )
            assert not first_command_output_file.is_file()
            job_running_thread.start()

            try:
                # Wait for the file from the first command to appear
                wait_count = 0
                while not first_command_output_file.is_file():
                    time.sleep(0.05)
                    wait_count += 1
                    if wait_count > 100:
                        raise TimeoutError("Timed out waiting for file to appear")
                # Wait a short extra time to ensure the second command is running
                time.sleep(0.1)
                abort_trigger.touch()
            finally:
                job_running_thread.join(timeout=30.0)
                assert not job_running_thread.is_alive(), "join() timed out"

        captured_stdout = redirected_stdout.getvalue()

        assert first_command_output_file.is_file()
        assert (job_dir / "run.out").is_file()
        assert (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_ABORTED").is_file()

        # Check that the abort trigger has been removed
        assert not abort_trigger.is_file()

        expected_commands = [
            f"{shutil.which('touch')} Test/job001/1.out",
            f"{shutil.which('bash')} -c 'trap \"\" TERM; sleep 30'",
            f"{shutil.which('touch')} Test/job001/test_file.txt",
        ]

        assert (job_dir / "note.txt").read_text() == textwrap.dedent(
            f"""\
             ++++ CCP-EM Pipeliner running job Test/job001/ at mock time 1
             ++++ with the following commands:
            {expected_commands[0]}
            {expected_commands[1]}
            {expected_commands[2]}
             ++++
            """
        )
        assert captured_stdout == textwrap.dedent(
            f"""\
            mock time 2: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
            mock time 3: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
            mock time 4: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
            mock time 5: CCP-EM Pipeliner job Test/job001/: Found abort trigger file, aborting job
            mock time 6: CCP-EM Pipeliner job Test/job001/: Terminating command 2
            mock time 7: CCP-EM Pipeliner job Test/job001/: Timed out waiting for process to stop. Killing command 2
            mock time 8: CCP-EM Pipeliner job Test/job001/: Command 2 killed
            mock time 9: CCP-EM Pipeliner job Test/job001/: Job aborted
            """  # noqa: E501
        )
        assert (job_dir / "run.out").read_text() == textwrap.dedent(
            f"""\
            mock time 2: CCP-EM Pipeliner job Test/job001/: Running command 1 of 3: {expected_commands[0]}
            mock time 3: CCP-EM Pipeliner job Test/job001/: Command 1 finished successfully
            mock time 4: CCP-EM Pipeliner job Test/job001/: Running command 2 of 3: {expected_commands[1]}
            mock time 5: CCP-EM Pipeliner job Test/job001/: Found abort trigger file, aborting job
            mock time 6: CCP-EM Pipeliner job Test/job001/: Terminating command 2
            mock time 7: CCP-EM Pipeliner job Test/job001/: Timed out waiting for process to stop. Killing command 2
            mock time 8: CCP-EM Pipeliner job Test/job001/: Command 2 killed
            mock time 9: CCP-EM Pipeliner job Test/job001/: Job aborted
            """  # noqa: E501
        )
        assert (job_dir / "run.err").read_text() == ""
        assert not (job_dir / "test_file.txt").exists()

    @staticmethod
    def test_abort_trigger_removed_on_job_success():
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)

        abort_trigger = job_dir / "PIPELINER_JOB_ABORT_NOW"

        job = DummyJob()
        job.output_dir = os.fspath(job_dir) + "/"

        # Create the abort trigger during create_post_run_output_nodes, when it is too
        # late to respond and the job will succeed anyway.
        job.create_post_run_output_nodes = abort_trigger.touch

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_runner.JobRunner(job).run_job()
        _ = redirected_stdout.getvalue()

        assert (job_dir / "run.out").is_file()
        assert (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_ABORTED").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()

        # Check that the abort trigger has been removed
        assert not abort_trigger.is_file()

    @staticmethod
    def test_output_nodes_added_in_post_run_node_creation():
        job_dir = Path("Test/job001")
        job_dir.mkdir(parents=True)

        # Make a job
        job = DummyJob()
        job.output_dir = os.fspath(job_dir)

        # Run the job
        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_runner.JobRunner(job).run_job()
        _ = redirected_stdout.getvalue()

        # Check that the node was added properly to the job pipeline
        assert (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        jdata = clean_starfile(job_dir / "job_pipeline.star")
        assert ["Test/job001/post_run.txt", "TestFile.txt.post_run.dummyjob"] in jdata

    def test_full_run_import_job_from_run_job_file(self):
        job_dir = Path("Import/job004")
        job_dir.mkdir(parents=True)
        job_file = job_dir / "run.job"
        shutil.copy(
            self.test_data / "JobFiles" / "Import" / "import_other.job", job_file
        )
        shutil.copy(self.test_data / "emd_3488.mrc", ".")

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_runner.main([os.fspath(job_file)])
        captured_stdout = redirected_stdout.getvalue()

        assert (job_dir / "run.out").is_file()
        assert (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert (job_dir / "emd_3488.mrc").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_ABORTED").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()

        expected_commands = [
            f"{shutil.which('cp')} emd_3488.mrc Import/job004/emd_3488.mrc",
        ]

        assert (job_dir / "note.txt").read_text() == textwrap.dedent(
            f"""\
             ++++ CCP-EM Pipeliner running job Import/job004/ at mock time 2
             ++++ with the following command:
            {expected_commands[0]}
             ++++
            """
        )
        assert captured_stdout == textwrap.dedent(
            f"""\
            mock time 1: CCP-EM Pipeliner: Loading job from Import/job004/run.job
            mock time 3: CCP-EM Pipeliner job Import/job004/: Running command 1 of 1: {expected_commands[0]}
            mock time 4: CCP-EM Pipeliner job Import/job004/: Command 1 finished successfully
            mock time 5: CCP-EM Pipeliner job Import/job004/: Job finished
            """  # noqa: E501
        )
        assert (job_dir / "run.out").read_text() == textwrap.dedent(
            f"""\
            mock time 3: CCP-EM Pipeliner job Import/job004/: Running command 1 of 1: {expected_commands[0]}
            mock time 4: CCP-EM Pipeliner job Import/job004/: Command 1 finished successfully
            mock time 5: CCP-EM Pipeliner job Import/job004/: Job finished
            """  # noqa: E501
        )
        assert (job_dir / "run.err").read_text() == ""

    def test_full_run_import_job_from_job_star_file(self):
        job_dir = Path("Import/job234")
        job_dir.mkdir(parents=True)
        job_file = job_dir / "job.star"
        shutil.copy(
            self.test_data / "JobFiles" / "Import" / "import_map_job.star", job_file
        )
        shutil.copy(self.test_data / "emd_3488.mrc", ".")

        with contextlib.redirect_stdout(io.StringIO()) as redirected_stdout:
            job_runner.main([os.fspath(job_file)])
        captured_stdout = redirected_stdout.getvalue()

        assert (job_dir / "run.out").is_file()
        assert (job_dir / "run.err").is_file()
        assert (job_dir / "note.txt").is_file()
        assert (job_dir / "emd_3488.mrc").is_file()
        assert (job_dir / "PIPELINER_JOB_EXIT_SUCCESS").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_ABORTED").is_file()
        assert not (job_dir / "PIPELINER_JOB_EXIT_FAILED").is_file()

        expected_commands = [
            f"{shutil.which('cp')} emd_3488.mrc Import/job234/emd_3488.mrc",
        ]

        assert (job_dir / "note.txt").read_text() == textwrap.dedent(
            f"""\
             ++++ CCP-EM Pipeliner running job Import/job234/ at mock time 2
             ++++ with the following command:
            {expected_commands[0]}
             ++++
            """
        )
        assert captured_stdout == textwrap.dedent(
            f"""\
            mock time 1: CCP-EM Pipeliner: Loading job from Import/job234/job.star
            mock time 3: CCP-EM Pipeliner job Import/job234/: Running command 1 of 1: {expected_commands[0]}
            mock time 4: CCP-EM Pipeliner job Import/job234/: Command 1 finished successfully
            mock time 5: CCP-EM Pipeliner job Import/job234/: Job finished
            """  # noqa: E501
        )
        assert (job_dir / "run.out").read_text() == textwrap.dedent(
            f"""\
            mock time 3: CCP-EM Pipeliner job Import/job234/: Running command 1 of 1: {expected_commands[0]}
            mock time 4: CCP-EM Pipeliner job Import/job234/: Command 1 finished successfully
            mock time 5: CCP-EM Pipeliner job Import/job234/: Job finished
            """  # noqa: E501
        )
        assert (job_dir / "run.err").read_text() == ""


if __name__ == "__main__":
    unittest.main()
