#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
from unittest.mock import patch

from pipeliner_tests import test_data
from pipeliner.job_factory import new_job_of_type

from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    expected_warning,
)
from pipeliner.data_structure import NODE_MASK3D, NODE_LOGFILE, NODE_DENSITYMAP


class ConfidenceMapTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="confidencemap")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch(
        "pipeliner.jobs.ccpem.confidence_map_job.ConfidenceMap.fdr_path",
        new="path/to/ccpem-python/lib/py2/FDRcontrol.py",
    )
    def test_get_command(self):
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/ConfidenceMap/FDR.job"),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "emd_3488_confidenceMap.mrc": f"{NODE_MASK3D}.mrc.confidencemap."
                "fdr_map",
                "diag_image.pdf": f"{NODE_LOGFILE}.pdf.confidencemap.noise_window",
            },
            expected_commands=[
                "sh -c 'echo \"backend: Agg\" > matplotlibrc'",
                "ccpem-python path/to/ccpem-python/lib/py2/FDRcontrol.py "
                "--em_map ../../Import/job001/emd_3488.mrc "
                "-method BH --testProc rightSided",
            ],
        )

    def test_confidence_map(self):
        job_running_test(
            test_jobfile=os.path.join(self.test_data, "JobFiles/ConfidenceMap/FDR.job"),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
            ],
            expected_outfiles=["run.out", "run.err", "emd_3488_confidenceMap.mrc"],
        )

    def test_create_results_display(self):
        # make a confidencemap job that looks like it has run
        conjob = new_job_of_type("confidencemap.map_analysis")
        jobname = "ConfidenceMap/job001/"
        conjob.output_dir = jobname
        conjob.joboptions["input_map"].value = "emd_3488.mrc"

        # make the output file
        mapfile = os.path.join(self.test_data, "emd_3488.mrc")
        os.makedirs(jobname)
        shutil.copy(mapfile, f"{jobname}emd_3488_confidenceMap.mrc")

        # create results and check
        with expected_warning(RuntimeWarning, "divide"):
            dispobjs = conjob.create_results_display()
        exp_mapmodel = {
            "maps": ["ConfidenceMap/job001/emd_3488_confidenceMap.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: 3D Confidence map",
            "maps_data": "ConfidenceMap/job001/emd_3488_confidenceMap.mrc",
            "models_data": "",
            "associated_data": ["ConfidenceMap/job001/emd_3488_confidenceMap.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        resfile = os.path.join(self.test_data, "ResultsFiles/confidence_map.json")
        with open(resfile, "r") as res:
            exp_montage = json.load(res)

        assert dispobjs[0].__dict__ == exp_montage
        assert dispobjs[1].__dict__ == exp_mapmodel

        assert os.path.isfile(dispobjs[0].img)


if __name__ == "__main__":
    unittest.main()
