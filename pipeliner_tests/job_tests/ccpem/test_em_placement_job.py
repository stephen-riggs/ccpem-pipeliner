#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from unittest.mock import patch

from pipeliner_tests import test_data

from pipeliner.job_factory import active_job_from_proc

from pipeliner.scripts.job_scripts.em_placement import (
    em_placement_config,
    write_seq_from_structure,
)
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.utils import get_python_command
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS, NODE_SEQUENCE


class EMPlacementTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="em_placement")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch("pipeliner.jobs.ccpem.em_placement_job.shutil.which")
    def test_get_command_em_placement(self, mock_which):
        mock_which.return_value = "/test/path/to/ccp4/bin/ccp4-python"
        config_script = em_placement_config.__file__
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/EMPlacement/em_placement_fit_model_job.star"
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
                "Import/job001/3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
                "Import/job002/5me2_a_to_move.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job003/3488_5me2.fasta": f"{NODE_SEQUENCE}.fasta",
            },
            output_nodes={},
            expected_commands=[
                f"{shlex.join(get_python_command())} {config_script} "
                " ../../Import/job001/3488_run_half1_class001_unfil.mrc"
                " ../../Import/job001/3488_run_half2_class001_unfil.mrc 3.2 None"
                " ../../Import/job003/3488_5me2.fasta 5me2_a_to_move"
                " ../../Import/job002/5me2_a_to_move.pdb 1.5 em_placement.phil",
                "/test/path/to/ccp4/libexec/phaser_voyager.em_placement "
                "em_placement.phil",
            ],
        )

    @patch("pipeliner.jobs.ccpem.em_placement_job.shutil.which")
    def test_get_command_em_placement_refstr(self, mock_which):
        mock_which.return_value = "/test/path/to/ccp4/bin/ccp4-python"
        config_script = em_placement_config.__file__
        write_seq_script = write_seq_from_structure.__file__
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/EMPlacement/em_placement_fit_model_refstr_job.star",
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
                "Import/job001/3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc.halfmap",
                "Import/job002/5me2_a_to_move.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job003/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes={},
            expected_commands=[
                f"{shlex.join(get_python_command())} {write_seq_script} "
                "-p ../../Import/job003/5ni1_updated.cif -mid 5ni1_updated",
                f"{shlex.join(get_python_command())} {config_script} "
                " ../../Import/job001/3488_run_half1_class001_unfil.mrc"
                " ../../Import/job001/3488_run_half2_class001_unfil.mrc 3.2 None"
                " 5ni1_updated_seq.fasta 5me2_a_to_move"
                " ../../Import/job002/5me2_a_to_move.pdb 1.5 em_placement.phil",
                "/test/path/to/ccp4/libexec/phaser_voyager.em_placement "
                "em_placement.phil",
            ],
        )

    def test_em_placement_fit_model(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/EMPlacement/em_placement_fit_model_job.star"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "5me2_a_to_move.pdb"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "3488_5me2.fasta"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "em_placement.phil",
            ],
            expected_search_files={
                "emplaced_files*/docked_model1.pdb": 1,
                "emplaced_files*/docked_model2.pdb": 1,
            },
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "em_placement best scoring model and map",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["EMPlacement/job998/emplaced_files/docked_model1.map"],
            "maps_opacity": [0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": ["EMPlacement/job998/emplaced_files/docked_model1.pdb"],
            "maps_data": "EMPlacement/job998/emplaced_files/docked_model1.map",
            "models_data": "EMPlacement/job998/emplaced_files/docked_model1.pdb",
            "associated_data": [
                "EMPlacement/job998/emplaced_files/docked_model1.map",
                "EMPlacement/job998/emplaced_files/docked_model1.pdb",
            ],
        }


if __name__ == "__main__":
    unittest.main()
