#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from unittest.mock import patch

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc

from pipeliner.scripts.job_scripts.em_placement import emplace_local_config
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.utils import get_python_command
from pipeliner.data_structure import NODE_DENSITYMAP, NODE_ATOMCOORDS


class EMPlacementTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="em_placement")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch("pipeliner.jobs.ccpem.emplace_local_job.which")
    def test_get_command_emplace_local(self, mock_which):
        """
        Local search mode of em_placement.  Searches in a sphere defined by centre
        of map coordinates and radius (both in angstroms)
        """
        mock_which.return_value = "/test/path/to/ccp4/bin/ccp4-python"

        config_script = emplace_local_config.__file__
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/EMPlace_local/emplace_local_atomic_model_fit_job.star",
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job002/3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job003/5me2_a_to_move.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={},
            expected_commands=[
                f"{shlex.join(get_python_command())} {config_script} "
                "None ../../Import/job001/3488_run_half1_class001_unfil.mrc "
                "../../Import/job002/3488_run_half2_class001_unfil.mrc 3.2 "
                "5me2_a_to_move ../../Import/job003/5me2_a_to_move.pdb 1.2 "
                "35.0 53.0 38.0 emplace_local.phil",
                "/test/path/to/ccp4/libexec/phaser_voyager.emplace_local "
                "emplace_local.phil",
            ],
        )

    def test_em_placement_local(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/EMPlace_local/emplace_local_atomic_model_fit_job.star",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "5me2_a_to_move.pdb"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "emplace_local.phil",
                "top_model_1.pdb",
            ],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "emplace_local best scoring model and map",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["EMPlacement/job998/top_model_1.map"],
            "maps_opacity": [0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": ["EMPlacement/job998/top_model_1.pdb"],
            "maps_data": "EMPlacement/job998/top_model_1.map",
            "models_data": "EMPlacement/job998/top_model_1.pdb",
            "associated_data": [
                "EMPlacement/job998/top_model_1.map",
                "EMPlacement/job998/top_model_1.pdb",
            ],
        }

    def test_script_writes_correct_philfile(self):
        expected = [
            "",
            "voyager",
            "{",
            "remove_phasertng_folder = True",
            "model_file = mmfile",
            "map = map",
            "map1 = half",
            "map2 = half2",
            "d_min = 1.0",
            "model_vrms = 2222",
            "sphere_center = 1,2,3",
            "biological_unit {",
            "molecule",
            "{",
            "molecule_name = molname",
            "}",
            "}",
            "}",
            "",
        ]

        emplace_local_config.main(
            [
                "map",
                "half",
                "half2",
                "1.0",
                "molname",
                "mmfile",
                "2222",
                "1",
                "2",
                "3",
                "outfile.phil",
            ]
        )
        with open("outfile.phil") as ofp:
            wrote = [x.strip() for x in ofp.readlines()]
        assert wrote == expected


if __name__ == "__main__":
    unittest.main()
