#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import mrcfile
import shlex

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner.scripts.job_scripts import model_to_map
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.utils import get_python_command
from pipeliner.data_structure import NODE_ATOMCOORDS, NODE_DENSITYMAP


class GemmiModelToMapTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="gemmi_model_to_map")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command(self):
        model_to_map_script = model_to_map.__file__
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Gemmi/gemmi_model_to_map.job"
            ),
            input_nodes={
                "Import/job001/5me2_a.pdb": "AtomCoords.pdb",
            },
            output_nodes={
                "gemmi_5me2_a.mrc": f"{NODE_DENSITYMAP}.mrc.simulated",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {model_to_map_script} "
                "--resolution 10.0 --pdb ../../Import/job001/5me2_a.pdb"
            ],
        )

    def test_get_command_ref_map(self):
        model_to_map_script = model_to_map.__file__
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Gemmi/gemmi_model_to_map_ref_map.job"
            ),
            input_nodes={
                "Import/job001/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job002/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "gemmi_5me2_a.mrc": f"{NODE_DENSITYMAP}.mrc.simulated",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {model_to_map_script} "
                "--resolution 10.0 --pdb ../../Import/job001/5me2_a.pdb"
                " --map_ref ../../Import/job002/emd_3488.mrc"
            ],
        )

    def test_get_command_set_dims(self):
        model_to_map_script = model_to_map.__file__
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Gemmi/gemmi_model_to_map_set_dims.job"
            ),
            input_nodes={
                "Import/job001/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "gemmi_5me2_a.mrc": f"{NODE_DENSITYMAP}.mrc.simulated",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {model_to_map_script} "
                "--resolution 10.0 --pdb ../../Import/job001/5me2_a.pdb"
                " --dim_x 101.11 --dim_y 103.33 --dim_z 105.55"
            ],
        )

    def test_model_to_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Gemmi/gemmi_model_to_map.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
            ],
            expected_outfiles=["run.out", "run.err", "gemmi_5me2_a.mrc"],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()

        assert dispobjs[1].__dict__ == {
            "maps": ["GemmiModelToMap/job998/gemmi_5me2_a.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["Import/job001/5me2_a.pdb"],
            "title": "3D viewer: Map from model",
            "maps_data": "GemmiModelToMap/job998/gemmi_5me2_a.mrc",
            "models_data": "Import/job001/5me2_a.pdb",
            "associated_data": [
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc",
                "Import/job001/5me2_a.pdb",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    def test_model_to_map_ref_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Gemmi/gemmi_model_to_map_ref_map.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
            ],
            expected_outfiles=["gemmi_5me2_a.mrc"],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Map slices: Map from model",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc: xy",
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc: xz",
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc: yz",
            ],
            "associated_data": ["GemmiModelToMap/job998/gemmi_5me2_a.mrc"],
            "img": "GemmiModelToMap/job998/Thumbnails/slices_montage_000.png",
        }

        assert dispobjs[1].__dict__ == {
            "maps": ["GemmiModelToMap/job998/gemmi_5me2_a.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["Import/job001/5me2_a.pdb"],
            "title": "3D viewer: Map from model",
            "maps_data": "GemmiModelToMap/job998/gemmi_5me2_a.mrc",
            "models_data": "Import/job001/5me2_a.pdb",
            "associated_data": [
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc",
                "Import/job001/5me2_a.pdb",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        # Check expected output map dimensions
        map_ref = "./Import/job002/emd_3488.mrc"
        with mrcfile.open(
            map_ref, mode="r", permissive=False, header_only=False
        ) as mrc:
            ref_dims = mrc.header.cella

        map_out = "./GemmiModelToMap/job998/gemmi_5me2_a.mrc"
        with mrcfile.open(
            map_out, mode="r", permissive=False, header_only=False
        ) as mrc:
            out_dims = mrc.header.cella
        assert ref_dims == out_dims

    def test_model_to_map_set_dmins(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Gemmi/gemmi_model_to_map_set_dims.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
            ],
            expected_outfiles=["run.out", "run.err"],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "Map slices: Map from model",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
            "labels": [
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc: xy",
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc: xz",
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc: yz",
            ],
            "associated_data": ["GemmiModelToMap/job998/gemmi_5me2_a.mrc"],
            "img": "GemmiModelToMap/job998/Thumbnails/slices_montage_000.png",
        }
        assert dispobjs[1].__dict__ == {
            "maps": ["GemmiModelToMap/job998/gemmi_5me2_a.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["Import/job001/5me2_a.pdb"],
            "title": "3D viewer: Map from model",
            "maps_data": "GemmiModelToMap/job998/gemmi_5me2_a.mrc",
            "models_data": "Import/job001/5me2_a.pdb",
            "associated_data": [
                "GemmiModelToMap/job998/gemmi_5me2_a.mrc",
                "Import/job001/5me2_a.pdb",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        # Check expected output map dimensions
        map_out = "./GemmiModelToMap/job998/gemmi_5me2_a.mrc"
        with mrcfile.open(
            map_out, mode="r", permissive=False, header_only=False
        ) as mrc:
            out_dims = mrc.header.cella
        assert round(float(out_dims.x), 2) == 101.11
        assert round(float(out_dims.y), 2) == 103.33
        assert round(float(out_dims.z), 2) == 105.55


if __name__ == "__main__":
    unittest.main()
