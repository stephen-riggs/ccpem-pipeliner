#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import math
from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc

from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.data_structure import NODE_ATOMCOORDS


class GemmiProcessModelTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="gemmi_process_model_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_gemmi_process_model(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_pdb_run.job",
            ),
            input_nodes={
                "Import/job001/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "5me2_a_gemmi_processed.cif": f"{NODE_ATOMCOORDS}.cif.gemmi."
                "process_model"
            },
            expected_commands=[
                (
                    "gemmi convert --remove-waters --shorten --style plain "
                    "../../Import/job001/5me2_a.pdb"
                    " 5me2_a_gemmi_processed.cif"
                ),
            ],
        )

    def test_get_command_gemmi_process_model_pdb2pdb(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_pdb2pdb_run.job",
            ),
            input_nodes={
                "Import/job001/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "5me2_a_gemmi_processed.pdb": f"{NODE_ATOMCOORDS}.pdb.gemmi."
                "process_model"
            },
            expected_commands=[
                (
                    "gemmi convert --remove-waters --shorten"
                    " ../../Import/job001/5me2_a.pdb"
                    " 5me2_a_gemmi_processed.pdb"
                ),
            ],
        )

    def test_get_command_gemmi_process_model_cif2cif(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_cif2cif_run.job",
            ),
            input_nodes={
                "Import/job001/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes={
                "5ni1_updated_gemmi_processed.cif": f"{NODE_ATOMCOORDS}.cif.gemmi."
                "process_model"
            },
            expected_commands=[
                (
                    "gemmi convert --remove-h --anisou no --remove-waters "
                    "--select //A,B --shorten --all-auth --sort --style aligned "
                    "../../Import/job001/5ni1_updated.cif"
                    " 5ni1_updated_gemmi_processed.cif"
                ),
            ],
        )

    def test_gemmi_process_model_pdb(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_pdb_run.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5me2_a.pdb"),
                ),
            ],
            expected_outfiles=["run.out", "run.err", "5me2_a_gemmi_processed.cif"],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "3D viewer: Overlaid models",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": [
                "Import/job001/5me2_a.pdb",
                "GemmiProcessModel/job998/5me2_a_gemmi_processed.cif",
            ],
            "maps_data": "",
            "models_data": (
                "Import/job001/5me2_a.pdb, "
                "GemmiProcessModel/job998/5me2_a_gemmi_processed.cif"
            ),
            "associated_data": [
                "Import/job001/5me2_a.pdb",
                "GemmiProcessModel/job998/5me2_a_gemmi_processed.cif",
            ],
        }

    def test_gemmi_process_model_cif(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_cif_run.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "5ni1_A.fasta"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "5ni1_updated_gemmi_processed.pdb",
            ],
        )

        job = active_job_from_proc(proc)
        assert math.isclose(
            os.stat(
                os.path.join(
                    self.test_dir,
                    "GemmiProcessModel",
                    "job998",
                    "5ni1_updated_gemmi_processed.pdb",
                )
            ).st_size,
            192294,
            rel_tol=0.01,
        )
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "3D viewer: Overlaid models",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": [
                "Import/job001/5ni1_updated.cif",
                "GemmiProcessModel/job998/5ni1_updated_gemmi_processed.pdb",
            ],
            "maps_data": "",
            "models_data": (
                "Import/job001/5ni1_updated.cif, "
                "GemmiProcessModel/job998/5ni1_updated_gemmi_processed.pdb"
            ),
            "associated_data": [
                "Import/job001/5ni1_updated.cif",
                "GemmiProcessModel/job998/5ni1_updated_gemmi_processed.pdb",
            ],
        }

    def test_gemmi_convert_cif2cif(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_cif2cif_run.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "5ni1_updated_gemmi_processed.cif",
            ],
        )

        job = active_job_from_proc(proc)
        assert math.isclose(
            os.stat(
                os.path.join(
                    self.test_dir,
                    "GemmiProcessModel",
                    "job998",
                    "5ni1_updated_gemmi_processed.cif",
                )
            ).st_size,
            196426,
            rel_tol=0.01,
        )
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "3D viewer: Overlaid models",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": [
                "Import/job001/5ni1_updated.cif",
                "GemmiProcessModel/job998/5ni1_updated_gemmi_processed.cif",
            ],
            "maps_data": "",
            "models_data": (
                "Import/job001/5ni1_updated.cif, "
                "GemmiProcessModel/job998/5ni1_updated_gemmi_processed.cif"
            ),
            "associated_data": [
                "Import/job001/5ni1_updated.cif",
                "GemmiProcessModel/job998/5ni1_updated_gemmi_processed.cif",
            ],
        }

    def test_gemmi_convertonly_cif(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/Gemmi/gemmi_model_utilities_convert_cif_run_convertonly.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                )
            ],
            expected_outfiles=["run.out", "run.err", "5ni1_processed.pdb"],
        )

        job = active_job_from_proc(proc)
        assert math.isclose(
            os.stat(
                os.path.join(
                    self.test_dir, "GemmiProcessModel", "job998", "5ni1_processed.pdb"
                )
            ).st_size,
            381024,
            rel_tol=0.01,
        )
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "3D viewer: Model: GemmiProcessModel/job998/5ni1_processed.pdb",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": [
                "GemmiProcessModel/job998/5ni1_processed.pdb",
            ],
            "maps_data": "",
            "models_data": ("GemmiProcessModel/job998/5ni1_processed.pdb"),
            "associated_data": [
                "GemmiProcessModel/job998/5ni1_processed.pdb",
            ],
        }


if __name__ == "__main__":
    unittest.main()
