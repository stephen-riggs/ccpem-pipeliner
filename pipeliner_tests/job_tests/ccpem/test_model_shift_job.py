#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from pipeliner_tests import test_data

from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import run_subprocess, get_python_command
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.scripts.job_scripts import shift_model_refmap_origin
import gemmi


class ModelShiftTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="model-shift")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_shift_refmap_origin(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/ModelShift/refmap_origin.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "6vyb_Ndom.cif"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_21457_seg.mrc"),
                ),
            ],
            expected_outfiles=["run.out", "run.err", "6vyb_Ndom_shifted.cif"],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        output_modelfile = os.path.join(
            self.test_dir,
            "ModelShift",
            "job998",
            "6vyb_Ndom_shifted.cif",
        )
        # check output size
        assert os.path.isfile(output_modelfile)
        # compare output model atom coordinate
        out_atom = gemmi.read_structure(output_modelfile)[0]["A"]["194"]["PHE"]["CA"][0]
        out_coord = out_atom.pos.tolist()
        ref_model = os.path.join(self.test_data, "6vyb_Ndom_shifted.pdb")
        ref_atom = gemmi.read_structure(ref_model)[0]["A"]["194"]["PHE"]["CA"][0]
        ref_coord = ref_atom.pos.tolist()
        assert out_coord == ref_coord

        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "3D viewer: Overlaid map: Import/job002/emd_21457_seg.mrc model: "
            "ModelShift/job998/6vyb_Ndom_shifted.cif",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["Import/job002/emd_21457_seg.mrc"],
            "maps_opacity": [0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": ["ModelShift/job998/6vyb_Ndom_shifted.cif"],
            "maps_data": "Import/job002/emd_21457_seg.mrc",
            "models_data": "ModelShift/job998/6vyb_Ndom_shifted.cif",
            "associated_data": [
                "Import/job002/emd_21457_seg.mrc",
                "ModelShift/job998/6vyb_Ndom_shifted.cif",
            ],
        }

    def test_translate_vector(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data,
                "JobFiles/ModelShift/translate_coord.job",
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "6vyb_Ndom.cif"),
                )
            ],
            expected_outfiles=["run.out", "run.err", "6vyb_Ndom_shifted.cif"],
            print_err=True,
            show_contents=True,
        )

        job = active_job_from_proc(proc)
        output_modelfile = os.path.join(
            self.test_dir,
            "ModelShift",
            "job998",
            "6vyb_Ndom_shifted.cif",
        )
        # check output size
        assert os.path.isfile(output_modelfile)
        # compare output model atom coordinate
        out_atom = gemmi.read_structure(output_modelfile)[0]["A"]["194"]["PHE"]["CA"][0]
        out_coord = out_atom.pos.tolist()
        inp_model = os.path.join(self.test_data, "6vyb_Ndom.cif")
        inp_atom = gemmi.read_structure(inp_model)[0]["A"]["194"]["PHE"]["CA"][0]
        inp_coord = inp_atom.pos.tolist()
        assert out_coord == [
            inp_coord[0] + 10.0,
            inp_coord[1] + 5.0,
            inp_coord[2] + 10.0,
        ]

        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "3D viewer: Model: ModelShift/job998/6vyb_Ndom_shifted.cif",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": [],
            "maps_opacity": [],
            "maps_colours": [],
            "models_colours": [],
            "models": ["ModelShift/job998/6vyb_Ndom_shifted.cif"],
            "maps_data": "",
            "models_data": "ModelShift/job998/6vyb_Ndom_shifted.cif",
            "associated_data": ["ModelShift/job998/6vyb_Ndom_shifted.cif"],
        }

    def test_script_translate(self):
        input_model = os.path.join(self.test_data, "6vyb_Ndom.cif")
        run_subprocess(
            [
                get_python_command()[0],
                os.path.realpath(shift_model_refmap_origin.__file__),
                "--model",
                input_model,
                "--tx",
                "10.0",
                "--ty",
                "5.0",
                "--tz",
                "10.0",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("6vyb_Ndom_shifted.cif")
        # compare output model atom coordinate
        out_atom = gemmi.read_structure("6vyb_Ndom_shifted.cif")[0]["A"]["194"]["PHE"][
            "CA"
        ][0]
        out_coord = out_atom.pos.tolist()
        inp_model = os.path.join(self.test_data, "6vyb_Ndom.cif")
        inp_atom = gemmi.read_structure(inp_model)[0]["A"]["194"]["PHE"]["CA"][0]
        inp_coord = inp_atom.pos.tolist()
        assert out_coord == [
            inp_coord[0] + 10.0,
            inp_coord[1] + 5.0,
            inp_coord[2] + 10.0,
        ]

    def test_script_refmap_nstart(self):
        input_model = os.path.join(self.test_data, "6vyb_Ndom.cif")
        reference_map = os.path.join(self.test_data, "emd_21457_seg_nstart.mrc")
        run_subprocess(
            [
                get_python_command()[0],
                os.path.realpath(shift_model_refmap_origin.__file__),
                "--model",
                input_model,
                "--use_record",
                "nstart",
                "--refmap",
                reference_map,
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("6vyb_Ndom_shifted.cif")
        # compare output model atom coordinate
        out_atom = gemmi.read_structure("6vyb_Ndom_shifted.cif")[0]["A"]["194"]["PHE"][
            "CA"
        ][0]
        out_coord = out_atom.pos.tolist()
        ref_model = os.path.join(self.test_data, "6vyb_Ndom_shifted.pdb")
        ref_atom = gemmi.read_structure(ref_model)[0]["A"]["194"]["PHE"]["CA"][0]
        ref_coord = ref_atom.pos.tolist()
        assert out_coord == ref_coord

    def test_script_refmap_shift_back(self):
        input_model = os.path.join(self.test_data, "6vyb_Ndom_shifted.pdb")
        reference_map = os.path.join(self.test_data, "emd_21457_seg.mrc")
        run_subprocess(
            [
                get_python_command()[0],
                os.path.realpath(shift_model_refmap_origin.__file__),
                "--model",
                input_model,
                "--use_record",
                "origin",
                "--refmap",
                reference_map,
                "--fitted_zero",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("6vyb_Ndom_shifted_shifted.pdb")
        # compare output model atom coordinate
        out_atom = gemmi.read_structure("6vyb_Ndom_shifted_shifted.pdb")[0]["A"]["194"][
            "PHE"
        ]["CA"][0]
        out_coord = out_atom.pos.tolist()
        ref_model = os.path.join(self.test_data, "6vyb_Ndom.cif")
        ref_atom = gemmi.read_structure(ref_model)[0]["A"]["194"]["PHE"]["CA"][0]
        ref_coord = ref_atom.pos.tolist()
        assert out_coord == ref_coord

    def test_get_command_refmap_origin_shift_back(self):
        exp_out_nodes = {
            "6vyb_Ndom_shifted_shifted.pdb": "AtomCoords.pdb.shifted."
            "emd_21457_seg_origin_nonzero",
        }
        exp_coms = []
        input_models = [
            "../../Import/job001/6vyb_Ndom_shifted.pdb",
        ]
        refmap = "../../Import/job002/emd_21457_seg.mrc"
        for input_model in input_models:
            exp_coms += [
                f"{shlex.join(get_python_command())} "
                f"{os.path.realpath(shift_model_refmap_origin.__file__)} "
                f"--model {input_model} --refmap {refmap} --use_record origin "
                "--fitted_zero --ofile 6vyb_Ndom_shifted_shifted.pdb",
            ]

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ModelShift/refmap_shift_back.job"
            ),
            input_nodes={
                "Import/job001/6vyb_Ndom_shifted.pdb": "AtomCoords.pdb",
                "Import/job002/emd_21457_seg.mrc": "DensityMap.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_refmap_nstart(self):
        exp_out_nodes = {
            "6vyb_Ndom_shifted.cif": "AtomCoords.cif.shifted."
            "emd_21457_seg_nstart_nstart",
        }
        exp_coms = []
        input_models = [
            "../../Import/job001/6vyb_Ndom.cif",
        ]
        refmap = "../../Import/job002/emd_21457_seg_nstart.mrc"
        for input_model in input_models:
            exp_coms += [
                f"{shlex.join(get_python_command())} "
                f"{os.path.realpath(shift_model_refmap_origin.__file__)} "
                f"--model {input_model} --refmap {refmap} --use_record nstart --ofile "
                "6vyb_Ndom_shifted.cif",
            ]

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ModelShift/refmap_nstart.job"
            ),
            input_nodes={
                "Import/job001/6vyb_Ndom.cif": "AtomCoords.cif",
                "Import/job002/emd_21457_seg_nstart.mrc": "DensityMap.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_translate_vector(self):
        exp_out_nodes = {"6vyb_Ndom_shifted.cif": "AtomCoords.cif.shifted"}
        exp_coms = []
        input_models = [
            "../../Import/job001/6vyb_Ndom.cif",
        ]
        for input_model in input_models:
            exp_coms += [
                f"{shlex.join(get_python_command())} "
                f"{os.path.realpath(shift_model_refmap_origin.__file__)} "
                f"--model {input_model} --tx 10.0 --ty 5.0 --tz 10.0 --ofile "
                "6vyb_Ndom_shifted.cif",
            ]

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ModelShift/translate_coord.job"
            ),
            input_nodes={
                "Import/job001/6vyb_Ndom.cif": "AtomCoords.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_multi(self):
        exp_out_nodes = {
            "6vyb_Ndom_shifted.cif": "AtomCoords.cif.shifted.emd_21457_seg_origin",
            "6vyb_Ndom_shifted.pdb": "AtomCoords.pdb.shifted.emd_21457_seg_origin",
        }
        exp_coms = []
        input_models = [
            "../../Import/job001/6vyb_Ndom.cif",
            "../../Import/job002/6vyb_Ndom.pdb",
        ]
        refmap = "../../Import/job003/emd_21457_seg.mrc"
        for input_model in input_models:
            modelid, ext = os.path.splitext(os.path.basename(input_model))
            out_model = modelid + "_shifted" + ext
            exp_coms += [
                f"{shlex.join(get_python_command())} "
                f"{os.path.realpath(shift_model_refmap_origin.__file__)} "
                f"--model {input_model} --refmap {refmap} --use_record origin --ofile "
                f"{out_model}",
            ]

        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/ModelShift/multi_shift.job"),
            input_nodes={
                "Import/job001/6vyb_Ndom.cif": "AtomCoords.cif",
                "Import/job002/6vyb_Ndom.pdb": "AtomCoords.pdb",
                "Import/job003/emd_21457_seg.mrc": "DensityMap.mrc",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )


if __name__ == "__main__":
    unittest.main()
