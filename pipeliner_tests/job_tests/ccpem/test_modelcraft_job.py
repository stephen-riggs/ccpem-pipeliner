#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    slow_test,
)
from pipeliner.user_settings import get_modelcraft_executable
from pipeliner.data_structure import (
    NODE_SEQUENCE,
    NODE_DENSITYMAP,
    NODE_MASK3D,
    NODE_ATOMCOORDS,
)


class ModelCraftJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="modelcraft")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_modelcraft_half_maps(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ModelCraft/build_with_half_maps.job"
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job002/3488_run_half2_class001_unfil.mrc": f"{NODE_DENSITYMAP}"
                ".mrc",
                "Import/job003/5ni1_mod.fasta": f"{NODE_SEQUENCE}.fasta",
                "Import/job004/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "modelcraft.cif": f"{NODE_ATOMCOORDS}.cif.modelcraft",
            },
            expected_commands=[
                f"{get_modelcraft_executable()} em --contents"
                " ../../Import/job003/5ni1_mod.fasta"
                " --map ../../Import/job001/3488_run_half1_class001_unfil.mrc"
                " ../../Import/job002/3488_run_half2_class001_unfil.mrc"
                " --resolution 3.2 --cycles 1 --mask"
                " ../../Import/job004/emd_3488_mask.mrc"
                " --threads 1"
                " --keep-files"
                " --directory ."
                " --overwrite-directory"
            ],
            show_outputnodes=True,
        )

    def test_get_commands_modelcraft_full_map(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/ModelCraft/build_with_full_map.job",
            ),
            input_nodes={
                "Import/job003/5ni1_mod.fasta": f"{NODE_SEQUENCE}.fasta",
                "Import/job004/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
                "Import/job005/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "modelcraft.cif": f"{NODE_ATOMCOORDS}.cif.modelcraft",
            },
            expected_commands=[
                f"{get_modelcraft_executable()} em --contents"
                " ../../Import/job003/5ni1_mod.fasta"
                " --map ../../Import/job005/emd_3488.mrc"
                " --resolution 3.2 --cycles 1 --mask"
                " ../../Import/job004/emd_3488_mask.mrc"
                " --threads 1"
                " --keep-files"
                " --directory ."
                " --overwrite-directory"
            ],
            show_outputnodes=True,
        )

    def test_get_commands_modelcraft_auto_stop_cycle(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/ModelCraft/modelcraft_check_auto_stop_cycles.job",
            ),
            input_nodes={
                "Import/job001/3488_run_half1_class001_unfil."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/3488_run_half2_class001_unfil."
                "mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job003/5ni1_mod.fasta": f"{NODE_SEQUENCE}.fasta",
                "Import/job004/emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc",
            },
            output_nodes={
                "modelcraft.cif": "AtomCoords.cif.modelcraft",
            },
            expected_commands=[
                f"{get_modelcraft_executable()} em --contents"
                " ../../Import/job003/5ni1_mod.fasta"
                " --map ../../Import/job001/3488_run_half1_class001_unfil.mrc"
                " ../../Import/job002/3488_run_half2_class001_unfil.mrc"
                " --resolution 3.2 --cycles 1 --mask"
                " ../../Import/job004/emd_3488_mask.mrc"
                " --auto-stop-cycles 3"
                " --threads 1"
                " --keep-files"
                " --directory ."
                " --overwrite-directory"
            ],
            show_outputnodes=True,
        )

    @slow_test
    def test_build_model_half_maps(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/ModelCraft/build_with_half_maps.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "5ni1_mod.fasta"),
                ),
                (
                    "Import/job004",
                    os.path.join(self.test_data, "emd_3488_mask.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "job_1_servalcat/map0_trimmed.mrc",
                "job_1_servalcat/map1_trimmed.mrc",
                "job_2_servalcat/nemap_normalized_fo.mrc",
                "job_3_cbuccaneer/xyzout.cif",
                "modelcraft.cif",
                "modelcraft.json",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Residues built and FSC average for modelcraft.cif",
            "start_collapsed": False,
            "dobj_type": "table",
            "flag": "",
            "header_tooltips": ["Residues built", "FSC average"],
            "headers": ["Residues built", "FSC average"],
            "table_data": [[578, 0.7868]],
            "associated_data": ["ModelCraft/job998/modelcraft.json"],
        }
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Overlaid map: Import/job001/3488_run_half1_"
            "class001_unfil.mrc "
            "model: ModelCraft/job998/modelcraft.cif",
            "maps": ["Import/job001/3488_run_half1_class001_unfil.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["ModelCraft/job998/modelcraft.cif"],
            "maps_data": "Import/job001/3488_run_half1_class001_unfil.mrc",
            "models_data": "ModelCraft/job998/modelcraft.cif",
            "associated_data": [
                "Import/job001/3488_run_half1_class001_unfil.mrc",
                "ModelCraft/job998/modelcraft.cif",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @slow_test
    def test_build_model_full_map(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/ModelCraft/build_with_full_map.job"
            ),
            input_files=[
                (
                    "Import/job003",
                    os.path.join(self.test_data, "5ni1_mod.fasta"),
                ),
                (
                    "Import/job004",
                    os.path.join(self.test_data, "emd_3488_mask.mrc"),
                ),
                (
                    "Import/job005",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "job_1_servalcat/map0_trimmed.mrc",
                "job_2_refmacat/mapin.ccp4",
                "job_3_cbuccaneer/xyzout.cif",
                "modelcraft.cif",
                "modelcraft.json",
            ],
        )
        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Residues built and FSC average for modelcraft.cif",
            "start_collapsed": False,
            "dobj_type": "table",
            "flag": "",
            "header_tooltips": ["Residues built", "FSC average"],
            "headers": ["Residues built", "FSC average"],
            "table_data": [[671, 0.6091]],
            "associated_data": ["ModelCraft/job998/modelcraft.json"],
        }
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Overlaid map: Import/job005/emd_3488.mrc model: "
            "ModelCraft/job998/modelcraft.cif",
            "maps": ["Import/job005/emd_3488.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["ModelCraft/job998/modelcraft.cif"],
            "maps_data": "Import/job005/emd_3488.mrc",
            "models_data": "ModelCraft/job998/modelcraft.cif",
            "associated_data": [
                "Import/job005/emd_3488.mrc",
                "ModelCraft/job998/modelcraft.cif",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }


# class BuccaneerJobTest(unittest.TestCase):
#    def setUp(self):
#        """
#        Setup test data and output directories.
#        """
#        self.test_data = os.path.dirname(test_data.__file__)
#        #self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
#
#        # Change to test directory
#        self._orig_dir = os.getcwd()
#        os.chdir(self.test_dir)
#
#    def tearDown(self):
#        os.chdir(self._orig_dir)
#        if os.path.exists(self.test_dir):
#            shutil.rmtree(self.test_dir)
#
#    def test_get_buccaneer_commands(self):
#        os.makedirs("sequences")
#        touch(os.path.join(self.test_dir, "sequences/5ni1.fasta"))
#        os.makedirs("PostProcess/job010/")
#        shutil.copy(
#            os.path.join(self.test_data, "emd_3488.mrc"),
#            os.path.join(self.test_dir, "PostProcess/job010/postprocess_masked.mrc"),
#        )
#        script_path = make_buccaneer_args_file.__file__
#        general_get_command_test(
#            jobtype="Buccaneer",
#            jobfile="buccaneer_job.star",
#            jobnumber=1502,
#            input_nodes={
#                "PostProcess/job010/postprocess_masked.mrc": f"{NODE_DENSITYMAP}.mrc",
#                "sequences/5ni1.fasta": f"{NODE_SEQUENCE}.fasta",
#            },
#            output_nodes={"refined1.pdb": "AtomCoords.pdb.buccaneer"},
#            expected_commands=[
#                f"python3 {script_path} --job_title 'RELION pipeline test with"
#                " EMD-3488' --input_map"
#                f" {os.path.abspath('PostProcess/job010/postprocess_masked.mrc')}"
#                " --resolution 3.3 --input_seq"
#                f" {os.path.abspath('sequences/5ni1.fasta')}"
#                " --extend_pdb None --ncycle 1 --ncycle_refmac 20 --ncycle_buc1st"
#                " 1 --ncycle_bucnth 1 --map_sharpen 0.0 --ncpus 1 --lib_in None"
#                " --keywords '' --refmac_keywords ''",
#                "ccpem-buccaneer --no-gui --args tmp_buccaneer_args.json"
#                " --job_location Buccaneer/job1502/",
#                "rm tmp_buccaneer_args.json",
#            ],
#        )
#
#    def test_make_results_display(self):
#        # make it look like a buccaneer job has run
#        os.makedirs("ModelBuild/job001")
#        touch("ModelBuild/job001/refined1.pdb")
#        write_default_jobstar(
#            "buccaneer.atomic_model_build", "ModelBuild/job001/job.star"
#        )
#        pipelinefile = os.path.join(self.test_data, "Pipelines/single_buccaneer.star")
#        shutil.copy(pipelinefile, "default_pipeline.star")
#        mapfile = os.path.join(self.test_data, "emd_3488.mrc")
#        shutil.copy(mapfile, "map1.mrc")
#
#        # set up the pipeline and get the job
#        with ProjectGraph() as pipeline:
#            buc_proc = pipeline.find_process("ModelBuild/job001/")
#
#        buc_job = active_job_from_proc(buc_proc)
#        buc_job.joboptions["input_map"].value = "map1.mrc"
#
#        dispobjs = buc_job.create_results_display()
#        print(dispobjs[0].__dict__)
#        assert dispobjs[0].__dict__ == {
#            "maps": ["map1.mrc"],
#            "dobj_type": "mapmodel",
#            "maps_opacity": [0.5],
#            "models": ["ModelBuild/job001/refined1.pdb"],
#            "title": "3D viewer: Overlaid map: map1.mrc model: ModelBuild/job001/"
#            "refined1.pdb",
#            "maps_data": "map1.mrc",
#            "models_data": "ModelBuild/job001/refined1.pdb",
#            "associated_data": ["map1.mrc", "ModelBuild/job001/refined1.pdb"],
#            "start_collapsed": True,
#            "flag": "",
#            "maps_colours": [],
#            "models_colours": [],
#        }


if __name__ == "__main__":
    unittest.main()
