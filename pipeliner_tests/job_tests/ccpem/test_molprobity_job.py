#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import math
import shlex

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import run_subprocess, get_python_command
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    slow_test,
)
from pipeliner.scripts.job_scripts.model_validation import get_molprobity_results
from pipeliner.data_structure import NODE_ATOMCOORDS, NODE_LOGFILE

get_molp_results_path = os.path.realpath(get_molprobity_results.__file__)


class MolprobityTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="molprobity-validation")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_molprobity_results(self):
        molp_out = os.path.join(self.test_data, "molprobity_5fj8.out")
        run_subprocess(
            [
                get_python_command()[0],
                os.path.realpath(get_molprobity_results.__file__),
                "-molp",
                molp_out,
                "-id",
                "5fj8",
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile("5fj8_molprobity_summary.json")
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "5fj8_molprobity_summary.json")
            ).st_size,
            767,
            rel_tol=0.05,
        )
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "5fj8_residue_molprobity_outliers.json")
            ).st_size,
            6872,
            rel_tol=0.05,
        )
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "5fj8_molprobity_outliers.json")
            ).st_size,
            4338,
            rel_tol=0.05,
        )

    def test_get_command_molprobity_global(self):
        exp_out_nodes = {
            "5me2_a_molprobity.out": f"{NODE_LOGFILE}.txt.molprobity.output"
        }
        exp_coms = []
        exp_coms += [
            "cp ../../Import/job001/5me2_a.pdb 5me2_a_reduce_out.pdb",
            "sh -c 'molprobity.reduce -FLIP -Quiet ../../Import/job001/5me2_a.pdb >"
            " 5me2_a_reduce_out.pdb'",
            "molprobity.molprobity 5me2_a_reduce_out.pdb output.percentiles=True",
            "mv molprobity.out 5me2_a_molprobity.out",
            "mv molprobity_coot.py 5me2_a_molprobity_coot.py",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 5me2_a -molp 5me2_a_molprobity.out -cootscript"
            " 5me2_a_molprobity_coot.py",
        ]

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Molprobity/validate_global.job"
            ),
            input_nodes={
                "Import/job001/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_molprobity_clash(self):
        exp_out_nodes = {
            "5ni1_updated_clashscore.out": f"{NODE_LOGFILE}.txt.clashscore.output"
        }
        exp_coms = []
        exp_coms += [
            "gemmi convert --shorten ../../Import/job001/5ni1_updated.cif "
            "5ni1_updated.pdb"
        ]
        exp_out_nodes["5ni1_updated.pdb"] = f"{NODE_ATOMCOORDS}.pdb.gemmi.pdbconvert"
        exp_coms += [
            "sh -c 'molprobity.clashscore 5ni1_updated.pdb > "
            "5ni1_updated_clashscore.out'",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 5ni1_updated -clash 5ni1_updated_clashscore.out",
        ]

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Molprobity/validate_clash.job"
            ),
            input_nodes={
                "Import/job001/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_molprobity_rama_rota(self):
        exp_out_nodes = {}
        exp_coms = []
        exp_coms += [
            "gemmi convert --shorten ../../Import/job001/5ni1_updated.cif "
            "5ni1_updated.pdb"
        ]
        exp_out_nodes["5ni1_updated.pdb"] = f"{NODE_ATOMCOORDS}.pdb.gemmi.pdbconvert"
        exp_coms += [
            "sh -c 'molprobity.ramalyze 5ni1_updated.pdb > 5ni1_updated_ramalyze.out'",
            "sh -c 'molprobity.rotalyze 5ni1_updated.pdb > 5ni1_updated_rotalyze.out'",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 5ni1_updated -rama 5ni1_updated_ramalyze.out "
            "-rota 5ni1_updated_rotalyze.out",
        ]
        exp_out_nodes["5ni1_updated_ramalyze.out"] = (
            f"{NODE_LOGFILE}.txt.ramalyze.output"
        )
        exp_out_nodes["5ni1_updated_rotalyze.out"] = (
            f"{NODE_LOGFILE}.txt.rotalyze.output"
        )
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Molprobity/validate_rama_rota.job"
            ),
            input_nodes={
                "Import/job001/5ni1_updated.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    def test_get_command_global_multi(self):
        exp_out_nodes = {}
        exp_coms = []
        exp_coms += [
            "cp ../../Import/job001/5me2_a.pdb 5me2_a_reduce_out.pdb",
            "sh -c 'molprobity.reduce -FLIP -Quiet ../../Import/job001/5me2_a.pdb >"
            " 5me2_a_reduce_out.pdb'",
            "molprobity.molprobity 5me2_a_reduce_out.pdb output.percentiles=True",
            "mv molprobity.out 5me2_a_molprobity.out",
            "mv molprobity_coot.py 5me2_a_molprobity_coot.py",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 5me2_a -molp 5me2_a_molprobity.out -cootscript"
            " 5me2_a_molprobity_coot.py",
        ]
        exp_out_nodes["5me2_a_molprobity.out"] = f"{NODE_LOGFILE}.txt.molprobity.output"
        # 6yyt
        exp_coms += [
            "gemmi convert --shorten ../../Import/job002/6yyt_noA.cif 6yyt_noA.pdb"
        ]
        exp_out_nodes["6yyt_noA.pdb"] = f"{NODE_ATOMCOORDS}.pdb.gemmi.pdbconvert"
        exp_coms += [
            "cp 6yyt_noA.pdb 6yyt_noA_reduce_out.pdb",
            "sh -c 'molprobity.reduce -FLIP -Quiet 6yyt_noA.pdb >"
            " 6yyt_noA_reduce_out.pdb'",
            "molprobity.molprobity 6yyt_noA_reduce_out.pdb output.percentiles=True",
            "mv molprobity.out 6yyt_noA_molprobity.out",
            "mv molprobity_coot.py 6yyt_noA_molprobity_coot.py",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 6yyt_noA -molp 6yyt_noA_molprobity.out -cootscript"
            " 6yyt_noA_molprobity_coot.py",
        ]
        exp_out_nodes["6yyt_noA_molprobity.out"] = (
            f"{NODE_LOGFILE}.txt.molprobity.output"
        )
        # modelcraft
        exp_coms += [
            "gemmi convert --shorten ../../Import/job003/5ni1_modelcraft_10cyc.cif "
            "5ni1_modelcraft_10cyc.pdb"
        ]
        exp_out_nodes["5ni1_modelcraft_10cyc.pdb"] = (
            f"{NODE_ATOMCOORDS}.pdb.gemmi.pdbconvert"
        )
        exp_coms += [
            "cp 5ni1_modelcraft_10cyc.pdb 5ni1_modelcraft_10cyc_reduce_out.pdb",
            "sh -c 'molprobity.reduce -FLIP -Quiet 5ni1_modelcraft_10cyc.pdb"
            " > 5ni1_modelcraft_10cyc_reduce_out.pdb'",
            "molprobity.molprobity 5ni1_modelcraft_10cyc_reduce_out.pdb"
            " output.percentiles=True",
            "mv molprobity.out 5ni1_modelcraft_10cyc_molprobity.out",
            "mv molprobity_coot.py 5ni1_modelcraft_10cyc_molprobity_coot.py",
            f"{shlex.join(get_python_command())} {get_molp_results_path} "
            "-id 5ni1_modelcraft_10cyc -molp 5ni1_modelcraft_10cyc_molprobity.out"
            " -cootscript 5ni1_modelcraft_10cyc_molprobity_coot.py",
        ]
        exp_out_nodes["5ni1_modelcraft_10cyc_molprobity.out"] = (
            f"{NODE_LOGFILE}.txt.molprobity.output"
        )

        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Molprobity/validate_global_multi.job"
            ),
            input_nodes={
                "Import/job001/5me2_a.pdb": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job002/6yyt_noA.cif": f"{NODE_ATOMCOORDS}.cif",
                "Import/job003/5ni1_modelcraft_10cyc.cif": f"{NODE_ATOMCOORDS}.cif",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
            show_coms=True,
            show_outputnodes=True,
        )

    @slow_test
    def test_molprobity_global_multi(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molprobity/validate_global_multi.job"
            ),
            input_files=[
                ("Import/job001", os.path.join(self.test_data, "5me2_a.pdb")),
                ("Import/job002", os.path.join(self.test_data, "6yyt_noA.cif")),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "5ni1_modelcraft_10cyc.cif"),
                ),
            ],
            expected_outfiles=[
                "5me2_a_molprobity.out",
                "6yyt_noA_molprobity.out",
                "5ni1_modelcraft_10cyc_molprobity.out",
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # molp table
        mp_table = dispobjs[0].__dict__
        assert len(mp_table["table_data"]) == 14
        assert (
            mp_table["table_data"][0][0] == "Ramachandran_outliers"
            and mp_table["table_data"][0][1] == "0.00%"
        )

    def test_molprobity_clash_rama_rota_multi(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molprobity/validate_clash_rama_rota_multi.job"
            ),
            input_files=[
                ("Import/job001", os.path.join(self.test_data, "5ni1_updated.cif")),
                ("Import/job002", os.path.join(self.test_data, "6yyt_noA.cif")),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "5ni1_modelcraft_10cyc.cif"),
                ),
            ],
            expected_outfiles=[
                "5ni1_updated_clashscore.out",
                "6yyt_noA_clashscore.out",
                "5ni1_modelcraft_10cyc_clashscore.out",
                "5ni1_updated_ramalyze.out",
                "6yyt_noA_ramalyze.out",
                "5ni1_modelcraft_10cyc_ramalyze.out",
                "5ni1_updated_rotalyze.out",
                "6yyt_noA_rotalyze.out",
                "5ni1_modelcraft_10cyc_rotalyze.out",
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # molp table
        mp_table = dispobjs[0].__dict__
        assert len(mp_table["table_data"]) == 4
        assert (
            mp_table["table_data"][0][0] == "Ramachandran_outliers"
            and mp_table["table_data"][0][1] == "0.00%"
        )

    def test_molprobity_task_clashscore(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molprobity/validate_clash.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "5ni1_updated.cif"),
                )
            ],
            expected_outfiles=[
                "5ni1_updated_clashscore.out",
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        # molp table
        mp_table = dispobjs[0].__dict__
        assert len(mp_table["table_data"]) == 1
        assert (
            mp_table["table_data"][0][0] == "Clashscore"
            and mp_table["table_data"][0][1] == "3.53"
        )


if __name__ == "__main__":
    unittest.main()
