#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex

from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner.utils import get_job_script, get_python_command
from pipeliner.data_structure import NODE_ATOMCOORDS, NODE_DENSITYMAP
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    slow_test,
)

molrep_tools_script = get_job_script("molrep/molrep_tools.py")


class MolrepTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="molrep_")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_molrep(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_model.job"
            ),
            input_nodes={
                "Import/job001/1ake_10A_molrep.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/1AKE_cha_molrep_to_move.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "molrep.pdb": f"{NODE_ATOMCOORDS}.pdb.molrep",
                "molrep.cif": f"{NODE_ATOMCOORDS}.cif.molrep",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {molrep_tools_script} "
                "--symlink_mrc_map ../../Import/job001/1ake_10A_molrep.mrc --keywords"
                " '_NMON 1\nNCSM 1\nstick\nnp 1\nnpt 1\n'",
                "molrep -m ../../Import/job002/1AKE_cha_molrep_to_move.pdb"
                " -f 1ake_10A_molrep.map -k keywords.txt",
                f"{shlex.join(get_python_command())} {molrep_tools_script} --fix_pdb",
                "gemmi convert molrep.pdb molrep.cif",
            ],
        )

    def test_get_commands_molrep_ent_fixed_input(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_fixed_model_ent.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/pdb5ni1_A.ent": f"{NODE_ATOMCOORDS}.pdb",
                "Import/job003/5me2_a_to_move.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "molrep.pdb": f"{NODE_ATOMCOORDS}.pdb.molrep",
                "molrep.cif": f"{NODE_ATOMCOORDS}.cif.molrep",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {molrep_tools_script}"
                " --symlink_ent_pdb ../../Import/job002/pdb5ni1_A.ent",
                f"{shlex.join(get_python_command())} {molrep_tools_script}"
                " --symlink_mrc_map ../../Import/job001/emd_3488.mrc --keywords"
                " '_NMON 1\nNCSM 1\nstick\nnp 1\nnpt 1\n'",
                "molrep -m ../../Import/job003/5me2_a_to_move.pdb -f emd_3488.map"
                " -k keywords.txt -mx pdb5ni1_A.pdb",
                f"{shlex.join(get_python_command())} {molrep_tools_script} --fix_pdb",
                "gemmi convert molrep.pdb molrep.cif",
            ],
        )

    def test_get_commands_molrep_ent_input(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_model_ent.job"
            ),
            input_nodes={
                "Import/job001/emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "Import/job002/pdb5ni1_A.ent": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "molrep.pdb": f"{NODE_ATOMCOORDS}.pdb.molrep",
                "molrep.cif": f"{NODE_ATOMCOORDS}.cif.molrep",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {molrep_tools_script}"
                " --symlink_ent_pdb ../../Import/job002/pdb5ni1_A.ent"
                " --symlink_mrc_map ../../Import/job001/emd_3488.mrc --keywords"
                " '_NMON 1\nNCSM 1\nstick\nnp 1\nnpt 1\n'",
                "molrep -m pdb5ni1_A.pdb -f emd_3488.map -k keywords.txt",
                f"{shlex.join(get_python_command())} {molrep_tools_script} --fix_pdb",
                "gemmi convert molrep.pdb molrep.cif",
            ],
        )

    @slow_test
    def test_molrep_fit_model(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_model.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "1ake_10A_molrep.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "1AKE_cha_molrep_to_move.pdb"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "molrep.pdb",
                "molrep.btc",
                "molrep.pdb.orig",
                "molrep.cif",
                "keywords.txt",
            ],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Fitted monomer scores",
            "start_collapsed": False,
            "dobj_type": "table",
            "flag": "",
            "header_tooltips": [
                "Monomer number",
                "Z-score of Translation peaks. < 6 is unlikely at high resolutions. "
                "> 100 is highly likely to be a correct fit.",
                "Overlap between monomers. 1.00 indicates no overlap, 0 indicates "
                "full overlap",
                "Final correlation coefficient between search model and map",
                "Final_CC x Packing_Coef. < 0.1 is unlikely to be correct, "
                "< 0.2 less likely to be correct",
            ],
            "headers": ["Nmon", "TF/sg", "Packing_Coef", "Final_CC", "Score"],
            "table_data": [["1", "41.75", "1.0000", "0.9655", "0.966"]],
            "associated_data": ["Molrep/job998/run.out"],
        }
        assert dispobjs[1].__dict__ == {
            "maps": ["Import/job001/1ake_10A_molrep.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["Molrep/job998/molrep.pdb"],
            "title": "3D viewer: Molrep docked model",
            "maps_data": "Import/job001/1ake_10A_molrep.mrc",
            "models_data": "Molrep/job998/molrep.pdb",
            "associated_data": [
                "Import/job001/1ake_10A_molrep.mrc",
                "Molrep/job998/molrep.pdb",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @slow_test
    def test_molrep_fit_model_cif_format(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_cif_model.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "1ake_10A_molrep.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "1AKE_cha_molrep_to_move.cif"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "molrep.pdb",
                "molrep.btc",
                "molrep.pdb.orig",
                "molrep.cif",
                "keywords.txt",
            ],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[1].__dict__ == {
            "maps": ["Import/job001/1ake_10A_molrep.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [0.5],
            "models": ["Molrep/job998/molrep.pdb"],
            "title": "3D viewer: Molrep docked model",
            "maps_data": "Import/job001/1ake_10A_molrep.mrc",
            "models_data": "Molrep/job998/molrep.pdb",
            "associated_data": [
                "Import/job001/1ake_10A_molrep.mrc",
                "Molrep/job998/molrep.pdb",
            ],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }

    @slow_test
    def test_molrep_fit_fixed_model(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_fixed_model.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "5me2_chain_a.cif"),
                ),
                (
                    "Import/job003",
                    os.path.join(self.test_data, "5me2_a_to_move.pdb"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "molrep.pdb",
                "molrep.btc",
                "molrep.pdb.orig",
                "molrep.cif",
                "keywords.txt",
            ],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Molrep docked model",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["Import/job001/emd_3488.mrc"],
            "maps_opacity": [0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": ["Molrep/job998/molrep.pdb", "Import/job002/5me2_chain_a.cif"],
            "maps_data": "Import/job001/emd_3488.mrc",
            "models_data": "Molrep/job998/molrep.pdb, Import/job002/5me2_chain_a.cif",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "Molrep/job998/molrep.pdb",
                "Import/job002/5me2_chain_a.cif",
            ],
        }

    @slow_test
    def test_molrep_fit_model_ent(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/Molrep/molrep_fit_model_ent.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "emd_3488.mrc"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "pdb5ni1_A.ent"),
                ),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
                "molrep.pdb",
                "molrep.btc",
                "molrep.pdb.orig",
                "molrep.cif",
                "keywords.txt",
            ],
        )

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Molrep docked model",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["Import/job001/emd_3488.mrc"],
            "maps_opacity": [0.5],
            "maps_colours": [],
            "models_colours": [],
            "models": ["Molrep/job998/molrep.pdb"],
            "maps_data": "Import/job001/emd_3488.mrc",
            "models_data": "Molrep/job998/molrep.pdb",
            "associated_data": [
                "Import/job001/emd_3488.mrc",
                "Molrep/job998/molrep.pdb",
            ],
        }


if __name__ == "__main__":
    unittest.main()
