#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
from unittest.mock import patch
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.utils import touch
from pipeliner.job_factory import new_job_of_type
from pipeliner.data_structure import NODE_ATOMCOORDS, NODE_DENSITYMAP, NODE_LOGFILE


@patch.dict(os.environ, {"PIPELINER_CCPEM_SHARE_DIR": "/fake/path/to/ccpem/share"})
class ProShadeTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="proshade_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_proshade_plugin_symmetry(self):
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/ProShade/proshade_symmetry_job.star"
            ),
            input_nodes={"test.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={
                "proshade_symmetry.json": f"{NODE_LOGFILE}.json.proshade.map_analysis."
                "symmetry"
            },
            expected_commands=[
                "proshade --rvpath /fake/path/to/ccpem/share -S -f test.mrc",
                "mv proshade_report ProShade/job999/proshade_report",
            ],
        )

    def test_get_command_proshade_plugin_exp_symmetry(self):
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data)
                / "JobFiles/ProShade/proshade_exp_symmetry_job.star"
            ),
            input_nodes={"test.mrc": f"{NODE_DENSITYMAP}.mrc"},
            output_nodes={
                "proshade_symmetry.json": f"{NODE_LOGFILE}.json.proshade.map_analysis."
                "symmetry"
            },
            expected_commands=[
                "proshade --rvpath /fake/path/to/ccpem/share -S -f test.mrc --sym C2",
                "mv proshade_report ProShade/job999/proshade_report",
            ],
        )

    def test_get_command_proshade_plugin_overlay(self):
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/ProShade/proshade_overlay_job.star"
            ),
            input_nodes={
                "test1.mrc": f"{NODE_DENSITYMAP}.mrc",
                "test2.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "test2_overlaid.mrc": f"{NODE_DENSITYMAP}.mrc.proshade.overlaid"
            },
            expected_commands=[
                "proshade --rvpath /fake/path/to/ccpem/share -O -f test1.mrc"
                " -f test2.mrc --clearMap ProShade/job999/test2_overlaid.mrc"
                " --cellBorderSpace 20.0",
                "mv proshade_report ProShade/job999/proshade_report",
            ],
        )

    def test_get_command_proshade_plugin_overlay_with_coords(self):
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data)
                / "JobFiles/ProShade/proshade_overlay_with_coords_job.star"
            ),
            input_nodes={
                "test1.mrc": f"{NODE_DENSITYMAP}.mrc",
                "test2.pdb": f"{NODE_ATOMCOORDS}.pdb",
            },
            output_nodes={
                "test2_overlaid.pdb": f"{NODE_ATOMCOORDS}.pdb.proshade.overlaid"
            },
            expected_commands=[
                "proshade --rvpath /fake/path/to/ccpem/share -O"
                " -f test1.mrc -f test2.pdb --clearMap"
                " ProShade/job999/test2_overlaid.pdb",
                "mv proshade_report ProShade/job999/proshade_report",
            ],
        )

    def test_symmetry_writing_displayobjs(self):
        os.makedirs("ProShade/job001")
        touch("test.mrc")
        pipe = os.path.join(self.test_data, "JobFiles/ProShade/proshade_pipeline.star")
        shutil.copy(pipe, "default_pipeline.star")
        log = os.path.join(self.test_data, "JobFiles/ProShade/logfile.txt")
        shutil.copy(log, "ProShade/job001/run.out")
        jobstar = os.path.join(
            self.test_data, "JobFiles/ProShade/proshade_symmetry_job.star"
        )
        shutil.copy(jobstar, "ProShade/job001/job.star")
        project = PipelinerProject()
        job = project.get_job("ProShade/job001/")
        dispobjs = job.create_results_display()
        axes = os.path.join(self.test_data, "JobFiles/ProShade/axes_results.json")
        elems = os.path.join(self.test_data, "JobFiles/ProShade/elements_results.json")
        alts = os.path.join(
            self.test_data, "JobFiles/ProShade/alternatives_results.json"
        )

        with open(axes, "r") as ax:
            exp_ax = json.load(ax)
        with open(elems, "r") as elm:
            exp_elem = json.load(elm)
        with open(alts, "r") as alt:
            exp_alt = json.load(alt)

        assert dispobjs[0].__dict__ == exp_ax
        assert dispobjs[1].__dict__ == exp_elem
        assert dispobjs[2].__dict__ == exp_alt

    def test_sym_input_regex_validation(self):
        pss_job = new_job_of_type("proshade.map_analysis.symmetry")
        pss_job.joboptions["input_map"].value = "input_map.mrc"
        for good_sym in ["D5", "C4", "C27", "T", "O"]:
            pss_job.joboptions["point_sym"].value = good_sym
            val_results = pss_job.validate_joboptions()
            assert val_results == []
        for bad_sym in ["D", "C", "O8", "T4", "X", "X27"]:
            pss_job.joboptions["point_sym"].value = bad_sym
            val_results = pss_job.validate_joboptions()
            assert val_results[0].message == "Symmerty must be C[1-9] D[1-9], T, or O"


if __name__ == "__main__":
    unittest.main()
