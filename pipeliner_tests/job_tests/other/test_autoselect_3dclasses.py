#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import unittest
import os
import shutil
import tempfile
import shlex
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner.utils import get_job_script, touch, get_python_command
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.nodes import (
    NODE_DENSITYMAP,
    NODE_PARTICLEGROUPMETADATA,
)
from pipeliner.jobs.other.autoselect_class3d_job import select_3dclass_from_model
from pipeliner.scripts.job_scripts.autoselect_class3d import autoselect_class3d
from pipeliner.starfile_handler import DataStarFile
from pipeliner.results_display_objects import (
    ResultsDisplayMontage,
    ResultsDisplayMapModel,
)


class AutoSelect3DTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="autoselect3d_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_select_3dclass_from_model(self):
        modfile = os.path.join(self.test_data, "class3d_model.star")
        clno = select_3dclass_from_model(modfile, verbose=True)
        assert clno == 4

    def test_select_3dclass_from_model_equal_res(self):
        modfile = os.path.join(self.test_data, "class3d_model_equal_res.star")
        clno = select_3dclass_from_model(modfile, verbose=True)
        assert clno == 3

    def test_select_3dclass_from_model_random(self):
        modfile = os.path.join(self.test_data, "class3d_model_rando.star")
        clno = select_3dclass_from_model(modfile, verbose=True)
        assert clno in [1, 2]

    def test_command_generation(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/Autoselect3D/autoselect3d_job.star"
            ),
            input_nodes={
                "Class3D/job010/run_it025_data.star": NODE_PARTICLEGROUPMETADATA
                + ".star."
                "relion.class3d"
            },
            output_nodes={
                "selected_particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "selected_class.mrc": f"{NODE_DENSITYMAP}.mrc.3d_class.relion",
            },
            expected_commands=[
                f" {shlex.join(get_python_command())} "
                f"{get_job_script('autoselect_class3d/autoselect_class3d.py')} --data "
                "Class3D/job010/run_it025_data.star --outdir Select/job999/"
            ],
            show_outputnodes=True,
        )

    def test_autoselect_script(self):
        cl3ddir = "Class3D/job010/"
        odir = "Select/job011"
        os.makedirs(cl3ddir)
        os.makedirs(odir)
        for n in range(1, 5):
            touch(os.path.join(cl3ddir, f"run_it025_class{n:03d}.mrc"))
        datafile = os.path.join(cl3ddir, "run_it025_data.star")
        shutil.copy(
            os.path.join(self.test_data, "class3d_data.star"),
            datafile,
        )
        shutil.copy(
            os.path.join(self.test_data, "class3d_model.star"),
            os.path.join(cl3ddir, "run_it025_model.star"),
        )
        autoselect_class3d.main(in_args=["--data", datafile, "--outdir", odir])
        parts_file = os.path.join(odir, "selected_particles.star")
        assert os.path.isfile(parts_file)
        assert os.path.isfile(os.path.join(odir, "selected_class.mrc"))

        wrote = DataStarFile(parts_file)
        assert wrote.loop_as_list("optics", headers=True) == [
            [
                "_rlnOpticsGroupName",
                "_rlnOpticsGroup",
                "_rlnMtfFileName",
                "_rlnMicrographOriginalPixelSize",
                "_rlnVoltage",
                "_rlnSphericalAberration",
                "_rlnAmplitudeContrast",
                "_rlnImagePixelSize",
                "_rlnImageSize",
                "_rlnImageDimensionality",
            ],
            [
                "opticsGroup1",
                "1",
                "mtf_k2_200kV.star",
                "0.885000",
                "200.000000",
                "1.400000",
                "0.100000",
                "3.540000",
                "64",
                "2",
            ],
        ]

        assert wrote.loop_as_list("particles", headers=True) == [
            [
                "_rlnCoordinateX",
                "_rlnCoordinateY",
                "_rlnAutopickFigureOfMerit",
                "_rlnClassNumber",
                "_rlnAnglePsi",
                "_rlnImageName",
                "_rlnMicrographName",
                "_rlnOpticsGroup",
                "_rlnCtfMaxResolution",
                "_rlnCtfFigureOfMerit",
                "_rlnDefocusU",
                "_rlnDefocusV",
                "_rlnDefocusAngle",
                "_rlnCtfBfactor",
                "_rlnCtfScalefactor",
                "_rlnPhaseShift",
                "_rlnGroupNumber",
                "_rlnAngleRot",
                "_rlnAngleTilt",
                "_rlnOriginXAngst",
                "_rlnOriginYAngst",
                "_rlnNormCorrection",
                "_rlnLogLikeliContribution",
                "_rlnMaxValueProbDistribution",
                "_rlnNrOfSignificantSamples",
            ],
            [
                "1068.000000",
                "2190.000000",
                "11.750497",
                "4",
                "-32.90317",
                "000014@Extract/job012/Movies/20170629_00021_frameImage.mrcs",
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "1",
                "4.809192",
                "0.131159",
                "10864.146484",
                "10575.793945",
                "77.995003",
                "0.000000",
                "1.000000",
                "0.000000",
                "1",
                "-68.49398",
                "125.406903",
                "-5.66077",
                "-16.28077",
                "0.810001",
                "10898.362563",
                "0.958165",
                "4",
            ],
            [
                "1722.000000",
                "2418.000000",
                "11.348187",
                "4",
                "-63.06974",
                "000015@Extract/job012/Movies/20170629_00021_frameImage.mrcs",
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "1",
                "4.809192",
                "0.131159",
                "10864.146484",
                "10575.793945",
                "77.995003",
                "0.000000",
                "1.000000",
                "0.000000",
                "1",
                "-153.89267",
                "151.099210",
                "1.419233",
                "-9.20077",
                "0.809379",
                "10964.399307",
                "0.950591",
                "4",
            ],
            [
                "3534.000000",
                "1524.000000",
                "10.908073",
                "4",
                "124.210712",
                "000016@Extract/job012/Movies/20170629_00021_frameImage.mrcs",
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "1",
                "4.809192",
                "0.131159",
                "10864.146484",
                "10575.793945",
                "77.995003",
                "0.000000",
                "1.000000",
                "0.000000",
                "1",
                "-35.04658",
                "114.011119",
                "12.039233",
                "-5.66077",
                "0.780615",
                "10845.789015",
                "0.470894",
                "9",
            ],
            [
                "3282.000000",
                "2538.000000",
                "14.443563",
                "4",
                "49.507589",
                "000003@Extract/job012/Movies/20170629_00021_frameImage.mrcs",
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "1",
                "4.809192",
                "0.131159",
                "10864.146484",
                "10575.793945",
                "77.995003",
                "0.000000",
                "1.000000",
                "0.000000",
                "1",
                "-68.67230",
                "114.346961",
                "-5.66077",
                "-9.20077",
                "0.743938",
                "10845.943670",
                "0.998472",
                "2",
            ],
        ]

    def test_make_results_display(self):
        rdos = [
            ResultsDisplayMontage(
                title="Map slices: Selected class 4",
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "Select/job999/selected_class.mrc: xy",
                    "Select/job999/selected_class.mrc: xz",
                    "Select/job999/selected_class.mrc: yz",
                ],
                associated_data=["Select/job999/selected_class.mrc"],
                img="Select/job999/Thumbnails/slices_montage_000.png",
            ),
            ResultsDisplayMapModel(
                title="Selected class 4",
                maps=["Select/job999/selected_class.mrc"],
                maps_data="Select/job999/selected_class.mrc",
                associated_data=["Select/job999/selected_class.mrc"],
            ),
            ResultsDisplayMontage(
                title="Select/job999/selected_particles.star; 20/1158 images",
                xvalues=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                yvalues=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                labels=[
                    "000001@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000002@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000003@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000004@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000005@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000006@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000007@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000008@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000009@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000010@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000011@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000012@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000013@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000014@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000015@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000016@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000017@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000018@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000019@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                    "000020@Extract/job007/Movies/20170629_00021_frameImage.mrcs",
                ],
                associated_data=["Select/job999/selected_particles.star"],
                img="Select/job999/NodeDisplay/Thumbnails/montage_s000.png",
            ),
        ]

        test_data = Path(self.test_data)
        Path("Class3D/job010/").mkdir(parents=True)
        Path("Extract/job007/Movies").mkdir(parents=True)
        shutil.copy(
            test_data / "class3d_model.star", "Class3D/job010/run_it025_model.star"
        )
        shutil.copy(
            test_data / "image_stack.mrcs",
            "Extract/job007/Movies/20170629_00021_frameImage.mrcs",
        )
        job_display_object_generation_test(
            jobfile=str(test_data / "JobFiles/Autoselect3D/autoselect3d_job.star"),
            expected_display_objects=rdos,
            files_to_create={
                "selected_class.mrc": str(test_data / "emd_3488.mrc"),
                "selected_particles.star": str(test_data / "particles.star"),
            },
        )


if __name__ == "__main__":
    unittest.main()
