#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import unittest
import os
import shutil
import tempfile
import subprocess
import shlex

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.utils import get_job_script, get_python_command
from pipeliner.data_structure import NODE_MLMODEL, NODE_IMAGE2DSTACK


train_script = get_job_script("cinderella/make_cinderella_config.py")


class CinderellaTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="cinderella_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_class2dpredict(self):
        testfile = os.path.join(
            self.test_data,
            "JobFiles/Cinderella/cinderella_classify_class_averages_job.star",
        )
        job_generate_commands_test(
            jobfile=testfile,
            input_nodes={
                "my_classes.mrcs": f"{NODE_IMAGE2DSTACK}.mrc.class_averages",
                "my_model.h5": f"{NODE_MLMODEL}.h5.cinderella",
            },
            output_nodes={
                "my_classes_good.hdf": f"{NODE_IMAGE2DSTACK}.hdf.class_averages",
                "my_classes_bad.hdf": f"{NODE_IMAGE2DSTACK}.hdf.class_averages.bad",
            },
            expected_commands=[
                "sp_cinderella_predict.py -i my_classes.mrcs -w my_model.h5 -t 0.7 "
                "-o Cinderella/job999/ --gpu 1"
            ],
        )

    def test_get_command_cinderella_clavs_trains(self):
        testfile = os.path.join(
            self.test_data,
            "JobFiles/Cinderella/cinderella_train_class_averages_job.star",
        )
        os.makedirs("path/to/good")
        os.makedirs("path/to/bad")

        job_generate_commands_test(
            jobfile=testfile,
            input_nodes={},
            output_nodes={"trained_model.h5": f"{NODE_MLMODEL}.h5.cinderella"},
            expected_commands=[
                f"{shlex.join(get_python_command())} "
                f"{train_script} 64 no_mr 32 path/to/good/ "
                "path/to/bad/ no_ptw Cinderella/job999/ 100 15",
                "sp_cinderella_train.py -c Cinderella/job999/training_config.json"
                " --gpu 1",
            ],
        )

    def test_make_config_script_nomr_or_ptw(self):
        os.makedirs("Cinderella/job999")
        subprocess.run(
            get_python_command()
            + [
                train_script,
                "64",
                "no_mr",
                "32",
                "path/to/good/",
                "path/to/bad/",
                "no_ptw",
                "Cinderella/job999/",
                "100",
                "15",
            ]
        )

        configfile = "Cinderella/job999/training_config.json"
        ref_file = os.path.join(self.test_data, "JobFiles/Cinderella/config_file.json")
        assert os.path.isfile("Cinderella/job999/training_config.json")
        with open(configfile, "r") as cf:
            actual = json.load(cf)
        with open(ref_file, "r") as rf:
            expected = json.load(rf)

        for i in actual:
            for j in actual[i]:
                assert actual[i][j] == expected[i][j]
        for i in expected:
            for j in expected[i]:
                assert actual[i][j] == expected[i][j]

    def test_make_config_script_mr_and_ptw(self):
        os.makedirs("Cinderella/job999")
        subprocess.run(
            get_python_command()
            + [
                train_script,
                "64",
                "15",
                "32",
                "path/to/good/",
                "path/to/bad/",
                "path/to/pretrained/",
                "Cinderella/job999/",
                "100",
                "15",
            ]
        )

        configfile = "Cinderella/job999/training_config.json"
        ref_file = os.path.join(self.test_data, "JobFiles/Cinderella/config_mrptw.json")
        assert os.path.isfile("Cinderella/job999/training_config.json")
        with open(configfile, "r") as cf:
            actual = json.load(cf)
        with open(ref_file, "r") as rf:
            expected = json.load(rf)

        for i in actual:
            for j in actual[i]:
                assert actual[i][j] == expected[i][j]
        for i in expected:
            for j in expected[i]:
                assert actual[i][j] == expected[i][j]


if __name__ == "__main__":
    unittest.main()
