#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
import shlex
from pathlib import Path
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
)
from pipeliner.data_structure import SUCCESS_FILE
from pipeliner.utils import get_job_script, get_python_command
from pipeliner.data_structure import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_IMAGE3D,
    NODE_LOGFILE,
)


class CryoEFTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_cryoEF_plugin_input(self):
        os.makedirs(os.path.join(self.test_dir, "Refine3D/job001/"))
        shutil.copy(
            os.path.join(self.test_data, "run_data.star"),
            os.path.join(self.test_dir, "Refine3D/job001/"),
        )
        input_file_script = get_job_script("cryoef/make_cryoef_angle_file.py")
        job_generate_commands_test(
            jobfile=str(Path(self.test_data) / "JobFiles/CryoEF/cryoef_star_job.star"),
            input_nodes={
                "Refine3D/job001/run_data."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "cryoef_angles_K.mrc": f"{NODE_IMAGE3D}.mrc.cryoef.fourierspace_"
                "transferfunction",
                "cryoef_angles_R.mrc": f"{NODE_IMAGE3D}.mrc.cryoef.realspace_"
                "powerspectrum",
                "cryoef_angles.log": f"{NODE_LOGFILE}.txt.cryoef",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {input_file_script}"
                " --input_starfile Refine3D/job001/run_data.star"
                " --output_dir CryoEF/job999/",
                "cryoEF -f CryoEF/job999/cryoef_angles.dat -B 150.0 "
                "-D 80.0 -b 128 -r 3.8 -g C1",
            ],
        )

    def test_run_cryoEF(self):
        os.makedirs(os.path.join(self.test_dir, "Refine3D/job001/"))
        shutil.copy(
            os.path.join(self.test_data, "run_data.star"),
            os.path.join(self.test_dir, "Refine3D/job001/"),
        )
        out_proc = job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/CryoEF/cryoef_star_job.star"
            ),
            input_files=[],
            expected_outfiles=(
                "run.out",
                "run.err",
                "cryoef_angles_K.mrc",
                "cryoef_angles_R.mrc",
                SUCCESS_FILE,
            ),
        )

        # make sure cleanup happened
        assert not os.path.isfile("cryoef_angles.dat")
        dispobjs = active_job_from_proc(out_proc).create_results_display()
        assert dispobjs[0].__dict__ == {
            "title": "Tilted collection recommendations",
            "dobj_type": "table",
            "headers": ["Tilt angle", "Reccomended particles to collect"],
            "header_tooltips": ["Tilt angle", "Reccomended particles to collect"],
            "table_data": [["42", "~3.2x"], ["39", "~3.0x"]],
            "associated_data": ["CryoEF/job998/cryoef_angles.log"],
            "start_collapsed": False,
            "flag": "",
        }

        hd = os.path.join(self.test_data, "JobFiles/CryoEF/results_histo.json")
        with open(hd, "r") as hdf:
            exp_histo = json.load(hdf)
        assert dispobjs[1].__dict__ == exp_histo

        assert dispobjs[2].__dict__ == {
            "maps": ["CryoEF/job998/cryoef_angles_K.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Fourier space information density",
            "maps_data": "CryoEF/job998/cryoef_angles_K.mrc",
            "models_data": "",
            "associated_data": ["CryoEF/job998/cryoef_angles_K.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }


if __name__ == "__main__":
    unittest.main()
