#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import shlex
from pipeliner_tests import test_data
from pipeliner.job_factory import active_job_from_proc
from pipeliner_tests.testing_tools import (
    job_running_test,
    job_generate_commands_test,
    slow_test,
)
from pipeliner.scripts.job_scripts.doublehelix import get_doublehelix_results
from pipeliner.utils import get_python_command
from pipeliner.data_structure import NODE_LOGFILE, NODE_DENSITYMAP, NODE_RESTRAINTS


class DoubleHelixTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="doubleHelix")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @slow_test
    def test_doublehelix_identify(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/DoubleHelix/findseq.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3j79_A.pdb"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_2660_small.map"),
                ),
                ("Import/job003", os.path.join(self.test_data, "3j79_A.fa")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        assert len(dispobjs) == 1

    def test_doublehelix_assign(self):
        proc = job_running_test(
            test_jobfile=os.path.join(
                self.test_data, "JobFiles/DoubleHelix/assignseq.job"
            ),
            input_files=[
                (
                    "Import/job001",
                    os.path.join(self.test_data, "3j79_A.pdb"),
                ),
                (
                    "Import/job002",
                    os.path.join(self.test_data, "emd_2660_small.map"),
                ),
                ("Import/job003", os.path.join(self.test_data, "3j79_A.fa")),
            ],
            expected_outfiles=[
                "run.out",
                "run.err",
            ],
            print_err=True,
        )

        dispobjs = active_job_from_proc(proc).create_results_display()
        assert len(dispobjs) == 2

    def test_get_command_modelvalidation(self):
        exp_out_nodes = {}
        exp_coms = []
        # checkmysequence
        exp_coms += [
            "doubleHelix --modelin ../../Import/job001/3j79_A.pdb"
            " --mapin ../../Import/job002/emd_2660_small.map --seqin"
            " ../../Import/job003/3j79_A.fa --identify --type RNA"
            " --jsonout 3j79_A_emd_2660_smalldoublehelix.json",
            f"{shlex.join(get_python_command())} {get_doublehelix_results.__file__} "
            "-j 3j79_A_emd_2660_smalldoublehelix.json -id 3j79_A_emd_2660_small",
        ]
        exp_out_nodes["3j79_A_emd_2660_smalldoublehelix.json"] = (
            f"{NODE_LOGFILE}.json.doublehelix.json_out"
        )
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/DoubleHelix/findseq.job"),
            input_nodes={
                "Import/job002/emd_2660_small.map": f"{NODE_DENSITYMAP}.mrc",
                "Import/job001/3j79_A.pdb": "AtomCoords.pdb",
                "Import/job003/3j79_A.fa": "Sequence.fa",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
        )

    def test_get_command_restraints(self):
        exp_out_nodes = {}
        exp_coms = []
        # checkmysequence
        exp_coms += [
            "doubleHelix --modelin ../../Import/job001/3j79_A.pdb"
            " --type RNA --restraints",
        ]
        exp_out_nodes["3j79_A_doubleHelix_restraints_refmac.txt"] = (
            f"{NODE_RESTRAINTS}.txt.doublehelix.restraints_out"
        )
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/DoubleHelix/create_restraints.job"
            ),
            input_nodes={
                "Import/job001/3j79_A.pdb": "AtomCoords.pdb",
            },
            output_nodes=exp_out_nodes,
            expected_commands=exp_coms,
        )


if __name__ == "__main__":
    unittest.main()
