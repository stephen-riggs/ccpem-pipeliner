#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner.utils import touch
from pipeliner.project_graph import ProjectGraph
from pipeliner.job_factory import active_job_from_proc
from pipeliner.data_structure import (
    NODE_DENSITYMAP,
    NODE_PARTICLEGROUPMETADATA,
    NODE_LOGFILE,
    NODE_PROCESSDATA,
)
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_running_test,
)


class ExternalProcessingImportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_all_nodes_are_unique(self):
        """The basic test all imported outputs are unique so they just need
        to be copied in to the directory"""
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_simple.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        touch("processed_map.mrc")
        touch("logfile.log")
        mapout = os.path.abspath("processed_map.mrc")
        logout = os.path.abspath("logfile.log")
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/Import/external_import_job.star"
            ),
            input_nodes={
                "mapfile.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d",
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "test_external_import.json": f"{NODE_PROCESSDATA}.json.pipeliner."
                "external_workflow",
            },
            output_nodes={
                "processed_map.mrc": f"{NODE_DENSITYMAP}.mrc.externalprog.fixed",
                "logfile.log": f"{NODE_LOGFILE}.txt.externalprog",
            },
            expected_commands=[
                f"cp {mapout} Import/job999/processed_map.mrc",
                f"cp {logout} Import/job999/logfile.log",
                "cp test_external_import.json Import/job999/externa"
                "l_processing.json",
            ],
        )

    def test_all_nodes_have_single_dir(self):
        """The imported nodes have the same name and a 1-deep directory structure"""
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_depth1.json"
        )
        shutil.copy(importfile, "test_external_import.json")

        os.makedirs("step1")
        os.makedirs("step2")
        touch("step1/processed_map.mrc")
        touch("step2/processed_map.mrc")
        mapout = os.path.abspath("step1/processed_map.mrc")
        logout = os.path.abspath("step2/processed_map.mrc")

        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/Import/external_import_job.star"
            ),
            input_nodes={
                "mapfile.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d",
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "test_external_import.json": f"{NODE_PROCESSDATA}.json.pipeliner."
                "external_workflow",
            },
            output_nodes={
                "step1/processed_map.mrc": f"{NODE_DENSITYMAP}.mrc.externalprog.fixed",
                "step2/processed_map.mrc": f"{NODE_LOGFILE}.mrc.externalprog",
            },
            expected_commands=[
                "mkdir -p Import/job999/step1",
                f"cp {mapout} Import/job999/step1/processed_map.mrc",
                "mkdir -p Import/job999/step2",
                f"cp {logout} Import/job999/step2/processed_map.mrc",
                "cp test_external_import.json Import/job999/externa"
                "l_processing.json",
            ],
        )

    def test_all_nodes_have_longer_dir(self):
        """The imported nodes have the same name and a deeper directory structure"""
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_depth2.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        os.makedirs("test1/step1")
        os.makedirs("test2/step1")
        touch("test1/step1/processed_map.mrc")
        touch("test2/step1/processed_map.mrc")
        mapout = os.path.abspath("test1/step1/processed_map.mrc")
        logout = os.path.abspath("test2/step1/processed_map.mrc")
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/Import/external_import_job.star"
            ),
            input_nodes={
                "mapfile.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d",
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "test_external_import.json": f"{NODE_PROCESSDATA}.json.pipeliner."
                "external_workflow",
            },
            output_nodes={
                "test1/step1/processed_map.mrc": f"{NODE_DENSITYMAP}.mrc.externalprog."
                "fixed",
                "test2/step1/processed_map.mrc": f"{NODE_DENSITYMAP}.mrc.externalprog",
            },
            expected_commands=[
                "mkdir -p Import/job999/test1/step1",
                f"cp {mapout} Import/job999/test1/step1/processed_map.mrc",
                "mkdir -p Import/job999/test2/step1",
                f"cp {logout} Import/job999/test2/step1/processed_map.mrc",
                "cp test_external_import.json Import/job999/externa"
                "l_processing.json",
            ],
        )

    def test_all_nodes_have_variable_dir(self):
        """The imported nodes have the same name and a deeper directory structure
        with varying lengths"""
        importfile = os.path.join(
            self.test_data,
            "ExternalImport/external_processing_depth_multi.json",
        )
        shutil.copy(importfile, "test_external_import.json")
        os.makedirs("test1/step1/")
        os.makedirs("silly/test1/step1")
        touch("test1/step1/processed_map.mrc")
        touch("silly/test1/step1/processed_map.mrc")
        mapout = os.path.abspath("test1/step1/processed_map.mrc")
        logout = os.path.abspath("silly/test1/step1/processed_map.mrc")
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/Import/external_import_job.star"
            ),
            input_nodes={
                "mapfile.mrc": f"{NODE_DENSITYMAP}.mrc.relion.refine3d",
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                "test_external_import.json": f"{NODE_PROCESSDATA}.json.pipeliner."
                "external_workflow",
            },
            output_nodes={
                "test1/step1/processed_map.mrc": f"{NODE_DENSITYMAP}.mrc.externalprog."
                "fixed",
                "silly/test1/step1/processed_map.mrc": f"{NODE_LOGFILE}.mrc."
                "externalprog",
            },
            expected_commands=[
                "mkdir -p Import/job999/test1/step1",
                f"cp {mapout} Import/job999/test1/step1/processed_map.mrc",
                "mkdir -p Import/job999/silly/test1/step1",
                f"cp {logout} Import/job999/silly/test1/step1/processed_map.mrc",
                "cp test_external_import.json Import/job999/externa"
                "l_processing.json",
            ],
        )

    def test_running(self):
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_live.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/Import/external_import_job.star"
            ),
            input_files=[
                ("Refine3D/job999", str(Path(self.test_data) / "emd_3488.mrc")),
                ("Refine3D/job999", str(Path(self.test_data) / "particles.star")),
                ("SomeOtherProgram", str(Path(self.test_data) / "logfile.log")),
                ("SomeOtherProgram", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=["emd_3488_mask.mrc", "logfile.log"],
        )

    def test_running_multidir(self):
        importfile = os.path.join(
            self.test_data,
            "ExternalImport/external_processing_live_multi.json",
        )
        shutil.copy(importfile, "test_external_import.json")
        job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/Import/external_import_job.star"
            ),
            input_files=[
                ("Refine3D/job999", str(Path(self.test_data) / "emd_3488.mrc")),
                ("Refine3D/job002", str(Path(self.test_data) / "particles.star")),
                (
                    "SomeOtherProgram/test1/",
                    str(Path(self.test_data) / "emd_3488_mask.mrc"),
                ),
                (
                    "SomeOtherProgram/test2/",
                    str(Path(self.test_data) / "emd_3488_mask.mrc"),
                ),
            ],
            expected_outfiles=["test1/emd_3488_mask.mrc", "test2/emd_3488_mask.mrc"],
        )

    def test_results_display(self):
        # setup and run the job
        importfile = os.path.join(
            self.test_data, "ExternalImport/external_processing_live.json"
        )
        shutil.copy(importfile, "test_external_import.json")
        job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/Import/external_import_job.star"
            ),
            input_files=[
                ("Refine3D/job001", str(Path(self.test_data) / "emd_3488.mrc")),
                ("Refine3D/job001", str(Path(self.test_data) / "particles.star")),
                ("SomeOtherProgram", str(Path(self.test_data) / "logfile.log")),
                ("SomeOtherProgram", str(Path(self.test_data) / "emd_3488_mask.mrc")),
            ],
            expected_outfiles=["emd_3488_mask.mrc", "logfile.log"],
        )
        with ProjectGraph() as pipeline:
            proc = pipeline.process_list[0]

        job = active_job_from_proc(proc)
        dispobjs = job.create_results_display()
        expected = [
            {
                "title": "Inputs",
                "dobj_type": "table",
                "headers": ["File", "NodeType"],
                "header_tooltips": ["File", "NodeType"],
                "table_data": [
                    [
                        "test_external_import.json",
                        f"{NODE_PROCESSDATA}.json.pipeliner.external_workflow",
                    ],
                    [
                        "Refine3D/job001/emd_3488.mrc",
                        f"{NODE_DENSITYMAP}.mrc.relion.refine3d",
                    ],
                    [
                        "Refine3D/job001/particles.star",
                        f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
                    ],
                ],
                "associated_data": [
                    "test_external_import.json",
                    "Refine3D/job001/emd_3488.mrc",
                    "Refine3D/job001/particles.star",
                ],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Outputs",
                "dobj_type": "table",
                "headers": ["File", "NodeType"],
                "header_tooltips": ["File", "NodeType"],
                "table_data": [
                    [
                        "Import/job998/emd_3488_mask.mrc",
                        f"{NODE_DENSITYMAP}.mrc.externalprog.fixed",
                    ],
                    ["Import/job998/logfile.log", f"{NODE_LOGFILE}.txt.externalprog"],
                ],
                "associated_data": [
                    "Import/job998/emd_3488_mask.mrc",
                    "Import/job998/logfile.log",
                ],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Processing Info",
                "dobj_type": "text",
                "display_data": (
                    "\n  Step 1: I did a thing,\n  Step 2: I did another thing,\n  "
                    "Step 3: \n    substep a: A result,\n    substep b: B result\n  "
                    "\n"
                ),
                "associated_data": ["Import/job998/external_processing.json"],
                "start_collapsed": False,
                "flag": "",
            },
        ]
        for n, do in enumerate(dispobjs):
            assert do.__dict__ == expected[n], [do.__dict__, expected[n]]


if __name__ == "__main__":
    unittest.main()
