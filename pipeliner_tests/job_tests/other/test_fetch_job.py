#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import requests
import shlex
from pathlib import Path
from unittest.mock import patch

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_running_test,
    live_test,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.node_factory import create_node
from pipeliner.utils import get_job_script, get_python_command
from pipeliner.data_structure import NODE_ATOMCOORDS, NODE_DENSITYMAP, NODE_SEQUENCE
from pipeliner.scripts.job_scripts.fetch import emdb_get_associated_models
from pipeliner.scripts.job_scripts import download_map_model


def check_dbs():
    try:
        ad = "https://files.wwpdb.org/pub/emdb/structures/EMD-0002/map/emd_0002.map.gz"
        response = requests.head(ad, timeout=3.1)
        emdb = response.status_code == 200
        ad = "https://files.wwpdb.org/pub/pdb/data/structures/all/mmCIF/100d.cif.gz"
        response = requests.head(ad, timeout=3.1)
        pdb_pdb = response.status_code == 200
        ad = "https://files.wwpdb.org/pub/pdb/data/structures/all/pdb/pdb100d.ent.gz"
        response = requests.head(ad, timeout=3.1)
        pdb_mmcif = response.status_code == 200
        ad = "https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-model_v4.pdb"
        response = requests.head(ad, timeout=3.1)
        alphafold = response.status_code == 200
        return all([emdb, pdb_mmcif, pdb_pdb, alphafold])

    except requests.RequestException as ex:
        print(ex)
        return False


dbs_available = check_dbs()
tools_script = get_job_script("download_map_model.py")
asoc_script = get_job_script("fetch/emdb_get_associated_models.py")


class FetchImportTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = os.path.join(self.test_data, "JobFiles")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_pdb(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_pdb_ent_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={"7NDO.pdb": f"{NODE_ATOMCOORDS}.pdb.from_pdb"},
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://files.rcsb.org/download/7NDO.pdb.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/7NDO.pdb.gz",
            ],
        )

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_pdb_with_fasta(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_pdb_fasta_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={
                "7NDO.pdb": f"{NODE_ATOMCOORDS}.pdb.from_pdb",
                "7ndo.fasta": f"{NODE_SEQUENCE}.fasta",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://files.rcsb.org/download/7NDO.pdb.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/7NDO.pdb.gz",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://www.ebi.ac.uk/pdbe/entry/pdb/7ndo/fasta -P Fetch/job999/",
                "mv Fetch/job999/fasta Fetch/job999/7NDO.fasta",
            ],
            show_outputnodes=True,
        )

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_mmcif(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_pdb_mmcif_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={"7NDO.cif": f"{NODE_ATOMCOORDS}.cif.from_pdb"},
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://files.rcsb.org/download/7NDO.cif.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/7NDO.cif.gz",
            ],
        )

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_emdb(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_emdb_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={"emd_0002.mrc": f"{NODE_DENSITYMAP}.mrc.from_emdb"},
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://files.wwpdb.org/pub/emdb/structures/EMD-0002/"
                "header/emd-0002.xml -P Fetch/job999/",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://files.wwpdb.org/pub/emdb/structures/EMD-0002/map/"
                "emd_0002.map.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/emd_0002.map.gz",
                "mv Fetch/job999/emd_0002.map Fetch/job999/emd_0002.mrc",
            ],
        )

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_emdb_with_halfmaps(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_emdb_hms_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={"emd_28897.mrc": f"{NODE_DENSITYMAP}.mrc.from_emdb"},
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://files.wwpdb.org/pub/emdb/structures/EMD-28897/"
                "header/emd-28897.xml -P Fetch/job999/",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://files.wwpdb.org/pub/emdb/structures/EMD-28897/"
                "map/emd_28897.map.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/emd_28897.map.gz",
                "mv Fetch/job999/emd_28897.map Fetch/job999/emd_28897.mrc",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://ftp.ebi.ac.uk/pub/databases/emdb/structures/EMD-"
                "28897/other/emd_28897_half_map_1.map.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/emd_28897_half_map_1.map.gz",
                "mv Fetch/job999/emd_28897_half_map_1.map Fetch/job999/emd_"
                "28897_half_map_1.mrc",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://ftp.ebi.ac.uk/pub/databases/emdb/structures/"
                "EMD-28897/other/emd_28897_half_map_2.map.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/emd_28897_half_map_2.map.gz",
                "mv Fetch/job999/emd_28897_half_map_2.map Fetch/job999/emd_"
                "28897_half_map_2.mrc",
            ],
        )

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_emdb_with_asocmod_and_sequence(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(
            self.jobfiles, "Fetch", "fetch_emdb_asoc_and_seq_job.star"
        )
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={"emd_0002.mrc": f"{NODE_DENSITYMAP}.mrc.from_emdb"},
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://files.wwpdb.org/pub/emdb/structures/EMD-0002/"
                "header/emd-0002.xml -P Fetch/job999/",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://files.wwpdb.org/pub/emdb/structures/EMD-0002/"
                "map/emd_0002.map.gz -P Fetch/job999/",
                "gzip -d Fetch/job999/emd_0002.map.gz",
                "mv Fetch/job999/emd_0002.map Fetch/job999/emd_0002.mrc",
                f"{shlex.join(get_python_command())} {asoc_script} --id 0002 --outdir "
                "Fetch/job999/ --do_cif --get_seqs",
            ],
        )

    @live_test(condition=dbs_available)
    def test_running_pdb(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_pdb_ent_job.star")
        job_running_test(
            test_jobfile=jobfile,
            input_files=[],
            expected_outfiles=["7NDO.pdb"],
        )

    @live_test(condition=dbs_available)
    def test_running_emdb(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_emdb_job.star")
        job_running_test(
            test_jobfile=jobfile,
            input_files=[],
            expected_outfiles=["emd_0002.mrc"],
        )
        assert os.path.isfile("Fetch/job998/emd-0002.xml")

    @live_test(condition=dbs_available)
    def test_running_emdb_with_asoc_and_seq_but_they_dont_exist(self):
        jobfile = os.path.join(
            self.jobfiles, "Fetch", "fetch_emdb_asoc_and_seq_job.star"
        )
        job_running_test(
            test_jobfile=jobfile,
            input_files=[],
            expected_outfiles=["emd_0002.mrc"],
        )
        assert os.path.isfile("Fetch/job998/emd-0002.xml")

    @live_test(condition=dbs_available)
    def test_get_associated_models_script_cif(self):
        fd = Path("Fetch/job998/")
        fd.mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "JobFiles/Fetch/fetch_emdb/emd-12232.xml", fd
        )
        emdb_get_associated_models.main(
            ["--id", "12232", "--outdir", "Fetch/job998/", "--do_cif", "--get_seqs"]
        )
        exp_files = ["7bnq.fasta", "7BNQ.cif"]
        for f in exp_files:
            assert Path(f"Fetch/job998/{f}").is_file(), f

    @live_test(condition=dbs_available)
    def test_get_associated_models_script_pdb(self):
        fd = Path("Fetch/job998/")
        fd.mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "JobFiles/Fetch/fetch_emdb/emd-12232.xml", fd
        )
        emdb_get_associated_models.main(
            ["--id", "12232", "--outdir", "Fetch/job998/", "--get_seqs"]
        )
        exp_files = ["7bnq.fasta", "7BNQ.pdb"]
        for f in exp_files:
            assert Path(f"Fetch/job998/{f}").is_file(), f

    @live_test(condition=dbs_available)
    def test_running_emdb_with_asoc_and_seq_cif(self):
        jobfile = os.path.join(
            self.jobfiles, "Fetch", "fetch_emdb_asoc_seq_cif_job.star"
        )
        job_running_test(
            test_jobfile=jobfile,
            input_files=[],
            expected_outfiles=["emd_12232.mrc", "7bnq.fasta", "7BNQ.cif"],
            print_run=True,
        )
        assert os.path.isfile("Fetch/job998/emd-12232.xml")

    @live_test(condition=dbs_available)
    def test_running_mmcif(self):
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_pdb_mmcif_job.star")
        job_running_test(
            test_jobfile=jobfile,
            input_files=[],
            expected_outfiles=["7NDO.cif"],
        )

    def test_parse_emdb_xml(self):
        f = "emd-15285.xml"
        jobdir = "Fetch/job001"
        xml = os.path.join(self.test_data, "Metadata", f)
        os.makedirs(jobdir)
        shutil.copy(xml, os.path.join(jobdir, f))
        job = new_job_of_type("pipeliner.fetch.emdb")
        job.output_dir = jobdir
        job.joboptions["db_id"].value = "15285"
        expected = (
            "EMD-15285: Cryo-EM structure of alpha-synuclein filaments from Parkinson's"
            " disease and dementia with Lewy bodies",
            "Yang Y et al.\nStructures of alpha-synuclein filaments from human brains"
            " with Lewy pathology.\nNature (2022) doi:  DOI: "
            "doi:10.1038/s41586-022-05319-3\npubmed:  PubMed: 36108674",
            "256 x 256 x 256",
            "0.727",
            "2.2",
            [],
            [["8a9l"]],
        )
        assert job.parse_emdb_header() == expected

    def test_parse_emdb_xml_with_associated_emdbs(self):
        f = "emd-0001.xml"
        jobdir = "Fetch/job001"
        xml = os.path.join(self.test_data, "Metadata", f)
        os.makedirs(jobdir)
        shutil.copy(xml, os.path.join(jobdir, f))
        job = new_job_of_type("pipeliner.fetch.emdb")
        job.output_dir = jobdir
        job.joboptions["db_id"].value = "0001"
        expected = (
            "EMD-0001: Cryo-EM structure of bacterial RNA polymerase-sigma54 holoenzyme"
            " transcription open complex",
            "Glyde R et al.\nStructures of Bacterial RNA Polymerase Complexes Reveal "
            "the Mechanism of DNA Loading and Transcription Initiation.\nMol. Cell "
            "(2018) doi:  DOI: doi:10.1016/j.molcel.2018.05.021\npubmed:  "
            "PubMed: 29932903",
            "256 x 256 x 256",
            "1.06",
            "3.4",
            [["EMD-4397", ""]],
            [["6gh5"]],
        )
        assert job.parse_emdb_header() == expected

    def test_parse_emdb_16489(self):
        f = "emd-16489.xml"
        jobdir = "Fetch/job001"
        xml = os.path.join(self.test_data, "Metadata", f)
        os.makedirs(jobdir)
        shutil.copy(xml, os.path.join(jobdir, f))
        job = new_job_of_type("pipeliner.fetch.emdb")
        job.output_dir = jobdir
        job.joboptions["db_id"].value = "16489"
        expected = ("EMD-16489", "", "200 x 200 x 200", "1.327", "3.4", [], [["8c8o"]])
        actual = job.parse_emdb_header()
        assert actual == expected

    def test_parse_pdb_cif_data(self):
        f = "2onv.cif"
        jobdir = "Fetch/job001"
        xml = os.path.join(self.test_data, "Metadata", f)
        os.makedirs(jobdir)
        shutil.copy(xml, os.path.join(jobdir, f))
        job = new_job_of_type("pipeliner.fetch.pdb")
        job.output_dir = jobdir
        job.output_nodes = [
            create_node(os.path.join(jobdir, f), f"{NODE_ATOMCOORDS}.cif.from_pdb")
        ]
        job.joboptions["db_id"].value = "2onv"
        expected_ref = {
            "authors": [
                "Sawaya, M.R.",
                "Sambashivan, S.",
                "Nelson, R.",
                "Ivanova, M.I.",
                "Sievers, S.A.",
                "et al.",
            ],
            "title": "Atomic structures of amyloid cross-beta spines reveal varied"
            " steric zippers.",
            "journal": "Nature",
            "year": "2007",
            "volume": "447",
            "issue": "",
            "pages": "453",
            "doi": "10.1038/nature05695",
            "other_metadata": {"pubmed": "17468747"},
        }
        assert job.get_pdb_cite()[0][0].__dict__ == expected_ref
        assert job.get_pdb_cite()[1] == "1.600"

    def test_parse_pdb_pdb_data(self):
        f = "2onv.pdb"
        jobdir = "Fetch/job001"
        xml = os.path.join(self.test_data, "Metadata", f)
        os.makedirs(jobdir)
        shutil.copy(xml, os.path.join(jobdir, f))
        job = new_job_of_type("pipeliner.fetch.pdb")
        job.output_dir = jobdir
        job.output_nodes = [
            create_node(os.path.join(jobdir, f), f"{NODE_ATOMCOORDS}.pdb.from_pdb")
        ]
        job.joboptions["db_id"].value = "2onv"
        expected_ref = {
            "authors": [
                "M.R.Sawaya",
                "S.Sambashivan",
                "R.Nelson",
                "M.I.Ivanova",
                "S.A.Sievers",
                "et al.",
            ],
            "title": "Atomic structures of amyloid cross-beta spines reveal varied"
            " steric zippers. ",
            "journal": "Nature",
            "year": "2007",
            "volume": "447",
            "issue": "",
            "pages": "453",
            "doi": "10.1038/NATURE05695",
            "other_metadata": {"pubmed": "17468747"},
        }
        assert job.get_pdb_cite()[0][0].__dict__ == expected_ref
        assert job.get_pdb_cite()[1] == "1.61"

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_alphafold_pdb(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_af_pdb_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={
                "AF-F4HVG8-F1-model_v4.pdb": f"{NODE_ATOMCOORDS}.pdb.simulated."
                "alphafold"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-model_"
                "v4.pdb -P Fetch/job999/",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-predicted"
                "_aligned_error_v4.json -P Fetch/job999/",
            ],
        )

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_alphafold_with_fasta(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_af_pdb_fasta_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={
                "AF-F4HVG8-F1-model_v4.pdb": f"{NODE_ATOMCOORDS}.pdb.simulated."
                "alphafold",
                "F4HVG8.fasta": f"{NODE_SEQUENCE}.fasta",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-model_"
                "v4.pdb -P Fetch/job999/",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-predicted"
                "_aligned_error_v4.json -P Fetch/job999/",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://rest.uniprot.org/uniprotkb/F4HVG8.fasta -P Fetch/job999/",
            ],
        )

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_alphafold_mmcif(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_af_mmcif_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={
                "AF-F4HVG8-F1-model_v4.cif": f"{NODE_ATOMCOORDS}.cif.simulated."
                "alphafold"
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-model_"
                "v4.cif -P Fetch/job999/",
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://alphafold.ebi.ac.uk/files/AF-F4HVG8-F1-predicted"
                "_aligned_error_v4.json -P Fetch/job999/",
            ],
        )

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_fasta_from_pdb(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_fasta_pdb_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={
                "5ljo.fasta": f"{NODE_SEQUENCE}.fasta",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://www.ebi.ac.uk/pdbe/entry/pdb/5ljo/fasta -P Fetch/job999/",
                "mv Fetch/job999/fasta Fetch/job999/5ljo.fasta",
            ],
        )

    @patch("pipeliner.jobs.other.fetch_mapmodel_job.url_has_error")
    def test_get_commands_fasta_from_uniprot(self, mock_url_check):
        mock_url_check.return_value = ""
        jobfile = os.path.join(self.jobfiles, "Fetch", "fetch_fasta_uniprot_job.star")
        job_generate_commands_test(
            jobfile=jobfile,
            input_nodes={},
            output_nodes={
                "P0A940.fasta": f"{NODE_SEQUENCE}.fasta",
            },
            expected_commands=[
                f"{shlex.join(get_python_command())} {tools_script} "
                "https://rest.uniprot.org/uniprotkb/P0A940.fasta -P Fetch/job999/",
            ],
        )

    @live_test(condition=dbs_available)
    @patch("shutil.which")
    def test_download_map_model_script(self, mock_which):
        Path("test").mkdir()
        mock_which.return_value = None
        download_map_model.main(
            ["https://files.rcsb.org/download/5LJO.pdb.gz", "-P", "test"]
        )
        assert Path("test/5LJO.pdb.gz").is_file()

    @patch("shutil.which")
    @patch("pipeliner.scripts.job_scripts.download_map_model.curl_dl")
    def test_download_map_model_script_curl(self, mock_curl, mock_which):
        Path("test").mkdir()
        mock_which.side_effect = lambda x: "curl" if x == "curl" else None
        download_map_model.main(
            ["https://files.rcsb.org/download/5LJO.pdb.gz", "-P", "test"]
        )
        mock_curl.assert_called_with(
            url="https://files.rcsb.org/download/5LJO.pdb.gz", directory_prefix="test"
        )

    @live_test(condition=dbs_available, programs=["curl"])
    @patch("shutil.which")
    def test_download_map_model_script_curl_live(self, mock_which):
        Path("test").mkdir()
        mock_which.side_effect = lambda x: "curl" if x == "curl" else None
        download_map_model.main(
            ["https://files.rcsb.org/download/5LJO.pdb.gz", "-P", "test"]
        )
        assert Path("test/5LJO.pdb.gz").is_file()

    @live_test(condition=dbs_available, programs=["wget"])
    @patch("shutil.which")
    def test_download_map_model_script_wget_live(self, mock_which):
        Path("test").mkdir()
        mock_which.side_effect = lambda x: "wget" if x == "wget" else None
        download_map_model.main(
            ["https://files.rcsb.org/download/5LJO.pdb.gz", "-P", "test"]
        )
        assert Path("test/5LJO.pdb.gz").is_file()

    @patch("shutil.which")
    @patch("pipeliner.scripts.job_scripts.download_map_model.wget_dl")
    def test_download_map_model_script_wget(self, mock_wget, mock_which):
        Path("test").mkdir()
        mock_which.side_effect = lambda x: "wget" if x == "wget" else None
        download_map_model.main(
            ["https://files.rcsb.org/download/5LJO.pdb.gz", "-P", "test"]
        )
        mock_wget.assert_called_with(
            url="https://files.rcsb.org/download/5LJO.pdb.gz", directory_prefix="test"
        )


if __name__ == "__main__":
    unittest.main()
