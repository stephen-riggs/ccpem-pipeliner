#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.nodes import NODE_DENSITYMAP, NODE_ATOMCOORDS
from pipeliner.results_display_objects import ResultsDisplayMapModel


class MoorhenJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands(self):
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/Moorhen/moorhen_job.star"),
            input_nodes={
                "Import/job001/map.mrc": NODE_DENSITYMAP + ".mrc",
                "Fetch/job002/model.pdb": NODE_ATOMCOORDS + ".pdb",
                "Fetch/job003/model2.pdb": NODE_ATOMCOORDS + ".pdb",
            },
            output_nodes={"model2_moorhen.pdb": NODE_ATOMCOORDS + ".pdb.manual_refine"},
            expected_commands=[
                "cp -r tmp_123456/. Moorhen/job999/",
                "rm -r tmp_123456",
            ],
        )

    def test_create_results_display(self):
        importdir = "Import/job001"
        os.makedirs(importdir)
        shutil.copy(
            os.path.join(self.test_data, "1ake_10A.mrc"), "Import/job001/map.mrc"
        )
        job_display_object_generation_test(
            jobfile=os.path.join(self.test_data, "JobFiles/Moorhen/moorhen_job.star"),
            expected_display_objects=[
                ResultsDisplayMapModel(
                    title="Overlaid map/models",
                    start_collapsed=True,
                    flag="",
                    maps=["Import/job001/map.mrc"],
                    maps_opacity=[0.5],
                    maps_colours=[],
                    models_colours=[],
                    models=[
                        "Fetch/job002/model.pdb",
                        "Moorhen/job999/model2_moorhen.pdb",
                    ],
                    maps_data="Import/job001/map.mrc",
                    models_data=(
                        "Fetch/job002/model.pdb, " "Moorhen/job999/model2_moorhen.pdb"
                    ),
                    associated_data=[
                        "Import/job001/map.mrc",
                        "Fetch/job002/model.pdb",
                        "Moorhen/job999/model2_moorhen.pdb",
                    ],
                ),
            ],
            files_to_create={},
        )
