#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.results_display_objects import (
    ResultsDisplayMapModel,
    ResultsDisplayMontage,
)


class AlignSymmetryJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_standard(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/AlignSymmetry/align_symmetry_job.star"
            ),
            input_nodes={"Refine3D/job001/mymap.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"aligned.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_align_symmetry --i Refine3D/job001/mymap.mrc --sym C1 --o "
                "AlignSymmetry/job999/aligned.mrc --box_size 64 --nr_uniform 400",
            ],
        )

    def test_get_commands_advanced(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data,
                "JobFiles/AlignSymmetry/align_symmetry_norecentre_job.star",
            ),
            input_nodes={"Refine3D/job001/mymap.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"aligned.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_align_symmetry --i Refine3D/job001/mymap.mrc --sym C1 --o "
                "AlignSymmetry/job999/aligned.mrc --box_size 64 --nr_uniform 400 "
                "--no_recentre --only_rot True",
            ],
        )

    def test_get_metadata(self):
        os.makedirs("AlignSymmetry/job999")
        shutil.copy(
            Path(self.test_data) / "JobFiles/AlignSymmetry/run.out",
            "AlignSymmetry/job999/run.out",
        )
        job = new_job_of_type("relion.map_utilities.align_symmetry")
        job.output_dir = "AlignSymmetry/job999"
        md = job.gather_metadata()
        assert md == {
            "applied_rotations": {"psi": 420.8, "rot": 359.983, "tilt": 10.55}
        }

    def test_get_results_display(self):
        exp_rdo = [
            ResultsDisplayMontage(
                title="Map slices: Map aligned on symmetry axes",
                start_collapsed=False,
                flag="",
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "AlignSymmetry/job999/aligned.mrc: xy",
                    "AlignSymmetry/job999/aligned.mrc: xz",
                    "AlignSymmetry/job999/aligned.mrc: yz",
                ],
                associated_data=["AlignSymmetry/job999/aligned.mrc"],
                img="AlignSymmetry/job999/Thumbnails/slices_montage_000.png",
            ),
            ResultsDisplayMapModel(
                title="3D viewer: Map aligned on symmetry axes",
                flag="",
                maps=["AlignSymmetry/job999/aligned.mrc"],
                maps_opacity=[1.0],
                maps_colours=[],
                models_colours=[],
                models=[],
                maps_data="AlignSymmetry/job999/aligned.mrc",
                models_data="",
                associated_data=["AlignSymmetry/job999/aligned.mrc"],
            ),
        ]
        job_display_object_generation_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/AlignSymmetry/align_symmetry_job.star"
            ),
            expected_display_objects=exp_rdo,
            files_to_create={"aligned.mrc": str(Path(self.test_data) / "emd_3488.mrc")},
        )


if __name__ == "__main__":
    unittest.main()
