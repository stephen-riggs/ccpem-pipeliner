#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.results_display_objects import (
    ResultsDisplayMapModel,
    ResultsDisplayMontage,
)


class ApplySymmetryJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_sym_with_fold(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ApplySymmetry/apply_symmetry_c_job.star"
            ),
            input_nodes={"emd_2660_small.map": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"emd_2660_small_c6sym.mrc": NODE_DENSITYMAP + ".mrc.c6sym"},
            expected_commands=[
                "relion_image_handler --i emd_2660_small.map:mrc --sym C6 "
                "--o ApplySymmetry/job999/emd_2660_small_c6sym.mrc"
            ],
        )

    def test_get_commands_sym_with_fold_multi(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ApplySymmetry/apply_symmetry_c_multi_job.star"
            ),
            input_nodes={
                "emd_2660_small.map": NODE_DENSITYMAP + ".mrc",
                "emd_2660_big.mrc": NODE_DENSITYMAP + ".mrc",
            },
            output_nodes={
                "emd_2660_small_c6sym.mrc": NODE_DENSITYMAP + ".mrc.c6sym",
                "emd_2660_big_c6sym.mrc": NODE_DENSITYMAP + ".mrc.c6sym",
            },
            expected_commands=[
                "relion_image_handler --i emd_2660_small.map:mrc --sym C6 "
                "--o ApplySymmetry/job999/emd_2660_small_c6sym.mrc",
                "relion_image_handler --i emd_2660_big.mrc --sym C6 "
                "--o ApplySymmetry/job999/emd_2660_big_c6sym.mrc",
            ],
        )

    def test_get_commands_sym_without_fold(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ApplySymmetry/apply_symmetry_i_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_isym.mrc": NODE_DENSITYMAP + ".mrc.isym"},
            expected_commands=[
                "relion_image_handler --i test.mrc --sym I"
                " --o ApplySymmetry/job999/test_isym.mrc"
            ],
        )

    def test_creating_results_display(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdos = [
            ResultsDisplayMontage(
                title="Map slices: emd_2660_small_c6sym.mrc with C6 symmetry applied",
                associated_data=["ApplySymmetry/job999/emd_2660_small_c6sym.mrc"],
                img="ApplySymmetry/job999/Thumbnails/slices_montage_000.png",
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "ApplySymmetry/job999/emd_2660_small_c6sym.mrc: xy",
                    "ApplySymmetry/job999/emd_2660_small_c6sym.mrc: xz",
                    "ApplySymmetry/job999/emd_2660_small_c6sym.mrc: yz",
                ],
            ),
            ResultsDisplayMapModel(
                title="3D viewer: emd_2660_small_c6sym.mrc with C6 symmetry applied",
                associated_data=["ApplySymmetry/job999/emd_2660_small_c6sym.mrc"],
                maps=["ApplySymmetry/job999/emd_2660_small_c6sym.mrc"],
                maps_data="ApplySymmetry/job999/emd_2660_small_c6sym.mrc",
            ),
        ]
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ApplySymmetry/apply_symmetry_c_job.star"
            ),
            expected_display_objects=expected_rdos,
            files_to_create={"emd_2660_small_c6sym.mrc": mrc_file},
        )

    def test_creating_results_display_multi(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdos = [
            ResultsDisplayMontage(
                title="Map slices: emd_2660_small_c6sym.mrc with C6 symmetry applied",
                associated_data=["ApplySymmetry/job999/emd_2660_small_c6sym.mrc"],
                img="ApplySymmetry/job999/Thumbnails/slices_montage_000.png",
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "ApplySymmetry/job999/emd_2660_small_c6sym.mrc: xy",
                    "ApplySymmetry/job999/emd_2660_small_c6sym.mrc: xz",
                    "ApplySymmetry/job999/emd_2660_small_c6sym.mrc: yz",
                ],
            ),
            ResultsDisplayMapModel(
                title="3D viewer: emd_2660_small_c6sym.mrc with C6 symmetry applied",
                associated_data=["ApplySymmetry/job999/emd_2660_small_c6sym.mrc"],
                maps=["ApplySymmetry/job999/emd_2660_small_c6sym.mrc"],
                maps_data="ApplySymmetry/job999/emd_2660_small_c6sym.mrc",
            ),
            ResultsDisplayMontage(
                title="Map slices: emd_2660_big_c6sym.mrc with C6 symmetry applied",
                associated_data=["ApplySymmetry/job999/emd_2660_big_c6sym.mrc"],
                img="ApplySymmetry/job999/Thumbnails/slices_montage_001.png",
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "ApplySymmetry/job999/emd_2660_big_c6sym.mrc: xy",
                    "ApplySymmetry/job999/emd_2660_big_c6sym.mrc: xz",
                    "ApplySymmetry/job999/emd_2660_big_c6sym.mrc: yz",
                ],
            ),
            ResultsDisplayMapModel(
                title="3D viewer: emd_2660_big_c6sym.mrc with C6 symmetry applied",
                associated_data=["ApplySymmetry/job999/emd_2660_big_c6sym.mrc"],
                maps=["ApplySymmetry/job999/emd_2660_big_c6sym.mrc"],
                maps_data="ApplySymmetry/job999/emd_2660_big_c6sym.mrc",
            ),
        ]

        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ApplySymmetry/apply_symmetry_c_multi_job.star"
            ),
            expected_display_objects=expected_rdos,
            files_to_create={
                "emd_2660_small_c6sym.mrc": mrc_file,
                "emd_2660_big_c6sym.mrc": mrc_file,
            },
        )


if __name__ == "__main__":
    unittest.main()
