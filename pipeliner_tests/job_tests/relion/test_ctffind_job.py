#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shlex
import shutil
import tempfile
import unittest
from unittest.mock import patch, Mock
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)

from pipeliner.api.manage_project import PipelinerProject
from pipeliner.nodes import NODE_MICROGRAPHGROUPMETADATA, NODE_LOGFILE
from pipeliner.user_settings import get_ctffind_executable, get_gctf_executable
from pipeliner.job_factory import new_job_of_type
from pipeliner.deposition_tools.onedep_deposition_objects import (
    EmSoftware,
    EmCtfCorrection,
)
from pipeliner.pipeliner_job import ExternalProgram
from pipeliner.utils import get_job_script, get_python_command


class CtfFindTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/CtfFind"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctffind(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctffind_ctffind_run.job"),
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
                ".ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 /path/to/relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job999/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "CtfFind/job999/micrographs_ctf.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctffind_relionstyle_jobname(self):
        """Automatic conversion of ambiguous relion style job name"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctffind_ctffind_run_relionstyle.job"),
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 /path/to/relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job999/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "CtfFind/job999/micrographs_ctf.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctffind_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctffind_job.star"),
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 /path/to/relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job999/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "CtfFind/job999/micrographs_ctf.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctffind_continue_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctffind_continue_job.star"),
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 /path/to/relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job999/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --only_do_unfinished --pipeline_control CtfFind/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "CtfFind/job999/micrographs_ctf.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_is_continue_works(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctffind_continue_run.job"),
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 /path/to/relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job999/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --only_do_unfinished --pipeline_control CtfFind/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "CtfFind/job999/micrographs_ctf.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_gctf(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctffind_gctf_run.job"),
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 /path/to/relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job999/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --use_gctf True --gctf_exe"
                f" {get_gctf_executable()} --ignore_ctffind_params --gpu 0:1"
                " --pipeline_control CtfFind/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "CtfFind/job999/micrographs_ctf.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_gctf_relionstyle_jobname(self):
        """Automatic conversion of ambiguous relion style job name"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctffind_gctf_run_relionstyle.job"),
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "mpirun -n 6 /path/to/relion_run_ctffind_mpi --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job999/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                "--FStep 500 --dAst 100 --use_gctf True --gctf_exe"
                f" {get_gctf_executable()} --ignore_ctffind_params --gpu 0:1"
                " --pipeline_control CtfFind/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "CtfFind/job999/micrographs_ctf.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_nonmpi(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctffind_nompi_run.job"),
            input_nodes={
                "MotionCorr/job002/corrected_micrographs.star": ""
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion",
            },
            output_nodes={
                "micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion."
                "ctf",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctffind",
            },
            expected_commands=[
                "relion_run_ctffind --i "
                "MotionCorr/job002/corrected_micrographs.star --o CtfFind/job999/"
                " --Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 "
                f"--FStep 500 --dAst 100 --ctffind_exe {get_ctffind_executable()}"
                " --ctfWin -1 --is_ctffind4 --fast_search --use_given_ps"
                " --pipeline_control CtfFind/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "CtfFind/job999/micrographs_ctf.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_nofile(self):
        with self.assertRaises(ValueError):
            job_generate_commands_test(
                jobfile=str(self.jobfiles / "ctffind_ctffind_nofile.job"),
                input_nodes={},
                output_nodes={},
                expected_commands=" ",
            )

    def test_ctffind_generate_display_data(self):
        get_relion_tutorial_data(relion_version=4, dirs=["CtfFind", "MotionCorr"])

        project = PipelinerProject()
        job = project.get_job("CtfFind/job003/")
        dispobjs = job.create_results_display()
        expected = [
            {
                "title": "Defocus",
                "dobj_type": "histogram",
                "bins": [4, 7, 6, 4, 3],
                "bin_edges": [
                    0.813312,
                    0.9096186,
                    1.0059252,
                    1.1022318,
                    1.1985384,
                    1.294845,
                ],
                "xlabel": "Defocus (uM)",
                "ylabel": "Micrographs",
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Astigmatism",
                "dobj_type": "histogram",
                "bins": [1, 0, 0, 0, 0, 0, 2, 8, 7, 6],
                "bin_edges": [
                    51.68,
                    54.569,
                    57.458,
                    60.346999999999994,
                    63.236,
                    66.125,
                    69.014,
                    71.90299999999999,
                    74.792,
                    77.681,
                    80.57,
                ],
                "xlabel": "Astigmatism (Angstrom)",
                "ylabel": "Micrographs",
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Resolution of good Thon ring fit",
                "dobj_type": "histogram",
                "bins": [8, 6, 5, 2, 2, 0, 0, 0, 0, 0, 0, 1],
                "bin_edges": [
                    3.0,
                    3.15,
                    3.3,
                    3.45,
                    3.6,
                    3.75,
                    3.9,
                    4.05,
                    4.2,
                    4.35,
                    4.5,
                    4.65,
                    4.8,
                ],
                "xlabel": "Resolution (Angstrom)",
                "ylabel": "Micrographs",
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Per-micrograph defocus",
                "start_collapsed": False,
                "dobj_type": "graph",
                "flag": "",
                "xvalues": [
                    [
                        1.082769,
                        0.796313,
                        1.038126,
                        0.958674,
                        1.119836,
                        0.979985,
                        0.8923639999999999,
                        1.0365280000000001,
                        1.275936,
                        0.9805530000000001,
                        1.135205,
                        1.057579,
                        1.074288,
                        1.212013,
                        1.1169040000000001,
                        1.081931,
                        0.8828370000000001,
                        0.8101470000000001,
                        0.980125,
                        0.980858,
                        0.882692,
                        0.970859,
                        1.2778049999999999,
                        1.1828,
                    ]
                ],
                "xaxis_label": "Defocus x (μm)",
                "xrange": [0.796313, 1.2778049999999999],
                "yvalues": [
                    [
                        1.111626,
                        0.830311,
                        1.0656,
                        0.983651,
                        1.148599,
                        1.013594,
                        0.929225,
                        1.0678040000000002,
                        1.3065129999999998,
                        1.0159639999999999,
                        1.169346,
                        1.086415,
                        1.103049,
                        1.212013,
                        1.145648,
                        1.108983,
                        0.911666,
                        0.834485,
                        1.010842,
                        1.0122799999999998,
                        0.9144610000000001,
                        1.007535,
                        1.311885,
                        1.210985,
                    ]
                ],
                "yaxis_label": "Defocus y (μm)",
                "yrange": [0.830311, 1.311885],
                "associated_data": ["CtfFind/job003/micrographs_ctf.star"],
                "data_series_labels": ["All micrographs"],
                "modes": ["markers"],
            },
        ]

        for i in dispobjs:
            if i.title != "Example power spectra":
                if i.__dict__ not in expected:
                    print(expected[3])
                    print(i.__dict__)
                assert i.__dict__ in expected
            else:
                # can't just compare to dict because images are random
                assert i.title == "Example power spectra"
                assert i.dobj_type == "montage"
                assert i.flag == ""
                assert i.xvalues == [0, 1, 2, 3]
                assert i.yvalues == [0, 0, 0, 0]
                assert len(i.labels) == 4
                assert len(i.associated_data) == 4
                assert i.img == "CtfFind/job003/Thumbnails/montage_f000.png"
        dicts = [x.__dict__ for x in dispobjs]
        for i in expected:
            assert i in dicts

    def test_ctffind_generate_display_data_nologfiles(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        tdata = os.getenv("PIPELINER_RELION4_TUTORIAL_DATA")
        get_relion_tutorial_data(relion_version=4, dirs=["MotionCorr"])
        os.makedirs("CtfFind/job003/Movies")
        shutil.copy(
            os.path.join(tdata, "CtfFind/job003/job.star"), "CtfFind/job003/job.star"
        )

        project = PipelinerProject()
        job = project.get_job("CtfFind/job003/")
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "Results pending...",
            "dobj_type": "pending",
            "message": "The result not available yet",
            "reason": "No log files found",
            "start_collapsed": False,
            "flag": "",
        }

    @staticmethod
    def test_get_ondep_objs_ctffind4():
        job = new_job_of_type("relion.ctffind.ctffind4")
        job.joboptions["input_star_mics"].value = "MotionCor/job002/micrographs.star"
        depobjs = job.prepare_deposition_data("ONEDEP")
        expected = [
            EmCtfCorrection(
                id=None,
                image_processing_id="JOBREF: MotionCor/job002/",
                type="PHASE FLIPPING AND AMPLITUDE CORRECTION",
                details=None,
            ),
            EmSoftware(
                id=None,
                category="CTF CORRECTION",
                details=None,
                name="ctffind4",
                version=ExternalProgram(get_ctffind_executable()).get_version(),
                image_processing_id=None,
                fitting_id=None,
                imaging_id=None,
            ),
        ]
        assert depobjs == expected

    @staticmethod
    def test_get_ondep_objs_gctf():
        job = new_job_of_type("relion.ctffind.gctf")
        job.joboptions["input_star_mics"].value = "MotionCor/job002/micrographs.star"
        depobjs = job.prepare_deposition_data("ONEDEP")
        expected = [
            EmCtfCorrection(
                id=None,
                image_processing_id="JOBREF: MotionCor/job002/",
                type="PHASE FLIPPING AND AMPLITUDE CORRECTION",
                details=None,
            ),
            EmSoftware(
                id=None,
                category="CTF CORRECTION",
                details=None,
                name="gctf",
                version=ExternalProgram(get_gctf_executable()).get_version(),
                image_processing_id=None,
                fitting_id=None,
                imaging_id=None,
            ),
        ]
        assert depobjs == expected

    def test_validate_float16_joboptions(self):
        mcpath = Path("MotionCorr/job002/")
        mcpath.mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "JobFiles/MotionCorr/motioncorr_own_job.star",
            mcpath / "job.star",
        )
        job = new_job_of_type("relion.ctffind.ctffind4")
        job.joboptions["input_star_mics"].value = "MotionCorr/job002/micrographs.star"
        job.joboptions["use_given_ps"].value = "No"
        val = job.additional_joboption_validation()
        assert val[0].raised_by[0].label == "Use power spectra from MotionCorr job?"
        assert val[0].type == "error"
        assert val[0].message == (
            "Input micrographs appear to be in float16, pre-calculated power spectra "
            "should be used"
        )

    def test_validate_float16_joboptions_no_file_to_check(self):
        job = new_job_of_type("relion.ctffind.ctffind4")
        job.joboptions["input_star_mics"].value = "MotionCorr/job002/micrographs.star"
        job.joboptions["use_given_ps"].value = "No"
        val = job.additional_joboption_validation()
        assert not val

    def test_warning_if_precalc_PS_dont_exist(self):
        mcpath = Path("MotionCorr/job002/")
        mcpath.mkdir(parents=True)
        shutil.copy(
            Path(self.test_data) / "JobFiles/MotionCorr/motioncorr_no_ps_job.star",
            mcpath / "job.star",
        )
        job = new_job_of_type("relion.ctffind.ctffind4")
        job.joboptions["input_star_mics"].value = "MotionCorr/job002/micrographs.star"
        job.joboptions["use_given_ps"].value = "Yes"
        val = job.additional_joboption_validation()
        assert val[0].raised_by[0].label == "Use power spectra from MotionCorr job?"
        assert val[0].type == "warning"
        assert val[0].message == (
            "It does not appear the job that created the input file wrote the "
            "necessary power spectra files to use this option"
        )

    def test_warning_if_precalc_PS_no_file_to_check(self):
        job = new_job_of_type("relion.ctffind.ctffind4")
        job.joboptions["input_star_mics"].value = "MotionCorr/job002/micrographs.star"
        job.joboptions["use_given_ps"].value = "No"
        val = job.additional_joboption_validation()
        assert not val


if __name__ == "__main__":
    unittest.main()
