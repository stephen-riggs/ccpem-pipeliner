#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from unittest.mock import patch, Mock
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)

from pipeliner.api.manage_project import PipelinerProject
from pipeliner.data_structure import (
    NODE_PARTICLEGROUPMETADATA,
    NODE_LOGFILE,
    NODE_PROCESSDATA,
)


class CtfRefineTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/CtfRefine"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)
        os.mkdir("CtfRefine")
        os.mkdir("CtfRefine/job999")

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctfrefine_params_only_defocus(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_defocus.job"),
            input_nodes={
                "Refine3D/job600/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job700/postprocess.star": "ProcessData.star.relion."
                "postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.ctfrefine.ctf_params",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job999/"
                " --fit_defocus --kmin_defocus 29 --kmin_bfact 29 --fit_mode fmfff "
                "--j 8 --pipeline_control CtfRefine/job999/",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctfrefine_params_only_defocus_astig(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_defocus_astig.job"),
            input_nodes={
                "Refine3D/job600/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job700/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.ctfrefine.ctf_params",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job999/"
                " --fit_defocus --kmin_defocus 29 --kmin_bfact 29 --fit_mode fmmff "
                "--j 8 --pipeline_control CtfRefine/job999/",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctfrefine_params_only_defocus_astig_bfact(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_defocus_astig_bf.job"),
            input_nodes={
                "Refine3D/job600/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job700/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion.ctfrefine.ctf_params",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job999/"
                " --fit_defocus --kmin_defocus 29 --kmin_bfact 29 --fit_mode "
                "fppfp --j 8 --pipeline_control CtfRefine/job999/",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctfrefine_params_only_focus_astig_bfact_phase(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_defocus_astig_bf_ps.job"),
            input_nodes={
                "Refine3D/job600/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job700/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion.ctfrefine.ctf_params",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job999/"
                " --fit_defocus --kmin_defocus 29 --kmin_bfact 29 --fit_mode mppfp "
                "--j 8 --pipeline_control CtfRefine/job999/",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctfrefine_continue_aniso_mag_only(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_continue.job"),
            input_nodes={
                "Refine3D/job600/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job700/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}."
                "star.relion.ctfrefine.anisotropic_magnification",
            },
            expected_commands=[
                "mpirun -n 16 /path/to/relion_ctf_refine_mpi --i "
                "Refine3D/job600/run_data.star --f "
                "PostProcess/job700/postprocess.star --o CtfRefine/job999/ "
                "--only_do_unfinished --fit_aniso --kmin_mag 29 --j 8 "
                "--pipeline_control CtfRefine/job999/",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_get_command_ctf_refine_aberration_only(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_aberr_only_job.star"),
            input_nodes={
                "Refine3D/job001/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job002/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion.ctfrefine.higher_order_aberrations",
            },
            expected_commands=[
                "relion_ctf_refine --i Refine3D/job001/run_data.star --f "
                "PostProcess/job002/postprocess.star --o CtfRefine/job999/ "
                "--fit_beamtilt --kmin_tilt 30 --odd_aberr_max_n 3 --fit_aberr"
                " --kmin_aberr 30 --j 1",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_ctfrefine_aberr_then_aniso(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_aberr_aniso_job.star"),
            input_nodes={
                "Refine3D/job001/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job002/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion.ctfrefine.higher_order_aberrations.anisotropic_"
                "magnification",
            },
            expected_commands=[
                "relion_ctf_refine --i Refine3D/job001/run_data.star --f "
                "PostProcess/job002/postprocess.star --o CtfRefine/job999/ "
                "--fit_beamtilt --kmin_tilt 30 --odd_aberr_max_n 3 --fit_aberr "
                "--kmin_aberr 30 --j 1",
                "cp CtfRefine/job999/particles_ctf_refine.star "
                "CtfRefine/job999/particles_ctf_refine_aberrations.star",
                "relion_ctf_refine --i CtfRefine/job999/particles_ctf_refine_"
                "aberrations.star --f PostProcess/job002/postprocess.star --o "
                "CtfRefine/job999/ --fit_aniso --kmin_mag 30 --j 1 --pipeline_control "
                "CtfRefine/job999/",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_ctfrefine_aberr_then_ctf_params(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_no_aniso_job.star"),
            input_nodes={
                "Refine3D/job001/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job002/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion.ctfrefine.higher_order_aberrations.ctf_params",
            },
            expected_commands=[
                "relion_ctf_refine --i Refine3D/job001/run_data.star --f "
                "PostProcess/job002/postprocess.star --o CtfRefine/job999/ "
                "--fit_beamtilt --kmin_tilt 30 --odd_aberr_max_n 3 --fit_aberr "
                "--kmin_aberr 30 --j 1",
                "cp CtfRefine/job999/particles_ctf_refine.star "
                "CtfRefine/job999/particles_ctf_refine_aberrations.star",
                "relion_ctf_refine --i CtfRefine/job999/particles_ctf_refine_"
                "aberrations.star --f PostProcess/job002/postprocess.star --o "
                "CtfRefine/job999/ --fit_defocus --kmin_defocus 30 --kmin_bfact 30 "
                "--fit_mode pppfp --j 1 --pipeline_control CtfRefine/job999/",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_ctfrefine_aniso_then_params(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_no_aberr_job.star"),
            input_nodes={
                "Refine3D/job001/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job002/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion.ctfrefine.anisotropic_magnification.ctf_params",
            },
            expected_commands=[
                "relion_ctf_refine --i Refine3D/job001/run_data.star --f "
                "PostProcess/job002/postprocess.star --o CtfRefine/job999/ "
                "--fit_aniso --kmin_mag 30 --j 1 --pipeline_control CtfRefine/job999/",
                "cp CtfRefine/job999/particles_ctf_refine.star "
                "CtfRefine/job999/particles_ctf_refine_aniso_mag.star",
                "relion_ctf_refine --i CtfRefine/job999/particles_ctf_refine_"
                "aniso_mag.star --f PostProcess/job002/postprocess.star --o "
                "CtfRefine/job999/ --fit_defocus --kmin_defocus 30 --kmin_bfact 30"
                " --fit_mode pppfp --j 1 --pipeline_control CtfRefine/job999/",
            ],
        )

    @patch.object(shutil, "which", Mock(side_effect=lambda cmd: "/path/to/" + cmd))
    def test_ctfrefine_everything(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "ctfrefine_everything_job.star"),
            input_nodes={
                "Refine3D/job001/run_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion",
                "PostProcess/job002/postprocess.star": f"{NODE_PROCESSDATA}.star."
                "relion.postprocess",
            },
            output_nodes={
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.ctfrefine",
                "particles_ctf_refine.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion.ctfrefine.higher_order_aberrations.anisotropic_"
                "magnification.ctf_params",
            },
            expected_commands=[
                "relion_ctf_refine --i Refine3D/job001/run_data.star --f "
                "PostProcess/job002/postprocess.star --o CtfRefine/job999/ "
                "--fit_beamtilt --kmin_tilt 30 --odd_aberr_max_n 3 --fit_aberr"
                " --kmin_aberr 30 --j 1",
                "cp CtfRefine/job999/particles_ctf_refine.star "
                "CtfRefine/job999/particles_ctf_refine_aberrations.star",
                "relion_ctf_refine --i CtfRefine/job999/particles_ctf_refine_"
                "aberrations.star --f PostProcess/job002/postprocess.star --o "
                "CtfRefine/job999/ --fit_aniso --kmin_mag 30 --j 1 --pipeline_control "
                "CtfRefine/job999/",
                "cp CtfRefine/job999/particles_ctf_refine.star "
                "CtfRefine/job999/particles_ctf_refine_aniso_mag.star",
                "relion_ctf_refine --i CtfRefine/job999/particles_ctf_refine_aniso_"
                "mag.star --f PostProcess/job002/postprocess.star --o CtfRefine/job999/"
                " --fit_defocus --kmin_defocus 30 --kmin_bfact 30 --fit_mode pppfp "
                "--j 1 --pipeline_control CtfRefine/job999/",
            ],
        )

    def test_ctfrefine_generate_display_data_aberration_corr(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        get_relion_tutorial_data(relion_version=4, dirs=["CtfRefine"])

        project = PipelinerProject()
        job = project.get_job("CtfRefine/job022/")
        dispobjs = job.create_results_display()

        expected = {
            "title": "CTF refinement graphical ouputs",
            "dobj_type": "montage",
            "xvalues": [0, 1, 0, 1, 0, 1, 0, 1],
            "yvalues": [3, 3, 2, 2, 1, 1, 0, 0],
            "labels": [
                "aberr_delta-phase_iter-fit_optics-group_1_N-4",
                "aberr_delta-phase_lin-fit_optics-group_1_N-4",
                "aberr_delta-phase_lin-fit_optics-group_1_N-4_residual",
                "aberr_delta-phase_per-pixel_optics-group_1",
                "beamtilt_delta-phase_iter-fit_optics-group_1_N-3",
                "beamtilt_delta-phase_lin-fit_optics-group_1_N-3",
                "beamtilt_delta-phase_lin-fit_optics-group_1_N-3_residual",
                "beamtilt_delta-phase_per-pixel_optics-group_1",
            ],
            "associated_data": [
                "CtfRefine/job022/aberr_delta-phase_iter-fit_optics-group_1_N-4.mrc",
                "CtfRefine/job022/aberr_delta-phase_lin-fit_optics-group_1_N-4.mrc",
                "CtfRefine/job022/aberr_delta-phase_lin-fit_optics-group_1_N-4"
                "_residual.mrc",
                "CtfRefine/job022/aberr_delta-phase_per-pixel_optics-group_1.mrc",
                "CtfRefine/job022/beamtilt_delta-phase_iter-fit_optics-group_1_N-3.mrc",
                "CtfRefine/job022/beamtilt_delta-phase_lin-fit_optics-group_1_N-3.mrc",
                "CtfRefine/job022/beamtilt_delta-phase_lin-fit_optics-group_1_N-3"
                "_residual.mrc",
                "CtfRefine/job022/beamtilt_delta-phase_per-pixel_optics-group_1.mrc",
            ],
            "img": "CtfRefine/job022/Thumbnails/montage_f000.png",
            "start_collapsed": False,
            "flag": "",
        }

        assert dispobjs[0].__dict__ == expected

    def test_ctfrefine_generate_display_data_anisomag_corr(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        get_relion_tutorial_data(relion_version=4, dirs=["CtfRefine"])

        project = PipelinerProject()
        job = project.get_job("CtfRefine/job023/")
        dispobjs = job.create_results_display()

        expected = {
            "title": "Anisotropic magnification graphical ouputs",
            "dobj_type": "montage",
            "xvalues": [0, 1, 0, 1],
            "yvalues": [1, 1, 0, 0],
            "labels": [
                "mag_disp_x_fit_optics-group_1",
                "mag_disp_x_optics-group_1",
                "mag_disp_y_fit_optics-group_1",
                "mag_disp_y_optics-group_1",
            ],
            "associated_data": [
                "CtfRefine/job023/mag_disp_x_fit_optics-group_1.mrc",
                "CtfRefine/job023/mag_disp_x_optics-group_1.mrc",
                "CtfRefine/job023/mag_disp_y_fit_optics-group_1.mrc",
                "CtfRefine/job023/mag_disp_y_optics-group_1.mrc",
            ],
            "img": "CtfRefine/job023/Thumbnails/montage_f000.png",
            "start_collapsed": False,
            "flag": "",
        }
        assert dispobjs[0].__dict__ == expected

    def test_ctfrefine_generate_display_data_per_particle(self):
        """If there are no logfiles should return a ResultsDisplayPending"""
        get_relion_tutorial_data(relion_version=4, dirs=["CtfRefine"])

        project = PipelinerProject()
        job = project.get_job("CtfRefine/job024/")
        dispobjs = job.create_results_display()

        expected = {
            "title": "Ctf refinements performed",
            "dobj_type": "table",
            "headers": ["Refinement type", "Basis"],
            "header_tooltips": ["Refinement type", "Basis"],
            "table_data": [
                ["defocus", "Per-particle"],
                ["astigmatism", "Per-micrograph"],
                ["phase shift", "Not performed"],
                ["b-factor", "Not performed"],
            ],
            "associated_data": ["CtfRefine/job024/particles_ctf_refine.star"],
            "start_collapsed": False,
            "flag": "",
        }

        assert dispobjs[0].__dict__ == expected


if __name__ == "__main__":
    unittest.main()
