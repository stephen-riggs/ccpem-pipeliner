#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.results_display_objects import (
    ResultsDisplayMapModel,
    ResultsDisplayMontage,
)


class FlipHandednessJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands(self):
        job_generate_commands_test(
            jobfile=os.path.join(self.test_data, "JobFiles/FlipMap/flip_map_job.star"),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_flipped.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i test.mrc --invert_hand --o "
                "Flip/job999/test_flipped.mrc"
            ],
        )

    def test_get_commands_multi(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FlipMap/flip_map_multi_job.star"
            ),
            input_nodes={
                "test.mrc": NODE_DENSITYMAP + ".mrc",
                "test2.mrc": NODE_DENSITYMAP + ".mrc",
            },
            output_nodes={
                "test_flipped.mrc": NODE_DENSITYMAP + ".mrc",
                "test2_flipped.mrc": NODE_DENSITYMAP + ".mrc",
            },
            expected_commands=[
                "relion_image_handler --i test.mrc --invert_hand --o "
                "Flip/job999/test_flipped.mrc",
                "relion_image_handler --i test2.mrc --invert_hand --o "
                "Flip/job999/test2_flipped.mrc",
            ],
        )

    def test_get_commands_mapext(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FlipMap/flip_map_mapext_job.star"
            ),
            input_nodes={"emd_2660_small.map": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"emd_2660_small_flipped.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i emd_2660_small.map:mrc --invert_hand --o "
                "Flip/job999/emd_2660_small_flipped.mrc"
            ],
        )

    def test_creating_results_display(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdos = [
            ResultsDisplayMontage(
                title="Map slices: test_flipped.mrc flipped",
                associated_data=["Flip/job999/test_flipped.mrc"],
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "Flip/job999/test_flipped.mrc: xy",
                    "Flip/job999/test_flipped.mrc: xz",
                    "Flip/job999/test_flipped.mrc: yz",
                ],
                img="Flip/job999/Thumbnails/slices_montage_000.png",
            ),
            ResultsDisplayMapModel(
                title="3D viewer: test_flipped.mrc flipped",
                associated_data=["Flip/job999/test_flipped.mrc"],
                maps=["Flip/job999/test_flipped.mrc"],
                maps_data="Flip/job999/test_flipped.mrc",
            ),
        ]
        job_display_object_generation_test(
            jobfile=os.path.join(self.test_data, "JobFiles/FlipMap/flip_map_job.star"),
            expected_display_objects=expected_rdos,
            files_to_create={"test_flipped.mrc": mrc_file},
        )

    def test_creating_results_display_multi(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdos = [
            ResultsDisplayMontage(
                title="Map slices: test_flipped.mrc flipped",
                associated_data=[
                    "Flip/job999/test_flipped.mrc",
                ],
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "Flip/job999/test_flipped.mrc: xy",
                    "Flip/job999/test_flipped.mrc: xz",
                    "Flip/job999/test_flipped.mrc: yz",
                ],
                img="Flip/job999/Thumbnails/slices_montage_000.png",
            ),
            ResultsDisplayMapModel(
                title="3D viewer: test_flipped.mrc flipped",
                associated_data=["Flip/job999/test_flipped.mrc"],
                maps=["Flip/job999/test_flipped.mrc"],
                maps_data="Flip/job999/test_flipped.mrc",
            ),
            ResultsDisplayMontage(
                title="Map slices: test2_flipped.mrc flipped",
                associated_data=["Flip/job999/test2_flipped.mrc"],
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "Flip/job999/test2_flipped.mrc: xy",
                    "Flip/job999/test2_flipped.mrc: xz",
                    "Flip/job999/test2_flipped.mrc: yz",
                ],
                img="Flip/job999/Thumbnails/slices_montage_001.png",
            ),
            ResultsDisplayMapModel(
                title="3D viewer: test2_flipped.mrc flipped",
                associated_data=["Flip/job999/test2_flipped.mrc"],
                maps=["Flip/job999/test2_flipped.mrc"],
                maps_data="Flip/job999/test2_flipped.mrc",
            ),
        ]
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/FlipMap/flip_map_multi_job.star"
            ),
            expected_display_objects=expected_rdos,
            files_to_create={
                "test_flipped.mrc": mrc_file,
                "test2_flipped.mrc": mrc_file,
            },
        )


if __name__ == "__main__":
    unittest.main()
