#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import os
import shutil
import tempfile
import unittest
from unittest.mock import patch
from pathlib import Path

from pipeliner import job_factory
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)

from pipeliner.api.manage_project import PipelinerProject
from pipeliner.data_structure import (
    RELION_SUCCESS_FILE,
    NODE_PARTICLEGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_OPTIMISERDATA,
)


class InitialModelTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/InitialModel"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch.dict(os.environ)
    def test_get_scratch_env_var(self):
        # Ensure RELION_SCRATCH_DIR is unset to begin with
        os.environ.pop("RELION_SCRATCH_DIR", None)

        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/InitialModel/initialmodel.job")
        )
        assert job.joboptions["scratch_dir"].default_value == ""

        os.environ["RELION_SCRATCH_DIR"] = os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/InitialModel/initialmodel.job")
        )
        assert job.joboptions["scratch_dir"].default_value == os.path.join(
            self.test_data, "fake_SCRATCH_dir"
        )

    def test_get_command_inimodel(self):
        """Test with run.job file, default params"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "initialmodel.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "run_it200_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "initialmodel",
                "run_it200_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "initialmodel",
                "run_it200_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "initial_model.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job999/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job999/",
                "rm -f InitialModel/job999/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job999/run_it200_model.star"
                " --o InitialModel/job999/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class",
            ],
        )

    def test_get_command_inimodel_jobstar(self):
        """Test with job.star file default params"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "initialmodel_job.star"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion"
            },
            output_nodes={
                "run_it200_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "initialmodel",
                "run_it200_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "initialmodel",
                "run_it200_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "initial_model.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job999/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job999/",
                "rm -f InitialModel/job999/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job999/run_it200_model.star"
                " --o InitialModel/job999/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class",
            ],
        )

    def test_get_command_inimodel_3classes_c1(self):
        """Three classes with c1 symmetry"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "initialmodel_3classes.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}."
                "star.relion"
            },
            output_nodes={
                "run_it200_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "initialmodel",
                "run_it200_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "initialmodel",
                "run_it200_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "run_it200_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "run_it200_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "initial_model.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job999/run --iter 200 --ctf --K 3 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job999/",
                "rm -f InitialModel/job999/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job999/run_it200_model.star"
                " --o InitialModel/job999/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class",
            ],
        )

    def test_get_command_inimodel_continue(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "initialmodel_continue.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion",
                "InitialModel/job999/run_it200_optimiser.star": f"{NODE_OPTIMISERDATA}"
                ".star.relion.initialmodel",
            },
            output_nodes={
                "run_it300_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "initialmodel",
                "run_it300_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "initialmodel",
                "run_it300_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "initial_model.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --continue InitialModel/job999/run_it200_optimiser.star"
                " --o InitialModel/job999/run --iter 300 --ctf --K 1 --sym C1"
                " --flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job999/",
                "rm -f InitialModel/job999/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i "
                "InitialModel/job999/run_it300_model.star --o "
                "InitialModel/job999/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class",
            ],
        )

    def test_get_command_inimodel_finesampling(self):
        """Test with job.star file finer sampling"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "initialmodel_finesampling.job"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}."
                "star.relion"
            },
            output_nodes={
                "run_it200_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "initialmodel",
                "run_it200_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "initialmodel",
                "run_it200_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "initial_model.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job999/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 "
                "--particle_diameter 200 --oversampling 1 --healpix_order 5 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job999/",
                "rm -f InitialModel/job999/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job999/run_it200_model.star"
                " --o InitialModel/job999/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class",
            ],
        )

    def test_get_command_inimodel_with_sym_runas_c1(self):
        """single class non-c1 symmetry, symmetry applied after refinement"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "initialmodel_c4_job.star"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "run_it200_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "initialmodel",
                "run_it200_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "initialmodel",
                "run_it200_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "initial_model.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel.c4sym",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job999/run --iter 200 --ctf --K 1 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job999/",
                "rm -f InitialModel/job999/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job999/run_it200_model.star"
                " --o InitialModel/job999/initial_model.mrc --sym C4 --apply_sym "
                "--select_largest_class",
            ],
        )

    def test_get_command_inimodel_with_sym(self):
        """single class non-c1 symmetry, symmetry applied during refinement"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "initialmodel_c4_inrefine_job.star"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion"
            },
            output_nodes={
                "run_it200_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "initialmodel",
                "run_it200_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "initialmodel",
                "run_it200_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel"
                + ".c4sym",
                "initial_model.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel.c4sym",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job999/run --iter 200 --ctf --K 1 --sym C4 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job999/",
                "rm -f InitialModel/job999/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job999/run_it200_model.star "
                "--o InitialModel/job999/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class",
            ],
        )

    def test_get_command_inimodel_with_sym_runas_c1_multiclass(self):
        """single class non-C1 symmetry, symmetry applied after refinement with
        multiple classes"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "initialmodel_c4_multiclass_job.star"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion"
            },
            output_nodes={
                "run_it200_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "initialmodel",
                "run_it200_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "initialmodel",
                "run_it200_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "run_it200_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "run_it200_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
                "initial_model.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel"
                + ".c4sym",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job999/run --iter 200 --ctf --K 3 --sym C1 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job999/",
                "rm -f InitialModel/job999/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job999/run_it200_model.star "
                "--o InitialModel/job999/initial_model.mrc --sym C4 --apply_sym "
                "--select_largest_class",
            ],
        )

    def test_get_command_inimodel_with_sym_multiclass(self):
        """single class non-C1 symmetry, symmetry applied during refinement with
        multiple classes"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "initialmodel_c4_inrefine_multiclass_job.star"),
            input_nodes={
                "Select/job014/particles.star": f"{NODE_PARTICLEGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "run_it200_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "initialmodel",
                "run_it200_data.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion."
                "initialmodel",
                "run_it200_class001.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel"
                + ".c4sym",
                "run_it200_class002.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel"
                + ".c4sym",
                "run_it200_class003.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel"
                + ".c4sym",
                "initial_model.mrc": f"{NODE_DENSITYMAP}.mrc.relion.initialmodel.c4sym",
            },
            expected_commands=[
                "relion_refine --grad --denovo_3dref --i Select/job014/particles.star "
                "--o InitialModel/job999/run --iter 200 --ctf --K 3 --sym C4 "
                "--flatten_solvent --zero_mask --dont_combine_weights_via_disc "
                "--scratch_dir None --pool 3 --pad 2 "
                "--particle_diameter 200 --oversampling 1 --healpix_order 1 "
                "--offset_range 6 --offset_step 4.0 --j 1 --pipeline_control "
                "InitialModel/job999/",
                "rm -f InitialModel/job999/RELION_JOB_EXIT_SUCCESS",
                "relion_align_symmetry --i InitialModel/job999/run_it200_model.star "
                "--o InitialModel/job999/initial_model.mrc --sym C1 --apply_sym "
                "--select_largest_class",
            ],
        )

    def test_create_display_inimod(self):
        get_relion_tutorial_data(relion_version=4, dirs="InitialModel")

        project = PipelinerProject()
        job = project.get_job("InitialModel/job015/")
        dispobjs = job.create_results_display()
        expected_montage = {
            "associated_data": ["InitialModel/job015/initial_model.mrc"],
            "dobj_type": "montage",
            "flag": "",
            "img": "InitialModel/job015/Thumbnails/slices_montage_000.png",
            "labels": [
                "InitialModel/job015/initial_model.mrc: xy",
                "InitialModel/job015/initial_model.mrc: xz",
                "InitialModel/job015/initial_model.mrc: yz",
            ],
            "start_collapsed": False,
            "title": (
                "Map slices: Initial Model - InitialModel/job015/initial_model.mrc"
            ),
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        expected_mm = {
            "maps": ["InitialModel/job015/initial_model.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": "3D viewer: Initial Model - InitialModel/job015/initial_model.mrc",
            "maps_data": "InitialModel/job015/initial_model.mrc",
            "models_data": "",
            "associated_data": ["InitialModel/job015/initial_model.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobjs[0].__dict__ == expected_montage
        assert dispobjs[1].__dict__ == expected_mm

    def test_create_display_inimod_not_finished(self):
        get_relion_tutorial_data(relion_version=4, dirs="InitialModel")
        os.remove(f"InitialModel/job015/{RELION_SUCCESS_FILE}")
        os.remove("InitialModel/job015/run_it100_class001.mrc")
        os.remove("InitialModel/job015/initial_model.mrc")

        project = PipelinerProject()
        job = project.get_job("InitialModel/job015/")
        dispobjs = job.create_results_display()
        expected_montage = {
            "associated_data": ["InitialModel/job015/run_it090_class001.mrc"],
            "dobj_type": "montage",
            "flag": "",
            "img": "InitialModel/job015/Thumbnails/slices_montage_000.png",
            "labels": [
                "InitialModel/job015/run_it090_class001.mrc: xy",
                "InitialModel/job015/run_it090_class001.mrc: xz",
                "InitialModel/job015/run_it090_class001.mrc: yz",
            ],
            "start_collapsed": False,
            "title": (
                "Map slices: Initial model - run_it090_class001.mrc intermediate "
                "result iteration 90/100"
            ),
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        expected_mm = {
            "maps": ["InitialModel/job015/run_it090_class001.mrc"],
            "dobj_type": "mapmodel",
            "maps_opacity": [1.0],
            "models": [],
            "title": (
                "3D viewer: Initial model - run_it090_class001.mrc intermediate result"
                " iteration 90/100"
            ),
            "maps_data": "InitialModel/job015/run_it090_class001.mrc",
            "models_data": "",
            "associated_data": ["InitialModel/job015/run_it090_class001.mrc"],
            "start_collapsed": True,
            "flag": "",
            "maps_colours": [],
            "models_colours": [],
        }
        assert dispobjs[0].__dict__ == expected_montage
        assert dispobjs[1].__dict__ == expected_mm


if __name__ == "__main__":
    unittest.main()
