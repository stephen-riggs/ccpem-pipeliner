#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)
from pipeliner.api.manage_project import PipelinerProject
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROGRAPHCOORDSGROUP,
    NODE_PARTICLEGROUPMETADATA,
)


class ManualPickTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/ManualPick"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_basic(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "manualpick.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_selected.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "manualpick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "manualpick",
            },
            expected_commands=[
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job999/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job999/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --pipeline_control ManualPick/job999/"
            ],
        )

    def test_get_command_helcial(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "manualpick_helical.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_selected.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "manualpick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "manualpick.helixstartend",
            },
            expected_commands=[
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job999/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job999/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --do_startend --pipeline_control ManualPick/job999/"
            ],
        )

    def test_get_command_helcial_relionstyle_jobname(self):
        """Make sure ambiguous relion style job name is converted"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "manualpick_helical_relionstyle.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_selected.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "manualpick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                f"manualpick.helixstartend",
            },
            expected_commands=[
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job999/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job999/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --do_startend --pipeline_control ManualPick/job999/"
            ],
        )

    def test_get_command_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "manualpick_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_selected.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "manualpick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "manualpick",
            },
            expected_commands=[
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job999/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job999/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --pipeline_control ManualPick/job999/"
            ],
        )

    def test_get_command_redblue(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "manualpick_color.job"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "fake_colorfile.star": f"{NODE_PARTICLEGROUPMETADATA}.star",
            },
            output_nodes={
                "micrographs_selected.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "manualpick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "manualpick",
            },
            expected_commands=[
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job999/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job999/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 --lowpass 20 "
                "--angpix 0.885 --particle_diameter 200"
                " --color_label rlnParticleSelectZScore --blue 0 --red 2 "
                "--color_star fake_colorfile.star --pipeline_control "
                "ManualPick/job999/"
            ],
        )

    def test_get_command_topaz_denoise(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "manualpick_topaz_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_selected.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "manualpick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "manualpick",
            },
            expected_commands=[
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job999/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job999/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 "
                "--topaz_denoise --topaz_exe public/EM/TOPAZ/topaz "
                "--particle_diameter 200 --pipeline_control ManualPick/job999/"
            ],
        )

    def test_get_command_topaz_denoise_with_FOM(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "manualpick_topaz_fom_job.star"),
            input_nodes={
                "CtfFind/job003/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_selected.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion",
                "manualpick.star": f"{NODE_MICROGRAPHCOORDSGROUP}.star.relion."
                "manualpick",
            },
            expected_commands=[
                "relion_manualpick --i CtfFind/job003/micrographs_ctf.star"
                " --odir ManualPick/job999/ --pickname manualpick --allow_save"
                " --fast_save --selection ManualPick/job999/micrographs_selected.star"
                " --scale 0.25 --sigma_contrast 3 --black 0 --white 0 "
                "--topaz_denoise --topaz_exe public/EM/TOPAZ/topaz "
                "--particle_diameter 200 --minimum_pick_fom 0.1 --pipeline_control"
                " ManualPick/job999/"
            ],
        )

    def test_manpick_generate_display_data(self):
        get_relion_tutorial_data(
            relion_version=4, dirs=["ManualPick", "CtfFind", "MotionCorr"]
        )

        project = PipelinerProject()
        job = project.get_job("ManualPick/job004/")
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "Example picked particles",
            "dobj_type": "image",
            "image_path": "ManualPick/job004/Thumbnails/picked_coords000.png",
            "image_desc": "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc:"
            " 11 particles",
            "associated_data": [
                "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                "ManualPick/job004/Movies/20170629_00021_frameImage_manualpick.star",
            ],
            "start_collapsed": False,
            "flag": "",
        }

        assert dispobjs[1].__dict__ == {
            "title": "11 picked particles",
            "dobj_type": "histogram",
            "bins": [1],
            "bin_edges": [10.5, 11.5],
            "xlabel": "Number of particles",
            "ylabel": "Micrographs",
            "associated_data": ["ManualPick/job004/manualpick.star"],
            "start_collapsed": False,
            "flag": "",
        }


if __name__ == "__main__":
    unittest.main()
