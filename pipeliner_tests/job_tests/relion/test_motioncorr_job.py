#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
import shlex
from unittest.mock import patch
from pathlib import Path

from pipeliner.api.manage_project import PipelinerProject
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_MICROSCOPEDATA,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
    NODE_LOGFILE,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.deposition_tools.onedep_deposition_objects import (
    EmSoftware,
    EmImageProcessing,
)
from pipeliner.pipeliner_job import ExternalProgram
from pipeliner.utils import get_job_script, get_python_command


class MotionCorrTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/MotionCorr"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_own(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_own.job"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32"
                " --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --float16 --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_own_relionstyle_jobname(self):
        """Make sure ambiguous relion style job name is converted"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_own_relionstyle.job"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32"
                " --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --float16 --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_own_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_own_job.star"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --float16 --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_own_jobstar_nofloat16(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_own_nofloat16_job.star"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_own_continue(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_own_continue.job"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star --o"
                " MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum 0 --use_own"
                " --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1.277 "
                "--preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --grouping_for_ps 3"
                " --float16 --only_do_unfinished --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_own_noDW(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_own_noDW.job"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star"
                " --o MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum"
                " -1 --use_own --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1"
                " --preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref"
                " Movies/gain.mrc --gain_rot 0 --gain_flip 0 --grouping_for_ps 4"
                " --float16 --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_get_command_own_savenoDW(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_own_save_noDW.job"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star"
                " --o MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum"
                " -1 --use_own --j 24 --bin_factor 1 --bfactor 150 --dose_per_frame 1"
                " --preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 "
                "--group_frames 3 --gainref"
                " Movies/gain.mrc --gain_rot 0 --gain_flip 0 --dose_weighting "
                "--save_noDW --float16 --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    @patch("pipeliner.jobs.relion.motioncorr_job.get_motioncor2_executable")
    def test_get_command_mocorr(self, patch_mc):
        patch_mc.return_value = "mocor2"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_mocorr.job"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "mpirun -n 2 relion_run_motioncorr_mpi --i Import/job001/movies.star"
                " --o MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum -1"
                " --use_motioncor2 --motioncor2_exe mocor2 --other_motioncor2_args"
                " --mocorr_test_arg --gpu 0:1 --bin_factor 1 --bfactor 150"
                " --dose_per_frame 1.5 --preexposure 2 --patch_x 5 --patch_y 5"
                " --eer_grouping 32 --group_frames 3 --gainref Movies/gain.mrc"
                " --gain_rot 0 --gain_flip 0 --dose_weighting --save_noDW"
                " --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    @patch("pipeliner.jobs.relion.motioncorr_job.get_motioncor2_executable")
    def test_get_command_mocorr_relionstyle_jobname(self, patch_mc):
        """Make sure ambiguous relion style job name is converted"""
        patch_mc.return_value = "mocor2"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_mocor2_relionstyle_job.star"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "relion_run_motioncorr --i Import/job001/movies.star"
                " --o MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum 0"
                " --use_motioncor2 --motioncor2_exe mocor2"
                " --other_motioncor2_args 'some other args' --gpu 0 --bin_factor 1"
                " --bfactor 150 --dose_per_frame 1.277 --preexposure 0 --patch_x 5"
                " --patch_y 5 --eer_grouping 32 --group_frames 3"
                " --gainref Movies/gain.mrc --gain_rot 0 --gain_flip 0 --dose_weighting"
                " --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    @patch("pipeliner.jobs.relion.motioncorr_job.get_motioncor2_executable")
    def test_get_command_mocorr_defectfile(self, patch_mc):
        patch_mc.return_value = "mocor2"
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "motioncorr_mocorr_defect.job"),
            input_nodes={
                "Import/job001/movies.star": f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star"
                ".relion",
                "Movies/gain.mrc": f"{NODE_MICROSCOPEDATA}.mrc.gainreference",
                "defect_file.mrc": f"{NODE_MICROSCOPEDATA}.mrc.defectfile",
            },
            output_nodes={
                "corrected_micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star."
                "relion.motioncorr",
                "logfile.pdf": f"{NODE_LOGFILE}.pdf.relion.motioncorr",
            },
            expected_commands=[
                "mpirun -n 2 relion_run_motioncorr_mpi --i Import/job001/movies.star"
                " --o MotionCorr/job999/ --first_frame_sum 1 --last_frame_sum -1"
                " --use_motioncor2 --motioncor2_exe mocor2 --other_motioncor2_args"
                " --mocorr_test_arg --gpu 0:1 --defect_file defect_file.mrc"
                " --bin_factor 1 --bfactor 150 --dose_per_frame 1.5 --preexposure 2"
                " --patch_x 5 --patch_y 5 --eer_grouping 32 --group_frames 3"
                " --gainref Movies/gain.mrc --gain_rot 0 --gain_flip 0 --dose_weighting"
                " --save_noDW --pipeline_control MotionCorr/job999/",
                f"{shlex.join(get_python_command())} "
                f"{get_job_script('check_star_file.py')} --fn_in "
                "MotionCorr/job999/corrected_micrographs.star --block micrographs "
                "--clear_relion_success_file",
            ],
        )

    def test_mocorr_generate_results(self):
        get_relion_tutorial_data(relion_version=4, dirs=["MotionCorr"])

        project = PipelinerProject()
        job = project.get_job("MotionCorr/job002/")
        dispobjs = job.create_results_display()

        expfile = os.path.join(self.test_data, "ResultsFiles/mocorr_results.json")
        with open(expfile, "r") as ef:
            exp = json.load(ef)
        assert dispobjs[0].__dict__ == exp
        assert os.path.isfile(dispobjs[0].img)

    @staticmethod
    def test_get_onedep_deposition_objects_own():
        job = new_job_of_type("relion.motioncorr.own")
        job.output_dir = "MotionCorr/job999/"
        job.joboptions["input_star_mics"].value = "Import/job001/movies.star"
        with patch.object(ExternalProgram, "get_version") as mock_vers:
            mock_vers.return_value = "1 Billion"
            dobj = job.prepare_deposition_data("ONEDEP")

        expected = [
            EmImageProcessing(
                id=None, image_recording_id=None, details="Motion correction"
            ),
            EmSoftware(
                id=None,
                category="OTHER",
                details=None,
                name="relion_run_motioncorr",
                version="1 Billion",
                image_processing_id="JOBREF: MotionCorr/job999/",
                fitting_id=None,
                imaging_id="JOBREF: Import/job001/",
            ),
        ]
        assert dobj == expected

    @staticmethod
    def test_get_onedep_deposition_objects_mocorr():
        job = new_job_of_type("relion.motioncorr.motioncor2")
        job.output_dir = "MotionCorr/job999/"
        job.joboptions["input_star_mics"].value = "Import/job001/movies.star"
        with patch("pipeliner.user_settings.get_motioncor2_executable") as mock_name:
            mock_name.return_value = "mocorr2"
            with patch.object(ExternalProgram, "get_version") as mock_vers:
                mock_vers.return_value = "2.0"
                dobj = job.prepare_deposition_data("ONEDEP")
        expected = [
            EmImageProcessing(
                id=None, image_recording_id=None, details="Motion correction"
            ),
            EmSoftware(
                id=None,
                category="OTHER",
                details=None,
                name="motioncor2",
                version="2.0",
                image_processing_id="JOBREF: MotionCorr/job999/",
                fitting_id=None,
                imaging_id="JOBREF: Import/job001/",
            ),
        ]
        assert dobj == expected


if __name__ == "__main__":
    unittest.main()
