#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile

from pipeliner.nodes import NODE_DENSITYMAP
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_display_object_generation_test,
)
from pipeliner.results_display_objects import (
    ResultsDisplayMapModel,
    ResultsDisplayMontage,
)


class ReboxRescaleJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_commands_no_rescale(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_reboxed.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i test.mrc --new_box 100"
                " --o ReboxRescale/job999/test_reboxed.mrc"
            ],
        )

    def test_get_commands_rebox_rescale(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_rescale_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_reboxed_rescaled.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i test.mrc --new_box 100 --rescale_angpix 1.68"
                " --force_header_angpix 1.68 --o ReboxRescale/job999/test_reboxed_"
                "rescaled.mrc"
            ],
        )

    def test_get_commands_rebox_rescale_multi(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_rescale_multi_job.star"
            ),
            input_nodes={
                "test.mrc": NODE_DENSITYMAP + ".mrc",
                "test2.mrc": NODE_DENSITYMAP + ".mrc",
            },
            output_nodes={
                "test_reboxed_rescaled.mrc": NODE_DENSITYMAP + ".mrc",
                "test2_reboxed_rescaled.mrc": NODE_DENSITYMAP + ".mrc",
            },
            expected_commands=[
                "relion_image_handler --i test.mrc --new_box 100 --rescale_angpix 1.68"
                " --force_header_angpix 1.68 --o ReboxRescale/job999/test_reboxed_"
                "rescaled.mrc",
                "relion_image_handler --i test2.mrc --new_box 100 --rescale_angpix 1.68"
                " --force_header_angpix 1.68 --o ReboxRescale/job999/test2_reboxed_"
                "rescaled.mrc",
            ],
        )

    def test_get_commands_rescale(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rescale_job.star"
            ),
            input_nodes={"emd_2660_small.map": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"emd_2660_small_rescaled.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i emd_2660_small.map:mrc --rescale_angpix 1.68 "
                "--force_header_angpix 1.68 "
                "--o ReboxRescale/job999/emd_2660_small_rescaled.mrc"
            ],
        )

    def test_get_commands_rescale_no_force(self):
        job_generate_commands_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rescale_noforce_job.star"
            ),
            input_nodes={"test.mrc": NODE_DENSITYMAP + ".mrc"},
            output_nodes={"test_rescaled.mrc": NODE_DENSITYMAP + ".mrc"},
            expected_commands=[
                "relion_image_handler --i test.mrc --rescale_angpix 1.68"
                " --o ReboxRescale/job999/test_rescaled.mrc"
            ],
        )

    def test_creating_results_display_rebox(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdos = [
            ResultsDisplayMontage(
                title="Map slices: test_reboxed.mrc reboxed to 100³ px",
                associated_data=["ReboxRescale/job999/test_reboxed.mrc"],
                img="ReboxRescale/job999/Thumbnails/slices_montage_000.png",
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "ReboxRescale/job999/test_reboxed.mrc: xy",
                    "ReboxRescale/job999/test_reboxed.mrc: xz",
                    "ReboxRescale/job999/test_reboxed.mrc: yz",
                ],
            ),
            ResultsDisplayMapModel(
                title="3D viewer: test_reboxed.mrc reboxed to 100³ px",
                associated_data=["ReboxRescale/job999/test_reboxed.mrc"],
                maps=["ReboxRescale/job999/test_reboxed.mrc"],
                maps_data="ReboxRescale/job999/test_reboxed.mrc",
            ),
        ]
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_job.star"
            ),
            expected_display_objects=expected_rdos,
            files_to_create={"test_reboxed.mrc": mrc_file},
        )

    def test_creating_results_display_rescale(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdos = [
            ResultsDisplayMontage(
                title="Map slices: emd_2660_small_rescaled.mrc rescaled to 1.68 Å/px",
                associated_data=["ReboxRescale/job999/emd_2660_small_rescaled.mrc"],
                img="ReboxRescale/job999/Thumbnails/slices_montage_000.png",
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "ReboxRescale/job999/emd_2660_small_rescaled.mrc: xy",
                    "ReboxRescale/job999/emd_2660_small_rescaled.mrc: xz",
                    "ReboxRescale/job999/emd_2660_small_rescaled.mrc: yz",
                ],
            ),
            ResultsDisplayMapModel(
                title="3D viewer: emd_2660_small_rescaled.mrc rescaled to 1.68 Å/px",
                associated_data=["ReboxRescale/job999/emd_2660_small_rescaled.mrc"],
                maps=["ReboxRescale/job999/emd_2660_small_rescaled.mrc"],
                maps_data="ReboxRescale/job999/emd_2660_small_rescaled.mrc",
            ),
        ]
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rescale_job.star"
            ),
            expected_display_objects=expected_rdos,
            files_to_create={"emd_2660_small_rescaled.mrc": mrc_file},
        )

    def test_creating_results_display_rebox_rescale(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")
        expected_rdos = [
            ResultsDisplayMontage(
                title=(
                    "Map slices: test_reboxed_rescaled.mrc reboxed to 100³ px "
                    "rescaled to 1.68 Å/px"
                ),
                associated_data=["ReboxRescale/job999/test_reboxed_rescaled.mrc"],
                img="ReboxRescale/job999/Thumbnails/slices_montage_000.png",
                xvalues=[0, 1, 2],
                yvalues=[0, 0, 0],
                labels=[
                    "ReboxRescale/job999/test_reboxed_rescaled.mrc: xy",
                    "ReboxRescale/job999/test_reboxed_rescaled.mrc: xz",
                    "ReboxRescale/job999/test_reboxed_rescaled.mrc: yz",
                ],
            ),
            ResultsDisplayMapModel(
                title=(
                    "3D viewer: test_reboxed_rescaled.mrc reboxed to 100³ px "
                    "rescaled to 1.68 Å/px"
                ),
                associated_data=["ReboxRescale/job999/test_reboxed_rescaled.mrc"],
                maps=["ReboxRescale/job999/test_reboxed_rescaled.mrc"],
                maps_data="ReboxRescale/job999/test_reboxed_rescaled.mrc",
            ),
        ]
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_rescale_job.star"
            ),
            expected_display_objects=expected_rdos,
            files_to_create={"test_reboxed_rescaled.mrc": mrc_file},
        )

    def test_creating_results_display_rebox_rescale_multi(self):
        mrc_file = os.path.join(self.test_data, "1ake_10A_molrep.mrc")

        expected_rdos = [
            ResultsDisplayMontage(
                title="Map slices",
                associated_data=[
                    "ReboxRescale/job999/test2_reboxed_rescaled.mrc",
                    "ReboxRescale/job999/test_reboxed_rescaled.mrc",
                ],
                img="ReboxRescale/job999/Thumbnails/slices_montage_000.png",
                xvalues=[0, 1, 2, 0, 1, 2],
                yvalues=[0, 0, 0, 1, 1, 1],
                labels=[
                    "test2_reboxed_rescaled.mrc reboxed to 100³ px rescaled to "
                    "1.68 Å/px: xy",
                    "test2_reboxed_rescaled.mrc reboxed to 100³ px rescaled to "
                    "1.68 Å/px: xz",
                    "test2_reboxed_rescaled.mrc reboxed to 100³ px rescaled to "
                    "1.68 Å/px: yz",
                    "test_reboxed_rescaled.mrc reboxed to 100³ px rescaled to "
                    "1.68 Å/px: xy",
                    "test_reboxed_rescaled.mrc reboxed to 100³ px rescaled to "
                    "1.68 Å/px: xz",
                    "test_reboxed_rescaled.mrc reboxed to 100³ px rescaled to "
                    "1.68 Å/px: yz",
                ],
            ),
        ]
        expected_rdos += [
            ResultsDisplayMapModel(
                title=(
                    f"3D viewer: test{x}_reboxed_rescaled.mrc reboxed to 100³ px "
                    "rescaled to 1.68 Å/px"
                ),
                associated_data=[f"ReboxRescale/job999/test{x}_reboxed_rescaled.mrc"],
                maps=[f"ReboxRescale/job999/test{x}_reboxed_rescaled.mrc"],
                maps_data=f"ReboxRescale/job999/test{x}_reboxed_rescaled.mrc",
            )
            for x in ["", "2"]
        ]
        job_display_object_generation_test(
            jobfile=os.path.join(
                self.test_data, "JobFiles/ReboxRescale/rebox_rescale_multi_job.star"
            ),
            expected_display_objects=expected_rdos,
            files_to_create={
                "test_reboxed_rescaled.mrc": mrc_file,
                "test2_reboxed_rescaled.mrc": mrc_file,
            },
        )


if __name__ == "__main__":
    unittest.main()
