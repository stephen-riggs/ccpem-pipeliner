#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner import job_factory
from pipeliner.pipeliner_job import ExternalProgram
from pipeliner.jobs.relion.relion_job import RelionJob, relion_program
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import job_generate_commands_test
from pipeliner.job_factory import new_job_of_type
from pipeliner.job_options import IntJobOption
from pipeliner.data_structure import NODE_MASK3D, NODE_DENSITYMAP, IMPORT_OTHER_NAME


class RelionJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Import"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_relion_job_cannot_be_instantiated(self):
        with self.assertRaises(NotImplementedError):
            RelionJob()

    def test_reading_import_mask_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        assert job.PROCESS_NAME == IMPORT_OTHER_NAME
        assert job.output_dir == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 8
        assert job.joboptions["fn_in_other"].label == "Input files:"
        assert job.joboptions["fn_in_other"].value == "emd_3488_mask.mrc"
        assert job.joboptions["is_relion"].label == "Are they RELION STAR files?"
        assert job.joboptions["is_relion"].value == "No"
        assert job.joboptions["node_type"].label == "RELION STAR file type:"
        assert (
            job.joboptions["node_type"].value == "RELION coordinates STAR file (.star)"
        )
        assert (
            job.joboptions["optics_group_particles"].label
            == "Rename optics group for particles:"
        )
        assert job.joboptions["optics_group_particles"].value == ""
        assert (
            job.joboptions["pipeliner_node_type"].label
            == "Node type for imported files:"
        )
        assert job.joboptions["pipeliner_node_type"].value == NODE_MASK3D
        assert job.joboptions["alt_nodetype"].label == "OR: Create a new node type:"
        assert job.joboptions["alt_nodetype"].value == ""
        assert job.joboptions["kwds"].label == "Keywords"
        assert job.joboptions["kwds"].value == ""
        assert (
            job.joboptions["is_synthetic"].label
            == "Does the node contain synthetic data?"
        )
        assert job.joboptions["is_synthetic"].value == "No"

    def test_reading_import_map_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        )
        assert job.PROCESS_NAME == IMPORT_OTHER_NAME
        assert job.output_dir == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 8
        assert job.joboptions["fn_in_other"].label == "Input files:"
        assert job.joboptions["fn_in_other"].value == "emd_3488.mrc"
        assert job.joboptions["is_relion"].label == "Are they RELION STAR files?"
        assert job.joboptions["is_relion"].value == "No"
        assert job.joboptions["node_type"].label == "RELION STAR file type:"
        assert (
            job.joboptions["node_type"].value == "RELION coordinates STAR file (.star)"
        )
        assert (
            job.joboptions["optics_group_particles"].label
            == "Rename optics group for particles:"
        )
        assert job.joboptions["optics_group_particles"].value == ""
        assert (
            job.joboptions["pipeliner_node_type"].label
            == "Node type for imported files:"
        )
        assert job.joboptions["pipeliner_node_type"].value == NODE_DENSITYMAP
        assert job.joboptions["alt_nodetype"].label == "OR: Create a new node type:"
        assert job.joboptions["alt_nodetype"].value == ""
        assert job.joboptions["kwds"].label == "Keywords"
        assert job.joboptions["kwds"].value == ""
        assert (
            job.joboptions["is_synthetic"].label
            == "Does the node contain synthetic data?"
        )
        assert job.joboptions["is_synthetic"].value == "No"

    def test_reading_import_halfmap_job(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_halfmaps.job")
        )
        assert job.PROCESS_NAME == IMPORT_OTHER_NAME
        assert job.output_dir == ""
        assert len(job.input_nodes) == 0
        assert len(job.output_nodes) == 0
        assert len(job.joboptions) == 8
        assert job.joboptions["fn_in_other"].label == "Input files:"
        assert job.joboptions["fn_in_other"].value == "3488_run_half1_class001.mrc"
        assert job.joboptions["is_relion"].label == "Are they RELION STAR files?"
        assert job.joboptions["is_relion"].value == "No"
        assert job.joboptions["node_type"].label == "RELION STAR file type:"
        assert (
            job.joboptions["node_type"].value == "RELION coordinates STAR file (.star)"
        )
        assert (
            job.joboptions["optics_group_particles"].label
            == "Rename optics group for particles:"
        )
        assert job.joboptions["optics_group_particles"].value == ""
        assert (
            job.joboptions["pipeliner_node_type"].label
            == "Node type for imported files:"
        )
        assert job.joboptions["pipeliner_node_type"].value == NODE_DENSITYMAP
        assert job.joboptions["alt_nodetype"].label == "OR: Create a new node type:"
        assert job.joboptions["alt_nodetype"].value == ""
        assert job.joboptions["kwds"].label == "Keywords"
        assert job.joboptions["kwds"].value == "halfmap"
        assert (
            job.joboptions["is_synthetic"].label
            == "Does the node contain synthetic data?"
        )
        assert job.joboptions["is_synthetic"].value == "No"

    def test_get_import_mask_commands(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "import_mask.job"),
            input_nodes={},
            output_nodes={"emd_3488_mask.mrc": f"{NODE_MASK3D}.mrc"},
            expected_commands=["cp emd_3488_mask.mrc Import/job999/emd_3488_mask.mrc"],
        )

    def test_get_import_halfmaps_commands(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "import_halfmaps.job"),
            input_nodes={},
            output_nodes={
                "3488_run_half1_class001.mrc": f"{NODE_DENSITYMAP}.mrc.halfmap"
            },
            expected_commands=[
                "cp 3488_run_half1_class001.mrc Import/job999/3488_run_"
                "half1_class001.mrc",
            ],
        )

    def test_get_import_halfmaps_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "import_halfmaps_job.star"),
            input_nodes={},
            output_nodes={
                "3488_run_half1_class001_unfil.mrc": f"{NODE_DENSITYMAP}.mrc"
            },
            expected_commands=[
                "cp 3488_run_half1_class001_unfil.mrc Import/job999/3488_run_"
                "half1_class001_unfil.mrc",
            ],
        )

    def test_get_import_map_commands(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "import_map_job.star"),
            input_nodes={},
            output_nodes={"emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc"},
            expected_commands=[
                "cp emd_3488.mrc Import/job999/emd_3488.mrc",
            ],
        )

    def test_import_job_directory_creation_with_default_jobdir(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        job_dir = os.path.join(self.test_dir, "Import/job023/")
        assert not os.path.isdir(job_dir)

        output_dir = "Import/job023/"
        job.output_dir = output_dir
        job.prepare_to_run()
        assert os.path.isdir(job_dir)

    def test_import_job_directory_creation_with_defined_jobdir(self):
        job = job_factory.read_job(
            os.path.join(self.test_data, "JobFiles/Import/import_mask.job")
        )
        job_dir = os.path.join(self.test_dir, "job_directory/")
        assert not os.path.isdir(job_dir)

        job.output_dir = job_dir
        job.prepare_to_run()
        assert os.path.isdir(job_dir)

    @staticmethod
    def test_import_job_movies_parameter_validation():
        job = new_job_of_type("relion.import.movies")
        job.joboptions["fn_in_raw"].value = "Movies/*.mrc"
        job.joboptions["optics_group_name"].value = "@pticGroups!"
        exp_err = [
            (
                "error",
                "Can only contain alphanumeric characters underscores and hyphens",
                "Optics group name:",
            ),
            ("warning", "No files found", "Raw input files:"),
        ]
        errors = job.validate_joboptions()
        assert len(errors) == 2
        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)
            assert error in exp_err
        for i in exp_err:
            assert i in formatted_errors

    @staticmethod
    def test_import_job_additional_parameter_validation():
        job = new_job_of_type("relion.import")
        job.joboptions["node_type"].value = "Particles STAR file (.star)"
        job.joboptions["fn_in_other"].value = "particles.star"
        job.joboptions["optics_group_particles"].value = "@pticGroups!"
        job.joboptions["is_relion"].value = "Yes"

        exp_errs = [
            (
                "error",
                "Can only contain alphanumeric characters",
                "Rename optics group for particles:",
            ),
            (
                "warning",
                "particles.star; file not found",
                "Input files:",
            ),
        ]
        errors = job.validate_joboptions()
        assert len(errors) == 2
        formatted_errors = []
        for i in errors:
            error = (i.type, i.message, i.raised_by[0].label)
            formatted_errors.append(error)
            assert error in exp_errs
        for i in exp_errs:
            assert i in formatted_errors

    @staticmethod
    def test_relion_program_returns_expected():
        relpro = relion_program("relion_refine")
        expected = ExternalProgram(
            command="relion_refine", vers_com=["relion", "--version"], vers_lines=[0]
        )
        assert relpro.__dict__ == expected.__dict__

    @staticmethod
    def test_assert_mpi_is_assigned_correctly():
        job = RelionJob(force=True)
        job.joboptions["nr_mpi"] = IntJobOption(label="Number of MPI:", default_value=1)
        job.joboptions["nr_mpi"].value = 4
        relpro = relion_program("relion_refine", auto_mpi=job)
        expected = ExternalProgram(
            command="relion_refine_mpi",
            vers_com=["relion", "--version"],
            vers_lines=[0],
        )
        assert relpro.__dict__ == expected.__dict__

    @staticmethod
    def test_assert_no_mpi_assigned_if_mpis_is_1():
        job = RelionJob(force=True)
        job.joboptions["nr_mpi"] = IntJobOption(label="Number of MPI:", default_value=1)
        relpro = relion_program("relion_refine", auto_mpi=job)
        expected = ExternalProgram(
            command="relion_refine",
            vers_com=["relion", "--version"],
            vers_lines=[0],
        )
        assert relpro.__dict__ == expected.__dict__

    @staticmethod
    def test_assert_no_mpi_assigned_if_no_mpi_jobop():
        job = RelionJob(force=True)
        relpro = relion_program("relion_refine", auto_mpi=job)
        expected = ExternalProgram(
            command="relion_refine",
            vers_com=["relion", "--version"],
            vers_lines=[0],
        )
        assert relpro.__dict__ == expected.__dict__


if __name__ == "__main__":
    unittest.main()
