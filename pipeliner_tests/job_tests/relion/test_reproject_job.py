#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    job_running_test,
)
from pipeliner.job_factory import new_job_of_type
from pipeliner.data_structure import (
    NODE_DENSITYMAP,
    NODE_EULERANGLES,
    NODE_IMAGE2DSTACK,
)


class ReprojectJobTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)

        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_project_plugin_from_file(self):
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Reproject/reproj_angles.star"),
            self.test_dir,
        )
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/Reproject/project_plugin_job.star"
            ),
            input_nodes={
                "emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
                "reproj_angles.star": f"{NODE_EULERANGLES}.star.relion",
            },
            output_nodes={
                "reproj.mrcs": f"{NODE_IMAGE2DSTACK}.mrc.relion.projections",
            },
            expected_commands=[
                "relion_project --i emd_3488.mrc --ang_simulate reproj_angles.star"
                " --angpix 1.07 --o Reproject/job999/reproj.mrcs"
            ],
        )

    def test_get_command_project_plugin_random(self):
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Reproject/reproj_angles.star"),
            self.test_dir,
        )
        job_generate_commands_test(
            jobfile=str(
                Path(self.test_data) / "JobFiles/Reproject/project_random_job.star"
            ),
            input_nodes={
                "emd_3488.mrc": f"{NODE_DENSITYMAP}.mrc",
            },
            output_nodes={
                "reproj.mrcs": f"{NODE_IMAGE2DSTACK}.mrc.relion.projections",
                "reproj.star": f"{NODE_EULERANGLES}.star.relion",
            },
            expected_commands=[
                "relion_project --i emd_3488.mrc --nr_uniform 10"
                " --angpix 1.07 --o Reproject/job999/reproj"
            ],
        )

    def test_running_projection_job_from_file(self):
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)
        shutil.copy(
            os.path.join(self.test_data, "JobFiles/Reproject/reproj_angles.star"),
            self.test_dir,
        )
        job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/Reproject/project_plugin_job.star"
            ),
            input_files=[],
            expected_outfiles=(
                "run.out",
                "run.err",
                "reproj.mrcs",
            ),
        )

    def test_running_projection_job_random(self):

        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)
        job_running_test(
            test_jobfile=str(
                Path(self.test_data) / "JobFiles/Reproject/project_random_job.star"
            ),
            input_files=[],
            expected_outfiles=(
                "run.out",
                "run.err",
                "reproj.mrcs",
                "reproj.star",
            ),
            show_contents=True,
        )

    def test_reproject_results_display_from_job_generated_file(self):
        stack = os.path.join(self.test_data, "image_stack.mrcs")
        star = os.path.join(self.test_data, "reproj.star")
        jobdir = os.path.join("Reproject/job007")
        os.makedirs(jobdir)
        shutil.copy(stack, os.path.join(jobdir, "reproj.mrcs"))
        shutil.copy(star, os.path.join(jobdir, "reproj.star"))
        rp_job = new_job_of_type("relion.reproject")
        rp_job.output_dir = jobdir + "/"
        results = rp_job.create_results_display()

        exp_file = os.path.join(self.test_data, "ResultsFiles/reproject.json")
        with open(exp_file, "r") as exp_f:
            exp = json.load(exp_f)
        assert results[0].__dict__ == exp

    def test_reproject_results_display_random_from_input_particles_file(self):
        stack = os.path.join(self.test_data, "image_stack.mrcs")
        star = os.path.join(self.test_data, "reproj.star")
        jobdir = os.path.join("Reproject/job007")
        os.makedirs(jobdir)
        shutil.copy(stack, os.path.join(jobdir, "reproj.mrcs"))
        shutil.copy(star, "input.star")
        rp_job = new_job_of_type("relion.reproject")
        rp_job.output_dir = jobdir + "/"
        rp_job.joboptions["angles_filename"].value = "input.star"
        results = rp_job.create_results_display()

        exp_file = os.path.join(self.test_data, "ResultsFiles/reproject.json")
        with open(exp_file, "r") as exp_f:
            exp = json.load(exp_f)
        for i in exp:
            assert exp[i] == results[0].__dict__[i]


if __name__ == "__main__":
    unittest.main()
