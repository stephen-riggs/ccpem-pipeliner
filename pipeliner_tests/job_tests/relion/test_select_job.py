#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import json
from pathlib import Path
from unittest.mock import patch

from pipeliner.api.manage_project import PipelinerProject
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    job_generate_commands_test,
    get_relion_tutorial_data,
)
from pipeliner.nodes import (
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_IMAGE2DGROUPMETADATA,
    NODE_OPTIMISERDATA,
    NODE_PARTICLEGROUPMETADATA,
)
from pipeliner.job_factory import new_job_of_type


class SelectTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.jobfiles = Path(self.test_data) / "JobFiles/Select"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_command_select_mics_on_val(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_mics_by_val.job"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job999/micrographs.star --select rlnCtfFigureOfMerit "
                "--minval 12345.0 --maxval 23456.0 --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_select_mics_on_val_relionstyle_name(self):
        """Test conversion of ambiguous relion style name"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_mics_by_val_relionstyle.job"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job999/micrographs.star --select rlnCtfFigureOfMerit "
                "--minval 12345.0 --maxval 23456.0 --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_select_jobstar(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_job.star"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job999/micrographs.star --select rlnCtfFigureOfMerit "
                "--minval 12345.0 --maxval 23456.0 --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_discard_mics(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_mics_discard.job"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job999/micrographs.star --discard_on_stats "
                "--discard_label rlnCtfFigureOfMerit --discard_sigma 4"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_discard_mics_relionstyle_jobname(self):
        """Check ambiguous jobname is converted"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_mics_discard_relionstyle.job"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job999/micrographs.star --discard_on_stats "
                "--discard_label rlnCtfFigureOfMerit --discard_sigma 4"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_mics_regroup_error(self):
        """Ignore illegal regrouping of selected mics"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_mics_regroup_error.job"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star "
                "--o Select/job999/micrographs.star --select rlnCtfFigureOfMerit "
                "--minval 12345.0 --maxval 23456.0 --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_split_mics_defined_number(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_mics_split_defined.job"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_split1.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job999/micrographs.star --split --size_split 100"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_split_mics_nr_groups(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_mics_split_nr.job"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_split1.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split2.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split3.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split4.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job999/micrographs.star --split --nr_split 4"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_split_mics_nr_groups_relionstyle_name(self):
        """Check interpreting ambiguous jobname"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_mics_split_relionstyle.job"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_split1.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split2.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split3.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
                "micrographs_split4.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion",
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job999/micrographs.star --split --nr_split 4"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_split_parts_nr_groups(self):
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_parts_split_nr.job"),
            input_nodes={
                "Extract/job300/particles."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "particles_split1.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "particles_split2.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "particles_split3.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "particles_split4.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            },
            expected_commands=[
                "relion_star_handler --i Extract/job300/particles.star"
                " --o Select/job999/particles.star --split --nr_split 4"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_split_parts_defined_number(self):
        """split particles into n parts per subset"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_mics_split_defined.job"),
            input_nodes={
                "CtfFind/job300/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs_split1.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i CtfFind/job300/micrographs_ctf.star"
                " --o Select/job999/micrographs.star --split --size_split 100"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_remove_duplicates(self):
        """remove duplicate for particles"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_parts_remove_dups.job"),
            input_nodes={
                "Extract/job300/particles."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i Extract/job300/particles.star"
                " --o Select/job999/particles.star --remove_duplicates 30"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_interactive_from_model(self):
        """get particles from model file with regrouping and recentering"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_interactive_averages_job.star"),
            input_nodes={
                "Class2D/job006/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star"
                ".relion"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
            },
            expected_commands=[
                "relion_display --gui --i Class2D/job006/run_it025_optimiser.s"
                "tar --allow_save --fn_parts Select/job999/particles.star --fn_imgs "
                "Select/job999/class_averages.star --recenter --regroup 60 "
                "--pipeline_control Select/job999/"
            ],
        )

    def test_get_command_select_parts_by_class(self):
        """get particles from data file by class"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_parts_by_class.job"),
            input_nodes={
                "Class3D/job002/run_it025_data."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star."
                "relion"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_star_handler --i Class3D/job002/run_it025_data.star"
                " --o Select/job999/particles.star --select rlnCtfFigureOfMerit "
                "--minval 3.0 --maxval 3.0 --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_interactive_from_model_norecenter(self):
        """get particles from model file with regrouping and no recentering"""
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles / "select_interactive_averages_norecenter_job.star"
            ),
            input_nodes={
                "Class2D/job006/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star."
                "relion"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
            },
            expected_commands=[
                "relion_display --gui --i Class2D/job006/run_it025_optimiser.s"
                "tar --allow_save --fn_parts Select/job999/particles.star --fn_imgs "
                "Select/job999/class_averages.star --regroup 60 "
                "--pipeline_control Select/job999/"
            ],
        )

    def test_get_command_interactive_from_model_norecenter_noregroup(self):
        """get particles from model file with
        regrouping and no recentering or regrouping"""
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles
                / ("select_interactive_averages_norecenter_noregroup_job.star")
            ),
            input_nodes={
                "Class2D/job006/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star."
                "relion"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
            },
            expected_commands=[
                "relion_display --gui --i Class2D/job006/run_it025_optimiser.s"
                "tar --allow_save --fn_parts Select/job999/particles.star --fn_imgs "
                "Select/job999/class_averages.star "
                "--pipeline_control Select/job999/"
            ],
        )

    def test_get_command_interactive_from_mics(self):
        """get micrographs from mics file"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_interactive_micrographs_job.star"),
            input_nodes={
                "CtfFind/job001/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_display --gui --i CtfFind/job001/micrographs_ctf.star"
                " --allow_save --fn_imgs Select/job999/micrographs.star"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_interactive_from_mics_regroup(self):
        """get micrographs from mics file ignoring illegal regrouping"""
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles / "select_interactive_micrographs_regroup_job.star"
            ),
            input_nodes={
                "CtfFind/job001/micrographs_ctf.star": f"{NODE_MICROGRAPHGROUPMETADATA}"
                ".star.relion"
            },
            output_nodes={
                "micrographs.star": f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_display --gui --i CtfFind/job001/micrographs_ctf.star"
                " --allow_save --fn_imgs Select/job999/micrographs.star"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_interactive_from_parts(self):
        """get particles from data file"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_interactive_parts_job.star"),
            input_nodes={
                "Refine3D/job010/run_data."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_display --gui --i Refine3D/job010/run_data.star"
                " --allow_save --fn_imgs Select/job999/particles.star"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_interactive_from_parts_relionstyle_jobname(self):
        """get particles from data file, job has ambiguous relion style name"""
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles / "select_interactive_parts_relionstyle_job.star"
            ),
            input_nodes={
                "Refine3D/job010/run_data."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_display --gui --i Refine3D/job010/run_data.star"
                " --allow_save --fn_imgs Select/job999/particles.star"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_interactive_from_parts_regroup(self):
        """get particles from data file ignoring illegal regrouping"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_interactive_parts_regroup_job.star"),
            input_nodes={
                "Refine3D/job010/run_data."
                "star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_display --gui --i Refine3D/job010/run_data.star"
                " --allow_save --fn_imgs Select/job999/particles.star"
                " --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_interactive_from_model_class3d_norecenter(self):
        """get particles from model file with regrouping and no recentering"""
        job_generate_commands_test(
            jobfile=str(
                self.jobfiles
                / ("select_interactive_averages_class3d_norecenter_job.star")
            ),
            input_nodes={
                "Class3D/job006/run_it025_optimiser.star": f"{NODE_OPTIMISERDATA}.star."
                "relion"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            },
            expected_commands=[
                "relion_display --gui --i "
                "Class3D/job006/run_it025_optimiser.star"
                " --allow_save --fn_parts Select/job999/particles.star"
                " --regroup 60 --pipeline_control Select/job999/"
            ],
        )

    def test_get_command_autorank_relionstyle_jobname(self):
        """get particles from auto rank,
        make sure ambiguous relion style job name is read correctly"""
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_autorank_relionstyle_job.star"),
            input_nodes={
                "test_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class2d"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
                "rank_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "autoselect",
            },
            expected_commands=[
                "relion_class_ranker --opt test_optimiser.star --o Select/job999/ "
                "--fn_sel_parts particles.star --fn_sel_classavgs class_averages.star "
                "--python python --fn_root rank --do_granularity_features "
                "--auto_select --min_score 0.5 --pipeline_control Select/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.select_job.RelionSelectClass2DAuto.relion5_plus")
    def test_get_command_autorank(self, mock_r5):
        """get particles from auto rank default jobstar file"""
        mock_r5.return_value = False
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_class2dauto_job.star"),
            input_nodes={
                "test_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class2d"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
                "rank_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "autoselect",
            },
            expected_commands=[
                "relion_class_ranker --opt test_optimiser.star --o Select/job999/ "
                "--fn_sel_parts particles.star --fn_sel_classavgs class_averages.star "
                "--python python --fn_root rank --do_granularity_features "
                "--auto_select --min_score 0.5 --pipeline_control Select/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.select_job.RelionSelectClass2DAuto.relion5_plus")
    def test_get_command_autorank_with_relion5(self, mock_r5):
        """get particles from auto rank default jobstar file"""
        mock_r5.return_value = True
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_class2dauto_job.star"),
            input_nodes={
                "test_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class2d"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
                "rank_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "autoselect",
            },
            expected_commands=[
                "relion_class_ranker --opt test_optimiser.star --o Select/job999/ "
                "--fn_sel_parts particles.star --fn_sel_classavgs class_averages.star "
                "--fn_root rank --do_granularity_features "
                "--auto_select --min_score 0.5 --pipeline_control Select/job999/"
            ],
        )

    @patch("pipeliner.jobs.relion.select_job.RelionSelectClass2DAuto.relion5_plus")
    def test_get_command_autorank_with_additional_arg(self, mock_r5):
        """get particles from auto rank default jobstar file"""
        mock_r5.return_value = False
        job_generate_commands_test(
            jobfile=str(self.jobfiles / "select_class2dauto_addarg_job.star"),
            input_nodes={
                "test_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion.class2d"
            },
            output_nodes={
                "particles.star": f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
                "class_averages.star": f"{NODE_IMAGE2DGROUPMETADATA}.star.relion."
                "classaverages",
                "rank_optimiser.star": f"{NODE_OPTIMISERDATA}.star.relion."
                "autoselect",
            },
            expected_commands=[
                "relion_class_ranker --opt test_optimiser.star --o Select/job999/ "
                "--fn_sel_parts particles.star --fn_sel_classavgs class_averages.star "
                "--python python --fn_root rank --do_granularity_features "
                "--auto_select --min_score 0.5 --extra arg --pipeline_control "
                "Select/job999/"
            ],
        )

    def test_get_command_autorank_ambiguous_relionstyle_jobname(self):
        """get particles from auto rank with recentering and regrouping,
        raises error because job name cannot be interpreted"""
        with self.assertRaises(RuntimeError):
            job_generate_commands_test(
                jobfile=str(self.jobfiles / "select_relionstyle_none_job.star"),
                input_nodes={},
                output_nodes={},
                expected_commands=[],
            )

    def test_create_display_split_mics(self):
        get_relion_tutorial_data(relion_version=4, dirs=["Select", "MotionCorr"])

        project = PipelinerProject()
        job = project.get_job("Select/job005/")
        dispobjs = job.create_results_display()

        expected = [
            {
                "title": "Select/job005/micrographs_split1.star; 4/10 images",
                "dobj_type": "montage",
                "xvalues": [0, 1, 2, 3],
                "yvalues": [0, 0, 0, 0],
                "labels": [
                    "MotionCorr/job002/Movies/20170629_00021_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00022_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00023_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00024_frameImage.mrc",
                ],
                "associated_data": ["Select/job005/micrographs_split1.star"],
                "img": "Select/job005/Thumbnails/montage_f000.png",
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Select/job005/micrographs_split2.star; 4/10 images",
                "dobj_type": "montage",
                "xvalues": [0, 1, 2, 3],
                "yvalues": [0, 0, 0, 0],
                "labels": [
                    "MotionCorr/job002/Movies/20170629_00031_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00035_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00036_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00037_frameImage.mrc",
                ],
                "associated_data": ["Select/job005/micrographs_split2.star"],
                "img": "Select/job005/Thumbnails/montage_f001.png",
                "start_collapsed": False,
                "flag": "",
            },
            {
                "title": "Select/job005/micrographs_split3.star; 4/4 images",
                "dobj_type": "montage",
                "xvalues": [0, 1, 2, 3],
                "yvalues": [0, 0, 0, 0],
                "labels": [
                    "MotionCorr/job002/Movies/20170629_00046_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00047_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00048_frameImage.mrc",
                    "MotionCorr/job002/Movies/20170629_00049_frameImage.mrc",
                ],
                "associated_data": ["Select/job005/micrographs_split3.star"],
                "img": "Select/job005/Thumbnails/montage_f002.png",
                "start_collapsed": False,
                "flag": "",
            },
        ]

        for n, do in enumerate(dispobjs):
            assert do.__dict__ == expected[n]

    def test_create_display_select3dclasses(self):
        get_relion_tutorial_data(
            relion_version=4, dirs=["Select", "MotionCorr", "Class3D", "Extract"]
        )

        project = PipelinerProject()
        job = project.get_job("Select/job017/")
        dispobjs = job.create_results_display()

        resultfile = os.path.join(self.test_data, "ResultsFiles/select_class3d.json")
        with open(resultfile, "r") as res:
            expected = json.load(res)
        assert dispobjs[0].__dict__ == expected
        assert dispobjs[1].__dict__ == {
            "title": "3D viewer: Selected class: run_it025_class004.mrc",
            "start_collapsed": True,
            "dobj_type": "mapmodel",
            "flag": "",
            "maps": ["Class3D/job016/run_it025_class004.mrc"],
            "maps_opacity": [1.0],
            "maps_colours": [],
            "models_colours": [],
            "models": [],
            "maps_data": "",
            "models_data": "",
            "associated_data": ["Class3D/job016/run_it025_class004.mrc"],
        }

    def test_create_display_select_2dauto(self):
        get_relion_tutorial_data(
            relion_version=4, dirs=["Select", "Extract", "Class2D"]
        )

        project = PipelinerProject()
        job = project.get_job("Select/job009/")
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "Selected class averages: score > 0.5",
            "dobj_type": "montage",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "Class 23: 0.680323",
                "Class 39: 0.644231",
                "Class 40: 0.735355",
                "Class 43: 0.748991",
                "Class 46: 0.58601",
            ],
            "associated_data": ["Select/job009/class_averages.star"],
            "img": "Select/job009/Thumbnails/montage_s000.png",
            "start_collapsed": False,
            "flag": "",
        }

        rdo_files = {
            1: "ResultsFiles/select_2dauto_parts.json",
            2: "ResultsFiles/select_2dauto_histo.json",
            3: "ResultsFiles/select_2dauto_rejected_classes.json",
        }
        for pair in rdo_files.items():
            do_file = os.path.join(self.test_data, pair[1])
            with open(do_file, "r") as doe:
                expected = json.load(doe)
            assert dispobjs[pair[0]].__dict__ == expected

    def test_create_display_select_2dauto_no_selected_classes(self):
        get_relion_tutorial_data(
            relion_version=4, dirs=["Select", "Extract", "Class2D"]
        )
        # the class averages file is empty because all classes were rejected
        clavfile = Path("Select/job009/class_averages.star")
        clavfile.unlink()
        clavfile.touch()
        project = PipelinerProject()
        job = project.get_job("Select/job009/")
        dispobjs = job.create_results_display()

        assert dispobjs[0].__dict__ == {
            "title": "No classes selected",
            "start_collapsed": False,
            "dobj_type": "text",
            "flag": "",
            "display_data": "No classes scored above the 0.5 threshold",
            "associated_data": ["Select/job009/class_averages.star"],
        }

        rdo_files = {
            1: "ResultsFiles/select_2dauto_histo.json",
            2: "ResultsFiles/select_2dauto_all_rejected_classes.json",
        }
        for pair in rdo_files.items():
            do_file = os.path.join(self.test_data, pair[1])
            with open(do_file, "r") as doe:
                expected = json.load(doe)
            assert dispobjs[pair[0]].__dict__ == expected

    def test_create_display_select_2dauto_uneven(self):
        get_relion_tutorial_data(
            relion_version=4, dirs=["Select", "Extract", "Class2D"]
        )

        project = PipelinerProject()
        job = project.get_job("Select/job014/")
        dispobjs = job.create_results_display()

        cf = "ResultsFiles/select_2dauto_uneven_classes.json"
        classes_file = os.path.join(self.test_data, cf)
        with open(classes_file, "r") as doe:
            expected = json.load(doe)
        assert dispobjs[0].__dict__ == expected

        pf = "ResultsFiles/select_2dauto_uneven_parts.json"
        parts_file = os.path.join(self.test_data, pf)
        with open(parts_file, "r") as doe:
            expected = json.load(doe)
        assert dispobjs[1].__dict__ == expected

    def test_results_display_interactive(self):
        os.makedirs("Select/job001")
        os.makedirs("Class2D/job008")
        shutil.copy(Path(self.test_data) / "class2d_model.star", "model.star")
        shutil.copy(
            Path(self.test_data) / "class_averages.star",
            "Select/job001/class_averages.star",
        )
        shutil.copy(
            Path(self.test_data) / "particles.star", "Select/job001/particles.star"
        )
        shutil.copy(
            Path(self.test_data) / "image_stack.mrcs",
            "Class2D/job008/run_it025_classes.mrcs",
        )
        job = new_job_of_type("relion.select.interactive")
        job.joboptions["fn_model"].value = "model.star"
        job.output_dir = "Select/job001"
        res = job.create_results_display()
        assert res[0].__dict__ == {
            "title": "Selected 2D classes",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "000023@Class2D/job008/run_it025_classes.mrcs",
                "000039@Class2D/job008/run_it025_classes.mrcs",
                "000040@Class2D/job008/run_it025_classes.mrcs",
                "000043@Class2D/job008/run_it025_classes.mrcs",
                "000046@Class2D/job008/run_it025_classes.mrcs",
            ],
            "associated_data": ["Select/job001/class_averages.star"],
            "img": "Select/job001/Thumbnails/montage_s000.png",
        }

    # TODO: Need tests for
    #  - split particles


if __name__ == "__main__":
    unittest.main()
