#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import os
import unittest
import tempfile
import shutil
from pathlib import Path

from pipeliner.utils import touch
from pipeliner_tests import test_data
from pipeliner.scripts import cleanup_summary_reports
from pipeliner.starfile_handler import DataStarFile


class SummaryCleanUpTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

        test_file = Path(self.test_data) / "Metadata/summary_file_4.star"
        shutil.copy(test_file, "pipeliner_summary_data.star")
        df = DataStarFile("pipeliner_summary_data.star")
        self.archive = df.column_as_list("project_archives", "_pipelinerArchiveName")
        self.metadata = df.column_as_list(
            "metadata_reports", "_pipelinerMetadataReportName"
        )
        self.ref = df.column_as_list(
            "reference_reports", "_pipelinerReferenceReportName"
        )
        for i in (self.archive, self.metadata, self.ref):
            for f in i:
                dir = os.path.dirname(f)
                os.makedirs(dir, exist_ok=True)
                touch(f)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_cleanup_all_no_args(self):
        rm = [
            "ReferenceReports/20230322122525_job002.json",
            "MetadataReports/20230322122525_job002.json",
            "ProjectArchives/20230322122525_job002.tar.gz",
        ]
        for f in rm:
            os.remove(f)
        cleanup_summary_reports.main([])

        result = DataStarFile("pipeliner_summary_data.star")
        assert (
            result.column_as_list("project_archives", "_pipelinerArchiveName")
            == self.archive[1:]
        )
        assert (
            result.column_as_list("reference_reports", "_pipelinerReferenceReportName")
            == self.ref[1:]
        )
        assert (
            result.column_as_list("metadata_reports", "_pipelinerMetadataReportName")
            == self.metadata[1:]
        )

    def test_cleanup_all_args(self):
        rm = [
            "ReferenceReports/20230322122525_job002.json",
            "MetadataReports/20230322122525_job002.json",
            "ProjectArchives/20230322122525_job002.tar.gz",
        ]
        for f in rm:
            os.remove(f)
        cleanup_summary_reports.main(["-a", "-m", "-r"])

        result = DataStarFile("pipeliner_summary_data.star")
        assert (
            result.column_as_list("project_archives", "_pipelinerArchiveName")
            == self.archive[1:]
        )
        assert (
            result.column_as_list("reference_reports", "_pipelinerReferenceReportName")
            == self.ref[1:]
        )
        assert (
            result.column_as_list("metadata_reports", "_pipelinerMetadataReportName")
            == self.metadata[1:]
        )

    def test_cleanup_just_md(self):
        rm = [
            "ReferenceReports/20230322122525_job002.json",
            "MetadataReports/20230322122525_job002.json",
            "ProjectArchives/20230322122525_job002.tar.gz",
        ]
        for f in rm:
            os.remove(f)
        cleanup_summary_reports.main(["-m"])

        result = DataStarFile("pipeliner_summary_data.star")
        assert (
            result.column_as_list("project_archives", "_pipelinerArchiveName")
            == self.archive
        )
        assert (
            result.column_as_list("reference_reports", "_pipelinerReferenceReportName")
            == self.ref
        )
        assert (
            result.column_as_list("metadata_reports", "_pipelinerMetadataReportName")
            == self.metadata[1:]
        )

    def test_no_error_on_missing_file(self):
        os.remove("pipeliner_summary_data.star")
        cleanup_summary_reports.main([])


if __name__ == "__main__":
    unittest.main()
