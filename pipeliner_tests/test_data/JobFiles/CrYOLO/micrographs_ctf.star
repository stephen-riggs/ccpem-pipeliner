
# version 30001

data_optics

loop_ 
_rlnOpticsGroupName #1 
_rlnOpticsGroup #2 
_rlnMicrographOriginalPixelSize #3 
_rlnVoltage #4 
_rlnSphericalAberration #5 
_rlnAmplitudeContrast #6 
_rlnMicrographPixelSize #7 
opticsGroup1            1     1.000000   300.000000     2.700000     0.100000     1.000000 
 

# version 30001

data_micrographs

loop_ 
_rlnMicrographName #1 
_rlnOpticsGroup #2 
_rlnCtfImage #3 
_rlnDefocusU #4 
_rlnDefocusV #5 
_rlnCtfAstigmatism #6 
_rlnDefocusAngle #7 
_rlnCtfFigureOfMerit #8 
_rlnCtfMaxResolution #9 
MotionCorr/job002/Movies/20170629_00021_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00021_frameImage_PS.ctf:mrc 17569.884766 17078.089844   491.794922    80.484955     0.096598     5.471756 
MotionCorr/job002/Movies/20170629_00022_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00022_frameImage_PS.ctf:mrc 15916.856445 15427.661133   489.195312    78.018829     0.164570     4.119540 
MotionCorr/job002/Movies/20170629_00023_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00023_frameImage_PS.ctf:mrc 17257.617188 16754.341797   503.275391    76.363495     0.163288     3.916940 
MotionCorr/job002/Movies/20170629_00024_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00024_frameImage_PS.ctf:mrc 18911.693359 18378.443359   533.250000    76.143097     0.161564     3.548515 
MotionCorr/job002/Movies/20170629_00025_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00025_frameImage_PS.ctf:mrc 17221.136719 16717.988281   503.148438    73.384163     0.148018     4.004469 
MotionCorr/job002/Movies/20170629_00026_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00026_frameImage_PS.ctf:mrc 13473.061523 13023.612305   449.449219    76.175987     0.194961     3.675897 
MotionCorr/job002/Movies/20170629_00027_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00027_frameImage_PS.ctf:mrc 14766.458984 14245.321289   521.137695    77.565567     0.187670     3.620202 
MotionCorr/job002/Movies/20170629_00028_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00028_frameImage_PS.ctf:mrc 16350.236328 15868.659180   481.577148    76.382454     0.186221     3.833155 
MotionCorr/job002/Movies/20170629_00029_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00029_frameImage_PS.ctf:mrc 13352.339844 12879.421875   472.917969    79.150513     0.200407     3.833155 
MotionCorr/job002/Movies/20170629_00030_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00030_frameImage_PS.ctf:mrc 14720.168945 14266.064453   454.104492    77.028931     0.201356     3.874595 
MotionCorr/job002/Movies/20170629_00031_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00031_frameImage_PS.ctf:mrc 16326.540039 15851.256836   475.283203    78.457283     0.216873     3.812766 
MotionCorr/job002/Movies/20170629_00032_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00032_frameImage_PS.ctf:mrc 14981.678711 14384.169922   597.508789    77.082649     0.162459     3.657143 
MotionCorr/job002/Movies/20170629_00033_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00033_frameImage_PS.ctf:mrc 16224.375977 15694.442383   529.933594    76.905403     0.184207     3.462802 
MotionCorr/job002/Movies/20170629_00034_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00034_frameImage_PS.ctf:mrc 17803.056641 17291.742188   511.314453    75.820541     0.155198     3.548515 
MotionCorr/job002/Movies/20170629_00035_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00035_frameImage_PS.ctf:mrc 16361.500000 15868.924805   492.575195    77.549995     0.177001     3.657143 
MotionCorr/job002/Movies/20170629_00036_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00036_frameImage_PS.ctf:mrc 17955.240234 17479.310547   475.929688    73.793678     0.182291     3.853763 
MotionCorr/job002/Movies/20170629_00037_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00037_frameImage_PS.ctf:mrc 16318.333984 15831.125000   487.208984    78.604012     0.209221     3.752880 
MotionCorr/job002/Movies/20170629_00038_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00038_frameImage_PS.ctf:mrc 17944.994141 17462.281250   482.712891    73.332306     0.225704     3.733333 
MotionCorr/job002/Movies/20170629_00039_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00039_frameImage_PS.ctf:mrc 18497.267578 18021.291016   475.976562    72.269348     0.170039     3.602010 
MotionCorr/job002/Movies/20170629_00040_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00040_frameImage_PS.ctf:mrc 19578.523438 19120.765625   457.757812    72.919708     0.168270     3.694845 
MotionCorr/job002/Movies/20170629_00041_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00041_frameImage_PS.ctf:mrc 21120.154297 20682.056641   438.097656    76.723160     0.172864     3.620202 
MotionCorr/job002/Movies/20170629_00042_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00042_frameImage_PS.ctf:mrc 18555.056641 18127.080078   427.976562    70.904556     0.155902     3.713990 
MotionCorr/job002/Movies/20170629_00043_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00043_frameImage_PS.ctf:mrc 19732.906250 19194.330078   538.576172    73.579407     0.170409     3.792593 
MotionCorr/job002/Movies/20170629_00044_frameImage.mrc            1 CtfFind/job003/Movies/20170629_00044_frameImage_PS.ctf:mrc 21244.880859 20689.691406   555.189453    74.440849     0.173054     3.429665 
 
