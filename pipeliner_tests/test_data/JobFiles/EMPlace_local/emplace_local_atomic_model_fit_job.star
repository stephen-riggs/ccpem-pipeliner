
# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_job

_rlnJobTypeLabel    emplace_local.atomic_model_fit
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'centre_x'           35.0
  'centre_y'           53.0
  'centre_z'           38.0
  'do_queue'           No 
'input_map1'           'Import/job001/3488_run_half1_class001_unfil.mrc'
'input_map2'           'Import/job002/3488_run_half2_class001_unfil.mrc'
'min_dedicated'            1 
'starting_model_vrms'          1.2 
'use_halfmaps'          Yes
       model           'Import/job003/5me2_a_to_move.pdb' 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
  resolution           3.2 
 
