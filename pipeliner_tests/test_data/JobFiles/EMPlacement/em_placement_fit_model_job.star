
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    em_placement.atomic_model_fit
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'best_resolution'           3.2 
  'do_queue'           No 
  'full_map'           'Import/job001/emd_3488.mrc' 
 'input_map1'          'Import/job001/3488_run_half1_class001_unfil.mrc' 
 'input_map2'     'Import/job001/3488_run_half2_class001_unfil.mrc'
'min_dedicated'            1 
'estimated_rmsd'         1.5 
       model           'Import/job002/5me2_a_to_move.pdb' 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
    sequence           'Import/job003/3488_5me2.fasta' 
    symmetry           'None' 
 
