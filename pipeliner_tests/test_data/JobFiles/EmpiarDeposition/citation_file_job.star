
# RELION version 4.0 / CCP-EM Pipeliner version 0.1.0

data_job

_rlnJobTypeLabel    pipeliner.deposition.empiar
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'book_chapter_title'           ''
'ca_address'           'my house'
   'ca_city'           'Portland'
'ca_country'           'US'
  'ca_email'           'pi@pi.com'
'ca_first_name'           'Some'
'ca_middle_name'           'Crazy'
  'ca_orcid'           '1234-5678-91011'
    'ca_org'           'An important one'
'ca_postcode'           '97223'
  'ca_state'           'Oregon'
'ca_surname'           'Guy'
    'ca_tel'           '+1 (503) 639-5777'
'citation_authors'           ''
'citation_file'           'citation.json'
'citation_title'           ''
'cite_country'           ''
'corresponding_is_pi'           Yes
'dep_corr_mics'          Yes 
'dep_corr_parts'          Yes 
 'dep_parts'          Yes 
'dep_raw_mics'          Yes 
  'emdb_ref'           '0010'
'empiar_ref'           '123'
'empiar_token'          '123abc'
'empiar_transfer_password'    'EXpassword'
'entry_authors'           'Lebowski, J, 1234-5678-91011:::Sobchak, W'
'entry_authors_as_citation'           No
'entry_title'           'My first deposition'
'first_page'           ''
 'input_job' 'PostProcess/job030/'
'j_or_nj_citation'           No 
'journal_abbrev'           ''
 'last_page'           ''
'pi_address'           '' 
   'pi_city'           '' 
'pi_country' 'Select a country' 
  'pi_email'           '' 
'pi_first_name'           '' 
'pi_middle_name'           '' 
  'pi_orcid'           '' 
    'pi_org'           '' 
'pi_postcode'           '' 
  'pi_state'           '' 
'pi_surname'           '' 
    'pi_tel'           '' 
'pub_location'           ''
'release_status' 'Directly after the submission has been processed' 
'thumbnail_file'           'mythumb.png'
'upload_entry_author_file'           '' 
     details           ''
         doi           '' 
     editors           ''
       issue           ''
     journal           ''
    language           ''
   published           Yes
   publisher           ''
    pubmedid           ''
      volume           ''
        year           ''
 
