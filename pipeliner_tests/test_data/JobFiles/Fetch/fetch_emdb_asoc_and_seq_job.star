
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    pipeliner.fetch.emdb
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
     'db_id'           '0002'
     'get_associated_models'  yes
     'asoc_format'      'mmCIF'
     'get_fasta'        yes
