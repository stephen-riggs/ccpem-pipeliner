
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    locscale.postprocess.local_sharpening
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'input_halfmap1'           'Import/job001/3488_run_half1_class001_unfil.mrc' 
'input_halfmap2'           'Import/job002/3488_run_half2_class001_unfil.mrc' 
'input_mask'           '' 
'input_model'           'Import/job003/5ni1_updated.cif' 
  resolution       3.2
  n_nodes           1
  method        Model-based
  use_advanced     Yes
  skip_brefine    Yes
 