
# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_job

_rlnJobTypeLabel    moorhen.atomic_model_refine
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# RELION version 4.0 / CCP-EM Pipeliner version 0.4.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'input_maps'           'Import/job001/map.mrc'
'input_models'           'Fetch/job002/model.pdb:::Fetch/job003/model2.pdb'
'moorhen_tmp_dir'           'tmp_123456'
'saved_models_basenames'           'model2_moorhen.pdb'
 
