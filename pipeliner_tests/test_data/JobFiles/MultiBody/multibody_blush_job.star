# version 30001

data_job

_rlnJobTypeLabel                       relion.multibody.refine
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
do_analyse        Yes
do_blush            Yes
do_combine_thru_disc         No 
   do_pad1         No 
do_parallel_discio        Yes 
do_preread_images         No 
  do_queue         No 
 do_select         No 
do_subtracted_bodies        No 
eigenval_max        999 
eigenval_min       -999 
 fn_bodies  bodyfile.star 
   fn_cont         "" 
     fn_in  Refine3D/job400/run_it025_optimiser.star 
   gpu_ids         "1:2" 
min_dedicated          1 
 nr_movies          3 
    nr_mpi          16 
   nr_pool          3 
nr_threads          8 
offset_range          3 
offset_step       0.75 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
  sampling "1.8 degrees" 
scratch_dir         "" 
select_eigenval          1 
skip_gridding        Yes 
   use_gpu         Yes 