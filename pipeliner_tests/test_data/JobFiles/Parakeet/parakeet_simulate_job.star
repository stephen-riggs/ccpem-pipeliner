
# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_job

_rlnJobTypeLabel    parakeet.simulate
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
'min_dedicated'            1 
'nr_threads'            1 
'other_args'           '' 
        conf  parakeet_config.yaml 
         mtf 'synthetic_mtf_300kV.star' 
 opticsGroup            1 
opticsGroupName opticsGroup1 
   particles            1 
        qsub         qsub 
  qsubscript           '' 
   queuename      openmpi 
 
