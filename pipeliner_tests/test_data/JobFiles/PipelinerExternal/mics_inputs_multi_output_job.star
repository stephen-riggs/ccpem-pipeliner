
# version 50001
# CCP-EM Pipeliner version 1.0.1

data_job

_rlnJobTypeLabel    pipeliner.run_external_program
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# version 50001
# CCP-EM Pipeliner version 1.0.1

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
'alt_nodetype'           '' 
'do_outputs'           Yes
 'fn_output'           'micrographs.star:::micrographs2.star'
    'in_map'           '' 
   'in_mask'           '' 
'in_micrographs'       'CtfFind/job003/micrographs_ctf.star:::MotionCorr/job002/corrected_micrographs.star'
 'in_movies'           '' 
  'in_other'           '' 
'in_particles'         ''
'output_kwds'          'kwd1:::kwd2'
'output_nodetype'   MicrographGroupMetadata
    commands           'run command1 --arg XXXmicrographs_1XXX --arg2:::run command2 --arg XXXmicrographs_2XXX --arg2'
 
