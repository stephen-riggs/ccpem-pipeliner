#$ -cwd -V
#$ -l h_rt=96:00:00     # specifiy max run time here
#$ -m be
#$ -e MultiBody/job001/run.err
#$ -o MultiBody/job001/run.out
#$ -P openmpi
#$  -M Here is number 1    # put your email address here 
#$  -l coproc_v100=4   # GPUS in relion should be left blank
 
## load modules 
module load intel/19.0.4 cuda/10.1.168

## Here are some more extra variables
Here is number 2
Here is number 3

## set library paths
export PATH=$CUDA_HOME/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/ufaserv1_k/fbscem/relion_arc/arc4/relion/build/lib/:

## print some diagnostic info 
module list
nvidia-smi -L
which relion_refine_mpi

## run relion

let NSLOTS=NSLOTS/2
mpirun -n 16 relion_refine_mpi --continue Refine3D/job400/run_it025_optimiser.star --o MultiBody/job001/run --solvent_correct_fsc --multibody_masks bodyfile.star --oversampling 1 --healpix_order 4 --auto_local_healpix_order 4 --offset_range 3 --offset_step 1.5 --j 9 --gpu 1:2 --dont_combine_weights_via_disc --pool 3 --pad 2 --pipeline_control MultiBody/job001/

#One more extra command for good measure 
Here is number 4
dedicated nodes: 24
output name: MultiBody/job001/
