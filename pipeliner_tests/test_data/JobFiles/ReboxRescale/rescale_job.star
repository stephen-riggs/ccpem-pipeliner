
# Relion version 4.0 / CCP-EM_pipeliner vers 0.1.0

data_job

_rlnJobTypeLabel    relion.map_utilities.rebox_rescale_map
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo    0
 

# Relion version 4.0 / CCP-EM_pipeliner vers 0.1.0

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_rebox'           No
  'box_size'          100
 'input_map'           'emd_2660_small.map'
 'do_rescale'           Yes
 'rescale_angpix'       1.68
 
