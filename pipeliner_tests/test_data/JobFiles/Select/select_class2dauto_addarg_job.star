# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_job

_rlnJobTypeLabel    relion.select.class2dauto
 
_rlnJobIsContinue    0
 
_rlnJobIsTomo				0


# version 4.0 / CCP-EM_pipeliner / 202102-devel

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  'do_queue'           No 
  'fn_model'           test_optimiser.star 
'image_angpix'           -1 
'min_dedicated'            1 
'other_args'           '' 
'python_exe'       python 
'rank_threshold'          0.5 
        qsub         qsub 
  qsubscript '' 
   queuename      openmpi
   other_args      '--extra arg'
 