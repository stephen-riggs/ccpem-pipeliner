# version 30001

data_job

_rlnJobTypeLabel                    relion.subtract
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
  center_x          0 
  center_y          0 
  center_z          0 
do_center_mask        No 
do_center_xyz         No 
   do_data         No 
do_fliplabel         No 
  do_queue         No 
   fn_data         "" 
fn_fliplabel         "" 
   fn_mask  MaskCreate/job501/mask.mrc
    fn_opt Refine3D/job500/run_it017_optimiser.star 
min_dedicated          1 
   new_box         420 
    nr_mpi          16 
other_args         --here_is_an_additional_arg 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 