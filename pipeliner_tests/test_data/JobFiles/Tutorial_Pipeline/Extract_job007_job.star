
# version 30001

data_job

_rlnJobType                             5
_rlnJobIsContinue                       0
_rlnJobIsTomo				0

# version 30001

data_joboptions_values

loop_ 
_rlnJobOptionVariable #1 
_rlnJobOptionValue #2 
    angpix          1 
bg_diameter        200 
black_dust         -1 
coords_suffix AutoPick/job006/coords_suffix_autopick.star 
do_cut_into_segments        Yes 
do_extract_helical_tubes        Yes 
do_extract_helix         No 
 do_invert        Yes 
   do_norm        Yes 
  do_queue         No 
do_recenter         No 
do_reextract         No 
do_rescale        Yes 
do_reset_offsets         No 
do_set_angpix         No 
extract_size        256 
fndata_reextract         "" 
helical_bimodal_angular_priors        Yes 
helical_nr_asu          1 
helical_rise          1 
helical_tube_outer_diameter        200 
min_dedicated         24 
    nr_mpi          1 
other_args         "" 
      qsub       qsub 
qsubscript "" 
 queuename    openmpi 
recenter_x          0 
recenter_y          0 
recenter_z          0 
   rescale         64 
 star_mics Select/job005/micrographs_selected.star 
white_dust         -1 