data_metadata_reports
loop_
_pipelinerMetadataReportName
_pipelinerInitiatedFrom
_pipelinerNumberOfJobs
MetadataReports/20230322122525_job002.json MotionCorr/job002/ 1
MetadataReports/20230322123534_job003.json CtfFind/job003/ 2
MetadataReports/20230322124555_job004.json AutoPick/job004/ 1
MetadataReports/20230322125784_job005.json Extract/job005/ 4

data_reference_reports
loop_
_pipelinerReferenceReportName
_pipelinerInitiatedFrom
_pipelinerNumberOfJobs
ReferenceReports/20230322122525_job002.json MotionCorr/job002/ 2
ReferenceReports/20230322123534_job003.json CtfFind/job003/ 3
ReferenceReports/20230322124555_job004.json AutoPick/job004/ 4
ReferenceReports/20230322125784_job005.json Extract/job005/ 5

data_project_archives
loop_
_pipelinerArchiveName
_pipelinerInitiatedFrom
_pipelinerArchiveType
ProjectArchives/20230322122525_job002.tar.gz MotionCorr/job002/   full
ProjectArchives/20230322123534_job003.json CtfFind/job003/      full
ProjectArchives/20230322124555_job004.json AutoPick/job004/     simple
ProjectArchives/20230322125784_job005 Extract/job005/      full