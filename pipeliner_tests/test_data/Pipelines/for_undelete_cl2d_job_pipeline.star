
# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                      2
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
Class2D/job008/ Class2D/LoG_based/            relion.class2d.em            Succeeded	 

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Extract/job007/particles.star            ParticleGroupMetadata.star.relion
Class2D/job008/run_it025_data.star			ParticleGroupMetadata.star.relion.class2d
Class2D/job008/run_it025_optimiser.star			OptimiserData.star.relion.class2d

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Extract/job007/particles.star Class2D/job008/ 

# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Class2D/job008/ Class2D/job008/run_it025_optimiser.star
Class2D/job008/ Class2D/job008/run_it025_data.star