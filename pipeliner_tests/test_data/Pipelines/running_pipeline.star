# version 30001 / CCP-EM_pipeliner

data_pipeline_general

_rlnPipeLineJobCounter                       2
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessTypeLabel #3 
_rlnPipeLineProcessStatusLabel #4 
PostProcess/job001/       None           relion.postprocess            Running
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeTypeLabel #2 
Mask/emd_3488_mask.mrc            Mask3D.mrc 
HalfMaps/3488_run_half1_class001_unfil.mrc           DensityMap.mrc.halfmap 
PostProcess/job001/postprocess.mrc           DensityMap.mrc.relion.postprocessed 
PostProcess/job001/postprocess_masked.mrc           DensityMap.mrc.relion.postprocessed.masked
PostProcess/job001/postprocess.star           ProcessData.star.relion.postprocess
PostProcess/job001/logfile.pdf           LogFile.pdf.relion.postprocess
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Mask/emd_3488_mask.mrc PostProcess/job001/ 
HalfMaps/3488_run_half1_class001_unfil.mrc PostProcess/job001/ 
 

# version 30001 / CCP-EM_pipeliner

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
PostProcess/job001/ PostProcess/job001/postprocess.mrc 
PostProcess/job001/ PostProcess/job001/postprocess_masked.mrc 
PostProcess/job001/ PostProcess/job001/postprocess.star 
PostProcess/job001/ PostProcess/job001/logfile.pdf 