#$ -cwd -V
#$ -l h_rt=96:00:00     # specifiy max run time here
#$ -m be
#$ -e XXXerrfileXXX
#$ -o XXXoutfileXXX
#$ -P XXXqueueXXX
#$  -M XXXextra1XXX    # put your email address here 
#$  -l coproc_v100=4   # GPUS in relion should be left blank
 
## load modules 
module load intel/19.0.4 cuda/10.1.168

## Here are some more extra variables
XXXextra2XXX
XXXextra3XXX

## set library paths
export PATH=$CUDA_HOME/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/ufaserv1_k/fbscem/relion_arc/arc4/relion/build/lib/:

## print some diagnostic info 
module list
nvidia-smi -L
which relion_refine_mpi

## run relion

let NSLOTS=NSLOTS/2
XXXcommandXXX

#One more extra command for good measure 
XXXextra4XXX
dedicated nodes: XXXdedicatedXXX
output name: XXXnameXXX
