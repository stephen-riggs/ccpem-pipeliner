#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import os
import shutil
import tempfile
import unittest
from pathlib import Path
from unittest.mock import patch
from pipeliner_tests import test_data

from pipeliner.doppio_tools import (
    get_doppio_filesize_limit,
    get_settings_path,
    set_doppio_map_bin,
)


class PipelinerUserSettingsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="ccpem-pipeliner_")
        self.test_data = os.path.dirname(test_data.__file__)
        self.settings_file = Path(self.test_dir) / "settings.json"

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @patch("platform.system", return_value="Linux")
    @patch.dict("os.environ")
    def test_get_settings_path_linux(self, _):
        os.environ["XDG_CONFIG_HOME"] = self.test_dir
        path = get_settings_path()
        assert (
            path
            == Path(self.test_dir)
            / "ccpem"
            / "doppio"
            / "doppio-config"
            / "app-settings.json"
        )

    @patch("platform.system", return_value="Darwin")
    @patch("pathlib.Path.expanduser")
    def test_get_settings_path_mac(self, mock_path_expanduser, _):
        mock_path_expanduser.return_value = Path(self.test_dir)
        path = get_settings_path()
        assert (
            path
            == Path(self.test_dir)
            / "ccpem"
            / "doppio"
            / "doppio-config"
            / "app-settings.json"
        )

    @patch(
        "pipeliner.doppio_tools.get_settings_path",
        return_value=Path("app-settings.json"),
    )
    def test_get_filesize_limit(self, _):
        test_config = "app-settings.json"
        config_dict = {
            "apiStartTimeoutMs": 100000,
            "knownProjects": ["~/ccpem-project", "/Users/tjg99114/test_aps"],
            "maxResultSizeMB": 99,
            "port": 1430,
            "projectDirectory": "~/ccpem-project",
            "suggestHiddenFilesAsInput": False,
            "themeFile": "doppio_default.json",
        }
        with open(test_config, "w") as f:
            json.dump(config_dict, f)

        assert get_doppio_filesize_limit() == 99.0

    def test_get_doppio_bin_factor(self):
        the_map = Path(self.test_data) / "3488_run_half2_class001_unfil.mrc"  # 4MB map
        binfact, new_px = set_doppio_map_bin(the_map, 5)
        assert binfact == 1.0
        assert new_px == 0
        binfact, new_px = set_doppio_map_bin(the_map, 3)
        assert binfact == 0.9
        assert new_px == 1.167
        binfact, new_px = set_doppio_map_bin(the_map, 1)
        assert binfact == 0.6
        assert new_px == 1.75


if __name__ == "__main__":
    unittest.main()
