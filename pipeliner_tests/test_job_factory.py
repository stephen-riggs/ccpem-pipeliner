#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
from importlib_metadata import entry_points
import unittest
import os

from pipeliner.job_factory import get_job_types, job_from_dict, new_job_of_type
from pipeliner.jobs.ccpem import molrep_job
from pipeliner.jobs.relion import refine3D_job
from pipeliner.jobs.relion import motioncorr_job
from pipeliner.jobs.other import cryolo_job
from pipeliner.jobs.relion.motioncorr_job import RelionMotionCorrOwn

# pipeliner job categories
PIPELINER_JOB_CATEGORY_NAMES = [
    "2D Classification",
    "3D Classification",
    "3D Refinement",
    "Atomic Model Build",
    "Atomic Model Fit",
    "Atomic Model Refine",
    "Atomic Model Split",
    "Atomic Model Utilities",
    "Atomic Model Validation",
    "Automated Class Selection",
    "Automated Particle Picking",
    "Continuous Heterogeneity",
    "Create Ligand",
    "CTF Determination",
    "CTF Refinement",
    "Deposition",
    "Difference Map",
    "External",
    "Extract Particles",
    "Fetch",
    "Image Analysis",
    "Import",
    "Initial Model Generation",
    "Join STAR Files",
    "Local Resolution Determination",
    "Manual Particle Picking",
    "Map Analysis",
    "Map Model Validation",
    "Map Postprocessing",
    "Map Utilities",
    "Mask Creation",
    "Micrograph Analysis",
    "Motion Correction",
    "Multi-body Refinement",
    "Particle Polishing",
    "Particle Subtraction",
    "Reconstruct",
    "Reproject",
    "Simulate",
    "Subset Selection",
    "Workflow",
]


class JobFactoryTest(unittest.TestCase):
    def test_all_jobs_have_same_process_name_in_class_and_entry_point_definitions(self):
        # TODO: avoid the need for this test by removing the duplication of names
        for entry_point in entry_points(group="ccpem_pipeliner.jobs"):
            job_class = entry_point.load()
            assert job_class.PROCESS_NAME == entry_point.name

    def test_get_job_types_returns_all_jobs(self):
        all_jobs = [ep.name for ep in entry_points(group="ccpem_pipeliner.jobs")]
        found_jobs = get_job_types()
        assert len(found_jobs) == len(all_jobs)

    def test_get_job_types_with_empty_search_term_finds_all_jobs(self):
        all_jobs = [ep.name for ep in entry_points(group="ccpem_pipeliner.jobs")]
        found_jobs = get_job_types("")
        assert len(found_jobs) == len(all_jobs)

    def test_get_job_types_with_non_empty_search_term(self):
        all_jobs = [ep.name for ep in entry_points(group="ccpem_pipeliner.jobs")]
        found_jobs = get_job_types("Relion")
        assert len(found_jobs) < len(all_jobs)
        for job in found_jobs:
            assert "relion" in job.PROCESS_NAME.lower()

    def test_new_job_of_type_with_good_job_name(self):
        job = new_job_of_type("relion.refine3d")
        assert type(job) is refine3D_job.RelionRefine3D

    def test_new_job_of_type_with_bad_job_name(self):
        with self.assertRaisesRegex(ValueError, "Job type 'bad_job_name' not found"):
            new_job_of_type("bad_job_name")

    def test_new_job_of_type_with_relion_4_job_type_and_no_options(self):
        with self.assertRaisesRegex(
            ValueError, "Job type 'relion.motioncorr' not found"
        ):
            new_job_of_type("relion.motioncorr")

    def test_new_job_of_type_with_relion_4_job_type_and_incorrect_options(self):
        with self.assertRaisesRegex(
            ValueError, "Job type 'relion.motioncorr' not found; conversion failed"
        ):
            new_job_of_type("relion.motioncorr", {"unknown_option": "Yes"})

    def test_new_job_of_type_with_relion_4_job_type_and_correct_options(self):
        job = new_job_of_type("relion.motioncorr", {"do_own_motioncor": "Yes"})
        assert type(job) is motioncorr_job.RelionMotionCorrOwn

    def test_acedrg_job_does_not_leave_files_behind(self):
        assert not os.path.isdir("AcedrgOut_TMP")
        found_jobs = get_job_types("acedrg")
        assert len(found_jobs) == 1
        assert not os.path.isdir("AcedrgOut_TMP")

    def test_some_example_job_types(self):
        assert type(get_job_types("molrep.atomic_model_fit")[0]) is molrep_job.MolrepJob
        assert type(get_job_types("relion.refine3d")[0]) is refine3D_job.RelionRefine3D
        assert type(get_job_types("cryolo.autopick")[0]) is cryolo_job.CrYOLOAutopick

    def test_job_from_dict(self):
        job = job_from_dict(
            {"_rlnJobTypeLabel": "relion.motioncorr.own", "group_frames": 100}
        )
        assert type(job) is RelionMotionCorrOwn
        assert job.joboptions["group_frames"].value == 100

    def test_job_from_dict_with_compatibility_jobop(self):
        job = job_from_dict(
            {
                "_rlnJobTypeLabel": "relion.motioncorr.own",
                "group_frames": 100,
                "do_own_motioncor": "Yes",  # compatibility JobOption
            }
        )
        assert type(job) is RelionMotionCorrOwn
        assert job.joboptions["group_frames"].value == 100

    def test_job_from_dict_with_bad_jobopt(self):
        with self.assertRaises(ValueError):
            job_from_dict(
                {
                    "_rlnJobTypeLabel": "relion.motioncorr.own",
                    "group_frames": 100,
                    "Bad_job_op": "doodoodoodooo",  # bad JobOption
                }
            )

    def test_all_jobs_have_valid_categories(self):
        all_jobs = [ep.name for ep in entry_points(group="ccpem_pipeliner.jobs")]
        test_pass = True
        for i in all_jobs:
            job = new_job_of_type(i)
            if job.get_category_label() not in PIPELINER_JOB_CATEGORY_NAMES:
                print(
                    f"Job {job.PROCESS_NAME} has non-standard category "
                    f"{job.get_category_label()}"
                )
                test_pass = False
        assert test_pass


if __name__ == "__main__":
    unittest.main()
