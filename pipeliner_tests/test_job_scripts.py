#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
import math
from pathlib import Path

from pipeliner_tests import test_data
from pipeliner.utils import run_subprocess, get_python_command, touch
from pipeliner.scripts.job_scripts import (
    get_map_parameters,
    check_star_file,
    cleanup_tmp_files,
    check_for_output_files,
)
from pipeliner.data_structure import RELION_SUCCESS_FILE


class PipelinerJobScriptsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_jobscripts")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_run_subprocess_get_map_parameters(self):
        map_input = os.path.join(self.test_data, "emd_3488.mrc")

        run_subprocess(
            get_python_command()
            + [
                get_map_parameters.__file__,
                "-m",
                map_input,
                "-odir",
                self.test_dir,
            ],
            capture_output=True,
            text=True,
        )
        assert os.path.isfile(
            os.path.join(self.test_dir, "emd_3488_map_parameters.json")
        )
        assert math.isclose(
            os.stat(
                os.path.join(self.test_dir, "emd_3488_map_parameters.json")
            ).st_size,
            326,
            rel_tol=0.05,
        )

    def test_check_starfile_all_good(self):
        datafile = str(Path(self.test_data) / "class2d_data.star")
        assert check_star_file.main(["--fn_in", datafile, "--block", "particles"])

    def test_check_starfile_all_good_sole_block(self):
        datafile = str(Path(self.test_data) / "sole_block.star")
        assert check_star_file.main(["--fn_in", datafile])

    def test_check_starfile_bad_blockname(self):
        datafile = str(Path(self.test_data) / "class2d_data.star")
        with self.assertRaises(RuntimeError):
            check_star_file.main(["--fn_in", datafile, "--block", "bad"])

    def test_check_starfile_empty(self):
        datafile = str(Path(self.test_data) / "empty.star")
        with self.assertRaises(RuntimeError):
            check_star_file.main(["--fn_in", datafile, "--block", "coordinate_files"])

    def test_check_starfile_empty_removes_status_file(self):
        datafile = str(Path(self.test_data) / "empty.star")
        shutil.copy(datafile, self.test_dir)
        sf = Path(self.test_dir) / RELION_SUCCESS_FILE
        touch(str(sf))
        with self.assertRaises(RuntimeError):
            check_star_file.main(
                [
                    "--fn_in",
                    "empty.star",
                    "--block",
                    "coordinate_files",
                    "--clear_relion_success_file",
                ]
            )
        assert not sf.is_file()

    def test_check_starfile_empty_soleblock(self):
        datafile = str(Path(self.test_data) / "empty_sole_block.star")
        with self.assertRaises(RuntimeError):
            check_star_file.main(["--fn_in", datafile])

    def test_cleanup_tmp_files(self):
        t1 = Path("tmp1.txt")
        t2 = Path("tmp2.txt")
        t3 = Path("tmp1/tmp2/test.tmp")
        d1 = Path("mydir/")
        d2 = Path("dir1/dir2/")
        xd1 = Path("nonexistent_dir/")
        xf1 = Path("nonexistent.txt")
        for d in [t3.parent, d1, d2]:
            d.mkdir(parents=True)
            assert d.is_dir()

        for f in [t1, t2, t3]:
            f.touch()
            assert f.is_file()

        cleanup_tmp_files.main(
            [
                "--files",
                str(t1),
                str(t2),
                str(t3),
                str(xf1),
                "--dirs",
                str(d1),
                str(d2),
                str(xd1),
            ]
        )
        for f in [t1, t2, t3]:
            assert not f.is_file()
        for d in [d1, d2]:
            assert not d.is_dir()

    def test_check_for_output_files(self):
        files = [f"f{n}.txt" for n in range(3)]
        for f in files:
            Path(f).touch()
        check_for_output_files.main(["--files"] + files)

    def test_check_for_output_files_missing(self):
        files = [f"f{n}.txt" for n in range(3)]
        for f in files:
            Path(f).touch()
        files.append("file4.txt")
        with self.assertRaises(ValueError):
            check_for_output_files.main(["--files"] + files)


if __name__ == "__main__":
    unittest.main()
