#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import tempfile
import shutil
from pathlib import Path

from pipeliner.job_options import (
    files_exts,
    IntJobOption,
    FloatJobOption,
    ExternalFileJobOption,
    StringJobOption,
    InputNodeJobOption,
    MultipleChoiceJobOption,
    BooleanJobOption,
    MultiExternalFileJobOption,
    MultiStringJobOption,
    DirPathJobOption,
    JobOptionCondition,
    SearchStringJobOption,
    MultiInputNodeJobOption,
)
from pipeliner_tests import test_data
from pipeliner.utils import touch


class JobOptionsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_files_extensions_single(self):
        name = "Test file type"
        exts = [".mrc"]
        result = files_exts(name, exts)
        assert result == "Test file type (*.mrc)"

    def test_files_extensions_multiple(self):
        name = "Test file types"
        exts = [".mrc", "_unfil.mrcs", ".txt"]
        result = files_exts(name, exts)
        assert result == "Test file types (*.mrc *_unfil.mrcs *.txt)"

    def test_files_extensions_single_exact(self):
        name = "Exact file type"
        exts = ["myfile.mrc"]
        result = files_exts(name, exts, True)
        assert result == "Exact file type (myfile.mrc)"

    def test_files_extensions_multiple_exact(self):
        name = "Exact file types"
        exts = ["myfile.mrc", "f_unfil.mrcs", "text.txt"]
        result = files_exts(name, exts, True)
        assert result == "Exact file types (myfile.mrc f_unfil.mrcs text.txt)"

    # IntJobOption
    def test_validate_int_jo_noerror(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
        )
        int_jo.value = 1
        assert len(int_jo.validate()) == 0

    def test_validate_int_jo_is_required(self):
        int_jo = IntJobOption(
            label="Test int JobOption", default_value=0, is_required=True
        )
        int_jo.value = None
        valobj = int_jo.validate()
        ermsg = "This option is required"
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is IntJobOption

    def test_validate_int_jo_below_hard_min(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
            is_required=True,
            hard_min=0,
        )
        int_jo.value = -1
        ermsg = "Value cannot be less than 0"
        valobj = int_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is IntJobOption

    def test_validate_int_jo_above_hard_max(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
            is_required=True,
            hard_max=0,
        )
        int_jo.value = 1
        ermsg = "Value cannot be greater than 0"
        valobj = int_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is IntJobOption

    def test_validate_int_jo_below_soft_min(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
            is_required=True,
            suggested_min=0,
        )
        int_jo.value = -5
        ermsg = "Value outside suggested range: Min 0"
        valobj = int_jo.validate()
        assert len(valobj) == 1
        assert valobj[0].type == "warning"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is IntJobOption

    def test_validate_int_jo_below_soft_min_negtive_one(self):
        """'-1 shouldn't trigger the soft min because it is often used as default'"""
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
            is_required=True,
            suggested_min=0,
        )
        int_jo.value = -1
        valobj = int_jo.validate()
        assert len(valobj) == 0

    def test_validate_int_jo_above_soft_max(self):
        int_jo = IntJobOption(
            label="Test int JobOption",
            default_value=0,
            is_required=True,
            suggested_max=0,
        )
        int_jo.value = 1
        ermsg = "Value outside suggested range: Max 0"
        valobj = int_jo.validate()
        assert len(valobj) == 1
        assert valobj[0].type == "warning"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is IntJobOption

    # FloatJobOption

    def test_validate_float_jo_noerror(self):
        float_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0,
        )
        float_jo.value = 1
        assert len(float_jo.validate()) == 0

    def test_validate_float_jo_is_required(self):
        float_jo = FloatJobOption(
            label="Test float JobOption", default_value=0, is_required=True
        )
        float_jo.value = None
        ermsg = "This option is required"
        valobj = float_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is FloatJobOption

    def test_validate_float_jo_below_hard_min(self):
        float_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0,
            is_required=True,
            hard_min=0,
        )
        float_jo.value = -1
        ermsg = "Value cannot be less than 0"
        valobj = float_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is FloatJobOption

    def test_validate_float_jo_above_hard_max(self):
        float_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0,
            is_required=True,
            hard_max=0,
        )
        float_jo.value = 1
        ermsg = "Value cannot be greater than 0"
        valobj = float_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is FloatJobOption

    def test_validate_float_jo_below_soft_min(self):
        int_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0.0,
            is_required=True,
            suggested_min=0.0,
        )
        int_jo.value = -5.0
        ermsg = "Value outside suggested range: Min 0.0"
        valobj = int_jo.validate()
        assert len(valobj) == 1
        assert valobj[0].type == "warning"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is FloatJobOption

    def test_validate_float_jo_below_soft_min_negtive_one(self):
        """'-1' shouldn't trigger the soft min because it is often used as default"""
        int_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0.0,
            is_required=True,
            suggested_min=0.0,
        )
        int_jo.value = -1.0
        valobj = int_jo.validate()
        assert len(valobj) == 0

    def test_validate_float_jo_above_soft_max(self):
        int_jo = FloatJobOption(
            label="Test float JobOption",
            default_value=0.0,
            is_required=True,
            suggested_max=0.0,
        )
        int_jo.value = 1.0
        ermsg = "Value outside suggested range: Max 0.0"
        valobj = int_jo.validate()
        assert len(valobj) == 1
        assert valobj[0].type == "warning"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is FloatJobOption

    # StringJobOption

    def test_validate_string_jo_noerror(self):
        float_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
        )
        assert len(float_jo.validate()) == 0

    def test_validate_string_jo_not_required(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
        )
        string_jo.value = ""
        assert len(string_jo.validate()) == 0

    def test_validate_string_joboption_regexmatches(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        string_jo.value = "test string 123"
        assert len(string_jo.validate()) == 0

    def test_validate_string_joboption_regex_nomatch(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        string_jo.value = "NONONO string 123"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = string_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is StringJobOption

    def test_validate_string_joboption_regex_nomatch_with_message(self):
        string_jo = StringJobOption(
            label="Test string JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
            regex_error_message="Value does not match",
        )
        string_jo.value = "NONONO string 123"
        ermsg = "Value does not match"
        valobj = string_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is StringJobOption

    # MultiStringJobOption

    def test_validate_multistring_joboption_regex_nomatch(self):
        string_jo = MultiStringJobOption(
            label="Test string JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        string_jo.value = "NONONO string 123:::test .+ 123:::badbad"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = string_jo.validate()
        assert type(valobj) is list
        assert len(valobj) == 2
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is MultiStringJobOption
        assert valobj[1].type == "error"
        assert valobj[1].message == ermsg
        assert type(valobj[1].raised_by[0]) is MultiStringJobOption

    def test_validate_multistring_joboption_regex_nomatch_with_message(self):
        string_jo = MultiStringJobOption(
            label="Test string JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
            regex_error_message="Value does not match",
        )
        string_jo.value = "test .+ 123:::NONONO string 123:::badbadbad"
        ermsg = "Value does not match"
        valobj = string_jo.validate()
        assert type(valobj) is list
        assert len(valobj) == 2
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is MultiStringJobOption
        assert valobj[1].type == "error"
        assert valobj[1].message == ermsg
        assert type(valobj[1].raised_by[0]) is MultiStringJobOption

    def test_multistring_returns_list(self):
        string_jo = MultiStringJobOption(
            label="Test string JobOption",
            default_value="String",
        )
        string_jo.value = "str1:::str2:::str3"
        assert string_jo.get_list() == ["str1", "str2", "str3"]

    def test_multistring_returns_single_value(self):
        string_jo = MultiStringJobOption(
            label="Test string JobOption",
            default_value="String",
        )
        string_jo.value = "single"
        assert string_jo.get_list() == ["single"]

    def test_multistring_returns_list_from_single_val(self):
        msjo = MultiStringJobOption(label="testjo")
        msjo.value = "Test"
        sval = msjo.get_list()
        assert sval == ["Test"]

    def test_multistring_works_with_spaces(self):
        msjo = MultiStringJobOption(label="testjo")
        msjo.value = "Test ::: Test1:::   Test2 :::          Test3"
        sval = msjo.get_list()
        assert sval == ["Test", "Test1", "Test2", "Test3"]

    def test_multistring_with_regex_allmatch(self):
        msjo = MultiStringJobOption(label="testjo", validation_regex="test .+ 123")
        msjo.value = "test string 123 ::: test string2 123"
        sval = msjo.get_list()
        assert sval == ["test string 123", "test string2 123"]
        vali = msjo.validate()
        assert vali == []

    # ExternalFileNameJobOption

    def test_filename_jo_empty_string(self):
        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value="",
        )
        assert filename_jo.get_string() == ""

    def test_filename_jo_get_basename(self):
        jo = ExternalFileJobOption(
            label="Test inputnode JobOption",
            default_value="Import/job001/file.star",
        )
        assert jo.get_basename_mapping() == {"Import/job001/file.star": "file.star"}

    def test_validate_filename_jo_not_required(self):
        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value="String",
        )
        filename_jo.value = ""
        assert len(filename_jo.validate()) == 0

    def test_validate_filename_joboption_regexmatches(self):
        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        filename_jo.value = "test filename 123"
        assert len(filename_jo.validate()) == 0

    def test_validate_filename_joboption_regex_nomatch(self):
        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
        )
        filename_jo.value = "NONONO filename 123"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = filename_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is ExternalFileJobOption

    def test_filename_exists_validation_allOK(self):
        """File actually exists"""

        test_file = os.path.join(self.test_dir, "test.txt")
        touch(test_file)
        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value=test_file,
            is_required=True,
        )
        filename_jo.value = test_file
        assert len(filename_jo.check_file()) == 0

    def test_filename_exists_validation_file_missing_validate_doesnt_find(self):
        """should only return warning because file might be created later"""

        test_file = os.path.join(self.test_dir, "test.txt")
        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value=test_file,
            is_required=True,
        )
        filename_jo.value = test_file
        val = filename_jo.validate()
        assert len(val) == 1
        assert val[0].message == "File not found"
        assert val[0].type == "warning"

    def test_external_check_file_missing(self):
        """The check_file() function should catch a missing file"""

        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value="missing_file.txt",
            is_required=True,
        )
        result = filename_jo.check_file()
        assert len(result) == 1
        assert result[0].message == "File missing_file.txt not found"
        assert result[0].type == "error"

    def test_external_check_file_exists(self):
        """The check_file() function should catch a missing file"""

        test_file = os.path.join(self.test_dir, "test.txt")
        touch(test_file)
        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value=test_file,
            is_required=True,
        )
        filename_jo.value = test_file
        result = filename_jo.check_file()
        assert len(result) == 0

    def test_filename_exists_validation_file_inPATH(self):
        """File is an exe in the $PATH shouldn't fail validation"""

        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value="ls",
            is_required=True,
        )
        filename_jo.value = "ls"
        assert len(filename_jo.check_file()) == 0

    def test_filename_exists_but_is_not_a_file(self):
        Path("test").mkdir()
        filename_jo = ExternalFileJobOption(
            label="Test filename JobOption",
            default_value="test",
            is_required=True,
        )
        val = filename_jo.validate()
        assert len(val) == 1
        assert val[0].type == "error"
        assert val[0].message == "Target is not a file"

    # SearchStringJobOption

    def test_search_string_validate_none_found(self):
        """Warning because files might be created later"""
        jo = SearchStringJobOption(
            label="Test filename JobOption",
            default_value="test/*.mrc",
            is_required=True,
        )
        jo.value = "test/*.mrc"
        ermsg = "No files found"
        valobj = jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "warning"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is SearchStringJobOption

    def test_search_string_counts_files(self):
        os.makedirs("test")
        for n in range(5):
            touch(f"test/file_{n}.txt")
        jo = SearchStringJobOption(
            label="Test filename JobOption",
            default_value="test/*.mrc",
            is_required=True,
        )
        jo.value = "test/*.txt"
        msg = "5 files found"
        valobj = jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "info"
        assert valobj[0].message == msg
        assert type(valobj[0].raised_by[0]) is SearchStringJobOption

    def test_searchstring_checkfile_missing(self):
        """Error because files must exist"""
        jo = SearchStringJobOption(
            label="Test filename JobOption",
            default_value="test/*.mrc",
            is_required=True,
        )
        jo.value = "test/*.mrc"
        ermsg = "No files found with search string test/*.mrc"
        valobj = jo.check_file()
        assert len(valobj) == 1
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is SearchStringJobOption

    def test_search_string_returns(self):
        os.makedirs("test")
        files = [f"test/file_{n}.txt" for n in range(5)]
        for f in files:
            touch(f)
        jo = SearchStringJobOption(
            label="Test filename JobOption",
            default_value="test/*.txt",
            is_required=True,
        )
        jo.value = "test/*.txt"
        found = jo.get_list()
        found.sort()
        assert found == files
        assert jo.get_string() == "test/*.txt"

    # InputNodeJobOption

    def test_inputnode_jo_empty_string(self):
        float_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="",
            node_type="test.node",
        )
        assert float_jo.get_string() == ""
        assert float_jo.get_input_nodes() == []

    def test_inputnode_jo_get_basename(self):
        float_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="Import/job001/file.star",
            node_type="test.node",
        )
        assert float_jo.get_basename_mapping() == {
            "Import/job001/file.star": "file.star"
        }

    def test_validate_inputnode_jo_file_missing_noerror(self):
        "Validation should not return error because file might be created later"
        float_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            node_type="test.node",
        )
        assert len(float_jo.validate()) == 0

    def test_validate_inputnode_joboption_regexmatches(self):
        inputnode_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
            node_type="test.node",
        )
        inputnode_jo.value = "test inputnode 123"
        assert len(inputnode_jo.validate()) == 0

    def test_validate_inputnode_joboption_regex_nomatch(self):
        inputnode_jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="String",
            is_required=True,
            validation_regex="test .+ 123",
            node_type="test.node",
        )
        inputnode_jo.value = "NONONO inputnode 123"
        ermsg = "Value format must match pattern 'test .+ 123'"
        valobj = inputnode_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is InputNodeJobOption

    def test_inputnode_return_node_nokwds(self):
        jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="file.mrc",
            is_required=True,
            validation_regex="test .+ 123",
            node_type="test",
        )
        nodes = jo.get_input_nodes()
        assert len(nodes) == 1
        assert nodes[0].type == "test.mrc"

    def test_inputnode_return_with_kwds(self):
        jo = InputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="file.mrc",
            is_required=True,
            validation_regex="test .+ 123",
            node_type="test",
            node_kwds=["kw1", "kw2"],
        )
        nodes = jo.get_input_nodes()
        assert len(nodes) == 1
        assert nodes[0].type == "test.mrc.kw1.kw2"

    # MultiInputNodeJobOption

    def test_multiinputnode_jo_empty_string(self):
        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="",
            node_type="test.node",
        )
        assert jo.get_list() == []
        assert jo.get_input_nodes() == []

    def test_muliinputnode_jo_get_basename_mapping(self):
        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="dir1/file1.txt:::dir2/file2.txt",
            node_type="test.node",
        )
        assert jo.get_basename_mapping() == {
            "dir1/file1.txt": "file1.txt",
            "dir2/file2.txt": "file2.txt",
        }

    def test_validate_multiinputnode_jo_file_missing(self):
        "Validation should return warning because file might be created later"
        touch("f1.txt")
        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="f1.txt:::f2.txt:::f3.txt",
            node_type="test.node",
        )
        val = jo.validate()
        assert len(val) == 2
        assert val[0].message == "File f2.txt does not exist yet"
        assert val[0].type == "warning"
        assert val[1].message == "File f3.txt does not exist yet"
        assert val[1].type == "warning"

    def test_checkfile_multiinputnode_jo_file_missing(self):
        "Checkfile should return error if file missing"
        touch("f1.txt")
        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="f1.txt:::f2.txt:::f3.txt",
            node_type="test.node",
        )
        val = jo.check_file()
        assert len(val) == 2
        assert val[0].message == (
            "File f2.txt not found, has the job expected to produce it finished "
            "successfully?"
        )
        assert val[0].type == "error"
        assert val[1].message == (
            "File f3.txt not found, has the job expected to produce it finished "
            "successfully?"
        )
        assert val[1].type == "error"

    def test_validate_multiinputnode_joboption_regexmatches(self):
        for n in range(1, 3):
            touch(f"f{n}.txt")
        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="f1.txt:::f2.txt",
            validation_regex="f[12].txt",
            node_type="test.node",
        )
        assert len(jo.validate()) == 0

    def test_validate_multiinputnode_joboption_regex_nomatch(self):
        for n in range(1, 5):
            touch(f"f{n}.txt")
        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="f1.txt:::f2.txt:::f3.txt:::f4.txt",
            validation_regex="f[12].txt",
            node_type="test.node",
        )
        val = jo.validate()
        assert len(val) == 2
        msgs = [
            "f3.txt format must match pattern 'f[12].txt'",
            "f4.txt format must match pattern 'f[12].txt'",
        ]
        assert [x.message for x in val] == msgs
        assert all([x.type == "error" for x in val])

    def test_validate_multiinputnode_joboption_noduplicates(self):
        touch("f1.txt")
        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="f1.txt:::f1.txt",
            validation_regex="f[12].txt",
            node_type="test.node",
        )
        val = jo.validate()
        assert len(val) == 1
        assert val[0].message == "Files cannot have duplicate names"
        assert val[0].type == "error"

    def test_validate_multiinputnode_joboption_noduplicates_dirs(self):
        for dir in [Path("test1"), Path("test2")]:
            dir.mkdir()
            f = dir / "f1.txt"
            f.touch()
        touch("f1.txt")

        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="test1/f1.txt:::test2/f1.txt",
            node_type="test.node",
        )
        val = jo.validate()
        for f in val:
            print(val.__dict__)
        assert len(val) == 0

    def test_multiinputnode_return_nodes_nokwds(self):
        files = [f"f{n}.txt" for n in range(1, 3)]
        for f in files:
            touch(f)
        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="f1.txt:::f2.txt",
            node_type="test",
        )
        names = jo.get_list()
        assert names == files
        nodes = jo.get_input_nodes()
        assert [x.name for x in nodes] == files
        assert all([x.type == "test.txt" for x in nodes])

    def test_multiinputnode_return_nodes_with_kwds(self):
        jo = MultiInputNodeJobOption(
            label="Test inputnode JobOption",
            default_value="f1.txt:::f2.txt:::f3.txt",
            node_type="node1",
            node_kwds=["kw1a", "kw1b"],
        )
        nodes = jo.get_input_nodes()
        assert len(nodes) == 3
        assert all([x.type == "node1.txt.kw1a.kw1b" for x in nodes])

    # MultipleChoiceJobOption

    def test_validate_multichoice_jo_noerror(self):
        mc_jo = MultipleChoiceJobOption(
            label="Test mc JobOption",
            choices=["1", "2", "3", "4"],
            default_value_index=0,
        )
        assert len(mc_jo.validate()) == 0

    def test_validate_multichoice_bad_option(self):
        mc_jo = MultipleChoiceJobOption(
            label="Test mc JobOption",
            choices=["1", "2", "3", "4"],
            default_value_index=0,
        )
        mc_jo.value = "mbili"
        ermsg = "'mbili' not in list of accepted values: ['1', '2', '3', '4']"
        valobj = mc_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is MultipleChoiceJobOption

    def test_validate_multichoice_jo_noval_required(self):
        mc_jo = MultipleChoiceJobOption(
            label="Test mc JobOption",
            choices=["1", "2", "3", "4"],
            default_value_index=0,
            is_required=True,
        )
        mc_jo.value = ""
        ermsg = "This option is required"
        valobj = mc_jo.validate()
        assert type(valobj) is list
        assert valobj[0].type == "error"
        assert valobj[0].message == ermsg
        assert type(valobj[0].raised_by[0]) is MultipleChoiceJobOption

    # BooleanJobOption

    def test_validate_bool_joboption_noerror(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        assert len(bool_jo.validate()) == 0

    def test_validate_bool_joboption_yes(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = "Yes"
        assert len(bool_jo.validate()) == 0

    def test_validate_bool_joboption_no(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = "No"
        assert len(bool_jo.validate()) == 0

    def test_bool_joboption_with_value_yes(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = "Yes"
        assert bool_jo.get_boolean() is True

    def test_bool_joboption_with_value_no(self):
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = "No"
        assert bool_jo.get_boolean() is False

    def test_bool_joboption_with_value_empty(self):
        # Empty string is False, even if default value was True
        bool_jo = BooleanJobOption(
            label="Boolio",
            default_value=True,
        )
        bool_jo.value = ""
        assert bool_jo.get_boolean() is False

    # DirPathJobOption

    def test_dirpath_validate_no_dir_warning(self):
        """Returns a warning as dir may be created later"""
        jo = DirPathJobOption(label="dirjo", must_exist=False, default_value="mydir")
        val = jo.validate()
        assert len(val) == 1
        assert val[0].message == "Directory does not exist"
        assert val[0].type == "warning"

    def test_dirpath_validate_no_dir_error(self):
        """Returns a warning as dir may be created later"""
        jo = DirPathJobOption(label="dirjo", must_exist=True, default_value="mydir")
        val = jo.validate()
        assert len(val) == 1
        assert val[0].message == "Directory does not exist"
        assert val[0].type == "error"

    def test_dirpath_target_is_file(self):
        touch("itsafile")
        jo = DirPathJobOption(label="dirjo", default_value="itsafile")
        val = jo.validate()
        assert len(val) == 1
        assert val[0].message == "Path is a file; must be a directory"
        assert val[0].type == "error"

    def test_dirpath_validate_must_be_in_project(self):
        """Returns a warning as dir may be created later"""
        jo = DirPathJobOption(
            label="dirjo",
            must_be_in_project=True,
            default_value="/",
        )
        val = jo.validate()
        assert len(val) == 1
        assert val[0].message == (
            "This field requires a path relative to the project and all files must be "
            "in the project.  IE: Path cannot start with '/' or '../'"
        )
        assert val[0].type == "error"

    def test_dirpath_validate_outside_project_ok(self):
        """Returns a warning as dir may be created later"""
        jo = DirPathJobOption(
            label="dirjo",
            must_be_in_project=False,
            default_value="/",
        )
        val = jo.validate()
        assert len(val) == 0

    def test_dirpath_validate_must_be_in_project_alternatives(self):
        """Returns a warning as dir may be created later"""
        jo = DirPathJobOption(
            label="dirjo",
            must_be_in_project=True,
            default_value="",
        )

        msg = (
            "This field requires a path relative to the project and all files must be "
            "in the project.  IE: Path cannot start with '/' or '../'"
        )

        jo.value = "~/"
        val = jo.validate()
        assert len(val) == 2
        assert val[0].message == msg
        assert val[0].type == "error"

        jo.value = "../"
        val = jo.validate()
        assert len(val) == 1
        assert val[0].message == msg
        assert val[0].type == "error"

    # MultiExternalFileJobOption

    def test_multifile_returns_list(self):
        mfjo = MultiExternalFileJobOption(label="testjo")
        mfjo.value = "test1:::test2:::test3:::test4"
        assert mfjo.get_list() == ["test1", "test2", "test3", "test4"]

    def test_mulifile_get_basename_mapping(self):
        jo = MultiExternalFileJobOption(
            label="Test inputnode JobOption",
            default_value="dir1/file1.txt:::dir2/file2.txt",
        )
        assert jo.get_basename_mapping() == {
            "dir1/file1.txt": "file1.txt",
            "dir2/file2.txt": "file2.txt",
        }

    def test_check_file_multifile(self):
        touch("f1.txt")
        touch("f2.txt")
        mfjo = MultiExternalFileJobOption(label="testjo", is_required=True)
        mfjo.value = "f1.txt ::: f2.txt"
        assert not mfjo.check_file()

    def test_multifile_validate_missing_files(self):
        """Should return warnings as files might be created later"""
        touch("f1.txt")
        mfjo = MultiExternalFileJobOption(label="testjo")
        mfjo.value = "f1.txt:::f2.txt:::f3.txt"
        val = mfjo.validate()
        assert len(val) == 2
        for vm in val:
            assert vm.type == "warning"
        valmsg = [x.message for x in val]
        for f in ["f2", "f3"]:
            assert f"{f}.txt; file not found" in valmsg

    def test_multifile_checkfile_nofiles(self):
        """Should return errors for missing files"""
        touch("f1.txt")
        mfjo = MultiExternalFileJobOption(label="testjo")
        mfjo.value = "f1.txt:::f2.txt:::f3.txt"
        val = mfjo.check_file()
        assert len(val) == 2
        for vm in val:
            assert vm.type == "error"
        valmsg = [x.message for x in val]
        for f in ["f2", "f3"]:
            assert f"File not found: {f}.txt" in valmsg

    def test_multifile_validate_duplicate_files(self):
        """Should return error if duplicate file names exist"""
        touch("f1.txt")
        mfjo = MultiExternalFileJobOption(label="testjo")
        mfjo.value = "f1.txt:::f1.txt"
        val = mfjo.validate()
        assert len(val) == 1
        assert val[0].message == "Files cannot have duplicate names"

    def test_multifile_validate_duplicate_files_with_dirs(self):
        """Should return error if duplicate file names exist"""
        for dir in [Path("test1"), Path("test2")]:
            dir.mkdir()
            f = dir / "f1.txt"
            f.touch()
        touch("f1.txt")
        mfjo = MultiExternalFileJobOption(label="testjo")
        mfjo.value = "test1/f1.txt:::test2/f1.txt"
        val = mfjo.validate()
        assert len(val) == 0

    # JobOptionCondition tests

    def test_condition_single(self):
        testjo = IntJobOption(label="test jobop", default_value=1)
        jobops = {"test": testjo}
        testjo.value = 1
        true_con = JobOptionCondition(conditions=[("test", "=", 1)])
        check, passes, err = true_con.check_condition_is_met(jobops)
        assert check
        assert err == []
        assert passes == [("test", "=", 1)]

        false_con = JobOptionCondition(conditions=[("test", "=", 0)])
        check, passes, err = false_con.check_condition_is_met(jobops)
        assert not check
        assert err == [("test", "=", 0)]
        assert passes == []

    def test_condition_any(self):
        int_test = IntJobOption(label="test jobop", default_value=1)
        bool_test = BooleanJobOption(label="bool test", default_value=True)

        jobops = {"int_test": int_test, "bool_test": bool_test}
        int_test.value = 1
        bool_test.value = "True"
        true_con = JobOptionCondition(
            conditions=[("int_test", "=", 1), ("bool_test", "=", True)],
            operation="ANY",
        )
        check, passes, err = true_con.check_condition_is_met(jobops)
        assert check
        assert err == []
        assert passes == [("int_test", "=", 1), ("bool_test", "=", True)]

        one_con = JobOptionCondition(
            conditions=[("int_test", "=", 1), ("bool_test", "=", False)],
            operation="ANY",
        )
        check, passes, err = one_con.check_condition_is_met(jobops)
        assert check
        assert err == [("bool_test", "=", False)]
        assert passes == [("int_test", "=", 1)]

        false_con = JobOptionCondition(
            conditions=[("int_test", "=", 0), ("bool_test", "=", False)],
            operation="ANY",
        )
        check, passes, err = false_con.check_condition_is_met(jobops)
        assert not check
        assert err == [("int_test", "=", 0), ("bool_test", "=", False)]
        assert passes == []

    def test_condition_all(self):
        int_test = IntJobOption(label="test jobop", default_value=1)
        bool_test = BooleanJobOption(label="bool test", default_value=True)

        jobops = {"int_test": int_test, "bool_test": bool_test}
        int_test.value = 1
        bool_test.value = "True"
        true_con = JobOptionCondition(
            conditions=[("int_test", "=", 1), ("bool_test", "=", True)],
            operation="ALL",
        )
        check, passes, errs = true_con.check_condition_is_met(jobops)
        assert check
        assert errs == []
        assert passes == [("int_test", "=", 1), ("bool_test", "=", True)]

        false_not_all_con = JobOptionCondition(
            conditions=[("int_test", "=", 1), ("bool_test", "=", False)],
            operation="ALL",
        )
        check, passes, errs = false_not_all_con.check_condition_is_met(jobops)
        assert not check
        assert errs == [("bool_test", "=", False)]
        assert passes == [("int_test", "=", 1)]

        false_all_con = JobOptionCondition(
            conditions=[("int_test", "=", 0), ("bool_test", "=", False)],
            operation="ALL",
        )
        check, passes, errs = false_all_con.check_condition_is_met(jobops)
        assert not check
        assert errs == [("int_test", "=", 0), ("bool_test", "=", False)]
        assert passes == []


if __name__ == "__main__":
    unittest.main()
