#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import tempfile
import shutil
import mrcfile
import numpy as np
from pathlib import Path

from pipeliner.mrc_image_tools import (
    get_mrc_dims,
    get_mrc_stats,
    unstack_mrcs,
    stack_mrcs,
    substack_mrcs,
    get_tiff_dims,
    get_tiff_stats,
    read_tiff,
    remove_outliers,
    norm_array,
    threed_array_to_montage,
    threed_array_to_images,
    mrcs_slices_montage,
    mrcs_slices_montage_base64,
    normalise_images,
    norm_image_to_base64_string,
)
from pipeliner_tests import test_data


class PipelinerMrcImageToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_mrc_dims_3d(self):
        dims = get_mrc_dims(os.path.join(self.test_data, "emd_3488.mrc"))
        assert dims == (100, 100, 100), dims

    def test_get_mrc_dims_stack(self):
        dims = get_mrc_dims(os.path.join(self.test_data, "image_stack.mrcs"))
        assert dims == (64, 64, 215), dims

    def test_get_stats_stack(self):
        stats = get_mrc_stats(os.path.join(self.test_data, "image_stack.mrcs"), True)
        exp0 = [
            (64, 64),
            "-3.287",
            "4.297",
            "0.3098",
            "1.08",
            ("3.54", "3.54"),
            "12: 16-bit float (IEEE754)",
        ]
        exp1 = [
            (64, 64),
            "-4.086",
            "4.26",
            "0.04053",
            "1.0205",
            ("3.54", "3.54"),
            "12: 16-bit float (IEEE754)",
        ]
        assert len(stats) == 215

        # format everything to do comparisons even if types don't match'
        assert [int(x) for x in stats[0][0]] == [x for x in exp0[0]]
        for n, st in enumerate(stats[0][1:5]):
            assert str(st) == exp0[n + 1], (str(st), exp0[n + 1], n + 1)
        assert [str(x) for x in stats[0][5]] == [x for x in exp0[5]]
        assert stats[0][6] == exp0[6]

        # format everything to do comparisons even if types don't match'
        assert [int(x) for x in stats[-1][0]] == [x for x in exp1[0]]
        for n, st in enumerate(stats[-1][1:5]):
            assert str(st) == exp1[n + 1], (str(st), exp1[n + 1], n + 1)
        assert [str(x) for x in stats[-1][5]] == [x for x in exp1[5]]
        assert stats[-1][6] == exp1[6]

    def test_get_stats_3d(self):
        stats = get_mrc_stats(os.path.join(self.test_data, "emd_3488.mrc"))
        exp = [
            (100, 100, 100),
            "-0.54142815",
            "0.70043814",
            "0.0015229764",
            "0.03714919",
            ("1.05", "1.05", "1.05"),
            "2: 32-bit signed real",
        ]

        # format everything to do comparisons even if types don't match'
        assert [int(x) for x in stats[0]] == [x for x in exp[0]]
        for n, st in enumerate(stats[1:4]):
            assert str(st) == exp[n + 1], (str(st), exp[n + 1])
        assert [str(x) for x in stats[5]] == [x for x in exp[5]]
        assert stats[6] == exp[6]

    def test_unstack_1_img(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        outfile, count = unstack_mrcs(img_stack, [0])
        assert os.path.isfile("unstack_0.mrc")
        assert not os.path.isfile("unstack_1.mrc")
        assert count == 1
        assert outfile == "unstack_%.mrc"

    def test_unstack_100_imgs(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        outfile, count = unstack_mrcs(img_stack, range(100))
        for n in range(100):
            assert os.path.isfile(f"unstack_{n:03d}.mrc")
        assert not os.path.isfile("unstack_100.mrc")
        assert count == 100
        assert outfile == "unstack_%%%.mrc"

    def test_unstack_all_imgs(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        outfile, count = unstack_mrcs(img_stack)
        for n in range(214):
            assert os.path.isfile(f"unstack_{n:03d}.mrc")
        assert not os.path.isfile("unstack_215.mrc")
        assert outfile == "unstack_%%%.mrc"
        assert count == 215, count

    def test_unstack_nondefault_name(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        outfile, count = unstack_mrcs(img_stack, [0], "lala")
        assert os.path.isfile("lala_0.mrc")
        assert not os.path.isfile("lala_1.mrc")
        assert outfile == "lala_%.mrc"
        assert count == 1

    def test_unstack_nondefault_name_strip_ext(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        outfile, count = unstack_mrcs(img_stack, [0], "lala.mrc")
        assert os.path.isfile("lala_0.mrc")
        assert not os.path.isfile("lala_1.mrc")
        assert not os.path.isfile("lala.mrc_0.mrc")
        assert outfile == "lala_%.mrc"
        assert count == 1

    def test_unstack_new_dir(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        outfile, count = unstack_mrcs(img_stack, range(100), "test_dir/us")
        for n in range(100):
            assert os.path.isfile(f"test_dir/us_{n:03d}.mrc")
        assert not os.path.isfile("test_dir/us_215.mrc")
        assert outfile == "test_dir/us_%%%.mrc"
        assert count == 100

    def test_unstack_error_too_high(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        imgs = [0, 3, 19, 34, 56, 68, 100, 115, 179, 215]
        with self.assertRaises(ValueError):
            unstack_mrcs(img_stack, imgs)

    def test_unstack_new_noname(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        outfile, count = unstack_mrcs(img_stack, range(100), "test_dir/")
        for n in range(100):
            assert os.path.isfile(f"test_dir/{n:03d}.mrc")
        assert not os.path.isfile("test_dir/215.mrc")
        assert outfile == "test_dir/%%%.mrc", outfile
        assert count == 100

    def test_stack_mrcs(self):
        single_img = os.path.join(self.test_data, "single.mrc")
        shutil.copy(single_img, os.path.join(self.test_dir, "1.mrc"))
        shutil.copy(single_img, os.path.join(self.test_dir, "2.mrc"))
        shutil.copy(single_img, os.path.join(self.test_dir, "3.mrc"))
        outfile, count = stack_mrcs(["1.mrc", "2.mrc", "3.mrc"])
        with mrcfile.open("stacked.mrcs") as result:
            assert result.data.shape == (3, 64, 64), result.data.shape
            assert str(result.voxel_size) == "(3.54, 3.54, 1.)"
        assert outfile == "stacked.mrcs"
        assert count == 3

    def test_stack_mrcs_mismatch_error(self):
        single_img = os.path.join(self.test_data, "single.mrc")
        big_img = os.path.join(self.test_data, "single_100.mrc")

        shutil.copy(single_img, os.path.join(self.test_dir, "1.mrc"))
        shutil.copy(single_img, os.path.join(self.test_dir, "2.mrc"))
        shutil.copy(big_img, os.path.join(self.test_dir, "3.mrc"))
        with self.assertRaises(ValueError):
            stack_mrcs(["1.mrc", "2.mrc", "3.mrc"])

    def test_substack_1st_100_imgs(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        outfile, count = substack_mrcs(img_stack, range(100))
        assert os.path.isfile("substack.mrcs")
        assert count == 100
        assert outfile == "substack.mrcs"
        with mrcfile.open("substack.mrcs") as mrc:
            assert mrc.data.shape == (100, 64, 64), mrc.data.shape

    def test_substack_10_random_imgs(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        imgs = [0, 3, 19, 34, 56, 68, 100, 115, 179, 214]
        outfile, count = substack_mrcs(img_stack, imgs, "rando.mrcs")
        assert os.path.isfile("rando.mrcs")
        assert count == 10
        assert outfile == "rando.mrcs"
        with mrcfile.open("rando.mrcs") as mrc:
            assert mrc.data.shape == (10, 64, 64), mrc.data.shape

    def test_substack_error_too_high(self):
        img_stack = os.path.join(self.test_data, "image_stack.mrcs")
        imgs = [0, 3, 19, 34, 56, 68, 100, 115, 179, 215]
        with self.assertRaises(ValueError):
            substack_mrcs(img_stack, imgs, "rando.mrcs")

    def test_get_tiff_dims(self):
        shutil.copy(os.path.join(self.test_data, "movie_tiff.tiff"), "tf.tiff")
        x, y, z = get_tiff_dims("tf.tiff")
        assert x == 78, x
        assert y == 78, y
        assert z == 24, z

    def test_get_tiff_dims_RGB(self):
        shutil.copy(
            os.path.join(self.test_data, "emd_3488.map_glow_zstd_thumb.tif"), "tf.tiff"
        )
        x, y, z = get_tiff_dims("tf.tiff")
        assert x == 32, x
        assert y == 32, y
        assert z == 1, z

    def test_get_tiff_stats_stack(self):
        shutil.copy(os.path.join(self.test_data, "movie_tiff.tiff"), "tf.tiff")
        stats = get_tiff_stats("tf.tiff")
        first_last = [
            [
                (78, 78),
                0,
                6,
                0.9681130834976989,
                0.9734994469818274,
                None,
                "16-bit unsigned integer: uint16",
            ],
            [
                (78, 78),
                0,
                6,
                0.9515121630506246,
                0.98431954089935,
                None,
                "16-bit unsigned integer: uint16",
            ],
        ]

        assert stats[0] == first_last[0]
        assert stats[-1] == first_last[1]
        assert len(stats) == 24

    def test_get_tiff_stats_RGB_image(self):
        shutil.copy(
            os.path.join(self.test_data, "emd_3488.map_glow_zstd_thumb.tif"), "tf.tiff"
        )
        stats = get_tiff_stats("tf.tiff")
        assert stats[0][0] == (32, 32)
        assert stats[0][1] == 0
        assert stats[0][2] == 244

    def test_read_tiff(self):
        shutil.copy(os.path.join(self.test_data, "movie_tiff.tiff"), "tf.tiff")
        tiffarray = read_tiff("tf.tiff")
        assert isinstance(tiffarray, np.ndarray)
        assert tiffarray.shape == (24, 78, 78)
        assert tiffarray.dtype == np.uint16

    def test_read_RGB_tiff(self):
        shutil.copy(
            os.path.join(self.test_data, "emd_3488.map_glow_zstd_thumb.tif"), "rgb.tiff"
        )
        tiffarray = read_tiff("rgb.tiff")
        assert isinstance(tiffarray, np.ndarray)
        # single RGB image
        assert tiffarray.shape == (1, 32, 32, 3)
        assert tiffarray.dtype == np.uint8

    def test_normalise_images(self):
        shutil.copy(
            os.path.join(self.test_data, "emd_3488.map_glow_zstd_thumb.tif"), "rgb.tiff"
        )
        tiffarray = read_tiff("rgb.tiff")
        assert isinstance(tiffarray, np.ndarray)
        # single normalised RGB image
        normed_imgs = normalise_images(tiffarray, 32, inpnormed=True)
        assert normed_imgs.shape == (1, 32, 32, 3)

    def test_threed_array_to_montage_RGB(self):
        shutil.copy(
            os.path.join(self.test_data, "emd_3488.map_glow_zstd_thumb.tif"), "rgb.tiff"
        )
        tiffarray = read_tiff("rgb.tiff")
        assert isinstance(tiffarray, np.ndarray)
        # single RGB image
        montage_array = threed_array_to_montage(tiffarray, 1, 32, inpnormed=True)
        assert montage_array.shape == (36, 36, 3)
        assert montage_array.dtype == np.uint8

    def test_threed_array_to_montage_base64(self):
        shutil.copy(
            os.path.join(self.test_data, "emd_3488.map_glow_zstd_thumb.tif"), "rgb.tiff"
        )
        tiffarray = read_tiff("rgb.tiff")
        assert isinstance(tiffarray, np.ndarray)
        # single base64 image
        image_list_file_path = threed_array_to_images(tiffarray, 1, 32, inpnormed=True)
        assert image_list_file_path == "imagelist.json"

    norm_image_to_base64_string

    def test_norm_image_to_base64_string(self):
        shutil.copy(
            os.path.join(self.test_data, "emd_3488.map_glow_zstd_thumb.tif"), "rgb.tiff"
        )
        tiffarray = read_tiff("rgb.tiff")
        assert isinstance(tiffarray, np.ndarray)
        # single normalised RGB image
        normed_imgs = normalise_images(tiffarray, 32, inpnormed=True)
        assert normed_imgs.shape == (1, 32, 32, 3)
        # single base64 image
        base64_string = norm_image_to_base64_string(normed_imgs[0])
        assert isinstance(base64_string, str)
        validation_base64_image_path = os.path.join(
            self.test_data, "emd_3488.map_glow_zstd_thumb.base64.txt"
        )
        with open(validation_base64_image_path, "r") as vi:
            expected = vi.read()

        assert base64_string == expected

    def test_remove_outliers(self):
        x, y = np.meshgrid(np.linspace(-1, 1, 5), np.linspace(-1, 1, 5))
        d = np.sqrt(x * x + y * y)
        mean = 0
        sig = 1
        g = np.exp(-((d - mean) ** 2 / (2 * sig**2)))
        gmin = np.amin(g)
        gmax = np.amax(g)
        assert np.sum(g - remove_outliers(g)) == 0.0
        g[4][0] = 2.0
        g[0][0] = -0.8
        assert remove_outliers(g)[4][0] == gmax
        assert remove_outliers(g)[0][0] == gmin

    def test_norm_array(self):
        x, y = np.meshgrid(np.linspace(-1, 1, 5), np.linspace(-1, 1, 5))
        d = np.sqrt(x * x + y * y)
        mean = 0
        sig = 1
        g = np.exp(-((d - mean) ** 2 / (2 * sig**2)))
        gmin = np.amin(g)
        gmax = np.amax(g)
        assert norm_array(g)[g == gmin][0] == 0
        assert norm_array(g)[g == gmax][0] == 255

    def test_mrcs_slices_montage(self):
        testfile = Path(self.test_data) / "emd_3488.mrc"
        for f in range(3):
            shutil.copy(testfile, f"f{f}.mrc")
        labels = mrcs_slices_montage(
            in_files={"f0.mrc": "file1", "f1.mrc": "", "f2.mrc": "numero dos"},
            outname="montage.jpg",
        )
        assert Path("montage.jpg").is_file()
        assert labels == [
            "numero dos: xy",
            "numero dos: xz",
            "numero dos: yz",
            "f1.mrc: xy",
            "f1.mrc: xz",
            "f1.mrc: yz",
            "file1: xy",
            "file1: xz",
            "file1: yz",
        ]

    def test_mrcs_slices_montage_base64(self):
        testfile = Path(self.test_data) / "emd_3488.mrc"
        for f in range(3):
            shutil.copy(testfile, f"f{f}.mrc")
        slicenames, base64_file = mrcs_slices_montage_base64(
            in_files={"f0.mrc": "file1", "f1.mrc": "", "f2.mrc": "numero dos"},
            outname="",
        )
        assert Path("imagelist.json").is_file()
        assert base64_file == "imagelist.json"
        assert slicenames == [
            "file1: xy",
            "file1: xz",
            "file1: yz",
            "f1.mrc: xy",
            "f1.mrc: xz",
            "f1.mrc: yz",
            "numero dos: xy",
            "numero dos: xz",
            "numero dos: yz",
        ]


if __name__ == "__main__":
    unittest.main()
