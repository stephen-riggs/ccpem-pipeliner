#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import unittest
import os
import tempfile
import shutil

from pipeliner.nodes import (
    Node,
    DensityMapNode,
    MicrographMovieGroupMetadataNode,
    ParticleGroupMetadataNode,
    MicrographGroupMetadataNode,
    Image2DGroupMetadataNode,
    AtomCoordsNode,
    NODE_DISPLAY_FILE,
    NODE_SUMMARIES_FILE,
    NODE_DISPLAY_DIR,
    check_file_is_mrc,
    check_file_is_cif,
    check_file_is_tif,
    check_file_is_text,
    GenNodeFormatConverter,
)
from pipeliner_tests import test_data
from pipeliner.results_display_objects import (
    ResultsDisplayTextFile,
    ResultsDisplayMontage,
    ResultsDisplayJson,
)
from pipeliner.starfile_handler import DataStarFile
from pipeliner.data_structure import NODE_LOGFILE
from pipeliner_tests.testing_tools import live_test


class NodesTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_node_with_empty_name_raises_error(self):
        with self.assertRaises(ValueError):
            Node("", "test.node")

    def test_instantiate_node_superclass(self):
        node = Node("my_file.txt", "MyNodeType", ["Moja", "Mbili"])
        assert node.__dict__ == {
            "format": "txt",
            "format_converter": None,
            "input_for_processes_list": [],
            "kwds": ["moja", "mbili"],
            "name": "my_file.txt",
            "output_from_process": None,
            "toplevel_description": "A general node type for files without a more "
            "specific Node class defined",
            "toplevel_type": "MyNodeType",
            "type": "MyNodeType.txt.moja.mbili",
        }

    def test_check_file_is_mrc_works(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        check = check_file_is_mrc(mrcfile)
        assert check
        nonmrcfile = os.path.join(self.test_data, "autopick.star")
        check = check_file_is_mrc(nonmrcfile)
        assert not check

    def test_check_file_is_text_works(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        check = check_file_is_text(mrcfile)
        assert not check
        nonmrcfile = os.path.join(self.test_data, "autopick.star")
        check = check_file_is_text(nonmrcfile)
        assert check

    def test_check_file_is_cif_works(self):
        ciffile = os.path.join(self.test_data, "StarFiles/20starfile_converted.star")
        check = check_file_is_cif(ciffile)
        assert check
        nonciffile = os.path.join(self.test_data, "emd_3488.mrc")
        check = check_file_is_cif(nonciffile)
        assert not check

    def test_is_tiff_works(self):
        ciffile = os.path.join(self.test_data, "StarFiles/20starfile_converted.star")
        check = check_file_is_tif(ciffile)
        assert not check
        tifffile = os.path.join(self.test_data, "movie_tiff.tiff")
        check = check_file_is_tif(tifffile)
        assert check

    def test_node_format_generaliser(self):
        gen = GenNodeFormatConverter(
            allowed_ext={("mrc", "mrcs"): "mrc"},
            check_funct={"mrc": check_file_is_mrc},
        )
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        ext = gen.gen_nodetype(mrcfile)
        assert ext == "mrc"

    def test_node_format_generaliser_file_not_present(self):
        gen = GenNodeFormatConverter(
            allowed_ext={("mrc", "mrcs"): "mrc"},
            check_funct={"mrc": check_file_is_mrc},
        )
        ext = gen.gen_nodetype("ghost_file.mrc")
        assert ext == "mrc"

    def test_node_format_generaliser_type_not_in_allowed(self):
        gen = GenNodeFormatConverter(
            allowed_ext={("mrc", "mrcs"): "mrc"},
            check_funct={"mrc": check_file_is_mrc},
        )
        ext = gen.gen_nodetype("ghost_file.star")
        assert ext == "star"

    def test_node_format_generaliser_filetype_wrong(self):
        mrcfile = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.star"))
        gen = GenNodeFormatConverter(
            allowed_ext={("star"): "star"},
            check_funct={"star": check_file_is_cif},
        )
        ext = gen.gen_nodetype("file.star")
        assert ext == "XstarX"

    def test_node_updates_format(self):
        node = Node(
            name="test_node.log",
            toplevel_type=NODE_LOGFILE,
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
        )

        assert node.format == "txt"
        assert node.type == f"{NODE_LOGFILE}.txt.test"

    def test_node_format_no_ext_no_override(self):
        node = Node(
            name="test_node",
            toplevel_type=NODE_LOGFILE,
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
        )

        assert node.format == "XX"
        assert node.type == f"{NODE_LOGFILE}.XX.test"

    def test_node_format_no_ext_with_override(self):
        node = Node(
            name="test_node",
            toplevel_type=NODE_LOGFILE,
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
            override_format="txt",
        )

        assert node.format == "txt"
        assert node.type == f"{NODE_LOGFILE}.txt.test"

    def test_node_format_no_ext_with_override_generalised(self):
        node = Node(
            name="test_node",
            toplevel_type=NODE_LOGFILE,
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
            override_format="log",
        )

        assert node.format == "txt"
        assert node.type == f"{NODE_LOGFILE}.txt.test"

    def test_node_format_with_override(self):
        node = Node(
            name="test_node.XXX",
            toplevel_type=NODE_LOGFILE,
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
            override_format="txt",
        )

        assert node.format == "txt"
        assert node.type == f"{NODE_LOGFILE}.txt.test"

    def test_node_format_with_override_and_generalise(self):
        node = Node(
            name="test_node.XXX",
            toplevel_type=NODE_LOGFILE,
            kwds=["test"],
            toplevel_description="testnode",
            format_converter=GenNodeFormatConverter(
                allowed_ext={("log",): "txt"}, check_funct={}
            ),
            override_format="log",
        )

        assert node.format == "txt"
        assert node.type == f"{NODE_LOGFILE}.txt.test"

    def test_making_default_display_non_text(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mrcfile, "emd_3488.mrc")
        node = Node(name="emd_3488.mrc", toplevel_type="SomeNode")
        rdo = node.default_results_display(output_dir="myoutdir/")
        expected = {
            "title": "emd_3488.mrc",
            "dobj_type": "pending",
            "start_collapsed": False,
            "message": "Nothing to display",
            "reason": "The node type for this file in myoutdir/ (SomeNode.mrc) "
            "has no default display method",
            "flag": "",
        }
        assert rdo.__dict__ == expected

    def test_making_default_display_text(self):
        txtfile = os.path.join(self.test_data, "logfile.log")
        shutil.copy(txtfile, "logfile.log")
        node = Node(name="logfile.log", toplevel_type="SomeNode")
        rdo = node.default_results_display(output_dir="myoutdir/")
        expected = {
            "dobj_type": "textfile",
            "file_path": "logfile.log",
            "flag": "",
            "start_collapsed": False,
            "title": "logfile.log",
        }
        assert rdo.__dict__ == expected

    def test_making_default_display_text_file_missing(self):
        node = Node(name="logfile.log", toplevel_type="SomeNode")
        rdo = node.default_results_display(output_dir="myoutdir/")
        expected = {
            "dobj_type": "pending",
            "flag": "",
            "message": "[Errno 2] No such file or directory: 'logfile.log'",
            "reason": "The file logfile.log was not found",
            "start_collapsed": False,
            "title": "logfile.log",
        }
        assert rdo.__dict__ == expected

    def test_making_default_display_json(self):
        txtfile = os.path.join(self.test_data, "5me2_a_residue_coordinates.json")
        shutil.copy(txtfile, "5me2_a_residue_coordinates.json")
        node = Node(name="5me2_a_residue_coordinates.json", toplevel_type="SomeNode")
        rdo = node.default_results_display(output_dir="myoutdir/")
        expected = {
            "dobj_type": "json",
            "file_path": "5me2_a_residue_coordinates.json",
            "flag": "",
            "start_collapsed": False,
            "title": "5me2_a_residue_coordinates.json",
        }
        assert rdo.__dict__ == expected

    def test_making_default_display_json_invalid_format(self):
        txtfile = os.path.join(self.test_data, "invalid_json_example.json")
        shutil.copy(txtfile, "invalid_json_example.json.json")
        node = Node(name="invalid_json_example.json.json", toplevel_type="SomeNode")
        rdo = node.default_results_display(output_dir="myoutdir/")
        expected = {
            "dobj_type": "textfile",
            "file_path": "invalid_json_example.json.json",
            "flag": "",
            "start_collapsed": False,
            "title": "invalid_json_example.json.json",
        }
        assert rdo.__dict__ == expected

    def test_making_default_display_json_file_missing(self):
        node = Node(name="5me2_a_residue_coordinates.json", toplevel_type="SomeNode")
        rdo = node.default_results_display(output_dir="myoutdir/")
        expected = {
            "dobj_type": "pending",
            "flag": "",
            "message": (
                "[Errno 2] No such file or directory:"
                " '5me2_a_residue_coordinates.json'"
            ),
            "reason": "The file 5me2_a_residue_coordinates.json was not found",
            "start_collapsed": False,
            "title": "5me2_a_residue_coordinates.json",
        }
        assert rdo.__dict__ == expected

    def test_creating_default_node_densitymap(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.mrc"))
        os.makedirs("outdir")
        node = DensityMapNode(name="file.mrc")
        rdo = node.default_results_display(output_dir="outdir")
        expected = {
            "associated_data": ["file.mrc"],
            "dobj_type": "montage",
            "flag": "",
            "img": "outdir/Thumbnails/slices_montage_000.png",
            "labels": ["file.mrc: xy", "file.mrc: xz", "file.mrc: yz"],
            "start_collapsed": False,
            "title": "Map slices: file.mrc",
            "xvalues": [0, 1, 2],
            "yvalues": [0, 0, 0],
        }
        assert type(rdo) is ResultsDisplayMontage
        assert rdo.__dict__ == expected

    def test_making_default_general_node_text_files(self):
        textfile = os.path.join(self.test_data, "particles.star")
        shutil.copy(textfile, os.path.join(self.test_dir, "file.star"))
        node = Node(name="file.star", toplevel_type="Generic")
        rdo = node.default_results_display(output_dir="outdir")
        expected = {
            "file_path": "file.star",
            "dobj_type": "textfile",
            "start_collapsed": False,
            "title": "file.star",
            "flag": "",
        }
        assert type(rdo) is ResultsDisplayTextFile
        assert rdo.__dict__ == expected

    def test_making_default_general_node_json_files(self):
        jsonfile = os.path.join(self.test_data, "5me2_a_residue_coordinates.json")
        shutil.copy(
            jsonfile, os.path.join(self.test_dir, "5me2_a_residue_coordinates.json")
        )
        node = Node(name="5me2_a_residue_coordinates.json", toplevel_type="Generic")
        rdo = node.default_results_display(output_dir="outdir")
        expected = {
            "file_path": "5me2_a_residue_coordinates.json",
            "dobj_type": "json",
            "start_collapsed": False,
            "title": "5me2_a_residue_coordinates.json",
            "flag": "",
        }
        assert type(rdo) is ResultsDisplayJson
        assert rdo.__dict__ == expected

    def test_making_default_general_node_invalid_json_files(self):
        jsonfile = os.path.join(self.test_data, "invalid_json_example.json")
        shutil.copy(jsonfile, os.path.join(self.test_dir, "invalid_json_example.json"))
        node = Node(name="invalid_json_example.json", toplevel_type="Generic")
        rdo = node.default_results_display(output_dir="outdir")
        expected = {
            "file_path": "invalid_json_example.json",
            "dobj_type": "textfile",
            "start_collapsed": False,
            "title": "invalid_json_example.json",
            "flag": "",
        }
        assert type(rdo) is ResultsDisplayTextFile
        assert rdo.__dict__ == expected

    def test_making_default_node_relion_kwd_moviesgroup(self):
        movies = os.path.join(self.test_data, "movies_mrcs.star")
        movfile = os.path.join(self.test_data, "single.mrc")
        shutil.copy(movies, "movies.star")
        os.makedirs("Movies")
        for n in range(1, 25):
            os.symlink(movfile, f"Movies/20170629_{n:05d}_frameImage.mrcs")
        movnode = MicrographMovieGroupMetadataNode(name="movies.star", kwds=["relion"])
        do = movnode.default_results_display(output_dir="")
        assert do.__dict__ == {
            "title": "movies.star; 2/24 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1],
            "yvalues": [0, 0],
            "labels": [
                "Movies/20170629_00001_frameImage.mrcs",
                "Movies/20170629_00002_frameImage.mrcs",
            ],
            "associated_data": ["movies.star"],
            "img": "Thumbnails/montage_f000.png",
        }
        assert os.path.isfile("Thumbnails/montage_f000.png")

    def test_making_default_node_relion_kwd_particlesgroup(self):
        partsfile = os.path.join(self.test_data, "particles.star")
        shutil.copy(partsfile, "particles.star")
        os.makedirs("Extract/job007/Movies")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Extract/job007/Movies/20170629_00021_frameImage.mrcs")
        parts_node = ParticleGroupMetadataNode(name="particles.star", kwds=["relion"])
        do = parts_node.default_results_display(output_dir="")
        with open(
            os.path.join(self.test_data, "ResultsFiles/nodes_particles.json")
        ) as f:
            expected = json.load(f)
        assert do.__dict__ == expected
        assert os.path.isfile("Thumbnails/montage_s000.png")

    def test_making_default_node_relion_kwd_micsgroup(self):
        mics = os.path.join(self.test_data, "micrographs_ctf.star")
        micfile = os.path.join(self.test_data, "single.mrc")
        shutil.copy(mics, "micrographs.star")
        os.makedirs("MotionCorr/job002/Movies")
        for n in range(21, 32):
            os.symlink(
                micfile, f"MotionCorr/job002/Movies/20170629_{n:05d}_frameImage.mrc"
            )
        movnode = MicrographGroupMetadataNode(name="micrographs.star", kwds=["relion"])
        do = movnode.default_results_display(output_dir="")
        with open(os.path.join(self.test_data, "ResultsFiles/nodes_mics.json")) as f:
            expected = json.load(f)
        assert do.__dict__ == expected

    def test_making_default_node_relion_kwd_classavs(self):
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        shutil.copy(clavsfile, "clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(
            name="clavs.star", kwds=["relion", "classaverages"]
        )
        do = clavs_node.default_results_display(output_dir="")
        assert do.__dict__ == {
            "title": "clavs.star; 5/5 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "000023@Class2D/job008/run_it025_classes.mrcs",
                "000039@Class2D/job008/run_it025_classes.mrcs",
                "000040@Class2D/job008/run_it025_classes.mrcs",
                "000043@Class2D/job008/run_it025_classes.mrcs",
                "000046@Class2D/job008/run_it025_classes.mrcs",
            ],
            "associated_data": ["clavs.star"],
            "img": "Thumbnails/montage_s000.png",
        }
        assert os.path.isfile("Thumbnails/montage_s000.png")

    def test_making_default_node_relion_kwd_classavs_wrong_type_gives_textfile(self):
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        shutil.copy(clavsfile, "clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(name="clavs.star")
        clavs_node.format = "xxx"
        do = clavs_node.default_results_display(output_dir="")
        assert do.__dict__ == {
            "file_path": "clavs.star",
            "flag": "",
            "start_collapsed": False,
            "title": "clavs.star",
            "dobj_type": "textfile",
        }

    def test_writing_display_file_no_nodes_starfile(self):
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job009")
        shutil.copy(clavsfile, "Select/job009/clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job009/clavs.star", kwds=["relion", "classaverages"]
        )
        clavs_node.write_default_result_file()
        assert os.path.isfile("node_display.star")
        assert os.path.isdir("Select/job009/NodeDisplay")
        exp = DataStarFile(
            os.path.join(self.test_data, "ResultsFiles/nodes_results_single.star")
        )
        expected = exp.loop_as_list(
            block="pipeliner_node_display",
            columns=["_pipelinerNodeName", "_pipelinerDisplayFile"],
            headers=True,
        )
        assert expected == [
            ["_pipelinerNodeName", "_pipelinerDisplayFile"],
            [
                "Select/job009/clavs.star",
                "Select/job009/NodeDisplay/results_display001_montage.json",
            ],
        ]

    def test_writing_display_file_nodes_starfile_exists(self):
        nd_file = os.path.join(self.test_data, "ResultsFiles/nodes_results_single.star")
        shutil.copy(nd_file, NODE_DISPLAY_FILE)
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job010")
        shutil.copy(clavsfile, "Select/job010/clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job010/clavs.star", kwds=["relion", "classaverages"]
        )
        clavs_node.write_default_result_file()
        assert os.path.isfile(NODE_DISPLAY_FILE)
        assert os.path.isdir("Select/job010/NodeDisplay")

        exp = DataStarFile(NODE_DISPLAY_FILE)
        expected = exp.loop_as_list(
            block="pipeliner_node_display",
            columns=["_pipelinerNodeName", "_pipelinerDisplayFile"],
            headers=True,
        )
        assert expected == [
            ["_pipelinerNodeName", "_pipelinerDisplayFile"],
            [
                "Select/job009/clavs.star",
                "Select/job009/NodeDisplay/results_display001_montage.json",
            ],
            [
                "Select/job010/clavs.star",
                "Select/job010/NodeDisplay/results_display000_montage.json",
            ],
        ]

    def test_writing_display_file_already_in_nodes_starfile(self):
        nd_file = os.path.join(self.test_data, "ResultsFiles/nodes_results_single.star")
        shutil.copy(nd_file, NODE_DISPLAY_FILE)
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job009")
        shutil.copy(clavsfile, "Select/job009/clavs.star")
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job009/clavs.star", kwds=["relion", "classaverages"]
        )
        clavs_node.write_default_result_file()
        assert os.path.isfile("node_display.star")
        assert os.path.isdir("Select/job009/NodeDisplay")
        exp = DataStarFile(NODE_DISPLAY_FILE)
        expected = exp.loop_as_list(
            block="pipeliner_node_display",
            columns=["_pipelinerNodeName", "_pipelinerDisplayFile"],
            headers=True,
        )
        assert expected == [
            ["_pipelinerNodeName", "_pipelinerDisplayFile"],
            [
                "Select/job009/clavs.star",
                "Select/job009/NodeDisplay/results_display000_montage.json",
            ],
        ]

    def test_get_node_display_object_already_in_starfile(self):
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")

        nd_file = os.path.join(self.test_data, "ResultsFiles/nodes_results_multi.star")
        shutil.copy(nd_file, NODE_DISPLAY_FILE)
        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job009")
        os.makedirs(f"Select/job009/{NODE_DISPLAY_DIR}")
        shutil.copy(clavsfile, "Select/job009/clavs.star")
        dispfile = os.path.join(self.test_data, "ResultsFiles/select_2dauto.json")
        shutil.copy(
            dispfile,
            f"Select/job009/{NODE_DISPLAY_DIR}/results_display000_montage.json",
        )
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job009/clavs.star", kwds=["relion", "classaverages"]
        )
        dispobj = clavs_node.get_result_display_object()
        with open(dispfile) as exp:
            expected = json.load(exp)
        assert dispobj.__dict__ == expected

    def test_get_node_display_object_not_in_starfile(self):
        """If not in the star file it should be created"""
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")

        nd_file = os.path.join(self.test_data, "ResultsFiles/nodes_results_single.star")
        shutil.copy(nd_file, NODE_DISPLAY_FILE)

        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job010")
        shutil.copy(clavsfile, "Select/job010/clavs.star")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job010/clavs.star", kwds=["relion", "classaverages"]
        )
        dispobj = clavs_node.get_result_display_object()
        assert dispobj.__dict__ == {
            "title": "Select/job010/clavs.star; 5/5 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "000023@Class2D/job008/run_it025_classes.mrcs",
                "000039@Class2D/job008/run_it025_classes.mrcs",
                "000040@Class2D/job008/run_it025_classes.mrcs",
                "000043@Class2D/job008/run_it025_classes.mrcs",
                "000046@Class2D/job008/run_it025_classes.mrcs",
            ],
            "associated_data": ["Select/job010/clavs.star"],
            "img": "Select/job010/NodeDisplay/Thumbnails/montage_s000.png",
        }

    def test_get_node_display_object_file_missing(self):
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")

        nd_file = os.path.join(
            self.test_data, "ResultsFiles/nodes_results_missing.star"
        )
        shutil.copy(nd_file, NODE_DISPLAY_FILE)

        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job010")
        shutil.copy(clavsfile, "Select/job010/clavs.star")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job010/clavs.star", kwds=["relion", "classaverages"]
        )
        dispobj = clavs_node.get_result_display_object()
        assert dispobj.dobj_type == "pending"
        assert "No such file" in dispobj.reason

    def test_get_node_display_object_starfile_doesnt_exist(self):
        os.makedirs("Class2D/job008")
        imgstack = os.path.join(self.test_data, "image_stack.mrcs")
        shutil.copy(imgstack, "Class2D/job008/run_it025_classes.mrcs")

        clavsfile = os.path.join(self.test_data, "class_averages.star")
        os.makedirs("Select/job010")
        shutil.copy(clavsfile, "Select/job010/clavs.star")
        clavs_node = Image2DGroupMetadataNode(
            name="Select/job010/clavs.star", kwds=["relion", "classaverages"]
        )
        dispobj = clavs_node.get_result_display_object()
        assert os.path.isfile(NODE_DISPLAY_FILE)
        assert os.path.isfile("Select/job010/NodeDisplay/Thumbnails/montage_s000.png")
        assert dispobj.__dict__ == {
            "title": "Select/job010/clavs.star; 5/5 images",
            "start_collapsed": False,
            "dobj_type": "montage",
            "flag": "",
            "xvalues": [0, 1, 2, 3, 4],
            "yvalues": [0, 0, 0, 0, 0],
            "labels": [
                "000023@Class2D/job008/run_it025_classes.mrcs",
                "000039@Class2D/job008/run_it025_classes.mrcs",
                "000040@Class2D/job008/run_it025_classes.mrcs",
                "000043@Class2D/job008/run_it025_classes.mrcs",
                "000046@Class2D/job008/run_it025_classes.mrcs",
            ],
            "associated_data": ["Select/job010/clavs.star"],
            "img": "Select/job010/NodeDisplay/Thumbnails/montage_s000.png",
        }

    def test_get_default_summary_densitymap(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        shutil.copy(mrcfile, os.path.join(self.test_dir, "file.mrc"))
        node = DensityMapNode(name="file.mrc")
        summary_string = node.make_summary_file()
        expected = (
            "nx              : 100\nny              : 100\nnz              : 100\n"
            "mode            : 2\n"
            "nxstart         : 0\nnystart         : 0\nnzstart         : 0\n"
            "mx              : 100\nmy              : 100\nmz              : 100\n"
            "cella           : (104.99999, 104.99999, 104.99999)\n"
            "cellb           : (90., 90., 90.)\n"
            "mapc            : 1\nmapr            : 2\nmaps            : 3\n"
            "dmin            : -0.5414281487464905\n"
            "dmax            : 0.7004381418228149\n"
            "dmean           : 0.0015229764394462109\n"
            "ispg            : 1\nnsymbt          : 0\n"
            "extra1          : b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00'\n"
            "exttyp          : b''\nnversion        : 0\n"
            "extra2          : b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00"
            "\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00"
            "\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00"
            "\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00"
            "\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00"
            "\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00"
            "\\x00\\x00\\x00\\x00'\n"
            "origin          : (0., 0., 0.)\nmap             : b'MAP '\n"
            "machst          : [68 65  0  0]\nrms             : 0.037149183452129364\n"
            "nlabl           : 1\n"
            "label           : [b'::::EMDATABANK.org::::EMD-3488::::                 "
            "                             '\n b'                                     "
            "                                           '\n b'                       "
            "                                                         '\n b'         "
            "                                                                       '\n"
            " b'                                                                     "
            "           '\n b'                                                       "
            "                         '\n b'                                         "
            "                                       '\n b'                           "
            "                                                     '\n b'             "
            "                                                                   '\n b' "
            "                                                                         "
            "      ']\n"
        )
        assert summary_string == expected

    @live_test(programs=["gemmi"])
    def test_create_default_summary_file_atomiccoord(self):
        modelfile = os.path.join(self.test_data, "5ni1_updated.cif")
        shutil.copy(modelfile, os.path.join(self.test_dir, "5ni1_updated.cif"))
        node = AtomCoordsNode(name="5ni1_updated.cif")
        summary_string = node.make_summary_file()
        expected = " Spacegroup   P 1\n   Not a crystal.\n   Atoms in:"
        assert summary_string[:48] == expected
        assert len(summary_string.split("\n")) == 19

    def test_writing_summary_file_node_star_file(self):
        mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        os.makedirs("PostProcess/job001")
        shutil.copy(mrcfile, "PostProcess/job001/emd_3488.mrc")
        node = DensityMapNode(name="PostProcess/job001/emd_3488.mrc")
        node.write_summary_file()
        assert os.path.isdir("PostProcess/job001/NodeDisplay")
        exp = DataStarFile(NODE_SUMMARIES_FILE)
        expected = exp.loop_as_list(
            block="pipeliner_node_summaries",
            columns=["_pipelinerNodeName", "_pipelinerSummaryFile"],
            headers=True,
        )
        assert expected == [
            ["_pipelinerNodeName", "_pipelinerSummaryFile"],
            [
                "PostProcess/job001/emd_3488.mrc",
                "PostProcess/job001/NodeDisplay/emd_3488_mrc_summary.txt",
            ],
        ]


if __name__ == "__main__":
    unittest.main()
