#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#
import json
import unittest
import os
import shutil
import tempfile
from pathlib import Path
from unittest.mock import patch
from pipeliner.process import Process
from pipeliner_tests import test_data
from pipeliner.nodes import Node
from pipeliner.data_structure import JOBINFO_FILE, JOBSTATUS_SUCCESS
from pipeliner.utils import update_jobinfo_file


class PipelinerProcessTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @staticmethod
    def make_job_info_file(comment=None):
        Path("Import/job001").mkdir(parents=True)
        jobinfo = {
            "job_directory": "Import/job001/",
            "comments": [] if comment is None else [comment],
            "created": "2024-05-16 12:00:00.000",
            "history": ["2024-05-16 12:00:00.000: Ran job"],
            "command_history": ["2024-05-16 12:00:00.000: command number one"],
        }
        with open(Path("Import/job001/") / JOBINFO_FILE, "w") as jif:
            json.dump(jobinfo, jif, indent=2)

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_existing_jobinfo_file_no_comments(self, dtt_mock):
        dtt_mock.return_value = "DATE_TIME_TAG2"
        self.make_job_info_file()
        proc = Process(
            name="Import/job001/",
            p_type="relion.import",
            status=JOBSTATUS_SUCCESS,
        )
        update_jobinfo_file(
            proc.name,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001/") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": [
                "2024-05-16 12:00:00.000: Ran job",
                "DATE_TIME_TAG2: New run",
            ],
            "command_history": [
                "2024-05-16 12:00:00.000: command number one",
                "DATE_TIME_TAG2: 1 2 3",
            ],
        }

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_existing_jobinfo_file_with_comments(self, dtt_mock):
        dtt_mock.return_value = "DATE_TIME_TAG2"
        self.make_job_info_file(comment="1st comment")
        proc = Process(
            name="Import/job001/",
            p_type="relion.import",
            status=JOBSTATUS_SUCCESS,
        )
        update_jobinfo_file(
            proc.name,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["1st comment", "New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": [
                "2024-05-16 12:00:00.000: Ran job",
                "DATE_TIME_TAG2: New run",
            ],
            "command_history": [
                "2024-05-16 12:00:00.000: command number one",
                "DATE_TIME_TAG2: 1 2 3",
            ],
        }

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_noexistant_jobinfo_file(self, dtt_mock):
        dtt_mock.return_value = "2024-05-16 12:00:00.000"
        Path("Import/job001").mkdir(parents=True)
        proc = Process(
            name="Import/job001/",
            p_type="relion.import",
            status=JOBSTATUS_SUCCESS,
        )
        update_jobinfo_file(
            proc.name,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": ["2024-05-16 12:00:00.000: New run"],
            "command_history": ["2024-05-16 12:00:00.000: 1 2 3"],
        }

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_jobinfo_file_error_on_read(self, dtt_mock):
        dtt_mock.return_value = "2024-05-16 12:00:00.000"
        indir = Path("Import/job001")
        indir.mkdir(parents=True)
        jof = indir / JOBINFO_FILE
        jof.touch()

        proc = Process(
            name="Import/job001/",
            p_type="relion.import",
            status=JOBSTATUS_SUCCESS,
        )
        update_jobinfo_file(
            proc.name,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": ["2024-05-16 12:00:00.000: New run"],
            "command_history": ["2024-05-16 12:00:00.000: 1 2 3"],
        }

    @patch("pipeliner.utils.date_time_tag")
    def test_updating_jobinfo_file_dtt_conflict(self, dtt_mock):
        dtt_mock.return_value = "2024-05-16 12:00:00.000"
        self.make_job_info_file()
        proc = Process(
            name="Import/job001/",
            p_type="relion.import",
            status=JOBSTATUS_SUCCESS,
        )
        update_jobinfo_file(
            proc.name,
            action="New run",
            command_list=["1", "2", "3"],
            comment="New comment",
        )
        with open(Path("Import/job001/") / JOBINFO_FILE) as jif:
            wrote = json.load(jif)
        assert wrote == {
            "job_directory": "Import/job001/",
            "comments": ["New comment"],
            "created": "2024-05-16 12:00:00.000",
            "history": [
                "2024-05-16 12:00:00.000: Ran job",
                "2024-05-16 12:00:00.000: New run",
            ],
            "command_history": [
                "2024-05-16 12:00:00.000: command number one",
                "2024-05-16 12:00:00.000: 1 2 3",
            ],
        }

    def test_remove_nonexistent_nodes(self):
        keep_nodes = [
            Node(name="Import/job001/file1.txt", toplevel_type="TestNode"),
            Node(name="Import/job002/file1.txt", toplevel_type="TestNode"),
            Node(name="Import/job003/file1.txt", toplevel_type="TestNode"),
        ]
        discard_nodes = [
            Node(name="Import/job004/file1.txt", toplevel_type="TestNode"),
            Node(name="Import/job005/file1.txt", toplevel_type="TestNode"),
            Node(name="Import/job006/file1.txt", toplevel_type="TestNode"),
        ]
        for node in keep_nodes:
            np = Path(node.name)
            Path(np.parent).mkdir(exist_ok=True, parents=True)
            np.touch()

        proc = Process(
            name="Import/job001/",
            p_type="relion.import",
            status=JOBSTATUS_SUCCESS,
        )
        proc.output_nodes = keep_nodes + discard_nodes

        proc.remove_nonexistent_nodes()
        assert proc.output_nodes == keep_nodes
