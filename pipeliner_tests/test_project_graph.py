#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import io
import unittest
import os
import shutil
import stat
import tempfile
from pathlib import Path
from unittest.mock import patch

from gemmi import cif

from pipeliner.node_factory import create_node
from pipeliner import star_writer, job_factory, job_manager
from pipeliner.project_graph import ProjectGraph
from pipeliner.utils import touch
from pipeliner.api.api_utils import write_default_jobstar
from pipeliner_tests import test_data
from pipeliner.data_structure import (
    NODES_DIR,
    SUCCESS_FILE,
    FAIL_FILE,
    ABORT_FILE,
    JOBSTATUS_SUCCESS,
    JOBSTATUS_ABORT,
    JOBSTATUS_RUN,
    JOBSTATUS_FAIL,
    CLASS2D_PARTICLE_NAME_EM,
    INIMODEL_JOB_NAME,
    CLASS3D_PARTICLE_NAME,
    REFINE3D_PARTICLE_NAME,
    EXTRACT_PARTICLE_NAME,
    CTFFIND_GCTF_NAME,
    STATUS2LABEL,
    CLEANUP_DIR,
    CLEANUP_LOG,
)
from pipeliner.process import Process
from pipeliner_tests.testing_tools import (
    live_test,
    expected_warning,
    compare_starfiles,
    clean_starfile,
)
from pipeliner_tests.testing_tools import DummyJob
from pipeliner.nodes import (
    Node,
    NODE_MICROGRAPHGROUPMETADATA,
    NODE_LOGFILE,
    NODE_OPTIMISERDATA,
    NODE_PARTICLEGROUPMETADATA,
    NODE_DENSITYMAP,
    NODE_MICROGRAPHMOVIEGROUPMETADATA,
)

UNKNOWN_STAR_NODE = "UnknownNodeType.star"


class ProjectGraphTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")
        self.test_starfile = os.path.join(self.test_data, "class2d_data.star")
        self.test_mrcfile = os.path.join(self.test_data, "emd_3488.mrc")
        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_opening_empty_pipeline_with_context_manager(self):
        src_file = os.path.join(self.test_data, "Pipelines/empty_pipeline.star")
        shutil.copy(src_file, "default_pipeline.star")
        with ProjectGraph() as pipeline:
            assert pipeline.name == "default"
            assert pipeline.star_file == Path("default_pipeline.star")
            assert pipeline.read_only is True
            assert pipeline._lock is None
            assert len(pipeline.node_list) == 0
            assert len(pipeline.process_list) == 0
            assert pipeline.job_counter == 1

    def test_opening_empty_pipeline_without_context_manager(self):
        src_file = os.path.join(self.test_data, "Pipelines/empty_pipeline.star")
        shutil.copy(src_file, "default_pipeline.star")
        pipeline = ProjectGraph()
        assert pipeline.name == "default"
        assert pipeline.star_file == Path("default_pipeline.star")
        assert pipeline.read_only is True
        assert pipeline._lock is None
        assert len(pipeline.node_list) == 0
        assert len(pipeline.process_list) == 0
        assert pipeline.job_counter == 1

    def test_opening_empty_pipeline_writable_with_context_manager(self):
        src_file = os.path.join(self.test_data, "Pipelines/empty_pipeline.star")
        shutil.copy(src_file, "default_pipeline.star")
        with ProjectGraph(read_only=False) as pipeline:
            assert pipeline.read_only is False
            assert pipeline._lock is not None
            assert Path(".relion_lock").is_dir()
        assert not Path(".relion_lock").exists()

    def test_opening_empty_pipeline_writable_without_context_manager(self):
        src_file = os.path.join(self.test_data, "Pipelines/empty_pipeline.star")
        shutil.copy(src_file, "default_pipeline.star")
        pipeline = ProjectGraph(read_only=False)
        assert pipeline.read_only is False
        assert pipeline._lock is not None
        assert Path(".relion_lock").is_dir()
        pipeline.close()
        assert pipeline._lock is None
        assert not Path(".relion_lock").exists()

    @staticmethod
    def test_making_new_empty_pipeline_with_context_manager():
        with ProjectGraph(create_new=True) as pipeline:
            assert pipeline.read_only is False
            assert pipeline._lock is not None
            assert Path(".relion_lock").is_dir()
            # Pipeline file should not be written until context manager exits
            assert not Path("default_pipeline.star").exists()
        assert not Path(".relion_lock").exists()
        assert Path("default_pipeline.star").is_file()

    @staticmethod
    def test_making_new_empty_pipeline_without_context_manager():
        pipeline = ProjectGraph(create_new=True)
        assert pipeline.read_only is False
        assert pipeline._lock is not None
        assert Path(".relion_lock").is_dir()
        assert not Path("default_pipeline.star").exists()
        pipeline._write()
        assert Path(".relion_lock").is_dir()
        assert Path("default_pipeline.star").is_file()
        pipeline.close()
        assert not Path(".relion_lock").exists()
        assert Path("default_pipeline.star").is_file()

    def test_making_new_pipeline_overwriting_existing_file_raises_exception(self):
        pipeline_file = Path("default_pipeline.star")
        pipeline_file.write_text("Old contents")
        with self.assertRaises(FileExistsError):
            ProjectGraph(create_new=True)
        assert not Path(".relion_lock").exists()
        assert pipeline_file.read_text() == "Old contents"

    @staticmethod
    def test_warning_issued_when_not_unlocked_properly():
        pipeline = ProjectGraph(create_new=True)
        with expected_warning(RuntimeWarning, "not unlocked properly"):
            del pipeline

    @staticmethod
    def test_no_warning_when_unlocked_properly():
        pipeline = ProjectGraph(create_new=True)
        pipeline.close()
        with expected_warning(nwarn=0):
            del pipeline

    @patch("os.mkdir", side_effect=PermissionError)
    def test_making_new_pipeline_without_dir_write_permission_raises_error(self, _):
        with self.assertRaises(PermissionError):
            ProjectGraph(create_new=True)
        assert not Path(".relion_lock").exists()

    @patch("os.access", return_value=False)
    def test_opening_pipeline_without_dir_write_permission_raises_error(self, _):
        pipeline_file = Path("default_pipeline.star")
        pipeline_file.touch()
        with self.assertRaises(PermissionError):
            ProjectGraph(read_only=False)
        assert not Path(".relion_lock").exists()

    @patch("gemmi.cif.read_file", side_effect=PermissionError)
    def test_opening_unreadable_pipeline_read_only_raises_runtime_exception(self, _):
        pipeline_file = Path("default_pipeline.star")
        pipeline_file.touch()
        with self.assertRaises(RuntimeError) as catcher:
            with ProjectGraph():
                pass
        assert isinstance(catcher.exception.__cause__, PermissionError)

    @patch("os.access", return_value=False)
    def test_opening_unreadable_pipeline_editable_raises_permission_error(self, _):
        # Very similar to the test above, but the exception is different
        # Kept as a separate test for now because of the different code paths involved
        pipeline_file = Path("default_pipeline.star")
        pipeline_file.touch()
        with self.assertRaises(PermissionError):
            with ProjectGraph(read_only=False):
                pass
        assert not Path(".relion_lock").exists()

    @patch("os.access", return_value=False)
    def test_opening_editable_pipeline_without_write_permission_raises_error(self, _):
        pipeline_file = Path("default_pipeline.star")
        pipeline_file.touch()
        with self.assertRaises(PermissionError):
            with ProjectGraph(read_only=False):
                pass
        assert not Path(".relion_lock").exists()

    def test_name_with_directory_part_raises_exception(self):
        """Opening a pipeline in a different directory should use ``dir`` instead."""
        with self.assertRaisesRegex(ValueError, "should not contain a directory part"):
            _ = ProjectGraph(name="subdir/default", create_new=True)

    @staticmethod
    def test_making_new_empty_pipeline_in_subdirectory():
        adir = Path("example_subdir")
        adir.mkdir()
        with ProjectGraph(pipeline_dir=adir, create_new=True) as pipeline:
            assert pipeline.read_only is False
            assert pipeline._lock is not None
            assert not Path(".relion_lock").exists()
            assert (adir / ".relion_lock").is_dir()
            # Pipeline file should not be written until context manager exits
            assert not Path("default_pipeline.star").exists()
            assert not (adir / "default_pipeline.star").exists()
        assert not Path(".relion_lock").exists()
        assert not (adir / ".relion_lock").is_dir()
        assert not Path("default_pipeline.star").exists()
        assert (adir / "default_pipeline.star").is_file()

    def test_cannot_write_to_name_attribute(self):
        with ProjectGraph(create_new=True) as pipeline:
            with self.assertRaises(AttributeError):
                pipeline.name = "new name"  # noqa

    def test_reading_old_style_tutorial_pipeline(self):
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )
        # Read pipeline STAR file
        # Need read_only=False because star file is in relion format=
        with ProjectGraph(name="tutorial", read_only=False) as pipeline:
            # Check pipeline was read correctly
            assert pipeline.job_counter == 30
            assert len(pipeline.process_list) == 28
            assert len(pipeline.node_list) == 62

    def test_reading_new_style_tutorial_pipeline(self):
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/converted_relion31_pipeline.star"),
            self.test_dir,
        )
        # Read pipeline STAR file
        with ProjectGraph(name="converted_relion31") as pipeline:
            # Check pipeline was read correctly
            assert pipeline.job_counter == 32
            assert len(pipeline.process_list) == 31
            assert len(pipeline.node_list) == 72

    def test_converting_old_style_to_new_style_pipeline(self):
        """Make sure the nodes edges and process lists have
        the right number of entries"""
        # Copy tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            self.test_dir,
        )

        # Read and update pipeline STAR file
        with ProjectGraph(name="tutorial", read_only=False) as pipeline:
            pipeline._write()

        # Read the pipeline STAR file again to check it can be read properly
        with ProjectGraph(name="tutorial") as pipeline:
            # Check pipeline was read correctly
            assert pipeline.job_counter == 30
            assert len(pipeline.process_list) == 28
            assert len(pipeline.node_list) == 62

    def test_star_writer_formatting_with_tutorial_pipeline_old_style(self):
        """Check the star writer writes out a converted pipeline
        in the old style - this is deprecated now because pipelines have
        a new format different from other starfiles in the dev-pipeliner version"""

        # read the pipeline and then immediately rewrite it
        pipeline_file = os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star")
        doc = cif.read_file(pipeline_file)
        out_stream = io.StringIO()
        star_writer.write_to_stream(doc, out_stream)
        actual = out_stream.getvalue()
        with open("wrote_pipeline.star", "w") as outfile:
            outfile.write(actual)
        compare_starfiles("wrote_pipeline.star", pipeline_file)

    def test_star_writer_formatting_with_tutorial_pipeline_new_style(self):
        """Check the star writer writes out a converted pipeline when
        reading a new style pipeline"""
        # read the pipeline and immediately rewrite it
        pipeline_file = os.path.join(
            self.test_data, "Pipelines/converted_relion31_pipeline.star"
        )
        doc = cif.read_file(pipeline_file)
        out_stream = io.StringIO()
        star_writer.write_to_stream(doc, out_stream)
        actual = out_stream.getvalue()

        # read the written files
        with open("wrote_pipeline.star", "w") as outfile:
            outfile.write(actual)
        compare_starfiles(pipeline_file, "wrote_pipeline.star")

    def test_opening_with_missing_file_raises_exception(self):
        # Ensure there is no pipeline file to begin with
        assert len(os.listdir()) == 0

        # Try to open a project with no pipeline file and no "create new" flag
        with self.assertRaises(FileNotFoundError):
            ProjectGraph()

        # Make sure we did not create any files
        assert len(os.listdir()) == 0

    def test_remake_pipeline_nodes(self):
        # Copy short tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        # make the nodes directory, make sure it's empty
        os.makedirs(".Nodes")
        assert len(os.listdir(".Nodes")) == 0

        # Open the pipeline STAR file
        with ProjectGraph(name="short", read_only=False) as pipeline:
            # remake the directory with nothing to write
            pipeline.remake_node_directory()
            assert len(os.listdir(".Nodes")) == 0

            # now make the files exist
            os.makedirs("Import/job001/")
            os.makedirs("MotionCorr/job002/")
            os.makedirs("CtfFind/job003/")
            pretend_files = [
                "MotionCorr/job002/logfile.pdf",
                "CtfFind/job003/logfile.pdf",
            ]
            for pfile in pretend_files:
                touch(pfile)
            # these files need to be actual starfiles for node type checking to function
            star_files = [
                "Import/job001/movies.star",
                "MotionCorr/job002/corrected_micrographs.star",
                "CtfFind/job003/micrographs_ctf.star",
            ]
            for sfile in star_files:
                shutil.copy(self.test_starfile, sfile)

            pfalias = [
                f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}/Import/movies/movies.star",
                f"{NODE_MICROGRAPHGROUPMETADATA}"
                "/MotionCorr/own/corrected_micrographs.star",
                f"{NODE_LOGFILE}/MotionCorr/own/logfile.pdf",
                f"{NODE_MICROGRAPHGROUPMETADATA}/CtfFind/gctf/micrographs_ctf.star",
                f"{NODE_LOGFILE}/CtfFind/gctf/logfile.pdf",
            ]

            # remake the node dirs again now that the files exist
            pipeline.remake_node_directory()
            for pfa in pfalias:
                thefile = ".Nodes/" + pfa
                assert os.path.isfile(thefile), thefile
                mode_str = stat.filemode(os.stat(thefile).st_mode)
                assert mode_str == "-rwxrwxrwx", mode_str

    def test_remake_pipeline_nodes_with_scheduled(self):
        # Copy short tutorial pipeline file to test directory
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_sched_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_sched_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        # make the nodes directory, make sure it's empty
        os.makedirs(".Nodes")
        assert len(os.listdir(".Nodes")) == 0

        # Open pipeline STAR file
        with ProjectGraph(name="short_sched", read_only=False) as pipeline:
            # remake the directory
            pipeline.remake_node_directory()

            pfalias = [
                f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}/Import/movies/movies.star",
                f"{NODE_MICROGRAPHGROUPMETADATA}"
                "/MotionCorr/own/corrected_micrographs.star",
                f"{NODE_LOGFILE}/MotionCorr/own/logfile.pdf",
                f"{NODE_MICROGRAPHGROUPMETADATA}/CtfFind/gctf/micrographs_ctf.star",
                f"{NODE_LOGFILE}/CtfFind/gctf/logfile.pdf",
            ]

            for pfa in pfalias:
                thefile = ".Nodes/" + pfa
                assert os.path.isfile(thefile), thefile

    def test_checking_pipeline_for_finished_jobs(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_running_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_running_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_running", read_only=False) as pipeline:
            pipeline.check_process_completion()
            pipeline._write()
            converted = pipeline.star_file.read_text()

            # make the files
            os.makedirs("Import/job001/")
            os.makedirs("MotionCorr/job002/")
            os.makedirs("CtfFind/job003/")
            for f in [
                "Import/job001/" + FAIL_FILE,
                "MotionCorr/job002/logfile.pdf",
                "MotionCorr/job002/" + SUCCESS_FILE,
                "CtfFind/job003/" + ABORT_FILE,
            ]:
                touch(f)
                assert os.path.isfile(f), f
            shutil.copy(
                self.test_starfile, "MotionCorr/job002/corrected_micrographs.star"
            )

            # update the pipeline
            pipeline.check_process_completion()
            pipeline._write()
            updated = pipeline.star_file.read_text()
            assert updated != converted
            uplines = updated.split("\n")
            assert uplines[19].split()[-1] == STATUS2LABEL[3], uplines[17]
            assert uplines[20].split()[-1] == STATUS2LABEL[2], uplines[18]
            assert uplines[21].split()[-1] == STATUS2LABEL[4], uplines[19]
            mics_nodesfile = os.listdir(
                f".Nodes/{NODE_MICROGRAPHGROUPMETADATA}/MotionCorr/own/"
            )
            assert mics_nodesfile == ["corrected_micrographs.star"]
            log_nodesfile = os.listdir(f".Nodes/{NODE_LOGFILE}/MotionCorr/own/")
            assert log_nodesfile == ["logfile.pdf"]

    def test_check_process_completion_with_new_output_node(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_running_pipeline.star"),
            self.test_dir,
        )

        # Ensure pipeline is up to date before running test
        with ProjectGraph(name="short_running", read_only=False) as pipeline:
            pipeline.check_process_completion()
            pipeline._write()
            converted = clean_starfile(pipeline.star_file)

        os.makedirs("Import/job001/")
        os.makedirs("MotionCorr/job002/")
        os.makedirs("CtfFind/job003/")

        # make the files
        for f in [
            "MotionCorr/job002/corrected_micrographs.star",
            "MotionCorr/job002/logfile.pdf",
            "MotionCorr/job002/new_node.txt",
            "MotionCorr/job002/" + SUCCESS_FILE,
        ]:
            touch(f)
            assert os.path.isfile(f), f

        # Set up the job's mini pipeline with an extra node
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/minipipe_mocorr_pipeline.star"),
            "MotionCorr/job002/job_pipeline.star",
        )
        with ProjectGraph(
            name="job", pipeline_dir="MotionCorr/job002", read_only=False
        ) as job_pipeline:
            process = job_pipeline.process_list[0]
            node = Node(
                name="MotionCorr/job002/new_node.txt",
                toplevel_type="NewNode",
                kwds=["kwd1", "kwd2"],
            )
            job_pipeline.add_new_output_edge(process, node)

        # Make sure the job pipeline file contains the new node
        job_pipeline = clean_starfile("MotionCorr/job002/job_pipeline.star")
        assert [
            "MotionCorr/job002/new_node.txt",
            "NewNode.txt.kwd1.kwd2",
        ] in job_pipeline

        # Update the main pipeline by calling check_process_completion()
        with ProjectGraph(name="short_running", read_only=False) as pipeline:
            # update the pipeline
            pipeline.check_process_completion()
            pipeline._write()
            updated = clean_starfile(pipeline.star_file)

        expected_new_lines = [
            ["MotionCorr/job002/new_node.txt", "NewNode.txt.kwd1.kwd2"],
            ["MotionCorr/job002/", "MotionCorr/job002/new_node.txt"],
        ]
        for line in expected_new_lines:
            assert line not in converted
            assert line in updated

        expected_nodefiles = [
            ".Nodes/MicrographGroupMetadata/MotionCorr/own/corrected_micrographs.star",
            ".Nodes/LogFile/MotionCorr/own/logfile.pdf",
            ".Nodes/NewNode/MotionCorr/own/new_node.txt",
        ]
        for file in expected_nodefiles:
            assert os.path.isfile(file)

    def test_removing_process_from_pipeline(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        os.makedirs("CtfFind/job003/")
        os.symlink(
            os.path.abspath("CtfFind/job003/"),
            os.path.join(self.test_dir, "CtfFind/gctf"),
            True,
        )
        touch("CtfFind/job003/logfile.pdf")
        assert os.path.isfile("CtfFind/job003/logfile.pdf")
        shutil.copy(self.test_starfile, "CtfFind/job003/micrographs_ctf.star")
        assert os.path.isfile("CtfFind/job003/micrographs_ctf.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short", read_only=False) as pipeline:
            pipeline._write()
            old_pipe = clean_starfile(pipeline.star_file)
            pipeline.delete_job(pipeline.process_list[2])
            pipeline._write()
            new_pipe = clean_starfile(pipeline.star_file)

        lines_to_remove = [
            [
                "CtfFind/job003/micrographs_ctf.star",
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.ctf",
            ],
            ["CtfFind/job003/logfile.pdf", f"{NODE_LOGFILE}.pdf.relion.ctffind"],
            ["CtfFind/job003/", "CtfFind/job003/micrographs_ctf.star"],
            ["CtfFind/job003/", "CtfFind/job003/logfile.pdf"],
            ["CtfFind/job003/", "CtfFind/gctf/", "relion.ctffind.gctf", "Succeeded"],
        ]
        for line in lines_to_remove:
            assert line in old_pipe
            assert line not in new_pipe

        assert not os.path.isfile("CtfFind/job003/logfile.pdf")
        assert not os.path.isfile("CtfFind/job003/micrographs_ctf.star")
        assert not os.path.isdir("CtfFind/job003/")

    def test_removing_multiple_processes_from_pipeline(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            self.test_dir,
        )

        assert os.path.isfile("short_pipeline.star")
        assert not os.path.isfile("default_pipeline.star")

        os.makedirs("CtfFind/job003/")
        os.makedirs("MotionCorr/job002/")
        os.symlink(
            os.path.abspath("MotionCorr/job002/"),
            os.path.join(self.test_dir, "MotionCorr/own"),
        )
        os.symlink(
            os.path.abspath("CtfFind/job003/"),
            os.path.join(self.test_dir, "CtfFind/gctf"),
        )
        files = [
            "CtfFind/job003/logfile.pdf",
            "MotionCorr/job002/logfile.pdf",
        ]

        for f in files:
            touch(f)
            assert os.path.isfile(f), f
        for starfile in [
            "CtfFind/job003/micrographs_ctf.star",
            "MotionCorr/job002/corrected_micrographs.star",
        ]:
            shutil.copy(self.test_starfile, starfile)

        # Open pipeline STAR file
        with ProjectGraph(name="short", read_only=False) as pipeline:
            pipeline._write()
            old_pipe = clean_starfile(pipeline.star_file)
            pipeline.delete_job(pipeline.process_list[1])
            pipeline._write()
            new_pipe = clean_starfile(pipeline.star_file)

        lines_to_remove = [
            [
                "CtfFind/job003/micrographs_ctf.star",
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.ctf",
            ],
            ["CtfFind/job003/logfile.pdf", f"{NODE_LOGFILE}.pdf.relion.ctffind"],
            ["CtfFind/job003/", "CtfFind/job003/micrographs_ctf.star"],
            ["CtfFind/job003/", "CtfFind/job003/logfile.pdf"],
            [
                "MotionCorr/job002/",
                "MotionCorr/own/",
                "relion.motioncorr.own",
                "Succeeded",
            ],
            ["CtfFind/job003/", "CtfFind/gctf/", "relion.ctffind.gctf", "Succeeded"],
            [
                "MotionCorr/job002/corrected_micrographs.star",
                f"{NODE_MICROGRAPHGROUPMETADATA}.star.relion.motioncorr",
            ],
            ["MotionCorr/job002/logfile.pdf", f"{NODE_LOGFILE}.pdf.relion.motioncorr"],
            ["MotionCorr/job002/corrected_micrographs.star", "CtfFind/job003/"],
            ["MotionCorr/job002/", "MotionCorr/job002/corrected_micrographs.star"],
            ["MotionCorr/job002/", "MotionCorr/job002/logfile.pdf"],
        ]
        for line in lines_to_remove:
            assert line in old_pipe
            assert line not in new_pipe

        assert not os.path.isfile("CtfFind/job003/logfile.pdf")
        assert not os.path.isfile("CtfFind/job003/micrographs_ctf.star")
        assert not os.path.isdir("CtfFind/job003/")

    def test_new_status_same_as_current_raises_exception(self):
        # copy in pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_running_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_running_pipeline.star")

        # make the directory structure
        os.makedirs("CtfFind/job003/")
        touch("CtfFind/job003/logfile.pdf")
        assert os.path.isfile("CtfFind/job003/logfile.pdf")
        shutil.copy(self.test_starfile, "CtfFind/job003/micrographs_ctf.star")
        assert os.path.isfile("CtfFind/job003/micrographs_ctf.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_running", read_only=False) as pipeline:
            # get process and try to mark it finished
            fin_proc = pipeline.process_list[2]
            # mark it finished
            with self.assertRaises(ValueError):
                pipeline.update_status(fin_proc, JOBSTATUS_RUN)

    def test_mark_as_finished_nonrefinejob(self):
        # copy in pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_running_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_running_pipeline.star")

        # make the directory structure
        os.makedirs("CtfFind/job003/")
        touch("CtfFind/job003/logfile.pdf")
        assert os.path.isfile("CtfFind/job003/logfile.pdf")
        shutil.copy(self.test_starfile, "CtfFind/job003/micrographs_ctf.star")
        assert os.path.isfile("CtfFind/job003/micrographs_ctf.star")

        jstar = os.path.join(
            self.test_data, "JobFiles/CtfFind/tutorial_job003_job.star"
        )
        shutil.copy(jstar, "CtfFind/job003/job.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_running", read_only=False) as pipeline:
            # get process to mark finished
            fin_proc = pipeline.process_list[2]

            # mark it finished
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        # check pipeline lines were written as expected
        written = clean_starfile("short_running_pipeline.star")
        assert [
            "CtfFind/job003/",
            "CtfFind/gctf/",
            CTFFIND_GCTF_NAME,
            JOBSTATUS_SUCCESS,
        ] in written
        assert [
            "CtfFind/job003/",
            "CtfFind/gctf/",
            CTFFIND_GCTF_NAME,
            JOBSTATUS_RUN,
        ] not in written
        assert os.path.isfile("CtfFind/job003/" + SUCCESS_FILE)

    def test_mark_as_finished_class2d(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        write_default_jobstar("relion.class2d.em", "Class2D/job008/job.star")
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                shutil.copy(self.test_starfile, ff)
                assert os.path.isfile(ff), ff

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[7]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_cl2d_pipeline.star")
        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/run_it005_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it005_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it005_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_data.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = ["Class2D/job008/", "Class2D/LoG_based/", "Class2D", "Running "]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + SUCCESS_FILE)

    def test_mark_as_finished_inimodel(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_inimod_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_inimod_pipeline.star")

        dirs = [
            "Extract/job007",
            "InitialModel/job009",
        ]
        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("InitialModel/job009"),
            os.path.join(self.test_dir, "InitialModel/symC1"),
        )
        assert os.path.islink("InitialModel/symC1")

        files = [
            ("_data.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_class001.mrc", self.test_mrcfile),
        ]
        for i in range(0, 103):
            for f in files:
                ff = "InitialModel/job009/run_it{0:03d}{1}".format(i, f[0])
                shutil.copy(f[1], ff)
                assert os.path.isfile(ff), ff
        shutil.copy(self.test_starfile, "Extract/job007/particles.star")

        write_default_jobstar("relion.initialmodel", "InitialModel/job009/job.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_inimod", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[8]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_inimod_pipeline.star")

        newlines = [
            [
                "InitialModel/job009/",
                "InitialModel/symC1/",
                INIMODEL_JOB_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "InitialModel/job009/run_it102_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.initialmodel",
            ],
            [
                "InitialModel/job009/run_it102_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.initialmodel",
            ],
            [
                "InitialModel/job009/run_it102_class001.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ],
            ["InitialModel/job009/", "InitialModel/job009/run_it102_optimiser.star"],
            ["InitialModel/job009/", "InitialModel/job009/run_it102_data.star"],
            ["InitialModel/job009/", "InitialModel/job009/run_it102_optimiser.star"],
            ["InitialModel/job009/", "InitialModel/job009/run_it102_class001.mrc"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "InitialModel/job009/",
            "InitialModel/symC1/",
            INIMODEL_JOB_NAME,
            JOBSTATUS_RUN,
        ]

        assert removed_line not in written
        assert os.path.isfile("InitialModel/job009/" + SUCCESS_FILE)

    def test_mark_as_finished_class3d(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl3d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl3d_pipeline.star")

        dirs = [
            "Class3D/job010",
            "InitialModel/job009",
            "Extract/job007",
        ]

        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("Class3D/job010"),
            os.path.join(self.test_dir, "Class3D/first_exhaustive"),
        )
        assert os.path.islink("Class3D/first_exhaustive")

        cl3_files = [
            ("_data.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_class001.mrc", self.test_mrcfile),
            ("_class002.mrc", self.test_mrcfile),
            ("_class003.mrc", self.test_mrcfile),
            ("_class004.mrc", self.test_mrcfile),
        ]
        for i in range(0, 13):
            for f in cl3_files:
                ff = "Class3D/job010/run_it{0:03d}{1}".format(i, f[0])
                shutil.copy(f[1], ff)
                assert os.path.isfile(ff), ff

        in_files = [
            "InitialModel/job009/run_it150_class001.star",
            "Extract/job007/particles.star",
        ]
        for f in in_files:
            shutil.copy(self.test_starfile, f)
            assert os.path.isfile(f)

        jstar = os.path.join(
            self.test_data, "JobFiles/Class3D/tutorial_job016_job.star"
        )
        shutil.copy(jstar, "Class3D/job010/job.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl3d", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[9]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_cl3d_pipeline.star")

        newlines = [
            [
                "Class3D/job010/",
                "Class3D/first_exhaustive/",
                CLASS3D_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class3D/job010/run_it012_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class3d",
            ],
            [
                "Class3D/job010/run_it012_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class3d",
            ],
            [
                "Class3D/job010/run_it012_class001.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            ],
            [
                "Class3D/job010/run_it012_class002.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            ],
            [
                "Class3D/job010/run_it012_class003.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            ],
            [
                "Class3D/job010/run_it012_class004.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            ],
            ["Class3D/job010/", "Class3D/job010/run_it012_optimiser.star"],
            ["Class3D/job010/", "Class3D/job010/run_it012_data.star"],
            ["Class3D/job010/", "Class3D/job010/run_it012_optimiser.star"],
            ["Class3D/job010/", "Class3D/job010/run_it012_class001.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it012_class002.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it012_class003.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it012_class004.mrc"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "Class3D/job010/",
            "Class3D/first_exhaustive/",
            CLASS3D_PARTICLE_NAME,
            JOBSTATUS_RUN,
        ]
        assert removed_line not in written, removed_line

    def test_mark_as_finished_ref3d(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_ref3d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_ref3d_pipeline.star")

        dirs = [
            "Refine3D/job011",
            "Class3D/job010",
            "Extract/job007",
            "InitialModel/job008",
        ]

        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("Refine3D/job011"),
            os.path.join(self.test_dir, "Refine3D/first3dref"),
        )
        os.symlink(
            os.path.abspath("Class3D/job010"),
            os.path.join(self.test_dir, "Class3D/first_exhaustive"),
        )

        assert os.path.islink("Refine3D/first3dref")

        in_files = [
            ("InitialModel/job008/run_it150_class001.star", self.test_starfile),
            ("Extract/job007/particles.star", self.test_starfile),
            ("Class3D/job010/run_it025_class001.mrc", self.test_mrcfile),
            ("Class3D/job010/run_it025_class002.mrc", self.test_mrcfile),
            ("Class3D/job010/run_it025_class003.mrc", self.test_mrcfile),
            ("Class3D/job010/run_it025_class004.mrc", self.test_mrcfile),
        ]
        for f in in_files:
            shutil.copy(f[1], f[0])
            assert os.path.isfile(f[0]), f[0]

        r3d_files = [
            ("_data.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_half1_class001.mrc", self.test_mrcfile),
            ("_half2_class001.mrc", self.test_mrcfile),
        ]
        for i in range(0, 18):
            for f in r3d_files:
                ff = "Refine3D/job011/run_it{0:03d}{1}".format(i, f[0])
                shutil.copy(f[1], ff)
                assert os.path.isfile(ff), ff
        jstar = os.path.join(
            self.test_data, "JobFiles/Refine3D/tutorial_job025_job.star"
        )
        shutil.copy(jstar, "Refine3D/job011/job.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_ref3d", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[10]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_ref3d_pipeline.star")

        newlines = [
            [
                "Refine3D/job011/",
                "Refine3D/first3dref/",
                REFINE3D_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Refine3D/job011/run_it017_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
            ],
            [
                "Refine3D/job011/run_it017_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
            ],
            [
                "Refine3D/job011/run_it017_half1_class001.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.halfmap",
            ],
            ["Extract/job007/particles.star", "Refine3D/job011/"],
            ["InitialModel/job009/run_it150_class001.mrc", "Refine3D/job011/"],
            ["Refine3D/job011/", "Refine3D/job011/run_it017_optimiser.star"],
            ["Refine3D/job011/", "Refine3D/job011/run_it017_data.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = ["Refine3D/job011/ Refine3D/first3dref/", "Refine3D", "Running"]
        assert removed_line not in written
        assert os.path.isfile("Refine3D/job011/" + SUCCESS_FILE)

    def test_mark_as_finished_class2d_continue(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        write_default_jobstar("relion.class2d.em", "Class2D/job008/job.star")
        assert os.path.islink("Class2D/LoG_based")

        files = [
            ("_data.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
        ]
        for i in range(0, 14):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f[0])
                shutil.copy(f[1], ff)
                assert os.path.isfile(ff), ff

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[7]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_cl2d_pipeline.star")

        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/run_it013_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it013_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it013_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            ["Class2D/job008/", "Class2D/job008/run_it013_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it013_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it013_data.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_RUN,
        ]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + SUCCESS_FILE)

    def test_mark_as_finished_inimodel_continue(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_inimod_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_inimod_pipeline.star")

        dirs = [
            "Extract/job007",
            "InitialModel/job009",
        ]
        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("InitialModel/job009"),
            os.path.join(self.test_dir, "InitialModel/symC1"),
        )
        write_default_jobstar("relion.initialmodel", "InitialModel/job009/job.star")
        assert os.path.islink("InitialModel/symC1")

        files = [
            ("_data.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_class001.mrc", self.test_mrcfile),
        ]
        for i in range(0, 144):
            for f in files:
                ff = "InitialModel/job009/run_it{0:03d}{1}".format(i, f[0])
                shutil.copy(f[1], ff)
                assert os.path.isfile(ff), ff

        shutil.copy(self.test_starfile, "Extract/job007/particles.star")

        write_default_jobstar("relion.initialmodel", "InitialModel/job009/job.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_inimod", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[8]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_inimod_pipeline.star")

        newlines = [
            [
                "InitialModel/job009/",
                "InitialModel/symC1/",
                INIMODEL_JOB_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "InitialModel/job009/run_it143_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.initialmodel",
            ],
            [
                "InitialModel/job009/run_it143_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.initialmodel",
            ],
            [
                "InitialModel/job009/run_it143_class001.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.initialmodel",
            ],
            [
                "InitialModel/job009/",
                "InitialModel/job009/run_it143_optimiser.star",
            ],
            [
                "InitialModel/job009/",
                "InitialModel/job009/run_it143_optimiser.star",
            ],
            [
                "InitialModel/job009/",
                "InitialModel/job009/run_it143_class001.mrc",
            ],
            ["InitialModel/job009/", "InitialModel/job009/run_it143_data.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "InitialModel/job009/",
            "InitialModel/symC1/",
            INIMODEL_JOB_NAME,
            JOBSTATUS_RUN,
        ]

        assert removed_line not in written
        assert os.path.isfile("InitialModel/job009/" + SUCCESS_FILE)

    def test_mark_as_finished_class3d_continue(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl3d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl3d_pipeline.star")

        dirs = [
            "Class3D/job010",
            "InitialModel/job009",
            "Extract/job007",
        ]

        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("Class3D/job010"),
            os.path.join(self.test_dir, "Class3D/first_exhaustive"),
        )
        assert os.path.islink("Class3D/first_exhaustive")

        cl3_files = [
            ("_data.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_class001.mrc", self.test_mrcfile),
            ("_class002.mrc", self.test_mrcfile),
            ("_class003.mrc", self.test_mrcfile),
            ("_class004.mrc", self.test_mrcfile),
        ]
        for i in range(0, 20):
            for f in cl3_files:
                ff = "Class3D/job010/run_it{0:03d}{1}".format(i, f[0])
                shutil.copy(f[1], ff)
                assert os.path.isfile(ff), ff
        in_files = [
            "InitialModel/job009/run_it150_class001.star",
            "Extract/job007/particles.star",
        ]
        for f in in_files:
            shutil.copy(self.test_mrcfile, f)
            assert os.path.isfile(f)

        jstar = os.path.join(
            self.test_data, "JobFiles/Class3D/tutorial_job016_job.star"
        )
        shutil.copy(jstar, "Class3D/job010/job.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl3d", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[9]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_cl3d_pipeline.star")

        newlines = [
            [
                "Class3D/job010/",
                "Class3D/first_exhaustive/",
                CLASS3D_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class3D/job010/run_it019_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class3d",
            ],
            [
                "Class3D/job010/run_it019_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class3d",
            ],
            [
                "Class3D/job010/run_it019_class001.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            ],
            [
                "Class3D/job010/run_it019_class002.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            ],
            [
                "Class3D/job010/run_it019_class003.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            ],
            [
                "Class3D/job010/run_it019_class004.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.class3d",
            ],
            ["Class3D/job010/", "Class3D/job010/run_it019_optimiser.star"],
            ["Class3D/job010/", "Class3D/job010/run_it019_optimiser.star"],
            ["Class3D/job010/", "Class3D/job010/run_it019_class001.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it019_class002.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it019_class003.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it019_class004.mrc"],
            ["Class3D/job010/", "Class3D/job010/run_it019_data.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "Class3D/job010/",
            "Class3D/first_exhaustive/",
            "relion.class3d",
            "Running",
        ]
        assert removed_line not in written
        assert os.path.isfile("Class3D/job010/" + SUCCESS_FILE)

    def test_mark_as_finished_ref3d_continue(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_ref3d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_ref3d_pipeline.star")

        dirs = [
            "Refine3D/job011",
            "Class3D/job010",
            "Extract/job007",
            "InitialModel/job008",
        ]

        for fdir in dirs:
            os.makedirs(fdir)
            assert os.path.isdir(fdir)

        os.symlink(
            os.path.abspath("Refine3D/job011"),
            os.path.join(self.test_dir, "Refine3D/first3dref"),
        )
        os.symlink(
            os.path.abspath("Class3D/job010"),
            os.path.join(self.test_dir, "Class3D/first_exhaustive"),
        )

        assert os.path.islink("Refine3D/first3dref")

        in_files = [
            ("InitialModel/job008/run_it150_class001.star", self.test_starfile),
            ("Extract/job007/particles.star", self.test_starfile),
            ("Class3D/job010/run_it025_class001.mrc", self.test_mrcfile),
            ("Class3D/job010/run_it025_class002.mrc", self.test_mrcfile),
            ("Class3D/job010/run_it025_class003.mrc", self.test_mrcfile),
            ("Class3D/job010/run_it025_class004.mrc", self.test_mrcfile),
        ]
        for f in in_files:
            shutil.copy(f[1], f[0])
            assert os.path.isfile(f[0]), f[0]

        r3d_files = [
            ("_data.star", self.test_starfile),
            ("_optimiser.star", self.test_starfile),
            ("_half1_class001.mrc", self.test_mrcfile),
            ("_half2_class001.mrc", self.test_mrcfile),
        ]
        for i in range(0, 18):
            for f in r3d_files:
                ff = "Refine3D/job011/run_it{0:03d}{1}".format(i, f[0])
                shutil.copy(f[1], ff)
                assert os.path.isfile(ff), ff
        jstar = os.path.join(
            self.test_data, "JobFiles/Refine3D/tutorial_job025_job.star"
        )
        shutil.copy(jstar, "Refine3D/job011/job.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_ref3d", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[10]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_ref3d_pipeline.star")

        newlines = [
            [
                "Refine3D/job011/",
                "Refine3D/first3dref/",
                REFINE3D_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Refine3D/job011/run_it017_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.refine3d",
            ],
            [
                "Refine3D/job011/run_it017_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.refine3d",
            ],
            [
                "Refine3D/job011/run_it017_half1_class001.mrc",
                f"{NODE_DENSITYMAP}.mrc.relion.halfmap",
            ],
            ["Extract/job007/particles.star", "Refine3D/job011/"],
            ["InitialModel/job009/run_it150_class001.mrc", "Refine3D/job011/"],
            ["Refine3D/job011/", "Refine3D/job011/run_it017_optimiser.star"],
            ["Refine3D/job011/", "Refine3D/job011/run_it017_data.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "Refine3D/job011/",
            "Refine3D/first3dref/",
            REFINE3D_PARTICLE_NAME,
            JOBSTATUS_RUN,
        ]
        assert removed_line not in written
        assert os.path.isfile("Refine3D/job011/" + SUCCESS_FILE)

    def test_mark_as_finished_class2d_aborted(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_abort_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_abort_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        write_default_jobstar("relion.class2d.em", "Class2D/job008/job.star")
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                shutil.copy(self.test_starfile, ff)
                assert os.path.isfile(ff), ff

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_abort", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[7]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_cl2d_abort_pipeline.star")

        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/run_it005_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it005_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            ],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_data.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
        ]
        for line in newlines:
            assert line in written, (line, written)

        removed_line = ["Class2D/job008/", "Class2D/LoG_based/", "Class2D", "Aborted"]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + SUCCESS_FILE)

    def test_mark_class2d_failed(self):
        """Take a job that is currently failed and mark it as finished"""
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_fail_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_fail_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                shutil.copy(self.test_starfile, ff)
                assert os.path.isfile(ff), ff

        write_default_jobstar("relion.class2d.em", "Class2D/job008/job.star")

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_fail", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[7]
            pipeline.update_status(fin_proc, JOBSTATUS_SUCCESS)

        written = clean_starfile("short_cl2d_fail_pipeline.star")

        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/run_it005_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it005_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            ],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_data.star"],
            ["Class2D/job008/", "Class2D/job008/run_it005_optimiser.star"],
        ]
        for line in newlines:
            assert line in written, line

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_FAIL,
        ]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + SUCCESS_FILE)

    def test_class2d_mark_aborted_as_failed(self):
        """Take an aborted job and mark it as failed"""
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_abort_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_abort_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                shutil.copy(self.test_starfile, ff)
                assert os.path.isfile(ff), ff

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_abort", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[7]
            pipeline.update_status(fin_proc, JOBSTATUS_FAIL)

        written = clean_starfile("short_cl2d_abort_pipeline.star")

        newlines = [
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_FAIL,
            ],
        ]
        for line in newlines:
            assert line in written, (line, written)

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_ABORT,
        ]
        assert removed_line not in written
        assert os.path.isfile("Class2D/job008/" + FAIL_FILE)
        assert not os.path.isfile("Class2D/job008/" + ABORT_FILE)

    def test_mark_as_aborted_class2d_running(self):
        """Take a job marked as running and mark it aborted"""
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_pipeline.star")

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        os.symlink(
            os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
        )
        assert os.path.islink("Class2D/LoG_based")

        files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        for i in range(0, 6):
            for f in files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                shutil.copy(self.test_starfile, ff)
                assert os.path.isfile(ff), ff

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d", read_only=False) as pipeline:
            fin_proc = pipeline.process_list[7]
            with self.assertRaises(ValueError):
                pipeline.update_status(fin_proc, JOBSTATUS_ABORT)

    def test_delete_job(self):
        # opy over files
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_del_pipeline.star"),
            self.test_dir,
        )
        assert os.path.isfile("short_cl2d_del_pipeline.star")

        # make the files and their symlinks for aliases
        fdirs = {
            "Class2D/job008": "Class2D/LoG_based",
            "Extract/job007": "Extract/LoG_based",
        }
        for fdir in fdirs:
            os.makedirs(fdir)
            os.symlink(
                os.path.abspath(fdir), os.path.join(self.test_dir, fdirs[fdir]), True
            )
            assert os.path.isdir(fdir)
            assert os.path.islink(fdirs[fdir])

        cl2d_files = [
            "_data.star",
            "_optimiser.star",
            "_optimiser.star",
        ]
        file_list = list()
        for i in range(0, 26):
            for f in cl2d_files:
                ff = "Class2D/job008/run_it{0:03d}{1}".format(i, f)
                shutil.copy(self.test_starfile, ff)
                assert os.path.isfile(ff), ff
                file_list.append(ff)

        cl2d_other_files = [
            "default_pipeline.star",
            "job_pipeline.star",
            "run.job",
            SUCCESS_FILE,
            "job.star",
            "note.txt",
            "run.err",
            "run.out",
        ]

        for f in cl2d_other_files:
            ff = "Class2D/job008/" + f
            touch(ff)
            assert os.path.isfile(ff)
            file_list.append(ff)

        extract_files = [
            "default_pipeline.star",
            "job_pipeline.star",
            "run.job",
            SUCCESS_FILE,
            "job.star",
            "note.txt",
            "run.err",
            "run.out",
        ]
        for f in extract_files:
            ff = "Extract/job007/" + f
            touch(ff)
            assert os.path.isfile(ff)
            file_list.append(ff)
        shutil.copy(self.test_starfile, "Extract/job007/particles.star")

        moviedir = "Extract/job007/Movies"
        os.makedirs(moviedir)
        assert os.path.isdir("Extract/job007/Movies")

        for i in range(1, 11):
            f = moviedir + "/movie_parts{:03d}.mrcs".format(i)
            shutil.copy(self.test_mrcfile, f)
            assert os.path.isfile(f)
            file_list.append(f)

        # make to .Nodes files
        nodes_files = [
            ".Nodes/ParticleGroupMetadata/Extract/job007/particles.star",
            ".Nodes/ParticleGroupMetadata/Class2D/job008/run_it025_data.star",
        ]
        for f in nodes_files:
            os.makedirs(os.path.dirname(f), exist_ok=True)
            shutil.copy(self.test_starfile, f)

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_del", read_only=False) as pipeline:
            del_proc = pipeline.process_list[6]
            pipeline.delete_job(del_proc)

        # makesure the lines have been removed from the pipeline
        written = clean_starfile("short_cl2d_del_pipeline.star")

        removed_lines = [
            [
                "Extract/job007/ Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/ Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Extract/job007/particles.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            ],
            [
                "Class2D/job008/run_it025_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            ],
            ["Extract/job007/particles.star", "Class2D/job008/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_data.star"],
        ]

        for removed_line in removed_lines:
            assert removed_line not in written, removed_line

        # make sure the files are gone
        for f in file_list:
            assert not os.path.isfile(f), f
            trashname = "Trash/" + f
            assert os.path.isfile(trashname), trashname

        # make sure the .Nodes entries are gone
        for f in nodes_files:
            assert not os.path.isfile(f), f
            dirname = os.path.dirname(f)
            assert not os.path.isdir(dirname), dirname

    def make_alias_file_structure(self, pipe_name, alias):
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/{}_pipeline.star".format(pipe_name)
            ),
            self.test_dir,
        )

        assert os.path.isfile("{}_pipeline.star".format(pipe_name))

        fdir = "Class2D/job008"
        os.makedirs(fdir)
        if alias is not None:
            os.symlink(
                os.path.abspath(fdir), os.path.join(self.test_dir, "Class2D/LoG_based")
            )
            assert os.path.islink("Class2D/LoG_based")

        real_files = [
            "Class2D/job008/run_it025_optimiser.star",
            "Class2D/job008/run_it025_data.star",
        ]
        for f in real_files:
            shutil.copy(self.test_starfile, f)

        outnodes = [
            f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            f"{NODE_OPTIMISERDATA}.star.relion.class2d",
        ]
        if alias is None:
            alias = "job008"
        for node in outnodes:
            onode = f"{NODES_DIR}{node.split('.')[0]}/Class2D/{alias}"
            os.makedirs(onode)
            assert os.path.isdir(onode)

        files = [
            f"{NODES_DIR}{NODE_OPTIMISERDATA}/Class2D/{alias}/run_it025_optimiser.star",
            f"{NODES_DIR}{NODE_PARTICLEGROUPMETADATA}/Class2D/{alias}/"
            "run_it025_data.star",
        ]
        for f in files:
            shutil.copy(self.test_starfile, f)
            assert os.path.isfile(f), f

    def test_change_job_alias(self):
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_finished", read_only=False) as pipeline:
            change_proc = pipeline.process_list[7]
            pipeline.set_job_alias(change_proc, "pretty_new_one")

        assert os.path.islink(os.path.join(self.test_dir, "Class2D/pretty_new_one"))
        assert not os.path.islink(os.path.join(self.test_dir, "Class2D/LoG_based"))
        oldnodes = [
            f"{NODE_PARTICLEGROUPMETADATA}/Class2D/LoG_based/run_it025_data.star",
            f"{NODE_OPTIMISERDATA}/Class2D/LoG_based/run_it025_optimiser.star",
        ]

        new_nodes = [
            f"{NODE_PARTICLEGROUPMETADATA}/Class2D/pretty_new_one/run_it025_data.star",
            f"{NODE_OPTIMISERDATA}/Class2D/pretty_new_one/run_it025_optimiser.star",
        ]

        for n in oldnodes:
            assert not os.path.isfile(os.path.join(NODES_DIR, n)), n
        for n in new_nodes:
            assert os.path.isfile(os.path.join(NODES_DIR, n)), n

        pipe_data = clean_starfile("short_cl2d_finished_pipeline.star")

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]
        new_line = [
            "Class2D/job008/",
            "Class2D/pretty_new_one/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]

        assert removed_line not in pipe_data, removed_line
        assert new_line in pipe_data, new_line
        # check the job's job_pipeline.star was updated
        jobpipe = clean_starfile("Class2D/job008/job_pipeline.star")
        assert new_line in jobpipe

    def test_change_job_alias_to_none(self):
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_finished", read_only=False) as pipeline:
            change_proc = pipeline.process_list[7]
            pipeline.set_job_alias(change_proc, "")

        assert not os.path.islink(os.path.join(self.test_dir, "Class2D/LoG_based"))
        oldnodes = [
            f"{NODE_PARTICLEGROUPMETADATA}/Class2D/LoG_based/run_it025_data.star",
            f"{NODE_OPTIMISERDATA}/Class2D/LoG_based/run_it025_optimiser.star",
        ]

        new_nodes = [
            f"{NODE_PARTICLEGROUPMETADATA}/Class2D/job008/run_it025_data.star",
            f"{NODE_OPTIMISERDATA}/Class2D/job008/run_it025_optimiser.star",
        ]

        for n in oldnodes:
            assert not os.path.isfile(os.path.join(NODES_DIR, n)), n
        for n in new_nodes:
            assert os.path.isfile(os.path.join(NODES_DIR, n)), n

        pipe_data = clean_starfile("short_cl2d_finished_pipeline.star")

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]
        new_line = [
            "Class2D/job008/",
            "None",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]

        assert removed_line not in pipe_data, removed_line
        assert new_line in pipe_data, new_line
        jobpipe = clean_starfile("Class2D/job008/job_pipeline.star")
        assert new_line in jobpipe

    def test_change_job_alias_from_none(self):
        self.make_alias_file_structure("short_cl2d_noalias", None)

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_noalias", read_only=False) as pipeline:
            change_proc = pipeline.process_list[7]
            pipeline.set_job_alias(change_proc, "from_none")

        new_nodes = [
            f"{NODE_PARTICLEGROUPMETADATA}/Class2D/from_none/run_it025_data.star",
            f"{NODE_OPTIMISERDATA}/Class2D/from_none/run_it025_optimiser.star",
        ]

        old_nodes = [
            f"{NODE_PARTICLEGROUPMETADATA}/Class2D/job008/run_it025_data.star",
            f"{NODE_OPTIMISERDATA}/Class2D/job008/run_it025_optimiser.star",
        ]

        for n in old_nodes:
            assert not os.path.isfile(os.path.join(NODES_DIR, n)), n
        for n in new_nodes:
            assert os.path.isfile(os.path.join(NODES_DIR, n)), n

        pipe_data = clean_starfile("short_cl2d_noalias_pipeline.star")

        new_line = [
            "Class2D/job008/",
            "Class2D/from_none/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]
        removed_line = [
            "Class2D/job008/",
            "None",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]

        assert removed_line not in pipe_data, removed_line
        assert new_line in pipe_data, new_line
        jobpipe = clean_starfile("Class2D/job008/job_pipeline.star")
        assert new_line in jobpipe

    def test_job_alias_illegal_names(self):
        self.make_alias_file_structure("short_cl2d_noalias", None)

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_noalias", read_only=False) as pipeline:
            change_proc = pipeline.process_list[7]
            badnames = [
                "l",
                "JAH!!",
                "*nice*",
                "lala&",
                "(best",
                "worst)",
                "#HASH|",
                "pointy^^",
                '"quotes"',
                "'quotes'",
                "job420",
                "{left",
                "right}",
                ",comma",
                "100%",
                "any_more?",
                "\\slash",
                "otherslash/",
            ]
            for badname in badnames:
                with self.assertRaises(ValueError):
                    pipeline.set_job_alias(change_proc, badname)

    def test_change_job_alias_with_no_job(self):
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")
        with ProjectGraph(name="short_cl2d_finished", read_only=False) as pipeline:
            with self.assertRaises(ValueError):
                pipeline.set_job_alias(None, "example_alias")

    def test_change_job_alias_not_unique(self):
        """Test trying to use a non unique alias raises and error"""
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_finished", read_only=False) as pipeline:
            change_proc = pipeline.process_list[7]
            # set another process with the desired alias
            pipeline.process_list[6].alias = "Class2D/taken/"
            with self.assertRaises(ValueError):
                pipeline.set_job_alias(change_proc, "taken")

    def test_change_job_alias_not_unique_ok_if_same_job(self):
        """Test trying to change the alias to the same alias, like what would happen
        when a script archive run script is executed"""
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        with ProjectGraph(name="short_cl2d_finished", read_only=False) as pipeline:
            change_proc = pipeline.process_list[7]
            # same proc, same alias - no error should occur
            pipeline.set_job_alias(change_proc, "LoG_based")

    def test_change_job_alias_not_unique_after_fixing_space(self):
        """Test the case where removing an illegal space makes the
        alias not unique"""
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_finished", read_only=False) as pipeline:
            change_proc = pipeline.process_list[7]
            pipeline.process_list[6].alias = "Class2D/its_taken/"
            with self.assertRaises(ValueError):
                pipeline.set_job_alias(change_proc, "its taken")

    def test_change_job_alias_spaces_are_OK(self):
        """Test automatically replacing spaces in aliases with _"""
        self.make_alias_file_structure("short_cl2d_finished", "LoG_based")

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_finished", read_only=False) as pipeline:
            change_proc = pipeline.process_list[7]
            pipeline.set_job_alias(change_proc, "pretty new one")

        assert os.path.islink(os.path.join(self.test_dir, "Class2D/pretty_new_one"))
        assert not os.path.islink(os.path.join(self.test_dir, "Class2D/LoG_based"))
        oldnodes = [
            f"{NODE_PARTICLEGROUPMETADATA}/Class2D/LoG_based/run_it025_data.star",
            f"{NODE_OPTIMISERDATA}/Class2D/LoG_based/run_it025_optimiser.star",
        ]
        new_nodes = [
            f"{NODE_PARTICLEGROUPMETADATA}/Class2D/pretty_new_one/run_it025_data.star",
            f"{NODE_OPTIMISERDATA}/Class2D/pretty_new_one/run_it025_optimiser.star",
        ]

        for n in oldnodes:
            assert not os.path.isfile(os.path.join(NODES_DIR, n)), n
        for n in new_nodes:
            assert os.path.isfile(os.path.join(NODES_DIR, n)), n

        pipe_data = clean_starfile("short_cl2d_finished_pipeline.star")

        removed_line = [
            "Class2D/job008/",
            "Class2D/LoG_based/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]
        new_line = [
            "Class2D/job008/",
            "Class2D/pretty_new_one/",
            CLASS2D_PARTICLE_NAME_EM,
            JOBSTATUS_SUCCESS,
        ]

        assert removed_line not in pipe_data, removed_line
        assert new_line in pipe_data, new_line

    def make_undelete_file_structure(self):
        # make the directories needed
        dirs = [
            "Class2D/job008",
            "Extract/job007",
            "Trash",
        ]

        for d in dirs:
            os.makedirs(d)
            assert os.path.isdir(d), d

        # make files common to all run types
        common_files = [
            "run.out",
            "run.err",
            "note.txt",
            "run.job",
            "default_pipeline.star",
            SUCCESS_FILE,
        ]

        outfiles = list()
        for d in dirs[:-1]:
            for f in common_files:
                fn = d + "/" + f
                touch(fn)
                assert os.path.isfile(fn), fn
                outfiles.append(fn)

        # copy in the pipeline
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_cl2d_job_pipeline.star"
            ),
            os.path.join(self.test_dir, "Class2D/job008/job_pipeline.star"),
        )
        # add the specific files for each run
        outfiles.append("Class2D/job008/job_pipeline.star")

        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_extract_job_pipeline.star"
            ),
            os.path.join(self.test_dir, "Extract/job007/job_pipeline.star"),
        )
        outfiles.append("Extract/job007/job_pipeline.star")

        shutil.copy(self.test_starfile, "Extract/job007/particles.star")
        assert os.path.isfile("Extract/job007/particles.star")
        outfiles.append("Extract/job007/particles.star")

        os.makedirs("Extract/job007/Movies")
        assert os.path.isdir("Extract/job007/Movies")

        # make particles files
        for i in range(1, 11):
            f = "Extract/job007/Movies/movie_{:03d}.mrcs".format(i)
            shutil.copy(self.test_mrcfile, f)
            assert os.path.isfile(f), f
            outfiles.append(f)

        # make class2d files
        cl2d_files = ["_data.star", "_optimiser.star", "_optimiser.star"]
        for i in range(1, 26):
            for f in cl2d_files:
                ff = "Class2D/job008/run_it{:03d}{}".format(i, f)
                shutil.copy(self.test_starfile, ff)
                assert os.path.isfile(ff)
                outfiles.append(ff)

        shutil.move("Class2D", "Trash/Class2D")
        shutil.move("Extract", "Trash/Extract")

        assert not os.path.isdir("Class2D")
        assert not os.path.isdir("Extract")
        assert os.path.isdir("Trash/Class2D")
        assert os.path.isdir("Trash/Extract")

        return outfiles

    def test_undelete_single(self):
        # make the file structure like jobs have been run
        outfiles = self.make_undelete_file_structure()

        # copy in the pipeline
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_deleted_pipeline.star"
            ),
            self.test_dir,
        )

        # lines that should be restored
        restored_lines = [
            [
                "Extract/job007/",
                "Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            ["CtfFind/job003/micrographs_ctf.star", "Extract/job007/"],
            ["AutoPick/job006/coords_suffix_autopick.star", "Extract/job007/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
        ]

        # make sure the lines to be undelete are not in the pipeline
        original = clean_starfile("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line not in original, line

        # Open pipeline STAR file
        with ProjectGraph(name="for_undelete_deleted", read_only=False) as pipeline:
            pipeline.undelete_job("Extract/job007/")

        # make sure the undeleted lines are back in the pipeline
        wrote = clean_starfile("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line in wrote, line

        # make sure the files are back
        for f in outfiles:
            if "Extract" in f:
                assert os.path.isfile(f), f

        # make sure the .Nodes entries are back
        nodes_files = [
            f".Nodes/{NODE_PARTICLEGROUPMETADATA}/Extract/LoG_based/particles.star",
        ]
        for f in nodes_files:
            assert os.path.isfile(f), f

        # make sure the alias is restored
        assert os.path.islink("Extract/LoG_based")

    def test_undelete_single_no_alias(self):
        # set up file structure
        outfiles = self.make_undelete_file_structure()

        # get pipelines
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_deleted_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_noalias_job_pipeline.star"
            ),
            os.path.join(self.test_dir, "Trash/Extract/job007/job_pipeline.star"),
        )

        # lines to be restored
        restored_lines = [
            ["Extract/job007/", "None", EXTRACT_PARTICLE_NAME, JOBSTATUS_SUCCESS],
            ["CtfFind/job003/micrographs_ctf.star", "Extract/job007/"],
            ["AutoPick/job006/coords_suffix_autopick.star", "Extract/job007/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
        ]

        # make sure lines to be restored are not in the pipeline
        original = clean_starfile("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line not in original, line

        # Open pipeline STAR file
        with ProjectGraph(name="for_undelete_deleted", read_only=False) as pipeline:
            pipeline.undelete_job("Extract/job007/")

        # make sure the lines have been restored to the pipeline
        wrote = clean_starfile("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line in wrote, line

        # make sure the files are back
        for f in outfiles:
            if "Extract" in f:
                assert os.path.isfile(f), f

        # make sure the .Nodes entries are back
        nodes_files = [
            f".Nodes/{NODE_PARTICLEGROUPMETADATA}/Extract/job007/particles.star",
        ]
        for f in nodes_files:
            assert os.path.isfile(f), f

    def test_undelete_single_alias_not_unique(self):
        # make file structure
        outfiles = self.make_undelete_file_structure()

        # copy in the pipelines
        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_extra_alias_pipeline.star"
            ),
            self.test_dir,
        )

        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_extract_job_pipeline.star"
            ),
            os.path.join(self.test_dir, "Trash/Extract/job007/job_pipeline.star"),
        )

        # lines to be restored
        restored_lines = [
            ["Extract/job007/", "None", EXTRACT_PARTICLE_NAME, JOBSTATUS_SUCCESS],
            ["CtfFind/job003/micrographs_ctf.star", "Extract/job007/"],
            ["AutoPick/job006/coords_suffix_autopick.star", "Extract/job007/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
        ]

        # make sure lines to be restored are not in pipeline
        original = clean_starfile("for_undelete_extra_alias_pipeline.star")

        for line in restored_lines:
            assert line not in original, line

        # Open pipeline STAR file
        with ProjectGraph(name="for_undelete_extra_alias", read_only=False) as pipeline:
            pipeline.undelete_job("Extract/job007/")

        # make sure the lines are back in  the pipeline
        wrote = clean_starfile("for_undelete_extra_alias_pipeline.star")

        for line in restored_lines:
            assert line in wrote, line

        # make sure the files are back
        for f in outfiles:
            if "Extract" in f:
                assert os.path.isfile(f), f

        # make sure the .Nodes entries are back
        nodes_files = [
            f".Nodes/{NODE_PARTICLEGROUPMETADATA}/Extract/job007/particles.star",
        ]
        for f in nodes_files:
            assert os.path.isfile(f), f

    def test_undelete_process_with_parents(self):
        outfiles = self.make_undelete_file_structure()

        shutil.copy(
            os.path.join(
                self.test_data, "Pipelines/for_undelete_deleted_pipeline.star"
            ),
            self.test_dir,
        )

        restored_lines = [
            [
                "Extract/job007/",
                "Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            ["CtfFind/job003/micrographs_ctf.star", "Extract/job007/"],
            ["AutoPick/job006/coords_suffix_autopick.star", "Extract/job007/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
            [
                "Class2D/job008/run_it025_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it025_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            ],
            ["Extract/job007/particles.star", "Class2D/job008/"],
            ["Class2D/job008/", "Class2D/job008/run_it025_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_data.star"],
        ]

        original = clean_starfile("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line not in original, line

        # Open pipeline STAR file
        with ProjectGraph(name="for_undelete_deleted", read_only=False) as pipeline:
            pipeline.undelete_job("Class2D/job008/")

        wrote = clean_starfile("for_undelete_deleted_pipeline.star")

        for line in restored_lines:
            assert line in wrote, line

        for f in outfiles:
            assert os.path.isfile(f), f

        assert os.path.islink("Extract/LoG_based")
        assert os.path.islink("Class2D/LoG_based")

    def test_delete_job_then_undelete(self):
        # create the file structure - move things out of the trash
        # like they weren't deleted

        outfiles = self.make_undelete_file_structure()
        shutil.move("Trash/Class2D", "Class2D")
        shutil.move("Trash/Extract", "Extract")
        os.symlink(
            os.path.abspath("Extract/job007"),
            os.path.join(self.test_dir, "Extract/LoG_based"),
            True,
        )
        os.symlink(
            os.path.abspath("Class2D/job008"),
            os.path.join(self.test_dir, "Class2D/LoG_based"),
            True,
        )

        # make the .Nodes entries
        nodes_files = [
            ".Nodes/{}/Extract/job007/particles.star".format(
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            ),
            ".Nodes/{}/Class2D/job008/run_it025_optimiser.star".format(
                f"{NODE_OPTIMISERDATA}.star.relion.class2d"
            ),
            ".Nodes/{}/Class2D/job008/run_it025_data.star".format(
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d"
            ),
        ]
        for f in nodes_files:
            os.makedirs(os.path.dirname(f), exist_ok=True)
            shutil.copy(self.test_starfile, f)

        # copy in the pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_cl2d_del_pipeline.star"),
            self.test_dir,
        )

        # Open pipeline STAR file
        with ProjectGraph(name="short_cl2d_del", read_only=False) as pipeline:
            del_proc = pipeline.process_list[6]
            pipeline.delete_job(del_proc)

        # open the newly written pipeline
        written = clean_starfile("short_cl2d_del_pipeline.star")

        removed_lines = [
            [
                "Extract/job007/",
                "Extract/LoG_based/",
                EXTRACT_PARTICLE_NAME,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Class2D/job008/",
                "Class2D/LoG_based/",
                CLASS2D_PARTICLE_NAME_EM,
                JOBSTATUS_SUCCESS,
            ],
            [
                "Extract/job007/particles.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion",
            ],
            [
                "Class2D/job008/run_it025_data.star",
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d",
            ],
            [
                "Class2D/job008/run_it025_optimiser.star",
                f"{NODE_OPTIMISERDATA}.star.relion.class2d",
            ],
            ["Extract/job007/particles.star", "Class2D/job008/"],
            ["Extract/job007/", "Extract/job007/particles.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_optimiser.star"],
            ["Class2D/job008/", "Class2D/job008/run_it025_data.star"],
        ]

        # make sure everything is deleted as expected
        for line in removed_lines:
            assert line not in written, line

        # makes sure the .Nodes entries are not there
        nodes_files = [
            ".Nodes/{}/Extract/job007/particles.star".format(
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion"
            ),
            ".Nodes/{}/Class2D/job008/run_it025_optimiser.star".format(
                f"{NODE_OPTIMISERDATA}.star.relion.class2d"
            ),
            ".Nodes/{}/Class2D/job008/run_it025_data.star".format(
                f"{NODE_PARTICLEGROUPMETADATA}.star.relion.class2d"
            ),
        ]
        for f in nodes_files:
            assert not os.path.isfile(f), f

        # make sure the files are in the trash
        for f in outfiles:
            assert not os.path.isfile(f), f
            trashname = "Trash/" + f
            assert os.path.isfile(trashname), trashname

        # Open pipeline STAR file again
        with ProjectGraph(name="short_cl2d_del", read_only=False) as pipeline:
            # undelete the job that has a parent
            pipeline.undelete_job("Class2D/job008/")

        # make sure the lines are restored to the pipeline
        restored = clean_starfile("short_cl2d_del_pipeline.star")
        for line in removed_lines:
            assert line in restored, (line, restored)

        # make sure the files are back
        for f in outfiles:
            assert os.path.isfile(f), f

        # make sure the .Nodes entries are back
        nodes_files = [
            f".Nodes/{NODE_PARTICLEGROUPMETADATA}/Extract/LoG_based/particles.star",
            f".Nodes/{NODE_OPTIMISERDATA}/Class2D/LoG_based/run_it025_optimiser.star",
            f".Nodes/{NODE_PARTICLEGROUPMETADATA}/Class2D/LoG_based/run_it025_data"
            ".star",
        ]
        for f in nodes_files:
            assert os.path.isfile(f), f

    @live_test(jobs=["relion.import", "relion.maskcreate", "relion.postprocess"])
    def test_check_pipeline_entries(self):
        """Make sure a bug where input and output edges are duplicated
        is not occurring - any node that was used as an input to another
        process was duplicated in input and output edges"""
        # TODO: modify this to avoid needing to actually run the jobs

        # copy in the map
        shutil.copy(os.path.join(self.test_data, "emd_3488.mrc"), self.test_dir)

        # Prepare a new pipeline and run the import job
        with ProjectGraph(create_new=True) as pipeline:
            map_job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/Import/import_map.job")
            )
            job_manager.run_job(pipeline=pipeline, job=map_job, run_in_foreground=True)

            # run the mask create job
            mask_job = job_factory.read_job(
                os.path.join(self.test_data, "JobFiles/MaskCreate/maskcreate.job")
            )
            job_manager.run_job(pipeline=pipeline, job=mask_job, run_in_foreground=True)

            # copy in the halfmaps
            halfmap_import_dir = "Halfmaps"
            os.makedirs(halfmap_import_dir)
            shutil.copy(
                os.path.join(self.test_data, "3488_run_half1_class001_unfil.mrc"),
                halfmap_import_dir,
            )
            shutil.copy(
                os.path.join(self.test_data, "3488_run_half2_class001_unfil.mrc"),
                halfmap_import_dir,
            )

            # run the postprocess job
            pp_job = job_factory.read_job(
                os.path.join(
                    self.test_data, "JobFiles/PostProcess/postprocess_halfmapdir.job"
                )
            )
            job_manager.run_job(pipeline=pipeline, job=pp_job, run_in_foreground=True)

        duplicated_lines = [
            ["Import/job001/emd_3488.mrc", "MaskCreate/job002/"],
            ["MaskCreate/job002/mask.mrc", "PostProcess/job003/"],
            ["Import/job001/", "Import/job001/emd_3488.mrc"],
            ["MaskCreate/job002/", "MaskCreate/job002/mask.mrc"],
        ]

        # make sure the duplicated nodes are not there
        dups = 0
        pipe_data = clean_starfile("default_pipeline.star")

        for line in duplicated_lines:
            for pipe_line in pipe_data:
                if line in pipe_line:
                    dups += 1
            assert dups <= 1, line
            dups = 0

    def test_get_project_procs_list(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/tutorial_pipeline.star"),
            "default_pipeline.star",
        )

        # Read pipeline STAR file
        # Need read_only=False because star file is in relion format
        with ProjectGraph(read_only=False) as pipeline:
            procslist = pipeline.get_project_procs_list("LocalRes/job029/")

        expected = [
            "LocalRes/job029/",
            "Refine3D/job027/",
            "Polish/job026/",
            "CtfRefine/job024/",
            "MaskCreate/job022/",
            "Refine3D/job021/",
            "Extract/job020/",
            "Select/job019/",
            "Class3D/job018/",
            "InitialModel/job017/",
            "Select/job016/",
            "Class2D/job015/",
            "Select/job014/",
            "Extract/job012/",
            "AutoPick/job011/",
            "Select/job009/",
            "Class2D/job008/",
            "Extract/job007/",
            "AutoPick/job006/",
            "Select/job005/",
            "ManualPick/job004/",
            "CtfFind/job003/",
            "MotionCorr/job002/",
            "Import/job001/",
        ]
        assert [x.name for x in procslist] == expected

    def test_node_types_are_not_changed_after_read(self):
        # Prepare a pipeline
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            "default_pipeline.star",
        )
        # Make a node with a bad format (that would have its format changed if it was
        # created as a new node)
        os.makedirs("Import/job001")
        with open("Import/job001/movies.star", "w") as not_a_real_star_file:
            not_a_real_star_file.write("Not a real star file")
        # Verify that the node format would be changed
        test_node = create_node(
            "Import/job001/movies.star", NODE_MICROGRAPHMOVIEGROUPMETADATA
        )
        assert test_node.format == "XstarX"

        # Read pipeline STAR file - no validation occurs
        with ProjectGraph(read_only=False) as pipeline:
            assert pipeline.node_list[0].name == "Import/job001/movies.star"
            assert pipeline.node_list[0].format == "star"
            assert (
                pipeline.node_list[0].type
                == f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion"
            )

    def test_add_job_doesnt_add_outputnodes_if_job_failed(self):
        pipe = os.path.join(self.test_data, "Pipelines/empty_pipeline.star")
        shutil.copy(pipe, "default_pipeline.star")
        job = DummyJob()
        job.input_nodes = [Node("Input.txt", NODE_LOGFILE)]
        os.makedirs("Test/job001")
        job.output_dir = "Test/job001"

        with ProjectGraph(read_only=False) as pipeline:
            pipeline.add_job(job=job, as_status=JOBSTATUS_FAIL, do_overwrite=True)

        with ProjectGraph() as pipeline:
            assert len(pipeline.node_list) == 1
            input_edges, output_edges = pipeline.get_pipeline_edges()
            assert len(input_edges) == 1
            assert len(output_edges) == 0

    def test_add_job_doesnt_add_outputnodes_if_job_aborted(self):
        pipe = os.path.join(self.test_data, "Pipelines/empty_pipeline.star")
        shutil.copy(pipe, "default_pipeline.star")
        job = DummyJob()
        job.input_nodes = [Node("Input.txt", NODE_LOGFILE)]
        os.makedirs("Test/job001")
        job.output_dir = "Test/job001"

        with ProjectGraph(read_only=False) as pipeline:
            pipeline.add_job(job=job, as_status=JOBSTATUS_ABORT, do_overwrite=True)

        with ProjectGraph() as pipeline:
            assert len(pipeline.node_list) == 1
            input_edges, output_edges = pipeline.get_pipeline_edges()
            assert len(input_edges) == 1
            assert len(output_edges) == 0

    def test_missing_nodes_removed_on_fail(self):
        pipe = os.path.join(self.test_data, "Pipelines/empty_pipeline.star")
        shutil.copy(pipe, "default_pipeline.star")
        job = DummyJob()
        job.output_nodes = [
            Node("Test/job001/not_a_file.txt", "FakeNodeType"),
            Node("Test/job001/is_a_file.txt", "FakeNodeType"),
        ]
        os.makedirs("Test/job001")
        job.output_dir = "Test/job001/"
        touch("Test/job001/is_a_file.txt")

        with ProjectGraph(read_only=False) as pipeline:
            pipeline.add_job(job=job, as_status=JOBSTATUS_RUN, do_overwrite=True)
            nodes = [x.name for x in pipeline.node_list]
            assert "Test/job001/not_a_file.txt" in nodes
            assert "Test/job001/is_a_file.txt" in nodes

            touch(f"Test/job001/{FAIL_FILE}")
            pipeline.check_process_completion()
            nodes = [x.name for x in pipeline.node_list]
            assert "Test/job001/not_a_file.txt" not in nodes
            assert "Test/job001/is_a_file.txt" in nodes

    def test_missing_nodes_removed_on_abort(self):
        pipe = os.path.join(self.test_data, "Pipelines/empty_pipeline.star")
        shutil.copy(pipe, "default_pipeline.star")
        job = DummyJob()
        job.output_nodes = [
            Node("Test/job001/not_a_file.txt", "FakeNodeType"),
            Node("Test/job001/is_a_file.txt", "FakeNodeType"),
        ]
        os.makedirs("Test/job001")
        job.output_dir = "Test/job001/"
        touch("Test/job001/is_a_file.txt")

        with ProjectGraph(read_only=False) as pipeline:
            pipeline.add_job(job=job, as_status=JOBSTATUS_RUN, do_overwrite=True)
            nodes = [x.name for x in pipeline.node_list]
            assert "Test/job001/not_a_file.txt" in nodes
            assert "Test/job001/is_a_file.txt" in nodes

            touch(f"Test/job001/{ABORT_FILE}")
            pipeline.check_process_completion()
            nodes = [x.name for x in pipeline.node_list]
            assert "Test/job001/not_a_file.txt" not in nodes
            assert "Test/job001/is_a_file.txt" in nodes

    def test_relion5_jobname_conversion(self):
        pipe = os.path.join(self.test_data, "Pipelines/bad_reextract_name.star")
        shutil.copy(pipe, "default_pipeline.star")
        with ProjectGraph() as pipeline:
            assert pipeline.process_list[0].type == "relion.extract"

    @patch("pipeliner.project_graph.new_job_of_type")
    @patch("pipeliner.project_graph.read_job")
    def test_cleanup_a_job(self, mock_njot, mock_read):
        shutil.copy(
            Path(self.test_data) / "Pipelines/relion40_tutorial.star",
            "default_pipeline.star",
        )
        jobname = Path("CleanedUp/job001/")

        # dirs
        keep_dir = jobname / "keep_dir/"
        rm_dir = jobname / "rm_dir/"
        for d in [keep_dir, rm_dir]:
            d.mkdir(parents=True)

        # files
        rm_files = [jobname / f"r_file{n}.txt" for n in range(5)]
        keep_files = [jobname / f"k_file{n}.txt" for n in range(5)]
        for f in rm_files + keep_files:
            f.touch()

        def dummy_cleanup(harsh: bool):
            return list(jobname.glob("r_*.txt")), [str(jobname / "rm_dir/")]

        proc = Process(name=str(jobname), p_type="dummyjob", status=JOBSTATUS_SUCCESS)
        job = DummyJob()
        job.prepare_clean_up_lists = dummy_cleanup
        job.output_dir = str(jobname) + "/"
        job.write_jobstar(output_dir=str(jobname) + "/")
        mock_njot.return_value = job
        mock_read.return_value = job

        pipeline = ProjectGraph()
        with patch("pipeliner.project_graph.date_time_tag") as dtt_mock:
            dtt_mock.return_value = "DATE TIME TAG"
            pipeline.clean_up_job(proc, do_harsh=False)

        assert keep_dir.is_dir()
        assert not rm_dir.is_dir()
        for f in rm_files:
            assert not f.is_file()
            cleaned = Path(CLEANUP_DIR) / f
            assert cleaned.is_file()
        for f in keep_files:
            assert f.is_file()
        with open(CLEANUP_LOG) as clog:
            lines = clog.readlines()
        assert lines[-1].strip("\n") == (
            "DATE TIME TAG: Cleanup; 5 file(s), 1 directory(s) removed"
        )

    def test_restoring_cleanedup_files(self):
        shutil.copy(
            Path(self.test_data) / "Pipelines/relion40_tutorial.star",
            "default_pipeline.star",
        )
        jobname = Path("CleanedUp/job001/")

        # dirs
        keep_dir = jobname / "keep_dir/"
        keep_dir.mkdir(parents=True)
        rm_dir = jobname / "rm_dir/"
        cleaned_dir = Path(CLEANUP_DIR) / rm_dir
        cleaned_dir.mkdir(parents=True)

        # files
        rm_files = [jobname / f"r_file{n}.txt" for n in range(5)]
        keep_files = [jobname / f"k_file{n}.txt" for n in range(5)]
        for f in keep_files:
            f.touch()
        for f in rm_files:
            cleaned = Path(CLEANUP_DIR) / f
            cleaned.touch()

        proc = Process(name=str(jobname), p_type="dummyjob", status=JOBSTATUS_SUCCESS)
        with patch("pipeliner.project_graph.date_time_tag") as dtt_mock:
            dtt_mock.return_value = "DATE TIME TAG"
            with ProjectGraph() as pipeline:
                pipeline.restore_cleaned_up_files(proc)

        for f in keep_files:
            assert f.is_file()
        for d in [keep_dir, rm_dir]:
            assert d.is_dir()
        for f in rm_files:
            assert f.is_file()
            cleaned = Path(CLEANUP_DIR) / f
            assert not cleaned.is_file()
            assert not cleaned.is_dir()
        with open(CLEANUP_LOG) as clog:
            lines = clog.readlines()
        assert lines[-1].strip("\n") == "DATE TIME TAG: Restored 5 cleaned up items"

    def test_fix_for_reserved_word_in_node_name(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            "test_pipeline.star",
        )
        with ProjectGraph("test", read_only=False) as pipeline:
            test_node = Node(
                name="data_movies.star",
                toplevel_type="data_bad",
                kwds=["test"],
                do_validation=False,
            )
            pipeline.add_new_output_edge(pipeline.process_list[0], test_node)
            pipeline.add_new_input_edge(test_node, pipeline.process_list[1])
            pipeline._write()

        with open("test_pipeline.star") as wrote_pipe:
            lines = wrote_pipe.readlines()

        # check lines are quoted
        assert "'data_movies.star' 'data_bad.star.test' \n" in lines
        assert "Import/job001/ 'data_movies.star' \n" in lines
        assert "'data_movies.star' MotionCorr/job002/ \n" in lines

        # check it reads in normally
        with ProjectGraph("test", read_only=False) as pipeline:
            assert pipeline.node_list[-1].name == "data_movies.star"
            assert [x.name for x in pipeline.process_list[0].output_nodes] == [
                "Import/job001/movies.star",
                "data_movies.star",
            ]
            assert [x.type for x in pipeline.process_list[0].output_nodes] == [
                f"{NODE_MICROGRAPHMOVIEGROUPMETADATA}.star.relion",
                "data_bad.star.test",
            ]
            assert [x.name for x in pipeline.process_list[1].input_nodes] == [
                "Import/job001/movies.star",
                "data_movies.star",
            ]
            pipeline._write()

        # check read cycle preserves quotes
        with open("test_pipeline.star") as wrote_pipe:
            lines = wrote_pipe.readlines()
        # check lines are quoted
        assert "'data_movies.star' 'data_bad.star.test' \n" in lines
        assert "Import/job001/ 'data_movies.star' \n" in lines
        assert "'data_movies.star' MotionCorr/job002/ \n" in lines

    def test_fix_for_reserved_word_in_alias_name(self):
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/short_pipeline.star"),
            "test_pipeline.star",
        )
        with ProjectGraph("test", read_only=False) as pipeline:
            pipeline.process_list[0].alias = "data_alias"
            pipeline._write()

        with open("test_pipeline.star") as wrote_pipe:
            lines = [x.split() for x in wrote_pipe.readlines()]

        # check lines are quoted
        assert [
            "Import/job001/",
            "'data_alias'",
            "relion.import.movies",
            "Succeeded",
        ] in lines

        # check it reads in normally
        with ProjectGraph("test", read_only=False) as pipeline:
            assert pipeline.process_list[0].alias == "data_alias", [
                pipeline.process_list[0].alias
            ]
            pipeline._write()

        # check read cycle preserves quotes
        with open("test_pipeline.star") as wrote_pipe:
            lines = [x.split() for x in wrote_pipe.readlines()]

        # check lines are quoted
        assert [
            "Import/job001/",
            "'data_alias'",
            "relion.import.movies",
            "Succeeded",
        ] in lines


if __name__ == "__main__":
    unittest.main()
