#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import json
import os
import tempfile
import shutil

from pipeliner.reference_reports import (
    get_reference_list,
    write_reference_file,
    add_ref_report_to_summary_file,
    remove_ref_report_from_summary_file,
    prepare_reference_report,
)
from pipeliner.summary_data_tools import SUMMARY_DATA_FILE
from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import get_relion_tutorial_data
from pipeliner.starfile_handler import DataStarFile
from pipeliner.pipeliner_job import Ref


class ReferenceReportsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="relion_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def get_tutorial_refs(self):
        get_relion_tutorial_data(relion_version=4)
        prepare_reference_report("PostProcess/job030/", "references.json")

    def test_prepare_reference_report(self):
        get_relion_tutorial_data(relion_version=4)
        prepare_reference_report("PostProcess/job030/", "references.json")
        refs_file = os.path.join(self.test_data, "Metadata/relion_tutorial_refs.json")
        with open(refs_file, "r") as ref:
            exp_refs = json.load(ref)
        exp = [exp_refs[x] for x in exp_refs]
        with open("references.json") as wrote:
            written_refs = json.load(wrote)
        wrote = [written_refs[x] for x in written_refs]
        for i in wrote:
            print(i)
            assert i in exp, i
        for i in exp:
            assert i in wrote, i

    def test_refs_list_with_additional_refs_test(self):
        """Should only return the running references, not the fetched map references"""
        fetch_job = os.path.join(self.test_data, "JobFiles/Fetch/fetch_emdb")
        import_dir = os.path.join(self.test_dir, "Fetch/job001")
        shutil.copytree(fetch_job, import_dir)
        shutil.copy(os.path.join(import_dir, "default_pipeline.star"), self.test_dir)
        refs = get_reference_list("Fetch/job001/")
        assert len(refs) == 1
        result = refs["Fetch/job001/"]
        assert result[0][0].command == "gzip"
        assert result[1] == []

    def test_writing_ref_report_file(self):
        ref1 = Ref(
            authors="author1",
            title="A paper",
            journal="Journal1",
            year="1978",
            volume="1",
            issue="1.1",
            pages="1-2",
            doi="12345",
        )
        ref2 = Ref(
            authors=["author1", "author2"],
            title="Another paper",
            journal="Journal2",
            year="1979",
            volume="2",
            issue="2.2",
            pages="2-4",
            doi="54321",
        )
        write_reference_file(
            [(ref1, ["Import/job001"]), (ref2, ["Import/job001", "MotionCorr/job002"])],
            "reffile.json",
        )
        exp_f = os.path.join(self.test_data, "Metadata/simple_ref_report.json")
        with open(exp_f) as ef:
            expected = json.load(ef)
        with open("reffile.json") as af:
            actual = json.load(af)
        assert expected == actual

    def test_adding_ref_report_to_summary_file(self):
        add_ref_report_to_summary_file("reffile.json", "PostProcess/job030/", 30)
        added = DataStarFile(SUMMARY_DATA_FILE).loop_as_list(
            "reference_reports",
            headers=True,
        )
        assert added == [
            [
                "_pipelinerReferenceReportName",
                "_pipelinerInitiatedFrom",
                "_pipelinerNumberOfJobs",
            ],
            ["reffile.json", "PostProcess/job030/", "30"],
        ]

    def test_removing_ref_report_from_summary_file(self):
        shutil.copy(
            os.path.join(self.test_data, "Metadata/summary_file_4.star"),
            SUMMARY_DATA_FILE,
        )
        remove_ref_report_from_summary_file(
            "ReferenceReports/20230322123534_job003.json"
        )
        after = DataStarFile(SUMMARY_DATA_FILE).loop_as_list(
            "reference_reports", headers=True
        )
        assert after == [
            [
                "_pipelinerReferenceReportName",
                "_pipelinerInitiatedFrom",
                "_pipelinerNumberOfJobs",
            ],
            [
                "ReferenceReports/20230322122525_job002.json",
                "MotionCorr/job002/",
                "2",
            ],
            [
                "ReferenceReports/20230322124555_job004.json",
                "AutoPick/job004/",
                "4",
            ],
            ["ReferenceReports/20230322125784_job005.json", "Extract/job005/", "5"],
        ]
