#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import tempfile
import shutil
from pathlib import Path

from gemmi import cif

from pipeliner_tests import test_data
from pipeliner_tests.testing_tools import (
    make_conversion_file_structure,
    compare_starfiles,
)
from pipeliner.project_graph import ProjectGraph
from pipeliner.relion_compatibility import (
    get_pipeline_version,
    convert_relion20_datafile,
    get_job_type,
    convert_relion3_1_pipeline,
    jobstar_options_as_dict,
)
from pipeliner.starfile_handler import JobStar


class RelionCompatibilityTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories and get example file.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_make_conversion_file_structure(self):
        make_conversion_file_structure()
        created_dirs = {
            "AutoPick": ["job006", "job010", "job011"],
            "Class2D": ["job008", "job013"],
            "Class3D": ["job016"],
            "CtfFind": ["job003"],
            "CtfRefine": ["job022", "job023", "job024"],
            "Extract": ["job007", "job012", "job018"],
            "Import": ["job001"],
            "InitialModel": ["job015"],
            "LocalRes": ["job031"],
            "ManualPick": ["job004"],
            "MaskCreate": ["job020"],
            "MotionCorr": ["job002"],
            "Polish": ["job027", "job028"],
            "PostProcess": ["job021", "job026", "job030"],
            "Refine3D": ["job019", "job025", "job029"],
            "Select": ["job005", "job009", "job014", "job017"],
        }
        for dr in created_dirs:
            for jobdir in created_dirs[dr]:
                the_dir = os.path.join(dr, jobdir)
                assert os.path.isdir(the_dir)

    def test_pipeline_version_check_identifies_relion31_style_pipeline(self):
        pipeline = os.path.join(
            self.test_data, "Pipelines/relion31_tutorial_pipeline.star"
        )
        data = cif.read_file(pipeline)

        assert get_pipeline_version(data) == "relion3"

    def test_pipeline_version_check_identifies_relion40_style_pipeline(self):
        pipeline = os.path.join(self.test_data, "Pipelines/relion40_tutorial.star")
        data = cif.read_file(pipeline)

        assert get_pipeline_version(data) == "relion4"

    def test_pipeline_version_check_identifies_new_style_pipeline(self):
        pipeline = os.path.join(
            self.test_data, "Pipelines/converted_relion31_pipeline.star"
        )
        data = cif.read_file(pipeline)

        assert get_pipeline_version(data) == "ccpem-pipeliner"

    def test_pipeline_versioncheck_on_new_empty_pipeline(self):
        with ProjectGraph(create_new=True) as pipeline:
            file = pipeline.star_file
        data = cif.read_file(str(file))
        print(file.read_text())
        assert get_pipeline_version(data) == "ccpem-pipeliner"

    def test_pipeline_versioncheck_on_empty_pipeline(self):
        pipeline = os.path.join(self.test_data, "Pipelines/empty_pipeline.star")
        data = cif.read_file(pipeline)
        assert get_pipeline_version(data) == "ccpem-pipeliner"

    def test_pipeline_versioncheck_on_pipeline_with_empty_process_block(self):
        Path("empty_process_pipeline.star").write_text(
            "data_pipeline_general\n\ndata_pipeline_processes\n"
        )
        data = cif.read_file("empty_process_pipeline.star")
        # This only happens if pipeline writing went wrong, so expect an error
        with self.assertRaisesRegex(ValueError, "No data found in pipeline_processes"):
            get_pipeline_version(data)

    def test_pipeline_versioncheck_on_empty_file(self):
        open("empty_file.star", "w").close()
        data = cif.read_file("empty_file.star")
        with self.assertRaisesRegex(ValueError, "Block pipeline_general not found"):
            get_pipeline_version(data)

    def test_convert_relion20_particles_file(self):
        test_star = os.path.join(self.test_data, "StarFiles/relion20_parts.star")
        ref_star = os.path.join(self.test_data, "StarFiles/20starfile_converted.star")
        shutil.copy(test_star, "20starfile.star")
        convert_relion20_datafile("20starfile.star", "particles", boxsize=256)
        compare_starfiles(ref_star, "20starfile_converted.star")

    def test_get_job_type(self):
        """Test that the get_jobtype handles RELION3.1 style
        process ids"""
        test_file = os.path.join(
            self.test_data, "JobFiles/Tutorial_Pipeline/Import_job001_job.star"
        )
        shutil.copy(test_file, os.path.join(self.test_dir, "test_job.star"))
        jobops_dict = jobstar_options_as_dict("test_job.star")
        jobtype = get_job_type(jobops_dict)
        assert jobtype[0] == "relion.import.movies"

    def test_relion31_pipeline_conversion(self):
        """Convert a pipeline from the RELION 3.1 to RELION 4.0 format
        Currently broken until coordinate updating is fixed"""
        make_conversion_file_structure()

        # additional dirs for the micrograph files coordinate conversion reads
        mic_files = [
            "Select/job005/micrographs_selected.star",
            "CtfFind/job003//micrographs_ctf.star",
        ]
        testfile = os.path.join(self.test_data, "micrographs_ctf.star")
        for f in mic_files:
            shutil.copy(testfile, f)

        pipeline = os.path.join(
            self.test_data, "Pipelines/relion31_tutorial_pipeline.star"
        )
        shutil.copy(pipeline, "default_pipeline.star")
        convert_relion3_1_pipeline("default_pipeline.star")

        expipe = os.path.join(
            self.test_data, "Pipelines/converted_relion31_pipeline.star"
        )

        compare_starfiles(expipe, "default_pipeline.star")

        # check backup was written
        compare_starfiles(pipeline, "default_pipeline.star.old")

    def test_pipeline_conversion_with_helical_jobs(self):
        """Convert a pipeline from the RELION 3.1 to RELION 4.0 format
        Class2D, Class3D and Refine3D jobs are spoofed to be helical"""
        make_conversion_file_structure()

        # make the Class2D, Class3D, and Refine3D jobfiles into helical
        for jdir in ["Class2D/job008/", "Class3D/job016/", "Refine3D/job019/"]:
            jobfile = os.path.join(jdir, "job.star")
            js = JobStar(jobfile)
            js.modify({"do_helix": "Yes"})
            js.write()

        # additional dirs for the micrograph files coordinate conversion reads
        mic_files = [
            "Select/job005/micrographs_selected.star",
            "CtfFind/job003//micrographs_ctf.star",
        ]
        testfile = os.path.join(self.test_data, "micrographs_ctf.star")
        for f in mic_files:
            shutil.copy(testfile, f)

        pipeline = os.path.join(
            self.test_data, "Pipelines/relion31_tutorial_pipeline.star"
        )
        shutil.copy(pipeline, "default_pipeline.star")
        convert_relion3_1_pipeline("default_pipeline.star")

        changed = ["Class2D/job008", "Class3D/job016", "Refine3D/job019"]

        # chack that .helical has been appended to the node type
        with ProjectGraph() as hpipe:
            for i in hpipe.node_list:
                if os.path.dirname(i.name) in changed:
                    assert i.type.split(".")[-1] == "helical"
                else:
                    assert i.type.split(".")[-1] != "helical"

    def test_pipeline_conversion_cant_update_coords(self):
        """Convert a pipeline from the RELION 3.1 to RELION 4.0 format
        with errors because the coordinate job files were not available
        for converting the coordinate files"""

        make_conversion_file_structure()
        pipeline = os.path.join(
            self.test_data, "Pipelines/relion31_tutorial_pipeline.star"
        )
        shutil.copy(pipeline, "default_pipeline.star")
        convert_relion3_1_pipeline("default_pipeline.star")

        expipe = os.path.join(
            self.test_data, "Pipelines/converted_relion31_coordserrors_pipeline.star"
        )
        compare_starfiles(expipe, "default_pipeline.star")

        # check backup was written
        compare_starfiles(pipeline, "default_pipeline.star.old")


if __name__ == "__main__":
    unittest.main()
