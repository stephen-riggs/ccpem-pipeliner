import unittest
import tempfile
import os
import shutil
import traceback
from pathlib import Path

from pipeliner.api.manage_project import PipelinerProject
from pipeliner_tests.testing_tools import get_relion_tutorial_data, slow_test
from pipeliner.scripts import regenerate_results


class TestingToolsTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    @staticmethod
    def copy_real_files(src, dst):
        if not Path(src).is_symlink():
            shutil.copy2(src, dst)

    @slow_test
    def test_all_functions_with_relion4_pipeline(self):
        get_relion_tutorial_data(relion_version=4)
        proj = PipelinerProject()
        errors = {}
        functs_args = [
            (proj.create_reference_report, {"terminal_job": "PostProcess/job030/"}),
            (proj.prepare_metadata_report, {"jobname": "PostProcess/job030/"}),
            (proj.create_archive, {"job": "PostProcess/job030/", "tar": False}),
            (
                proj.create_archive,
                {"job": "PostProcess/job030/", "full": True, "tar": False},
            ),
            (proj.cleanup_all, {}),
        ]

        for funct in functs_args:
            if Path(".relion_lock").is_dir():
                shutil.rmtree(".relion_lock")
            try:
                funct[0](**funct[1])
            except Exception:
                tb = str(traceback.format_exc())
                errors[str(funct[0])] = tb

        try:
            regenerate_results.main(in_args=[])
        except Exception:
            tb = str(traceback.format_exc())
            errors["Regenerate results"] = tb

        if errors:
            print("*" * 16)
            for err, tb in errors.items():
                print(f"**** ERROR CAUSED BY: {err}")
                print(tb)
            print("*" * 16)

        assert not errors

    @slow_test
    def test_all_functions_with_relion5_pipeline(self):
        get_relion_tutorial_data(relion_version=5)
        proj = PipelinerProject()
        errors = {}
        functs_args = [
            (proj.create_reference_report, {"terminal_job": "ModelAngelo/job033/"}),
            (proj.prepare_metadata_report, {"jobname": "ModelAngelo/job033/"}),
            (proj.create_archive, {"job": "ModelAngelo/job033/", "tar": False}),
            (
                proj.create_archive,
                {"job": "ModelAngelo/job033/", "full": True, "tar": False},
            ),
            (proj.cleanup_all, {}),
        ]

        for funct in functs_args:
            if Path(".relion_lock").is_dir():
                shutil.rmtree(".relion_lock")
            try:
                funct[0](**funct[1])
            except Exception:
                tb = str(traceback.format_exc())
                errors[str(funct[0])] = tb

        # todo: can't test this because it's been cleaned and the files are gone
        #  make a new version of the project that has the files
        # try:
        #     regenerate_results.main(in_args=[])
        # except Exception:
        #     tb = str(traceback.format_exc())
        #     errors["Regenerate results"] = tb
        if errors:
            print("*" * 16)
            for err, tb in errors.items():
                print(f"**** ERROR CAUSED BY: {err}")
                print(tb)
            print("*" * 16)
        assert not errors
