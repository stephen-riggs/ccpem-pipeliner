#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
from pipeliner.runjob_reader import RunJobFile
import os
import tempfile
import shutil
from pipeliner_tests import test_data
from pipeliner.data_structure import NODE_DENSITYMAP
from pipeliner_tests.testing_tools import compare_starfiles


class RunJobReaderTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories and get example file.
        """
        self.test_data = os.path.dirname(test_data.__file__)
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_get_all_options(self):
        testfile = os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        the_rj = RunJobFile(testfile)
        options = the_rj.get_all_options()
        expected_dict = {
            "_rlnJobIsContinue": "0",
            "_rlnJobIsTomo": "0",
            "_rlnJobTypeLabel": "relion.import",
            "alt_nodetype": "",
            "fn_in_other": "emd_3488.mrc",
            "is_relion": "No",
            "is_synthetic": "No",
            "kwds": "",
            "node_type": "RELION coordinates STAR file (.star)",
            "optics_group_particles": "",
            "pipeliner_node_type": NODE_DENSITYMAP,
        }
        assert options == expected_dict

    def test_write_job_star_default_name(self):
        testfile = os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        shutil.copy(testfile, "test.job")
        the_rj = RunJobFile("test.job")
        the_rj.convert_to_jobstar()
        exp_file = os.path.join(self.test_data, "JobFiles/Import/import_map_job.star")
        compare_starfiles("test_job.star", exp_file)

    def test_write_job_star_custom_name(self):
        testfile = os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        shutil.copy(testfile, "test.job")
        the_rj = RunJobFile("test.job")
        the_rj.convert_to_jobstar("conv_job.star")
        exp_file = os.path.join(self.test_data, "JobFiles/Import/import_map_job.star")
        compare_starfiles("conv_job.star", exp_file)

    def test_write_job_star_custom_name_with_error(self):
        testfile = os.path.join(self.test_data, "JobFiles/Import/import_map.job")
        shutil.copy(testfile, "test.job")
        the_rj = RunJobFile("test.job")
        with self.assertRaises(ValueError):
            the_rj.convert_to_jobstar(output_name="job.not_star")

    def test_relion5_jobname_conversion(self):
        testfile = os.path.join(self.test_data, "JobFiles/Extract/reextract.job")
        shutil.copy(testfile, "test.job")
        the_rj = RunJobFile("test.job")
        assert the_rj.get_all_options()["_rlnJobTypeLabel"] == "relion.extract"

    def test_reading_file_with_env_dir(self):
        testfile = os.path.join(self.test_data, "StarFiles/env_var.job")
        the_rj = RunJobFile(testfile)
        assert the_rj.get_all_options()["scratch_dir"] == "$SCRATCH_DIR"


if __name__ == "__main__":
    unittest.main()
