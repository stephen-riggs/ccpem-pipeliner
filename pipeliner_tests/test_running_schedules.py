#
#    Copyright (C) 2023 CCP-EM
#
#    This Source Code Form is subject to the terms of the Mozilla
#    Public License, v. 2.0. If a copy of the MPL was not
#    distributed with this file, You can obtain one at
#    https://mozilla.org/MPL/2.0/
#

import unittest
import os
import shutil
import tempfile
from pathlib import Path

from pipeliner import job_manager, job_factory
from pipeliner.project_graph import ProjectGraph
from pipeliner_tests import test_data
from pipeliner.data_structure import SUCCESS_FILE
from pipeliner.utils import touch
from pipeliner.data_structure import JOBSTATUS_SUCCESS
from pipeliner_tests.testing_tools import live_test


class JobManagerScheduleTest(unittest.TestCase):
    def setUp(self):
        """
        Setup test data and output directories.
        """
        self.test_data = Path(test_data.__file__).parent
        self.test_dir = tempfile.mkdtemp(prefix="pipeliner_schedule_test_")

        # Change to test directory
        self._orig_dir = os.getcwd()
        os.chdir(self.test_dir)

    def tearDown(self):
        os.chdir(self._orig_dir)
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_running_schedule_with_no_pipeline_star_file(self):
        with self.assertRaises(FileNotFoundError):
            job_manager.run_schedule(
                fn_sched="schedule1",
                job_ids=["Dummy/job234/"],
                nr_repeat=1,
                minutes_wait=0,
                minutes_wait_before=0,
                seconds_wait_after=0,
                pipeline_name="sched_simple",
            )
        assert not Path("RUNNING_PIPELINER_sched_simple_schedule1").is_file()

    def test_running_schedule_with_nonexistent_job(self):
        shutil.copy(
            self.test_data / "Pipelines" / "sched_simple_pipeline.star", self.test_dir
        )
        job_manager.run_schedule(
            fn_sched="schedule1",
            job_ids=["Dummy/job234/"],
            nr_repeat=1,
            minutes_wait=0,
            minutes_wait_before=0,
            seconds_wait_after=0,
            pipeline_name="sched_simple",
        )
        assert not Path("RUNNING_PIPELINER_sched_simple_schedule1").is_file()
        logfile = Path("pipeline_schedule1.log")
        assert logfile.is_file()
        assert (
            "ERROR: Cannot find process with name/alias: Dummy/job234/, schedule"
            " terminated" in logfile.read_text()
        )

    def test_running_schedule_with_unscheduled_job(self):
        shutil.copy(
            self.test_data / "Pipelines" / "sched_simple_pipeline.star", self.test_dir
        )
        refine3D_job_dir = Path("Refine3D/job001")
        refine3D_job_dir.mkdir(parents=True)
        shutil.copy(
            self.test_data / "JobFiles" / "Refine3D" / "relion_refine3D_job.star",
            refine3D_job_dir / "job.star",
        )
        job_manager.run_schedule(
            fn_sched="schedule1",
            job_ids=["Refine3D/job001/"],
            nr_repeat=1,
            minutes_wait=0,
            minutes_wait_before=0,
            seconds_wait_after=0,
            pipeline_name="sched_simple",
        )
        assert not Path("RUNNING_PIPELINER_sched_simple_schedule1").is_file()
        logfile = Path("pipeline_schedule1.log")
        assert logfile.is_file()
        assert (
            "ERROR: There was a problem running job Refine3D/job001/, schedule"
            " terminated" in logfile.read_text()
        )

    def test_schedule_locked(self):
        touch("RUNNING_PIPELINER_sched_simple_schedule1")
        shutil.copy(
            os.path.join(self.test_data, "Pipelines/sched_simple_pipeline.star"),
            self.test_dir,
        )

        with self.assertRaises(ValueError):
            job_manager.run_schedule(
                fn_sched="schedule1",
                job_ids=["Import/job002/", "PostProcess/job003/"],
                nr_repeat=1,
                minutes_wait=0,
                minutes_wait_before=0,
                seconds_wait_after=0,
                pipeline_name="sched_simple",
            )

    @live_test(jobs=["relion.import", "relion.postprocess"])
    def test_simple_schedule(self):
        """import a mask and run PostProcess"""
        # Prepare the directory structure as if Refine3D job has been run
        fake_refine3D = Path("Refine3D/job001")
        fake_refine3D.mkdir(parents=True)

        shutil.copy(self.test_data / "3488_run_half1_class001_unfil.mrc", fake_refine3D)
        shutil.copy(self.test_data / "3488_run_half2_class001_unfil.mrc", fake_refine3D)
        shutil.copy(self.test_data / "emd_3488_mask.mrc", self.test_dir)

        shutil.copy(
            self.test_data / "Pipelines/sched_simple_pipeline.star",
            self.test_dir,
        )
        import_job = job_factory.read_job(
            os.fspath(self.test_data / "JobFiles/Import/import_mask_job.star")
        )
        pp_job = job_factory.read_job(
            os.fspath(
                self.test_data / "JobFiles/PostProcess/postprocess_for_sched_job.star"
            )
        )
        with ProjectGraph(name="sched_simple", read_only=False) as pipeline:
            pipeline.job_counter = 2
            job_manager.schedule_job(pipeline=pipeline, job=import_job)
            job_manager.schedule_job(
                pipeline=pipeline,
                job=pp_job,
                overwrite=pipeline.find_process("PostProcess/job003/"),
            )

        job_manager.run_schedule(
            fn_sched="schedule1",
            job_ids=["Import/job002/", "PostProcess/job003/"],
            nr_repeat=1,
            minutes_wait=0,
            minutes_wait_before=0,
            seconds_wait_after=0,
            pipeline_name="sched_simple",
        )
        with ProjectGraph(name="sched_simple") as pipeline:
            assert pipeline.find_process("Import/job002/").status == JOBSTATUS_SUCCESS
            assert (
                pipeline.find_process("PostProcess/job003/").status == JOBSTATUS_SUCCESS
            )

        import_files = [
            SUCCESS_FILE,
            "emd_3488_mask.mrc",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        pp_files = [
            SUCCESS_FILE,
            "job.star",
            "job_pipeline.star",
            "logfile.pdf",
            "logfile.pdf.lst",
            "note.txt",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        import_dir = Path("Import/job002")
        for fname in import_files:
            assert (import_dir / fname).is_file()
        pp_dir = Path("PostProcess/job003")
        for fname in pp_files:
            assert (pp_dir / fname).is_file()

        log_data = Path("pipeline_schedule1.log").read_text()

        loglines = [
            "-- Starting repeat 1/1",
            "---- Executing Import/job002/",
            "---- Executing PostProcess/job003/",
            "+ Performed all requested repeats in schedule schedule1. Stopping "
            "pipeliner now ...",
        ]
        for line in loglines:
            assert line in log_data

    @live_test(jobs=["relion.import", "relion.postprocess"])
    def test_simple_schedule_with_repeats(self):
        """import a mask and run PostProcess do it three times to test repeating"""
        # Prepare the directory structure as if Refine3D job has been run
        fake_refine3D = Path("Refine3D/job001")
        fake_refine3D.mkdir(parents=True)

        shutil.copy(self.test_data / "3488_run_half1_class001_unfil.mrc", fake_refine3D)
        shutil.copy(self.test_data / "3488_run_half2_class001_unfil.mrc", fake_refine3D)
        shutil.copy(self.test_data / "emd_3488_mask.mrc", self.test_dir)

        shutil.copy(
            self.test_data / "Pipelines/sched_simple_pipeline.star",
            self.test_dir,
        )
        import_job = job_factory.read_job(
            os.fspath(self.test_data / "JobFiles/Import/import_mask_job.star")
        )
        pp_job = job_factory.read_job(
            os.fspath(
                self.test_data / "JobFiles/PostProcess/postprocess_for_sched_job.star"
            )
        )
        with ProjectGraph(name="sched_simple", read_only=False) as pipeline:
            pipeline.job_counter = 2
            job_manager.schedule_job(pipeline=pipeline, job=import_job)
            job_manager.schedule_job(
                pipeline=pipeline,
                job=pp_job,
                overwrite=pipeline.find_process("PostProcess/job003/"),
            )

        with open("PostProcess/job003/job.star") as original_jobstar:
            original_js_data = [x.split() for x in original_jobstar.readlines()]
        assert ["_rlnJobIsContinue", "0"] in original_js_data
        assert ["_rlnJobIsContinue", "1"] not in original_js_data

        job_manager.run_schedule(
            fn_sched="schedule1",
            job_ids=["Import/job002/", "PostProcess/job003/"],
            nr_repeat=3,
            minutes_wait=0,
            minutes_wait_before=0,
            seconds_wait_after=1,
            pipeline_name="sched_simple",
        )
        with ProjectGraph(name="sched_simple") as pipeline:
            assert pipeline.find_process("Import/job002/").status == JOBSTATUS_SUCCESS
            assert (
                pipeline.find_process("PostProcess/job003/").status == JOBSTATUS_SUCCESS
            )

        import_files = [
            SUCCESS_FILE,
            "emd_3488_mask.mrc",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        pp_files = [
            SUCCESS_FILE,
            "job.star",
            "job_pipeline.star",
            "logfile.pdf",
            "logfile.pdf.lst",
            "note.txt",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        import_dir = Path("Import/job002")
        for fname in import_files:
            assert (import_dir / fname).is_file()
        pp_dir = Path("PostProcess/job003")
        for fname in pp_files:
            assert (pp_dir / fname).is_file()

        log_data = Path("pipeline_schedule1.log").read_text()

        loglines = [
            "-- Starting repeat 1/3",
            "-- Starting repeat 2/3",
            "-- Starting repeat 3/3",
            "---- Executing Import/job002/",
            "---- Executing PostProcess/job003/",
            "+ Performed all requested repeats in schedule schedule1. Stopping "
            "pipeliner now ...",
        ]

        with open("PostProcess/job003/job.star") as final_jobstar:
            final_js_data = [x.split() for x in final_jobstar.readlines()]
        assert ["_rlnJobIsContinue", "1"] in final_js_data
        assert ["_rlnJobIsContinue", "0"] not in final_js_data

        # test that postprocess actually ran three times
        linecount = 0
        with open("PostProcess/job003/run.out") as pp_runout:
            pp_data = pp_runout.readlines()
        for line in pp_data:
            if "+ FINAL RESOLUTION:" in line:
                linecount += 1
        assert linecount == 3

        for line in loglines:
            assert line in log_data, line

    @live_test(jobs=["relion.import", "relion.postprocess"])
    def test_simple_schedule_with_repeats_nonzero_waititime(self):
        """import a mask and run PostProcess do it three times to test repeating
        one minute wait between jobs to make sure it works"""
        # Prepare the directory structure as if Refine3D job has been run
        fake_refine3D = Path("Refine3D/job001")
        fake_refine3D.mkdir(parents=True)

        shutil.copy(self.test_data / "3488_run_half1_class001_unfil.mrc", fake_refine3D)
        shutil.copy(self.test_data / "3488_run_half2_class001_unfil.mrc", fake_refine3D)
        shutil.copy(self.test_data / "emd_3488_mask.mrc", self.test_dir)

        shutil.copy(
            self.test_data / "Pipelines/sched_simple_pipeline.star",
            self.test_dir,
        )
        import_job = job_factory.read_job(
            os.fspath(self.test_data / "JobFiles/Import/import_mask_job.star")
        )
        pp_job = job_factory.read_job(
            os.fspath(
                self.test_data / "JobFiles/PostProcess/postprocess_for_sched_job.star"
            )
        )
        with ProjectGraph(name="sched_simple", read_only=False) as pipeline:
            pipeline.job_counter = 2
            job_manager.schedule_job(pipeline=pipeline, job=import_job)
            job_manager.schedule_job(
                pipeline=pipeline,
                job=pp_job,
                overwrite=pipeline.find_process("PostProcess/job003/"),
            )

        with open("PostProcess/job003/job.star") as original_jobstar:
            original_js_data = [x.split() for x in original_jobstar.readlines()]
        assert ["_rlnJobIsContinue", "0"] in original_js_data
        assert ["_rlnJobIsContinue", "1"] not in original_js_data

        job_manager.run_schedule(
            fn_sched="schedule1",
            job_ids=["Import/job002/", "PostProcess/job003/"],
            nr_repeat=3,
            minutes_wait=1,
            minutes_wait_before=0,
            seconds_wait_after=1,
            pipeline_name="sched_simple",
        )
        with ProjectGraph(name="sched_simple") as pipeline:
            assert pipeline.find_process("Import/job002/").status == JOBSTATUS_SUCCESS
            assert (
                pipeline.find_process("PostProcess/job003/").status == JOBSTATUS_SUCCESS
            )

        import_files = [
            SUCCESS_FILE,
            "emd_3488_mask.mrc",
            "job.star",
            "job_pipeline.star",
            "note.txt",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        pp_files = [
            SUCCESS_FILE,
            "job.star",
            "job_pipeline.star",
            "logfile.pdf",
            "logfile.pdf.lst",
            "note.txt",
            "postprocess.mrc",
            "postprocess.star",
            "postprocess_fsc.eps",
            "postprocess_fsc.xml",
            "postprocess_guinier.eps",
            "postprocess_masked.mrc",
            "run.err",
            "run.job",
            "run.out",
            "sched_simple_pipeline.star",
        ]
        import_dir = Path("Import/job002")
        for fname in import_files:
            assert (import_dir / fname).is_file()
        pp_dir = Path("PostProcess/job003")
        for fname in pp_files:
            assert (pp_dir / fname).is_file()

        log_data = Path("pipeline_schedule1.log").read_text()

        loglines = [
            "-- Starting repeat 1/3",
            "-- Starting repeat 2/3",
            "-- Starting repeat 3/3",
            "---- Executing Import/job002/",
            "---- Executing PostProcess/job003/",
            "+ Performed all requested repeats in schedule schedule1. Stopping "
            "pipeliner now ...",
        ]
        with open("PostProcess/job003/job.star") as final_jobstar:
            final_js_data = [x.split() for x in final_jobstar.readlines()]
        assert ["_rlnJobIsContinue", "1"] in final_js_data
        assert ["_rlnJobIsContinue", "0"] not in final_js_data

        # test that postprocess actually ran three times
        linecount = 0
        with open("PostProcess/job003/run.out") as pp_runout:
            pp_data = pp_runout.readlines()
        for line in pp_data:
            if "+ FINAL RESOLUTION:" in line:
                linecount += 1
        assert linecount == 3

        for line in loglines:
            assert line in log_data, line


if __name__ == "__main__":
    unittest.main()
